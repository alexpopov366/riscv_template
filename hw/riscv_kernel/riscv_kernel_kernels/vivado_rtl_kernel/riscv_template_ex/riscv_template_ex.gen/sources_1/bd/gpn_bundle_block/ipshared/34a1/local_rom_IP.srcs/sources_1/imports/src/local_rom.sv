/*
 * Copyright © 2018 Eric Matthews,  Lesley Shannon
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Initial code developed under the supervision of Dr. Lesley Shannon,
 * Reconfigurable Computing Lab, Simon Fraser University.
 *
 * Author(s):
 *             Eric Matthews <ematthew@sfu.ca>
 */
 

module  local_rom
        #(
        parameter RAM_SIZE = 128,
        parameter preload_file = "/home/user/2020/taiga_booloader_v01/bootloader.hw_init",
        parameter USE_PRELOAD_FILE = 1
        )
        (
        input logic clk,
        input logic rstn,
        local_memory_interface.slave portA
        );
        
        localparam LINES = RAM_SIZE/4; //RAM width is 32-bits, so for RAM_SIZE in KB, divide by 4 and multiply by 1024.

        byte_en_ROM #(LINES, preload_file, USE_PRELOAD_FILE) inst_data_rom (
            .clk(clk),
            .addr_a(portA.addr[$clog2(LINES)- 1:0]),
            .en_a(portA.en),
            .be_a(portA.be),
            .data_in_a(portA.data_in),
            .data_out_a(portA.data_out)
        );

endmodule
