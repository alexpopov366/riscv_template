-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
-- Date        : Mon Aug 29 21:30:26 2022
-- Host        : dl580 running 64-bit Ubuntu 18.04.6 LTS
-- Command     : write_vhdl -force -mode funcsim
--               /home/user/MPS/riscv_kernel/riscv_kernel_kernels/vivado_rtl_kernel/riscv_template_ex/riscv_template_ex.gen/sources_1/bd/gpn_bundle_block_v001/ip/gpn_bundle_block_local_mem_0_0/gpn_bundle_block_local_mem_0_0_sim_netlist.vhdl
-- Design      : gpn_bundle_block_local_mem_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xcu200-fsgd2104-2-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gpn_bundle_block_local_mem_0_0_xilinx_byte_enable_ram is
  port (
    portA_data_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    portB_data_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clk : in STD_LOGIC;
    portA_addr : in STD_LOGIC_VECTOR ( 13 downto 0 );
    portB_addr : in STD_LOGIC_VECTOR ( 13 downto 0 );
    portA_data_in : in STD_LOGIC_VECTOR ( 31 downto 0 );
    portB_data_in : in STD_LOGIC_VECTOR ( 31 downto 0 );
    portA_en : in STD_LOGIC;
    portB_en : in STD_LOGIC;
    portA_be : in STD_LOGIC_VECTOR ( 3 downto 0 );
    portB_be : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gpn_bundle_block_local_mem_0_0_xilinx_byte_enable_ram : entity is "xilinx_byte_enable_ram";
end gpn_bundle_block_local_mem_0_0_xilinx_byte_enable_ram;

architecture STRUCTURE of gpn_bundle_block_local_mem_0_0_xilinx_byte_enable_ram is
  signal \genblk2[1].ram_reg_0_bram_0_i_1_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_0_i_2_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_0_i_3_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_0_i_4_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_0_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_0_n_1\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_0_n_132\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_0_n_133\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_0_n_134\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_0_n_135\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_0_n_136\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_0_n_137\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_0_n_138\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_0_n_139\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_0_n_28\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_0_n_29\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_0_n_30\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_0_n_31\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_0_n_32\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_0_n_33\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_0_n_34\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_0_n_35\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_0_n_60\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_0_n_61\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_0_n_62\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_0_n_63\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_0_n_64\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_0_n_65\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_0_n_66\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_0_n_67\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_1_i_1_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_1_i_2_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_1_i_3_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_1_i_4_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_1_i_5_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_1_i_6_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_1_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_1_n_1\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_1_n_132\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_1_n_133\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_1_n_134\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_1_n_135\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_1_n_136\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_1_n_137\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_1_n_138\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_1_n_139\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_1_n_28\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_1_n_29\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_1_n_30\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_1_n_31\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_1_n_32\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_1_n_33\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_1_n_34\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_1_n_35\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_1_n_60\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_1_n_61\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_1_n_62\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_1_n_63\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_1_n_64\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_1_n_65\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_1_n_66\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_1_n_67\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_2_i_1_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_2_i_2_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_2_i_3_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_2_i_4_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_2_i_5_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_2_i_6_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_2_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_2_n_1\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_2_n_132\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_2_n_133\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_2_n_134\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_2_n_135\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_2_n_136\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_2_n_137\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_2_n_138\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_2_n_139\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_2_n_28\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_2_n_29\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_2_n_30\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_2_n_31\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_2_n_32\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_2_n_33\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_2_n_34\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_2_n_35\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_2_n_60\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_2_n_61\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_2_n_62\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_2_n_63\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_2_n_64\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_2_n_65\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_2_n_66\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_2_n_67\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_3_i_1_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_0_bram_3_i_2_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_0_i_1_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_0_i_2_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_0_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_0_n_1\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_0_n_132\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_0_n_133\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_0_n_134\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_0_n_135\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_0_n_136\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_0_n_137\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_0_n_138\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_0_n_139\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_0_n_28\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_0_n_29\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_0_n_30\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_0_n_31\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_0_n_32\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_0_n_33\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_0_n_34\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_0_n_35\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_0_n_60\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_0_n_61\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_0_n_62\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_0_n_63\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_0_n_64\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_0_n_65\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_0_n_66\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_0_n_67\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_1_i_1_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_1_i_2_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_1_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_1_n_1\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_1_n_132\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_1_n_133\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_1_n_134\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_1_n_135\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_1_n_136\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_1_n_137\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_1_n_138\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_1_n_139\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_1_n_28\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_1_n_29\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_1_n_30\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_1_n_31\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_1_n_32\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_1_n_33\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_1_n_34\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_1_n_35\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_1_n_60\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_1_n_61\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_1_n_62\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_1_n_63\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_1_n_64\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_1_n_65\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_1_n_66\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_1_n_67\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_2_i_1_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_2_i_2_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_2_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_2_n_1\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_2_n_132\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_2_n_133\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_2_n_134\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_2_n_135\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_2_n_136\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_2_n_137\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_2_n_138\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_2_n_139\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_2_n_28\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_2_n_29\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_2_n_30\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_2_n_31\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_2_n_32\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_2_n_33\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_2_n_34\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_2_n_35\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_2_n_60\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_2_n_61\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_2_n_62\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_2_n_63\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_2_n_64\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_2_n_65\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_2_n_66\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_2_n_67\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_3_i_1_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_1_bram_3_i_2_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_0_i_1_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_0_i_2_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_0_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_0_n_1\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_0_n_132\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_0_n_133\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_0_n_134\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_0_n_135\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_0_n_136\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_0_n_137\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_0_n_138\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_0_n_139\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_0_n_28\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_0_n_29\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_0_n_30\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_0_n_31\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_0_n_32\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_0_n_33\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_0_n_34\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_0_n_35\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_0_n_60\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_0_n_61\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_0_n_62\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_0_n_63\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_0_n_64\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_0_n_65\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_0_n_66\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_0_n_67\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_1_i_1_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_1_i_2_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_1_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_1_n_1\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_1_n_132\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_1_n_133\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_1_n_134\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_1_n_135\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_1_n_136\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_1_n_137\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_1_n_138\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_1_n_139\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_1_n_28\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_1_n_29\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_1_n_30\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_1_n_31\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_1_n_32\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_1_n_33\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_1_n_34\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_1_n_35\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_1_n_60\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_1_n_61\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_1_n_62\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_1_n_63\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_1_n_64\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_1_n_65\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_1_n_66\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_1_n_67\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_2_i_1_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_2_i_2_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_2_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_2_n_1\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_2_n_132\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_2_n_133\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_2_n_134\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_2_n_135\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_2_n_136\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_2_n_137\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_2_n_138\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_2_n_139\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_2_n_28\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_2_n_29\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_2_n_30\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_2_n_31\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_2_n_32\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_2_n_33\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_2_n_34\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_2_n_35\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_2_n_60\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_2_n_61\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_2_n_62\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_2_n_63\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_2_n_64\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_2_n_65\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_2_n_66\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_2_n_67\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_3_i_17_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_2_bram_3_i_18_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_0_i_1_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_0_i_2_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_0_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_0_n_1\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_0_n_132\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_0_n_133\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_0_n_134\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_0_n_135\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_0_n_136\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_0_n_137\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_0_n_138\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_0_n_139\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_0_n_28\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_0_n_29\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_0_n_30\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_0_n_31\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_0_n_32\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_0_n_33\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_0_n_34\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_0_n_35\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_0_n_60\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_0_n_61\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_0_n_62\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_0_n_63\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_0_n_64\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_0_n_65\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_0_n_66\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_0_n_67\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_1_i_1_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_1_i_2_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_1_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_1_n_1\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_1_n_132\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_1_n_133\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_1_n_134\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_1_n_135\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_1_n_136\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_1_n_137\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_1_n_138\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_1_n_139\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_1_n_28\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_1_n_29\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_1_n_30\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_1_n_31\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_1_n_32\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_1_n_33\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_1_n_34\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_1_n_35\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_1_n_60\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_1_n_61\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_1_n_62\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_1_n_63\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_1_n_64\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_1_n_65\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_1_n_66\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_1_n_67\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_2_i_1_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_2_i_2_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_2_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_2_n_1\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_2_n_132\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_2_n_133\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_2_n_134\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_2_n_135\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_2_n_136\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_2_n_137\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_2_n_138\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_2_n_139\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_2_n_28\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_2_n_29\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_2_n_30\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_2_n_31\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_2_n_32\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_2_n_33\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_2_n_34\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_2_n_35\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_2_n_60\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_2_n_61\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_2_n_62\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_2_n_63\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_2_n_64\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_2_n_65\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_2_n_66\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_2_n_67\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_3_i_1_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_3_i_21_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_3_i_22_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_3_i_2_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_3_i_3_n_0\ : STD_LOGIC;
  signal \genblk2[1].ram_reg_3_bram_3_i_4_n_0\ : STD_LOGIC;
  signal p_1_in : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal p_2_in : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal \NLW_genblk2[1].ram_reg_0_bram_0_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_0_bram_0_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_0_bram_0_CASDOUTA_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_genblk2[1].ram_reg_0_bram_0_CASDOUTB_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_genblk2[1].ram_reg_0_bram_0_DOUTADOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_genblk2[1].ram_reg_0_bram_0_DOUTBDOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_genblk2[1].ram_reg_0_bram_0_DOUTPADOUTP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_0_bram_0_DOUTPBDOUTP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_0_bram_0_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_genblk2[1].ram_reg_0_bram_0_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \NLW_genblk2[1].ram_reg_0_bram_1_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_0_bram_1_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_0_bram_1_CASDOUTA_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_genblk2[1].ram_reg_0_bram_1_CASDOUTB_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_genblk2[1].ram_reg_0_bram_1_DOUTADOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_genblk2[1].ram_reg_0_bram_1_DOUTBDOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_genblk2[1].ram_reg_0_bram_1_DOUTPADOUTP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_0_bram_1_DOUTPBDOUTP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_0_bram_1_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_genblk2[1].ram_reg_0_bram_1_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \NLW_genblk2[1].ram_reg_0_bram_2_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_0_bram_2_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_0_bram_2_CASDOUTA_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_genblk2[1].ram_reg_0_bram_2_CASDOUTB_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_genblk2[1].ram_reg_0_bram_2_DOUTADOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_genblk2[1].ram_reg_0_bram_2_DOUTBDOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_genblk2[1].ram_reg_0_bram_2_DOUTPADOUTP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_0_bram_2_DOUTPBDOUTP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_0_bram_2_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_genblk2[1].ram_reg_0_bram_2_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \NLW_genblk2[1].ram_reg_0_bram_3_CASOUTDBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_0_bram_3_CASOUTSBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_0_bram_3_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_0_bram_3_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_0_bram_3_CASDOUTA_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_genblk2[1].ram_reg_0_bram_3_CASDOUTB_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_genblk2[1].ram_reg_0_bram_3_CASDOUTPA_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_0_bram_3_CASDOUTPB_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_0_bram_3_DOUTADOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_genblk2[1].ram_reg_0_bram_3_DOUTBDOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_genblk2[1].ram_reg_0_bram_3_DOUTPADOUTP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_0_bram_3_DOUTPBDOUTP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_0_bram_3_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_genblk2[1].ram_reg_0_bram_3_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \NLW_genblk2[1].ram_reg_1_bram_0_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_1_bram_0_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_1_bram_0_CASDOUTA_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_genblk2[1].ram_reg_1_bram_0_CASDOUTB_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_genblk2[1].ram_reg_1_bram_0_DOUTADOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_genblk2[1].ram_reg_1_bram_0_DOUTBDOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_genblk2[1].ram_reg_1_bram_0_DOUTPADOUTP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_1_bram_0_DOUTPBDOUTP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_1_bram_0_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_genblk2[1].ram_reg_1_bram_0_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \NLW_genblk2[1].ram_reg_1_bram_1_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_1_bram_1_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_1_bram_1_CASDOUTA_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_genblk2[1].ram_reg_1_bram_1_CASDOUTB_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_genblk2[1].ram_reg_1_bram_1_DOUTADOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_genblk2[1].ram_reg_1_bram_1_DOUTBDOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_genblk2[1].ram_reg_1_bram_1_DOUTPADOUTP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_1_bram_1_DOUTPBDOUTP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_1_bram_1_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_genblk2[1].ram_reg_1_bram_1_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \NLW_genblk2[1].ram_reg_1_bram_2_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_1_bram_2_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_1_bram_2_CASDOUTA_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_genblk2[1].ram_reg_1_bram_2_CASDOUTB_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_genblk2[1].ram_reg_1_bram_2_DOUTADOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_genblk2[1].ram_reg_1_bram_2_DOUTBDOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_genblk2[1].ram_reg_1_bram_2_DOUTPADOUTP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_1_bram_2_DOUTPBDOUTP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_1_bram_2_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_genblk2[1].ram_reg_1_bram_2_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \NLW_genblk2[1].ram_reg_1_bram_3_CASOUTDBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_1_bram_3_CASOUTSBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_1_bram_3_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_1_bram_3_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_1_bram_3_CASDOUTA_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_genblk2[1].ram_reg_1_bram_3_CASDOUTB_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_genblk2[1].ram_reg_1_bram_3_CASDOUTPA_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_1_bram_3_CASDOUTPB_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_1_bram_3_DOUTADOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_genblk2[1].ram_reg_1_bram_3_DOUTBDOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_genblk2[1].ram_reg_1_bram_3_DOUTPADOUTP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_1_bram_3_DOUTPBDOUTP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_1_bram_3_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_genblk2[1].ram_reg_1_bram_3_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \NLW_genblk2[1].ram_reg_2_bram_0_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_2_bram_0_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_2_bram_0_CASDOUTA_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_genblk2[1].ram_reg_2_bram_0_CASDOUTB_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_genblk2[1].ram_reg_2_bram_0_DOUTADOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_genblk2[1].ram_reg_2_bram_0_DOUTBDOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_genblk2[1].ram_reg_2_bram_0_DOUTPADOUTP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_2_bram_0_DOUTPBDOUTP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_2_bram_0_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_genblk2[1].ram_reg_2_bram_0_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \NLW_genblk2[1].ram_reg_2_bram_1_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_2_bram_1_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_2_bram_1_CASDOUTA_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_genblk2[1].ram_reg_2_bram_1_CASDOUTB_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_genblk2[1].ram_reg_2_bram_1_DOUTADOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_genblk2[1].ram_reg_2_bram_1_DOUTBDOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_genblk2[1].ram_reg_2_bram_1_DOUTPADOUTP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_2_bram_1_DOUTPBDOUTP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_2_bram_1_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_genblk2[1].ram_reg_2_bram_1_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \NLW_genblk2[1].ram_reg_2_bram_2_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_2_bram_2_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_2_bram_2_CASDOUTA_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_genblk2[1].ram_reg_2_bram_2_CASDOUTB_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_genblk2[1].ram_reg_2_bram_2_DOUTADOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_genblk2[1].ram_reg_2_bram_2_DOUTBDOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_genblk2[1].ram_reg_2_bram_2_DOUTPADOUTP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_2_bram_2_DOUTPBDOUTP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_2_bram_2_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_genblk2[1].ram_reg_2_bram_2_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \NLW_genblk2[1].ram_reg_2_bram_3_CASOUTDBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_2_bram_3_CASOUTSBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_2_bram_3_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_2_bram_3_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_2_bram_3_CASDOUTA_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_genblk2[1].ram_reg_2_bram_3_CASDOUTB_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_genblk2[1].ram_reg_2_bram_3_CASDOUTPA_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_2_bram_3_CASDOUTPB_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_2_bram_3_DOUTADOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_genblk2[1].ram_reg_2_bram_3_DOUTBDOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_genblk2[1].ram_reg_2_bram_3_DOUTPADOUTP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_2_bram_3_DOUTPBDOUTP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_2_bram_3_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_genblk2[1].ram_reg_2_bram_3_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \NLW_genblk2[1].ram_reg_3_bram_0_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_3_bram_0_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_3_bram_0_CASDOUTA_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_genblk2[1].ram_reg_3_bram_0_CASDOUTB_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_genblk2[1].ram_reg_3_bram_0_DOUTADOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_genblk2[1].ram_reg_3_bram_0_DOUTBDOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_genblk2[1].ram_reg_3_bram_0_DOUTPADOUTP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_3_bram_0_DOUTPBDOUTP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_3_bram_0_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_genblk2[1].ram_reg_3_bram_0_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \NLW_genblk2[1].ram_reg_3_bram_1_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_3_bram_1_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_3_bram_1_CASDOUTA_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_genblk2[1].ram_reg_3_bram_1_CASDOUTB_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_genblk2[1].ram_reg_3_bram_1_DOUTADOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_genblk2[1].ram_reg_3_bram_1_DOUTBDOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_genblk2[1].ram_reg_3_bram_1_DOUTPADOUTP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_3_bram_1_DOUTPBDOUTP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_3_bram_1_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_genblk2[1].ram_reg_3_bram_1_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \NLW_genblk2[1].ram_reg_3_bram_2_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_3_bram_2_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_3_bram_2_CASDOUTA_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_genblk2[1].ram_reg_3_bram_2_CASDOUTB_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_genblk2[1].ram_reg_3_bram_2_DOUTADOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_genblk2[1].ram_reg_3_bram_2_DOUTBDOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_genblk2[1].ram_reg_3_bram_2_DOUTPADOUTP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_3_bram_2_DOUTPBDOUTP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_3_bram_2_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_genblk2[1].ram_reg_3_bram_2_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \NLW_genblk2[1].ram_reg_3_bram_3_CASOUTDBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_3_bram_3_CASOUTSBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_3_bram_3_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_3_bram_3_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_genblk2[1].ram_reg_3_bram_3_CASDOUTA_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_genblk2[1].ram_reg_3_bram_3_CASDOUTB_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_genblk2[1].ram_reg_3_bram_3_CASDOUTPA_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_3_bram_3_CASDOUTPB_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_3_bram_3_DOUTADOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_genblk2[1].ram_reg_3_bram_3_DOUTBDOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_genblk2[1].ram_reg_3_bram_3_DOUTPADOUTP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_3_bram_3_DOUTPBDOUTP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_genblk2[1].ram_reg_3_bram_3_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_genblk2[1].ram_reg_3_bram_3_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of \genblk2[1].ram_reg_0_bram_0\ : label is "p0_d8";
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ of \genblk2[1].ram_reg_0_bram_0\ : label is "p0_d8";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of \genblk2[1].ram_reg_0_bram_0\ : label is "{SYNTH-15 {cell *THIS*} {string {address width (14) is more than optimal threshold of 12. Implementing using BWWE will require more logic and timing would be suboptimal. Please use attribute ram_decomp = power if BWWE is desired.}}} {SYNTH-7 {cell *THIS*}}";
  attribute RDADDR_COLLISION_HWCONFIG : string;
  attribute RDADDR_COLLISION_HWCONFIG of \genblk2[1].ram_reg_0_bram_0\ : label is "DELAYED_WRITE";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of \genblk2[1].ram_reg_0_bram_0\ : label is 524288;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of \genblk2[1].ram_reg_0_bram_0\ : label is "genblk2[1].ram";
  attribute RTL_RAM_TYPE : string;
  attribute RTL_RAM_TYPE of \genblk2[1].ram_reg_0_bram_0\ : label is "RAM_TDP";
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of \genblk2[1].ram_reg_0_bram_0\ : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of \genblk2[1].ram_reg_0_bram_0\ : label is 4095;
  attribute ram_offset : integer;
  attribute ram_offset of \genblk2[1].ram_reg_0_bram_0\ : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of \genblk2[1].ram_reg_0_bram_0\ : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of \genblk2[1].ram_reg_0_bram_0\ : label is 7;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_0_bram_0_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_0_bram_0_i_2\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_0_bram_0_i_3\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_0_bram_0_i_4\ : label is "soft_lutpair2";
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of \genblk2[1].ram_reg_0_bram_1\ : label is "p0_d8";
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ of \genblk2[1].ram_reg_0_bram_1\ : label is "p0_d8";
  attribute METHODOLOGY_DRC_VIOS of \genblk2[1].ram_reg_0_bram_1\ : label is "{SYNTH-15 {cell *THIS*} {string {address width (14) is more than optimal threshold of 12. Implementing using BWWE will require more logic and timing would be suboptimal. Please use attribute ram_decomp = power if BWWE is desired.}}} {SYNTH-7 {cell *THIS*}}";
  attribute RDADDR_COLLISION_HWCONFIG of \genblk2[1].ram_reg_0_bram_1\ : label is "DELAYED_WRITE";
  attribute RTL_RAM_BITS of \genblk2[1].ram_reg_0_bram_1\ : label is 524288;
  attribute RTL_RAM_NAME of \genblk2[1].ram_reg_0_bram_1\ : label is "genblk2[1].ram";
  attribute RTL_RAM_TYPE of \genblk2[1].ram_reg_0_bram_1\ : label is "RAM_TDP";
  attribute ram_addr_begin of \genblk2[1].ram_reg_0_bram_1\ : label is 4096;
  attribute ram_addr_end of \genblk2[1].ram_reg_0_bram_1\ : label is 8191;
  attribute ram_offset of \genblk2[1].ram_reg_0_bram_1\ : label is 0;
  attribute ram_slice_begin of \genblk2[1].ram_reg_0_bram_1\ : label is 0;
  attribute ram_slice_end of \genblk2[1].ram_reg_0_bram_1\ : label is 7;
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_0_bram_1_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_0_bram_1_i_2\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_0_bram_1_i_3\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_0_bram_1_i_4\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_0_bram_1_i_5\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_0_bram_1_i_6\ : label is "soft_lutpair2";
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of \genblk2[1].ram_reg_0_bram_2\ : label is "p0_d8";
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ of \genblk2[1].ram_reg_0_bram_2\ : label is "p0_d8";
  attribute METHODOLOGY_DRC_VIOS of \genblk2[1].ram_reg_0_bram_2\ : label is "{SYNTH-15 {cell *THIS*} {string {address width (14) is more than optimal threshold of 12. Implementing using BWWE will require more logic and timing would be suboptimal. Please use attribute ram_decomp = power if BWWE is desired.}}} {SYNTH-7 {cell *THIS*}}";
  attribute RDADDR_COLLISION_HWCONFIG of \genblk2[1].ram_reg_0_bram_2\ : label is "DELAYED_WRITE";
  attribute RTL_RAM_BITS of \genblk2[1].ram_reg_0_bram_2\ : label is 524288;
  attribute RTL_RAM_NAME of \genblk2[1].ram_reg_0_bram_2\ : label is "genblk2[1].ram";
  attribute RTL_RAM_TYPE of \genblk2[1].ram_reg_0_bram_2\ : label is "RAM_TDP";
  attribute ram_addr_begin of \genblk2[1].ram_reg_0_bram_2\ : label is 8192;
  attribute ram_addr_end of \genblk2[1].ram_reg_0_bram_2\ : label is 12287;
  attribute ram_offset of \genblk2[1].ram_reg_0_bram_2\ : label is 0;
  attribute ram_slice_begin of \genblk2[1].ram_reg_0_bram_2\ : label is 0;
  attribute ram_slice_end of \genblk2[1].ram_reg_0_bram_2\ : label is 7;
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_0_bram_2_i_3\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_0_bram_2_i_4\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_0_bram_2_i_5\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_0_bram_2_i_6\ : label is "soft_lutpair3";
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of \genblk2[1].ram_reg_0_bram_3\ : label is "p0_d8";
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ of \genblk2[1].ram_reg_0_bram_3\ : label is "p0_d8";
  attribute METHODOLOGY_DRC_VIOS of \genblk2[1].ram_reg_0_bram_3\ : label is "{SYNTH-15 {cell *THIS*} {string {address width (14) is more than optimal threshold of 12. Implementing using BWWE will require more logic and timing would be suboptimal. Please use attribute ram_decomp = power if BWWE is desired.}}} {SYNTH-6 {cell *THIS*}} {SYNTH-7 {cell *THIS*}}";
  attribute RDADDR_COLLISION_HWCONFIG of \genblk2[1].ram_reg_0_bram_3\ : label is "DELAYED_WRITE";
  attribute RTL_RAM_BITS of \genblk2[1].ram_reg_0_bram_3\ : label is 524288;
  attribute RTL_RAM_NAME of \genblk2[1].ram_reg_0_bram_3\ : label is "genblk2[1].ram";
  attribute RTL_RAM_TYPE of \genblk2[1].ram_reg_0_bram_3\ : label is "RAM_TDP";
  attribute ram_addr_begin of \genblk2[1].ram_reg_0_bram_3\ : label is 12288;
  attribute ram_addr_end of \genblk2[1].ram_reg_0_bram_3\ : label is 16383;
  attribute ram_offset of \genblk2[1].ram_reg_0_bram_3\ : label is 0;
  attribute ram_slice_begin of \genblk2[1].ram_reg_0_bram_3\ : label is 0;
  attribute ram_slice_end of \genblk2[1].ram_reg_0_bram_3\ : label is 7;
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_0_bram_3_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_0_bram_3_i_2\ : label is "soft_lutpair3";
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of \genblk2[1].ram_reg_1_bram_0\ : label is "p0_d8";
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ of \genblk2[1].ram_reg_1_bram_0\ : label is "p0_d8";
  attribute METHODOLOGY_DRC_VIOS of \genblk2[1].ram_reg_1_bram_0\ : label is "{SYNTH-15 {cell *THIS*} {string {address width (14) is more than optimal threshold of 12. Implementing using BWWE will require more logic and timing would be suboptimal. Please use attribute ram_decomp = power if BWWE is desired.}}} {SYNTH-7 {cell *THIS*}}";
  attribute RDADDR_COLLISION_HWCONFIG of \genblk2[1].ram_reg_1_bram_0\ : label is "DELAYED_WRITE";
  attribute RTL_RAM_BITS of \genblk2[1].ram_reg_1_bram_0\ : label is 524288;
  attribute RTL_RAM_NAME of \genblk2[1].ram_reg_1_bram_0\ : label is "genblk2[1].ram";
  attribute RTL_RAM_TYPE of \genblk2[1].ram_reg_1_bram_0\ : label is "RAM_TDP";
  attribute ram_addr_begin of \genblk2[1].ram_reg_1_bram_0\ : label is 0;
  attribute ram_addr_end of \genblk2[1].ram_reg_1_bram_0\ : label is 4095;
  attribute ram_offset of \genblk2[1].ram_reg_1_bram_0\ : label is 0;
  attribute ram_slice_begin of \genblk2[1].ram_reg_1_bram_0\ : label is 8;
  attribute ram_slice_end of \genblk2[1].ram_reg_1_bram_0\ : label is 15;
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_1_bram_0_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_1_bram_0_i_2\ : label is "soft_lutpair6";
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of \genblk2[1].ram_reg_1_bram_1\ : label is "p0_d8";
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ of \genblk2[1].ram_reg_1_bram_1\ : label is "p0_d8";
  attribute METHODOLOGY_DRC_VIOS of \genblk2[1].ram_reg_1_bram_1\ : label is "{SYNTH-15 {cell *THIS*} {string {address width (14) is more than optimal threshold of 12. Implementing using BWWE will require more logic and timing would be suboptimal. Please use attribute ram_decomp = power if BWWE is desired.}}} {SYNTH-7 {cell *THIS*}}";
  attribute RDADDR_COLLISION_HWCONFIG of \genblk2[1].ram_reg_1_bram_1\ : label is "DELAYED_WRITE";
  attribute RTL_RAM_BITS of \genblk2[1].ram_reg_1_bram_1\ : label is 524288;
  attribute RTL_RAM_NAME of \genblk2[1].ram_reg_1_bram_1\ : label is "genblk2[1].ram";
  attribute RTL_RAM_TYPE of \genblk2[1].ram_reg_1_bram_1\ : label is "RAM_TDP";
  attribute ram_addr_begin of \genblk2[1].ram_reg_1_bram_1\ : label is 4096;
  attribute ram_addr_end of \genblk2[1].ram_reg_1_bram_1\ : label is 8191;
  attribute ram_offset of \genblk2[1].ram_reg_1_bram_1\ : label is 0;
  attribute ram_slice_begin of \genblk2[1].ram_reg_1_bram_1\ : label is 8;
  attribute ram_slice_end of \genblk2[1].ram_reg_1_bram_1\ : label is 15;
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_1_bram_1_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_1_bram_1_i_2\ : label is "soft_lutpair6";
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of \genblk2[1].ram_reg_1_bram_2\ : label is "p0_d8";
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ of \genblk2[1].ram_reg_1_bram_2\ : label is "p0_d8";
  attribute METHODOLOGY_DRC_VIOS of \genblk2[1].ram_reg_1_bram_2\ : label is "{SYNTH-15 {cell *THIS*} {string {address width (14) is more than optimal threshold of 12. Implementing using BWWE will require more logic and timing would be suboptimal. Please use attribute ram_decomp = power if BWWE is desired.}}} {SYNTH-7 {cell *THIS*}}";
  attribute RDADDR_COLLISION_HWCONFIG of \genblk2[1].ram_reg_1_bram_2\ : label is "DELAYED_WRITE";
  attribute RTL_RAM_BITS of \genblk2[1].ram_reg_1_bram_2\ : label is 524288;
  attribute RTL_RAM_NAME of \genblk2[1].ram_reg_1_bram_2\ : label is "genblk2[1].ram";
  attribute RTL_RAM_TYPE of \genblk2[1].ram_reg_1_bram_2\ : label is "RAM_TDP";
  attribute ram_addr_begin of \genblk2[1].ram_reg_1_bram_2\ : label is 8192;
  attribute ram_addr_end of \genblk2[1].ram_reg_1_bram_2\ : label is 12287;
  attribute ram_offset of \genblk2[1].ram_reg_1_bram_2\ : label is 0;
  attribute ram_slice_begin of \genblk2[1].ram_reg_1_bram_2\ : label is 8;
  attribute ram_slice_end of \genblk2[1].ram_reg_1_bram_2\ : label is 15;
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_1_bram_2_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_1_bram_2_i_2\ : label is "soft_lutpair7";
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of \genblk2[1].ram_reg_1_bram_3\ : label is "p0_d8";
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ of \genblk2[1].ram_reg_1_bram_3\ : label is "p0_d8";
  attribute METHODOLOGY_DRC_VIOS of \genblk2[1].ram_reg_1_bram_3\ : label is "{SYNTH-15 {cell *THIS*} {string {address width (14) is more than optimal threshold of 12. Implementing using BWWE will require more logic and timing would be suboptimal. Please use attribute ram_decomp = power if BWWE is desired.}}} {SYNTH-6 {cell *THIS*}} {SYNTH-7 {cell *THIS*}}";
  attribute RDADDR_COLLISION_HWCONFIG of \genblk2[1].ram_reg_1_bram_3\ : label is "DELAYED_WRITE";
  attribute RTL_RAM_BITS of \genblk2[1].ram_reg_1_bram_3\ : label is 524288;
  attribute RTL_RAM_NAME of \genblk2[1].ram_reg_1_bram_3\ : label is "genblk2[1].ram";
  attribute RTL_RAM_TYPE of \genblk2[1].ram_reg_1_bram_3\ : label is "RAM_TDP";
  attribute ram_addr_begin of \genblk2[1].ram_reg_1_bram_3\ : label is 12288;
  attribute ram_addr_end of \genblk2[1].ram_reg_1_bram_3\ : label is 16383;
  attribute ram_offset of \genblk2[1].ram_reg_1_bram_3\ : label is 0;
  attribute ram_slice_begin of \genblk2[1].ram_reg_1_bram_3\ : label is 8;
  attribute ram_slice_end of \genblk2[1].ram_reg_1_bram_3\ : label is 15;
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_1_bram_3_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_1_bram_3_i_2\ : label is "soft_lutpair7";
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of \genblk2[1].ram_reg_2_bram_0\ : label is "p0_d8";
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ of \genblk2[1].ram_reg_2_bram_0\ : label is "p0_d8";
  attribute METHODOLOGY_DRC_VIOS of \genblk2[1].ram_reg_2_bram_0\ : label is "{SYNTH-15 {cell *THIS*} {string {address width (14) is more than optimal threshold of 12. Implementing using BWWE will require more logic and timing would be suboptimal. Please use attribute ram_decomp = power if BWWE is desired.}}} {SYNTH-7 {cell *THIS*}}";
  attribute RDADDR_COLLISION_HWCONFIG of \genblk2[1].ram_reg_2_bram_0\ : label is "DELAYED_WRITE";
  attribute RTL_RAM_BITS of \genblk2[1].ram_reg_2_bram_0\ : label is 524288;
  attribute RTL_RAM_NAME of \genblk2[1].ram_reg_2_bram_0\ : label is "genblk2[1].ram";
  attribute RTL_RAM_TYPE of \genblk2[1].ram_reg_2_bram_0\ : label is "RAM_TDP";
  attribute ram_addr_begin of \genblk2[1].ram_reg_2_bram_0\ : label is 0;
  attribute ram_addr_end of \genblk2[1].ram_reg_2_bram_0\ : label is 4095;
  attribute ram_offset of \genblk2[1].ram_reg_2_bram_0\ : label is 0;
  attribute ram_slice_begin of \genblk2[1].ram_reg_2_bram_0\ : label is 16;
  attribute ram_slice_end of \genblk2[1].ram_reg_2_bram_0\ : label is 23;
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_2_bram_0_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_2_bram_0_i_2\ : label is "soft_lutpair10";
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of \genblk2[1].ram_reg_2_bram_1\ : label is "p0_d8";
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ of \genblk2[1].ram_reg_2_bram_1\ : label is "p0_d8";
  attribute METHODOLOGY_DRC_VIOS of \genblk2[1].ram_reg_2_bram_1\ : label is "{SYNTH-15 {cell *THIS*} {string {address width (14) is more than optimal threshold of 12. Implementing using BWWE will require more logic and timing would be suboptimal. Please use attribute ram_decomp = power if BWWE is desired.}}} {SYNTH-7 {cell *THIS*}}";
  attribute RDADDR_COLLISION_HWCONFIG of \genblk2[1].ram_reg_2_bram_1\ : label is "DELAYED_WRITE";
  attribute RTL_RAM_BITS of \genblk2[1].ram_reg_2_bram_1\ : label is 524288;
  attribute RTL_RAM_NAME of \genblk2[1].ram_reg_2_bram_1\ : label is "genblk2[1].ram";
  attribute RTL_RAM_TYPE of \genblk2[1].ram_reg_2_bram_1\ : label is "RAM_TDP";
  attribute ram_addr_begin of \genblk2[1].ram_reg_2_bram_1\ : label is 4096;
  attribute ram_addr_end of \genblk2[1].ram_reg_2_bram_1\ : label is 8191;
  attribute ram_offset of \genblk2[1].ram_reg_2_bram_1\ : label is 0;
  attribute ram_slice_begin of \genblk2[1].ram_reg_2_bram_1\ : label is 16;
  attribute ram_slice_end of \genblk2[1].ram_reg_2_bram_1\ : label is 23;
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_2_bram_1_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_2_bram_1_i_2\ : label is "soft_lutpair10";
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of \genblk2[1].ram_reg_2_bram_2\ : label is "p0_d8";
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ of \genblk2[1].ram_reg_2_bram_2\ : label is "p0_d8";
  attribute METHODOLOGY_DRC_VIOS of \genblk2[1].ram_reg_2_bram_2\ : label is "{SYNTH-15 {cell *THIS*} {string {address width (14) is more than optimal threshold of 12. Implementing using BWWE will require more logic and timing would be suboptimal. Please use attribute ram_decomp = power if BWWE is desired.}}} {SYNTH-7 {cell *THIS*}}";
  attribute RDADDR_COLLISION_HWCONFIG of \genblk2[1].ram_reg_2_bram_2\ : label is "DELAYED_WRITE";
  attribute RTL_RAM_BITS of \genblk2[1].ram_reg_2_bram_2\ : label is 524288;
  attribute RTL_RAM_NAME of \genblk2[1].ram_reg_2_bram_2\ : label is "genblk2[1].ram";
  attribute RTL_RAM_TYPE of \genblk2[1].ram_reg_2_bram_2\ : label is "RAM_TDP";
  attribute ram_addr_begin of \genblk2[1].ram_reg_2_bram_2\ : label is 8192;
  attribute ram_addr_end of \genblk2[1].ram_reg_2_bram_2\ : label is 12287;
  attribute ram_offset of \genblk2[1].ram_reg_2_bram_2\ : label is 0;
  attribute ram_slice_begin of \genblk2[1].ram_reg_2_bram_2\ : label is 16;
  attribute ram_slice_end of \genblk2[1].ram_reg_2_bram_2\ : label is 23;
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_2_bram_2_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_2_bram_2_i_2\ : label is "soft_lutpair11";
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of \genblk2[1].ram_reg_2_bram_3\ : label is "p0_d8";
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ of \genblk2[1].ram_reg_2_bram_3\ : label is "p0_d8";
  attribute METHODOLOGY_DRC_VIOS of \genblk2[1].ram_reg_2_bram_3\ : label is "{SYNTH-15 {cell *THIS*} {string {address width (14) is more than optimal threshold of 12. Implementing using BWWE will require more logic and timing would be suboptimal. Please use attribute ram_decomp = power if BWWE is desired.}}} {SYNTH-6 {cell *THIS*}} {SYNTH-7 {cell *THIS*}}";
  attribute RDADDR_COLLISION_HWCONFIG of \genblk2[1].ram_reg_2_bram_3\ : label is "DELAYED_WRITE";
  attribute RTL_RAM_BITS of \genblk2[1].ram_reg_2_bram_3\ : label is 524288;
  attribute RTL_RAM_NAME of \genblk2[1].ram_reg_2_bram_3\ : label is "genblk2[1].ram";
  attribute RTL_RAM_TYPE of \genblk2[1].ram_reg_2_bram_3\ : label is "RAM_TDP";
  attribute ram_addr_begin of \genblk2[1].ram_reg_2_bram_3\ : label is 12288;
  attribute ram_addr_end of \genblk2[1].ram_reg_2_bram_3\ : label is 16383;
  attribute ram_offset of \genblk2[1].ram_reg_2_bram_3\ : label is 0;
  attribute ram_slice_begin of \genblk2[1].ram_reg_2_bram_3\ : label is 16;
  attribute ram_slice_end of \genblk2[1].ram_reg_2_bram_3\ : label is 23;
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_2_bram_3_i_1\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_2_bram_3_i_10\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_2_bram_3_i_11\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_2_bram_3_i_12\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_2_bram_3_i_13\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_2_bram_3_i_14\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_2_bram_3_i_15\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_2_bram_3_i_16\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_2_bram_3_i_17\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_2_bram_3_i_18\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_2_bram_3_i_2\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_2_bram_3_i_3\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_2_bram_3_i_4\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_2_bram_3_i_5\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_2_bram_3_i_6\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_2_bram_3_i_7\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_2_bram_3_i_8\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_2_bram_3_i_9\ : label is "soft_lutpair34";
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of \genblk2[1].ram_reg_3_bram_0\ : label is "p0_d8";
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ of \genblk2[1].ram_reg_3_bram_0\ : label is "p0_d8";
  attribute METHODOLOGY_DRC_VIOS of \genblk2[1].ram_reg_3_bram_0\ : label is "{SYNTH-15 {cell *THIS*} {string {address width (14) is more than optimal threshold of 12. Implementing using BWWE will require more logic and timing would be suboptimal. Please use attribute ram_decomp = power if BWWE is desired.}}} {SYNTH-7 {cell *THIS*}}";
  attribute RDADDR_COLLISION_HWCONFIG of \genblk2[1].ram_reg_3_bram_0\ : label is "DELAYED_WRITE";
  attribute RTL_RAM_BITS of \genblk2[1].ram_reg_3_bram_0\ : label is 524288;
  attribute RTL_RAM_NAME of \genblk2[1].ram_reg_3_bram_0\ : label is "genblk2[1].ram";
  attribute RTL_RAM_TYPE of \genblk2[1].ram_reg_3_bram_0\ : label is "RAM_TDP";
  attribute ram_addr_begin of \genblk2[1].ram_reg_3_bram_0\ : label is 0;
  attribute ram_addr_end of \genblk2[1].ram_reg_3_bram_0\ : label is 4095;
  attribute ram_offset of \genblk2[1].ram_reg_3_bram_0\ : label is 0;
  attribute ram_slice_begin of \genblk2[1].ram_reg_3_bram_0\ : label is 24;
  attribute ram_slice_end of \genblk2[1].ram_reg_3_bram_0\ : label is 31;
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_3_bram_0_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_3_bram_0_i_2\ : label is "soft_lutpair16";
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of \genblk2[1].ram_reg_3_bram_1\ : label is "p0_d8";
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ of \genblk2[1].ram_reg_3_bram_1\ : label is "p0_d8";
  attribute METHODOLOGY_DRC_VIOS of \genblk2[1].ram_reg_3_bram_1\ : label is "{SYNTH-15 {cell *THIS*} {string {address width (14) is more than optimal threshold of 12. Implementing using BWWE will require more logic and timing would be suboptimal. Please use attribute ram_decomp = power if BWWE is desired.}}} {SYNTH-7 {cell *THIS*}}";
  attribute RDADDR_COLLISION_HWCONFIG of \genblk2[1].ram_reg_3_bram_1\ : label is "DELAYED_WRITE";
  attribute RTL_RAM_BITS of \genblk2[1].ram_reg_3_bram_1\ : label is 524288;
  attribute RTL_RAM_NAME of \genblk2[1].ram_reg_3_bram_1\ : label is "genblk2[1].ram";
  attribute RTL_RAM_TYPE of \genblk2[1].ram_reg_3_bram_1\ : label is "RAM_TDP";
  attribute ram_addr_begin of \genblk2[1].ram_reg_3_bram_1\ : label is 4096;
  attribute ram_addr_end of \genblk2[1].ram_reg_3_bram_1\ : label is 8191;
  attribute ram_offset of \genblk2[1].ram_reg_3_bram_1\ : label is 0;
  attribute ram_slice_begin of \genblk2[1].ram_reg_3_bram_1\ : label is 24;
  attribute ram_slice_end of \genblk2[1].ram_reg_3_bram_1\ : label is 31;
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_3_bram_1_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_3_bram_1_i_2\ : label is "soft_lutpair19";
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of \genblk2[1].ram_reg_3_bram_2\ : label is "p0_d8";
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ of \genblk2[1].ram_reg_3_bram_2\ : label is "p0_d8";
  attribute METHODOLOGY_DRC_VIOS of \genblk2[1].ram_reg_3_bram_2\ : label is "{SYNTH-15 {cell *THIS*} {string {address width (14) is more than optimal threshold of 12. Implementing using BWWE will require more logic and timing would be suboptimal. Please use attribute ram_decomp = power if BWWE is desired.}}} {SYNTH-7 {cell *THIS*}}";
  attribute RDADDR_COLLISION_HWCONFIG of \genblk2[1].ram_reg_3_bram_2\ : label is "DELAYED_WRITE";
  attribute RTL_RAM_BITS of \genblk2[1].ram_reg_3_bram_2\ : label is 524288;
  attribute RTL_RAM_NAME of \genblk2[1].ram_reg_3_bram_2\ : label is "genblk2[1].ram";
  attribute RTL_RAM_TYPE of \genblk2[1].ram_reg_3_bram_2\ : label is "RAM_TDP";
  attribute ram_addr_begin of \genblk2[1].ram_reg_3_bram_2\ : label is 8192;
  attribute ram_addr_end of \genblk2[1].ram_reg_3_bram_2\ : label is 12287;
  attribute ram_offset of \genblk2[1].ram_reg_3_bram_2\ : label is 0;
  attribute ram_slice_begin of \genblk2[1].ram_reg_3_bram_2\ : label is 24;
  attribute ram_slice_end of \genblk2[1].ram_reg_3_bram_2\ : label is 31;
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_3_bram_2_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_3_bram_2_i_2\ : label is "soft_lutpair19";
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of \genblk2[1].ram_reg_3_bram_3\ : label is "p0_d8";
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ of \genblk2[1].ram_reg_3_bram_3\ : label is "p0_d8";
  attribute METHODOLOGY_DRC_VIOS of \genblk2[1].ram_reg_3_bram_3\ : label is "{SYNTH-15 {cell *THIS*} {string {address width (14) is more than optimal threshold of 12. Implementing using BWWE will require more logic and timing would be suboptimal. Please use attribute ram_decomp = power if BWWE is desired.}}} {SYNTH-6 {cell *THIS*}} {SYNTH-7 {cell *THIS*}}";
  attribute RDADDR_COLLISION_HWCONFIG of \genblk2[1].ram_reg_3_bram_3\ : label is "DELAYED_WRITE";
  attribute RTL_RAM_BITS of \genblk2[1].ram_reg_3_bram_3\ : label is 524288;
  attribute RTL_RAM_NAME of \genblk2[1].ram_reg_3_bram_3\ : label is "genblk2[1].ram";
  attribute RTL_RAM_TYPE of \genblk2[1].ram_reg_3_bram_3\ : label is "RAM_TDP";
  attribute ram_addr_begin of \genblk2[1].ram_reg_3_bram_3\ : label is 12288;
  attribute ram_addr_end of \genblk2[1].ram_reg_3_bram_3\ : label is 16383;
  attribute ram_offset of \genblk2[1].ram_reg_3_bram_3\ : label is 0;
  attribute ram_slice_begin of \genblk2[1].ram_reg_3_bram_3\ : label is 24;
  attribute ram_slice_end of \genblk2[1].ram_reg_3_bram_3\ : label is 31;
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_3_bram_3_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_3_bram_3_i_10\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_3_bram_3_i_11\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_3_bram_3_i_12\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_3_bram_3_i_13\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_3_bram_3_i_14\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_3_bram_3_i_15\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_3_bram_3_i_16\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_3_bram_3_i_17\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_3_bram_3_i_18\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_3_bram_3_i_19\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_3_bram_3_i_2\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_3_bram_3_i_20\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_3_bram_3_i_21\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_3_bram_3_i_22\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_3_bram_3_i_3\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_3_bram_3_i_4\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_3_bram_3_i_5\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_3_bram_3_i_6\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_3_bram_3_i_7\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_3_bram_3_i_8\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \genblk2[1].ram_reg_3_bram_3_i_9\ : label is "soft_lutpair24";
begin
\genblk2[1].ram_reg_0_bram_0\: unisim.vcomponents.RAMB36E2
    generic map(
      CASCADE_ORDER_A => "FIRST",
      CASCADE_ORDER_B => "FIRST",
      CLOCK_DOMAINS => "COMMON",
      DOA_REG => 0,
      DOB_REG => 0,
      ENADDRENA => "FALSE",
      ENADDRENB => "FALSE",
      EN_ECC_PIPE => "FALSE",
      EN_ECC_READ => "FALSE",
      EN_ECC_WRITE => "FALSE",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      RDADDRCHANGEA => "FALSE",
      RDADDRCHANGEB => "FALSE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 9,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SLEEP_ASYNC => "FALSE",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 9
    )
        port map (
      ADDRARDADDR(14 downto 3) => portA_addr(11 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(14 downto 3) => portB_addr(11 downto 0),
      ADDRBWRADDR(2 downto 0) => B"111",
      ADDRENA => '0',
      ADDRENB => '0',
      CASDIMUXA => '0',
      CASDIMUXB => '0',
      CASDINA(31 downto 0) => B"00000000000000000000000000000000",
      CASDINB(31 downto 0) => B"00000000000000000000000000000000",
      CASDINPA(3 downto 0) => B"0000",
      CASDINPB(3 downto 0) => B"0000",
      CASDOMUXA => '0',
      CASDOMUXB => '0',
      CASDOMUXEN_A => '1',
      CASDOMUXEN_B => '1',
      CASDOUTA(31 downto 8) => \NLW_genblk2[1].ram_reg_0_bram_0_CASDOUTA_UNCONNECTED\(31 downto 8),
      CASDOUTA(7) => \genblk2[1].ram_reg_0_bram_0_n_28\,
      CASDOUTA(6) => \genblk2[1].ram_reg_0_bram_0_n_29\,
      CASDOUTA(5) => \genblk2[1].ram_reg_0_bram_0_n_30\,
      CASDOUTA(4) => \genblk2[1].ram_reg_0_bram_0_n_31\,
      CASDOUTA(3) => \genblk2[1].ram_reg_0_bram_0_n_32\,
      CASDOUTA(2) => \genblk2[1].ram_reg_0_bram_0_n_33\,
      CASDOUTA(1) => \genblk2[1].ram_reg_0_bram_0_n_34\,
      CASDOUTA(0) => \genblk2[1].ram_reg_0_bram_0_n_35\,
      CASDOUTB(31 downto 8) => \NLW_genblk2[1].ram_reg_0_bram_0_CASDOUTB_UNCONNECTED\(31 downto 8),
      CASDOUTB(7) => \genblk2[1].ram_reg_0_bram_0_n_60\,
      CASDOUTB(6) => \genblk2[1].ram_reg_0_bram_0_n_61\,
      CASDOUTB(5) => \genblk2[1].ram_reg_0_bram_0_n_62\,
      CASDOUTB(4) => \genblk2[1].ram_reg_0_bram_0_n_63\,
      CASDOUTB(3) => \genblk2[1].ram_reg_0_bram_0_n_64\,
      CASDOUTB(2) => \genblk2[1].ram_reg_0_bram_0_n_65\,
      CASDOUTB(1) => \genblk2[1].ram_reg_0_bram_0_n_66\,
      CASDOUTB(0) => \genblk2[1].ram_reg_0_bram_0_n_67\,
      CASDOUTPA(3) => \genblk2[1].ram_reg_0_bram_0_n_132\,
      CASDOUTPA(2) => \genblk2[1].ram_reg_0_bram_0_n_133\,
      CASDOUTPA(1) => \genblk2[1].ram_reg_0_bram_0_n_134\,
      CASDOUTPA(0) => \genblk2[1].ram_reg_0_bram_0_n_135\,
      CASDOUTPB(3) => \genblk2[1].ram_reg_0_bram_0_n_136\,
      CASDOUTPB(2) => \genblk2[1].ram_reg_0_bram_0_n_137\,
      CASDOUTPB(1) => \genblk2[1].ram_reg_0_bram_0_n_138\,
      CASDOUTPB(0) => \genblk2[1].ram_reg_0_bram_0_n_139\,
      CASINDBITERR => '0',
      CASINSBITERR => '0',
      CASOREGIMUXA => '0',
      CASOREGIMUXB => '0',
      CASOREGIMUXEN_A => '1',
      CASOREGIMUXEN_B => '1',
      CASOUTDBITERR => \genblk2[1].ram_reg_0_bram_0_n_0\,
      CASOUTSBITERR => \genblk2[1].ram_reg_0_bram_0_n_1\,
      CLKARDCLK => clk,
      CLKBWRCLK => clk,
      DBITERR => \NLW_genblk2[1].ram_reg_0_bram_0_DBITERR_UNCONNECTED\,
      DINADIN(31 downto 8) => B"000000000000000000000000",
      DINADIN(7 downto 0) => portA_data_in(7 downto 0),
      DINBDIN(31 downto 8) => B"000000000000000000000000",
      DINBDIN(7 downto 0) => portB_data_in(7 downto 0),
      DINPADINP(3 downto 0) => B"0000",
      DINPBDINP(3 downto 0) => B"0000",
      DOUTADOUT(31 downto 0) => \NLW_genblk2[1].ram_reg_0_bram_0_DOUTADOUT_UNCONNECTED\(31 downto 0),
      DOUTBDOUT(31 downto 0) => \NLW_genblk2[1].ram_reg_0_bram_0_DOUTBDOUT_UNCONNECTED\(31 downto 0),
      DOUTPADOUTP(3 downto 0) => \NLW_genblk2[1].ram_reg_0_bram_0_DOUTPADOUTP_UNCONNECTED\(3 downto 0),
      DOUTPBDOUTP(3 downto 0) => \NLW_genblk2[1].ram_reg_0_bram_0_DOUTPBDOUTP_UNCONNECTED\(3 downto 0),
      ECCPARITY(7 downto 0) => \NLW_genblk2[1].ram_reg_0_bram_0_ECCPARITY_UNCONNECTED\(7 downto 0),
      ECCPIPECE => '1',
      ENARDEN => \genblk2[1].ram_reg_0_bram_0_i_1_n_0\,
      ENBWREN => \genblk2[1].ram_reg_0_bram_0_i_2_n_0\,
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_genblk2[1].ram_reg_0_bram_0_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '1',
      REGCEB => '1',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_genblk2[1].ram_reg_0_bram_0_SBITERR_UNCONNECTED\,
      SLEEP => '0',
      WEA(3) => \genblk2[1].ram_reg_0_bram_0_i_3_n_0\,
      WEA(2) => \genblk2[1].ram_reg_0_bram_0_i_3_n_0\,
      WEA(1) => \genblk2[1].ram_reg_0_bram_0_i_3_n_0\,
      WEA(0) => \genblk2[1].ram_reg_0_bram_0_i_3_n_0\,
      WEBWE(7 downto 4) => B"0000",
      WEBWE(3) => \genblk2[1].ram_reg_0_bram_0_i_4_n_0\,
      WEBWE(2) => \genblk2[1].ram_reg_0_bram_0_i_4_n_0\,
      WEBWE(1) => \genblk2[1].ram_reg_0_bram_0_i_4_n_0\,
      WEBWE(0) => \genblk2[1].ram_reg_0_bram_0_i_4_n_0\
    );
\genblk2[1].ram_reg_0_bram_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => portA_en,
      I1 => portA_addr(13),
      I2 => portA_addr(12),
      O => \genblk2[1].ram_reg_0_bram_0_i_1_n_0\
    );
\genblk2[1].ram_reg_0_bram_0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => portB_en,
      I1 => portB_addr(13),
      I2 => portB_addr(12),
      O => \genblk2[1].ram_reg_0_bram_0_i_2_n_0\
    );
\genblk2[1].ram_reg_0_bram_0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => portA_be(0),
      I1 => portA_addr(13),
      I2 => portA_addr(12),
      O => \genblk2[1].ram_reg_0_bram_0_i_3_n_0\
    );
\genblk2[1].ram_reg_0_bram_0_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => portB_be(0),
      I1 => portB_addr(13),
      I2 => portB_addr(12),
      O => \genblk2[1].ram_reg_0_bram_0_i_4_n_0\
    );
\genblk2[1].ram_reg_0_bram_1\: unisim.vcomponents.RAMB36E2
    generic map(
      CASCADE_ORDER_A => "MIDDLE",
      CASCADE_ORDER_B => "MIDDLE",
      CLOCK_DOMAINS => "COMMON",
      DOA_REG => 0,
      DOB_REG => 0,
      ENADDRENA => "FALSE",
      ENADDRENB => "FALSE",
      EN_ECC_PIPE => "FALSE",
      EN_ECC_READ => "FALSE",
      EN_ECC_WRITE => "FALSE",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      RDADDRCHANGEA => "FALSE",
      RDADDRCHANGEB => "FALSE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 9,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SLEEP_ASYNC => "FALSE",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 9
    )
        port map (
      ADDRARDADDR(14 downto 3) => portA_addr(11 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(14 downto 3) => portB_addr(11 downto 0),
      ADDRBWRADDR(2 downto 0) => B"111",
      ADDRENA => '0',
      ADDRENB => '0',
      CASDIMUXA => '0',
      CASDIMUXB => '0',
      CASDINA(31 downto 8) => B"000000000000000000000000",
      CASDINA(7) => \genblk2[1].ram_reg_0_bram_0_n_28\,
      CASDINA(6) => \genblk2[1].ram_reg_0_bram_0_n_29\,
      CASDINA(5) => \genblk2[1].ram_reg_0_bram_0_n_30\,
      CASDINA(4) => \genblk2[1].ram_reg_0_bram_0_n_31\,
      CASDINA(3) => \genblk2[1].ram_reg_0_bram_0_n_32\,
      CASDINA(2) => \genblk2[1].ram_reg_0_bram_0_n_33\,
      CASDINA(1) => \genblk2[1].ram_reg_0_bram_0_n_34\,
      CASDINA(0) => \genblk2[1].ram_reg_0_bram_0_n_35\,
      CASDINB(31 downto 8) => B"000000000000000000000000",
      CASDINB(7) => \genblk2[1].ram_reg_0_bram_0_n_60\,
      CASDINB(6) => \genblk2[1].ram_reg_0_bram_0_n_61\,
      CASDINB(5) => \genblk2[1].ram_reg_0_bram_0_n_62\,
      CASDINB(4) => \genblk2[1].ram_reg_0_bram_0_n_63\,
      CASDINB(3) => \genblk2[1].ram_reg_0_bram_0_n_64\,
      CASDINB(2) => \genblk2[1].ram_reg_0_bram_0_n_65\,
      CASDINB(1) => \genblk2[1].ram_reg_0_bram_0_n_66\,
      CASDINB(0) => \genblk2[1].ram_reg_0_bram_0_n_67\,
      CASDINPA(3) => \genblk2[1].ram_reg_0_bram_0_n_132\,
      CASDINPA(2) => \genblk2[1].ram_reg_0_bram_0_n_133\,
      CASDINPA(1) => \genblk2[1].ram_reg_0_bram_0_n_134\,
      CASDINPA(0) => \genblk2[1].ram_reg_0_bram_0_n_135\,
      CASDINPB(3) => \genblk2[1].ram_reg_0_bram_0_n_136\,
      CASDINPB(2) => \genblk2[1].ram_reg_0_bram_0_n_137\,
      CASDINPB(1) => \genblk2[1].ram_reg_0_bram_0_n_138\,
      CASDINPB(0) => \genblk2[1].ram_reg_0_bram_0_n_139\,
      CASDOMUXA => \genblk2[1].ram_reg_0_bram_1_i_1_n_0\,
      CASDOMUXB => \genblk2[1].ram_reg_0_bram_1_i_2_n_0\,
      CASDOMUXEN_A => portA_en,
      CASDOMUXEN_B => portB_en,
      CASDOUTA(31 downto 8) => \NLW_genblk2[1].ram_reg_0_bram_1_CASDOUTA_UNCONNECTED\(31 downto 8),
      CASDOUTA(7) => \genblk2[1].ram_reg_0_bram_1_n_28\,
      CASDOUTA(6) => \genblk2[1].ram_reg_0_bram_1_n_29\,
      CASDOUTA(5) => \genblk2[1].ram_reg_0_bram_1_n_30\,
      CASDOUTA(4) => \genblk2[1].ram_reg_0_bram_1_n_31\,
      CASDOUTA(3) => \genblk2[1].ram_reg_0_bram_1_n_32\,
      CASDOUTA(2) => \genblk2[1].ram_reg_0_bram_1_n_33\,
      CASDOUTA(1) => \genblk2[1].ram_reg_0_bram_1_n_34\,
      CASDOUTA(0) => \genblk2[1].ram_reg_0_bram_1_n_35\,
      CASDOUTB(31 downto 8) => \NLW_genblk2[1].ram_reg_0_bram_1_CASDOUTB_UNCONNECTED\(31 downto 8),
      CASDOUTB(7) => \genblk2[1].ram_reg_0_bram_1_n_60\,
      CASDOUTB(6) => \genblk2[1].ram_reg_0_bram_1_n_61\,
      CASDOUTB(5) => \genblk2[1].ram_reg_0_bram_1_n_62\,
      CASDOUTB(4) => \genblk2[1].ram_reg_0_bram_1_n_63\,
      CASDOUTB(3) => \genblk2[1].ram_reg_0_bram_1_n_64\,
      CASDOUTB(2) => \genblk2[1].ram_reg_0_bram_1_n_65\,
      CASDOUTB(1) => \genblk2[1].ram_reg_0_bram_1_n_66\,
      CASDOUTB(0) => \genblk2[1].ram_reg_0_bram_1_n_67\,
      CASDOUTPA(3) => \genblk2[1].ram_reg_0_bram_1_n_132\,
      CASDOUTPA(2) => \genblk2[1].ram_reg_0_bram_1_n_133\,
      CASDOUTPA(1) => \genblk2[1].ram_reg_0_bram_1_n_134\,
      CASDOUTPA(0) => \genblk2[1].ram_reg_0_bram_1_n_135\,
      CASDOUTPB(3) => \genblk2[1].ram_reg_0_bram_1_n_136\,
      CASDOUTPB(2) => \genblk2[1].ram_reg_0_bram_1_n_137\,
      CASDOUTPB(1) => \genblk2[1].ram_reg_0_bram_1_n_138\,
      CASDOUTPB(0) => \genblk2[1].ram_reg_0_bram_1_n_139\,
      CASINDBITERR => \genblk2[1].ram_reg_0_bram_0_n_0\,
      CASINSBITERR => \genblk2[1].ram_reg_0_bram_0_n_1\,
      CASOREGIMUXA => '0',
      CASOREGIMUXB => '0',
      CASOREGIMUXEN_A => '1',
      CASOREGIMUXEN_B => '1',
      CASOUTDBITERR => \genblk2[1].ram_reg_0_bram_1_n_0\,
      CASOUTSBITERR => \genblk2[1].ram_reg_0_bram_1_n_1\,
      CLKARDCLK => clk,
      CLKBWRCLK => clk,
      DBITERR => \NLW_genblk2[1].ram_reg_0_bram_1_DBITERR_UNCONNECTED\,
      DINADIN(31 downto 8) => B"000000000000000000000000",
      DINADIN(7 downto 0) => portA_data_in(7 downto 0),
      DINBDIN(31 downto 8) => B"000000000000000000000000",
      DINBDIN(7 downto 0) => portB_data_in(7 downto 0),
      DINPADINP(3 downto 0) => B"0000",
      DINPBDINP(3 downto 0) => B"0000",
      DOUTADOUT(31 downto 0) => \NLW_genblk2[1].ram_reg_0_bram_1_DOUTADOUT_UNCONNECTED\(31 downto 0),
      DOUTBDOUT(31 downto 0) => \NLW_genblk2[1].ram_reg_0_bram_1_DOUTBDOUT_UNCONNECTED\(31 downto 0),
      DOUTPADOUTP(3 downto 0) => \NLW_genblk2[1].ram_reg_0_bram_1_DOUTPADOUTP_UNCONNECTED\(3 downto 0),
      DOUTPBDOUTP(3 downto 0) => \NLW_genblk2[1].ram_reg_0_bram_1_DOUTPBDOUTP_UNCONNECTED\(3 downto 0),
      ECCPARITY(7 downto 0) => \NLW_genblk2[1].ram_reg_0_bram_1_ECCPARITY_UNCONNECTED\(7 downto 0),
      ECCPIPECE => '1',
      ENARDEN => \genblk2[1].ram_reg_0_bram_1_i_3_n_0\,
      ENBWREN => \genblk2[1].ram_reg_0_bram_1_i_4_n_0\,
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_genblk2[1].ram_reg_0_bram_1_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '1',
      REGCEB => '1',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_genblk2[1].ram_reg_0_bram_1_SBITERR_UNCONNECTED\,
      SLEEP => '0',
      WEA(3) => \genblk2[1].ram_reg_0_bram_1_i_5_n_0\,
      WEA(2) => \genblk2[1].ram_reg_0_bram_1_i_5_n_0\,
      WEA(1) => \genblk2[1].ram_reg_0_bram_1_i_5_n_0\,
      WEA(0) => \genblk2[1].ram_reg_0_bram_1_i_5_n_0\,
      WEBWE(7 downto 4) => B"0000",
      WEBWE(3) => \genblk2[1].ram_reg_0_bram_1_i_6_n_0\,
      WEBWE(2) => \genblk2[1].ram_reg_0_bram_1_i_6_n_0\,
      WEBWE(1) => \genblk2[1].ram_reg_0_bram_1_i_6_n_0\,
      WEBWE(0) => \genblk2[1].ram_reg_0_bram_1_i_6_n_0\
    );
\genblk2[1].ram_reg_0_bram_1_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => portA_addr(13),
      I1 => portA_addr(12),
      O => \genblk2[1].ram_reg_0_bram_1_i_1_n_0\
    );
\genblk2[1].ram_reg_0_bram_1_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => portB_addr(13),
      I1 => portB_addr(12),
      O => \genblk2[1].ram_reg_0_bram_1_i_2_n_0\
    );
\genblk2[1].ram_reg_0_bram_1_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => portA_en,
      I1 => portA_addr(13),
      I2 => portA_addr(12),
      O => \genblk2[1].ram_reg_0_bram_1_i_3_n_0\
    );
\genblk2[1].ram_reg_0_bram_1_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => portB_en,
      I1 => portB_addr(13),
      I2 => portB_addr(12),
      O => \genblk2[1].ram_reg_0_bram_1_i_4_n_0\
    );
\genblk2[1].ram_reg_0_bram_1_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => portA_be(0),
      I1 => portA_addr(13),
      I2 => portA_addr(12),
      O => \genblk2[1].ram_reg_0_bram_1_i_5_n_0\
    );
\genblk2[1].ram_reg_0_bram_1_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => portB_be(0),
      I1 => portB_addr(13),
      I2 => portB_addr(12),
      O => \genblk2[1].ram_reg_0_bram_1_i_6_n_0\
    );
\genblk2[1].ram_reg_0_bram_2\: unisim.vcomponents.RAMB36E2
    generic map(
      CASCADE_ORDER_A => "MIDDLE",
      CASCADE_ORDER_B => "MIDDLE",
      CLOCK_DOMAINS => "COMMON",
      DOA_REG => 0,
      DOB_REG => 0,
      ENADDRENA => "FALSE",
      ENADDRENB => "FALSE",
      EN_ECC_PIPE => "FALSE",
      EN_ECC_READ => "FALSE",
      EN_ECC_WRITE => "FALSE",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      RDADDRCHANGEA => "FALSE",
      RDADDRCHANGEB => "FALSE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 9,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SLEEP_ASYNC => "FALSE",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 9
    )
        port map (
      ADDRARDADDR(14 downto 3) => portA_addr(11 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(14 downto 3) => portB_addr(11 downto 0),
      ADDRBWRADDR(2 downto 0) => B"111",
      ADDRENA => '0',
      ADDRENB => '0',
      CASDIMUXA => '0',
      CASDIMUXB => '0',
      CASDINA(31 downto 8) => B"000000000000000000000000",
      CASDINA(7) => \genblk2[1].ram_reg_0_bram_1_n_28\,
      CASDINA(6) => \genblk2[1].ram_reg_0_bram_1_n_29\,
      CASDINA(5) => \genblk2[1].ram_reg_0_bram_1_n_30\,
      CASDINA(4) => \genblk2[1].ram_reg_0_bram_1_n_31\,
      CASDINA(3) => \genblk2[1].ram_reg_0_bram_1_n_32\,
      CASDINA(2) => \genblk2[1].ram_reg_0_bram_1_n_33\,
      CASDINA(1) => \genblk2[1].ram_reg_0_bram_1_n_34\,
      CASDINA(0) => \genblk2[1].ram_reg_0_bram_1_n_35\,
      CASDINB(31 downto 8) => B"000000000000000000000000",
      CASDINB(7) => \genblk2[1].ram_reg_0_bram_1_n_60\,
      CASDINB(6) => \genblk2[1].ram_reg_0_bram_1_n_61\,
      CASDINB(5) => \genblk2[1].ram_reg_0_bram_1_n_62\,
      CASDINB(4) => \genblk2[1].ram_reg_0_bram_1_n_63\,
      CASDINB(3) => \genblk2[1].ram_reg_0_bram_1_n_64\,
      CASDINB(2) => \genblk2[1].ram_reg_0_bram_1_n_65\,
      CASDINB(1) => \genblk2[1].ram_reg_0_bram_1_n_66\,
      CASDINB(0) => \genblk2[1].ram_reg_0_bram_1_n_67\,
      CASDINPA(3) => \genblk2[1].ram_reg_0_bram_1_n_132\,
      CASDINPA(2) => \genblk2[1].ram_reg_0_bram_1_n_133\,
      CASDINPA(1) => \genblk2[1].ram_reg_0_bram_1_n_134\,
      CASDINPA(0) => \genblk2[1].ram_reg_0_bram_1_n_135\,
      CASDINPB(3) => \genblk2[1].ram_reg_0_bram_1_n_136\,
      CASDINPB(2) => \genblk2[1].ram_reg_0_bram_1_n_137\,
      CASDINPB(1) => \genblk2[1].ram_reg_0_bram_1_n_138\,
      CASDINPB(0) => \genblk2[1].ram_reg_0_bram_1_n_139\,
      CASDOMUXA => \genblk2[1].ram_reg_0_bram_2_i_1_n_0\,
      CASDOMUXB => \genblk2[1].ram_reg_0_bram_2_i_2_n_0\,
      CASDOMUXEN_A => portA_en,
      CASDOMUXEN_B => portB_en,
      CASDOUTA(31 downto 8) => \NLW_genblk2[1].ram_reg_0_bram_2_CASDOUTA_UNCONNECTED\(31 downto 8),
      CASDOUTA(7) => \genblk2[1].ram_reg_0_bram_2_n_28\,
      CASDOUTA(6) => \genblk2[1].ram_reg_0_bram_2_n_29\,
      CASDOUTA(5) => \genblk2[1].ram_reg_0_bram_2_n_30\,
      CASDOUTA(4) => \genblk2[1].ram_reg_0_bram_2_n_31\,
      CASDOUTA(3) => \genblk2[1].ram_reg_0_bram_2_n_32\,
      CASDOUTA(2) => \genblk2[1].ram_reg_0_bram_2_n_33\,
      CASDOUTA(1) => \genblk2[1].ram_reg_0_bram_2_n_34\,
      CASDOUTA(0) => \genblk2[1].ram_reg_0_bram_2_n_35\,
      CASDOUTB(31 downto 8) => \NLW_genblk2[1].ram_reg_0_bram_2_CASDOUTB_UNCONNECTED\(31 downto 8),
      CASDOUTB(7) => \genblk2[1].ram_reg_0_bram_2_n_60\,
      CASDOUTB(6) => \genblk2[1].ram_reg_0_bram_2_n_61\,
      CASDOUTB(5) => \genblk2[1].ram_reg_0_bram_2_n_62\,
      CASDOUTB(4) => \genblk2[1].ram_reg_0_bram_2_n_63\,
      CASDOUTB(3) => \genblk2[1].ram_reg_0_bram_2_n_64\,
      CASDOUTB(2) => \genblk2[1].ram_reg_0_bram_2_n_65\,
      CASDOUTB(1) => \genblk2[1].ram_reg_0_bram_2_n_66\,
      CASDOUTB(0) => \genblk2[1].ram_reg_0_bram_2_n_67\,
      CASDOUTPA(3) => \genblk2[1].ram_reg_0_bram_2_n_132\,
      CASDOUTPA(2) => \genblk2[1].ram_reg_0_bram_2_n_133\,
      CASDOUTPA(1) => \genblk2[1].ram_reg_0_bram_2_n_134\,
      CASDOUTPA(0) => \genblk2[1].ram_reg_0_bram_2_n_135\,
      CASDOUTPB(3) => \genblk2[1].ram_reg_0_bram_2_n_136\,
      CASDOUTPB(2) => \genblk2[1].ram_reg_0_bram_2_n_137\,
      CASDOUTPB(1) => \genblk2[1].ram_reg_0_bram_2_n_138\,
      CASDOUTPB(0) => \genblk2[1].ram_reg_0_bram_2_n_139\,
      CASINDBITERR => \genblk2[1].ram_reg_0_bram_1_n_0\,
      CASINSBITERR => \genblk2[1].ram_reg_0_bram_1_n_1\,
      CASOREGIMUXA => '0',
      CASOREGIMUXB => '0',
      CASOREGIMUXEN_A => '1',
      CASOREGIMUXEN_B => '1',
      CASOUTDBITERR => \genblk2[1].ram_reg_0_bram_2_n_0\,
      CASOUTSBITERR => \genblk2[1].ram_reg_0_bram_2_n_1\,
      CLKARDCLK => clk,
      CLKBWRCLK => clk,
      DBITERR => \NLW_genblk2[1].ram_reg_0_bram_2_DBITERR_UNCONNECTED\,
      DINADIN(31 downto 8) => B"000000000000000000000000",
      DINADIN(7 downto 0) => portA_data_in(7 downto 0),
      DINBDIN(31 downto 8) => B"000000000000000000000000",
      DINBDIN(7 downto 0) => portB_data_in(7 downto 0),
      DINPADINP(3 downto 0) => B"0000",
      DINPBDINP(3 downto 0) => B"0000",
      DOUTADOUT(31 downto 0) => \NLW_genblk2[1].ram_reg_0_bram_2_DOUTADOUT_UNCONNECTED\(31 downto 0),
      DOUTBDOUT(31 downto 0) => \NLW_genblk2[1].ram_reg_0_bram_2_DOUTBDOUT_UNCONNECTED\(31 downto 0),
      DOUTPADOUTP(3 downto 0) => \NLW_genblk2[1].ram_reg_0_bram_2_DOUTPADOUTP_UNCONNECTED\(3 downto 0),
      DOUTPBDOUTP(3 downto 0) => \NLW_genblk2[1].ram_reg_0_bram_2_DOUTPBDOUTP_UNCONNECTED\(3 downto 0),
      ECCPARITY(7 downto 0) => \NLW_genblk2[1].ram_reg_0_bram_2_ECCPARITY_UNCONNECTED\(7 downto 0),
      ECCPIPECE => '1',
      ENARDEN => \genblk2[1].ram_reg_0_bram_2_i_3_n_0\,
      ENBWREN => \genblk2[1].ram_reg_0_bram_2_i_4_n_0\,
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_genblk2[1].ram_reg_0_bram_2_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '1',
      REGCEB => '1',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_genblk2[1].ram_reg_0_bram_2_SBITERR_UNCONNECTED\,
      SLEEP => '0',
      WEA(3) => \genblk2[1].ram_reg_0_bram_2_i_5_n_0\,
      WEA(2) => \genblk2[1].ram_reg_0_bram_2_i_5_n_0\,
      WEA(1) => \genblk2[1].ram_reg_0_bram_2_i_5_n_0\,
      WEA(0) => \genblk2[1].ram_reg_0_bram_2_i_5_n_0\,
      WEBWE(7 downto 4) => B"0000",
      WEBWE(3) => \genblk2[1].ram_reg_0_bram_2_i_6_n_0\,
      WEBWE(2) => \genblk2[1].ram_reg_0_bram_2_i_6_n_0\,
      WEBWE(1) => \genblk2[1].ram_reg_0_bram_2_i_6_n_0\,
      WEBWE(0) => \genblk2[1].ram_reg_0_bram_2_i_6_n_0\
    );
\genblk2[1].ram_reg_0_bram_2_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => portA_addr(12),
      I1 => portA_addr(13),
      O => \genblk2[1].ram_reg_0_bram_2_i_1_n_0\
    );
\genblk2[1].ram_reg_0_bram_2_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => portB_addr(12),
      I1 => portB_addr(13),
      O => \genblk2[1].ram_reg_0_bram_2_i_2_n_0\
    );
\genblk2[1].ram_reg_0_bram_2_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => portA_en,
      I1 => portA_addr(12),
      I2 => portA_addr(13),
      O => \genblk2[1].ram_reg_0_bram_2_i_3_n_0\
    );
\genblk2[1].ram_reg_0_bram_2_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => portB_en,
      I1 => portB_addr(12),
      I2 => portB_addr(13),
      O => \genblk2[1].ram_reg_0_bram_2_i_4_n_0\
    );
\genblk2[1].ram_reg_0_bram_2_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => portA_be(0),
      I1 => portA_addr(12),
      I2 => portA_addr(13),
      O => \genblk2[1].ram_reg_0_bram_2_i_5_n_0\
    );
\genblk2[1].ram_reg_0_bram_2_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => portB_be(0),
      I1 => portB_addr(12),
      I2 => portB_addr(13),
      O => \genblk2[1].ram_reg_0_bram_2_i_6_n_0\
    );
\genblk2[1].ram_reg_0_bram_3\: unisim.vcomponents.RAMB36E2
    generic map(
      CASCADE_ORDER_A => "LAST",
      CASCADE_ORDER_B => "LAST",
      CLOCK_DOMAINS => "COMMON",
      DOA_REG => 0,
      DOB_REG => 0,
      ENADDRENA => "FALSE",
      ENADDRENB => "FALSE",
      EN_ECC_PIPE => "FALSE",
      EN_ECC_READ => "FALSE",
      EN_ECC_WRITE => "FALSE",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      RDADDRCHANGEA => "FALSE",
      RDADDRCHANGEB => "FALSE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 9,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SLEEP_ASYNC => "FALSE",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 9
    )
        port map (
      ADDRARDADDR(14 downto 3) => portA_addr(11 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(14 downto 3) => portB_addr(11 downto 0),
      ADDRBWRADDR(2 downto 0) => B"111",
      ADDRENA => '0',
      ADDRENB => '0',
      CASDIMUXA => '0',
      CASDIMUXB => '0',
      CASDINA(31 downto 8) => B"000000000000000000000000",
      CASDINA(7) => \genblk2[1].ram_reg_0_bram_2_n_28\,
      CASDINA(6) => \genblk2[1].ram_reg_0_bram_2_n_29\,
      CASDINA(5) => \genblk2[1].ram_reg_0_bram_2_n_30\,
      CASDINA(4) => \genblk2[1].ram_reg_0_bram_2_n_31\,
      CASDINA(3) => \genblk2[1].ram_reg_0_bram_2_n_32\,
      CASDINA(2) => \genblk2[1].ram_reg_0_bram_2_n_33\,
      CASDINA(1) => \genblk2[1].ram_reg_0_bram_2_n_34\,
      CASDINA(0) => \genblk2[1].ram_reg_0_bram_2_n_35\,
      CASDINB(31 downto 8) => B"000000000000000000000000",
      CASDINB(7) => \genblk2[1].ram_reg_0_bram_2_n_60\,
      CASDINB(6) => \genblk2[1].ram_reg_0_bram_2_n_61\,
      CASDINB(5) => \genblk2[1].ram_reg_0_bram_2_n_62\,
      CASDINB(4) => \genblk2[1].ram_reg_0_bram_2_n_63\,
      CASDINB(3) => \genblk2[1].ram_reg_0_bram_2_n_64\,
      CASDINB(2) => \genblk2[1].ram_reg_0_bram_2_n_65\,
      CASDINB(1) => \genblk2[1].ram_reg_0_bram_2_n_66\,
      CASDINB(0) => \genblk2[1].ram_reg_0_bram_2_n_67\,
      CASDINPA(3) => \genblk2[1].ram_reg_0_bram_2_n_132\,
      CASDINPA(2) => \genblk2[1].ram_reg_0_bram_2_n_133\,
      CASDINPA(1) => \genblk2[1].ram_reg_0_bram_2_n_134\,
      CASDINPA(0) => \genblk2[1].ram_reg_0_bram_2_n_135\,
      CASDINPB(3) => \genblk2[1].ram_reg_0_bram_2_n_136\,
      CASDINPB(2) => \genblk2[1].ram_reg_0_bram_2_n_137\,
      CASDINPB(1) => \genblk2[1].ram_reg_0_bram_2_n_138\,
      CASDINPB(0) => \genblk2[1].ram_reg_0_bram_2_n_139\,
      CASDOMUXA => \genblk2[1].ram_reg_3_bram_3_i_1_n_0\,
      CASDOMUXB => \genblk2[1].ram_reg_3_bram_3_i_2_n_0\,
      CASDOMUXEN_A => portA_en,
      CASDOMUXEN_B => portB_en,
      CASDOUTA(31 downto 0) => \NLW_genblk2[1].ram_reg_0_bram_3_CASDOUTA_UNCONNECTED\(31 downto 0),
      CASDOUTB(31 downto 0) => \NLW_genblk2[1].ram_reg_0_bram_3_CASDOUTB_UNCONNECTED\(31 downto 0),
      CASDOUTPA(3 downto 0) => \NLW_genblk2[1].ram_reg_0_bram_3_CASDOUTPA_UNCONNECTED\(3 downto 0),
      CASDOUTPB(3 downto 0) => \NLW_genblk2[1].ram_reg_0_bram_3_CASDOUTPB_UNCONNECTED\(3 downto 0),
      CASINDBITERR => \genblk2[1].ram_reg_0_bram_2_n_0\,
      CASINSBITERR => \genblk2[1].ram_reg_0_bram_2_n_1\,
      CASOREGIMUXA => '0',
      CASOREGIMUXB => '0',
      CASOREGIMUXEN_A => '1',
      CASOREGIMUXEN_B => '1',
      CASOUTDBITERR => \NLW_genblk2[1].ram_reg_0_bram_3_CASOUTDBITERR_UNCONNECTED\,
      CASOUTSBITERR => \NLW_genblk2[1].ram_reg_0_bram_3_CASOUTSBITERR_UNCONNECTED\,
      CLKARDCLK => clk,
      CLKBWRCLK => clk,
      DBITERR => \NLW_genblk2[1].ram_reg_0_bram_3_DBITERR_UNCONNECTED\,
      DINADIN(31 downto 8) => B"000000000000000000000000",
      DINADIN(7 downto 0) => portA_data_in(7 downto 0),
      DINBDIN(31 downto 8) => B"000000000000000000000000",
      DINBDIN(7 downto 0) => portB_data_in(7 downto 0),
      DINPADINP(3 downto 0) => B"0000",
      DINPBDINP(3 downto 0) => B"0000",
      DOUTADOUT(31 downto 8) => \NLW_genblk2[1].ram_reg_0_bram_3_DOUTADOUT_UNCONNECTED\(31 downto 8),
      DOUTADOUT(7 downto 0) => portA_data_out(7 downto 0),
      DOUTBDOUT(31 downto 8) => \NLW_genblk2[1].ram_reg_0_bram_3_DOUTBDOUT_UNCONNECTED\(31 downto 8),
      DOUTBDOUT(7 downto 0) => portB_data_out(7 downto 0),
      DOUTPADOUTP(3 downto 0) => \NLW_genblk2[1].ram_reg_0_bram_3_DOUTPADOUTP_UNCONNECTED\(3 downto 0),
      DOUTPBDOUTP(3 downto 0) => \NLW_genblk2[1].ram_reg_0_bram_3_DOUTPBDOUTP_UNCONNECTED\(3 downto 0),
      ECCPARITY(7 downto 0) => \NLW_genblk2[1].ram_reg_0_bram_3_ECCPARITY_UNCONNECTED\(7 downto 0),
      ECCPIPECE => '1',
      ENARDEN => \genblk2[1].ram_reg_3_bram_3_i_3_n_0\,
      ENBWREN => \genblk2[1].ram_reg_3_bram_3_i_4_n_0\,
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_genblk2[1].ram_reg_0_bram_3_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '1',
      REGCEB => '1',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_genblk2[1].ram_reg_0_bram_3_SBITERR_UNCONNECTED\,
      SLEEP => '0',
      WEA(3) => \genblk2[1].ram_reg_0_bram_3_i_1_n_0\,
      WEA(2) => \genblk2[1].ram_reg_0_bram_3_i_1_n_0\,
      WEA(1) => \genblk2[1].ram_reg_0_bram_3_i_1_n_0\,
      WEA(0) => \genblk2[1].ram_reg_0_bram_3_i_1_n_0\,
      WEBWE(7 downto 4) => B"0000",
      WEBWE(3) => \genblk2[1].ram_reg_0_bram_3_i_2_n_0\,
      WEBWE(2) => \genblk2[1].ram_reg_0_bram_3_i_2_n_0\,
      WEBWE(1) => \genblk2[1].ram_reg_0_bram_3_i_2_n_0\,
      WEBWE(0) => \genblk2[1].ram_reg_0_bram_3_i_2_n_0\
    );
\genblk2[1].ram_reg_0_bram_3_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => portA_be(0),
      I1 => portA_addr(13),
      I2 => portA_addr(12),
      O => \genblk2[1].ram_reg_0_bram_3_i_1_n_0\
    );
\genblk2[1].ram_reg_0_bram_3_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => portB_be(0),
      I1 => portB_addr(13),
      I2 => portB_addr(12),
      O => \genblk2[1].ram_reg_0_bram_3_i_2_n_0\
    );
\genblk2[1].ram_reg_1_bram_0\: unisim.vcomponents.RAMB36E2
    generic map(
      CASCADE_ORDER_A => "FIRST",
      CASCADE_ORDER_B => "FIRST",
      CLOCK_DOMAINS => "COMMON",
      DOA_REG => 0,
      DOB_REG => 0,
      ENADDRENA => "FALSE",
      ENADDRENB => "FALSE",
      EN_ECC_PIPE => "FALSE",
      EN_ECC_READ => "FALSE",
      EN_ECC_WRITE => "FALSE",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      RDADDRCHANGEA => "FALSE",
      RDADDRCHANGEB => "FALSE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 9,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SLEEP_ASYNC => "FALSE",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 9
    )
        port map (
      ADDRARDADDR(14 downto 3) => portA_addr(11 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(14 downto 3) => portB_addr(11 downto 0),
      ADDRBWRADDR(2 downto 0) => B"111",
      ADDRENA => '0',
      ADDRENB => '0',
      CASDIMUXA => '0',
      CASDIMUXB => '0',
      CASDINA(31 downto 0) => B"00000000000000000000000000000000",
      CASDINB(31 downto 0) => B"00000000000000000000000000000000",
      CASDINPA(3 downto 0) => B"0000",
      CASDINPB(3 downto 0) => B"0000",
      CASDOMUXA => '0',
      CASDOMUXB => '0',
      CASDOMUXEN_A => '1',
      CASDOMUXEN_B => '1',
      CASDOUTA(31 downto 8) => \NLW_genblk2[1].ram_reg_1_bram_0_CASDOUTA_UNCONNECTED\(31 downto 8),
      CASDOUTA(7) => \genblk2[1].ram_reg_1_bram_0_n_28\,
      CASDOUTA(6) => \genblk2[1].ram_reg_1_bram_0_n_29\,
      CASDOUTA(5) => \genblk2[1].ram_reg_1_bram_0_n_30\,
      CASDOUTA(4) => \genblk2[1].ram_reg_1_bram_0_n_31\,
      CASDOUTA(3) => \genblk2[1].ram_reg_1_bram_0_n_32\,
      CASDOUTA(2) => \genblk2[1].ram_reg_1_bram_0_n_33\,
      CASDOUTA(1) => \genblk2[1].ram_reg_1_bram_0_n_34\,
      CASDOUTA(0) => \genblk2[1].ram_reg_1_bram_0_n_35\,
      CASDOUTB(31 downto 8) => \NLW_genblk2[1].ram_reg_1_bram_0_CASDOUTB_UNCONNECTED\(31 downto 8),
      CASDOUTB(7) => \genblk2[1].ram_reg_1_bram_0_n_60\,
      CASDOUTB(6) => \genblk2[1].ram_reg_1_bram_0_n_61\,
      CASDOUTB(5) => \genblk2[1].ram_reg_1_bram_0_n_62\,
      CASDOUTB(4) => \genblk2[1].ram_reg_1_bram_0_n_63\,
      CASDOUTB(3) => \genblk2[1].ram_reg_1_bram_0_n_64\,
      CASDOUTB(2) => \genblk2[1].ram_reg_1_bram_0_n_65\,
      CASDOUTB(1) => \genblk2[1].ram_reg_1_bram_0_n_66\,
      CASDOUTB(0) => \genblk2[1].ram_reg_1_bram_0_n_67\,
      CASDOUTPA(3) => \genblk2[1].ram_reg_1_bram_0_n_132\,
      CASDOUTPA(2) => \genblk2[1].ram_reg_1_bram_0_n_133\,
      CASDOUTPA(1) => \genblk2[1].ram_reg_1_bram_0_n_134\,
      CASDOUTPA(0) => \genblk2[1].ram_reg_1_bram_0_n_135\,
      CASDOUTPB(3) => \genblk2[1].ram_reg_1_bram_0_n_136\,
      CASDOUTPB(2) => \genblk2[1].ram_reg_1_bram_0_n_137\,
      CASDOUTPB(1) => \genblk2[1].ram_reg_1_bram_0_n_138\,
      CASDOUTPB(0) => \genblk2[1].ram_reg_1_bram_0_n_139\,
      CASINDBITERR => '0',
      CASINSBITERR => '0',
      CASOREGIMUXA => '0',
      CASOREGIMUXB => '0',
      CASOREGIMUXEN_A => '1',
      CASOREGIMUXEN_B => '1',
      CASOUTDBITERR => \genblk2[1].ram_reg_1_bram_0_n_0\,
      CASOUTSBITERR => \genblk2[1].ram_reg_1_bram_0_n_1\,
      CLKARDCLK => clk,
      CLKBWRCLK => clk,
      DBITERR => \NLW_genblk2[1].ram_reg_1_bram_0_DBITERR_UNCONNECTED\,
      DINADIN(31 downto 8) => B"000000000000000000000000",
      DINADIN(7 downto 0) => portA_data_in(15 downto 8),
      DINBDIN(31 downto 8) => B"000000000000000000000000",
      DINBDIN(7 downto 0) => portB_data_in(15 downto 8),
      DINPADINP(3 downto 0) => B"0000",
      DINPBDINP(3 downto 0) => B"0000",
      DOUTADOUT(31 downto 0) => \NLW_genblk2[1].ram_reg_1_bram_0_DOUTADOUT_UNCONNECTED\(31 downto 0),
      DOUTBDOUT(31 downto 0) => \NLW_genblk2[1].ram_reg_1_bram_0_DOUTBDOUT_UNCONNECTED\(31 downto 0),
      DOUTPADOUTP(3 downto 0) => \NLW_genblk2[1].ram_reg_1_bram_0_DOUTPADOUTP_UNCONNECTED\(3 downto 0),
      DOUTPBDOUTP(3 downto 0) => \NLW_genblk2[1].ram_reg_1_bram_0_DOUTPBDOUTP_UNCONNECTED\(3 downto 0),
      ECCPARITY(7 downto 0) => \NLW_genblk2[1].ram_reg_1_bram_0_ECCPARITY_UNCONNECTED\(7 downto 0),
      ECCPIPECE => '1',
      ENARDEN => \genblk2[1].ram_reg_0_bram_0_i_1_n_0\,
      ENBWREN => \genblk2[1].ram_reg_0_bram_0_i_2_n_0\,
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_genblk2[1].ram_reg_1_bram_0_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '1',
      REGCEB => '1',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_genblk2[1].ram_reg_1_bram_0_SBITERR_UNCONNECTED\,
      SLEEP => '0',
      WEA(3) => \genblk2[1].ram_reg_1_bram_0_i_1_n_0\,
      WEA(2) => \genblk2[1].ram_reg_1_bram_0_i_1_n_0\,
      WEA(1) => \genblk2[1].ram_reg_1_bram_0_i_1_n_0\,
      WEA(0) => \genblk2[1].ram_reg_1_bram_0_i_1_n_0\,
      WEBWE(7 downto 4) => B"0000",
      WEBWE(3) => \genblk2[1].ram_reg_1_bram_0_i_2_n_0\,
      WEBWE(2) => \genblk2[1].ram_reg_1_bram_0_i_2_n_0\,
      WEBWE(1) => \genblk2[1].ram_reg_1_bram_0_i_2_n_0\,
      WEBWE(0) => \genblk2[1].ram_reg_1_bram_0_i_2_n_0\
    );
\genblk2[1].ram_reg_1_bram_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => portA_be(1),
      I1 => portA_addr(13),
      I2 => portA_addr(12),
      O => \genblk2[1].ram_reg_1_bram_0_i_1_n_0\
    );
\genblk2[1].ram_reg_1_bram_0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => portB_be(1),
      I1 => portB_addr(13),
      I2 => portB_addr(12),
      O => \genblk2[1].ram_reg_1_bram_0_i_2_n_0\
    );
\genblk2[1].ram_reg_1_bram_1\: unisim.vcomponents.RAMB36E2
    generic map(
      CASCADE_ORDER_A => "MIDDLE",
      CASCADE_ORDER_B => "MIDDLE",
      CLOCK_DOMAINS => "COMMON",
      DOA_REG => 0,
      DOB_REG => 0,
      ENADDRENA => "FALSE",
      ENADDRENB => "FALSE",
      EN_ECC_PIPE => "FALSE",
      EN_ECC_READ => "FALSE",
      EN_ECC_WRITE => "FALSE",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      RDADDRCHANGEA => "FALSE",
      RDADDRCHANGEB => "FALSE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 9,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SLEEP_ASYNC => "FALSE",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 9
    )
        port map (
      ADDRARDADDR(14 downto 3) => portA_addr(11 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(14 downto 3) => portB_addr(11 downto 0),
      ADDRBWRADDR(2 downto 0) => B"111",
      ADDRENA => '0',
      ADDRENB => '0',
      CASDIMUXA => '0',
      CASDIMUXB => '0',
      CASDINA(31 downto 8) => B"000000000000000000000000",
      CASDINA(7) => \genblk2[1].ram_reg_1_bram_0_n_28\,
      CASDINA(6) => \genblk2[1].ram_reg_1_bram_0_n_29\,
      CASDINA(5) => \genblk2[1].ram_reg_1_bram_0_n_30\,
      CASDINA(4) => \genblk2[1].ram_reg_1_bram_0_n_31\,
      CASDINA(3) => \genblk2[1].ram_reg_1_bram_0_n_32\,
      CASDINA(2) => \genblk2[1].ram_reg_1_bram_0_n_33\,
      CASDINA(1) => \genblk2[1].ram_reg_1_bram_0_n_34\,
      CASDINA(0) => \genblk2[1].ram_reg_1_bram_0_n_35\,
      CASDINB(31 downto 8) => B"000000000000000000000000",
      CASDINB(7) => \genblk2[1].ram_reg_1_bram_0_n_60\,
      CASDINB(6) => \genblk2[1].ram_reg_1_bram_0_n_61\,
      CASDINB(5) => \genblk2[1].ram_reg_1_bram_0_n_62\,
      CASDINB(4) => \genblk2[1].ram_reg_1_bram_0_n_63\,
      CASDINB(3) => \genblk2[1].ram_reg_1_bram_0_n_64\,
      CASDINB(2) => \genblk2[1].ram_reg_1_bram_0_n_65\,
      CASDINB(1) => \genblk2[1].ram_reg_1_bram_0_n_66\,
      CASDINB(0) => \genblk2[1].ram_reg_1_bram_0_n_67\,
      CASDINPA(3) => \genblk2[1].ram_reg_1_bram_0_n_132\,
      CASDINPA(2) => \genblk2[1].ram_reg_1_bram_0_n_133\,
      CASDINPA(1) => \genblk2[1].ram_reg_1_bram_0_n_134\,
      CASDINPA(0) => \genblk2[1].ram_reg_1_bram_0_n_135\,
      CASDINPB(3) => \genblk2[1].ram_reg_1_bram_0_n_136\,
      CASDINPB(2) => \genblk2[1].ram_reg_1_bram_0_n_137\,
      CASDINPB(1) => \genblk2[1].ram_reg_1_bram_0_n_138\,
      CASDINPB(0) => \genblk2[1].ram_reg_1_bram_0_n_139\,
      CASDOMUXA => \genblk2[1].ram_reg_0_bram_1_i_1_n_0\,
      CASDOMUXB => \genblk2[1].ram_reg_0_bram_1_i_2_n_0\,
      CASDOMUXEN_A => portA_en,
      CASDOMUXEN_B => portB_en,
      CASDOUTA(31 downto 8) => \NLW_genblk2[1].ram_reg_1_bram_1_CASDOUTA_UNCONNECTED\(31 downto 8),
      CASDOUTA(7) => \genblk2[1].ram_reg_1_bram_1_n_28\,
      CASDOUTA(6) => \genblk2[1].ram_reg_1_bram_1_n_29\,
      CASDOUTA(5) => \genblk2[1].ram_reg_1_bram_1_n_30\,
      CASDOUTA(4) => \genblk2[1].ram_reg_1_bram_1_n_31\,
      CASDOUTA(3) => \genblk2[1].ram_reg_1_bram_1_n_32\,
      CASDOUTA(2) => \genblk2[1].ram_reg_1_bram_1_n_33\,
      CASDOUTA(1) => \genblk2[1].ram_reg_1_bram_1_n_34\,
      CASDOUTA(0) => \genblk2[1].ram_reg_1_bram_1_n_35\,
      CASDOUTB(31 downto 8) => \NLW_genblk2[1].ram_reg_1_bram_1_CASDOUTB_UNCONNECTED\(31 downto 8),
      CASDOUTB(7) => \genblk2[1].ram_reg_1_bram_1_n_60\,
      CASDOUTB(6) => \genblk2[1].ram_reg_1_bram_1_n_61\,
      CASDOUTB(5) => \genblk2[1].ram_reg_1_bram_1_n_62\,
      CASDOUTB(4) => \genblk2[1].ram_reg_1_bram_1_n_63\,
      CASDOUTB(3) => \genblk2[1].ram_reg_1_bram_1_n_64\,
      CASDOUTB(2) => \genblk2[1].ram_reg_1_bram_1_n_65\,
      CASDOUTB(1) => \genblk2[1].ram_reg_1_bram_1_n_66\,
      CASDOUTB(0) => \genblk2[1].ram_reg_1_bram_1_n_67\,
      CASDOUTPA(3) => \genblk2[1].ram_reg_1_bram_1_n_132\,
      CASDOUTPA(2) => \genblk2[1].ram_reg_1_bram_1_n_133\,
      CASDOUTPA(1) => \genblk2[1].ram_reg_1_bram_1_n_134\,
      CASDOUTPA(0) => \genblk2[1].ram_reg_1_bram_1_n_135\,
      CASDOUTPB(3) => \genblk2[1].ram_reg_1_bram_1_n_136\,
      CASDOUTPB(2) => \genblk2[1].ram_reg_1_bram_1_n_137\,
      CASDOUTPB(1) => \genblk2[1].ram_reg_1_bram_1_n_138\,
      CASDOUTPB(0) => \genblk2[1].ram_reg_1_bram_1_n_139\,
      CASINDBITERR => \genblk2[1].ram_reg_1_bram_0_n_0\,
      CASINSBITERR => \genblk2[1].ram_reg_1_bram_0_n_1\,
      CASOREGIMUXA => '0',
      CASOREGIMUXB => '0',
      CASOREGIMUXEN_A => '1',
      CASOREGIMUXEN_B => '1',
      CASOUTDBITERR => \genblk2[1].ram_reg_1_bram_1_n_0\,
      CASOUTSBITERR => \genblk2[1].ram_reg_1_bram_1_n_1\,
      CLKARDCLK => clk,
      CLKBWRCLK => clk,
      DBITERR => \NLW_genblk2[1].ram_reg_1_bram_1_DBITERR_UNCONNECTED\,
      DINADIN(31 downto 8) => B"000000000000000000000000",
      DINADIN(7 downto 0) => portA_data_in(15 downto 8),
      DINBDIN(31 downto 8) => B"000000000000000000000000",
      DINBDIN(7 downto 0) => portB_data_in(15 downto 8),
      DINPADINP(3 downto 0) => B"0000",
      DINPBDINP(3 downto 0) => B"0000",
      DOUTADOUT(31 downto 0) => \NLW_genblk2[1].ram_reg_1_bram_1_DOUTADOUT_UNCONNECTED\(31 downto 0),
      DOUTBDOUT(31 downto 0) => \NLW_genblk2[1].ram_reg_1_bram_1_DOUTBDOUT_UNCONNECTED\(31 downto 0),
      DOUTPADOUTP(3 downto 0) => \NLW_genblk2[1].ram_reg_1_bram_1_DOUTPADOUTP_UNCONNECTED\(3 downto 0),
      DOUTPBDOUTP(3 downto 0) => \NLW_genblk2[1].ram_reg_1_bram_1_DOUTPBDOUTP_UNCONNECTED\(3 downto 0),
      ECCPARITY(7 downto 0) => \NLW_genblk2[1].ram_reg_1_bram_1_ECCPARITY_UNCONNECTED\(7 downto 0),
      ECCPIPECE => '1',
      ENARDEN => \genblk2[1].ram_reg_0_bram_1_i_3_n_0\,
      ENBWREN => \genblk2[1].ram_reg_0_bram_1_i_4_n_0\,
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_genblk2[1].ram_reg_1_bram_1_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '1',
      REGCEB => '1',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_genblk2[1].ram_reg_1_bram_1_SBITERR_UNCONNECTED\,
      SLEEP => '0',
      WEA(3) => \genblk2[1].ram_reg_1_bram_1_i_1_n_0\,
      WEA(2) => \genblk2[1].ram_reg_1_bram_1_i_1_n_0\,
      WEA(1) => \genblk2[1].ram_reg_1_bram_1_i_1_n_0\,
      WEA(0) => \genblk2[1].ram_reg_1_bram_1_i_1_n_0\,
      WEBWE(7 downto 4) => B"0000",
      WEBWE(3) => \genblk2[1].ram_reg_1_bram_1_i_2_n_0\,
      WEBWE(2) => \genblk2[1].ram_reg_1_bram_1_i_2_n_0\,
      WEBWE(1) => \genblk2[1].ram_reg_1_bram_1_i_2_n_0\,
      WEBWE(0) => \genblk2[1].ram_reg_1_bram_1_i_2_n_0\
    );
\genblk2[1].ram_reg_1_bram_1_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => portA_be(1),
      I1 => portA_addr(13),
      I2 => portA_addr(12),
      O => \genblk2[1].ram_reg_1_bram_1_i_1_n_0\
    );
\genblk2[1].ram_reg_1_bram_1_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => portB_be(1),
      I1 => portB_addr(13),
      I2 => portB_addr(12),
      O => \genblk2[1].ram_reg_1_bram_1_i_2_n_0\
    );
\genblk2[1].ram_reg_1_bram_2\: unisim.vcomponents.RAMB36E2
    generic map(
      CASCADE_ORDER_A => "MIDDLE",
      CASCADE_ORDER_B => "MIDDLE",
      CLOCK_DOMAINS => "COMMON",
      DOA_REG => 0,
      DOB_REG => 0,
      ENADDRENA => "FALSE",
      ENADDRENB => "FALSE",
      EN_ECC_PIPE => "FALSE",
      EN_ECC_READ => "FALSE",
      EN_ECC_WRITE => "FALSE",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      RDADDRCHANGEA => "FALSE",
      RDADDRCHANGEB => "FALSE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 9,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SLEEP_ASYNC => "FALSE",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 9
    )
        port map (
      ADDRARDADDR(14 downto 3) => portA_addr(11 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(14 downto 3) => portB_addr(11 downto 0),
      ADDRBWRADDR(2 downto 0) => B"111",
      ADDRENA => '0',
      ADDRENB => '0',
      CASDIMUXA => '0',
      CASDIMUXB => '0',
      CASDINA(31 downto 8) => B"000000000000000000000000",
      CASDINA(7) => \genblk2[1].ram_reg_1_bram_1_n_28\,
      CASDINA(6) => \genblk2[1].ram_reg_1_bram_1_n_29\,
      CASDINA(5) => \genblk2[1].ram_reg_1_bram_1_n_30\,
      CASDINA(4) => \genblk2[1].ram_reg_1_bram_1_n_31\,
      CASDINA(3) => \genblk2[1].ram_reg_1_bram_1_n_32\,
      CASDINA(2) => \genblk2[1].ram_reg_1_bram_1_n_33\,
      CASDINA(1) => \genblk2[1].ram_reg_1_bram_1_n_34\,
      CASDINA(0) => \genblk2[1].ram_reg_1_bram_1_n_35\,
      CASDINB(31 downto 8) => B"000000000000000000000000",
      CASDINB(7) => \genblk2[1].ram_reg_1_bram_1_n_60\,
      CASDINB(6) => \genblk2[1].ram_reg_1_bram_1_n_61\,
      CASDINB(5) => \genblk2[1].ram_reg_1_bram_1_n_62\,
      CASDINB(4) => \genblk2[1].ram_reg_1_bram_1_n_63\,
      CASDINB(3) => \genblk2[1].ram_reg_1_bram_1_n_64\,
      CASDINB(2) => \genblk2[1].ram_reg_1_bram_1_n_65\,
      CASDINB(1) => \genblk2[1].ram_reg_1_bram_1_n_66\,
      CASDINB(0) => \genblk2[1].ram_reg_1_bram_1_n_67\,
      CASDINPA(3) => \genblk2[1].ram_reg_1_bram_1_n_132\,
      CASDINPA(2) => \genblk2[1].ram_reg_1_bram_1_n_133\,
      CASDINPA(1) => \genblk2[1].ram_reg_1_bram_1_n_134\,
      CASDINPA(0) => \genblk2[1].ram_reg_1_bram_1_n_135\,
      CASDINPB(3) => \genblk2[1].ram_reg_1_bram_1_n_136\,
      CASDINPB(2) => \genblk2[1].ram_reg_1_bram_1_n_137\,
      CASDINPB(1) => \genblk2[1].ram_reg_1_bram_1_n_138\,
      CASDINPB(0) => \genblk2[1].ram_reg_1_bram_1_n_139\,
      CASDOMUXA => \genblk2[1].ram_reg_0_bram_2_i_1_n_0\,
      CASDOMUXB => \genblk2[1].ram_reg_0_bram_2_i_2_n_0\,
      CASDOMUXEN_A => portA_en,
      CASDOMUXEN_B => portB_en,
      CASDOUTA(31 downto 8) => \NLW_genblk2[1].ram_reg_1_bram_2_CASDOUTA_UNCONNECTED\(31 downto 8),
      CASDOUTA(7) => \genblk2[1].ram_reg_1_bram_2_n_28\,
      CASDOUTA(6) => \genblk2[1].ram_reg_1_bram_2_n_29\,
      CASDOUTA(5) => \genblk2[1].ram_reg_1_bram_2_n_30\,
      CASDOUTA(4) => \genblk2[1].ram_reg_1_bram_2_n_31\,
      CASDOUTA(3) => \genblk2[1].ram_reg_1_bram_2_n_32\,
      CASDOUTA(2) => \genblk2[1].ram_reg_1_bram_2_n_33\,
      CASDOUTA(1) => \genblk2[1].ram_reg_1_bram_2_n_34\,
      CASDOUTA(0) => \genblk2[1].ram_reg_1_bram_2_n_35\,
      CASDOUTB(31 downto 8) => \NLW_genblk2[1].ram_reg_1_bram_2_CASDOUTB_UNCONNECTED\(31 downto 8),
      CASDOUTB(7) => \genblk2[1].ram_reg_1_bram_2_n_60\,
      CASDOUTB(6) => \genblk2[1].ram_reg_1_bram_2_n_61\,
      CASDOUTB(5) => \genblk2[1].ram_reg_1_bram_2_n_62\,
      CASDOUTB(4) => \genblk2[1].ram_reg_1_bram_2_n_63\,
      CASDOUTB(3) => \genblk2[1].ram_reg_1_bram_2_n_64\,
      CASDOUTB(2) => \genblk2[1].ram_reg_1_bram_2_n_65\,
      CASDOUTB(1) => \genblk2[1].ram_reg_1_bram_2_n_66\,
      CASDOUTB(0) => \genblk2[1].ram_reg_1_bram_2_n_67\,
      CASDOUTPA(3) => \genblk2[1].ram_reg_1_bram_2_n_132\,
      CASDOUTPA(2) => \genblk2[1].ram_reg_1_bram_2_n_133\,
      CASDOUTPA(1) => \genblk2[1].ram_reg_1_bram_2_n_134\,
      CASDOUTPA(0) => \genblk2[1].ram_reg_1_bram_2_n_135\,
      CASDOUTPB(3) => \genblk2[1].ram_reg_1_bram_2_n_136\,
      CASDOUTPB(2) => \genblk2[1].ram_reg_1_bram_2_n_137\,
      CASDOUTPB(1) => \genblk2[1].ram_reg_1_bram_2_n_138\,
      CASDOUTPB(0) => \genblk2[1].ram_reg_1_bram_2_n_139\,
      CASINDBITERR => \genblk2[1].ram_reg_1_bram_1_n_0\,
      CASINSBITERR => \genblk2[1].ram_reg_1_bram_1_n_1\,
      CASOREGIMUXA => '0',
      CASOREGIMUXB => '0',
      CASOREGIMUXEN_A => '1',
      CASOREGIMUXEN_B => '1',
      CASOUTDBITERR => \genblk2[1].ram_reg_1_bram_2_n_0\,
      CASOUTSBITERR => \genblk2[1].ram_reg_1_bram_2_n_1\,
      CLKARDCLK => clk,
      CLKBWRCLK => clk,
      DBITERR => \NLW_genblk2[1].ram_reg_1_bram_2_DBITERR_UNCONNECTED\,
      DINADIN(31 downto 8) => B"000000000000000000000000",
      DINADIN(7 downto 0) => portA_data_in(15 downto 8),
      DINBDIN(31 downto 8) => B"000000000000000000000000",
      DINBDIN(7 downto 0) => portB_data_in(15 downto 8),
      DINPADINP(3 downto 0) => B"0000",
      DINPBDINP(3 downto 0) => B"0000",
      DOUTADOUT(31 downto 0) => \NLW_genblk2[1].ram_reg_1_bram_2_DOUTADOUT_UNCONNECTED\(31 downto 0),
      DOUTBDOUT(31 downto 0) => \NLW_genblk2[1].ram_reg_1_bram_2_DOUTBDOUT_UNCONNECTED\(31 downto 0),
      DOUTPADOUTP(3 downto 0) => \NLW_genblk2[1].ram_reg_1_bram_2_DOUTPADOUTP_UNCONNECTED\(3 downto 0),
      DOUTPBDOUTP(3 downto 0) => \NLW_genblk2[1].ram_reg_1_bram_2_DOUTPBDOUTP_UNCONNECTED\(3 downto 0),
      ECCPARITY(7 downto 0) => \NLW_genblk2[1].ram_reg_1_bram_2_ECCPARITY_UNCONNECTED\(7 downto 0),
      ECCPIPECE => '1',
      ENARDEN => \genblk2[1].ram_reg_0_bram_2_i_3_n_0\,
      ENBWREN => \genblk2[1].ram_reg_0_bram_2_i_4_n_0\,
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_genblk2[1].ram_reg_1_bram_2_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '1',
      REGCEB => '1',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_genblk2[1].ram_reg_1_bram_2_SBITERR_UNCONNECTED\,
      SLEEP => '0',
      WEA(3) => \genblk2[1].ram_reg_1_bram_2_i_1_n_0\,
      WEA(2) => \genblk2[1].ram_reg_1_bram_2_i_1_n_0\,
      WEA(1) => \genblk2[1].ram_reg_1_bram_2_i_1_n_0\,
      WEA(0) => \genblk2[1].ram_reg_1_bram_2_i_1_n_0\,
      WEBWE(7 downto 4) => B"0000",
      WEBWE(3) => \genblk2[1].ram_reg_1_bram_2_i_2_n_0\,
      WEBWE(2) => \genblk2[1].ram_reg_1_bram_2_i_2_n_0\,
      WEBWE(1) => \genblk2[1].ram_reg_1_bram_2_i_2_n_0\,
      WEBWE(0) => \genblk2[1].ram_reg_1_bram_2_i_2_n_0\
    );
\genblk2[1].ram_reg_1_bram_2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => portA_be(1),
      I1 => portA_addr(12),
      I2 => portA_addr(13),
      O => \genblk2[1].ram_reg_1_bram_2_i_1_n_0\
    );
\genblk2[1].ram_reg_1_bram_2_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => portB_be(1),
      I1 => portB_addr(12),
      I2 => portB_addr(13),
      O => \genblk2[1].ram_reg_1_bram_2_i_2_n_0\
    );
\genblk2[1].ram_reg_1_bram_3\: unisim.vcomponents.RAMB36E2
    generic map(
      CASCADE_ORDER_A => "LAST",
      CASCADE_ORDER_B => "LAST",
      CLOCK_DOMAINS => "COMMON",
      DOA_REG => 0,
      DOB_REG => 0,
      ENADDRENA => "FALSE",
      ENADDRENB => "FALSE",
      EN_ECC_PIPE => "FALSE",
      EN_ECC_READ => "FALSE",
      EN_ECC_WRITE => "FALSE",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      RDADDRCHANGEA => "FALSE",
      RDADDRCHANGEB => "FALSE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 9,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SLEEP_ASYNC => "FALSE",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 9
    )
        port map (
      ADDRARDADDR(14 downto 3) => portA_addr(11 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(14 downto 3) => portB_addr(11 downto 0),
      ADDRBWRADDR(2 downto 0) => B"111",
      ADDRENA => '0',
      ADDRENB => '0',
      CASDIMUXA => '0',
      CASDIMUXB => '0',
      CASDINA(31 downto 8) => B"000000000000000000000000",
      CASDINA(7) => \genblk2[1].ram_reg_1_bram_2_n_28\,
      CASDINA(6) => \genblk2[1].ram_reg_1_bram_2_n_29\,
      CASDINA(5) => \genblk2[1].ram_reg_1_bram_2_n_30\,
      CASDINA(4) => \genblk2[1].ram_reg_1_bram_2_n_31\,
      CASDINA(3) => \genblk2[1].ram_reg_1_bram_2_n_32\,
      CASDINA(2) => \genblk2[1].ram_reg_1_bram_2_n_33\,
      CASDINA(1) => \genblk2[1].ram_reg_1_bram_2_n_34\,
      CASDINA(0) => \genblk2[1].ram_reg_1_bram_2_n_35\,
      CASDINB(31 downto 8) => B"000000000000000000000000",
      CASDINB(7) => \genblk2[1].ram_reg_1_bram_2_n_60\,
      CASDINB(6) => \genblk2[1].ram_reg_1_bram_2_n_61\,
      CASDINB(5) => \genblk2[1].ram_reg_1_bram_2_n_62\,
      CASDINB(4) => \genblk2[1].ram_reg_1_bram_2_n_63\,
      CASDINB(3) => \genblk2[1].ram_reg_1_bram_2_n_64\,
      CASDINB(2) => \genblk2[1].ram_reg_1_bram_2_n_65\,
      CASDINB(1) => \genblk2[1].ram_reg_1_bram_2_n_66\,
      CASDINB(0) => \genblk2[1].ram_reg_1_bram_2_n_67\,
      CASDINPA(3) => \genblk2[1].ram_reg_1_bram_2_n_132\,
      CASDINPA(2) => \genblk2[1].ram_reg_1_bram_2_n_133\,
      CASDINPA(1) => \genblk2[1].ram_reg_1_bram_2_n_134\,
      CASDINPA(0) => \genblk2[1].ram_reg_1_bram_2_n_135\,
      CASDINPB(3) => \genblk2[1].ram_reg_1_bram_2_n_136\,
      CASDINPB(2) => \genblk2[1].ram_reg_1_bram_2_n_137\,
      CASDINPB(1) => \genblk2[1].ram_reg_1_bram_2_n_138\,
      CASDINPB(0) => \genblk2[1].ram_reg_1_bram_2_n_139\,
      CASDOMUXA => \genblk2[1].ram_reg_3_bram_3_i_1_n_0\,
      CASDOMUXB => \genblk2[1].ram_reg_3_bram_3_i_2_n_0\,
      CASDOMUXEN_A => portA_en,
      CASDOMUXEN_B => portB_en,
      CASDOUTA(31 downto 0) => \NLW_genblk2[1].ram_reg_1_bram_3_CASDOUTA_UNCONNECTED\(31 downto 0),
      CASDOUTB(31 downto 0) => \NLW_genblk2[1].ram_reg_1_bram_3_CASDOUTB_UNCONNECTED\(31 downto 0),
      CASDOUTPA(3 downto 0) => \NLW_genblk2[1].ram_reg_1_bram_3_CASDOUTPA_UNCONNECTED\(3 downto 0),
      CASDOUTPB(3 downto 0) => \NLW_genblk2[1].ram_reg_1_bram_3_CASDOUTPB_UNCONNECTED\(3 downto 0),
      CASINDBITERR => \genblk2[1].ram_reg_1_bram_2_n_0\,
      CASINSBITERR => \genblk2[1].ram_reg_1_bram_2_n_1\,
      CASOREGIMUXA => '0',
      CASOREGIMUXB => '0',
      CASOREGIMUXEN_A => '1',
      CASOREGIMUXEN_B => '1',
      CASOUTDBITERR => \NLW_genblk2[1].ram_reg_1_bram_3_CASOUTDBITERR_UNCONNECTED\,
      CASOUTSBITERR => \NLW_genblk2[1].ram_reg_1_bram_3_CASOUTSBITERR_UNCONNECTED\,
      CLKARDCLK => clk,
      CLKBWRCLK => clk,
      DBITERR => \NLW_genblk2[1].ram_reg_1_bram_3_DBITERR_UNCONNECTED\,
      DINADIN(31 downto 8) => B"000000000000000000000000",
      DINADIN(7 downto 0) => portA_data_in(15 downto 8),
      DINBDIN(31 downto 8) => B"000000000000000000000000",
      DINBDIN(7 downto 0) => portB_data_in(15 downto 8),
      DINPADINP(3 downto 0) => B"0000",
      DINPBDINP(3 downto 0) => B"0000",
      DOUTADOUT(31 downto 8) => \NLW_genblk2[1].ram_reg_1_bram_3_DOUTADOUT_UNCONNECTED\(31 downto 8),
      DOUTADOUT(7 downto 0) => portA_data_out(15 downto 8),
      DOUTBDOUT(31 downto 8) => \NLW_genblk2[1].ram_reg_1_bram_3_DOUTBDOUT_UNCONNECTED\(31 downto 8),
      DOUTBDOUT(7 downto 0) => portB_data_out(15 downto 8),
      DOUTPADOUTP(3 downto 0) => \NLW_genblk2[1].ram_reg_1_bram_3_DOUTPADOUTP_UNCONNECTED\(3 downto 0),
      DOUTPBDOUTP(3 downto 0) => \NLW_genblk2[1].ram_reg_1_bram_3_DOUTPBDOUTP_UNCONNECTED\(3 downto 0),
      ECCPARITY(7 downto 0) => \NLW_genblk2[1].ram_reg_1_bram_3_ECCPARITY_UNCONNECTED\(7 downto 0),
      ECCPIPECE => '1',
      ENARDEN => \genblk2[1].ram_reg_3_bram_3_i_3_n_0\,
      ENBWREN => \genblk2[1].ram_reg_3_bram_3_i_4_n_0\,
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_genblk2[1].ram_reg_1_bram_3_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '1',
      REGCEB => '1',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_genblk2[1].ram_reg_1_bram_3_SBITERR_UNCONNECTED\,
      SLEEP => '0',
      WEA(3) => \genblk2[1].ram_reg_1_bram_3_i_1_n_0\,
      WEA(2) => \genblk2[1].ram_reg_1_bram_3_i_1_n_0\,
      WEA(1) => \genblk2[1].ram_reg_1_bram_3_i_1_n_0\,
      WEA(0) => \genblk2[1].ram_reg_1_bram_3_i_1_n_0\,
      WEBWE(7 downto 4) => B"0000",
      WEBWE(3) => \genblk2[1].ram_reg_1_bram_3_i_2_n_0\,
      WEBWE(2) => \genblk2[1].ram_reg_1_bram_3_i_2_n_0\,
      WEBWE(1) => \genblk2[1].ram_reg_1_bram_3_i_2_n_0\,
      WEBWE(0) => \genblk2[1].ram_reg_1_bram_3_i_2_n_0\
    );
\genblk2[1].ram_reg_1_bram_3_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => portA_be(1),
      I1 => portA_addr(13),
      I2 => portA_addr(12),
      O => \genblk2[1].ram_reg_1_bram_3_i_1_n_0\
    );
\genblk2[1].ram_reg_1_bram_3_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => portB_be(1),
      I1 => portB_addr(13),
      I2 => portB_addr(12),
      O => \genblk2[1].ram_reg_1_bram_3_i_2_n_0\
    );
\genblk2[1].ram_reg_2_bram_0\: unisim.vcomponents.RAMB36E2
    generic map(
      CASCADE_ORDER_A => "FIRST",
      CASCADE_ORDER_B => "FIRST",
      CLOCK_DOMAINS => "COMMON",
      DOA_REG => 0,
      DOB_REG => 0,
      ENADDRENA => "FALSE",
      ENADDRENB => "FALSE",
      EN_ECC_PIPE => "FALSE",
      EN_ECC_READ => "FALSE",
      EN_ECC_WRITE => "FALSE",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      RDADDRCHANGEA => "FALSE",
      RDADDRCHANGEB => "FALSE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 9,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SLEEP_ASYNC => "FALSE",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 9
    )
        port map (
      ADDRARDADDR(14 downto 3) => portA_addr(11 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(14 downto 3) => portB_addr(11 downto 0),
      ADDRBWRADDR(2 downto 0) => B"111",
      ADDRENA => '0',
      ADDRENB => '0',
      CASDIMUXA => '0',
      CASDIMUXB => '0',
      CASDINA(31 downto 0) => B"00000000000000000000000000000000",
      CASDINB(31 downto 0) => B"00000000000000000000000000000000",
      CASDINPA(3 downto 0) => B"0000",
      CASDINPB(3 downto 0) => B"0000",
      CASDOMUXA => '0',
      CASDOMUXB => '0',
      CASDOMUXEN_A => '1',
      CASDOMUXEN_B => '1',
      CASDOUTA(31 downto 8) => \NLW_genblk2[1].ram_reg_2_bram_0_CASDOUTA_UNCONNECTED\(31 downto 8),
      CASDOUTA(7) => \genblk2[1].ram_reg_2_bram_0_n_28\,
      CASDOUTA(6) => \genblk2[1].ram_reg_2_bram_0_n_29\,
      CASDOUTA(5) => \genblk2[1].ram_reg_2_bram_0_n_30\,
      CASDOUTA(4) => \genblk2[1].ram_reg_2_bram_0_n_31\,
      CASDOUTA(3) => \genblk2[1].ram_reg_2_bram_0_n_32\,
      CASDOUTA(2) => \genblk2[1].ram_reg_2_bram_0_n_33\,
      CASDOUTA(1) => \genblk2[1].ram_reg_2_bram_0_n_34\,
      CASDOUTA(0) => \genblk2[1].ram_reg_2_bram_0_n_35\,
      CASDOUTB(31 downto 8) => \NLW_genblk2[1].ram_reg_2_bram_0_CASDOUTB_UNCONNECTED\(31 downto 8),
      CASDOUTB(7) => \genblk2[1].ram_reg_2_bram_0_n_60\,
      CASDOUTB(6) => \genblk2[1].ram_reg_2_bram_0_n_61\,
      CASDOUTB(5) => \genblk2[1].ram_reg_2_bram_0_n_62\,
      CASDOUTB(4) => \genblk2[1].ram_reg_2_bram_0_n_63\,
      CASDOUTB(3) => \genblk2[1].ram_reg_2_bram_0_n_64\,
      CASDOUTB(2) => \genblk2[1].ram_reg_2_bram_0_n_65\,
      CASDOUTB(1) => \genblk2[1].ram_reg_2_bram_0_n_66\,
      CASDOUTB(0) => \genblk2[1].ram_reg_2_bram_0_n_67\,
      CASDOUTPA(3) => \genblk2[1].ram_reg_2_bram_0_n_132\,
      CASDOUTPA(2) => \genblk2[1].ram_reg_2_bram_0_n_133\,
      CASDOUTPA(1) => \genblk2[1].ram_reg_2_bram_0_n_134\,
      CASDOUTPA(0) => \genblk2[1].ram_reg_2_bram_0_n_135\,
      CASDOUTPB(3) => \genblk2[1].ram_reg_2_bram_0_n_136\,
      CASDOUTPB(2) => \genblk2[1].ram_reg_2_bram_0_n_137\,
      CASDOUTPB(1) => \genblk2[1].ram_reg_2_bram_0_n_138\,
      CASDOUTPB(0) => \genblk2[1].ram_reg_2_bram_0_n_139\,
      CASINDBITERR => '0',
      CASINSBITERR => '0',
      CASOREGIMUXA => '0',
      CASOREGIMUXB => '0',
      CASOREGIMUXEN_A => '1',
      CASOREGIMUXEN_B => '1',
      CASOUTDBITERR => \genblk2[1].ram_reg_2_bram_0_n_0\,
      CASOUTSBITERR => \genblk2[1].ram_reg_2_bram_0_n_1\,
      CLKARDCLK => clk,
      CLKBWRCLK => clk,
      DBITERR => \NLW_genblk2[1].ram_reg_2_bram_0_DBITERR_UNCONNECTED\,
      DINADIN(31 downto 8) => B"000000000000000000000000",
      DINADIN(7 downto 0) => p_1_in(23 downto 16),
      DINBDIN(31 downto 8) => B"000000000000000000000000",
      DINBDIN(7 downto 0) => p_2_in(23 downto 16),
      DINPADINP(3 downto 0) => B"0000",
      DINPBDINP(3 downto 0) => B"0000",
      DOUTADOUT(31 downto 0) => \NLW_genblk2[1].ram_reg_2_bram_0_DOUTADOUT_UNCONNECTED\(31 downto 0),
      DOUTBDOUT(31 downto 0) => \NLW_genblk2[1].ram_reg_2_bram_0_DOUTBDOUT_UNCONNECTED\(31 downto 0),
      DOUTPADOUTP(3 downto 0) => \NLW_genblk2[1].ram_reg_2_bram_0_DOUTPADOUTP_UNCONNECTED\(3 downto 0),
      DOUTPBDOUTP(3 downto 0) => \NLW_genblk2[1].ram_reg_2_bram_0_DOUTPBDOUTP_UNCONNECTED\(3 downto 0),
      ECCPARITY(7 downto 0) => \NLW_genblk2[1].ram_reg_2_bram_0_ECCPARITY_UNCONNECTED\(7 downto 0),
      ECCPIPECE => '1',
      ENARDEN => \genblk2[1].ram_reg_0_bram_0_i_1_n_0\,
      ENBWREN => \genblk2[1].ram_reg_0_bram_0_i_2_n_0\,
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_genblk2[1].ram_reg_2_bram_0_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '1',
      REGCEB => '1',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_genblk2[1].ram_reg_2_bram_0_SBITERR_UNCONNECTED\,
      SLEEP => '0',
      WEA(3) => \genblk2[1].ram_reg_2_bram_0_i_1_n_0\,
      WEA(2) => \genblk2[1].ram_reg_2_bram_0_i_1_n_0\,
      WEA(1) => \genblk2[1].ram_reg_2_bram_0_i_1_n_0\,
      WEA(0) => \genblk2[1].ram_reg_2_bram_0_i_1_n_0\,
      WEBWE(7 downto 4) => B"0000",
      WEBWE(3) => \genblk2[1].ram_reg_2_bram_0_i_2_n_0\,
      WEBWE(2) => \genblk2[1].ram_reg_2_bram_0_i_2_n_0\,
      WEBWE(1) => \genblk2[1].ram_reg_2_bram_0_i_2_n_0\,
      WEBWE(0) => \genblk2[1].ram_reg_2_bram_0_i_2_n_0\
    );
\genblk2[1].ram_reg_2_bram_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => portA_be(2),
      I1 => portA_addr(13),
      I2 => portA_addr(12),
      O => \genblk2[1].ram_reg_2_bram_0_i_1_n_0\
    );
\genblk2[1].ram_reg_2_bram_0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => portB_be(2),
      I1 => portB_addr(13),
      I2 => portB_addr(12),
      O => \genblk2[1].ram_reg_2_bram_0_i_2_n_0\
    );
\genblk2[1].ram_reg_2_bram_1\: unisim.vcomponents.RAMB36E2
    generic map(
      CASCADE_ORDER_A => "MIDDLE",
      CASCADE_ORDER_B => "MIDDLE",
      CLOCK_DOMAINS => "COMMON",
      DOA_REG => 0,
      DOB_REG => 0,
      ENADDRENA => "FALSE",
      ENADDRENB => "FALSE",
      EN_ECC_PIPE => "FALSE",
      EN_ECC_READ => "FALSE",
      EN_ECC_WRITE => "FALSE",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      RDADDRCHANGEA => "FALSE",
      RDADDRCHANGEB => "FALSE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 9,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SLEEP_ASYNC => "FALSE",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 9
    )
        port map (
      ADDRARDADDR(14 downto 3) => portA_addr(11 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(14 downto 3) => portB_addr(11 downto 0),
      ADDRBWRADDR(2 downto 0) => B"111",
      ADDRENA => '0',
      ADDRENB => '0',
      CASDIMUXA => '0',
      CASDIMUXB => '0',
      CASDINA(31 downto 8) => B"000000000000000000000000",
      CASDINA(7) => \genblk2[1].ram_reg_2_bram_0_n_28\,
      CASDINA(6) => \genblk2[1].ram_reg_2_bram_0_n_29\,
      CASDINA(5) => \genblk2[1].ram_reg_2_bram_0_n_30\,
      CASDINA(4) => \genblk2[1].ram_reg_2_bram_0_n_31\,
      CASDINA(3) => \genblk2[1].ram_reg_2_bram_0_n_32\,
      CASDINA(2) => \genblk2[1].ram_reg_2_bram_0_n_33\,
      CASDINA(1) => \genblk2[1].ram_reg_2_bram_0_n_34\,
      CASDINA(0) => \genblk2[1].ram_reg_2_bram_0_n_35\,
      CASDINB(31 downto 8) => B"000000000000000000000000",
      CASDINB(7) => \genblk2[1].ram_reg_2_bram_0_n_60\,
      CASDINB(6) => \genblk2[1].ram_reg_2_bram_0_n_61\,
      CASDINB(5) => \genblk2[1].ram_reg_2_bram_0_n_62\,
      CASDINB(4) => \genblk2[1].ram_reg_2_bram_0_n_63\,
      CASDINB(3) => \genblk2[1].ram_reg_2_bram_0_n_64\,
      CASDINB(2) => \genblk2[1].ram_reg_2_bram_0_n_65\,
      CASDINB(1) => \genblk2[1].ram_reg_2_bram_0_n_66\,
      CASDINB(0) => \genblk2[1].ram_reg_2_bram_0_n_67\,
      CASDINPA(3) => \genblk2[1].ram_reg_2_bram_0_n_132\,
      CASDINPA(2) => \genblk2[1].ram_reg_2_bram_0_n_133\,
      CASDINPA(1) => \genblk2[1].ram_reg_2_bram_0_n_134\,
      CASDINPA(0) => \genblk2[1].ram_reg_2_bram_0_n_135\,
      CASDINPB(3) => \genblk2[1].ram_reg_2_bram_0_n_136\,
      CASDINPB(2) => \genblk2[1].ram_reg_2_bram_0_n_137\,
      CASDINPB(1) => \genblk2[1].ram_reg_2_bram_0_n_138\,
      CASDINPB(0) => \genblk2[1].ram_reg_2_bram_0_n_139\,
      CASDOMUXA => \genblk2[1].ram_reg_0_bram_1_i_1_n_0\,
      CASDOMUXB => \genblk2[1].ram_reg_0_bram_1_i_2_n_0\,
      CASDOMUXEN_A => portA_en,
      CASDOMUXEN_B => portB_en,
      CASDOUTA(31 downto 8) => \NLW_genblk2[1].ram_reg_2_bram_1_CASDOUTA_UNCONNECTED\(31 downto 8),
      CASDOUTA(7) => \genblk2[1].ram_reg_2_bram_1_n_28\,
      CASDOUTA(6) => \genblk2[1].ram_reg_2_bram_1_n_29\,
      CASDOUTA(5) => \genblk2[1].ram_reg_2_bram_1_n_30\,
      CASDOUTA(4) => \genblk2[1].ram_reg_2_bram_1_n_31\,
      CASDOUTA(3) => \genblk2[1].ram_reg_2_bram_1_n_32\,
      CASDOUTA(2) => \genblk2[1].ram_reg_2_bram_1_n_33\,
      CASDOUTA(1) => \genblk2[1].ram_reg_2_bram_1_n_34\,
      CASDOUTA(0) => \genblk2[1].ram_reg_2_bram_1_n_35\,
      CASDOUTB(31 downto 8) => \NLW_genblk2[1].ram_reg_2_bram_1_CASDOUTB_UNCONNECTED\(31 downto 8),
      CASDOUTB(7) => \genblk2[1].ram_reg_2_bram_1_n_60\,
      CASDOUTB(6) => \genblk2[1].ram_reg_2_bram_1_n_61\,
      CASDOUTB(5) => \genblk2[1].ram_reg_2_bram_1_n_62\,
      CASDOUTB(4) => \genblk2[1].ram_reg_2_bram_1_n_63\,
      CASDOUTB(3) => \genblk2[1].ram_reg_2_bram_1_n_64\,
      CASDOUTB(2) => \genblk2[1].ram_reg_2_bram_1_n_65\,
      CASDOUTB(1) => \genblk2[1].ram_reg_2_bram_1_n_66\,
      CASDOUTB(0) => \genblk2[1].ram_reg_2_bram_1_n_67\,
      CASDOUTPA(3) => \genblk2[1].ram_reg_2_bram_1_n_132\,
      CASDOUTPA(2) => \genblk2[1].ram_reg_2_bram_1_n_133\,
      CASDOUTPA(1) => \genblk2[1].ram_reg_2_bram_1_n_134\,
      CASDOUTPA(0) => \genblk2[1].ram_reg_2_bram_1_n_135\,
      CASDOUTPB(3) => \genblk2[1].ram_reg_2_bram_1_n_136\,
      CASDOUTPB(2) => \genblk2[1].ram_reg_2_bram_1_n_137\,
      CASDOUTPB(1) => \genblk2[1].ram_reg_2_bram_1_n_138\,
      CASDOUTPB(0) => \genblk2[1].ram_reg_2_bram_1_n_139\,
      CASINDBITERR => \genblk2[1].ram_reg_2_bram_0_n_0\,
      CASINSBITERR => \genblk2[1].ram_reg_2_bram_0_n_1\,
      CASOREGIMUXA => '0',
      CASOREGIMUXB => '0',
      CASOREGIMUXEN_A => '1',
      CASOREGIMUXEN_B => '1',
      CASOUTDBITERR => \genblk2[1].ram_reg_2_bram_1_n_0\,
      CASOUTSBITERR => \genblk2[1].ram_reg_2_bram_1_n_1\,
      CLKARDCLK => clk,
      CLKBWRCLK => clk,
      DBITERR => \NLW_genblk2[1].ram_reg_2_bram_1_DBITERR_UNCONNECTED\,
      DINADIN(31 downto 8) => B"000000000000000000000000",
      DINADIN(7 downto 0) => p_1_in(23 downto 16),
      DINBDIN(31 downto 8) => B"000000000000000000000000",
      DINBDIN(7 downto 0) => p_2_in(23 downto 16),
      DINPADINP(3 downto 0) => B"0000",
      DINPBDINP(3 downto 0) => B"0000",
      DOUTADOUT(31 downto 0) => \NLW_genblk2[1].ram_reg_2_bram_1_DOUTADOUT_UNCONNECTED\(31 downto 0),
      DOUTBDOUT(31 downto 0) => \NLW_genblk2[1].ram_reg_2_bram_1_DOUTBDOUT_UNCONNECTED\(31 downto 0),
      DOUTPADOUTP(3 downto 0) => \NLW_genblk2[1].ram_reg_2_bram_1_DOUTPADOUTP_UNCONNECTED\(3 downto 0),
      DOUTPBDOUTP(3 downto 0) => \NLW_genblk2[1].ram_reg_2_bram_1_DOUTPBDOUTP_UNCONNECTED\(3 downto 0),
      ECCPARITY(7 downto 0) => \NLW_genblk2[1].ram_reg_2_bram_1_ECCPARITY_UNCONNECTED\(7 downto 0),
      ECCPIPECE => '1',
      ENARDEN => \genblk2[1].ram_reg_0_bram_1_i_3_n_0\,
      ENBWREN => \genblk2[1].ram_reg_0_bram_1_i_4_n_0\,
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_genblk2[1].ram_reg_2_bram_1_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '1',
      REGCEB => '1',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_genblk2[1].ram_reg_2_bram_1_SBITERR_UNCONNECTED\,
      SLEEP => '0',
      WEA(3) => \genblk2[1].ram_reg_2_bram_1_i_1_n_0\,
      WEA(2) => \genblk2[1].ram_reg_2_bram_1_i_1_n_0\,
      WEA(1) => \genblk2[1].ram_reg_2_bram_1_i_1_n_0\,
      WEA(0) => \genblk2[1].ram_reg_2_bram_1_i_1_n_0\,
      WEBWE(7 downto 4) => B"0000",
      WEBWE(3) => \genblk2[1].ram_reg_2_bram_1_i_2_n_0\,
      WEBWE(2) => \genblk2[1].ram_reg_2_bram_1_i_2_n_0\,
      WEBWE(1) => \genblk2[1].ram_reg_2_bram_1_i_2_n_0\,
      WEBWE(0) => \genblk2[1].ram_reg_2_bram_1_i_2_n_0\
    );
\genblk2[1].ram_reg_2_bram_1_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => portA_be(2),
      I1 => portA_addr(13),
      I2 => portA_addr(12),
      O => \genblk2[1].ram_reg_2_bram_1_i_1_n_0\
    );
\genblk2[1].ram_reg_2_bram_1_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => portB_be(2),
      I1 => portB_addr(13),
      I2 => portB_addr(12),
      O => \genblk2[1].ram_reg_2_bram_1_i_2_n_0\
    );
\genblk2[1].ram_reg_2_bram_2\: unisim.vcomponents.RAMB36E2
    generic map(
      CASCADE_ORDER_A => "MIDDLE",
      CASCADE_ORDER_B => "MIDDLE",
      CLOCK_DOMAINS => "COMMON",
      DOA_REG => 0,
      DOB_REG => 0,
      ENADDRENA => "FALSE",
      ENADDRENB => "FALSE",
      EN_ECC_PIPE => "FALSE",
      EN_ECC_READ => "FALSE",
      EN_ECC_WRITE => "FALSE",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      RDADDRCHANGEA => "FALSE",
      RDADDRCHANGEB => "FALSE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 9,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SLEEP_ASYNC => "FALSE",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 9
    )
        port map (
      ADDRARDADDR(14 downto 3) => portA_addr(11 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(14 downto 3) => portB_addr(11 downto 0),
      ADDRBWRADDR(2 downto 0) => B"111",
      ADDRENA => '0',
      ADDRENB => '0',
      CASDIMUXA => '0',
      CASDIMUXB => '0',
      CASDINA(31 downto 8) => B"000000000000000000000000",
      CASDINA(7) => \genblk2[1].ram_reg_2_bram_1_n_28\,
      CASDINA(6) => \genblk2[1].ram_reg_2_bram_1_n_29\,
      CASDINA(5) => \genblk2[1].ram_reg_2_bram_1_n_30\,
      CASDINA(4) => \genblk2[1].ram_reg_2_bram_1_n_31\,
      CASDINA(3) => \genblk2[1].ram_reg_2_bram_1_n_32\,
      CASDINA(2) => \genblk2[1].ram_reg_2_bram_1_n_33\,
      CASDINA(1) => \genblk2[1].ram_reg_2_bram_1_n_34\,
      CASDINA(0) => \genblk2[1].ram_reg_2_bram_1_n_35\,
      CASDINB(31 downto 8) => B"000000000000000000000000",
      CASDINB(7) => \genblk2[1].ram_reg_2_bram_1_n_60\,
      CASDINB(6) => \genblk2[1].ram_reg_2_bram_1_n_61\,
      CASDINB(5) => \genblk2[1].ram_reg_2_bram_1_n_62\,
      CASDINB(4) => \genblk2[1].ram_reg_2_bram_1_n_63\,
      CASDINB(3) => \genblk2[1].ram_reg_2_bram_1_n_64\,
      CASDINB(2) => \genblk2[1].ram_reg_2_bram_1_n_65\,
      CASDINB(1) => \genblk2[1].ram_reg_2_bram_1_n_66\,
      CASDINB(0) => \genblk2[1].ram_reg_2_bram_1_n_67\,
      CASDINPA(3) => \genblk2[1].ram_reg_2_bram_1_n_132\,
      CASDINPA(2) => \genblk2[1].ram_reg_2_bram_1_n_133\,
      CASDINPA(1) => \genblk2[1].ram_reg_2_bram_1_n_134\,
      CASDINPA(0) => \genblk2[1].ram_reg_2_bram_1_n_135\,
      CASDINPB(3) => \genblk2[1].ram_reg_2_bram_1_n_136\,
      CASDINPB(2) => \genblk2[1].ram_reg_2_bram_1_n_137\,
      CASDINPB(1) => \genblk2[1].ram_reg_2_bram_1_n_138\,
      CASDINPB(0) => \genblk2[1].ram_reg_2_bram_1_n_139\,
      CASDOMUXA => \genblk2[1].ram_reg_0_bram_2_i_1_n_0\,
      CASDOMUXB => \genblk2[1].ram_reg_0_bram_2_i_2_n_0\,
      CASDOMUXEN_A => portA_en,
      CASDOMUXEN_B => portB_en,
      CASDOUTA(31 downto 8) => \NLW_genblk2[1].ram_reg_2_bram_2_CASDOUTA_UNCONNECTED\(31 downto 8),
      CASDOUTA(7) => \genblk2[1].ram_reg_2_bram_2_n_28\,
      CASDOUTA(6) => \genblk2[1].ram_reg_2_bram_2_n_29\,
      CASDOUTA(5) => \genblk2[1].ram_reg_2_bram_2_n_30\,
      CASDOUTA(4) => \genblk2[1].ram_reg_2_bram_2_n_31\,
      CASDOUTA(3) => \genblk2[1].ram_reg_2_bram_2_n_32\,
      CASDOUTA(2) => \genblk2[1].ram_reg_2_bram_2_n_33\,
      CASDOUTA(1) => \genblk2[1].ram_reg_2_bram_2_n_34\,
      CASDOUTA(0) => \genblk2[1].ram_reg_2_bram_2_n_35\,
      CASDOUTB(31 downto 8) => \NLW_genblk2[1].ram_reg_2_bram_2_CASDOUTB_UNCONNECTED\(31 downto 8),
      CASDOUTB(7) => \genblk2[1].ram_reg_2_bram_2_n_60\,
      CASDOUTB(6) => \genblk2[1].ram_reg_2_bram_2_n_61\,
      CASDOUTB(5) => \genblk2[1].ram_reg_2_bram_2_n_62\,
      CASDOUTB(4) => \genblk2[1].ram_reg_2_bram_2_n_63\,
      CASDOUTB(3) => \genblk2[1].ram_reg_2_bram_2_n_64\,
      CASDOUTB(2) => \genblk2[1].ram_reg_2_bram_2_n_65\,
      CASDOUTB(1) => \genblk2[1].ram_reg_2_bram_2_n_66\,
      CASDOUTB(0) => \genblk2[1].ram_reg_2_bram_2_n_67\,
      CASDOUTPA(3) => \genblk2[1].ram_reg_2_bram_2_n_132\,
      CASDOUTPA(2) => \genblk2[1].ram_reg_2_bram_2_n_133\,
      CASDOUTPA(1) => \genblk2[1].ram_reg_2_bram_2_n_134\,
      CASDOUTPA(0) => \genblk2[1].ram_reg_2_bram_2_n_135\,
      CASDOUTPB(3) => \genblk2[1].ram_reg_2_bram_2_n_136\,
      CASDOUTPB(2) => \genblk2[1].ram_reg_2_bram_2_n_137\,
      CASDOUTPB(1) => \genblk2[1].ram_reg_2_bram_2_n_138\,
      CASDOUTPB(0) => \genblk2[1].ram_reg_2_bram_2_n_139\,
      CASINDBITERR => \genblk2[1].ram_reg_2_bram_1_n_0\,
      CASINSBITERR => \genblk2[1].ram_reg_2_bram_1_n_1\,
      CASOREGIMUXA => '0',
      CASOREGIMUXB => '0',
      CASOREGIMUXEN_A => '1',
      CASOREGIMUXEN_B => '1',
      CASOUTDBITERR => \genblk2[1].ram_reg_2_bram_2_n_0\,
      CASOUTSBITERR => \genblk2[1].ram_reg_2_bram_2_n_1\,
      CLKARDCLK => clk,
      CLKBWRCLK => clk,
      DBITERR => \NLW_genblk2[1].ram_reg_2_bram_2_DBITERR_UNCONNECTED\,
      DINADIN(31 downto 8) => B"000000000000000000000000",
      DINADIN(7 downto 0) => p_1_in(23 downto 16),
      DINBDIN(31 downto 8) => B"000000000000000000000000",
      DINBDIN(7 downto 0) => p_2_in(23 downto 16),
      DINPADINP(3 downto 0) => B"0000",
      DINPBDINP(3 downto 0) => B"0000",
      DOUTADOUT(31 downto 0) => \NLW_genblk2[1].ram_reg_2_bram_2_DOUTADOUT_UNCONNECTED\(31 downto 0),
      DOUTBDOUT(31 downto 0) => \NLW_genblk2[1].ram_reg_2_bram_2_DOUTBDOUT_UNCONNECTED\(31 downto 0),
      DOUTPADOUTP(3 downto 0) => \NLW_genblk2[1].ram_reg_2_bram_2_DOUTPADOUTP_UNCONNECTED\(3 downto 0),
      DOUTPBDOUTP(3 downto 0) => \NLW_genblk2[1].ram_reg_2_bram_2_DOUTPBDOUTP_UNCONNECTED\(3 downto 0),
      ECCPARITY(7 downto 0) => \NLW_genblk2[1].ram_reg_2_bram_2_ECCPARITY_UNCONNECTED\(7 downto 0),
      ECCPIPECE => '1',
      ENARDEN => \genblk2[1].ram_reg_0_bram_2_i_3_n_0\,
      ENBWREN => \genblk2[1].ram_reg_0_bram_2_i_4_n_0\,
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_genblk2[1].ram_reg_2_bram_2_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '1',
      REGCEB => '1',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_genblk2[1].ram_reg_2_bram_2_SBITERR_UNCONNECTED\,
      SLEEP => '0',
      WEA(3) => \genblk2[1].ram_reg_2_bram_2_i_1_n_0\,
      WEA(2) => \genblk2[1].ram_reg_2_bram_2_i_1_n_0\,
      WEA(1) => \genblk2[1].ram_reg_2_bram_2_i_1_n_0\,
      WEA(0) => \genblk2[1].ram_reg_2_bram_2_i_1_n_0\,
      WEBWE(7 downto 4) => B"0000",
      WEBWE(3) => \genblk2[1].ram_reg_2_bram_2_i_2_n_0\,
      WEBWE(2) => \genblk2[1].ram_reg_2_bram_2_i_2_n_0\,
      WEBWE(1) => \genblk2[1].ram_reg_2_bram_2_i_2_n_0\,
      WEBWE(0) => \genblk2[1].ram_reg_2_bram_2_i_2_n_0\
    );
\genblk2[1].ram_reg_2_bram_2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => portA_be(2),
      I1 => portA_addr(12),
      I2 => portA_addr(13),
      O => \genblk2[1].ram_reg_2_bram_2_i_1_n_0\
    );
\genblk2[1].ram_reg_2_bram_2_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => portB_be(2),
      I1 => portB_addr(12),
      I2 => portB_addr(13),
      O => \genblk2[1].ram_reg_2_bram_2_i_2_n_0\
    );
\genblk2[1].ram_reg_2_bram_3\: unisim.vcomponents.RAMB36E2
    generic map(
      CASCADE_ORDER_A => "LAST",
      CASCADE_ORDER_B => "LAST",
      CLOCK_DOMAINS => "COMMON",
      DOA_REG => 0,
      DOB_REG => 0,
      ENADDRENA => "FALSE",
      ENADDRENB => "FALSE",
      EN_ECC_PIPE => "FALSE",
      EN_ECC_READ => "FALSE",
      EN_ECC_WRITE => "FALSE",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      RDADDRCHANGEA => "FALSE",
      RDADDRCHANGEB => "FALSE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 9,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SLEEP_ASYNC => "FALSE",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 9
    )
        port map (
      ADDRARDADDR(14 downto 3) => portA_addr(11 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(14 downto 3) => portB_addr(11 downto 0),
      ADDRBWRADDR(2 downto 0) => B"111",
      ADDRENA => '0',
      ADDRENB => '0',
      CASDIMUXA => '0',
      CASDIMUXB => '0',
      CASDINA(31 downto 8) => B"000000000000000000000000",
      CASDINA(7) => \genblk2[1].ram_reg_2_bram_2_n_28\,
      CASDINA(6) => \genblk2[1].ram_reg_2_bram_2_n_29\,
      CASDINA(5) => \genblk2[1].ram_reg_2_bram_2_n_30\,
      CASDINA(4) => \genblk2[1].ram_reg_2_bram_2_n_31\,
      CASDINA(3) => \genblk2[1].ram_reg_2_bram_2_n_32\,
      CASDINA(2) => \genblk2[1].ram_reg_2_bram_2_n_33\,
      CASDINA(1) => \genblk2[1].ram_reg_2_bram_2_n_34\,
      CASDINA(0) => \genblk2[1].ram_reg_2_bram_2_n_35\,
      CASDINB(31 downto 8) => B"000000000000000000000000",
      CASDINB(7) => \genblk2[1].ram_reg_2_bram_2_n_60\,
      CASDINB(6) => \genblk2[1].ram_reg_2_bram_2_n_61\,
      CASDINB(5) => \genblk2[1].ram_reg_2_bram_2_n_62\,
      CASDINB(4) => \genblk2[1].ram_reg_2_bram_2_n_63\,
      CASDINB(3) => \genblk2[1].ram_reg_2_bram_2_n_64\,
      CASDINB(2) => \genblk2[1].ram_reg_2_bram_2_n_65\,
      CASDINB(1) => \genblk2[1].ram_reg_2_bram_2_n_66\,
      CASDINB(0) => \genblk2[1].ram_reg_2_bram_2_n_67\,
      CASDINPA(3) => \genblk2[1].ram_reg_2_bram_2_n_132\,
      CASDINPA(2) => \genblk2[1].ram_reg_2_bram_2_n_133\,
      CASDINPA(1) => \genblk2[1].ram_reg_2_bram_2_n_134\,
      CASDINPA(0) => \genblk2[1].ram_reg_2_bram_2_n_135\,
      CASDINPB(3) => \genblk2[1].ram_reg_2_bram_2_n_136\,
      CASDINPB(2) => \genblk2[1].ram_reg_2_bram_2_n_137\,
      CASDINPB(1) => \genblk2[1].ram_reg_2_bram_2_n_138\,
      CASDINPB(0) => \genblk2[1].ram_reg_2_bram_2_n_139\,
      CASDOMUXA => \genblk2[1].ram_reg_3_bram_3_i_1_n_0\,
      CASDOMUXB => \genblk2[1].ram_reg_3_bram_3_i_2_n_0\,
      CASDOMUXEN_A => portA_en,
      CASDOMUXEN_B => portB_en,
      CASDOUTA(31 downto 0) => \NLW_genblk2[1].ram_reg_2_bram_3_CASDOUTA_UNCONNECTED\(31 downto 0),
      CASDOUTB(31 downto 0) => \NLW_genblk2[1].ram_reg_2_bram_3_CASDOUTB_UNCONNECTED\(31 downto 0),
      CASDOUTPA(3 downto 0) => \NLW_genblk2[1].ram_reg_2_bram_3_CASDOUTPA_UNCONNECTED\(3 downto 0),
      CASDOUTPB(3 downto 0) => \NLW_genblk2[1].ram_reg_2_bram_3_CASDOUTPB_UNCONNECTED\(3 downto 0),
      CASINDBITERR => \genblk2[1].ram_reg_2_bram_2_n_0\,
      CASINSBITERR => \genblk2[1].ram_reg_2_bram_2_n_1\,
      CASOREGIMUXA => '0',
      CASOREGIMUXB => '0',
      CASOREGIMUXEN_A => '1',
      CASOREGIMUXEN_B => '1',
      CASOUTDBITERR => \NLW_genblk2[1].ram_reg_2_bram_3_CASOUTDBITERR_UNCONNECTED\,
      CASOUTSBITERR => \NLW_genblk2[1].ram_reg_2_bram_3_CASOUTSBITERR_UNCONNECTED\,
      CLKARDCLK => clk,
      CLKBWRCLK => clk,
      DBITERR => \NLW_genblk2[1].ram_reg_2_bram_3_DBITERR_UNCONNECTED\,
      DINADIN(31 downto 8) => B"000000000000000000000000",
      DINADIN(7 downto 0) => p_1_in(23 downto 16),
      DINBDIN(31 downto 8) => B"000000000000000000000000",
      DINBDIN(7 downto 0) => p_2_in(23 downto 16),
      DINPADINP(3 downto 0) => B"0000",
      DINPBDINP(3 downto 0) => B"0000",
      DOUTADOUT(31 downto 8) => \NLW_genblk2[1].ram_reg_2_bram_3_DOUTADOUT_UNCONNECTED\(31 downto 8),
      DOUTADOUT(7 downto 0) => portA_data_out(23 downto 16),
      DOUTBDOUT(31 downto 8) => \NLW_genblk2[1].ram_reg_2_bram_3_DOUTBDOUT_UNCONNECTED\(31 downto 8),
      DOUTBDOUT(7 downto 0) => portB_data_out(23 downto 16),
      DOUTPADOUTP(3 downto 0) => \NLW_genblk2[1].ram_reg_2_bram_3_DOUTPADOUTP_UNCONNECTED\(3 downto 0),
      DOUTPBDOUTP(3 downto 0) => \NLW_genblk2[1].ram_reg_2_bram_3_DOUTPBDOUTP_UNCONNECTED\(3 downto 0),
      ECCPARITY(7 downto 0) => \NLW_genblk2[1].ram_reg_2_bram_3_ECCPARITY_UNCONNECTED\(7 downto 0),
      ECCPIPECE => '1',
      ENARDEN => \genblk2[1].ram_reg_3_bram_3_i_3_n_0\,
      ENBWREN => \genblk2[1].ram_reg_3_bram_3_i_4_n_0\,
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_genblk2[1].ram_reg_2_bram_3_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '1',
      REGCEB => '1',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_genblk2[1].ram_reg_2_bram_3_SBITERR_UNCONNECTED\,
      SLEEP => '0',
      WEA(3) => \genblk2[1].ram_reg_2_bram_3_i_17_n_0\,
      WEA(2) => \genblk2[1].ram_reg_2_bram_3_i_17_n_0\,
      WEA(1) => \genblk2[1].ram_reg_2_bram_3_i_17_n_0\,
      WEA(0) => \genblk2[1].ram_reg_2_bram_3_i_17_n_0\,
      WEBWE(7 downto 4) => B"0000",
      WEBWE(3) => \genblk2[1].ram_reg_2_bram_3_i_18_n_0\,
      WEBWE(2) => \genblk2[1].ram_reg_2_bram_3_i_18_n_0\,
      WEBWE(1) => \genblk2[1].ram_reg_2_bram_3_i_18_n_0\,
      WEBWE(0) => \genblk2[1].ram_reg_2_bram_3_i_18_n_0\
    );
\genblk2[1].ram_reg_2_bram_3_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => portA_en,
      I1 => portA_data_in(23),
      O => p_1_in(23)
    );
\genblk2[1].ram_reg_2_bram_3_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => portB_en,
      I1 => portB_data_in(22),
      O => p_2_in(22)
    );
\genblk2[1].ram_reg_2_bram_3_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => portB_en,
      I1 => portB_data_in(21),
      O => p_2_in(21)
    );
\genblk2[1].ram_reg_2_bram_3_i_12\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => portB_en,
      I1 => portB_data_in(20),
      O => p_2_in(20)
    );
\genblk2[1].ram_reg_2_bram_3_i_13\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => portB_en,
      I1 => portB_data_in(19),
      O => p_2_in(19)
    );
\genblk2[1].ram_reg_2_bram_3_i_14\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => portB_en,
      I1 => portB_data_in(18),
      O => p_2_in(18)
    );
\genblk2[1].ram_reg_2_bram_3_i_15\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => portB_en,
      I1 => portB_data_in(17),
      O => p_2_in(17)
    );
\genblk2[1].ram_reg_2_bram_3_i_16\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => portB_en,
      I1 => portB_data_in(16),
      O => p_2_in(16)
    );
\genblk2[1].ram_reg_2_bram_3_i_17\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => portA_be(2),
      I1 => portA_addr(13),
      I2 => portA_addr(12),
      O => \genblk2[1].ram_reg_2_bram_3_i_17_n_0\
    );
\genblk2[1].ram_reg_2_bram_3_i_18\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => portB_be(2),
      I1 => portB_addr(13),
      I2 => portB_addr(12),
      O => \genblk2[1].ram_reg_2_bram_3_i_18_n_0\
    );
\genblk2[1].ram_reg_2_bram_3_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => portA_en,
      I1 => portA_data_in(22),
      O => p_1_in(22)
    );
\genblk2[1].ram_reg_2_bram_3_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => portA_en,
      I1 => portA_data_in(21),
      O => p_1_in(21)
    );
\genblk2[1].ram_reg_2_bram_3_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => portA_en,
      I1 => portA_data_in(20),
      O => p_1_in(20)
    );
\genblk2[1].ram_reg_2_bram_3_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => portA_en,
      I1 => portA_data_in(19),
      O => p_1_in(19)
    );
\genblk2[1].ram_reg_2_bram_3_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => portA_en,
      I1 => portA_data_in(18),
      O => p_1_in(18)
    );
\genblk2[1].ram_reg_2_bram_3_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => portA_en,
      I1 => portA_data_in(17),
      O => p_1_in(17)
    );
\genblk2[1].ram_reg_2_bram_3_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => portA_en,
      I1 => portA_data_in(16),
      O => p_1_in(16)
    );
\genblk2[1].ram_reg_2_bram_3_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => portB_en,
      I1 => portB_data_in(23),
      O => p_2_in(23)
    );
\genblk2[1].ram_reg_3_bram_0\: unisim.vcomponents.RAMB36E2
    generic map(
      CASCADE_ORDER_A => "FIRST",
      CASCADE_ORDER_B => "FIRST",
      CLOCK_DOMAINS => "COMMON",
      DOA_REG => 0,
      DOB_REG => 0,
      ENADDRENA => "FALSE",
      ENADDRENB => "FALSE",
      EN_ECC_PIPE => "FALSE",
      EN_ECC_READ => "FALSE",
      EN_ECC_WRITE => "FALSE",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      RDADDRCHANGEA => "FALSE",
      RDADDRCHANGEB => "FALSE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 9,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SLEEP_ASYNC => "FALSE",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 9
    )
        port map (
      ADDRARDADDR(14 downto 3) => portA_addr(11 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(14 downto 3) => portB_addr(11 downto 0),
      ADDRBWRADDR(2 downto 0) => B"111",
      ADDRENA => '0',
      ADDRENB => '0',
      CASDIMUXA => '0',
      CASDIMUXB => '0',
      CASDINA(31 downto 0) => B"00000000000000000000000000000000",
      CASDINB(31 downto 0) => B"00000000000000000000000000000000",
      CASDINPA(3 downto 0) => B"0000",
      CASDINPB(3 downto 0) => B"0000",
      CASDOMUXA => '0',
      CASDOMUXB => '0',
      CASDOMUXEN_A => '1',
      CASDOMUXEN_B => '1',
      CASDOUTA(31 downto 8) => \NLW_genblk2[1].ram_reg_3_bram_0_CASDOUTA_UNCONNECTED\(31 downto 8),
      CASDOUTA(7) => \genblk2[1].ram_reg_3_bram_0_n_28\,
      CASDOUTA(6) => \genblk2[1].ram_reg_3_bram_0_n_29\,
      CASDOUTA(5) => \genblk2[1].ram_reg_3_bram_0_n_30\,
      CASDOUTA(4) => \genblk2[1].ram_reg_3_bram_0_n_31\,
      CASDOUTA(3) => \genblk2[1].ram_reg_3_bram_0_n_32\,
      CASDOUTA(2) => \genblk2[1].ram_reg_3_bram_0_n_33\,
      CASDOUTA(1) => \genblk2[1].ram_reg_3_bram_0_n_34\,
      CASDOUTA(0) => \genblk2[1].ram_reg_3_bram_0_n_35\,
      CASDOUTB(31 downto 8) => \NLW_genblk2[1].ram_reg_3_bram_0_CASDOUTB_UNCONNECTED\(31 downto 8),
      CASDOUTB(7) => \genblk2[1].ram_reg_3_bram_0_n_60\,
      CASDOUTB(6) => \genblk2[1].ram_reg_3_bram_0_n_61\,
      CASDOUTB(5) => \genblk2[1].ram_reg_3_bram_0_n_62\,
      CASDOUTB(4) => \genblk2[1].ram_reg_3_bram_0_n_63\,
      CASDOUTB(3) => \genblk2[1].ram_reg_3_bram_0_n_64\,
      CASDOUTB(2) => \genblk2[1].ram_reg_3_bram_0_n_65\,
      CASDOUTB(1) => \genblk2[1].ram_reg_3_bram_0_n_66\,
      CASDOUTB(0) => \genblk2[1].ram_reg_3_bram_0_n_67\,
      CASDOUTPA(3) => \genblk2[1].ram_reg_3_bram_0_n_132\,
      CASDOUTPA(2) => \genblk2[1].ram_reg_3_bram_0_n_133\,
      CASDOUTPA(1) => \genblk2[1].ram_reg_3_bram_0_n_134\,
      CASDOUTPA(0) => \genblk2[1].ram_reg_3_bram_0_n_135\,
      CASDOUTPB(3) => \genblk2[1].ram_reg_3_bram_0_n_136\,
      CASDOUTPB(2) => \genblk2[1].ram_reg_3_bram_0_n_137\,
      CASDOUTPB(1) => \genblk2[1].ram_reg_3_bram_0_n_138\,
      CASDOUTPB(0) => \genblk2[1].ram_reg_3_bram_0_n_139\,
      CASINDBITERR => '0',
      CASINSBITERR => '0',
      CASOREGIMUXA => '0',
      CASOREGIMUXB => '0',
      CASOREGIMUXEN_A => '1',
      CASOREGIMUXEN_B => '1',
      CASOUTDBITERR => \genblk2[1].ram_reg_3_bram_0_n_0\,
      CASOUTSBITERR => \genblk2[1].ram_reg_3_bram_0_n_1\,
      CLKARDCLK => clk,
      CLKBWRCLK => clk,
      DBITERR => \NLW_genblk2[1].ram_reg_3_bram_0_DBITERR_UNCONNECTED\,
      DINADIN(31 downto 8) => B"000000000000000000000000",
      DINADIN(7 downto 0) => p_1_in(31 downto 24),
      DINBDIN(31 downto 8) => B"000000000000000000000000",
      DINBDIN(7 downto 0) => p_2_in(31 downto 24),
      DINPADINP(3 downto 0) => B"0000",
      DINPBDINP(3 downto 0) => B"0000",
      DOUTADOUT(31 downto 0) => \NLW_genblk2[1].ram_reg_3_bram_0_DOUTADOUT_UNCONNECTED\(31 downto 0),
      DOUTBDOUT(31 downto 0) => \NLW_genblk2[1].ram_reg_3_bram_0_DOUTBDOUT_UNCONNECTED\(31 downto 0),
      DOUTPADOUTP(3 downto 0) => \NLW_genblk2[1].ram_reg_3_bram_0_DOUTPADOUTP_UNCONNECTED\(3 downto 0),
      DOUTPBDOUTP(3 downto 0) => \NLW_genblk2[1].ram_reg_3_bram_0_DOUTPBDOUTP_UNCONNECTED\(3 downto 0),
      ECCPARITY(7 downto 0) => \NLW_genblk2[1].ram_reg_3_bram_0_ECCPARITY_UNCONNECTED\(7 downto 0),
      ECCPIPECE => '1',
      ENARDEN => \genblk2[1].ram_reg_0_bram_0_i_1_n_0\,
      ENBWREN => \genblk2[1].ram_reg_0_bram_0_i_2_n_0\,
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_genblk2[1].ram_reg_3_bram_0_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '1',
      REGCEB => '1',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_genblk2[1].ram_reg_3_bram_0_SBITERR_UNCONNECTED\,
      SLEEP => '0',
      WEA(3) => \genblk2[1].ram_reg_3_bram_0_i_1_n_0\,
      WEA(2) => \genblk2[1].ram_reg_3_bram_0_i_1_n_0\,
      WEA(1) => \genblk2[1].ram_reg_3_bram_0_i_1_n_0\,
      WEA(0) => \genblk2[1].ram_reg_3_bram_0_i_1_n_0\,
      WEBWE(7 downto 4) => B"0000",
      WEBWE(3) => \genblk2[1].ram_reg_3_bram_0_i_2_n_0\,
      WEBWE(2) => \genblk2[1].ram_reg_3_bram_0_i_2_n_0\,
      WEBWE(1) => \genblk2[1].ram_reg_3_bram_0_i_2_n_0\,
      WEBWE(0) => \genblk2[1].ram_reg_3_bram_0_i_2_n_0\
    );
\genblk2[1].ram_reg_3_bram_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => portA_be(3),
      I1 => portA_addr(13),
      I2 => portA_addr(12),
      O => \genblk2[1].ram_reg_3_bram_0_i_1_n_0\
    );
\genblk2[1].ram_reg_3_bram_0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => portB_be(3),
      I1 => portB_addr(13),
      I2 => portB_addr(12),
      O => \genblk2[1].ram_reg_3_bram_0_i_2_n_0\
    );
\genblk2[1].ram_reg_3_bram_1\: unisim.vcomponents.RAMB36E2
    generic map(
      CASCADE_ORDER_A => "MIDDLE",
      CASCADE_ORDER_B => "MIDDLE",
      CLOCK_DOMAINS => "COMMON",
      DOA_REG => 0,
      DOB_REG => 0,
      ENADDRENA => "FALSE",
      ENADDRENB => "FALSE",
      EN_ECC_PIPE => "FALSE",
      EN_ECC_READ => "FALSE",
      EN_ECC_WRITE => "FALSE",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      RDADDRCHANGEA => "FALSE",
      RDADDRCHANGEB => "FALSE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 9,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SLEEP_ASYNC => "FALSE",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 9
    )
        port map (
      ADDRARDADDR(14 downto 3) => portA_addr(11 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(14 downto 3) => portB_addr(11 downto 0),
      ADDRBWRADDR(2 downto 0) => B"111",
      ADDRENA => '0',
      ADDRENB => '0',
      CASDIMUXA => '0',
      CASDIMUXB => '0',
      CASDINA(31 downto 8) => B"000000000000000000000000",
      CASDINA(7) => \genblk2[1].ram_reg_3_bram_0_n_28\,
      CASDINA(6) => \genblk2[1].ram_reg_3_bram_0_n_29\,
      CASDINA(5) => \genblk2[1].ram_reg_3_bram_0_n_30\,
      CASDINA(4) => \genblk2[1].ram_reg_3_bram_0_n_31\,
      CASDINA(3) => \genblk2[1].ram_reg_3_bram_0_n_32\,
      CASDINA(2) => \genblk2[1].ram_reg_3_bram_0_n_33\,
      CASDINA(1) => \genblk2[1].ram_reg_3_bram_0_n_34\,
      CASDINA(0) => \genblk2[1].ram_reg_3_bram_0_n_35\,
      CASDINB(31 downto 8) => B"000000000000000000000000",
      CASDINB(7) => \genblk2[1].ram_reg_3_bram_0_n_60\,
      CASDINB(6) => \genblk2[1].ram_reg_3_bram_0_n_61\,
      CASDINB(5) => \genblk2[1].ram_reg_3_bram_0_n_62\,
      CASDINB(4) => \genblk2[1].ram_reg_3_bram_0_n_63\,
      CASDINB(3) => \genblk2[1].ram_reg_3_bram_0_n_64\,
      CASDINB(2) => \genblk2[1].ram_reg_3_bram_0_n_65\,
      CASDINB(1) => \genblk2[1].ram_reg_3_bram_0_n_66\,
      CASDINB(0) => \genblk2[1].ram_reg_3_bram_0_n_67\,
      CASDINPA(3) => \genblk2[1].ram_reg_3_bram_0_n_132\,
      CASDINPA(2) => \genblk2[1].ram_reg_3_bram_0_n_133\,
      CASDINPA(1) => \genblk2[1].ram_reg_3_bram_0_n_134\,
      CASDINPA(0) => \genblk2[1].ram_reg_3_bram_0_n_135\,
      CASDINPB(3) => \genblk2[1].ram_reg_3_bram_0_n_136\,
      CASDINPB(2) => \genblk2[1].ram_reg_3_bram_0_n_137\,
      CASDINPB(1) => \genblk2[1].ram_reg_3_bram_0_n_138\,
      CASDINPB(0) => \genblk2[1].ram_reg_3_bram_0_n_139\,
      CASDOMUXA => \genblk2[1].ram_reg_0_bram_1_i_1_n_0\,
      CASDOMUXB => \genblk2[1].ram_reg_0_bram_1_i_2_n_0\,
      CASDOMUXEN_A => portA_en,
      CASDOMUXEN_B => portB_en,
      CASDOUTA(31 downto 8) => \NLW_genblk2[1].ram_reg_3_bram_1_CASDOUTA_UNCONNECTED\(31 downto 8),
      CASDOUTA(7) => \genblk2[1].ram_reg_3_bram_1_n_28\,
      CASDOUTA(6) => \genblk2[1].ram_reg_3_bram_1_n_29\,
      CASDOUTA(5) => \genblk2[1].ram_reg_3_bram_1_n_30\,
      CASDOUTA(4) => \genblk2[1].ram_reg_3_bram_1_n_31\,
      CASDOUTA(3) => \genblk2[1].ram_reg_3_bram_1_n_32\,
      CASDOUTA(2) => \genblk2[1].ram_reg_3_bram_1_n_33\,
      CASDOUTA(1) => \genblk2[1].ram_reg_3_bram_1_n_34\,
      CASDOUTA(0) => \genblk2[1].ram_reg_3_bram_1_n_35\,
      CASDOUTB(31 downto 8) => \NLW_genblk2[1].ram_reg_3_bram_1_CASDOUTB_UNCONNECTED\(31 downto 8),
      CASDOUTB(7) => \genblk2[1].ram_reg_3_bram_1_n_60\,
      CASDOUTB(6) => \genblk2[1].ram_reg_3_bram_1_n_61\,
      CASDOUTB(5) => \genblk2[1].ram_reg_3_bram_1_n_62\,
      CASDOUTB(4) => \genblk2[1].ram_reg_3_bram_1_n_63\,
      CASDOUTB(3) => \genblk2[1].ram_reg_3_bram_1_n_64\,
      CASDOUTB(2) => \genblk2[1].ram_reg_3_bram_1_n_65\,
      CASDOUTB(1) => \genblk2[1].ram_reg_3_bram_1_n_66\,
      CASDOUTB(0) => \genblk2[1].ram_reg_3_bram_1_n_67\,
      CASDOUTPA(3) => \genblk2[1].ram_reg_3_bram_1_n_132\,
      CASDOUTPA(2) => \genblk2[1].ram_reg_3_bram_1_n_133\,
      CASDOUTPA(1) => \genblk2[1].ram_reg_3_bram_1_n_134\,
      CASDOUTPA(0) => \genblk2[1].ram_reg_3_bram_1_n_135\,
      CASDOUTPB(3) => \genblk2[1].ram_reg_3_bram_1_n_136\,
      CASDOUTPB(2) => \genblk2[1].ram_reg_3_bram_1_n_137\,
      CASDOUTPB(1) => \genblk2[1].ram_reg_3_bram_1_n_138\,
      CASDOUTPB(0) => \genblk2[1].ram_reg_3_bram_1_n_139\,
      CASINDBITERR => \genblk2[1].ram_reg_3_bram_0_n_0\,
      CASINSBITERR => \genblk2[1].ram_reg_3_bram_0_n_1\,
      CASOREGIMUXA => '0',
      CASOREGIMUXB => '0',
      CASOREGIMUXEN_A => '1',
      CASOREGIMUXEN_B => '1',
      CASOUTDBITERR => \genblk2[1].ram_reg_3_bram_1_n_0\,
      CASOUTSBITERR => \genblk2[1].ram_reg_3_bram_1_n_1\,
      CLKARDCLK => clk,
      CLKBWRCLK => clk,
      DBITERR => \NLW_genblk2[1].ram_reg_3_bram_1_DBITERR_UNCONNECTED\,
      DINADIN(31 downto 8) => B"000000000000000000000000",
      DINADIN(7 downto 0) => p_1_in(31 downto 24),
      DINBDIN(31 downto 8) => B"000000000000000000000000",
      DINBDIN(7 downto 0) => p_2_in(31 downto 24),
      DINPADINP(3 downto 0) => B"0000",
      DINPBDINP(3 downto 0) => B"0000",
      DOUTADOUT(31 downto 0) => \NLW_genblk2[1].ram_reg_3_bram_1_DOUTADOUT_UNCONNECTED\(31 downto 0),
      DOUTBDOUT(31 downto 0) => \NLW_genblk2[1].ram_reg_3_bram_1_DOUTBDOUT_UNCONNECTED\(31 downto 0),
      DOUTPADOUTP(3 downto 0) => \NLW_genblk2[1].ram_reg_3_bram_1_DOUTPADOUTP_UNCONNECTED\(3 downto 0),
      DOUTPBDOUTP(3 downto 0) => \NLW_genblk2[1].ram_reg_3_bram_1_DOUTPBDOUTP_UNCONNECTED\(3 downto 0),
      ECCPARITY(7 downto 0) => \NLW_genblk2[1].ram_reg_3_bram_1_ECCPARITY_UNCONNECTED\(7 downto 0),
      ECCPIPECE => '1',
      ENARDEN => \genblk2[1].ram_reg_0_bram_1_i_3_n_0\,
      ENBWREN => \genblk2[1].ram_reg_0_bram_1_i_4_n_0\,
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_genblk2[1].ram_reg_3_bram_1_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '1',
      REGCEB => '1',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_genblk2[1].ram_reg_3_bram_1_SBITERR_UNCONNECTED\,
      SLEEP => '0',
      WEA(3) => \genblk2[1].ram_reg_3_bram_1_i_1_n_0\,
      WEA(2) => \genblk2[1].ram_reg_3_bram_1_i_1_n_0\,
      WEA(1) => \genblk2[1].ram_reg_3_bram_1_i_1_n_0\,
      WEA(0) => \genblk2[1].ram_reg_3_bram_1_i_1_n_0\,
      WEBWE(7 downto 4) => B"0000",
      WEBWE(3) => \genblk2[1].ram_reg_3_bram_1_i_2_n_0\,
      WEBWE(2) => \genblk2[1].ram_reg_3_bram_1_i_2_n_0\,
      WEBWE(1) => \genblk2[1].ram_reg_3_bram_1_i_2_n_0\,
      WEBWE(0) => \genblk2[1].ram_reg_3_bram_1_i_2_n_0\
    );
\genblk2[1].ram_reg_3_bram_1_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => portA_be(3),
      I1 => portA_addr(13),
      I2 => portA_addr(12),
      O => \genblk2[1].ram_reg_3_bram_1_i_1_n_0\
    );
\genblk2[1].ram_reg_3_bram_1_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => portB_be(3),
      I1 => portB_addr(13),
      I2 => portB_addr(12),
      O => \genblk2[1].ram_reg_3_bram_1_i_2_n_0\
    );
\genblk2[1].ram_reg_3_bram_2\: unisim.vcomponents.RAMB36E2
    generic map(
      CASCADE_ORDER_A => "MIDDLE",
      CASCADE_ORDER_B => "MIDDLE",
      CLOCK_DOMAINS => "COMMON",
      DOA_REG => 0,
      DOB_REG => 0,
      ENADDRENA => "FALSE",
      ENADDRENB => "FALSE",
      EN_ECC_PIPE => "FALSE",
      EN_ECC_READ => "FALSE",
      EN_ECC_WRITE => "FALSE",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      RDADDRCHANGEA => "FALSE",
      RDADDRCHANGEB => "FALSE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 9,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SLEEP_ASYNC => "FALSE",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 9
    )
        port map (
      ADDRARDADDR(14 downto 3) => portA_addr(11 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(14 downto 3) => portB_addr(11 downto 0),
      ADDRBWRADDR(2 downto 0) => B"111",
      ADDRENA => '0',
      ADDRENB => '0',
      CASDIMUXA => '0',
      CASDIMUXB => '0',
      CASDINA(31 downto 8) => B"000000000000000000000000",
      CASDINA(7) => \genblk2[1].ram_reg_3_bram_1_n_28\,
      CASDINA(6) => \genblk2[1].ram_reg_3_bram_1_n_29\,
      CASDINA(5) => \genblk2[1].ram_reg_3_bram_1_n_30\,
      CASDINA(4) => \genblk2[1].ram_reg_3_bram_1_n_31\,
      CASDINA(3) => \genblk2[1].ram_reg_3_bram_1_n_32\,
      CASDINA(2) => \genblk2[1].ram_reg_3_bram_1_n_33\,
      CASDINA(1) => \genblk2[1].ram_reg_3_bram_1_n_34\,
      CASDINA(0) => \genblk2[1].ram_reg_3_bram_1_n_35\,
      CASDINB(31 downto 8) => B"000000000000000000000000",
      CASDINB(7) => \genblk2[1].ram_reg_3_bram_1_n_60\,
      CASDINB(6) => \genblk2[1].ram_reg_3_bram_1_n_61\,
      CASDINB(5) => \genblk2[1].ram_reg_3_bram_1_n_62\,
      CASDINB(4) => \genblk2[1].ram_reg_3_bram_1_n_63\,
      CASDINB(3) => \genblk2[1].ram_reg_3_bram_1_n_64\,
      CASDINB(2) => \genblk2[1].ram_reg_3_bram_1_n_65\,
      CASDINB(1) => \genblk2[1].ram_reg_3_bram_1_n_66\,
      CASDINB(0) => \genblk2[1].ram_reg_3_bram_1_n_67\,
      CASDINPA(3) => \genblk2[1].ram_reg_3_bram_1_n_132\,
      CASDINPA(2) => \genblk2[1].ram_reg_3_bram_1_n_133\,
      CASDINPA(1) => \genblk2[1].ram_reg_3_bram_1_n_134\,
      CASDINPA(0) => \genblk2[1].ram_reg_3_bram_1_n_135\,
      CASDINPB(3) => \genblk2[1].ram_reg_3_bram_1_n_136\,
      CASDINPB(2) => \genblk2[1].ram_reg_3_bram_1_n_137\,
      CASDINPB(1) => \genblk2[1].ram_reg_3_bram_1_n_138\,
      CASDINPB(0) => \genblk2[1].ram_reg_3_bram_1_n_139\,
      CASDOMUXA => \genblk2[1].ram_reg_0_bram_2_i_1_n_0\,
      CASDOMUXB => \genblk2[1].ram_reg_0_bram_2_i_2_n_0\,
      CASDOMUXEN_A => portA_en,
      CASDOMUXEN_B => portB_en,
      CASDOUTA(31 downto 8) => \NLW_genblk2[1].ram_reg_3_bram_2_CASDOUTA_UNCONNECTED\(31 downto 8),
      CASDOUTA(7) => \genblk2[1].ram_reg_3_bram_2_n_28\,
      CASDOUTA(6) => \genblk2[1].ram_reg_3_bram_2_n_29\,
      CASDOUTA(5) => \genblk2[1].ram_reg_3_bram_2_n_30\,
      CASDOUTA(4) => \genblk2[1].ram_reg_3_bram_2_n_31\,
      CASDOUTA(3) => \genblk2[1].ram_reg_3_bram_2_n_32\,
      CASDOUTA(2) => \genblk2[1].ram_reg_3_bram_2_n_33\,
      CASDOUTA(1) => \genblk2[1].ram_reg_3_bram_2_n_34\,
      CASDOUTA(0) => \genblk2[1].ram_reg_3_bram_2_n_35\,
      CASDOUTB(31 downto 8) => \NLW_genblk2[1].ram_reg_3_bram_2_CASDOUTB_UNCONNECTED\(31 downto 8),
      CASDOUTB(7) => \genblk2[1].ram_reg_3_bram_2_n_60\,
      CASDOUTB(6) => \genblk2[1].ram_reg_3_bram_2_n_61\,
      CASDOUTB(5) => \genblk2[1].ram_reg_3_bram_2_n_62\,
      CASDOUTB(4) => \genblk2[1].ram_reg_3_bram_2_n_63\,
      CASDOUTB(3) => \genblk2[1].ram_reg_3_bram_2_n_64\,
      CASDOUTB(2) => \genblk2[1].ram_reg_3_bram_2_n_65\,
      CASDOUTB(1) => \genblk2[1].ram_reg_3_bram_2_n_66\,
      CASDOUTB(0) => \genblk2[1].ram_reg_3_bram_2_n_67\,
      CASDOUTPA(3) => \genblk2[1].ram_reg_3_bram_2_n_132\,
      CASDOUTPA(2) => \genblk2[1].ram_reg_3_bram_2_n_133\,
      CASDOUTPA(1) => \genblk2[1].ram_reg_3_bram_2_n_134\,
      CASDOUTPA(0) => \genblk2[1].ram_reg_3_bram_2_n_135\,
      CASDOUTPB(3) => \genblk2[1].ram_reg_3_bram_2_n_136\,
      CASDOUTPB(2) => \genblk2[1].ram_reg_3_bram_2_n_137\,
      CASDOUTPB(1) => \genblk2[1].ram_reg_3_bram_2_n_138\,
      CASDOUTPB(0) => \genblk2[1].ram_reg_3_bram_2_n_139\,
      CASINDBITERR => \genblk2[1].ram_reg_3_bram_1_n_0\,
      CASINSBITERR => \genblk2[1].ram_reg_3_bram_1_n_1\,
      CASOREGIMUXA => '0',
      CASOREGIMUXB => '0',
      CASOREGIMUXEN_A => '1',
      CASOREGIMUXEN_B => '1',
      CASOUTDBITERR => \genblk2[1].ram_reg_3_bram_2_n_0\,
      CASOUTSBITERR => \genblk2[1].ram_reg_3_bram_2_n_1\,
      CLKARDCLK => clk,
      CLKBWRCLK => clk,
      DBITERR => \NLW_genblk2[1].ram_reg_3_bram_2_DBITERR_UNCONNECTED\,
      DINADIN(31 downto 8) => B"000000000000000000000000",
      DINADIN(7 downto 0) => p_1_in(31 downto 24),
      DINBDIN(31 downto 8) => B"000000000000000000000000",
      DINBDIN(7 downto 0) => p_2_in(31 downto 24),
      DINPADINP(3 downto 0) => B"0000",
      DINPBDINP(3 downto 0) => B"0000",
      DOUTADOUT(31 downto 0) => \NLW_genblk2[1].ram_reg_3_bram_2_DOUTADOUT_UNCONNECTED\(31 downto 0),
      DOUTBDOUT(31 downto 0) => \NLW_genblk2[1].ram_reg_3_bram_2_DOUTBDOUT_UNCONNECTED\(31 downto 0),
      DOUTPADOUTP(3 downto 0) => \NLW_genblk2[1].ram_reg_3_bram_2_DOUTPADOUTP_UNCONNECTED\(3 downto 0),
      DOUTPBDOUTP(3 downto 0) => \NLW_genblk2[1].ram_reg_3_bram_2_DOUTPBDOUTP_UNCONNECTED\(3 downto 0),
      ECCPARITY(7 downto 0) => \NLW_genblk2[1].ram_reg_3_bram_2_ECCPARITY_UNCONNECTED\(7 downto 0),
      ECCPIPECE => '1',
      ENARDEN => \genblk2[1].ram_reg_0_bram_2_i_3_n_0\,
      ENBWREN => \genblk2[1].ram_reg_0_bram_2_i_4_n_0\,
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_genblk2[1].ram_reg_3_bram_2_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '1',
      REGCEB => '1',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_genblk2[1].ram_reg_3_bram_2_SBITERR_UNCONNECTED\,
      SLEEP => '0',
      WEA(3) => \genblk2[1].ram_reg_3_bram_2_i_1_n_0\,
      WEA(2) => \genblk2[1].ram_reg_3_bram_2_i_1_n_0\,
      WEA(1) => \genblk2[1].ram_reg_3_bram_2_i_1_n_0\,
      WEA(0) => \genblk2[1].ram_reg_3_bram_2_i_1_n_0\,
      WEBWE(7 downto 4) => B"0000",
      WEBWE(3) => \genblk2[1].ram_reg_3_bram_2_i_2_n_0\,
      WEBWE(2) => \genblk2[1].ram_reg_3_bram_2_i_2_n_0\,
      WEBWE(1) => \genblk2[1].ram_reg_3_bram_2_i_2_n_0\,
      WEBWE(0) => \genblk2[1].ram_reg_3_bram_2_i_2_n_0\
    );
\genblk2[1].ram_reg_3_bram_2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => portA_be(3),
      I1 => portA_addr(12),
      I2 => portA_addr(13),
      O => \genblk2[1].ram_reg_3_bram_2_i_1_n_0\
    );
\genblk2[1].ram_reg_3_bram_2_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => portB_be(3),
      I1 => portB_addr(12),
      I2 => portB_addr(13),
      O => \genblk2[1].ram_reg_3_bram_2_i_2_n_0\
    );
\genblk2[1].ram_reg_3_bram_3\: unisim.vcomponents.RAMB36E2
    generic map(
      CASCADE_ORDER_A => "LAST",
      CASCADE_ORDER_B => "LAST",
      CLOCK_DOMAINS => "COMMON",
      DOA_REG => 0,
      DOB_REG => 0,
      ENADDRENA => "FALSE",
      ENADDRENB => "FALSE",
      EN_ECC_PIPE => "FALSE",
      EN_ECC_READ => "FALSE",
      EN_ECC_WRITE => "FALSE",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      RDADDRCHANGEA => "FALSE",
      RDADDRCHANGEB => "FALSE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 9,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SLEEP_ASYNC => "FALSE",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 9
    )
        port map (
      ADDRARDADDR(14 downto 3) => portA_addr(11 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(14 downto 3) => portB_addr(11 downto 0),
      ADDRBWRADDR(2 downto 0) => B"111",
      ADDRENA => '0',
      ADDRENB => '0',
      CASDIMUXA => '0',
      CASDIMUXB => '0',
      CASDINA(31 downto 8) => B"000000000000000000000000",
      CASDINA(7) => \genblk2[1].ram_reg_3_bram_2_n_28\,
      CASDINA(6) => \genblk2[1].ram_reg_3_bram_2_n_29\,
      CASDINA(5) => \genblk2[1].ram_reg_3_bram_2_n_30\,
      CASDINA(4) => \genblk2[1].ram_reg_3_bram_2_n_31\,
      CASDINA(3) => \genblk2[1].ram_reg_3_bram_2_n_32\,
      CASDINA(2) => \genblk2[1].ram_reg_3_bram_2_n_33\,
      CASDINA(1) => \genblk2[1].ram_reg_3_bram_2_n_34\,
      CASDINA(0) => \genblk2[1].ram_reg_3_bram_2_n_35\,
      CASDINB(31 downto 8) => B"000000000000000000000000",
      CASDINB(7) => \genblk2[1].ram_reg_3_bram_2_n_60\,
      CASDINB(6) => \genblk2[1].ram_reg_3_bram_2_n_61\,
      CASDINB(5) => \genblk2[1].ram_reg_3_bram_2_n_62\,
      CASDINB(4) => \genblk2[1].ram_reg_3_bram_2_n_63\,
      CASDINB(3) => \genblk2[1].ram_reg_3_bram_2_n_64\,
      CASDINB(2) => \genblk2[1].ram_reg_3_bram_2_n_65\,
      CASDINB(1) => \genblk2[1].ram_reg_3_bram_2_n_66\,
      CASDINB(0) => \genblk2[1].ram_reg_3_bram_2_n_67\,
      CASDINPA(3) => \genblk2[1].ram_reg_3_bram_2_n_132\,
      CASDINPA(2) => \genblk2[1].ram_reg_3_bram_2_n_133\,
      CASDINPA(1) => \genblk2[1].ram_reg_3_bram_2_n_134\,
      CASDINPA(0) => \genblk2[1].ram_reg_3_bram_2_n_135\,
      CASDINPB(3) => \genblk2[1].ram_reg_3_bram_2_n_136\,
      CASDINPB(2) => \genblk2[1].ram_reg_3_bram_2_n_137\,
      CASDINPB(1) => \genblk2[1].ram_reg_3_bram_2_n_138\,
      CASDINPB(0) => \genblk2[1].ram_reg_3_bram_2_n_139\,
      CASDOMUXA => \genblk2[1].ram_reg_3_bram_3_i_1_n_0\,
      CASDOMUXB => \genblk2[1].ram_reg_3_bram_3_i_2_n_0\,
      CASDOMUXEN_A => portA_en,
      CASDOMUXEN_B => portB_en,
      CASDOUTA(31 downto 0) => \NLW_genblk2[1].ram_reg_3_bram_3_CASDOUTA_UNCONNECTED\(31 downto 0),
      CASDOUTB(31 downto 0) => \NLW_genblk2[1].ram_reg_3_bram_3_CASDOUTB_UNCONNECTED\(31 downto 0),
      CASDOUTPA(3 downto 0) => \NLW_genblk2[1].ram_reg_3_bram_3_CASDOUTPA_UNCONNECTED\(3 downto 0),
      CASDOUTPB(3 downto 0) => \NLW_genblk2[1].ram_reg_3_bram_3_CASDOUTPB_UNCONNECTED\(3 downto 0),
      CASINDBITERR => \genblk2[1].ram_reg_3_bram_2_n_0\,
      CASINSBITERR => \genblk2[1].ram_reg_3_bram_2_n_1\,
      CASOREGIMUXA => '0',
      CASOREGIMUXB => '0',
      CASOREGIMUXEN_A => '1',
      CASOREGIMUXEN_B => '1',
      CASOUTDBITERR => \NLW_genblk2[1].ram_reg_3_bram_3_CASOUTDBITERR_UNCONNECTED\,
      CASOUTSBITERR => \NLW_genblk2[1].ram_reg_3_bram_3_CASOUTSBITERR_UNCONNECTED\,
      CLKARDCLK => clk,
      CLKBWRCLK => clk,
      DBITERR => \NLW_genblk2[1].ram_reg_3_bram_3_DBITERR_UNCONNECTED\,
      DINADIN(31 downto 8) => B"000000000000000000000000",
      DINADIN(7 downto 0) => p_1_in(31 downto 24),
      DINBDIN(31 downto 8) => B"000000000000000000000000",
      DINBDIN(7 downto 0) => p_2_in(31 downto 24),
      DINPADINP(3 downto 0) => B"0000",
      DINPBDINP(3 downto 0) => B"0000",
      DOUTADOUT(31 downto 8) => \NLW_genblk2[1].ram_reg_3_bram_3_DOUTADOUT_UNCONNECTED\(31 downto 8),
      DOUTADOUT(7 downto 0) => portA_data_out(31 downto 24),
      DOUTBDOUT(31 downto 8) => \NLW_genblk2[1].ram_reg_3_bram_3_DOUTBDOUT_UNCONNECTED\(31 downto 8),
      DOUTBDOUT(7 downto 0) => portB_data_out(31 downto 24),
      DOUTPADOUTP(3 downto 0) => \NLW_genblk2[1].ram_reg_3_bram_3_DOUTPADOUTP_UNCONNECTED\(3 downto 0),
      DOUTPBDOUTP(3 downto 0) => \NLW_genblk2[1].ram_reg_3_bram_3_DOUTPBDOUTP_UNCONNECTED\(3 downto 0),
      ECCPARITY(7 downto 0) => \NLW_genblk2[1].ram_reg_3_bram_3_ECCPARITY_UNCONNECTED\(7 downto 0),
      ECCPIPECE => '1',
      ENARDEN => \genblk2[1].ram_reg_3_bram_3_i_3_n_0\,
      ENBWREN => \genblk2[1].ram_reg_3_bram_3_i_4_n_0\,
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_genblk2[1].ram_reg_3_bram_3_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '1',
      REGCEB => '1',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_genblk2[1].ram_reg_3_bram_3_SBITERR_UNCONNECTED\,
      SLEEP => '0',
      WEA(3) => \genblk2[1].ram_reg_3_bram_3_i_21_n_0\,
      WEA(2) => \genblk2[1].ram_reg_3_bram_3_i_21_n_0\,
      WEA(1) => \genblk2[1].ram_reg_3_bram_3_i_21_n_0\,
      WEA(0) => \genblk2[1].ram_reg_3_bram_3_i_21_n_0\,
      WEBWE(7 downto 4) => B"0000",
      WEBWE(3) => \genblk2[1].ram_reg_3_bram_3_i_22_n_0\,
      WEBWE(2) => \genblk2[1].ram_reg_3_bram_3_i_22_n_0\,
      WEBWE(1) => \genblk2[1].ram_reg_3_bram_3_i_22_n_0\,
      WEBWE(0) => \genblk2[1].ram_reg_3_bram_3_i_22_n_0\
    );
\genblk2[1].ram_reg_3_bram_3_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => portA_addr(13),
      I1 => portA_addr(12),
      O => \genblk2[1].ram_reg_3_bram_3_i_1_n_0\
    );
\genblk2[1].ram_reg_3_bram_3_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => portA_en,
      I1 => portA_data_in(26),
      O => p_1_in(26)
    );
\genblk2[1].ram_reg_3_bram_3_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => portA_en,
      I1 => portA_data_in(25),
      O => p_1_in(25)
    );
\genblk2[1].ram_reg_3_bram_3_i_12\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => portA_en,
      I1 => portA_data_in(24),
      O => p_1_in(24)
    );
\genblk2[1].ram_reg_3_bram_3_i_13\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => portB_en,
      I1 => portB_data_in(31),
      O => p_2_in(31)
    );
\genblk2[1].ram_reg_3_bram_3_i_14\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => portB_en,
      I1 => portB_data_in(30),
      O => p_2_in(30)
    );
\genblk2[1].ram_reg_3_bram_3_i_15\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => portB_en,
      I1 => portB_data_in(29),
      O => p_2_in(29)
    );
\genblk2[1].ram_reg_3_bram_3_i_16\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => portB_en,
      I1 => portB_data_in(28),
      O => p_2_in(28)
    );
\genblk2[1].ram_reg_3_bram_3_i_17\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => portB_en,
      I1 => portB_data_in(27),
      O => p_2_in(27)
    );
\genblk2[1].ram_reg_3_bram_3_i_18\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => portB_en,
      I1 => portB_data_in(26),
      O => p_2_in(26)
    );
\genblk2[1].ram_reg_3_bram_3_i_19\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => portB_en,
      I1 => portB_data_in(25),
      O => p_2_in(25)
    );
\genblk2[1].ram_reg_3_bram_3_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => portB_addr(13),
      I1 => portB_addr(12),
      O => \genblk2[1].ram_reg_3_bram_3_i_2_n_0\
    );
\genblk2[1].ram_reg_3_bram_3_i_20\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => portB_en,
      I1 => portB_data_in(24),
      O => p_2_in(24)
    );
\genblk2[1].ram_reg_3_bram_3_i_21\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => portA_be(3),
      I1 => portA_addr(13),
      I2 => portA_addr(12),
      O => \genblk2[1].ram_reg_3_bram_3_i_21_n_0\
    );
\genblk2[1].ram_reg_3_bram_3_i_22\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => portB_be(3),
      I1 => portB_addr(13),
      I2 => portB_addr(12),
      O => \genblk2[1].ram_reg_3_bram_3_i_22_n_0\
    );
\genblk2[1].ram_reg_3_bram_3_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => portA_en,
      I1 => portA_addr(13),
      I2 => portA_addr(12),
      O => \genblk2[1].ram_reg_3_bram_3_i_3_n_0\
    );
\genblk2[1].ram_reg_3_bram_3_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => portB_en,
      I1 => portB_addr(13),
      I2 => portB_addr(12),
      O => \genblk2[1].ram_reg_3_bram_3_i_4_n_0\
    );
\genblk2[1].ram_reg_3_bram_3_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => portA_en,
      I1 => portA_data_in(31),
      O => p_1_in(31)
    );
\genblk2[1].ram_reg_3_bram_3_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => portA_en,
      I1 => portA_data_in(30),
      O => p_1_in(30)
    );
\genblk2[1].ram_reg_3_bram_3_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => portA_en,
      I1 => portA_data_in(29),
      O => p_1_in(29)
    );
\genblk2[1].ram_reg_3_bram_3_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => portA_en,
      I1 => portA_data_in(28),
      O => p_1_in(28)
    );
\genblk2[1].ram_reg_3_bram_3_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => portA_en,
      I1 => portA_data_in(27),
      O => p_1_in(27)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gpn_bundle_block_local_mem_0_0_byte_en_BRAM is
  port (
    portA_data_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    portB_data_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clk : in STD_LOGIC;
    portA_addr : in STD_LOGIC_VECTOR ( 13 downto 0 );
    portB_addr : in STD_LOGIC_VECTOR ( 13 downto 0 );
    portA_data_in : in STD_LOGIC_VECTOR ( 31 downto 0 );
    portB_data_in : in STD_LOGIC_VECTOR ( 31 downto 0 );
    portA_en : in STD_LOGIC;
    portB_en : in STD_LOGIC;
    portA_be : in STD_LOGIC_VECTOR ( 3 downto 0 );
    portB_be : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gpn_bundle_block_local_mem_0_0_byte_en_BRAM : entity is "byte_en_BRAM";
end gpn_bundle_block_local_mem_0_0_byte_en_BRAM;

architecture STRUCTURE of gpn_bundle_block_local_mem_0_0_byte_en_BRAM is
begin
ram_block: entity work.gpn_bundle_block_local_mem_0_0_xilinx_byte_enable_ram
     port map (
      clk => clk,
      portA_addr(13 downto 0) => portA_addr(13 downto 0),
      portA_be(3 downto 0) => portA_be(3 downto 0),
      portA_data_in(31 downto 0) => portA_data_in(31 downto 0),
      portA_data_out(31 downto 0) => portA_data_out(31 downto 0),
      portA_en => portA_en,
      portB_addr(13 downto 0) => portB_addr(13 downto 0),
      portB_be(3 downto 0) => portB_be(3 downto 0),
      portB_data_in(31 downto 0) => portB_data_in(31 downto 0),
      portB_data_out(31 downto 0) => portB_data_out(31 downto 0),
      portB_en => portB_en
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gpn_bundle_block_local_mem_0_0_local_mem is
  port (
    portA_data_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    portB_data_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clk : in STD_LOGIC;
    portA_addr : in STD_LOGIC_VECTOR ( 13 downto 0 );
    portB_addr : in STD_LOGIC_VECTOR ( 13 downto 0 );
    portA_data_in : in STD_LOGIC_VECTOR ( 31 downto 0 );
    portB_data_in : in STD_LOGIC_VECTOR ( 31 downto 0 );
    portA_en : in STD_LOGIC;
    portB_en : in STD_LOGIC;
    portA_be : in STD_LOGIC_VECTOR ( 3 downto 0 );
    portB_be : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gpn_bundle_block_local_mem_0_0_local_mem : entity is "local_mem";
end gpn_bundle_block_local_mem_0_0_local_mem;

architecture STRUCTURE of gpn_bundle_block_local_mem_0_0_local_mem is
begin
inst_data_ram: entity work.gpn_bundle_block_local_mem_0_0_byte_en_BRAM
     port map (
      clk => clk,
      portA_addr(13 downto 0) => portA_addr(13 downto 0),
      portA_be(3 downto 0) => portA_be(3 downto 0),
      portA_data_in(31 downto 0) => portA_data_in(31 downto 0),
      portA_data_out(31 downto 0) => portA_data_out(31 downto 0),
      portA_en => portA_en,
      portB_addr(13 downto 0) => portB_addr(13 downto 0),
      portB_be(3 downto 0) => portB_be(3 downto 0),
      portB_data_in(31 downto 0) => portB_data_in(31 downto 0),
      portB_data_out(31 downto 0) => portB_data_out(31 downto 0),
      portB_en => portB_en
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gpn_bundle_block_local_mem_0_0 is
  port (
    clk : in STD_LOGIC;
    rstn : in STD_LOGIC;
    portA_en : in STD_LOGIC;
    portA_be : in STD_LOGIC_VECTOR ( 3 downto 0 );
    portA_addr : in STD_LOGIC_VECTOR ( 29 downto 0 );
    portA_data_in : in STD_LOGIC_VECTOR ( 31 downto 0 );
    portA_data_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    portB_en : in STD_LOGIC;
    portB_be : in STD_LOGIC_VECTOR ( 3 downto 0 );
    portB_addr : in STD_LOGIC_VECTOR ( 29 downto 0 );
    portB_data_in : in STD_LOGIC_VECTOR ( 31 downto 0 );
    portB_data_out : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of gpn_bundle_block_local_mem_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of gpn_bundle_block_local_mem_0_0 : entity is "gpn_bundle_block_local_mem_0_0,local_mem,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of gpn_bundle_block_local_mem_0_0 : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of gpn_bundle_block_local_mem_0_0 : entity is "package_project";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of gpn_bundle_block_local_mem_0_0 : entity is "local_mem,Vivado 2020.2";
end gpn_bundle_block_local_mem_0_0;

architecture STRUCTURE of gpn_bundle_block_local_mem_0_0 is
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of clk : signal is "xilinx.com:signal:clock:1.0 clk CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of clk : signal is "XIL_INTERFACENAME clk, ASSOCIATED_RESET rstn, ASSOCIATED_BUSIF portA:portB, FREQ_HZ 200000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN gpn_bundle_block_kernel_clk, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of portA_en : signal is "user.org:interface:local_memory_interface:1.0 portA en";
  attribute X_INTERFACE_INFO of portB_en : signal is "user.org:interface:local_memory_interface:1.0 portB en";
  attribute X_INTERFACE_INFO of rstn : signal is "xilinx.com:signal:reset:1.0 rstn RST";
  attribute X_INTERFACE_PARAMETER of rstn : signal is "XIL_INTERFACENAME rstn, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of portA_addr : signal is "user.org:interface:local_memory_interface:1.0 portA addr";
  attribute X_INTERFACE_INFO of portA_be : signal is "user.org:interface:local_memory_interface:1.0 portA be";
  attribute X_INTERFACE_INFO of portA_data_in : signal is "user.org:interface:local_memory_interface:1.0 portA data_in";
  attribute X_INTERFACE_INFO of portA_data_out : signal is "user.org:interface:local_memory_interface:1.0 portA data_out";
  attribute X_INTERFACE_INFO of portB_addr : signal is "user.org:interface:local_memory_interface:1.0 portB addr";
  attribute X_INTERFACE_INFO of portB_be : signal is "user.org:interface:local_memory_interface:1.0 portB be";
  attribute X_INTERFACE_INFO of portB_data_in : signal is "user.org:interface:local_memory_interface:1.0 portB data_in";
  attribute X_INTERFACE_INFO of portB_data_out : signal is "user.org:interface:local_memory_interface:1.0 portB data_out";
begin
inst: entity work.gpn_bundle_block_local_mem_0_0_local_mem
     port map (
      clk => clk,
      portA_addr(13 downto 0) => portA_addr(13 downto 0),
      portA_be(3 downto 0) => portA_be(3 downto 0),
      portA_data_in(31 downto 0) => portA_data_in(31 downto 0),
      portA_data_out(31 downto 0) => portA_data_out(31 downto 0),
      portA_en => portA_en,
      portB_addr(13 downto 0) => portB_addr(13 downto 0),
      portB_be(3 downto 0) => portB_be(3 downto 0),
      portB_data_in(31 downto 0) => portB_data_in(31 downto 0),
      portB_data_out(31 downto 0) => portB_data_out(31 downto 0),
      portB_en => portB_en
    );
end STRUCTURE;
