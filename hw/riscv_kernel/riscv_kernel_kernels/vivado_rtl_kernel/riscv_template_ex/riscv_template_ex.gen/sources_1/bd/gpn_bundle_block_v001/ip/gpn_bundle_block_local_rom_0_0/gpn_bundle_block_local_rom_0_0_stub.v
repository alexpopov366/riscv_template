// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Sun Aug 28 18:14:54 2022
// Host        : dl580 running 64-bit Ubuntu 18.04.6 LTS
// Command     : write_verilog -force -mode synth_stub -rename_top gpn_bundle_block_local_rom_0_0 -prefix
//               gpn_bundle_block_local_rom_0_0_ gpn_bundle_block_local_rom_0_0_stub.v
// Design      : gpn_bundle_block_local_rom_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xcu200-fsgd2104-2-e
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "local_rom,Vivado 2020.2" *)
module gpn_bundle_block_local_rom_0_0(clk, rstn, porta_addr, porta_en, porta_be, 
  porta_data_in, porta_data_out)
/* synthesis syn_black_box black_box_pad_pin="clk,rstn,porta_addr[29:0],porta_en,porta_be[3:0],porta_data_in[31:0],porta_data_out[31:0]" */;
  input clk;
  input rstn;
  input [29:0]porta_addr;
  input porta_en;
  input [3:0]porta_be;
  input [31:0]porta_data_in;
  output [31:0]porta_data_out;
endmodule
