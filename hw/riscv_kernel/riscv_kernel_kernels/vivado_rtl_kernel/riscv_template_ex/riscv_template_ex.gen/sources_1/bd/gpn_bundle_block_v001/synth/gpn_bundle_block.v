//Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
//Date        : Mon Aug 29 21:26:24 2022
//Host        : dl580 running 64-bit Ubuntu 18.04.6 LTS
//Command     : generate_target gpn_bundle_block.bd
//Design      : gpn_bundle_block
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CORE_GENERATION_INFO = "gpn_bundle_block,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=gpn_bundle_block,x_ipVersion=1.00.a,x_ipLanguage=VERILOG,numBlks=30,numReposBlks=17,numNonXlnxBlks=3,numHierBlks=13,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=0,numPkgbdBlks=0,bdsource=USER,synth_mode=OOC_per_IP}" *) (* HW_HANDOFF = "gpn_bundle_block.hwdef" *) 
module gpn_bundle_block
   (ext_clk,
    ext_rstn,
    external_memory_bus_araddr,
    external_memory_bus_arburst,
    external_memory_bus_arcache,
    external_memory_bus_arid,
    external_memory_bus_arlen,
    external_memory_bus_arlock,
    external_memory_bus_arprot,
    external_memory_bus_arqos,
    external_memory_bus_arready,
    external_memory_bus_arregion,
    external_memory_bus_arsize,
    external_memory_bus_aruser,
    external_memory_bus_arvalid,
    external_memory_bus_awaddr,
    external_memory_bus_awburst,
    external_memory_bus_awcache,
    external_memory_bus_awid,
    external_memory_bus_awlen,
    external_memory_bus_awlock,
    external_memory_bus_awprot,
    external_memory_bus_awqos,
    external_memory_bus_awready,
    external_memory_bus_awregion,
    external_memory_bus_awsize,
    external_memory_bus_awuser,
    external_memory_bus_awvalid,
    external_memory_bus_bid,
    external_memory_bus_bready,
    external_memory_bus_bresp,
    external_memory_bus_buser,
    external_memory_bus_bvalid,
    external_memory_bus_rdata,
    external_memory_bus_rid,
    external_memory_bus_rlast,
    external_memory_bus_rready,
    external_memory_bus_rresp,
    external_memory_bus_ruser,
    external_memory_bus_rvalid,
    external_memory_bus_wdata,
    external_memory_bus_wlast,
    external_memory_bus_wready,
    external_memory_bus_wstrb,
    external_memory_bus_wuser,
    external_memory_bus_wvalid,
    global_memory_bus_araddr,
    global_memory_bus_arburst,
    global_memory_bus_arcache,
    global_memory_bus_arid,
    global_memory_bus_arlen,
    global_memory_bus_arlock,
    global_memory_bus_arprot,
    global_memory_bus_arqos,
    global_memory_bus_arready,
    global_memory_bus_arregion,
    global_memory_bus_arsize,
    global_memory_bus_aruser,
    global_memory_bus_arvalid,
    global_memory_bus_awaddr,
    global_memory_bus_awburst,
    global_memory_bus_awcache,
    global_memory_bus_awid,
    global_memory_bus_awlen,
    global_memory_bus_awlock,
    global_memory_bus_awprot,
    global_memory_bus_awqos,
    global_memory_bus_awready,
    global_memory_bus_awregion,
    global_memory_bus_awsize,
    global_memory_bus_awuser,
    global_memory_bus_awvalid,
    global_memory_bus_bid,
    global_memory_bus_bready,
    global_memory_bus_bresp,
    global_memory_bus_buser,
    global_memory_bus_bvalid,
    global_memory_bus_rdata,
    global_memory_bus_rid,
    global_memory_bus_rlast,
    global_memory_bus_rready,
    global_memory_bus_rresp,
    global_memory_bus_ruser,
    global_memory_bus_rvalid,
    global_memory_bus_wdata,
    global_memory_bus_wlast,
    global_memory_bus_wready,
    global_memory_bus_wstrb,
    global_memory_bus_wuser,
    global_memory_bus_wvalid,
    gpc2host_mq,
    gpc_control_reg,
    gpc_reset,
    gpc_start,
    gpc_state_reg,
    host2gpc_mq,
    kernel_clk,
    kernel_rstn,
    mq_ctrl,
    mq_sr);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 CLK.EXT_CLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME CLK.EXT_CLK, ASSOCIATED_BUSIF global_memory_bus:external_memory_bus, ASSOCIATED_RESET ext_rstn:ext_rstn, CLK_DOMAIN gpn_bundle_block_ext_clk, FREQ_HZ 300000000, FREQ_TOLERANCE_HZ 0, INSERT_VIP 0, PHASE 0.000" *) input ext_clk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 RST.EXT_RSTN RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME RST.EXT_RSTN, INSERT_VIP 0, POLARITY ACTIVE_LOW" *) input ext_rstn;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus ARADDR" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME external_memory_bus, ADDR_WIDTH 32, ARUSER_WIDTH 4, AWUSER_WIDTH 4, BUSER_WIDTH 4, CLK_DOMAIN gpn_bundle_block_ext_clk, DATA_WIDTH 32, FREQ_HZ 300000000, HAS_BRESP 1, HAS_BURST 1, HAS_CACHE 1, HAS_LOCK 1, HAS_PROT 1, HAS_QOS 1, HAS_REGION 0, HAS_RRESP 1, HAS_WSTRB 1, ID_WIDTH 6, INSERT_VIP 0, MAX_BURST_LENGTH 256, NUM_READ_OUTSTANDING 2, NUM_READ_THREADS 1, NUM_WRITE_OUTSTANDING 2, NUM_WRITE_THREADS 1, PHASE 0.000, PROTOCOL AXI4, READ_WRITE_MODE READ_WRITE, RUSER_BITS_PER_BYTE 0, RUSER_WIDTH 4, SUPPORTS_NARROW_BURST 1, WUSER_BITS_PER_BYTE 0, WUSER_WIDTH 4" *) output [31:0]external_memory_bus_araddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus ARBURST" *) output [1:0]external_memory_bus_arburst;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus ARCACHE" *) output [3:0]external_memory_bus_arcache;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus ARID" *) output [5:0]external_memory_bus_arid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus ARLEN" *) output [7:0]external_memory_bus_arlen;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus ARLOCK" *) output [0:0]external_memory_bus_arlock;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus ARPROT" *) output [2:0]external_memory_bus_arprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus ARQOS" *) output [3:0]external_memory_bus_arqos;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus ARREADY" *) input external_memory_bus_arready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus ARREGION" *) output [3:0]external_memory_bus_arregion;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus ARSIZE" *) output [2:0]external_memory_bus_arsize;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus ARUSER" *) output [3:0]external_memory_bus_aruser;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus ARVALID" *) output external_memory_bus_arvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus AWADDR" *) output [31:0]external_memory_bus_awaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus AWBURST" *) output [1:0]external_memory_bus_awburst;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus AWCACHE" *) output [3:0]external_memory_bus_awcache;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus AWID" *) output [5:0]external_memory_bus_awid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus AWLEN" *) output [7:0]external_memory_bus_awlen;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus AWLOCK" *) output [0:0]external_memory_bus_awlock;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus AWPROT" *) output [2:0]external_memory_bus_awprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus AWQOS" *) output [3:0]external_memory_bus_awqos;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus AWREADY" *) input external_memory_bus_awready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus AWREGION" *) output [3:0]external_memory_bus_awregion;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus AWSIZE" *) output [2:0]external_memory_bus_awsize;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus AWUSER" *) output [3:0]external_memory_bus_awuser;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus AWVALID" *) output external_memory_bus_awvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus BID" *) input [5:0]external_memory_bus_bid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus BREADY" *) output external_memory_bus_bready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus BRESP" *) input [1:0]external_memory_bus_bresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus BUSER" *) input [3:0]external_memory_bus_buser;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus BVALID" *) input external_memory_bus_bvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus RDATA" *) input [31:0]external_memory_bus_rdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus RID" *) input [5:0]external_memory_bus_rid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus RLAST" *) input external_memory_bus_rlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus RREADY" *) output external_memory_bus_rready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus RRESP" *) input [1:0]external_memory_bus_rresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus RUSER" *) input [3:0]external_memory_bus_ruser;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus RVALID" *) input external_memory_bus_rvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus WDATA" *) output [31:0]external_memory_bus_wdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus WLAST" *) output external_memory_bus_wlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus WREADY" *) input external_memory_bus_wready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus WSTRB" *) output [3:0]external_memory_bus_wstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus WUSER" *) output [3:0]external_memory_bus_wuser;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 external_memory_bus WVALID" *) output external_memory_bus_wvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus ARADDR" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME global_memory_bus, ADDR_WIDTH 32, ARUSER_WIDTH 4, AWUSER_WIDTH 4, BUSER_WIDTH 4, CLK_DOMAIN gpn_bundle_block_ext_clk, DATA_WIDTH 32, FREQ_HZ 300000000, HAS_BRESP 0, HAS_BURST 0, HAS_CACHE 0, HAS_LOCK 0, HAS_PROT 0, HAS_QOS 0, HAS_REGION 0, HAS_RRESP 0, HAS_WSTRB 1, ID_WIDTH 6, INSERT_VIP 0, MAX_BURST_LENGTH 256, NUM_READ_OUTSTANDING 2, NUM_READ_THREADS 1, NUM_WRITE_OUTSTANDING 2, NUM_WRITE_THREADS 1, PHASE 0.000, PROTOCOL AXI4, READ_WRITE_MODE READ_WRITE, RUSER_BITS_PER_BYTE 0, RUSER_WIDTH 4, SUPPORTS_NARROW_BURST 1, WUSER_BITS_PER_BYTE 0, WUSER_WIDTH 4" *) output [31:0]global_memory_bus_araddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus ARBURST" *) output [1:0]global_memory_bus_arburst;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus ARCACHE" *) output [3:0]global_memory_bus_arcache;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus ARID" *) output [5:0]global_memory_bus_arid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus ARLEN" *) output [7:0]global_memory_bus_arlen;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus ARLOCK" *) output [0:0]global_memory_bus_arlock;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus ARPROT" *) output [2:0]global_memory_bus_arprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus ARQOS" *) output [3:0]global_memory_bus_arqos;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus ARREADY" *) input global_memory_bus_arready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus ARREGION" *) output [3:0]global_memory_bus_arregion;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus ARSIZE" *) output [2:0]global_memory_bus_arsize;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus ARUSER" *) output [3:0]global_memory_bus_aruser;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus ARVALID" *) output global_memory_bus_arvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus AWADDR" *) output [31:0]global_memory_bus_awaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus AWBURST" *) output [1:0]global_memory_bus_awburst;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus AWCACHE" *) output [3:0]global_memory_bus_awcache;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus AWID" *) output [5:0]global_memory_bus_awid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus AWLEN" *) output [7:0]global_memory_bus_awlen;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus AWLOCK" *) output [0:0]global_memory_bus_awlock;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus AWPROT" *) output [2:0]global_memory_bus_awprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus AWQOS" *) output [3:0]global_memory_bus_awqos;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus AWREADY" *) input global_memory_bus_awready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus AWREGION" *) output [3:0]global_memory_bus_awregion;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus AWSIZE" *) output [2:0]global_memory_bus_awsize;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus AWUSER" *) output [3:0]global_memory_bus_awuser;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus AWVALID" *) output global_memory_bus_awvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus BID" *) input [5:0]global_memory_bus_bid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus BREADY" *) output global_memory_bus_bready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus BRESP" *) input [1:0]global_memory_bus_bresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus BUSER" *) input [3:0]global_memory_bus_buser;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus BVALID" *) input global_memory_bus_bvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus RDATA" *) input [31:0]global_memory_bus_rdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus RID" *) input [5:0]global_memory_bus_rid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus RLAST" *) input global_memory_bus_rlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus RREADY" *) output global_memory_bus_rready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus RRESP" *) input [1:0]global_memory_bus_rresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus RUSER" *) input [3:0]global_memory_bus_ruser;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus RVALID" *) input global_memory_bus_rvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus WDATA" *) output [31:0]global_memory_bus_wdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus WLAST" *) output global_memory_bus_wlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus WREADY" *) input global_memory_bus_wready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus WSTRB" *) output [3:0]global_memory_bus_wstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus WUSER" *) output [3:0]global_memory_bus_wuser;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 global_memory_bus WVALID" *) output global_memory_bus_wvalid;
  output [31:0]gpc2host_mq;
  input [31:0]gpc_control_reg;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 RST.GPC_RESET RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME RST.GPC_RESET, INSERT_VIP 0, POLARITY ACTIVE_HIGH" *) input gpc_reset;
  input [0:0]gpc_start;
  output [0:0]gpc_state_reg;
  input [31:0]host2gpc_mq;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 CLK.KERNEL_CLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME CLK.KERNEL_CLK, ASSOCIATED_RESET kernel_rstn:gpc_reset, CLK_DOMAIN gpn_bundle_block_kernel_clk, FREQ_HZ 200000000, FREQ_TOLERANCE_HZ 0, INSERT_VIP 0, PHASE 0.000" *) input kernel_clk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 RST.KERNEL_RSTN RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME RST.KERNEL_RSTN, INSERT_VIP 0, POLARITY ACTIVE_LOW" *) input kernel_rstn;
  output [1:0]mq_ctrl;
  input [2:0]mq_sr;

  wire M00_ACLK_0_1;
  wire M00_ARESETN_0_1;
  wire [0:0]axi_gpio_1_gpio_io_o;
  wire [31:0]axi_gpio_2_gpio_io_o;
  wire [31:0]axi_interconnect_0_M01_AXI_ARADDR;
  wire axi_interconnect_0_M01_AXI_ARREADY;
  wire axi_interconnect_0_M01_AXI_ARVALID;
  wire [31:0]axi_interconnect_0_M01_AXI_AWADDR;
  wire axi_interconnect_0_M01_AXI_AWREADY;
  wire axi_interconnect_0_M01_AXI_AWVALID;
  wire axi_interconnect_0_M01_AXI_BREADY;
  wire [1:0]axi_interconnect_0_M01_AXI_BRESP;
  wire axi_interconnect_0_M01_AXI_BVALID;
  wire [31:0]axi_interconnect_0_M01_AXI_RDATA;
  wire axi_interconnect_0_M01_AXI_RREADY;
  wire [1:0]axi_interconnect_0_M01_AXI_RRESP;
  wire axi_interconnect_0_M01_AXI_RVALID;
  wire [31:0]axi_interconnect_0_M01_AXI_WDATA;
  wire axi_interconnect_0_M01_AXI_WREADY;
  wire [3:0]axi_interconnect_0_M01_AXI_WSTRB;
  wire axi_interconnect_0_M01_AXI_WVALID;
  wire [31:0]axi_interconnect_0_M02_AXI_ARADDR;
  wire axi_interconnect_0_M02_AXI_ARREADY;
  wire axi_interconnect_0_M02_AXI_ARVALID;
  wire [31:0]axi_interconnect_0_M02_AXI_AWADDR;
  wire axi_interconnect_0_M02_AXI_AWREADY;
  wire axi_interconnect_0_M02_AXI_AWVALID;
  wire axi_interconnect_0_M02_AXI_BREADY;
  wire [1:0]axi_interconnect_0_M02_AXI_BRESP;
  wire axi_interconnect_0_M02_AXI_BVALID;
  wire [31:0]axi_interconnect_0_M02_AXI_RDATA;
  wire axi_interconnect_0_M02_AXI_RREADY;
  wire [1:0]axi_interconnect_0_M02_AXI_RRESP;
  wire axi_interconnect_0_M02_AXI_RVALID;
  wire [31:0]axi_interconnect_0_M02_AXI_WDATA;
  wire axi_interconnect_0_M02_AXI_WREADY;
  wire [3:0]axi_interconnect_0_M02_AXI_WSTRB;
  wire axi_interconnect_0_M02_AXI_WVALID;
  wire [31:0]axi_interconnect_1_M00_AXI_ARADDR;
  wire [1:0]axi_interconnect_1_M00_AXI_ARBURST;
  wire [3:0]axi_interconnect_1_M00_AXI_ARCACHE;
  wire [5:0]axi_interconnect_1_M00_AXI_ARID;
  wire [7:0]axi_interconnect_1_M00_AXI_ARLEN;
  wire [0:0]axi_interconnect_1_M00_AXI_ARLOCK;
  wire [2:0]axi_interconnect_1_M00_AXI_ARPROT;
  wire [3:0]axi_interconnect_1_M00_AXI_ARQOS;
  wire axi_interconnect_1_M00_AXI_ARREADY;
  wire [3:0]axi_interconnect_1_M00_AXI_ARREGION;
  wire [2:0]axi_interconnect_1_M00_AXI_ARSIZE;
  wire [3:0]axi_interconnect_1_M00_AXI_ARUSER;
  wire axi_interconnect_1_M00_AXI_ARVALID;
  wire [31:0]axi_interconnect_1_M00_AXI_AWADDR;
  wire [1:0]axi_interconnect_1_M00_AXI_AWBURST;
  wire [3:0]axi_interconnect_1_M00_AXI_AWCACHE;
  wire [5:0]axi_interconnect_1_M00_AXI_AWID;
  wire [7:0]axi_interconnect_1_M00_AXI_AWLEN;
  wire [0:0]axi_interconnect_1_M00_AXI_AWLOCK;
  wire [2:0]axi_interconnect_1_M00_AXI_AWPROT;
  wire [3:0]axi_interconnect_1_M00_AXI_AWQOS;
  wire axi_interconnect_1_M00_AXI_AWREADY;
  wire [3:0]axi_interconnect_1_M00_AXI_AWREGION;
  wire [2:0]axi_interconnect_1_M00_AXI_AWSIZE;
  wire [3:0]axi_interconnect_1_M00_AXI_AWUSER;
  wire axi_interconnect_1_M00_AXI_AWVALID;
  wire [5:0]axi_interconnect_1_M00_AXI_BID;
  wire axi_interconnect_1_M00_AXI_BREADY;
  wire [1:0]axi_interconnect_1_M00_AXI_BRESP;
  wire [3:0]axi_interconnect_1_M00_AXI_BUSER;
  wire axi_interconnect_1_M00_AXI_BVALID;
  wire [31:0]axi_interconnect_1_M00_AXI_RDATA;
  wire [5:0]axi_interconnect_1_M00_AXI_RID;
  wire axi_interconnect_1_M00_AXI_RLAST;
  wire axi_interconnect_1_M00_AXI_RREADY;
  wire [1:0]axi_interconnect_1_M00_AXI_RRESP;
  wire [3:0]axi_interconnect_1_M00_AXI_RUSER;
  wire axi_interconnect_1_M00_AXI_RVALID;
  wire [31:0]axi_interconnect_1_M00_AXI_WDATA;
  wire axi_interconnect_1_M00_AXI_WLAST;
  wire axi_interconnect_1_M00_AXI_WREADY;
  wire [3:0]axi_interconnect_1_M00_AXI_WSTRB;
  wire [3:0]axi_interconnect_1_M00_AXI_WUSER;
  wire axi_interconnect_1_M00_AXI_WVALID;
  wire [31:0]axi_interconnect_2_M03_AXI_ARADDR;
  wire [1:0]axi_interconnect_2_M03_AXI_ARBURST;
  wire [3:0]axi_interconnect_2_M03_AXI_ARCACHE;
  wire [5:0]axi_interconnect_2_M03_AXI_ARID;
  wire [7:0]axi_interconnect_2_M03_AXI_ARLEN;
  wire [0:0]axi_interconnect_2_M03_AXI_ARLOCK;
  wire [2:0]axi_interconnect_2_M03_AXI_ARPROT;
  wire [3:0]axi_interconnect_2_M03_AXI_ARQOS;
  wire axi_interconnect_2_M03_AXI_ARREADY;
  wire [3:0]axi_interconnect_2_M03_AXI_ARREGION;
  wire [2:0]axi_interconnect_2_M03_AXI_ARSIZE;
  wire [3:0]axi_interconnect_2_M03_AXI_ARUSER;
  wire axi_interconnect_2_M03_AXI_ARVALID;
  wire [31:0]axi_interconnect_2_M03_AXI_AWADDR;
  wire [1:0]axi_interconnect_2_M03_AXI_AWBURST;
  wire [3:0]axi_interconnect_2_M03_AXI_AWCACHE;
  wire [5:0]axi_interconnect_2_M03_AXI_AWID;
  wire [7:0]axi_interconnect_2_M03_AXI_AWLEN;
  wire [0:0]axi_interconnect_2_M03_AXI_AWLOCK;
  wire [2:0]axi_interconnect_2_M03_AXI_AWPROT;
  wire [3:0]axi_interconnect_2_M03_AXI_AWQOS;
  wire axi_interconnect_2_M03_AXI_AWREADY;
  wire [3:0]axi_interconnect_2_M03_AXI_AWREGION;
  wire [2:0]axi_interconnect_2_M03_AXI_AWSIZE;
  wire [3:0]axi_interconnect_2_M03_AXI_AWUSER;
  wire axi_interconnect_2_M03_AXI_AWVALID;
  wire [5:0]axi_interconnect_2_M03_AXI_BID;
  wire axi_interconnect_2_M03_AXI_BREADY;
  wire [1:0]axi_interconnect_2_M03_AXI_BRESP;
  wire [3:0]axi_interconnect_2_M03_AXI_BUSER;
  wire axi_interconnect_2_M03_AXI_BVALID;
  wire [31:0]axi_interconnect_2_M03_AXI_RDATA;
  wire [5:0]axi_interconnect_2_M03_AXI_RID;
  wire axi_interconnect_2_M03_AXI_RLAST;
  wire axi_interconnect_2_M03_AXI_RREADY;
  wire [1:0]axi_interconnect_2_M03_AXI_RRESP;
  wire [3:0]axi_interconnect_2_M03_AXI_RUSER;
  wire axi_interconnect_2_M03_AXI_RVALID;
  wire [31:0]axi_interconnect_2_M03_AXI_WDATA;
  wire axi_interconnect_2_M03_AXI_WLAST;
  wire axi_interconnect_2_M03_AXI_WREADY;
  wire [3:0]axi_interconnect_2_M03_AXI_WSTRB;
  wire [3:0]axi_interconnect_2_M03_AXI_WUSER;
  wire axi_interconnect_2_M03_AXI_WVALID;
  wire [31:0]axi_interconnect_2_M04_AXI_ARADDR;
  wire axi_interconnect_2_M04_AXI_ARREADY;
  wire axi_interconnect_2_M04_AXI_ARVALID;
  wire [31:0]axi_interconnect_2_M04_AXI_AWADDR;
  wire axi_interconnect_2_M04_AXI_AWREADY;
  wire axi_interconnect_2_M04_AXI_AWVALID;
  wire axi_interconnect_2_M04_AXI_BREADY;
  wire [1:0]axi_interconnect_2_M04_AXI_BRESP;
  wire axi_interconnect_2_M04_AXI_BVALID;
  wire [31:0]axi_interconnect_2_M04_AXI_RDATA;
  wire axi_interconnect_2_M04_AXI_RREADY;
  wire [1:0]axi_interconnect_2_M04_AXI_RRESP;
  wire axi_interconnect_2_M04_AXI_RVALID;
  wire [31:0]axi_interconnect_2_M04_AXI_WDATA;
  wire axi_interconnect_2_M04_AXI_WREADY;
  wire [3:0]axi_interconnect_2_M04_AXI_WSTRB;
  wire axi_interconnect_2_M04_AXI_WVALID;
  wire [31:0]axi_interconnect_2_M05_AXI_ARADDR;
  wire axi_interconnect_2_M05_AXI_ARREADY;
  wire axi_interconnect_2_M05_AXI_ARVALID;
  wire [31:0]axi_interconnect_2_M05_AXI_AWADDR;
  wire axi_interconnect_2_M05_AXI_AWREADY;
  wire axi_interconnect_2_M05_AXI_AWVALID;
  wire axi_interconnect_2_M05_AXI_BREADY;
  wire [1:0]axi_interconnect_2_M05_AXI_BRESP;
  wire axi_interconnect_2_M05_AXI_BVALID;
  wire [31:0]axi_interconnect_2_M05_AXI_RDATA;
  wire axi_interconnect_2_M05_AXI_RREADY;
  wire [1:0]axi_interconnect_2_M05_AXI_RRESP;
  wire axi_interconnect_2_M05_AXI_RVALID;
  wire [31:0]axi_interconnect_2_M05_AXI_WDATA;
  wire axi_interconnect_2_M05_AXI_WREADY;
  wire [3:0]axi_interconnect_2_M05_AXI_WSTRB;
  wire axi_interconnect_2_M05_AXI_WVALID;
  wire [31:0]axi_interconnect_2_M06_AXI_ARADDR;
  wire axi_interconnect_2_M06_AXI_ARREADY;
  wire axi_interconnect_2_M06_AXI_ARVALID;
  wire [31:0]axi_interconnect_2_M06_AXI_AWADDR;
  wire axi_interconnect_2_M06_AXI_AWREADY;
  wire axi_interconnect_2_M06_AXI_AWVALID;
  wire axi_interconnect_2_M06_AXI_BREADY;
  wire [1:0]axi_interconnect_2_M06_AXI_BRESP;
  wire axi_interconnect_2_M06_AXI_BVALID;
  wire [31:0]axi_interconnect_2_M06_AXI_RDATA;
  wire axi_interconnect_2_M06_AXI_RREADY;
  wire [1:0]axi_interconnect_2_M06_AXI_RRESP;
  wire axi_interconnect_2_M06_AXI_RVALID;
  wire [31:0]axi_interconnect_2_M06_AXI_WDATA;
  wire axi_interconnect_2_M06_AXI_WREADY;
  wire [3:0]axi_interconnect_2_M06_AXI_WSTRB;
  wire axi_interconnect_2_M06_AXI_WVALID;
  wire [31:0]axi_interconnect_3_M00_AXI_ARADDR;
  wire [1:0]axi_interconnect_3_M00_AXI_ARBURST;
  wire [3:0]axi_interconnect_3_M00_AXI_ARCACHE;
  wire [5:0]axi_interconnect_3_M00_AXI_ARID;
  wire [7:0]axi_interconnect_3_M00_AXI_ARLEN;
  wire [0:0]axi_interconnect_3_M00_AXI_ARLOCK;
  wire [2:0]axi_interconnect_3_M00_AXI_ARPROT;
  wire [3:0]axi_interconnect_3_M00_AXI_ARQOS;
  wire axi_interconnect_3_M00_AXI_ARREADY;
  wire [3:0]axi_interconnect_3_M00_AXI_ARREGION;
  wire [2:0]axi_interconnect_3_M00_AXI_ARSIZE;
  wire [3:0]axi_interconnect_3_M00_AXI_ARUSER;
  wire axi_interconnect_3_M00_AXI_ARVALID;
  wire [31:0]axi_interconnect_3_M00_AXI_AWADDR;
  wire [1:0]axi_interconnect_3_M00_AXI_AWBURST;
  wire [3:0]axi_interconnect_3_M00_AXI_AWCACHE;
  wire [5:0]axi_interconnect_3_M00_AXI_AWID;
  wire [7:0]axi_interconnect_3_M00_AXI_AWLEN;
  wire [0:0]axi_interconnect_3_M00_AXI_AWLOCK;
  wire [2:0]axi_interconnect_3_M00_AXI_AWPROT;
  wire [3:0]axi_interconnect_3_M00_AXI_AWQOS;
  wire axi_interconnect_3_M00_AXI_AWREADY;
  wire [3:0]axi_interconnect_3_M00_AXI_AWREGION;
  wire [2:0]axi_interconnect_3_M00_AXI_AWSIZE;
  wire [3:0]axi_interconnect_3_M00_AXI_AWUSER;
  wire axi_interconnect_3_M00_AXI_AWVALID;
  wire [5:0]axi_interconnect_3_M00_AXI_BID;
  wire axi_interconnect_3_M00_AXI_BREADY;
  wire [1:0]axi_interconnect_3_M00_AXI_BRESP;
  wire [3:0]axi_interconnect_3_M00_AXI_BUSER;
  wire axi_interconnect_3_M00_AXI_BVALID;
  wire [31:0]axi_interconnect_3_M00_AXI_RDATA;
  wire [5:0]axi_interconnect_3_M00_AXI_RID;
  wire axi_interconnect_3_M00_AXI_RLAST;
  wire axi_interconnect_3_M00_AXI_RREADY;
  wire [1:0]axi_interconnect_3_M00_AXI_RRESP;
  wire [3:0]axi_interconnect_3_M00_AXI_RUSER;
  wire axi_interconnect_3_M00_AXI_RVALID;
  wire [31:0]axi_interconnect_3_M00_AXI_WDATA;
  wire axi_interconnect_3_M00_AXI_WLAST;
  wire axi_interconnect_3_M00_AXI_WREADY;
  wire [3:0]axi_interconnect_3_M00_AXI_WSTRB;
  wire [3:0]axi_interconnect_3_M00_AXI_WUSER;
  wire axi_interconnect_3_M00_AXI_WVALID;
  wire clk_0_1;
  wire [0:0]gpio_io_i_0_1;
  wire [2:0]gpio_io_i_0_2;
  wire [31:0]gpio_io_i_0_3;
  wire [31:0]gpn0_local_memory_bus_ARADDR;
  wire [1:0]gpn0_local_memory_bus_ARBURST;
  wire [3:0]gpn0_local_memory_bus_ARCACHE;
  wire [5:0]gpn0_local_memory_bus_ARID;
  wire [7:0]gpn0_local_memory_bus_ARLEN;
  wire [0:0]gpn0_local_memory_bus_ARLOCK;
  wire [2:0]gpn0_local_memory_bus_ARPROT;
  wire [3:0]gpn0_local_memory_bus_ARQOS;
  wire [0:0]gpn0_local_memory_bus_ARREADY;
  wire [3:0]gpn0_local_memory_bus_ARREGION;
  wire [2:0]gpn0_local_memory_bus_ARSIZE;
  wire [3:0]gpn0_local_memory_bus_ARUSER;
  wire [0:0]gpn0_local_memory_bus_ARVALID;
  wire [31:0]gpn0_local_memory_bus_AWADDR;
  wire [1:0]gpn0_local_memory_bus_AWBURST;
  wire [3:0]gpn0_local_memory_bus_AWCACHE;
  wire [5:0]gpn0_local_memory_bus_AWID;
  wire [7:0]gpn0_local_memory_bus_AWLEN;
  wire [0:0]gpn0_local_memory_bus_AWLOCK;
  wire [2:0]gpn0_local_memory_bus_AWPROT;
  wire [3:0]gpn0_local_memory_bus_AWQOS;
  wire [0:0]gpn0_local_memory_bus_AWREADY;
  wire [3:0]gpn0_local_memory_bus_AWREGION;
  wire [2:0]gpn0_local_memory_bus_AWSIZE;
  wire [3:0]gpn0_local_memory_bus_AWUSER;
  wire [0:0]gpn0_local_memory_bus_AWVALID;
  wire [5:0]gpn0_local_memory_bus_BID;
  wire [0:0]gpn0_local_memory_bus_BREADY;
  wire [1:0]gpn0_local_memory_bus_BRESP;
  wire [3:0]gpn0_local_memory_bus_BUSER;
  wire [0:0]gpn0_local_memory_bus_BVALID;
  wire [31:0]gpn0_local_memory_bus_RDATA;
  wire [5:0]gpn0_local_memory_bus_RID;
  wire [0:0]gpn0_local_memory_bus_RLAST;
  wire [0:0]gpn0_local_memory_bus_RREADY;
  wire [1:0]gpn0_local_memory_bus_RRESP;
  wire [3:0]gpn0_local_memory_bus_RUSER;
  wire [0:0]gpn0_local_memory_bus_RVALID;
  wire [31:0]gpn0_local_memory_bus_WDATA;
  wire [0:0]gpn0_local_memory_bus_WLAST;
  wire [0:0]gpn0_local_memory_bus_WREADY;
  wire [3:0]gpn0_local_memory_bus_WSTRB;
  wire [3:0]gpn0_local_memory_bus_WUSER;
  wire [0:0]gpn0_local_memory_bus_WVALID;
  wire [31:0]gpn_control_reg_1;
  wire gpn_reset_1;
  wire kernel_rstn_1;
  wire [1:0]mq_st_gpio2_io_o;
  wire [0:0]proc_sys_reset_0_peripheral_aresetn;
  wire [0:0]proc_sys_reset_0_peripheral_reset;
  wire [29:0]taiga_wrapper_xilinx_0_data_bram_addr;
  wire [3:0]taiga_wrapper_xilinx_0_data_bram_be;
  wire [31:0]taiga_wrapper_xilinx_0_data_bram_data_in;
  wire [31:0]taiga_wrapper_xilinx_0_data_bram_data_out;
  wire taiga_wrapper_xilinx_0_data_bram_en;
  wire [29:0]taiga_wrapper_xilinx_0_instruction_bram_addr;
  wire [3:0]taiga_wrapper_xilinx_0_instruction_bram_be;
  wire [31:0]taiga_wrapper_xilinx_0_instruction_bram_data_in;
  wire [31:0]taiga_wrapper_xilinx_0_instruction_bram_data_out;
  wire taiga_wrapper_xilinx_0_instruction_bram_en;
  wire [29:0]taiga_wrapper_xilinx_0_instruction_rom_addr;
  wire [3:0]taiga_wrapper_xilinx_0_instruction_rom_be;
  wire [31:0]taiga_wrapper_xilinx_0_instruction_rom_data_in;
  wire [31:0]taiga_wrapper_xilinx_0_instruction_rom_data_out;
  wire taiga_wrapper_xilinx_0_instruction_rom_en;
  wire [31:0]taiga_wrapper_xilinx_0_m_axi_ARADDR;
  wire [1:0]taiga_wrapper_xilinx_0_m_axi_ARBURST;
  wire [3:0]taiga_wrapper_xilinx_0_m_axi_ARCACHE;
  wire [5:0]taiga_wrapper_xilinx_0_m_axi_ARID;
  wire [7:0]taiga_wrapper_xilinx_0_m_axi_ARLEN;
  wire taiga_wrapper_xilinx_0_m_axi_ARLOCK;
  wire [2:0]taiga_wrapper_xilinx_0_m_axi_ARPROT;
  wire [3:0]taiga_wrapper_xilinx_0_m_axi_ARQOS;
  wire taiga_wrapper_xilinx_0_m_axi_ARREADY;
  wire [2:0]taiga_wrapper_xilinx_0_m_axi_ARSIZE;
  wire [3:0]taiga_wrapper_xilinx_0_m_axi_ARUSER;
  wire taiga_wrapper_xilinx_0_m_axi_ARVALID;
  wire [31:0]taiga_wrapper_xilinx_0_m_axi_AWADDR;
  wire [1:0]taiga_wrapper_xilinx_0_m_axi_AWBURST;
  wire [3:0]taiga_wrapper_xilinx_0_m_axi_AWCACHE;
  wire [5:0]taiga_wrapper_xilinx_0_m_axi_AWID;
  wire [7:0]taiga_wrapper_xilinx_0_m_axi_AWLEN;
  wire taiga_wrapper_xilinx_0_m_axi_AWLOCK;
  wire [2:0]taiga_wrapper_xilinx_0_m_axi_AWPROT;
  wire [3:0]taiga_wrapper_xilinx_0_m_axi_AWQOS;
  wire taiga_wrapper_xilinx_0_m_axi_AWREADY;
  wire [2:0]taiga_wrapper_xilinx_0_m_axi_AWSIZE;
  wire [3:0]taiga_wrapper_xilinx_0_m_axi_AWUSER;
  wire taiga_wrapper_xilinx_0_m_axi_AWVALID;
  wire [5:0]taiga_wrapper_xilinx_0_m_axi_BID;
  wire taiga_wrapper_xilinx_0_m_axi_BREADY;
  wire [1:0]taiga_wrapper_xilinx_0_m_axi_BRESP;
  wire [3:0]taiga_wrapper_xilinx_0_m_axi_BUSER;
  wire taiga_wrapper_xilinx_0_m_axi_BVALID;
  wire [31:0]taiga_wrapper_xilinx_0_m_axi_RDATA;
  wire [5:0]taiga_wrapper_xilinx_0_m_axi_RID;
  wire taiga_wrapper_xilinx_0_m_axi_RLAST;
  wire taiga_wrapper_xilinx_0_m_axi_RREADY;
  wire [1:0]taiga_wrapper_xilinx_0_m_axi_RRESP;
  wire [3:0]taiga_wrapper_xilinx_0_m_axi_RUSER;
  wire taiga_wrapper_xilinx_0_m_axi_RVALID;
  wire [31:0]taiga_wrapper_xilinx_0_m_axi_WDATA;
  wire taiga_wrapper_xilinx_0_m_axi_WLAST;
  wire taiga_wrapper_xilinx_0_m_axi_WREADY;
  wire [3:0]taiga_wrapper_xilinx_0_m_axi_WSTRB;
  wire [3:0]taiga_wrapper_xilinx_0_m_axi_WUSER;
  wire taiga_wrapper_xilinx_0_m_axi_WVALID;

  assign M00_ACLK_0_1 = ext_clk;
  assign M00_ARESETN_0_1 = ext_rstn;
  assign axi_interconnect_1_M00_AXI_ARREADY = global_memory_bus_arready;
  assign axi_interconnect_1_M00_AXI_AWREADY = global_memory_bus_awready;
  assign axi_interconnect_1_M00_AXI_BID = global_memory_bus_bid[5:0];
  assign axi_interconnect_1_M00_AXI_BRESP = global_memory_bus_bresp[1:0];
  assign axi_interconnect_1_M00_AXI_BUSER = global_memory_bus_buser[3:0];
  assign axi_interconnect_1_M00_AXI_BVALID = global_memory_bus_bvalid;
  assign axi_interconnect_1_M00_AXI_RDATA = global_memory_bus_rdata[31:0];
  assign axi_interconnect_1_M00_AXI_RID = global_memory_bus_rid[5:0];
  assign axi_interconnect_1_M00_AXI_RLAST = global_memory_bus_rlast;
  assign axi_interconnect_1_M00_AXI_RRESP = global_memory_bus_rresp[1:0];
  assign axi_interconnect_1_M00_AXI_RUSER = global_memory_bus_ruser[3:0];
  assign axi_interconnect_1_M00_AXI_RVALID = global_memory_bus_rvalid;
  assign axi_interconnect_1_M00_AXI_WREADY = global_memory_bus_wready;
  assign axi_interconnect_3_M00_AXI_ARREADY = external_memory_bus_arready;
  assign axi_interconnect_3_M00_AXI_AWREADY = external_memory_bus_awready;
  assign axi_interconnect_3_M00_AXI_BID = external_memory_bus_bid[5:0];
  assign axi_interconnect_3_M00_AXI_BRESP = external_memory_bus_bresp[1:0];
  assign axi_interconnect_3_M00_AXI_BUSER = external_memory_bus_buser[3:0];
  assign axi_interconnect_3_M00_AXI_BVALID = external_memory_bus_bvalid;
  assign axi_interconnect_3_M00_AXI_RDATA = external_memory_bus_rdata[31:0];
  assign axi_interconnect_3_M00_AXI_RID = external_memory_bus_rid[5:0];
  assign axi_interconnect_3_M00_AXI_RLAST = external_memory_bus_rlast;
  assign axi_interconnect_3_M00_AXI_RRESP = external_memory_bus_rresp[1:0];
  assign axi_interconnect_3_M00_AXI_RUSER = external_memory_bus_ruser[3:0];
  assign axi_interconnect_3_M00_AXI_RVALID = external_memory_bus_rvalid;
  assign axi_interconnect_3_M00_AXI_WREADY = external_memory_bus_wready;
  assign clk_0_1 = kernel_clk;
  assign external_memory_bus_araddr[31:0] = axi_interconnect_3_M00_AXI_ARADDR;
  assign external_memory_bus_arburst[1:0] = axi_interconnect_3_M00_AXI_ARBURST;
  assign external_memory_bus_arcache[3:0] = axi_interconnect_3_M00_AXI_ARCACHE;
  assign external_memory_bus_arid[5:0] = axi_interconnect_3_M00_AXI_ARID;
  assign external_memory_bus_arlen[7:0] = axi_interconnect_3_M00_AXI_ARLEN;
  assign external_memory_bus_arlock[0] = axi_interconnect_3_M00_AXI_ARLOCK;
  assign external_memory_bus_arprot[2:0] = axi_interconnect_3_M00_AXI_ARPROT;
  assign external_memory_bus_arqos[3:0] = axi_interconnect_3_M00_AXI_ARQOS;
  assign external_memory_bus_arregion[3:0] = axi_interconnect_3_M00_AXI_ARREGION;
  assign external_memory_bus_arsize[2:0] = axi_interconnect_3_M00_AXI_ARSIZE;
  assign external_memory_bus_aruser[3:0] = axi_interconnect_3_M00_AXI_ARUSER;
  assign external_memory_bus_arvalid = axi_interconnect_3_M00_AXI_ARVALID;
  assign external_memory_bus_awaddr[31:0] = axi_interconnect_3_M00_AXI_AWADDR;
  assign external_memory_bus_awburst[1:0] = axi_interconnect_3_M00_AXI_AWBURST;
  assign external_memory_bus_awcache[3:0] = axi_interconnect_3_M00_AXI_AWCACHE;
  assign external_memory_bus_awid[5:0] = axi_interconnect_3_M00_AXI_AWID;
  assign external_memory_bus_awlen[7:0] = axi_interconnect_3_M00_AXI_AWLEN;
  assign external_memory_bus_awlock[0] = axi_interconnect_3_M00_AXI_AWLOCK;
  assign external_memory_bus_awprot[2:0] = axi_interconnect_3_M00_AXI_AWPROT;
  assign external_memory_bus_awqos[3:0] = axi_interconnect_3_M00_AXI_AWQOS;
  assign external_memory_bus_awregion[3:0] = axi_interconnect_3_M00_AXI_AWREGION;
  assign external_memory_bus_awsize[2:0] = axi_interconnect_3_M00_AXI_AWSIZE;
  assign external_memory_bus_awuser[3:0] = axi_interconnect_3_M00_AXI_AWUSER;
  assign external_memory_bus_awvalid = axi_interconnect_3_M00_AXI_AWVALID;
  assign external_memory_bus_bready = axi_interconnect_3_M00_AXI_BREADY;
  assign external_memory_bus_rready = axi_interconnect_3_M00_AXI_RREADY;
  assign external_memory_bus_wdata[31:0] = axi_interconnect_3_M00_AXI_WDATA;
  assign external_memory_bus_wlast = axi_interconnect_3_M00_AXI_WLAST;
  assign external_memory_bus_wstrb[3:0] = axi_interconnect_3_M00_AXI_WSTRB;
  assign external_memory_bus_wuser[3:0] = axi_interconnect_3_M00_AXI_WUSER;
  assign external_memory_bus_wvalid = axi_interconnect_3_M00_AXI_WVALID;
  assign global_memory_bus_araddr[31:0] = axi_interconnect_1_M00_AXI_ARADDR;
  assign global_memory_bus_arburst[1:0] = axi_interconnect_1_M00_AXI_ARBURST;
  assign global_memory_bus_arcache[3:0] = axi_interconnect_1_M00_AXI_ARCACHE;
  assign global_memory_bus_arid[5:0] = axi_interconnect_1_M00_AXI_ARID;
  assign global_memory_bus_arlen[7:0] = axi_interconnect_1_M00_AXI_ARLEN;
  assign global_memory_bus_arlock[0] = axi_interconnect_1_M00_AXI_ARLOCK;
  assign global_memory_bus_arprot[2:0] = axi_interconnect_1_M00_AXI_ARPROT;
  assign global_memory_bus_arqos[3:0] = axi_interconnect_1_M00_AXI_ARQOS;
  assign global_memory_bus_arregion[3:0] = axi_interconnect_1_M00_AXI_ARREGION;
  assign global_memory_bus_arsize[2:0] = axi_interconnect_1_M00_AXI_ARSIZE;
  assign global_memory_bus_aruser[3:0] = axi_interconnect_1_M00_AXI_ARUSER;
  assign global_memory_bus_arvalid = axi_interconnect_1_M00_AXI_ARVALID;
  assign global_memory_bus_awaddr[31:0] = axi_interconnect_1_M00_AXI_AWADDR;
  assign global_memory_bus_awburst[1:0] = axi_interconnect_1_M00_AXI_AWBURST;
  assign global_memory_bus_awcache[3:0] = axi_interconnect_1_M00_AXI_AWCACHE;
  assign global_memory_bus_awid[5:0] = axi_interconnect_1_M00_AXI_AWID;
  assign global_memory_bus_awlen[7:0] = axi_interconnect_1_M00_AXI_AWLEN;
  assign global_memory_bus_awlock[0] = axi_interconnect_1_M00_AXI_AWLOCK;
  assign global_memory_bus_awprot[2:0] = axi_interconnect_1_M00_AXI_AWPROT;
  assign global_memory_bus_awqos[3:0] = axi_interconnect_1_M00_AXI_AWQOS;
  assign global_memory_bus_awregion[3:0] = axi_interconnect_1_M00_AXI_AWREGION;
  assign global_memory_bus_awsize[2:0] = axi_interconnect_1_M00_AXI_AWSIZE;
  assign global_memory_bus_awuser[3:0] = axi_interconnect_1_M00_AXI_AWUSER;
  assign global_memory_bus_awvalid = axi_interconnect_1_M00_AXI_AWVALID;
  assign global_memory_bus_bready = axi_interconnect_1_M00_AXI_BREADY;
  assign global_memory_bus_rready = axi_interconnect_1_M00_AXI_RREADY;
  assign global_memory_bus_wdata[31:0] = axi_interconnect_1_M00_AXI_WDATA;
  assign global_memory_bus_wlast = axi_interconnect_1_M00_AXI_WLAST;
  assign global_memory_bus_wstrb[3:0] = axi_interconnect_1_M00_AXI_WSTRB;
  assign global_memory_bus_wuser[3:0] = axi_interconnect_1_M00_AXI_WUSER;
  assign global_memory_bus_wvalid = axi_interconnect_1_M00_AXI_WVALID;
  assign gpc2host_mq[31:0] = axi_gpio_2_gpio_io_o;
  assign gpc_state_reg[0] = axi_gpio_1_gpio_io_o;
  assign gpio_io_i_0_1 = gpc_start[0];
  assign gpio_io_i_0_2 = mq_sr[2:0];
  assign gpio_io_i_0_3 = host2gpc_mq[31:0];
  assign gpn_control_reg_1 = gpc_control_reg[31:0];
  assign gpn_reset_1 = gpc_reset;
  assign kernel_rstn_1 = kernel_rstn;
  assign mq_ctrl[1:0] = mq_st_gpio2_io_o;
  gpn_bundle_block_axi_interconnect_0_3 axi_interconnect_1
       (.ACLK(clk_0_1),
        .ARESETN(kernel_rstn_1),
        .M00_ACLK(M00_ACLK_0_1),
        .M00_ARESETN(M00_ARESETN_0_1),
        .M00_AXI_araddr(axi_interconnect_1_M00_AXI_ARADDR),
        .M00_AXI_arburst(axi_interconnect_1_M00_AXI_ARBURST),
        .M00_AXI_arcache(axi_interconnect_1_M00_AXI_ARCACHE),
        .M00_AXI_arid(axi_interconnect_1_M00_AXI_ARID),
        .M00_AXI_arlen(axi_interconnect_1_M00_AXI_ARLEN),
        .M00_AXI_arlock(axi_interconnect_1_M00_AXI_ARLOCK),
        .M00_AXI_arprot(axi_interconnect_1_M00_AXI_ARPROT),
        .M00_AXI_arqos(axi_interconnect_1_M00_AXI_ARQOS),
        .M00_AXI_arready(axi_interconnect_1_M00_AXI_ARREADY),
        .M00_AXI_arregion(axi_interconnect_1_M00_AXI_ARREGION),
        .M00_AXI_arsize(axi_interconnect_1_M00_AXI_ARSIZE),
        .M00_AXI_aruser(axi_interconnect_1_M00_AXI_ARUSER),
        .M00_AXI_arvalid(axi_interconnect_1_M00_AXI_ARVALID),
        .M00_AXI_awaddr(axi_interconnect_1_M00_AXI_AWADDR),
        .M00_AXI_awburst(axi_interconnect_1_M00_AXI_AWBURST),
        .M00_AXI_awcache(axi_interconnect_1_M00_AXI_AWCACHE),
        .M00_AXI_awid(axi_interconnect_1_M00_AXI_AWID),
        .M00_AXI_awlen(axi_interconnect_1_M00_AXI_AWLEN),
        .M00_AXI_awlock(axi_interconnect_1_M00_AXI_AWLOCK),
        .M00_AXI_awprot(axi_interconnect_1_M00_AXI_AWPROT),
        .M00_AXI_awqos(axi_interconnect_1_M00_AXI_AWQOS),
        .M00_AXI_awready(axi_interconnect_1_M00_AXI_AWREADY),
        .M00_AXI_awregion(axi_interconnect_1_M00_AXI_AWREGION),
        .M00_AXI_awsize(axi_interconnect_1_M00_AXI_AWSIZE),
        .M00_AXI_awuser(axi_interconnect_1_M00_AXI_AWUSER),
        .M00_AXI_awvalid(axi_interconnect_1_M00_AXI_AWVALID),
        .M00_AXI_bid(axi_interconnect_1_M00_AXI_BID),
        .M00_AXI_bready(axi_interconnect_1_M00_AXI_BREADY),
        .M00_AXI_bresp(axi_interconnect_1_M00_AXI_BRESP),
        .M00_AXI_buser(axi_interconnect_1_M00_AXI_BUSER),
        .M00_AXI_bvalid(axi_interconnect_1_M00_AXI_BVALID),
        .M00_AXI_rdata(axi_interconnect_1_M00_AXI_RDATA),
        .M00_AXI_rid(axi_interconnect_1_M00_AXI_RID),
        .M00_AXI_rlast(axi_interconnect_1_M00_AXI_RLAST),
        .M00_AXI_rready(axi_interconnect_1_M00_AXI_RREADY),
        .M00_AXI_rresp(axi_interconnect_1_M00_AXI_RRESP),
        .M00_AXI_ruser(axi_interconnect_1_M00_AXI_RUSER),
        .M00_AXI_rvalid(axi_interconnect_1_M00_AXI_RVALID),
        .M00_AXI_wdata(axi_interconnect_1_M00_AXI_WDATA),
        .M00_AXI_wlast(axi_interconnect_1_M00_AXI_WLAST),
        .M00_AXI_wready(axi_interconnect_1_M00_AXI_WREADY),
        .M00_AXI_wstrb(axi_interconnect_1_M00_AXI_WSTRB),
        .M00_AXI_wuser(axi_interconnect_1_M00_AXI_WUSER),
        .M00_AXI_wvalid(axi_interconnect_1_M00_AXI_WVALID),
        .S00_ACLK(clk_0_1),
        .S00_ARESETN(kernel_rstn_1),
        .S00_AXI_araddr(gpn0_local_memory_bus_ARADDR),
        .S00_AXI_arburst(gpn0_local_memory_bus_ARBURST),
        .S00_AXI_arcache(gpn0_local_memory_bus_ARCACHE),
        .S00_AXI_arid(gpn0_local_memory_bus_ARID),
        .S00_AXI_arlen(gpn0_local_memory_bus_ARLEN),
        .S00_AXI_arlock(gpn0_local_memory_bus_ARLOCK),
        .S00_AXI_arprot(gpn0_local_memory_bus_ARPROT),
        .S00_AXI_arqos(gpn0_local_memory_bus_ARQOS),
        .S00_AXI_arready(gpn0_local_memory_bus_ARREADY),
        .S00_AXI_arregion(gpn0_local_memory_bus_ARREGION),
        .S00_AXI_arsize(gpn0_local_memory_bus_ARSIZE),
        .S00_AXI_aruser(gpn0_local_memory_bus_ARUSER),
        .S00_AXI_arvalid(gpn0_local_memory_bus_ARVALID),
        .S00_AXI_awaddr(gpn0_local_memory_bus_AWADDR),
        .S00_AXI_awburst(gpn0_local_memory_bus_AWBURST),
        .S00_AXI_awcache(gpn0_local_memory_bus_AWCACHE),
        .S00_AXI_awid(gpn0_local_memory_bus_AWID),
        .S00_AXI_awlen(gpn0_local_memory_bus_AWLEN),
        .S00_AXI_awlock(gpn0_local_memory_bus_AWLOCK),
        .S00_AXI_awprot(gpn0_local_memory_bus_AWPROT),
        .S00_AXI_awqos(gpn0_local_memory_bus_AWQOS),
        .S00_AXI_awready(gpn0_local_memory_bus_AWREADY),
        .S00_AXI_awregion(gpn0_local_memory_bus_AWREGION),
        .S00_AXI_awsize(gpn0_local_memory_bus_AWSIZE),
        .S00_AXI_awuser(gpn0_local_memory_bus_AWUSER),
        .S00_AXI_awvalid(gpn0_local_memory_bus_AWVALID),
        .S00_AXI_bid(gpn0_local_memory_bus_BID),
        .S00_AXI_bready(gpn0_local_memory_bus_BREADY),
        .S00_AXI_bresp(gpn0_local_memory_bus_BRESP),
        .S00_AXI_buser(gpn0_local_memory_bus_BUSER),
        .S00_AXI_bvalid(gpn0_local_memory_bus_BVALID),
        .S00_AXI_rdata(gpn0_local_memory_bus_RDATA),
        .S00_AXI_rid(gpn0_local_memory_bus_RID),
        .S00_AXI_rlast(gpn0_local_memory_bus_RLAST),
        .S00_AXI_rready(gpn0_local_memory_bus_RREADY),
        .S00_AXI_rresp(gpn0_local_memory_bus_RRESP),
        .S00_AXI_ruser(gpn0_local_memory_bus_RUSER),
        .S00_AXI_rvalid(gpn0_local_memory_bus_RVALID),
        .S00_AXI_wdata(gpn0_local_memory_bus_WDATA),
        .S00_AXI_wlast(gpn0_local_memory_bus_WLAST),
        .S00_AXI_wready(gpn0_local_memory_bus_WREADY),
        .S00_AXI_wstrb(gpn0_local_memory_bus_WSTRB),
        .S00_AXI_wuser(gpn0_local_memory_bus_WUSER),
        .S00_AXI_wvalid(gpn0_local_memory_bus_WVALID));
  gpn_bundle_block_axi_interconnect_0_1 axi_interconnect_2
       (.ACLK(clk_0_1),
        .ARESETN(kernel_rstn_1),
        .M00_ACLK(clk_0_1),
        .M00_ARESETN(kernel_rstn_1),
        .M00_AXI_araddr(gpn0_local_memory_bus_ARADDR),
        .M00_AXI_arburst(gpn0_local_memory_bus_ARBURST),
        .M00_AXI_arcache(gpn0_local_memory_bus_ARCACHE),
        .M00_AXI_arid(gpn0_local_memory_bus_ARID),
        .M00_AXI_arlen(gpn0_local_memory_bus_ARLEN),
        .M00_AXI_arlock(gpn0_local_memory_bus_ARLOCK),
        .M00_AXI_arprot(gpn0_local_memory_bus_ARPROT),
        .M00_AXI_arqos(gpn0_local_memory_bus_ARQOS),
        .M00_AXI_arready(gpn0_local_memory_bus_ARREADY),
        .M00_AXI_arregion(gpn0_local_memory_bus_ARREGION),
        .M00_AXI_arsize(gpn0_local_memory_bus_ARSIZE),
        .M00_AXI_aruser(gpn0_local_memory_bus_ARUSER),
        .M00_AXI_arvalid(gpn0_local_memory_bus_ARVALID),
        .M00_AXI_awaddr(gpn0_local_memory_bus_AWADDR),
        .M00_AXI_awburst(gpn0_local_memory_bus_AWBURST),
        .M00_AXI_awcache(gpn0_local_memory_bus_AWCACHE),
        .M00_AXI_awid(gpn0_local_memory_bus_AWID),
        .M00_AXI_awlen(gpn0_local_memory_bus_AWLEN),
        .M00_AXI_awlock(gpn0_local_memory_bus_AWLOCK),
        .M00_AXI_awprot(gpn0_local_memory_bus_AWPROT),
        .M00_AXI_awqos(gpn0_local_memory_bus_AWQOS),
        .M00_AXI_awready(gpn0_local_memory_bus_AWREADY),
        .M00_AXI_awregion(gpn0_local_memory_bus_AWREGION),
        .M00_AXI_awsize(gpn0_local_memory_bus_AWSIZE),
        .M00_AXI_awuser(gpn0_local_memory_bus_AWUSER),
        .M00_AXI_awvalid(gpn0_local_memory_bus_AWVALID),
        .M00_AXI_bid(gpn0_local_memory_bus_BID),
        .M00_AXI_bready(gpn0_local_memory_bus_BREADY),
        .M00_AXI_bresp(gpn0_local_memory_bus_BRESP),
        .M00_AXI_buser(gpn0_local_memory_bus_BUSER),
        .M00_AXI_bvalid(gpn0_local_memory_bus_BVALID),
        .M00_AXI_rdata(gpn0_local_memory_bus_RDATA),
        .M00_AXI_rid(gpn0_local_memory_bus_RID),
        .M00_AXI_rlast(gpn0_local_memory_bus_RLAST),
        .M00_AXI_rready(gpn0_local_memory_bus_RREADY),
        .M00_AXI_rresp(gpn0_local_memory_bus_RRESP),
        .M00_AXI_ruser(gpn0_local_memory_bus_RUSER),
        .M00_AXI_rvalid(gpn0_local_memory_bus_RVALID),
        .M00_AXI_wdata(gpn0_local_memory_bus_WDATA),
        .M00_AXI_wlast(gpn0_local_memory_bus_WLAST),
        .M00_AXI_wready(gpn0_local_memory_bus_WREADY),
        .M00_AXI_wstrb(gpn0_local_memory_bus_WSTRB),
        .M00_AXI_wuser(gpn0_local_memory_bus_WUSER),
        .M00_AXI_wvalid(gpn0_local_memory_bus_WVALID),
        .M01_ACLK(clk_0_1),
        .M01_ARESETN(kernel_rstn_1),
        .M01_AXI_araddr(axi_interconnect_0_M01_AXI_ARADDR),
        .M01_AXI_arready(axi_interconnect_0_M01_AXI_ARREADY),
        .M01_AXI_arvalid(axi_interconnect_0_M01_AXI_ARVALID),
        .M01_AXI_awaddr(axi_interconnect_0_M01_AXI_AWADDR),
        .M01_AXI_awready(axi_interconnect_0_M01_AXI_AWREADY),
        .M01_AXI_awvalid(axi_interconnect_0_M01_AXI_AWVALID),
        .M01_AXI_bready(axi_interconnect_0_M01_AXI_BREADY),
        .M01_AXI_bresp(axi_interconnect_0_M01_AXI_BRESP),
        .M01_AXI_bvalid(axi_interconnect_0_M01_AXI_BVALID),
        .M01_AXI_rdata(axi_interconnect_0_M01_AXI_RDATA),
        .M01_AXI_rready(axi_interconnect_0_M01_AXI_RREADY),
        .M01_AXI_rresp(axi_interconnect_0_M01_AXI_RRESP),
        .M01_AXI_rvalid(axi_interconnect_0_M01_AXI_RVALID),
        .M01_AXI_wdata(axi_interconnect_0_M01_AXI_WDATA),
        .M01_AXI_wready(axi_interconnect_0_M01_AXI_WREADY),
        .M01_AXI_wstrb(axi_interconnect_0_M01_AXI_WSTRB),
        .M01_AXI_wvalid(axi_interconnect_0_M01_AXI_WVALID),
        .M02_ACLK(clk_0_1),
        .M02_ARESETN(kernel_rstn_1),
        .M02_AXI_araddr(axi_interconnect_0_M02_AXI_ARADDR),
        .M02_AXI_arready(axi_interconnect_0_M02_AXI_ARREADY),
        .M02_AXI_arvalid(axi_interconnect_0_M02_AXI_ARVALID),
        .M02_AXI_awaddr(axi_interconnect_0_M02_AXI_AWADDR),
        .M02_AXI_awready(axi_interconnect_0_M02_AXI_AWREADY),
        .M02_AXI_awvalid(axi_interconnect_0_M02_AXI_AWVALID),
        .M02_AXI_bready(axi_interconnect_0_M02_AXI_BREADY),
        .M02_AXI_bresp(axi_interconnect_0_M02_AXI_BRESP),
        .M02_AXI_bvalid(axi_interconnect_0_M02_AXI_BVALID),
        .M02_AXI_rdata(axi_interconnect_0_M02_AXI_RDATA),
        .M02_AXI_rready(axi_interconnect_0_M02_AXI_RREADY),
        .M02_AXI_rresp(axi_interconnect_0_M02_AXI_RRESP),
        .M02_AXI_rvalid(axi_interconnect_0_M02_AXI_RVALID),
        .M02_AXI_wdata(axi_interconnect_0_M02_AXI_WDATA),
        .M02_AXI_wready(axi_interconnect_0_M02_AXI_WREADY),
        .M02_AXI_wstrb(axi_interconnect_0_M02_AXI_WSTRB),
        .M02_AXI_wvalid(axi_interconnect_0_M02_AXI_WVALID),
        .M03_ACLK(clk_0_1),
        .M03_ARESETN(kernel_rstn_1),
        .M03_AXI_araddr(axi_interconnect_2_M03_AXI_ARADDR),
        .M03_AXI_arburst(axi_interconnect_2_M03_AXI_ARBURST),
        .M03_AXI_arcache(axi_interconnect_2_M03_AXI_ARCACHE),
        .M03_AXI_arid(axi_interconnect_2_M03_AXI_ARID),
        .M03_AXI_arlen(axi_interconnect_2_M03_AXI_ARLEN),
        .M03_AXI_arlock(axi_interconnect_2_M03_AXI_ARLOCK),
        .M03_AXI_arprot(axi_interconnect_2_M03_AXI_ARPROT),
        .M03_AXI_arqos(axi_interconnect_2_M03_AXI_ARQOS),
        .M03_AXI_arready(axi_interconnect_2_M03_AXI_ARREADY),
        .M03_AXI_arregion(axi_interconnect_2_M03_AXI_ARREGION),
        .M03_AXI_arsize(axi_interconnect_2_M03_AXI_ARSIZE),
        .M03_AXI_aruser(axi_interconnect_2_M03_AXI_ARUSER),
        .M03_AXI_arvalid(axi_interconnect_2_M03_AXI_ARVALID),
        .M03_AXI_awaddr(axi_interconnect_2_M03_AXI_AWADDR),
        .M03_AXI_awburst(axi_interconnect_2_M03_AXI_AWBURST),
        .M03_AXI_awcache(axi_interconnect_2_M03_AXI_AWCACHE),
        .M03_AXI_awid(axi_interconnect_2_M03_AXI_AWID),
        .M03_AXI_awlen(axi_interconnect_2_M03_AXI_AWLEN),
        .M03_AXI_awlock(axi_interconnect_2_M03_AXI_AWLOCK),
        .M03_AXI_awprot(axi_interconnect_2_M03_AXI_AWPROT),
        .M03_AXI_awqos(axi_interconnect_2_M03_AXI_AWQOS),
        .M03_AXI_awready(axi_interconnect_2_M03_AXI_AWREADY),
        .M03_AXI_awregion(axi_interconnect_2_M03_AXI_AWREGION),
        .M03_AXI_awsize(axi_interconnect_2_M03_AXI_AWSIZE),
        .M03_AXI_awuser(axi_interconnect_2_M03_AXI_AWUSER),
        .M03_AXI_awvalid(axi_interconnect_2_M03_AXI_AWVALID),
        .M03_AXI_bid(axi_interconnect_2_M03_AXI_BID),
        .M03_AXI_bready(axi_interconnect_2_M03_AXI_BREADY),
        .M03_AXI_bresp(axi_interconnect_2_M03_AXI_BRESP),
        .M03_AXI_buser(axi_interconnect_2_M03_AXI_BUSER),
        .M03_AXI_bvalid(axi_interconnect_2_M03_AXI_BVALID),
        .M03_AXI_rdata(axi_interconnect_2_M03_AXI_RDATA),
        .M03_AXI_rid(axi_interconnect_2_M03_AXI_RID),
        .M03_AXI_rlast(axi_interconnect_2_M03_AXI_RLAST),
        .M03_AXI_rready(axi_interconnect_2_M03_AXI_RREADY),
        .M03_AXI_rresp(axi_interconnect_2_M03_AXI_RRESP),
        .M03_AXI_ruser(axi_interconnect_2_M03_AXI_RUSER),
        .M03_AXI_rvalid(axi_interconnect_2_M03_AXI_RVALID),
        .M03_AXI_wdata(axi_interconnect_2_M03_AXI_WDATA),
        .M03_AXI_wlast(axi_interconnect_2_M03_AXI_WLAST),
        .M03_AXI_wready(axi_interconnect_2_M03_AXI_WREADY),
        .M03_AXI_wstrb(axi_interconnect_2_M03_AXI_WSTRB),
        .M03_AXI_wuser(axi_interconnect_2_M03_AXI_WUSER),
        .M03_AXI_wvalid(axi_interconnect_2_M03_AXI_WVALID),
        .M04_ACLK(clk_0_1),
        .M04_ARESETN(kernel_rstn_1),
        .M04_AXI_araddr(axi_interconnect_2_M04_AXI_ARADDR),
        .M04_AXI_arready(axi_interconnect_2_M04_AXI_ARREADY),
        .M04_AXI_arvalid(axi_interconnect_2_M04_AXI_ARVALID),
        .M04_AXI_awaddr(axi_interconnect_2_M04_AXI_AWADDR),
        .M04_AXI_awready(axi_interconnect_2_M04_AXI_AWREADY),
        .M04_AXI_awvalid(axi_interconnect_2_M04_AXI_AWVALID),
        .M04_AXI_bready(axi_interconnect_2_M04_AXI_BREADY),
        .M04_AXI_bresp(axi_interconnect_2_M04_AXI_BRESP),
        .M04_AXI_bvalid(axi_interconnect_2_M04_AXI_BVALID),
        .M04_AXI_rdata(axi_interconnect_2_M04_AXI_RDATA),
        .M04_AXI_rready(axi_interconnect_2_M04_AXI_RREADY),
        .M04_AXI_rresp(axi_interconnect_2_M04_AXI_RRESP),
        .M04_AXI_rvalid(axi_interconnect_2_M04_AXI_RVALID),
        .M04_AXI_wdata(axi_interconnect_2_M04_AXI_WDATA),
        .M04_AXI_wready(axi_interconnect_2_M04_AXI_WREADY),
        .M04_AXI_wstrb(axi_interconnect_2_M04_AXI_WSTRB),
        .M04_AXI_wvalid(axi_interconnect_2_M04_AXI_WVALID),
        .M05_ACLK(clk_0_1),
        .M05_ARESETN(kernel_rstn_1),
        .M05_AXI_araddr(axi_interconnect_2_M05_AXI_ARADDR),
        .M05_AXI_arready(axi_interconnect_2_M05_AXI_ARREADY),
        .M05_AXI_arvalid(axi_interconnect_2_M05_AXI_ARVALID),
        .M05_AXI_awaddr(axi_interconnect_2_M05_AXI_AWADDR),
        .M05_AXI_awready(axi_interconnect_2_M05_AXI_AWREADY),
        .M05_AXI_awvalid(axi_interconnect_2_M05_AXI_AWVALID),
        .M05_AXI_bready(axi_interconnect_2_M05_AXI_BREADY),
        .M05_AXI_bresp(axi_interconnect_2_M05_AXI_BRESP),
        .M05_AXI_bvalid(axi_interconnect_2_M05_AXI_BVALID),
        .M05_AXI_rdata(axi_interconnect_2_M05_AXI_RDATA),
        .M05_AXI_rready(axi_interconnect_2_M05_AXI_RREADY),
        .M05_AXI_rresp(axi_interconnect_2_M05_AXI_RRESP),
        .M05_AXI_rvalid(axi_interconnect_2_M05_AXI_RVALID),
        .M05_AXI_wdata(axi_interconnect_2_M05_AXI_WDATA),
        .M05_AXI_wready(axi_interconnect_2_M05_AXI_WREADY),
        .M05_AXI_wstrb(axi_interconnect_2_M05_AXI_WSTRB),
        .M05_AXI_wvalid(axi_interconnect_2_M05_AXI_WVALID),
        .M06_ACLK(clk_0_1),
        .M06_ARESETN(kernel_rstn_1),
        .M06_AXI_araddr(axi_interconnect_2_M06_AXI_ARADDR),
        .M06_AXI_arready(axi_interconnect_2_M06_AXI_ARREADY),
        .M06_AXI_arvalid(axi_interconnect_2_M06_AXI_ARVALID),
        .M06_AXI_awaddr(axi_interconnect_2_M06_AXI_AWADDR),
        .M06_AXI_awready(axi_interconnect_2_M06_AXI_AWREADY),
        .M06_AXI_awvalid(axi_interconnect_2_M06_AXI_AWVALID),
        .M06_AXI_bready(axi_interconnect_2_M06_AXI_BREADY),
        .M06_AXI_bresp(axi_interconnect_2_M06_AXI_BRESP),
        .M06_AXI_bvalid(axi_interconnect_2_M06_AXI_BVALID),
        .M06_AXI_rdata(axi_interconnect_2_M06_AXI_RDATA),
        .M06_AXI_rready(axi_interconnect_2_M06_AXI_RREADY),
        .M06_AXI_rresp(axi_interconnect_2_M06_AXI_RRESP),
        .M06_AXI_rvalid(axi_interconnect_2_M06_AXI_RVALID),
        .M06_AXI_wdata(axi_interconnect_2_M06_AXI_WDATA),
        .M06_AXI_wready(axi_interconnect_2_M06_AXI_WREADY),
        .M06_AXI_wstrb(axi_interconnect_2_M06_AXI_WSTRB),
        .M06_AXI_wvalid(axi_interconnect_2_M06_AXI_WVALID),
        .S00_ACLK(clk_0_1),
        .S00_ARESETN(kernel_rstn_1),
        .S00_AXI_araddr(taiga_wrapper_xilinx_0_m_axi_ARADDR),
        .S00_AXI_arburst(taiga_wrapper_xilinx_0_m_axi_ARBURST),
        .S00_AXI_arcache(taiga_wrapper_xilinx_0_m_axi_ARCACHE),
        .S00_AXI_arid(taiga_wrapper_xilinx_0_m_axi_ARID),
        .S00_AXI_arlen(taiga_wrapper_xilinx_0_m_axi_ARLEN),
        .S00_AXI_arlock(taiga_wrapper_xilinx_0_m_axi_ARLOCK),
        .S00_AXI_arprot(taiga_wrapper_xilinx_0_m_axi_ARPROT),
        .S00_AXI_arqos(taiga_wrapper_xilinx_0_m_axi_ARQOS),
        .S00_AXI_arready(taiga_wrapper_xilinx_0_m_axi_ARREADY),
        .S00_AXI_arsize(taiga_wrapper_xilinx_0_m_axi_ARSIZE),
        .S00_AXI_aruser(taiga_wrapper_xilinx_0_m_axi_ARUSER),
        .S00_AXI_arvalid(taiga_wrapper_xilinx_0_m_axi_ARVALID),
        .S00_AXI_awaddr(taiga_wrapper_xilinx_0_m_axi_AWADDR),
        .S00_AXI_awburst(taiga_wrapper_xilinx_0_m_axi_AWBURST),
        .S00_AXI_awcache(taiga_wrapper_xilinx_0_m_axi_AWCACHE),
        .S00_AXI_awid(taiga_wrapper_xilinx_0_m_axi_AWID),
        .S00_AXI_awlen(taiga_wrapper_xilinx_0_m_axi_AWLEN),
        .S00_AXI_awlock(taiga_wrapper_xilinx_0_m_axi_AWLOCK),
        .S00_AXI_awprot(taiga_wrapper_xilinx_0_m_axi_AWPROT),
        .S00_AXI_awqos(taiga_wrapper_xilinx_0_m_axi_AWQOS),
        .S00_AXI_awready(taiga_wrapper_xilinx_0_m_axi_AWREADY),
        .S00_AXI_awsize(taiga_wrapper_xilinx_0_m_axi_AWSIZE),
        .S00_AXI_awuser(taiga_wrapper_xilinx_0_m_axi_AWUSER),
        .S00_AXI_awvalid(taiga_wrapper_xilinx_0_m_axi_AWVALID),
        .S00_AXI_bid(taiga_wrapper_xilinx_0_m_axi_BID),
        .S00_AXI_bready(taiga_wrapper_xilinx_0_m_axi_BREADY),
        .S00_AXI_bresp(taiga_wrapper_xilinx_0_m_axi_BRESP),
        .S00_AXI_buser(taiga_wrapper_xilinx_0_m_axi_BUSER),
        .S00_AXI_bvalid(taiga_wrapper_xilinx_0_m_axi_BVALID),
        .S00_AXI_rdata(taiga_wrapper_xilinx_0_m_axi_RDATA),
        .S00_AXI_rid(taiga_wrapper_xilinx_0_m_axi_RID),
        .S00_AXI_rlast(taiga_wrapper_xilinx_0_m_axi_RLAST),
        .S00_AXI_rready(taiga_wrapper_xilinx_0_m_axi_RREADY),
        .S00_AXI_rresp(taiga_wrapper_xilinx_0_m_axi_RRESP),
        .S00_AXI_ruser(taiga_wrapper_xilinx_0_m_axi_RUSER),
        .S00_AXI_rvalid(taiga_wrapper_xilinx_0_m_axi_RVALID),
        .S00_AXI_wdata(taiga_wrapper_xilinx_0_m_axi_WDATA),
        .S00_AXI_wlast(taiga_wrapper_xilinx_0_m_axi_WLAST),
        .S00_AXI_wready(taiga_wrapper_xilinx_0_m_axi_WREADY),
        .S00_AXI_wstrb(taiga_wrapper_xilinx_0_m_axi_WSTRB),
        .S00_AXI_wuser(taiga_wrapper_xilinx_0_m_axi_WUSER),
        .S00_AXI_wvalid(taiga_wrapper_xilinx_0_m_axi_WVALID));
  gpn_bundle_block_axi_interconnect_1_0 axi_interconnect_3
       (.ACLK(clk_0_1),
        .ARESETN(kernel_rstn_1),
        .M00_ACLK(M00_ACLK_0_1),
        .M00_ARESETN(M00_ARESETN_0_1),
        .M00_AXI_araddr(axi_interconnect_3_M00_AXI_ARADDR),
        .M00_AXI_arburst(axi_interconnect_3_M00_AXI_ARBURST),
        .M00_AXI_arcache(axi_interconnect_3_M00_AXI_ARCACHE),
        .M00_AXI_arid(axi_interconnect_3_M00_AXI_ARID),
        .M00_AXI_arlen(axi_interconnect_3_M00_AXI_ARLEN),
        .M00_AXI_arlock(axi_interconnect_3_M00_AXI_ARLOCK),
        .M00_AXI_arprot(axi_interconnect_3_M00_AXI_ARPROT),
        .M00_AXI_arqos(axi_interconnect_3_M00_AXI_ARQOS),
        .M00_AXI_arready(axi_interconnect_3_M00_AXI_ARREADY),
        .M00_AXI_arregion(axi_interconnect_3_M00_AXI_ARREGION),
        .M00_AXI_arsize(axi_interconnect_3_M00_AXI_ARSIZE),
        .M00_AXI_aruser(axi_interconnect_3_M00_AXI_ARUSER),
        .M00_AXI_arvalid(axi_interconnect_3_M00_AXI_ARVALID),
        .M00_AXI_awaddr(axi_interconnect_3_M00_AXI_AWADDR),
        .M00_AXI_awburst(axi_interconnect_3_M00_AXI_AWBURST),
        .M00_AXI_awcache(axi_interconnect_3_M00_AXI_AWCACHE),
        .M00_AXI_awid(axi_interconnect_3_M00_AXI_AWID),
        .M00_AXI_awlen(axi_interconnect_3_M00_AXI_AWLEN),
        .M00_AXI_awlock(axi_interconnect_3_M00_AXI_AWLOCK),
        .M00_AXI_awprot(axi_interconnect_3_M00_AXI_AWPROT),
        .M00_AXI_awqos(axi_interconnect_3_M00_AXI_AWQOS),
        .M00_AXI_awready(axi_interconnect_3_M00_AXI_AWREADY),
        .M00_AXI_awregion(axi_interconnect_3_M00_AXI_AWREGION),
        .M00_AXI_awsize(axi_interconnect_3_M00_AXI_AWSIZE),
        .M00_AXI_awuser(axi_interconnect_3_M00_AXI_AWUSER),
        .M00_AXI_awvalid(axi_interconnect_3_M00_AXI_AWVALID),
        .M00_AXI_bid(axi_interconnect_3_M00_AXI_BID),
        .M00_AXI_bready(axi_interconnect_3_M00_AXI_BREADY),
        .M00_AXI_bresp(axi_interconnect_3_M00_AXI_BRESP),
        .M00_AXI_buser(axi_interconnect_3_M00_AXI_BUSER),
        .M00_AXI_bvalid(axi_interconnect_3_M00_AXI_BVALID),
        .M00_AXI_rdata(axi_interconnect_3_M00_AXI_RDATA),
        .M00_AXI_rid(axi_interconnect_3_M00_AXI_RID),
        .M00_AXI_rlast(axi_interconnect_3_M00_AXI_RLAST),
        .M00_AXI_rready(axi_interconnect_3_M00_AXI_RREADY),
        .M00_AXI_rresp(axi_interconnect_3_M00_AXI_RRESP),
        .M00_AXI_ruser(axi_interconnect_3_M00_AXI_RUSER),
        .M00_AXI_rvalid(axi_interconnect_3_M00_AXI_RVALID),
        .M00_AXI_wdata(axi_interconnect_3_M00_AXI_WDATA),
        .M00_AXI_wlast(axi_interconnect_3_M00_AXI_WLAST),
        .M00_AXI_wready(axi_interconnect_3_M00_AXI_WREADY),
        .M00_AXI_wstrb(axi_interconnect_3_M00_AXI_WSTRB),
        .M00_AXI_wuser(axi_interconnect_3_M00_AXI_WUSER),
        .M00_AXI_wvalid(axi_interconnect_3_M00_AXI_WVALID),
        .S00_ACLK(clk_0_1),
        .S00_ARESETN(kernel_rstn_1),
        .S00_AXI_araddr(axi_interconnect_2_M03_AXI_ARADDR),
        .S00_AXI_arburst(axi_interconnect_2_M03_AXI_ARBURST),
        .S00_AXI_arcache(axi_interconnect_2_M03_AXI_ARCACHE),
        .S00_AXI_arid(axi_interconnect_2_M03_AXI_ARID),
        .S00_AXI_arlen(axi_interconnect_2_M03_AXI_ARLEN),
        .S00_AXI_arlock(axi_interconnect_2_M03_AXI_ARLOCK),
        .S00_AXI_arprot(axi_interconnect_2_M03_AXI_ARPROT),
        .S00_AXI_arqos(axi_interconnect_2_M03_AXI_ARQOS),
        .S00_AXI_arready(axi_interconnect_2_M03_AXI_ARREADY),
        .S00_AXI_arregion(axi_interconnect_2_M03_AXI_ARREGION),
        .S00_AXI_arsize(axi_interconnect_2_M03_AXI_ARSIZE),
        .S00_AXI_aruser(axi_interconnect_2_M03_AXI_ARUSER),
        .S00_AXI_arvalid(axi_interconnect_2_M03_AXI_ARVALID),
        .S00_AXI_awaddr(axi_interconnect_2_M03_AXI_AWADDR),
        .S00_AXI_awburst(axi_interconnect_2_M03_AXI_AWBURST),
        .S00_AXI_awcache(axi_interconnect_2_M03_AXI_AWCACHE),
        .S00_AXI_awid(axi_interconnect_2_M03_AXI_AWID),
        .S00_AXI_awlen(axi_interconnect_2_M03_AXI_AWLEN),
        .S00_AXI_awlock(axi_interconnect_2_M03_AXI_AWLOCK),
        .S00_AXI_awprot(axi_interconnect_2_M03_AXI_AWPROT),
        .S00_AXI_awqos(axi_interconnect_2_M03_AXI_AWQOS),
        .S00_AXI_awready(axi_interconnect_2_M03_AXI_AWREADY),
        .S00_AXI_awregion(axi_interconnect_2_M03_AXI_AWREGION),
        .S00_AXI_awsize(axi_interconnect_2_M03_AXI_AWSIZE),
        .S00_AXI_awuser(axi_interconnect_2_M03_AXI_AWUSER),
        .S00_AXI_awvalid(axi_interconnect_2_M03_AXI_AWVALID),
        .S00_AXI_bid(axi_interconnect_2_M03_AXI_BID),
        .S00_AXI_bready(axi_interconnect_2_M03_AXI_BREADY),
        .S00_AXI_bresp(axi_interconnect_2_M03_AXI_BRESP),
        .S00_AXI_buser(axi_interconnect_2_M03_AXI_BUSER),
        .S00_AXI_bvalid(axi_interconnect_2_M03_AXI_BVALID),
        .S00_AXI_rdata(axi_interconnect_2_M03_AXI_RDATA),
        .S00_AXI_rid(axi_interconnect_2_M03_AXI_RID),
        .S00_AXI_rlast(axi_interconnect_2_M03_AXI_RLAST),
        .S00_AXI_rready(axi_interconnect_2_M03_AXI_RREADY),
        .S00_AXI_rresp(axi_interconnect_2_M03_AXI_RRESP),
        .S00_AXI_ruser(axi_interconnect_2_M03_AXI_RUSER),
        .S00_AXI_rvalid(axi_interconnect_2_M03_AXI_RVALID),
        .S00_AXI_wdata(axi_interconnect_2_M03_AXI_WDATA),
        .S00_AXI_wlast(axi_interconnect_2_M03_AXI_WLAST),
        .S00_AXI_wready(axi_interconnect_2_M03_AXI_WREADY),
        .S00_AXI_wstrb(axi_interconnect_2_M03_AXI_WSTRB),
        .S00_AXI_wuser(axi_interconnect_2_M03_AXI_WUSER),
        .S00_AXI_wvalid(axi_interconnect_2_M03_AXI_WVALID));
  gpn_bundle_block_axi_gpio_2_0 gpc2host_mq_RnM
       (.gpio_io_o(axi_gpio_2_gpio_io_o),
        .s_axi_aclk(clk_0_1),
        .s_axi_araddr(axi_interconnect_2_M04_AXI_ARADDR[8:0]),
        .s_axi_aresetn(proc_sys_reset_0_peripheral_aresetn),
        .s_axi_arready(axi_interconnect_2_M04_AXI_ARREADY),
        .s_axi_arvalid(axi_interconnect_2_M04_AXI_ARVALID),
        .s_axi_awaddr(axi_interconnect_2_M04_AXI_AWADDR[8:0]),
        .s_axi_awready(axi_interconnect_2_M04_AXI_AWREADY),
        .s_axi_awvalid(axi_interconnect_2_M04_AXI_AWVALID),
        .s_axi_bready(axi_interconnect_2_M04_AXI_BREADY),
        .s_axi_bresp(axi_interconnect_2_M04_AXI_BRESP),
        .s_axi_bvalid(axi_interconnect_2_M04_AXI_BVALID),
        .s_axi_rdata(axi_interconnect_2_M04_AXI_RDATA),
        .s_axi_rready(axi_interconnect_2_M04_AXI_RREADY),
        .s_axi_rresp(axi_interconnect_2_M04_AXI_RRESP),
        .s_axi_rvalid(axi_interconnect_2_M04_AXI_RVALID),
        .s_axi_wdata(axi_interconnect_2_M04_AXI_WDATA),
        .s_axi_wready(axi_interconnect_2_M04_AXI_WREADY),
        .s_axi_wstrb(axi_interconnect_2_M04_AXI_WSTRB),
        .s_axi_wvalid(axi_interconnect_2_M04_AXI_WVALID));
  gpn_bundle_block_axi_gpio_0_0 gpc_start_RnM
       (.gpio2_io_i(gpn_control_reg_1),
        .gpio_io_i(gpio_io_i_0_1),
        .s_axi_aclk(clk_0_1),
        .s_axi_araddr(axi_interconnect_0_M01_AXI_ARADDR[8:0]),
        .s_axi_aresetn(kernel_rstn_1),
        .s_axi_arready(axi_interconnect_0_M01_AXI_ARREADY),
        .s_axi_arvalid(axi_interconnect_0_M01_AXI_ARVALID),
        .s_axi_awaddr(axi_interconnect_0_M01_AXI_AWADDR[8:0]),
        .s_axi_awready(axi_interconnect_0_M01_AXI_AWREADY),
        .s_axi_awvalid(axi_interconnect_0_M01_AXI_AWVALID),
        .s_axi_bready(axi_interconnect_0_M01_AXI_BREADY),
        .s_axi_bresp(axi_interconnect_0_M01_AXI_BRESP),
        .s_axi_bvalid(axi_interconnect_0_M01_AXI_BVALID),
        .s_axi_rdata(axi_interconnect_0_M01_AXI_RDATA),
        .s_axi_rready(axi_interconnect_0_M01_AXI_RREADY),
        .s_axi_rresp(axi_interconnect_0_M01_AXI_RRESP),
        .s_axi_rvalid(axi_interconnect_0_M01_AXI_RVALID),
        .s_axi_wdata(axi_interconnect_0_M01_AXI_WDATA),
        .s_axi_wready(axi_interconnect_0_M01_AXI_WREADY),
        .s_axi_wstrb(axi_interconnect_0_M01_AXI_WSTRB),
        .s_axi_wvalid(axi_interconnect_0_M01_AXI_WVALID));
  gpn_bundle_block_axi_gpio_1_0 gpc_state
       (.gpio_io_o(axi_gpio_1_gpio_io_o),
        .s_axi_aclk(clk_0_1),
        .s_axi_araddr(axi_interconnect_0_M02_AXI_ARADDR[8:0]),
        .s_axi_aresetn(proc_sys_reset_0_peripheral_aresetn),
        .s_axi_arready(axi_interconnect_0_M02_AXI_ARREADY),
        .s_axi_arvalid(axi_interconnect_0_M02_AXI_ARVALID),
        .s_axi_awaddr(axi_interconnect_0_M02_AXI_AWADDR[8:0]),
        .s_axi_awready(axi_interconnect_0_M02_AXI_AWREADY),
        .s_axi_awvalid(axi_interconnect_0_M02_AXI_AWVALID),
        .s_axi_bready(axi_interconnect_0_M02_AXI_BREADY),
        .s_axi_bresp(axi_interconnect_0_M02_AXI_BRESP),
        .s_axi_bvalid(axi_interconnect_0_M02_AXI_BVALID),
        .s_axi_rdata(axi_interconnect_0_M02_AXI_RDATA),
        .s_axi_rready(axi_interconnect_0_M02_AXI_RREADY),
        .s_axi_rresp(axi_interconnect_0_M02_AXI_RRESP),
        .s_axi_rvalid(axi_interconnect_0_M02_AXI_RVALID),
        .s_axi_wdata(axi_interconnect_0_M02_AXI_WDATA),
        .s_axi_wready(axi_interconnect_0_M02_AXI_WREADY),
        .s_axi_wstrb(axi_interconnect_0_M02_AXI_WSTRB),
        .s_axi_wvalid(axi_interconnect_0_M02_AXI_WVALID));
  gpn_bundle_block_axi_gpio_2_1 hpst2gpc
       (.gpio_io_i(gpio_io_i_0_3),
        .s_axi_aclk(clk_0_1),
        .s_axi_araddr(axi_interconnect_2_M05_AXI_ARADDR[8:0]),
        .s_axi_aresetn(proc_sys_reset_0_peripheral_aresetn),
        .s_axi_arready(axi_interconnect_2_M05_AXI_ARREADY),
        .s_axi_arvalid(axi_interconnect_2_M05_AXI_ARVALID),
        .s_axi_awaddr(axi_interconnect_2_M05_AXI_AWADDR[8:0]),
        .s_axi_awready(axi_interconnect_2_M05_AXI_AWREADY),
        .s_axi_awvalid(axi_interconnect_2_M05_AXI_AWVALID),
        .s_axi_bready(axi_interconnect_2_M05_AXI_BREADY),
        .s_axi_bresp(axi_interconnect_2_M05_AXI_BRESP),
        .s_axi_bvalid(axi_interconnect_2_M05_AXI_BVALID),
        .s_axi_rdata(axi_interconnect_2_M05_AXI_RDATA),
        .s_axi_rready(axi_interconnect_2_M05_AXI_RREADY),
        .s_axi_rresp(axi_interconnect_2_M05_AXI_RRESP),
        .s_axi_rvalid(axi_interconnect_2_M05_AXI_RVALID),
        .s_axi_wdata(axi_interconnect_2_M05_AXI_WDATA),
        .s_axi_wready(axi_interconnect_2_M05_AXI_WREADY),
        .s_axi_wstrb(axi_interconnect_2_M05_AXI_WSTRB),
        .s_axi_wvalid(axi_interconnect_2_M05_AXI_WVALID));
  gpn_bundle_block_local_mem_0_0 local_mem_0
       (.clk(clk_0_1),
        .portA_addr(taiga_wrapper_xilinx_0_data_bram_addr),
        .portA_be(taiga_wrapper_xilinx_0_data_bram_be),
        .portA_data_in(taiga_wrapper_xilinx_0_data_bram_data_in),
        .portA_data_out(taiga_wrapper_xilinx_0_data_bram_data_out),
        .portA_en(taiga_wrapper_xilinx_0_data_bram_en),
        .portB_addr(taiga_wrapper_xilinx_0_instruction_bram_addr),
        .portB_be(taiga_wrapper_xilinx_0_instruction_bram_be),
        .portB_data_in(taiga_wrapper_xilinx_0_instruction_bram_data_in),
        .portB_data_out(taiga_wrapper_xilinx_0_instruction_bram_data_out),
        .portB_en(taiga_wrapper_xilinx_0_instruction_bram_en),
        .rstn(kernel_rstn_1));
  gpn_bundle_block_local_rom_0_0 local_rom_0
       (.clk(clk_0_1),
        .porta_addr(taiga_wrapper_xilinx_0_instruction_rom_addr),
        .porta_be(taiga_wrapper_xilinx_0_instruction_rom_be),
        .porta_data_in(taiga_wrapper_xilinx_0_instruction_rom_data_in),
        .porta_data_out(taiga_wrapper_xilinx_0_instruction_rom_data_out),
        .porta_en(taiga_wrapper_xilinx_0_instruction_rom_en),
        .rstn(kernel_rstn_1));
  gpn_bundle_block_axi_gpio_2_2 mq_st
       (.gpio2_io_o(mq_st_gpio2_io_o),
        .gpio_io_i(gpio_io_i_0_2),
        .s_axi_aclk(clk_0_1),
        .s_axi_araddr(axi_interconnect_2_M06_AXI_ARADDR[8:0]),
        .s_axi_aresetn(proc_sys_reset_0_peripheral_aresetn),
        .s_axi_arready(axi_interconnect_2_M06_AXI_ARREADY),
        .s_axi_arvalid(axi_interconnect_2_M06_AXI_ARVALID),
        .s_axi_awaddr(axi_interconnect_2_M06_AXI_AWADDR[8:0]),
        .s_axi_awready(axi_interconnect_2_M06_AXI_AWREADY),
        .s_axi_awvalid(axi_interconnect_2_M06_AXI_AWVALID),
        .s_axi_bready(axi_interconnect_2_M06_AXI_BREADY),
        .s_axi_bresp(axi_interconnect_2_M06_AXI_BRESP),
        .s_axi_bvalid(axi_interconnect_2_M06_AXI_BVALID),
        .s_axi_rdata(axi_interconnect_2_M06_AXI_RDATA),
        .s_axi_rready(axi_interconnect_2_M06_AXI_RREADY),
        .s_axi_rresp(axi_interconnect_2_M06_AXI_RRESP),
        .s_axi_rvalid(axi_interconnect_2_M06_AXI_RVALID),
        .s_axi_wdata(axi_interconnect_2_M06_AXI_WDATA),
        .s_axi_wready(axi_interconnect_2_M06_AXI_WREADY),
        .s_axi_wstrb(axi_interconnect_2_M06_AXI_WSTRB),
        .s_axi_wvalid(axi_interconnect_2_M06_AXI_WVALID));
  gpn_bundle_block_proc_sys_reset_0_0 proc_sys_reset_0
       (.aux_reset_in(1'b1),
        .dcm_locked(1'b1),
        .ext_reset_in(gpn_reset_1),
        .mb_debug_sys_rst(1'b0),
        .peripheral_aresetn(proc_sys_reset_0_peripheral_aresetn),
        .peripheral_reset(proc_sys_reset_0_peripheral_reset),
        .slowest_sync_clk(clk_0_1));
  gpn_bundle_block_taiga_wrapper_xilinx_0_0 taiga_wrapper_xilinx_0
       (.clk(clk_0_1),
        .data_bram_addr(taiga_wrapper_xilinx_0_data_bram_addr),
        .data_bram_be(taiga_wrapper_xilinx_0_data_bram_be),
        .data_bram_data_in(taiga_wrapper_xilinx_0_data_bram_data_in),
        .data_bram_data_out(taiga_wrapper_xilinx_0_data_bram_data_out),
        .data_bram_en(taiga_wrapper_xilinx_0_data_bram_en),
        .instruction_bram_addr(taiga_wrapper_xilinx_0_instruction_bram_addr),
        .instruction_bram_be(taiga_wrapper_xilinx_0_instruction_bram_be),
        .instruction_bram_data_in(taiga_wrapper_xilinx_0_instruction_bram_data_in),
        .instruction_bram_data_out(taiga_wrapper_xilinx_0_instruction_bram_data_out),
        .instruction_bram_en(taiga_wrapper_xilinx_0_instruction_bram_en),
        .instruction_rom_addr(taiga_wrapper_xilinx_0_instruction_rom_addr),
        .instruction_rom_be(taiga_wrapper_xilinx_0_instruction_rom_be),
        .instruction_rom_data_in(taiga_wrapper_xilinx_0_instruction_rom_data_in),
        .instruction_rom_data_out(taiga_wrapper_xilinx_0_instruction_rom_data_out),
        .instruction_rom_en(taiga_wrapper_xilinx_0_instruction_rom_en),
        .m_axi_araddr(taiga_wrapper_xilinx_0_m_axi_ARADDR),
        .m_axi_arburst(taiga_wrapper_xilinx_0_m_axi_ARBURST),
        .m_axi_arcache(taiga_wrapper_xilinx_0_m_axi_ARCACHE),
        .m_axi_arid(taiga_wrapper_xilinx_0_m_axi_ARID),
        .m_axi_arlen(taiga_wrapper_xilinx_0_m_axi_ARLEN),
        .m_axi_arlock(taiga_wrapper_xilinx_0_m_axi_ARLOCK),
        .m_axi_arprot(taiga_wrapper_xilinx_0_m_axi_ARPROT),
        .m_axi_arqos(taiga_wrapper_xilinx_0_m_axi_ARQOS),
        .m_axi_arready(taiga_wrapper_xilinx_0_m_axi_ARREADY),
        .m_axi_arsize(taiga_wrapper_xilinx_0_m_axi_ARSIZE),
        .m_axi_aruser(taiga_wrapper_xilinx_0_m_axi_ARUSER),
        .m_axi_arvalid(taiga_wrapper_xilinx_0_m_axi_ARVALID),
        .m_axi_awaddr(taiga_wrapper_xilinx_0_m_axi_AWADDR),
        .m_axi_awburst(taiga_wrapper_xilinx_0_m_axi_AWBURST),
        .m_axi_awcache(taiga_wrapper_xilinx_0_m_axi_AWCACHE),
        .m_axi_awid(taiga_wrapper_xilinx_0_m_axi_AWID),
        .m_axi_awlen(taiga_wrapper_xilinx_0_m_axi_AWLEN),
        .m_axi_awlock(taiga_wrapper_xilinx_0_m_axi_AWLOCK),
        .m_axi_awprot(taiga_wrapper_xilinx_0_m_axi_AWPROT),
        .m_axi_awqos(taiga_wrapper_xilinx_0_m_axi_AWQOS),
        .m_axi_awready(taiga_wrapper_xilinx_0_m_axi_AWREADY),
        .m_axi_awsize(taiga_wrapper_xilinx_0_m_axi_AWSIZE),
        .m_axi_awuser(taiga_wrapper_xilinx_0_m_axi_AWUSER),
        .m_axi_awvalid(taiga_wrapper_xilinx_0_m_axi_AWVALID),
        .m_axi_bid(taiga_wrapper_xilinx_0_m_axi_BID),
        .m_axi_bready(taiga_wrapper_xilinx_0_m_axi_BREADY),
        .m_axi_bresp(taiga_wrapper_xilinx_0_m_axi_BRESP),
        .m_axi_buser(taiga_wrapper_xilinx_0_m_axi_BUSER),
        .m_axi_bvalid(taiga_wrapper_xilinx_0_m_axi_BVALID),
        .m_axi_rdata(taiga_wrapper_xilinx_0_m_axi_RDATA),
        .m_axi_rid(taiga_wrapper_xilinx_0_m_axi_RID),
        .m_axi_rlast(taiga_wrapper_xilinx_0_m_axi_RLAST),
        .m_axi_rready(taiga_wrapper_xilinx_0_m_axi_RREADY),
        .m_axi_rresp(taiga_wrapper_xilinx_0_m_axi_RRESP),
        .m_axi_ruser(taiga_wrapper_xilinx_0_m_axi_RUSER),
        .m_axi_rvalid(taiga_wrapper_xilinx_0_m_axi_RVALID),
        .m_axi_wdata(taiga_wrapper_xilinx_0_m_axi_WDATA),
        .m_axi_wlast(taiga_wrapper_xilinx_0_m_axi_WLAST),
        .m_axi_wready(taiga_wrapper_xilinx_0_m_axi_WREADY),
        .m_axi_wstrb(taiga_wrapper_xilinx_0_m_axi_WSTRB),
        .m_axi_wuser(taiga_wrapper_xilinx_0_m_axi_WUSER),
        .m_axi_wvalid(taiga_wrapper_xilinx_0_m_axi_WVALID),
        .rst(proc_sys_reset_0_peripheral_reset));
endmodule

module gpn_bundle_block_axi_interconnect_0_1
   (ACLK,
    ARESETN,
    M00_ACLK,
    M00_ARESETN,
    M00_AXI_araddr,
    M00_AXI_arburst,
    M00_AXI_arcache,
    M00_AXI_arid,
    M00_AXI_arlen,
    M00_AXI_arlock,
    M00_AXI_arprot,
    M00_AXI_arqos,
    M00_AXI_arready,
    M00_AXI_arregion,
    M00_AXI_arsize,
    M00_AXI_aruser,
    M00_AXI_arvalid,
    M00_AXI_awaddr,
    M00_AXI_awburst,
    M00_AXI_awcache,
    M00_AXI_awid,
    M00_AXI_awlen,
    M00_AXI_awlock,
    M00_AXI_awprot,
    M00_AXI_awqos,
    M00_AXI_awready,
    M00_AXI_awregion,
    M00_AXI_awsize,
    M00_AXI_awuser,
    M00_AXI_awvalid,
    M00_AXI_bid,
    M00_AXI_bready,
    M00_AXI_bresp,
    M00_AXI_buser,
    M00_AXI_bvalid,
    M00_AXI_rdata,
    M00_AXI_rid,
    M00_AXI_rlast,
    M00_AXI_rready,
    M00_AXI_rresp,
    M00_AXI_ruser,
    M00_AXI_rvalid,
    M00_AXI_wdata,
    M00_AXI_wlast,
    M00_AXI_wready,
    M00_AXI_wstrb,
    M00_AXI_wuser,
    M00_AXI_wvalid,
    M01_ACLK,
    M01_ARESETN,
    M01_AXI_araddr,
    M01_AXI_arready,
    M01_AXI_arvalid,
    M01_AXI_awaddr,
    M01_AXI_awready,
    M01_AXI_awvalid,
    M01_AXI_bready,
    M01_AXI_bresp,
    M01_AXI_bvalid,
    M01_AXI_rdata,
    M01_AXI_rready,
    M01_AXI_rresp,
    M01_AXI_rvalid,
    M01_AXI_wdata,
    M01_AXI_wready,
    M01_AXI_wstrb,
    M01_AXI_wvalid,
    M02_ACLK,
    M02_ARESETN,
    M02_AXI_araddr,
    M02_AXI_arready,
    M02_AXI_arvalid,
    M02_AXI_awaddr,
    M02_AXI_awready,
    M02_AXI_awvalid,
    M02_AXI_bready,
    M02_AXI_bresp,
    M02_AXI_bvalid,
    M02_AXI_rdata,
    M02_AXI_rready,
    M02_AXI_rresp,
    M02_AXI_rvalid,
    M02_AXI_wdata,
    M02_AXI_wready,
    M02_AXI_wstrb,
    M02_AXI_wvalid,
    M03_ACLK,
    M03_ARESETN,
    M03_AXI_araddr,
    M03_AXI_arburst,
    M03_AXI_arcache,
    M03_AXI_arid,
    M03_AXI_arlen,
    M03_AXI_arlock,
    M03_AXI_arprot,
    M03_AXI_arqos,
    M03_AXI_arready,
    M03_AXI_arregion,
    M03_AXI_arsize,
    M03_AXI_aruser,
    M03_AXI_arvalid,
    M03_AXI_awaddr,
    M03_AXI_awburst,
    M03_AXI_awcache,
    M03_AXI_awid,
    M03_AXI_awlen,
    M03_AXI_awlock,
    M03_AXI_awprot,
    M03_AXI_awqos,
    M03_AXI_awready,
    M03_AXI_awregion,
    M03_AXI_awsize,
    M03_AXI_awuser,
    M03_AXI_awvalid,
    M03_AXI_bid,
    M03_AXI_bready,
    M03_AXI_bresp,
    M03_AXI_buser,
    M03_AXI_bvalid,
    M03_AXI_rdata,
    M03_AXI_rid,
    M03_AXI_rlast,
    M03_AXI_rready,
    M03_AXI_rresp,
    M03_AXI_ruser,
    M03_AXI_rvalid,
    M03_AXI_wdata,
    M03_AXI_wlast,
    M03_AXI_wready,
    M03_AXI_wstrb,
    M03_AXI_wuser,
    M03_AXI_wvalid,
    M04_ACLK,
    M04_ARESETN,
    M04_AXI_araddr,
    M04_AXI_arready,
    M04_AXI_arvalid,
    M04_AXI_awaddr,
    M04_AXI_awready,
    M04_AXI_awvalid,
    M04_AXI_bready,
    M04_AXI_bresp,
    M04_AXI_bvalid,
    M04_AXI_rdata,
    M04_AXI_rready,
    M04_AXI_rresp,
    M04_AXI_rvalid,
    M04_AXI_wdata,
    M04_AXI_wready,
    M04_AXI_wstrb,
    M04_AXI_wvalid,
    M05_ACLK,
    M05_ARESETN,
    M05_AXI_araddr,
    M05_AXI_arready,
    M05_AXI_arvalid,
    M05_AXI_awaddr,
    M05_AXI_awready,
    M05_AXI_awvalid,
    M05_AXI_bready,
    M05_AXI_bresp,
    M05_AXI_bvalid,
    M05_AXI_rdata,
    M05_AXI_rready,
    M05_AXI_rresp,
    M05_AXI_rvalid,
    M05_AXI_wdata,
    M05_AXI_wready,
    M05_AXI_wstrb,
    M05_AXI_wvalid,
    M06_ACLK,
    M06_ARESETN,
    M06_AXI_araddr,
    M06_AXI_arready,
    M06_AXI_arvalid,
    M06_AXI_awaddr,
    M06_AXI_awready,
    M06_AXI_awvalid,
    M06_AXI_bready,
    M06_AXI_bresp,
    M06_AXI_bvalid,
    M06_AXI_rdata,
    M06_AXI_rready,
    M06_AXI_rresp,
    M06_AXI_rvalid,
    M06_AXI_wdata,
    M06_AXI_wready,
    M06_AXI_wstrb,
    M06_AXI_wvalid,
    S00_ACLK,
    S00_ARESETN,
    S00_AXI_araddr,
    S00_AXI_arburst,
    S00_AXI_arcache,
    S00_AXI_arid,
    S00_AXI_arlen,
    S00_AXI_arlock,
    S00_AXI_arprot,
    S00_AXI_arqos,
    S00_AXI_arready,
    S00_AXI_arsize,
    S00_AXI_aruser,
    S00_AXI_arvalid,
    S00_AXI_awaddr,
    S00_AXI_awburst,
    S00_AXI_awcache,
    S00_AXI_awid,
    S00_AXI_awlen,
    S00_AXI_awlock,
    S00_AXI_awprot,
    S00_AXI_awqos,
    S00_AXI_awready,
    S00_AXI_awsize,
    S00_AXI_awuser,
    S00_AXI_awvalid,
    S00_AXI_bid,
    S00_AXI_bready,
    S00_AXI_bresp,
    S00_AXI_buser,
    S00_AXI_bvalid,
    S00_AXI_rdata,
    S00_AXI_rid,
    S00_AXI_rlast,
    S00_AXI_rready,
    S00_AXI_rresp,
    S00_AXI_ruser,
    S00_AXI_rvalid,
    S00_AXI_wdata,
    S00_AXI_wlast,
    S00_AXI_wready,
    S00_AXI_wstrb,
    S00_AXI_wuser,
    S00_AXI_wvalid);
  input ACLK;
  input ARESETN;
  input M00_ACLK;
  input M00_ARESETN;
  output [31:0]M00_AXI_araddr;
  output [1:0]M00_AXI_arburst;
  output [3:0]M00_AXI_arcache;
  output [5:0]M00_AXI_arid;
  output [7:0]M00_AXI_arlen;
  output [0:0]M00_AXI_arlock;
  output [2:0]M00_AXI_arprot;
  output [3:0]M00_AXI_arqos;
  input [0:0]M00_AXI_arready;
  output [3:0]M00_AXI_arregion;
  output [2:0]M00_AXI_arsize;
  output [3:0]M00_AXI_aruser;
  output [0:0]M00_AXI_arvalid;
  output [31:0]M00_AXI_awaddr;
  output [1:0]M00_AXI_awburst;
  output [3:0]M00_AXI_awcache;
  output [5:0]M00_AXI_awid;
  output [7:0]M00_AXI_awlen;
  output [0:0]M00_AXI_awlock;
  output [2:0]M00_AXI_awprot;
  output [3:0]M00_AXI_awqos;
  input [0:0]M00_AXI_awready;
  output [3:0]M00_AXI_awregion;
  output [2:0]M00_AXI_awsize;
  output [3:0]M00_AXI_awuser;
  output [0:0]M00_AXI_awvalid;
  input [5:0]M00_AXI_bid;
  output [0:0]M00_AXI_bready;
  input [1:0]M00_AXI_bresp;
  input [3:0]M00_AXI_buser;
  input [0:0]M00_AXI_bvalid;
  input [31:0]M00_AXI_rdata;
  input [5:0]M00_AXI_rid;
  input [0:0]M00_AXI_rlast;
  output [0:0]M00_AXI_rready;
  input [1:0]M00_AXI_rresp;
  input [3:0]M00_AXI_ruser;
  input [0:0]M00_AXI_rvalid;
  output [31:0]M00_AXI_wdata;
  output [0:0]M00_AXI_wlast;
  input [0:0]M00_AXI_wready;
  output [3:0]M00_AXI_wstrb;
  output [3:0]M00_AXI_wuser;
  output [0:0]M00_AXI_wvalid;
  input M01_ACLK;
  input M01_ARESETN;
  output [31:0]M01_AXI_araddr;
  input M01_AXI_arready;
  output M01_AXI_arvalid;
  output [31:0]M01_AXI_awaddr;
  input M01_AXI_awready;
  output M01_AXI_awvalid;
  output M01_AXI_bready;
  input [1:0]M01_AXI_bresp;
  input M01_AXI_bvalid;
  input [31:0]M01_AXI_rdata;
  output M01_AXI_rready;
  input [1:0]M01_AXI_rresp;
  input M01_AXI_rvalid;
  output [31:0]M01_AXI_wdata;
  input M01_AXI_wready;
  output [3:0]M01_AXI_wstrb;
  output M01_AXI_wvalid;
  input M02_ACLK;
  input M02_ARESETN;
  output [31:0]M02_AXI_araddr;
  input M02_AXI_arready;
  output M02_AXI_arvalid;
  output [31:0]M02_AXI_awaddr;
  input M02_AXI_awready;
  output M02_AXI_awvalid;
  output M02_AXI_bready;
  input [1:0]M02_AXI_bresp;
  input M02_AXI_bvalid;
  input [31:0]M02_AXI_rdata;
  output M02_AXI_rready;
  input [1:0]M02_AXI_rresp;
  input M02_AXI_rvalid;
  output [31:0]M02_AXI_wdata;
  input M02_AXI_wready;
  output [3:0]M02_AXI_wstrb;
  output M02_AXI_wvalid;
  input M03_ACLK;
  input M03_ARESETN;
  output [31:0]M03_AXI_araddr;
  output [1:0]M03_AXI_arburst;
  output [3:0]M03_AXI_arcache;
  output [5:0]M03_AXI_arid;
  output [7:0]M03_AXI_arlen;
  output [0:0]M03_AXI_arlock;
  output [2:0]M03_AXI_arprot;
  output [3:0]M03_AXI_arqos;
  input M03_AXI_arready;
  output [3:0]M03_AXI_arregion;
  output [2:0]M03_AXI_arsize;
  output [3:0]M03_AXI_aruser;
  output M03_AXI_arvalid;
  output [31:0]M03_AXI_awaddr;
  output [1:0]M03_AXI_awburst;
  output [3:0]M03_AXI_awcache;
  output [5:0]M03_AXI_awid;
  output [7:0]M03_AXI_awlen;
  output [0:0]M03_AXI_awlock;
  output [2:0]M03_AXI_awprot;
  output [3:0]M03_AXI_awqos;
  input M03_AXI_awready;
  output [3:0]M03_AXI_awregion;
  output [2:0]M03_AXI_awsize;
  output [3:0]M03_AXI_awuser;
  output M03_AXI_awvalid;
  input [5:0]M03_AXI_bid;
  output M03_AXI_bready;
  input [1:0]M03_AXI_bresp;
  input [3:0]M03_AXI_buser;
  input M03_AXI_bvalid;
  input [31:0]M03_AXI_rdata;
  input [5:0]M03_AXI_rid;
  input M03_AXI_rlast;
  output M03_AXI_rready;
  input [1:0]M03_AXI_rresp;
  input [3:0]M03_AXI_ruser;
  input M03_AXI_rvalid;
  output [31:0]M03_AXI_wdata;
  output M03_AXI_wlast;
  input M03_AXI_wready;
  output [3:0]M03_AXI_wstrb;
  output [3:0]M03_AXI_wuser;
  output M03_AXI_wvalid;
  input M04_ACLK;
  input M04_ARESETN;
  output [31:0]M04_AXI_araddr;
  input M04_AXI_arready;
  output M04_AXI_arvalid;
  output [31:0]M04_AXI_awaddr;
  input M04_AXI_awready;
  output M04_AXI_awvalid;
  output M04_AXI_bready;
  input [1:0]M04_AXI_bresp;
  input M04_AXI_bvalid;
  input [31:0]M04_AXI_rdata;
  output M04_AXI_rready;
  input [1:0]M04_AXI_rresp;
  input M04_AXI_rvalid;
  output [31:0]M04_AXI_wdata;
  input M04_AXI_wready;
  output [3:0]M04_AXI_wstrb;
  output M04_AXI_wvalid;
  input M05_ACLK;
  input M05_ARESETN;
  output [31:0]M05_AXI_araddr;
  input M05_AXI_arready;
  output M05_AXI_arvalid;
  output [31:0]M05_AXI_awaddr;
  input M05_AXI_awready;
  output M05_AXI_awvalid;
  output M05_AXI_bready;
  input [1:0]M05_AXI_bresp;
  input M05_AXI_bvalid;
  input [31:0]M05_AXI_rdata;
  output M05_AXI_rready;
  input [1:0]M05_AXI_rresp;
  input M05_AXI_rvalid;
  output [31:0]M05_AXI_wdata;
  input M05_AXI_wready;
  output [3:0]M05_AXI_wstrb;
  output M05_AXI_wvalid;
  input M06_ACLK;
  input M06_ARESETN;
  output [31:0]M06_AXI_araddr;
  input M06_AXI_arready;
  output M06_AXI_arvalid;
  output [31:0]M06_AXI_awaddr;
  input M06_AXI_awready;
  output M06_AXI_awvalid;
  output M06_AXI_bready;
  input [1:0]M06_AXI_bresp;
  input M06_AXI_bvalid;
  input [31:0]M06_AXI_rdata;
  output M06_AXI_rready;
  input [1:0]M06_AXI_rresp;
  input M06_AXI_rvalid;
  output [31:0]M06_AXI_wdata;
  input M06_AXI_wready;
  output [3:0]M06_AXI_wstrb;
  output M06_AXI_wvalid;
  input S00_ACLK;
  input S00_ARESETN;
  input [31:0]S00_AXI_araddr;
  input [1:0]S00_AXI_arburst;
  input [3:0]S00_AXI_arcache;
  input [5:0]S00_AXI_arid;
  input [7:0]S00_AXI_arlen;
  input S00_AXI_arlock;
  input [2:0]S00_AXI_arprot;
  input [3:0]S00_AXI_arqos;
  output S00_AXI_arready;
  input [2:0]S00_AXI_arsize;
  input [3:0]S00_AXI_aruser;
  input S00_AXI_arvalid;
  input [31:0]S00_AXI_awaddr;
  input [1:0]S00_AXI_awburst;
  input [3:0]S00_AXI_awcache;
  input [5:0]S00_AXI_awid;
  input [7:0]S00_AXI_awlen;
  input S00_AXI_awlock;
  input [2:0]S00_AXI_awprot;
  input [3:0]S00_AXI_awqos;
  output S00_AXI_awready;
  input [2:0]S00_AXI_awsize;
  input [3:0]S00_AXI_awuser;
  input S00_AXI_awvalid;
  output [5:0]S00_AXI_bid;
  input S00_AXI_bready;
  output [1:0]S00_AXI_bresp;
  output [3:0]S00_AXI_buser;
  output S00_AXI_bvalid;
  output [31:0]S00_AXI_rdata;
  output [5:0]S00_AXI_rid;
  output S00_AXI_rlast;
  input S00_AXI_rready;
  output [1:0]S00_AXI_rresp;
  output [3:0]S00_AXI_ruser;
  output S00_AXI_rvalid;
  input [31:0]S00_AXI_wdata;
  input S00_AXI_wlast;
  output S00_AXI_wready;
  input [3:0]S00_AXI_wstrb;
  input [3:0]S00_AXI_wuser;
  input S00_AXI_wvalid;

  wire axi_interconnect_2_ACLK_net;
  wire axi_interconnect_2_ARESETN_net;
  wire [31:0]axi_interconnect_2_to_s00_couplers_ARADDR;
  wire [1:0]axi_interconnect_2_to_s00_couplers_ARBURST;
  wire [3:0]axi_interconnect_2_to_s00_couplers_ARCACHE;
  wire [5:0]axi_interconnect_2_to_s00_couplers_ARID;
  wire [7:0]axi_interconnect_2_to_s00_couplers_ARLEN;
  wire axi_interconnect_2_to_s00_couplers_ARLOCK;
  wire [2:0]axi_interconnect_2_to_s00_couplers_ARPROT;
  wire [3:0]axi_interconnect_2_to_s00_couplers_ARQOS;
  wire axi_interconnect_2_to_s00_couplers_ARREADY;
  wire [2:0]axi_interconnect_2_to_s00_couplers_ARSIZE;
  wire [3:0]axi_interconnect_2_to_s00_couplers_ARUSER;
  wire axi_interconnect_2_to_s00_couplers_ARVALID;
  wire [31:0]axi_interconnect_2_to_s00_couplers_AWADDR;
  wire [1:0]axi_interconnect_2_to_s00_couplers_AWBURST;
  wire [3:0]axi_interconnect_2_to_s00_couplers_AWCACHE;
  wire [5:0]axi_interconnect_2_to_s00_couplers_AWID;
  wire [7:0]axi_interconnect_2_to_s00_couplers_AWLEN;
  wire axi_interconnect_2_to_s00_couplers_AWLOCK;
  wire [2:0]axi_interconnect_2_to_s00_couplers_AWPROT;
  wire [3:0]axi_interconnect_2_to_s00_couplers_AWQOS;
  wire axi_interconnect_2_to_s00_couplers_AWREADY;
  wire [2:0]axi_interconnect_2_to_s00_couplers_AWSIZE;
  wire [3:0]axi_interconnect_2_to_s00_couplers_AWUSER;
  wire axi_interconnect_2_to_s00_couplers_AWVALID;
  wire [5:0]axi_interconnect_2_to_s00_couplers_BID;
  wire axi_interconnect_2_to_s00_couplers_BREADY;
  wire [1:0]axi_interconnect_2_to_s00_couplers_BRESP;
  wire [3:0]axi_interconnect_2_to_s00_couplers_BUSER;
  wire axi_interconnect_2_to_s00_couplers_BVALID;
  wire [31:0]axi_interconnect_2_to_s00_couplers_RDATA;
  wire [5:0]axi_interconnect_2_to_s00_couplers_RID;
  wire axi_interconnect_2_to_s00_couplers_RLAST;
  wire axi_interconnect_2_to_s00_couplers_RREADY;
  wire [1:0]axi_interconnect_2_to_s00_couplers_RRESP;
  wire [3:0]axi_interconnect_2_to_s00_couplers_RUSER;
  wire axi_interconnect_2_to_s00_couplers_RVALID;
  wire [31:0]axi_interconnect_2_to_s00_couplers_WDATA;
  wire axi_interconnect_2_to_s00_couplers_WLAST;
  wire axi_interconnect_2_to_s00_couplers_WREADY;
  wire [3:0]axi_interconnect_2_to_s00_couplers_WSTRB;
  wire [3:0]axi_interconnect_2_to_s00_couplers_WUSER;
  wire axi_interconnect_2_to_s00_couplers_WVALID;
  wire [31:0]m00_couplers_to_axi_interconnect_2_ARADDR;
  wire [1:0]m00_couplers_to_axi_interconnect_2_ARBURST;
  wire [3:0]m00_couplers_to_axi_interconnect_2_ARCACHE;
  wire [5:0]m00_couplers_to_axi_interconnect_2_ARID;
  wire [7:0]m00_couplers_to_axi_interconnect_2_ARLEN;
  wire [0:0]m00_couplers_to_axi_interconnect_2_ARLOCK;
  wire [2:0]m00_couplers_to_axi_interconnect_2_ARPROT;
  wire [3:0]m00_couplers_to_axi_interconnect_2_ARQOS;
  wire [0:0]m00_couplers_to_axi_interconnect_2_ARREADY;
  wire [3:0]m00_couplers_to_axi_interconnect_2_ARREGION;
  wire [2:0]m00_couplers_to_axi_interconnect_2_ARSIZE;
  wire [3:0]m00_couplers_to_axi_interconnect_2_ARUSER;
  wire [0:0]m00_couplers_to_axi_interconnect_2_ARVALID;
  wire [31:0]m00_couplers_to_axi_interconnect_2_AWADDR;
  wire [1:0]m00_couplers_to_axi_interconnect_2_AWBURST;
  wire [3:0]m00_couplers_to_axi_interconnect_2_AWCACHE;
  wire [5:0]m00_couplers_to_axi_interconnect_2_AWID;
  wire [7:0]m00_couplers_to_axi_interconnect_2_AWLEN;
  wire [0:0]m00_couplers_to_axi_interconnect_2_AWLOCK;
  wire [2:0]m00_couplers_to_axi_interconnect_2_AWPROT;
  wire [3:0]m00_couplers_to_axi_interconnect_2_AWQOS;
  wire [0:0]m00_couplers_to_axi_interconnect_2_AWREADY;
  wire [3:0]m00_couplers_to_axi_interconnect_2_AWREGION;
  wire [2:0]m00_couplers_to_axi_interconnect_2_AWSIZE;
  wire [3:0]m00_couplers_to_axi_interconnect_2_AWUSER;
  wire [0:0]m00_couplers_to_axi_interconnect_2_AWVALID;
  wire [5:0]m00_couplers_to_axi_interconnect_2_BID;
  wire [0:0]m00_couplers_to_axi_interconnect_2_BREADY;
  wire [1:0]m00_couplers_to_axi_interconnect_2_BRESP;
  wire [3:0]m00_couplers_to_axi_interconnect_2_BUSER;
  wire [0:0]m00_couplers_to_axi_interconnect_2_BVALID;
  wire [31:0]m00_couplers_to_axi_interconnect_2_RDATA;
  wire [5:0]m00_couplers_to_axi_interconnect_2_RID;
  wire [0:0]m00_couplers_to_axi_interconnect_2_RLAST;
  wire [0:0]m00_couplers_to_axi_interconnect_2_RREADY;
  wire [1:0]m00_couplers_to_axi_interconnect_2_RRESP;
  wire [3:0]m00_couplers_to_axi_interconnect_2_RUSER;
  wire [0:0]m00_couplers_to_axi_interconnect_2_RVALID;
  wire [31:0]m00_couplers_to_axi_interconnect_2_WDATA;
  wire [0:0]m00_couplers_to_axi_interconnect_2_WLAST;
  wire [0:0]m00_couplers_to_axi_interconnect_2_WREADY;
  wire [3:0]m00_couplers_to_axi_interconnect_2_WSTRB;
  wire [3:0]m00_couplers_to_axi_interconnect_2_WUSER;
  wire [0:0]m00_couplers_to_axi_interconnect_2_WVALID;
  wire [31:0]m01_couplers_to_axi_interconnect_2_ARADDR;
  wire m01_couplers_to_axi_interconnect_2_ARREADY;
  wire m01_couplers_to_axi_interconnect_2_ARVALID;
  wire [31:0]m01_couplers_to_axi_interconnect_2_AWADDR;
  wire m01_couplers_to_axi_interconnect_2_AWREADY;
  wire m01_couplers_to_axi_interconnect_2_AWVALID;
  wire m01_couplers_to_axi_interconnect_2_BREADY;
  wire [1:0]m01_couplers_to_axi_interconnect_2_BRESP;
  wire m01_couplers_to_axi_interconnect_2_BVALID;
  wire [31:0]m01_couplers_to_axi_interconnect_2_RDATA;
  wire m01_couplers_to_axi_interconnect_2_RREADY;
  wire [1:0]m01_couplers_to_axi_interconnect_2_RRESP;
  wire m01_couplers_to_axi_interconnect_2_RVALID;
  wire [31:0]m01_couplers_to_axi_interconnect_2_WDATA;
  wire m01_couplers_to_axi_interconnect_2_WREADY;
  wire [3:0]m01_couplers_to_axi_interconnect_2_WSTRB;
  wire m01_couplers_to_axi_interconnect_2_WVALID;
  wire [31:0]m02_couplers_to_axi_interconnect_2_ARADDR;
  wire m02_couplers_to_axi_interconnect_2_ARREADY;
  wire m02_couplers_to_axi_interconnect_2_ARVALID;
  wire [31:0]m02_couplers_to_axi_interconnect_2_AWADDR;
  wire m02_couplers_to_axi_interconnect_2_AWREADY;
  wire m02_couplers_to_axi_interconnect_2_AWVALID;
  wire m02_couplers_to_axi_interconnect_2_BREADY;
  wire [1:0]m02_couplers_to_axi_interconnect_2_BRESP;
  wire m02_couplers_to_axi_interconnect_2_BVALID;
  wire [31:0]m02_couplers_to_axi_interconnect_2_RDATA;
  wire m02_couplers_to_axi_interconnect_2_RREADY;
  wire [1:0]m02_couplers_to_axi_interconnect_2_RRESP;
  wire m02_couplers_to_axi_interconnect_2_RVALID;
  wire [31:0]m02_couplers_to_axi_interconnect_2_WDATA;
  wire m02_couplers_to_axi_interconnect_2_WREADY;
  wire [3:0]m02_couplers_to_axi_interconnect_2_WSTRB;
  wire m02_couplers_to_axi_interconnect_2_WVALID;
  wire [31:0]m03_couplers_to_axi_interconnect_2_ARADDR;
  wire [1:0]m03_couplers_to_axi_interconnect_2_ARBURST;
  wire [3:0]m03_couplers_to_axi_interconnect_2_ARCACHE;
  wire [5:0]m03_couplers_to_axi_interconnect_2_ARID;
  wire [7:0]m03_couplers_to_axi_interconnect_2_ARLEN;
  wire [0:0]m03_couplers_to_axi_interconnect_2_ARLOCK;
  wire [2:0]m03_couplers_to_axi_interconnect_2_ARPROT;
  wire [3:0]m03_couplers_to_axi_interconnect_2_ARQOS;
  wire m03_couplers_to_axi_interconnect_2_ARREADY;
  wire [3:0]m03_couplers_to_axi_interconnect_2_ARREGION;
  wire [2:0]m03_couplers_to_axi_interconnect_2_ARSIZE;
  wire [3:0]m03_couplers_to_axi_interconnect_2_ARUSER;
  wire m03_couplers_to_axi_interconnect_2_ARVALID;
  wire [31:0]m03_couplers_to_axi_interconnect_2_AWADDR;
  wire [1:0]m03_couplers_to_axi_interconnect_2_AWBURST;
  wire [3:0]m03_couplers_to_axi_interconnect_2_AWCACHE;
  wire [5:0]m03_couplers_to_axi_interconnect_2_AWID;
  wire [7:0]m03_couplers_to_axi_interconnect_2_AWLEN;
  wire [0:0]m03_couplers_to_axi_interconnect_2_AWLOCK;
  wire [2:0]m03_couplers_to_axi_interconnect_2_AWPROT;
  wire [3:0]m03_couplers_to_axi_interconnect_2_AWQOS;
  wire m03_couplers_to_axi_interconnect_2_AWREADY;
  wire [3:0]m03_couplers_to_axi_interconnect_2_AWREGION;
  wire [2:0]m03_couplers_to_axi_interconnect_2_AWSIZE;
  wire [3:0]m03_couplers_to_axi_interconnect_2_AWUSER;
  wire m03_couplers_to_axi_interconnect_2_AWVALID;
  wire [5:0]m03_couplers_to_axi_interconnect_2_BID;
  wire m03_couplers_to_axi_interconnect_2_BREADY;
  wire [1:0]m03_couplers_to_axi_interconnect_2_BRESP;
  wire [3:0]m03_couplers_to_axi_interconnect_2_BUSER;
  wire m03_couplers_to_axi_interconnect_2_BVALID;
  wire [31:0]m03_couplers_to_axi_interconnect_2_RDATA;
  wire [5:0]m03_couplers_to_axi_interconnect_2_RID;
  wire m03_couplers_to_axi_interconnect_2_RLAST;
  wire m03_couplers_to_axi_interconnect_2_RREADY;
  wire [1:0]m03_couplers_to_axi_interconnect_2_RRESP;
  wire [3:0]m03_couplers_to_axi_interconnect_2_RUSER;
  wire m03_couplers_to_axi_interconnect_2_RVALID;
  wire [31:0]m03_couplers_to_axi_interconnect_2_WDATA;
  wire m03_couplers_to_axi_interconnect_2_WLAST;
  wire m03_couplers_to_axi_interconnect_2_WREADY;
  wire [3:0]m03_couplers_to_axi_interconnect_2_WSTRB;
  wire [3:0]m03_couplers_to_axi_interconnect_2_WUSER;
  wire m03_couplers_to_axi_interconnect_2_WVALID;
  wire [31:0]m04_couplers_to_axi_interconnect_2_ARADDR;
  wire m04_couplers_to_axi_interconnect_2_ARREADY;
  wire m04_couplers_to_axi_interconnect_2_ARVALID;
  wire [31:0]m04_couplers_to_axi_interconnect_2_AWADDR;
  wire m04_couplers_to_axi_interconnect_2_AWREADY;
  wire m04_couplers_to_axi_interconnect_2_AWVALID;
  wire m04_couplers_to_axi_interconnect_2_BREADY;
  wire [1:0]m04_couplers_to_axi_interconnect_2_BRESP;
  wire m04_couplers_to_axi_interconnect_2_BVALID;
  wire [31:0]m04_couplers_to_axi_interconnect_2_RDATA;
  wire m04_couplers_to_axi_interconnect_2_RREADY;
  wire [1:0]m04_couplers_to_axi_interconnect_2_RRESP;
  wire m04_couplers_to_axi_interconnect_2_RVALID;
  wire [31:0]m04_couplers_to_axi_interconnect_2_WDATA;
  wire m04_couplers_to_axi_interconnect_2_WREADY;
  wire [3:0]m04_couplers_to_axi_interconnect_2_WSTRB;
  wire m04_couplers_to_axi_interconnect_2_WVALID;
  wire [31:0]m05_couplers_to_axi_interconnect_2_ARADDR;
  wire m05_couplers_to_axi_interconnect_2_ARREADY;
  wire m05_couplers_to_axi_interconnect_2_ARVALID;
  wire [31:0]m05_couplers_to_axi_interconnect_2_AWADDR;
  wire m05_couplers_to_axi_interconnect_2_AWREADY;
  wire m05_couplers_to_axi_interconnect_2_AWVALID;
  wire m05_couplers_to_axi_interconnect_2_BREADY;
  wire [1:0]m05_couplers_to_axi_interconnect_2_BRESP;
  wire m05_couplers_to_axi_interconnect_2_BVALID;
  wire [31:0]m05_couplers_to_axi_interconnect_2_RDATA;
  wire m05_couplers_to_axi_interconnect_2_RREADY;
  wire [1:0]m05_couplers_to_axi_interconnect_2_RRESP;
  wire m05_couplers_to_axi_interconnect_2_RVALID;
  wire [31:0]m05_couplers_to_axi_interconnect_2_WDATA;
  wire m05_couplers_to_axi_interconnect_2_WREADY;
  wire [3:0]m05_couplers_to_axi_interconnect_2_WSTRB;
  wire m05_couplers_to_axi_interconnect_2_WVALID;
  wire [31:0]m06_couplers_to_axi_interconnect_2_ARADDR;
  wire m06_couplers_to_axi_interconnect_2_ARREADY;
  wire m06_couplers_to_axi_interconnect_2_ARVALID;
  wire [31:0]m06_couplers_to_axi_interconnect_2_AWADDR;
  wire m06_couplers_to_axi_interconnect_2_AWREADY;
  wire m06_couplers_to_axi_interconnect_2_AWVALID;
  wire m06_couplers_to_axi_interconnect_2_BREADY;
  wire [1:0]m06_couplers_to_axi_interconnect_2_BRESP;
  wire m06_couplers_to_axi_interconnect_2_BVALID;
  wire [31:0]m06_couplers_to_axi_interconnect_2_RDATA;
  wire m06_couplers_to_axi_interconnect_2_RREADY;
  wire [1:0]m06_couplers_to_axi_interconnect_2_RRESP;
  wire m06_couplers_to_axi_interconnect_2_RVALID;
  wire [31:0]m06_couplers_to_axi_interconnect_2_WDATA;
  wire m06_couplers_to_axi_interconnect_2_WREADY;
  wire [3:0]m06_couplers_to_axi_interconnect_2_WSTRB;
  wire m06_couplers_to_axi_interconnect_2_WVALID;
  wire [31:0]s00_couplers_to_xbar_ARADDR;
  wire [1:0]s00_couplers_to_xbar_ARBURST;
  wire [3:0]s00_couplers_to_xbar_ARCACHE;
  wire [5:0]s00_couplers_to_xbar_ARID;
  wire [7:0]s00_couplers_to_xbar_ARLEN;
  wire s00_couplers_to_xbar_ARLOCK;
  wire [2:0]s00_couplers_to_xbar_ARPROT;
  wire [3:0]s00_couplers_to_xbar_ARQOS;
  wire [0:0]s00_couplers_to_xbar_ARREADY;
  wire [2:0]s00_couplers_to_xbar_ARSIZE;
  wire [3:0]s00_couplers_to_xbar_ARUSER;
  wire s00_couplers_to_xbar_ARVALID;
  wire [31:0]s00_couplers_to_xbar_AWADDR;
  wire [1:0]s00_couplers_to_xbar_AWBURST;
  wire [3:0]s00_couplers_to_xbar_AWCACHE;
  wire [5:0]s00_couplers_to_xbar_AWID;
  wire [7:0]s00_couplers_to_xbar_AWLEN;
  wire s00_couplers_to_xbar_AWLOCK;
  wire [2:0]s00_couplers_to_xbar_AWPROT;
  wire [3:0]s00_couplers_to_xbar_AWQOS;
  wire [0:0]s00_couplers_to_xbar_AWREADY;
  wire [2:0]s00_couplers_to_xbar_AWSIZE;
  wire [3:0]s00_couplers_to_xbar_AWUSER;
  wire s00_couplers_to_xbar_AWVALID;
  wire [5:0]s00_couplers_to_xbar_BID;
  wire s00_couplers_to_xbar_BREADY;
  wire [1:0]s00_couplers_to_xbar_BRESP;
  wire [3:0]s00_couplers_to_xbar_BUSER;
  wire [0:0]s00_couplers_to_xbar_BVALID;
  wire [31:0]s00_couplers_to_xbar_RDATA;
  wire [5:0]s00_couplers_to_xbar_RID;
  wire [0:0]s00_couplers_to_xbar_RLAST;
  wire s00_couplers_to_xbar_RREADY;
  wire [1:0]s00_couplers_to_xbar_RRESP;
  wire [3:0]s00_couplers_to_xbar_RUSER;
  wire [0:0]s00_couplers_to_xbar_RVALID;
  wire [31:0]s00_couplers_to_xbar_WDATA;
  wire s00_couplers_to_xbar_WLAST;
  wire [0:0]s00_couplers_to_xbar_WREADY;
  wire [3:0]s00_couplers_to_xbar_WSTRB;
  wire [3:0]s00_couplers_to_xbar_WUSER;
  wire s00_couplers_to_xbar_WVALID;
  wire [31:0]xbar_to_m00_couplers_ARADDR;
  wire [1:0]xbar_to_m00_couplers_ARBURST;
  wire [3:0]xbar_to_m00_couplers_ARCACHE;
  wire [5:0]xbar_to_m00_couplers_ARID;
  wire [7:0]xbar_to_m00_couplers_ARLEN;
  wire [0:0]xbar_to_m00_couplers_ARLOCK;
  wire [2:0]xbar_to_m00_couplers_ARPROT;
  wire [3:0]xbar_to_m00_couplers_ARQOS;
  wire [0:0]xbar_to_m00_couplers_ARREADY;
  wire [3:0]xbar_to_m00_couplers_ARREGION;
  wire [2:0]xbar_to_m00_couplers_ARSIZE;
  wire [3:0]xbar_to_m00_couplers_ARUSER;
  wire [0:0]xbar_to_m00_couplers_ARVALID;
  wire [31:0]xbar_to_m00_couplers_AWADDR;
  wire [1:0]xbar_to_m00_couplers_AWBURST;
  wire [3:0]xbar_to_m00_couplers_AWCACHE;
  wire [5:0]xbar_to_m00_couplers_AWID;
  wire [7:0]xbar_to_m00_couplers_AWLEN;
  wire [0:0]xbar_to_m00_couplers_AWLOCK;
  wire [2:0]xbar_to_m00_couplers_AWPROT;
  wire [3:0]xbar_to_m00_couplers_AWQOS;
  wire [0:0]xbar_to_m00_couplers_AWREADY;
  wire [3:0]xbar_to_m00_couplers_AWREGION;
  wire [2:0]xbar_to_m00_couplers_AWSIZE;
  wire [3:0]xbar_to_m00_couplers_AWUSER;
  wire [0:0]xbar_to_m00_couplers_AWVALID;
  wire [5:0]xbar_to_m00_couplers_BID;
  wire [0:0]xbar_to_m00_couplers_BREADY;
  wire [1:0]xbar_to_m00_couplers_BRESP;
  wire [3:0]xbar_to_m00_couplers_BUSER;
  wire [0:0]xbar_to_m00_couplers_BVALID;
  wire [31:0]xbar_to_m00_couplers_RDATA;
  wire [5:0]xbar_to_m00_couplers_RID;
  wire [0:0]xbar_to_m00_couplers_RLAST;
  wire [0:0]xbar_to_m00_couplers_RREADY;
  wire [1:0]xbar_to_m00_couplers_RRESP;
  wire [3:0]xbar_to_m00_couplers_RUSER;
  wire [0:0]xbar_to_m00_couplers_RVALID;
  wire [31:0]xbar_to_m00_couplers_WDATA;
  wire [0:0]xbar_to_m00_couplers_WLAST;
  wire [0:0]xbar_to_m00_couplers_WREADY;
  wire [3:0]xbar_to_m00_couplers_WSTRB;
  wire [3:0]xbar_to_m00_couplers_WUSER;
  wire [0:0]xbar_to_m00_couplers_WVALID;
  wire [63:32]xbar_to_m01_couplers_ARADDR;
  wire [3:2]xbar_to_m01_couplers_ARBURST;
  wire [7:4]xbar_to_m01_couplers_ARCACHE;
  wire [11:6]xbar_to_m01_couplers_ARID;
  wire [15:8]xbar_to_m01_couplers_ARLEN;
  wire [1:1]xbar_to_m01_couplers_ARLOCK;
  wire [5:3]xbar_to_m01_couplers_ARPROT;
  wire [7:4]xbar_to_m01_couplers_ARQOS;
  wire xbar_to_m01_couplers_ARREADY;
  wire [7:4]xbar_to_m01_couplers_ARREGION;
  wire [5:3]xbar_to_m01_couplers_ARSIZE;
  wire [1:1]xbar_to_m01_couplers_ARVALID;
  wire [63:32]xbar_to_m01_couplers_AWADDR;
  wire [3:2]xbar_to_m01_couplers_AWBURST;
  wire [7:4]xbar_to_m01_couplers_AWCACHE;
  wire [11:6]xbar_to_m01_couplers_AWID;
  wire [15:8]xbar_to_m01_couplers_AWLEN;
  wire [1:1]xbar_to_m01_couplers_AWLOCK;
  wire [5:3]xbar_to_m01_couplers_AWPROT;
  wire [7:4]xbar_to_m01_couplers_AWQOS;
  wire xbar_to_m01_couplers_AWREADY;
  wire [7:4]xbar_to_m01_couplers_AWREGION;
  wire [5:3]xbar_to_m01_couplers_AWSIZE;
  wire [1:1]xbar_to_m01_couplers_AWVALID;
  wire [5:0]xbar_to_m01_couplers_BID;
  wire [1:1]xbar_to_m01_couplers_BREADY;
  wire [1:0]xbar_to_m01_couplers_BRESP;
  wire xbar_to_m01_couplers_BVALID;
  wire [31:0]xbar_to_m01_couplers_RDATA;
  wire [5:0]xbar_to_m01_couplers_RID;
  wire xbar_to_m01_couplers_RLAST;
  wire [1:1]xbar_to_m01_couplers_RREADY;
  wire [1:0]xbar_to_m01_couplers_RRESP;
  wire xbar_to_m01_couplers_RVALID;
  wire [63:32]xbar_to_m01_couplers_WDATA;
  wire [1:1]xbar_to_m01_couplers_WLAST;
  wire xbar_to_m01_couplers_WREADY;
  wire [7:4]xbar_to_m01_couplers_WSTRB;
  wire [1:1]xbar_to_m01_couplers_WVALID;
  wire [95:64]xbar_to_m02_couplers_ARADDR;
  wire [5:4]xbar_to_m02_couplers_ARBURST;
  wire [11:8]xbar_to_m02_couplers_ARCACHE;
  wire [17:12]xbar_to_m02_couplers_ARID;
  wire [23:16]xbar_to_m02_couplers_ARLEN;
  wire [2:2]xbar_to_m02_couplers_ARLOCK;
  wire [8:6]xbar_to_m02_couplers_ARPROT;
  wire [11:8]xbar_to_m02_couplers_ARQOS;
  wire xbar_to_m02_couplers_ARREADY;
  wire [11:8]xbar_to_m02_couplers_ARREGION;
  wire [8:6]xbar_to_m02_couplers_ARSIZE;
  wire [2:2]xbar_to_m02_couplers_ARVALID;
  wire [95:64]xbar_to_m02_couplers_AWADDR;
  wire [5:4]xbar_to_m02_couplers_AWBURST;
  wire [11:8]xbar_to_m02_couplers_AWCACHE;
  wire [17:12]xbar_to_m02_couplers_AWID;
  wire [23:16]xbar_to_m02_couplers_AWLEN;
  wire [2:2]xbar_to_m02_couplers_AWLOCK;
  wire [8:6]xbar_to_m02_couplers_AWPROT;
  wire [11:8]xbar_to_m02_couplers_AWQOS;
  wire xbar_to_m02_couplers_AWREADY;
  wire [11:8]xbar_to_m02_couplers_AWREGION;
  wire [8:6]xbar_to_m02_couplers_AWSIZE;
  wire [2:2]xbar_to_m02_couplers_AWVALID;
  wire [5:0]xbar_to_m02_couplers_BID;
  wire [2:2]xbar_to_m02_couplers_BREADY;
  wire [1:0]xbar_to_m02_couplers_BRESP;
  wire xbar_to_m02_couplers_BVALID;
  wire [31:0]xbar_to_m02_couplers_RDATA;
  wire [5:0]xbar_to_m02_couplers_RID;
  wire xbar_to_m02_couplers_RLAST;
  wire [2:2]xbar_to_m02_couplers_RREADY;
  wire [1:0]xbar_to_m02_couplers_RRESP;
  wire xbar_to_m02_couplers_RVALID;
  wire [95:64]xbar_to_m02_couplers_WDATA;
  wire [2:2]xbar_to_m02_couplers_WLAST;
  wire xbar_to_m02_couplers_WREADY;
  wire [11:8]xbar_to_m02_couplers_WSTRB;
  wire [2:2]xbar_to_m02_couplers_WVALID;
  wire [127:96]xbar_to_m03_couplers_ARADDR;
  wire [7:6]xbar_to_m03_couplers_ARBURST;
  wire [15:12]xbar_to_m03_couplers_ARCACHE;
  wire [23:18]xbar_to_m03_couplers_ARID;
  wire [31:24]xbar_to_m03_couplers_ARLEN;
  wire [3:3]xbar_to_m03_couplers_ARLOCK;
  wire [11:9]xbar_to_m03_couplers_ARPROT;
  wire [15:12]xbar_to_m03_couplers_ARQOS;
  wire xbar_to_m03_couplers_ARREADY;
  wire [15:12]xbar_to_m03_couplers_ARREGION;
  wire [11:9]xbar_to_m03_couplers_ARSIZE;
  wire [15:12]xbar_to_m03_couplers_ARUSER;
  wire [3:3]xbar_to_m03_couplers_ARVALID;
  wire [127:96]xbar_to_m03_couplers_AWADDR;
  wire [7:6]xbar_to_m03_couplers_AWBURST;
  wire [15:12]xbar_to_m03_couplers_AWCACHE;
  wire [23:18]xbar_to_m03_couplers_AWID;
  wire [31:24]xbar_to_m03_couplers_AWLEN;
  wire [3:3]xbar_to_m03_couplers_AWLOCK;
  wire [11:9]xbar_to_m03_couplers_AWPROT;
  wire [15:12]xbar_to_m03_couplers_AWQOS;
  wire xbar_to_m03_couplers_AWREADY;
  wire [15:12]xbar_to_m03_couplers_AWREGION;
  wire [11:9]xbar_to_m03_couplers_AWSIZE;
  wire [15:12]xbar_to_m03_couplers_AWUSER;
  wire [3:3]xbar_to_m03_couplers_AWVALID;
  wire [5:0]xbar_to_m03_couplers_BID;
  wire [3:3]xbar_to_m03_couplers_BREADY;
  wire [1:0]xbar_to_m03_couplers_BRESP;
  wire [3:0]xbar_to_m03_couplers_BUSER;
  wire xbar_to_m03_couplers_BVALID;
  wire [31:0]xbar_to_m03_couplers_RDATA;
  wire [5:0]xbar_to_m03_couplers_RID;
  wire xbar_to_m03_couplers_RLAST;
  wire [3:3]xbar_to_m03_couplers_RREADY;
  wire [1:0]xbar_to_m03_couplers_RRESP;
  wire [3:0]xbar_to_m03_couplers_RUSER;
  wire xbar_to_m03_couplers_RVALID;
  wire [127:96]xbar_to_m03_couplers_WDATA;
  wire [3:3]xbar_to_m03_couplers_WLAST;
  wire xbar_to_m03_couplers_WREADY;
  wire [15:12]xbar_to_m03_couplers_WSTRB;
  wire [15:12]xbar_to_m03_couplers_WUSER;
  wire [3:3]xbar_to_m03_couplers_WVALID;
  wire [159:128]xbar_to_m04_couplers_ARADDR;
  wire [9:8]xbar_to_m04_couplers_ARBURST;
  wire [19:16]xbar_to_m04_couplers_ARCACHE;
  wire [29:24]xbar_to_m04_couplers_ARID;
  wire [39:32]xbar_to_m04_couplers_ARLEN;
  wire [4:4]xbar_to_m04_couplers_ARLOCK;
  wire [14:12]xbar_to_m04_couplers_ARPROT;
  wire [19:16]xbar_to_m04_couplers_ARQOS;
  wire xbar_to_m04_couplers_ARREADY;
  wire [19:16]xbar_to_m04_couplers_ARREGION;
  wire [14:12]xbar_to_m04_couplers_ARSIZE;
  wire [4:4]xbar_to_m04_couplers_ARVALID;
  wire [159:128]xbar_to_m04_couplers_AWADDR;
  wire [9:8]xbar_to_m04_couplers_AWBURST;
  wire [19:16]xbar_to_m04_couplers_AWCACHE;
  wire [29:24]xbar_to_m04_couplers_AWID;
  wire [39:32]xbar_to_m04_couplers_AWLEN;
  wire [4:4]xbar_to_m04_couplers_AWLOCK;
  wire [14:12]xbar_to_m04_couplers_AWPROT;
  wire [19:16]xbar_to_m04_couplers_AWQOS;
  wire xbar_to_m04_couplers_AWREADY;
  wire [19:16]xbar_to_m04_couplers_AWREGION;
  wire [14:12]xbar_to_m04_couplers_AWSIZE;
  wire [4:4]xbar_to_m04_couplers_AWVALID;
  wire [5:0]xbar_to_m04_couplers_BID;
  wire [4:4]xbar_to_m04_couplers_BREADY;
  wire [1:0]xbar_to_m04_couplers_BRESP;
  wire xbar_to_m04_couplers_BVALID;
  wire [31:0]xbar_to_m04_couplers_RDATA;
  wire [5:0]xbar_to_m04_couplers_RID;
  wire xbar_to_m04_couplers_RLAST;
  wire [4:4]xbar_to_m04_couplers_RREADY;
  wire [1:0]xbar_to_m04_couplers_RRESP;
  wire xbar_to_m04_couplers_RVALID;
  wire [159:128]xbar_to_m04_couplers_WDATA;
  wire [4:4]xbar_to_m04_couplers_WLAST;
  wire xbar_to_m04_couplers_WREADY;
  wire [19:16]xbar_to_m04_couplers_WSTRB;
  wire [4:4]xbar_to_m04_couplers_WVALID;
  wire [191:160]xbar_to_m05_couplers_ARADDR;
  wire [11:10]xbar_to_m05_couplers_ARBURST;
  wire [23:20]xbar_to_m05_couplers_ARCACHE;
  wire [35:30]xbar_to_m05_couplers_ARID;
  wire [47:40]xbar_to_m05_couplers_ARLEN;
  wire [5:5]xbar_to_m05_couplers_ARLOCK;
  wire [17:15]xbar_to_m05_couplers_ARPROT;
  wire [23:20]xbar_to_m05_couplers_ARQOS;
  wire xbar_to_m05_couplers_ARREADY;
  wire [23:20]xbar_to_m05_couplers_ARREGION;
  wire [17:15]xbar_to_m05_couplers_ARSIZE;
  wire [5:5]xbar_to_m05_couplers_ARVALID;
  wire [191:160]xbar_to_m05_couplers_AWADDR;
  wire [11:10]xbar_to_m05_couplers_AWBURST;
  wire [23:20]xbar_to_m05_couplers_AWCACHE;
  wire [35:30]xbar_to_m05_couplers_AWID;
  wire [47:40]xbar_to_m05_couplers_AWLEN;
  wire [5:5]xbar_to_m05_couplers_AWLOCK;
  wire [17:15]xbar_to_m05_couplers_AWPROT;
  wire [23:20]xbar_to_m05_couplers_AWQOS;
  wire xbar_to_m05_couplers_AWREADY;
  wire [23:20]xbar_to_m05_couplers_AWREGION;
  wire [17:15]xbar_to_m05_couplers_AWSIZE;
  wire [5:5]xbar_to_m05_couplers_AWVALID;
  wire [5:0]xbar_to_m05_couplers_BID;
  wire [5:5]xbar_to_m05_couplers_BREADY;
  wire [1:0]xbar_to_m05_couplers_BRESP;
  wire xbar_to_m05_couplers_BVALID;
  wire [31:0]xbar_to_m05_couplers_RDATA;
  wire [5:0]xbar_to_m05_couplers_RID;
  wire xbar_to_m05_couplers_RLAST;
  wire [5:5]xbar_to_m05_couplers_RREADY;
  wire [1:0]xbar_to_m05_couplers_RRESP;
  wire xbar_to_m05_couplers_RVALID;
  wire [191:160]xbar_to_m05_couplers_WDATA;
  wire [5:5]xbar_to_m05_couplers_WLAST;
  wire xbar_to_m05_couplers_WREADY;
  wire [23:20]xbar_to_m05_couplers_WSTRB;
  wire [5:5]xbar_to_m05_couplers_WVALID;
  wire [223:192]xbar_to_m06_couplers_ARADDR;
  wire [13:12]xbar_to_m06_couplers_ARBURST;
  wire [27:24]xbar_to_m06_couplers_ARCACHE;
  wire [41:36]xbar_to_m06_couplers_ARID;
  wire [55:48]xbar_to_m06_couplers_ARLEN;
  wire [6:6]xbar_to_m06_couplers_ARLOCK;
  wire [20:18]xbar_to_m06_couplers_ARPROT;
  wire [27:24]xbar_to_m06_couplers_ARQOS;
  wire xbar_to_m06_couplers_ARREADY;
  wire [27:24]xbar_to_m06_couplers_ARREGION;
  wire [20:18]xbar_to_m06_couplers_ARSIZE;
  wire [6:6]xbar_to_m06_couplers_ARVALID;
  wire [223:192]xbar_to_m06_couplers_AWADDR;
  wire [13:12]xbar_to_m06_couplers_AWBURST;
  wire [27:24]xbar_to_m06_couplers_AWCACHE;
  wire [41:36]xbar_to_m06_couplers_AWID;
  wire [55:48]xbar_to_m06_couplers_AWLEN;
  wire [6:6]xbar_to_m06_couplers_AWLOCK;
  wire [20:18]xbar_to_m06_couplers_AWPROT;
  wire [27:24]xbar_to_m06_couplers_AWQOS;
  wire xbar_to_m06_couplers_AWREADY;
  wire [27:24]xbar_to_m06_couplers_AWREGION;
  wire [20:18]xbar_to_m06_couplers_AWSIZE;
  wire [6:6]xbar_to_m06_couplers_AWVALID;
  wire [5:0]xbar_to_m06_couplers_BID;
  wire [6:6]xbar_to_m06_couplers_BREADY;
  wire [1:0]xbar_to_m06_couplers_BRESP;
  wire xbar_to_m06_couplers_BVALID;
  wire [31:0]xbar_to_m06_couplers_RDATA;
  wire [5:0]xbar_to_m06_couplers_RID;
  wire xbar_to_m06_couplers_RLAST;
  wire [6:6]xbar_to_m06_couplers_RREADY;
  wire [1:0]xbar_to_m06_couplers_RRESP;
  wire xbar_to_m06_couplers_RVALID;
  wire [223:192]xbar_to_m06_couplers_WDATA;
  wire [6:6]xbar_to_m06_couplers_WLAST;
  wire xbar_to_m06_couplers_WREADY;
  wire [27:24]xbar_to_m06_couplers_WSTRB;
  wire [6:6]xbar_to_m06_couplers_WVALID;
  wire [27:0]NLW_xbar_m_axi_aruser_UNCONNECTED;
  wire [27:0]NLW_xbar_m_axi_awuser_UNCONNECTED;
  wire [27:0]NLW_xbar_m_axi_wuser_UNCONNECTED;

  assign M00_AXI_araddr[31:0] = m00_couplers_to_axi_interconnect_2_ARADDR;
  assign M00_AXI_arburst[1:0] = m00_couplers_to_axi_interconnect_2_ARBURST;
  assign M00_AXI_arcache[3:0] = m00_couplers_to_axi_interconnect_2_ARCACHE;
  assign M00_AXI_arid[5:0] = m00_couplers_to_axi_interconnect_2_ARID;
  assign M00_AXI_arlen[7:0] = m00_couplers_to_axi_interconnect_2_ARLEN;
  assign M00_AXI_arlock[0] = m00_couplers_to_axi_interconnect_2_ARLOCK;
  assign M00_AXI_arprot[2:0] = m00_couplers_to_axi_interconnect_2_ARPROT;
  assign M00_AXI_arqos[3:0] = m00_couplers_to_axi_interconnect_2_ARQOS;
  assign M00_AXI_arregion[3:0] = m00_couplers_to_axi_interconnect_2_ARREGION;
  assign M00_AXI_arsize[2:0] = m00_couplers_to_axi_interconnect_2_ARSIZE;
  assign M00_AXI_aruser[3:0] = m00_couplers_to_axi_interconnect_2_ARUSER;
  assign M00_AXI_arvalid[0] = m00_couplers_to_axi_interconnect_2_ARVALID;
  assign M00_AXI_awaddr[31:0] = m00_couplers_to_axi_interconnect_2_AWADDR;
  assign M00_AXI_awburst[1:0] = m00_couplers_to_axi_interconnect_2_AWBURST;
  assign M00_AXI_awcache[3:0] = m00_couplers_to_axi_interconnect_2_AWCACHE;
  assign M00_AXI_awid[5:0] = m00_couplers_to_axi_interconnect_2_AWID;
  assign M00_AXI_awlen[7:0] = m00_couplers_to_axi_interconnect_2_AWLEN;
  assign M00_AXI_awlock[0] = m00_couplers_to_axi_interconnect_2_AWLOCK;
  assign M00_AXI_awprot[2:0] = m00_couplers_to_axi_interconnect_2_AWPROT;
  assign M00_AXI_awqos[3:0] = m00_couplers_to_axi_interconnect_2_AWQOS;
  assign M00_AXI_awregion[3:0] = m00_couplers_to_axi_interconnect_2_AWREGION;
  assign M00_AXI_awsize[2:0] = m00_couplers_to_axi_interconnect_2_AWSIZE;
  assign M00_AXI_awuser[3:0] = m00_couplers_to_axi_interconnect_2_AWUSER;
  assign M00_AXI_awvalid[0] = m00_couplers_to_axi_interconnect_2_AWVALID;
  assign M00_AXI_bready[0] = m00_couplers_to_axi_interconnect_2_BREADY;
  assign M00_AXI_rready[0] = m00_couplers_to_axi_interconnect_2_RREADY;
  assign M00_AXI_wdata[31:0] = m00_couplers_to_axi_interconnect_2_WDATA;
  assign M00_AXI_wlast[0] = m00_couplers_to_axi_interconnect_2_WLAST;
  assign M00_AXI_wstrb[3:0] = m00_couplers_to_axi_interconnect_2_WSTRB;
  assign M00_AXI_wuser[3:0] = m00_couplers_to_axi_interconnect_2_WUSER;
  assign M00_AXI_wvalid[0] = m00_couplers_to_axi_interconnect_2_WVALID;
  assign M01_AXI_araddr[31:0] = m01_couplers_to_axi_interconnect_2_ARADDR;
  assign M01_AXI_arvalid = m01_couplers_to_axi_interconnect_2_ARVALID;
  assign M01_AXI_awaddr[31:0] = m01_couplers_to_axi_interconnect_2_AWADDR;
  assign M01_AXI_awvalid = m01_couplers_to_axi_interconnect_2_AWVALID;
  assign M01_AXI_bready = m01_couplers_to_axi_interconnect_2_BREADY;
  assign M01_AXI_rready = m01_couplers_to_axi_interconnect_2_RREADY;
  assign M01_AXI_wdata[31:0] = m01_couplers_to_axi_interconnect_2_WDATA;
  assign M01_AXI_wstrb[3:0] = m01_couplers_to_axi_interconnect_2_WSTRB;
  assign M01_AXI_wvalid = m01_couplers_to_axi_interconnect_2_WVALID;
  assign M02_AXI_araddr[31:0] = m02_couplers_to_axi_interconnect_2_ARADDR;
  assign M02_AXI_arvalid = m02_couplers_to_axi_interconnect_2_ARVALID;
  assign M02_AXI_awaddr[31:0] = m02_couplers_to_axi_interconnect_2_AWADDR;
  assign M02_AXI_awvalid = m02_couplers_to_axi_interconnect_2_AWVALID;
  assign M02_AXI_bready = m02_couplers_to_axi_interconnect_2_BREADY;
  assign M02_AXI_rready = m02_couplers_to_axi_interconnect_2_RREADY;
  assign M02_AXI_wdata[31:0] = m02_couplers_to_axi_interconnect_2_WDATA;
  assign M02_AXI_wstrb[3:0] = m02_couplers_to_axi_interconnect_2_WSTRB;
  assign M02_AXI_wvalid = m02_couplers_to_axi_interconnect_2_WVALID;
  assign M03_AXI_araddr[31:0] = m03_couplers_to_axi_interconnect_2_ARADDR;
  assign M03_AXI_arburst[1:0] = m03_couplers_to_axi_interconnect_2_ARBURST;
  assign M03_AXI_arcache[3:0] = m03_couplers_to_axi_interconnect_2_ARCACHE;
  assign M03_AXI_arid[5:0] = m03_couplers_to_axi_interconnect_2_ARID;
  assign M03_AXI_arlen[7:0] = m03_couplers_to_axi_interconnect_2_ARLEN;
  assign M03_AXI_arlock[0] = m03_couplers_to_axi_interconnect_2_ARLOCK;
  assign M03_AXI_arprot[2:0] = m03_couplers_to_axi_interconnect_2_ARPROT;
  assign M03_AXI_arqos[3:0] = m03_couplers_to_axi_interconnect_2_ARQOS;
  assign M03_AXI_arregion[3:0] = m03_couplers_to_axi_interconnect_2_ARREGION;
  assign M03_AXI_arsize[2:0] = m03_couplers_to_axi_interconnect_2_ARSIZE;
  assign M03_AXI_aruser[3:0] = m03_couplers_to_axi_interconnect_2_ARUSER;
  assign M03_AXI_arvalid = m03_couplers_to_axi_interconnect_2_ARVALID;
  assign M03_AXI_awaddr[31:0] = m03_couplers_to_axi_interconnect_2_AWADDR;
  assign M03_AXI_awburst[1:0] = m03_couplers_to_axi_interconnect_2_AWBURST;
  assign M03_AXI_awcache[3:0] = m03_couplers_to_axi_interconnect_2_AWCACHE;
  assign M03_AXI_awid[5:0] = m03_couplers_to_axi_interconnect_2_AWID;
  assign M03_AXI_awlen[7:0] = m03_couplers_to_axi_interconnect_2_AWLEN;
  assign M03_AXI_awlock[0] = m03_couplers_to_axi_interconnect_2_AWLOCK;
  assign M03_AXI_awprot[2:0] = m03_couplers_to_axi_interconnect_2_AWPROT;
  assign M03_AXI_awqos[3:0] = m03_couplers_to_axi_interconnect_2_AWQOS;
  assign M03_AXI_awregion[3:0] = m03_couplers_to_axi_interconnect_2_AWREGION;
  assign M03_AXI_awsize[2:0] = m03_couplers_to_axi_interconnect_2_AWSIZE;
  assign M03_AXI_awuser[3:0] = m03_couplers_to_axi_interconnect_2_AWUSER;
  assign M03_AXI_awvalid = m03_couplers_to_axi_interconnect_2_AWVALID;
  assign M03_AXI_bready = m03_couplers_to_axi_interconnect_2_BREADY;
  assign M03_AXI_rready = m03_couplers_to_axi_interconnect_2_RREADY;
  assign M03_AXI_wdata[31:0] = m03_couplers_to_axi_interconnect_2_WDATA;
  assign M03_AXI_wlast = m03_couplers_to_axi_interconnect_2_WLAST;
  assign M03_AXI_wstrb[3:0] = m03_couplers_to_axi_interconnect_2_WSTRB;
  assign M03_AXI_wuser[3:0] = m03_couplers_to_axi_interconnect_2_WUSER;
  assign M03_AXI_wvalid = m03_couplers_to_axi_interconnect_2_WVALID;
  assign M04_AXI_araddr[31:0] = m04_couplers_to_axi_interconnect_2_ARADDR;
  assign M04_AXI_arvalid = m04_couplers_to_axi_interconnect_2_ARVALID;
  assign M04_AXI_awaddr[31:0] = m04_couplers_to_axi_interconnect_2_AWADDR;
  assign M04_AXI_awvalid = m04_couplers_to_axi_interconnect_2_AWVALID;
  assign M04_AXI_bready = m04_couplers_to_axi_interconnect_2_BREADY;
  assign M04_AXI_rready = m04_couplers_to_axi_interconnect_2_RREADY;
  assign M04_AXI_wdata[31:0] = m04_couplers_to_axi_interconnect_2_WDATA;
  assign M04_AXI_wstrb[3:0] = m04_couplers_to_axi_interconnect_2_WSTRB;
  assign M04_AXI_wvalid = m04_couplers_to_axi_interconnect_2_WVALID;
  assign M05_AXI_araddr[31:0] = m05_couplers_to_axi_interconnect_2_ARADDR;
  assign M05_AXI_arvalid = m05_couplers_to_axi_interconnect_2_ARVALID;
  assign M05_AXI_awaddr[31:0] = m05_couplers_to_axi_interconnect_2_AWADDR;
  assign M05_AXI_awvalid = m05_couplers_to_axi_interconnect_2_AWVALID;
  assign M05_AXI_bready = m05_couplers_to_axi_interconnect_2_BREADY;
  assign M05_AXI_rready = m05_couplers_to_axi_interconnect_2_RREADY;
  assign M05_AXI_wdata[31:0] = m05_couplers_to_axi_interconnect_2_WDATA;
  assign M05_AXI_wstrb[3:0] = m05_couplers_to_axi_interconnect_2_WSTRB;
  assign M05_AXI_wvalid = m05_couplers_to_axi_interconnect_2_WVALID;
  assign M06_AXI_araddr[31:0] = m06_couplers_to_axi_interconnect_2_ARADDR;
  assign M06_AXI_arvalid = m06_couplers_to_axi_interconnect_2_ARVALID;
  assign M06_AXI_awaddr[31:0] = m06_couplers_to_axi_interconnect_2_AWADDR;
  assign M06_AXI_awvalid = m06_couplers_to_axi_interconnect_2_AWVALID;
  assign M06_AXI_bready = m06_couplers_to_axi_interconnect_2_BREADY;
  assign M06_AXI_rready = m06_couplers_to_axi_interconnect_2_RREADY;
  assign M06_AXI_wdata[31:0] = m06_couplers_to_axi_interconnect_2_WDATA;
  assign M06_AXI_wstrb[3:0] = m06_couplers_to_axi_interconnect_2_WSTRB;
  assign M06_AXI_wvalid = m06_couplers_to_axi_interconnect_2_WVALID;
  assign S00_AXI_arready = axi_interconnect_2_to_s00_couplers_ARREADY;
  assign S00_AXI_awready = axi_interconnect_2_to_s00_couplers_AWREADY;
  assign S00_AXI_bid[5:0] = axi_interconnect_2_to_s00_couplers_BID;
  assign S00_AXI_bresp[1:0] = axi_interconnect_2_to_s00_couplers_BRESP;
  assign S00_AXI_buser[3:0] = axi_interconnect_2_to_s00_couplers_BUSER;
  assign S00_AXI_bvalid = axi_interconnect_2_to_s00_couplers_BVALID;
  assign S00_AXI_rdata[31:0] = axi_interconnect_2_to_s00_couplers_RDATA;
  assign S00_AXI_rid[5:0] = axi_interconnect_2_to_s00_couplers_RID;
  assign S00_AXI_rlast = axi_interconnect_2_to_s00_couplers_RLAST;
  assign S00_AXI_rresp[1:0] = axi_interconnect_2_to_s00_couplers_RRESP;
  assign S00_AXI_ruser[3:0] = axi_interconnect_2_to_s00_couplers_RUSER;
  assign S00_AXI_rvalid = axi_interconnect_2_to_s00_couplers_RVALID;
  assign S00_AXI_wready = axi_interconnect_2_to_s00_couplers_WREADY;
  assign axi_interconnect_2_ACLK_net = ACLK;
  assign axi_interconnect_2_ARESETN_net = ARESETN;
  assign axi_interconnect_2_to_s00_couplers_ARADDR = S00_AXI_araddr[31:0];
  assign axi_interconnect_2_to_s00_couplers_ARBURST = S00_AXI_arburst[1:0];
  assign axi_interconnect_2_to_s00_couplers_ARCACHE = S00_AXI_arcache[3:0];
  assign axi_interconnect_2_to_s00_couplers_ARID = S00_AXI_arid[5:0];
  assign axi_interconnect_2_to_s00_couplers_ARLEN = S00_AXI_arlen[7:0];
  assign axi_interconnect_2_to_s00_couplers_ARLOCK = S00_AXI_arlock;
  assign axi_interconnect_2_to_s00_couplers_ARPROT = S00_AXI_arprot[2:0];
  assign axi_interconnect_2_to_s00_couplers_ARQOS = S00_AXI_arqos[3:0];
  assign axi_interconnect_2_to_s00_couplers_ARSIZE = S00_AXI_arsize[2:0];
  assign axi_interconnect_2_to_s00_couplers_ARUSER = S00_AXI_aruser[3:0];
  assign axi_interconnect_2_to_s00_couplers_ARVALID = S00_AXI_arvalid;
  assign axi_interconnect_2_to_s00_couplers_AWADDR = S00_AXI_awaddr[31:0];
  assign axi_interconnect_2_to_s00_couplers_AWBURST = S00_AXI_awburst[1:0];
  assign axi_interconnect_2_to_s00_couplers_AWCACHE = S00_AXI_awcache[3:0];
  assign axi_interconnect_2_to_s00_couplers_AWID = S00_AXI_awid[5:0];
  assign axi_interconnect_2_to_s00_couplers_AWLEN = S00_AXI_awlen[7:0];
  assign axi_interconnect_2_to_s00_couplers_AWLOCK = S00_AXI_awlock;
  assign axi_interconnect_2_to_s00_couplers_AWPROT = S00_AXI_awprot[2:0];
  assign axi_interconnect_2_to_s00_couplers_AWQOS = S00_AXI_awqos[3:0];
  assign axi_interconnect_2_to_s00_couplers_AWSIZE = S00_AXI_awsize[2:0];
  assign axi_interconnect_2_to_s00_couplers_AWUSER = S00_AXI_awuser[3:0];
  assign axi_interconnect_2_to_s00_couplers_AWVALID = S00_AXI_awvalid;
  assign axi_interconnect_2_to_s00_couplers_BREADY = S00_AXI_bready;
  assign axi_interconnect_2_to_s00_couplers_RREADY = S00_AXI_rready;
  assign axi_interconnect_2_to_s00_couplers_WDATA = S00_AXI_wdata[31:0];
  assign axi_interconnect_2_to_s00_couplers_WLAST = S00_AXI_wlast;
  assign axi_interconnect_2_to_s00_couplers_WSTRB = S00_AXI_wstrb[3:0];
  assign axi_interconnect_2_to_s00_couplers_WUSER = S00_AXI_wuser[3:0];
  assign axi_interconnect_2_to_s00_couplers_WVALID = S00_AXI_wvalid;
  assign m00_couplers_to_axi_interconnect_2_ARREADY = M00_AXI_arready[0];
  assign m00_couplers_to_axi_interconnect_2_AWREADY = M00_AXI_awready[0];
  assign m00_couplers_to_axi_interconnect_2_BID = M00_AXI_bid[5:0];
  assign m00_couplers_to_axi_interconnect_2_BRESP = M00_AXI_bresp[1:0];
  assign m00_couplers_to_axi_interconnect_2_BUSER = M00_AXI_buser[3:0];
  assign m00_couplers_to_axi_interconnect_2_BVALID = M00_AXI_bvalid[0];
  assign m00_couplers_to_axi_interconnect_2_RDATA = M00_AXI_rdata[31:0];
  assign m00_couplers_to_axi_interconnect_2_RID = M00_AXI_rid[5:0];
  assign m00_couplers_to_axi_interconnect_2_RLAST = M00_AXI_rlast[0];
  assign m00_couplers_to_axi_interconnect_2_RRESP = M00_AXI_rresp[1:0];
  assign m00_couplers_to_axi_interconnect_2_RUSER = M00_AXI_ruser[3:0];
  assign m00_couplers_to_axi_interconnect_2_RVALID = M00_AXI_rvalid[0];
  assign m00_couplers_to_axi_interconnect_2_WREADY = M00_AXI_wready[0];
  assign m01_couplers_to_axi_interconnect_2_ARREADY = M01_AXI_arready;
  assign m01_couplers_to_axi_interconnect_2_AWREADY = M01_AXI_awready;
  assign m01_couplers_to_axi_interconnect_2_BRESP = M01_AXI_bresp[1:0];
  assign m01_couplers_to_axi_interconnect_2_BVALID = M01_AXI_bvalid;
  assign m01_couplers_to_axi_interconnect_2_RDATA = M01_AXI_rdata[31:0];
  assign m01_couplers_to_axi_interconnect_2_RRESP = M01_AXI_rresp[1:0];
  assign m01_couplers_to_axi_interconnect_2_RVALID = M01_AXI_rvalid;
  assign m01_couplers_to_axi_interconnect_2_WREADY = M01_AXI_wready;
  assign m02_couplers_to_axi_interconnect_2_ARREADY = M02_AXI_arready;
  assign m02_couplers_to_axi_interconnect_2_AWREADY = M02_AXI_awready;
  assign m02_couplers_to_axi_interconnect_2_BRESP = M02_AXI_bresp[1:0];
  assign m02_couplers_to_axi_interconnect_2_BVALID = M02_AXI_bvalid;
  assign m02_couplers_to_axi_interconnect_2_RDATA = M02_AXI_rdata[31:0];
  assign m02_couplers_to_axi_interconnect_2_RRESP = M02_AXI_rresp[1:0];
  assign m02_couplers_to_axi_interconnect_2_RVALID = M02_AXI_rvalid;
  assign m02_couplers_to_axi_interconnect_2_WREADY = M02_AXI_wready;
  assign m03_couplers_to_axi_interconnect_2_ARREADY = M03_AXI_arready;
  assign m03_couplers_to_axi_interconnect_2_AWREADY = M03_AXI_awready;
  assign m03_couplers_to_axi_interconnect_2_BID = M03_AXI_bid[5:0];
  assign m03_couplers_to_axi_interconnect_2_BRESP = M03_AXI_bresp[1:0];
  assign m03_couplers_to_axi_interconnect_2_BUSER = M03_AXI_buser[3:0];
  assign m03_couplers_to_axi_interconnect_2_BVALID = M03_AXI_bvalid;
  assign m03_couplers_to_axi_interconnect_2_RDATA = M03_AXI_rdata[31:0];
  assign m03_couplers_to_axi_interconnect_2_RID = M03_AXI_rid[5:0];
  assign m03_couplers_to_axi_interconnect_2_RLAST = M03_AXI_rlast;
  assign m03_couplers_to_axi_interconnect_2_RRESP = M03_AXI_rresp[1:0];
  assign m03_couplers_to_axi_interconnect_2_RUSER = M03_AXI_ruser[3:0];
  assign m03_couplers_to_axi_interconnect_2_RVALID = M03_AXI_rvalid;
  assign m03_couplers_to_axi_interconnect_2_WREADY = M03_AXI_wready;
  assign m04_couplers_to_axi_interconnect_2_ARREADY = M04_AXI_arready;
  assign m04_couplers_to_axi_interconnect_2_AWREADY = M04_AXI_awready;
  assign m04_couplers_to_axi_interconnect_2_BRESP = M04_AXI_bresp[1:0];
  assign m04_couplers_to_axi_interconnect_2_BVALID = M04_AXI_bvalid;
  assign m04_couplers_to_axi_interconnect_2_RDATA = M04_AXI_rdata[31:0];
  assign m04_couplers_to_axi_interconnect_2_RRESP = M04_AXI_rresp[1:0];
  assign m04_couplers_to_axi_interconnect_2_RVALID = M04_AXI_rvalid;
  assign m04_couplers_to_axi_interconnect_2_WREADY = M04_AXI_wready;
  assign m05_couplers_to_axi_interconnect_2_ARREADY = M05_AXI_arready;
  assign m05_couplers_to_axi_interconnect_2_AWREADY = M05_AXI_awready;
  assign m05_couplers_to_axi_interconnect_2_BRESP = M05_AXI_bresp[1:0];
  assign m05_couplers_to_axi_interconnect_2_BVALID = M05_AXI_bvalid;
  assign m05_couplers_to_axi_interconnect_2_RDATA = M05_AXI_rdata[31:0];
  assign m05_couplers_to_axi_interconnect_2_RRESP = M05_AXI_rresp[1:0];
  assign m05_couplers_to_axi_interconnect_2_RVALID = M05_AXI_rvalid;
  assign m05_couplers_to_axi_interconnect_2_WREADY = M05_AXI_wready;
  assign m06_couplers_to_axi_interconnect_2_ARREADY = M06_AXI_arready;
  assign m06_couplers_to_axi_interconnect_2_AWREADY = M06_AXI_awready;
  assign m06_couplers_to_axi_interconnect_2_BRESP = M06_AXI_bresp[1:0];
  assign m06_couplers_to_axi_interconnect_2_BVALID = M06_AXI_bvalid;
  assign m06_couplers_to_axi_interconnect_2_RDATA = M06_AXI_rdata[31:0];
  assign m06_couplers_to_axi_interconnect_2_RRESP = M06_AXI_rresp[1:0];
  assign m06_couplers_to_axi_interconnect_2_RVALID = M06_AXI_rvalid;
  assign m06_couplers_to_axi_interconnect_2_WREADY = M06_AXI_wready;
  m00_couplers_imp_1ICGZB5 m00_couplers
       (.M_ACLK(axi_interconnect_2_ACLK_net),
        .M_ARESETN(axi_interconnect_2_ARESETN_net),
        .M_AXI_araddr(m00_couplers_to_axi_interconnect_2_ARADDR),
        .M_AXI_arburst(m00_couplers_to_axi_interconnect_2_ARBURST),
        .M_AXI_arcache(m00_couplers_to_axi_interconnect_2_ARCACHE),
        .M_AXI_arid(m00_couplers_to_axi_interconnect_2_ARID),
        .M_AXI_arlen(m00_couplers_to_axi_interconnect_2_ARLEN),
        .M_AXI_arlock(m00_couplers_to_axi_interconnect_2_ARLOCK),
        .M_AXI_arprot(m00_couplers_to_axi_interconnect_2_ARPROT),
        .M_AXI_arqos(m00_couplers_to_axi_interconnect_2_ARQOS),
        .M_AXI_arready(m00_couplers_to_axi_interconnect_2_ARREADY),
        .M_AXI_arregion(m00_couplers_to_axi_interconnect_2_ARREGION),
        .M_AXI_arsize(m00_couplers_to_axi_interconnect_2_ARSIZE),
        .M_AXI_aruser(m00_couplers_to_axi_interconnect_2_ARUSER),
        .M_AXI_arvalid(m00_couplers_to_axi_interconnect_2_ARVALID),
        .M_AXI_awaddr(m00_couplers_to_axi_interconnect_2_AWADDR),
        .M_AXI_awburst(m00_couplers_to_axi_interconnect_2_AWBURST),
        .M_AXI_awcache(m00_couplers_to_axi_interconnect_2_AWCACHE),
        .M_AXI_awid(m00_couplers_to_axi_interconnect_2_AWID),
        .M_AXI_awlen(m00_couplers_to_axi_interconnect_2_AWLEN),
        .M_AXI_awlock(m00_couplers_to_axi_interconnect_2_AWLOCK),
        .M_AXI_awprot(m00_couplers_to_axi_interconnect_2_AWPROT),
        .M_AXI_awqos(m00_couplers_to_axi_interconnect_2_AWQOS),
        .M_AXI_awready(m00_couplers_to_axi_interconnect_2_AWREADY),
        .M_AXI_awregion(m00_couplers_to_axi_interconnect_2_AWREGION),
        .M_AXI_awsize(m00_couplers_to_axi_interconnect_2_AWSIZE),
        .M_AXI_awuser(m00_couplers_to_axi_interconnect_2_AWUSER),
        .M_AXI_awvalid(m00_couplers_to_axi_interconnect_2_AWVALID),
        .M_AXI_bid(m00_couplers_to_axi_interconnect_2_BID),
        .M_AXI_bready(m00_couplers_to_axi_interconnect_2_BREADY),
        .M_AXI_bresp(m00_couplers_to_axi_interconnect_2_BRESP),
        .M_AXI_buser(m00_couplers_to_axi_interconnect_2_BUSER),
        .M_AXI_bvalid(m00_couplers_to_axi_interconnect_2_BVALID),
        .M_AXI_rdata(m00_couplers_to_axi_interconnect_2_RDATA),
        .M_AXI_rid(m00_couplers_to_axi_interconnect_2_RID),
        .M_AXI_rlast(m00_couplers_to_axi_interconnect_2_RLAST),
        .M_AXI_rready(m00_couplers_to_axi_interconnect_2_RREADY),
        .M_AXI_rresp(m00_couplers_to_axi_interconnect_2_RRESP),
        .M_AXI_ruser(m00_couplers_to_axi_interconnect_2_RUSER),
        .M_AXI_rvalid(m00_couplers_to_axi_interconnect_2_RVALID),
        .M_AXI_wdata(m00_couplers_to_axi_interconnect_2_WDATA),
        .M_AXI_wlast(m00_couplers_to_axi_interconnect_2_WLAST),
        .M_AXI_wready(m00_couplers_to_axi_interconnect_2_WREADY),
        .M_AXI_wstrb(m00_couplers_to_axi_interconnect_2_WSTRB),
        .M_AXI_wuser(m00_couplers_to_axi_interconnect_2_WUSER),
        .M_AXI_wvalid(m00_couplers_to_axi_interconnect_2_WVALID),
        .S_ACLK(axi_interconnect_2_ACLK_net),
        .S_ARESETN(axi_interconnect_2_ARESETN_net),
        .S_AXI_araddr(xbar_to_m00_couplers_ARADDR),
        .S_AXI_arburst(xbar_to_m00_couplers_ARBURST),
        .S_AXI_arcache(xbar_to_m00_couplers_ARCACHE),
        .S_AXI_arid(xbar_to_m00_couplers_ARID),
        .S_AXI_arlen(xbar_to_m00_couplers_ARLEN),
        .S_AXI_arlock(xbar_to_m00_couplers_ARLOCK),
        .S_AXI_arprot(xbar_to_m00_couplers_ARPROT),
        .S_AXI_arqos(xbar_to_m00_couplers_ARQOS),
        .S_AXI_arready(xbar_to_m00_couplers_ARREADY),
        .S_AXI_arregion(xbar_to_m00_couplers_ARREGION),
        .S_AXI_arsize(xbar_to_m00_couplers_ARSIZE),
        .S_AXI_aruser(xbar_to_m00_couplers_ARUSER),
        .S_AXI_arvalid(xbar_to_m00_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m00_couplers_AWADDR),
        .S_AXI_awburst(xbar_to_m00_couplers_AWBURST),
        .S_AXI_awcache(xbar_to_m00_couplers_AWCACHE),
        .S_AXI_awid(xbar_to_m00_couplers_AWID),
        .S_AXI_awlen(xbar_to_m00_couplers_AWLEN),
        .S_AXI_awlock(xbar_to_m00_couplers_AWLOCK),
        .S_AXI_awprot(xbar_to_m00_couplers_AWPROT),
        .S_AXI_awqos(xbar_to_m00_couplers_AWQOS),
        .S_AXI_awready(xbar_to_m00_couplers_AWREADY),
        .S_AXI_awregion(xbar_to_m00_couplers_AWREGION),
        .S_AXI_awsize(xbar_to_m00_couplers_AWSIZE),
        .S_AXI_awuser(xbar_to_m00_couplers_AWUSER),
        .S_AXI_awvalid(xbar_to_m00_couplers_AWVALID),
        .S_AXI_bid(xbar_to_m00_couplers_BID),
        .S_AXI_bready(xbar_to_m00_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m00_couplers_BRESP),
        .S_AXI_buser(xbar_to_m00_couplers_BUSER),
        .S_AXI_bvalid(xbar_to_m00_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m00_couplers_RDATA),
        .S_AXI_rid(xbar_to_m00_couplers_RID),
        .S_AXI_rlast(xbar_to_m00_couplers_RLAST),
        .S_AXI_rready(xbar_to_m00_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m00_couplers_RRESP),
        .S_AXI_ruser(xbar_to_m00_couplers_RUSER),
        .S_AXI_rvalid(xbar_to_m00_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m00_couplers_WDATA),
        .S_AXI_wlast(xbar_to_m00_couplers_WLAST),
        .S_AXI_wready(xbar_to_m00_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m00_couplers_WSTRB),
        .S_AXI_wuser(xbar_to_m00_couplers_WUSER),
        .S_AXI_wvalid(xbar_to_m00_couplers_WVALID));
  m01_couplers_imp_A785EE m01_couplers
       (.M_ACLK(axi_interconnect_2_ACLK_net),
        .M_ARESETN(axi_interconnect_2_ARESETN_net),
        .M_AXI_araddr(m01_couplers_to_axi_interconnect_2_ARADDR),
        .M_AXI_arready(m01_couplers_to_axi_interconnect_2_ARREADY),
        .M_AXI_arvalid(m01_couplers_to_axi_interconnect_2_ARVALID),
        .M_AXI_awaddr(m01_couplers_to_axi_interconnect_2_AWADDR),
        .M_AXI_awready(m01_couplers_to_axi_interconnect_2_AWREADY),
        .M_AXI_awvalid(m01_couplers_to_axi_interconnect_2_AWVALID),
        .M_AXI_bready(m01_couplers_to_axi_interconnect_2_BREADY),
        .M_AXI_bresp(m01_couplers_to_axi_interconnect_2_BRESP),
        .M_AXI_bvalid(m01_couplers_to_axi_interconnect_2_BVALID),
        .M_AXI_rdata(m01_couplers_to_axi_interconnect_2_RDATA),
        .M_AXI_rready(m01_couplers_to_axi_interconnect_2_RREADY),
        .M_AXI_rresp(m01_couplers_to_axi_interconnect_2_RRESP),
        .M_AXI_rvalid(m01_couplers_to_axi_interconnect_2_RVALID),
        .M_AXI_wdata(m01_couplers_to_axi_interconnect_2_WDATA),
        .M_AXI_wready(m01_couplers_to_axi_interconnect_2_WREADY),
        .M_AXI_wstrb(m01_couplers_to_axi_interconnect_2_WSTRB),
        .M_AXI_wvalid(m01_couplers_to_axi_interconnect_2_WVALID),
        .S_ACLK(axi_interconnect_2_ACLK_net),
        .S_ARESETN(axi_interconnect_2_ARESETN_net),
        .S_AXI_araddr(xbar_to_m01_couplers_ARADDR),
        .S_AXI_arburst(xbar_to_m01_couplers_ARBURST),
        .S_AXI_arcache(xbar_to_m01_couplers_ARCACHE),
        .S_AXI_arid(xbar_to_m01_couplers_ARID),
        .S_AXI_arlen(xbar_to_m01_couplers_ARLEN),
        .S_AXI_arlock(xbar_to_m01_couplers_ARLOCK),
        .S_AXI_arprot(xbar_to_m01_couplers_ARPROT),
        .S_AXI_arqos(xbar_to_m01_couplers_ARQOS),
        .S_AXI_arready(xbar_to_m01_couplers_ARREADY),
        .S_AXI_arregion(xbar_to_m01_couplers_ARREGION),
        .S_AXI_arsize(xbar_to_m01_couplers_ARSIZE),
        .S_AXI_arvalid(xbar_to_m01_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m01_couplers_AWADDR),
        .S_AXI_awburst(xbar_to_m01_couplers_AWBURST),
        .S_AXI_awcache(xbar_to_m01_couplers_AWCACHE),
        .S_AXI_awid(xbar_to_m01_couplers_AWID),
        .S_AXI_awlen(xbar_to_m01_couplers_AWLEN),
        .S_AXI_awlock(xbar_to_m01_couplers_AWLOCK),
        .S_AXI_awprot(xbar_to_m01_couplers_AWPROT),
        .S_AXI_awqos(xbar_to_m01_couplers_AWQOS),
        .S_AXI_awready(xbar_to_m01_couplers_AWREADY),
        .S_AXI_awregion(xbar_to_m01_couplers_AWREGION),
        .S_AXI_awsize(xbar_to_m01_couplers_AWSIZE),
        .S_AXI_awvalid(xbar_to_m01_couplers_AWVALID),
        .S_AXI_bid(xbar_to_m01_couplers_BID),
        .S_AXI_bready(xbar_to_m01_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m01_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m01_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m01_couplers_RDATA),
        .S_AXI_rid(xbar_to_m01_couplers_RID),
        .S_AXI_rlast(xbar_to_m01_couplers_RLAST),
        .S_AXI_rready(xbar_to_m01_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m01_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m01_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m01_couplers_WDATA),
        .S_AXI_wlast(xbar_to_m01_couplers_WLAST),
        .S_AXI_wready(xbar_to_m01_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m01_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m01_couplers_WVALID));
  m02_couplers_imp_1NJDMEM m02_couplers
       (.M_ACLK(axi_interconnect_2_ACLK_net),
        .M_ARESETN(axi_interconnect_2_ARESETN_net),
        .M_AXI_araddr(m02_couplers_to_axi_interconnect_2_ARADDR),
        .M_AXI_arready(m02_couplers_to_axi_interconnect_2_ARREADY),
        .M_AXI_arvalid(m02_couplers_to_axi_interconnect_2_ARVALID),
        .M_AXI_awaddr(m02_couplers_to_axi_interconnect_2_AWADDR),
        .M_AXI_awready(m02_couplers_to_axi_interconnect_2_AWREADY),
        .M_AXI_awvalid(m02_couplers_to_axi_interconnect_2_AWVALID),
        .M_AXI_bready(m02_couplers_to_axi_interconnect_2_BREADY),
        .M_AXI_bresp(m02_couplers_to_axi_interconnect_2_BRESP),
        .M_AXI_bvalid(m02_couplers_to_axi_interconnect_2_BVALID),
        .M_AXI_rdata(m02_couplers_to_axi_interconnect_2_RDATA),
        .M_AXI_rready(m02_couplers_to_axi_interconnect_2_RREADY),
        .M_AXI_rresp(m02_couplers_to_axi_interconnect_2_RRESP),
        .M_AXI_rvalid(m02_couplers_to_axi_interconnect_2_RVALID),
        .M_AXI_wdata(m02_couplers_to_axi_interconnect_2_WDATA),
        .M_AXI_wready(m02_couplers_to_axi_interconnect_2_WREADY),
        .M_AXI_wstrb(m02_couplers_to_axi_interconnect_2_WSTRB),
        .M_AXI_wvalid(m02_couplers_to_axi_interconnect_2_WVALID),
        .S_ACLK(axi_interconnect_2_ACLK_net),
        .S_ARESETN(axi_interconnect_2_ARESETN_net),
        .S_AXI_araddr(xbar_to_m02_couplers_ARADDR),
        .S_AXI_arburst(xbar_to_m02_couplers_ARBURST),
        .S_AXI_arcache(xbar_to_m02_couplers_ARCACHE),
        .S_AXI_arid(xbar_to_m02_couplers_ARID),
        .S_AXI_arlen(xbar_to_m02_couplers_ARLEN),
        .S_AXI_arlock(xbar_to_m02_couplers_ARLOCK),
        .S_AXI_arprot(xbar_to_m02_couplers_ARPROT),
        .S_AXI_arqos(xbar_to_m02_couplers_ARQOS),
        .S_AXI_arready(xbar_to_m02_couplers_ARREADY),
        .S_AXI_arregion(xbar_to_m02_couplers_ARREGION),
        .S_AXI_arsize(xbar_to_m02_couplers_ARSIZE),
        .S_AXI_arvalid(xbar_to_m02_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m02_couplers_AWADDR),
        .S_AXI_awburst(xbar_to_m02_couplers_AWBURST),
        .S_AXI_awcache(xbar_to_m02_couplers_AWCACHE),
        .S_AXI_awid(xbar_to_m02_couplers_AWID),
        .S_AXI_awlen(xbar_to_m02_couplers_AWLEN),
        .S_AXI_awlock(xbar_to_m02_couplers_AWLOCK),
        .S_AXI_awprot(xbar_to_m02_couplers_AWPROT),
        .S_AXI_awqos(xbar_to_m02_couplers_AWQOS),
        .S_AXI_awready(xbar_to_m02_couplers_AWREADY),
        .S_AXI_awregion(xbar_to_m02_couplers_AWREGION),
        .S_AXI_awsize(xbar_to_m02_couplers_AWSIZE),
        .S_AXI_awvalid(xbar_to_m02_couplers_AWVALID),
        .S_AXI_bid(xbar_to_m02_couplers_BID),
        .S_AXI_bready(xbar_to_m02_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m02_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m02_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m02_couplers_RDATA),
        .S_AXI_rid(xbar_to_m02_couplers_RID),
        .S_AXI_rlast(xbar_to_m02_couplers_RLAST),
        .S_AXI_rready(xbar_to_m02_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m02_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m02_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m02_couplers_WDATA),
        .S_AXI_wlast(xbar_to_m02_couplers_WLAST),
        .S_AXI_wready(xbar_to_m02_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m02_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m02_couplers_WVALID));
  m03_couplers_imp_DQTQ2H m03_couplers
       (.M_ACLK(axi_interconnect_2_ACLK_net),
        .M_ARESETN(axi_interconnect_2_ARESETN_net),
        .M_AXI_araddr(m03_couplers_to_axi_interconnect_2_ARADDR),
        .M_AXI_arburst(m03_couplers_to_axi_interconnect_2_ARBURST),
        .M_AXI_arcache(m03_couplers_to_axi_interconnect_2_ARCACHE),
        .M_AXI_arid(m03_couplers_to_axi_interconnect_2_ARID),
        .M_AXI_arlen(m03_couplers_to_axi_interconnect_2_ARLEN),
        .M_AXI_arlock(m03_couplers_to_axi_interconnect_2_ARLOCK),
        .M_AXI_arprot(m03_couplers_to_axi_interconnect_2_ARPROT),
        .M_AXI_arqos(m03_couplers_to_axi_interconnect_2_ARQOS),
        .M_AXI_arready(m03_couplers_to_axi_interconnect_2_ARREADY),
        .M_AXI_arregion(m03_couplers_to_axi_interconnect_2_ARREGION),
        .M_AXI_arsize(m03_couplers_to_axi_interconnect_2_ARSIZE),
        .M_AXI_aruser(m03_couplers_to_axi_interconnect_2_ARUSER),
        .M_AXI_arvalid(m03_couplers_to_axi_interconnect_2_ARVALID),
        .M_AXI_awaddr(m03_couplers_to_axi_interconnect_2_AWADDR),
        .M_AXI_awburst(m03_couplers_to_axi_interconnect_2_AWBURST),
        .M_AXI_awcache(m03_couplers_to_axi_interconnect_2_AWCACHE),
        .M_AXI_awid(m03_couplers_to_axi_interconnect_2_AWID),
        .M_AXI_awlen(m03_couplers_to_axi_interconnect_2_AWLEN),
        .M_AXI_awlock(m03_couplers_to_axi_interconnect_2_AWLOCK),
        .M_AXI_awprot(m03_couplers_to_axi_interconnect_2_AWPROT),
        .M_AXI_awqos(m03_couplers_to_axi_interconnect_2_AWQOS),
        .M_AXI_awready(m03_couplers_to_axi_interconnect_2_AWREADY),
        .M_AXI_awregion(m03_couplers_to_axi_interconnect_2_AWREGION),
        .M_AXI_awsize(m03_couplers_to_axi_interconnect_2_AWSIZE),
        .M_AXI_awuser(m03_couplers_to_axi_interconnect_2_AWUSER),
        .M_AXI_awvalid(m03_couplers_to_axi_interconnect_2_AWVALID),
        .M_AXI_bid(m03_couplers_to_axi_interconnect_2_BID),
        .M_AXI_bready(m03_couplers_to_axi_interconnect_2_BREADY),
        .M_AXI_bresp(m03_couplers_to_axi_interconnect_2_BRESP),
        .M_AXI_buser(m03_couplers_to_axi_interconnect_2_BUSER),
        .M_AXI_bvalid(m03_couplers_to_axi_interconnect_2_BVALID),
        .M_AXI_rdata(m03_couplers_to_axi_interconnect_2_RDATA),
        .M_AXI_rid(m03_couplers_to_axi_interconnect_2_RID),
        .M_AXI_rlast(m03_couplers_to_axi_interconnect_2_RLAST),
        .M_AXI_rready(m03_couplers_to_axi_interconnect_2_RREADY),
        .M_AXI_rresp(m03_couplers_to_axi_interconnect_2_RRESP),
        .M_AXI_ruser(m03_couplers_to_axi_interconnect_2_RUSER),
        .M_AXI_rvalid(m03_couplers_to_axi_interconnect_2_RVALID),
        .M_AXI_wdata(m03_couplers_to_axi_interconnect_2_WDATA),
        .M_AXI_wlast(m03_couplers_to_axi_interconnect_2_WLAST),
        .M_AXI_wready(m03_couplers_to_axi_interconnect_2_WREADY),
        .M_AXI_wstrb(m03_couplers_to_axi_interconnect_2_WSTRB),
        .M_AXI_wuser(m03_couplers_to_axi_interconnect_2_WUSER),
        .M_AXI_wvalid(m03_couplers_to_axi_interconnect_2_WVALID),
        .S_ACLK(axi_interconnect_2_ACLK_net),
        .S_ARESETN(axi_interconnect_2_ARESETN_net),
        .S_AXI_araddr(xbar_to_m03_couplers_ARADDR),
        .S_AXI_arburst(xbar_to_m03_couplers_ARBURST),
        .S_AXI_arcache(xbar_to_m03_couplers_ARCACHE),
        .S_AXI_arid(xbar_to_m03_couplers_ARID),
        .S_AXI_arlen(xbar_to_m03_couplers_ARLEN),
        .S_AXI_arlock(xbar_to_m03_couplers_ARLOCK),
        .S_AXI_arprot(xbar_to_m03_couplers_ARPROT),
        .S_AXI_arqos(xbar_to_m03_couplers_ARQOS),
        .S_AXI_arready(xbar_to_m03_couplers_ARREADY),
        .S_AXI_arregion(xbar_to_m03_couplers_ARREGION),
        .S_AXI_arsize(xbar_to_m03_couplers_ARSIZE),
        .S_AXI_aruser(xbar_to_m03_couplers_ARUSER),
        .S_AXI_arvalid(xbar_to_m03_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m03_couplers_AWADDR),
        .S_AXI_awburst(xbar_to_m03_couplers_AWBURST),
        .S_AXI_awcache(xbar_to_m03_couplers_AWCACHE),
        .S_AXI_awid(xbar_to_m03_couplers_AWID),
        .S_AXI_awlen(xbar_to_m03_couplers_AWLEN),
        .S_AXI_awlock(xbar_to_m03_couplers_AWLOCK),
        .S_AXI_awprot(xbar_to_m03_couplers_AWPROT),
        .S_AXI_awqos(xbar_to_m03_couplers_AWQOS),
        .S_AXI_awready(xbar_to_m03_couplers_AWREADY),
        .S_AXI_awregion(xbar_to_m03_couplers_AWREGION),
        .S_AXI_awsize(xbar_to_m03_couplers_AWSIZE),
        .S_AXI_awuser(xbar_to_m03_couplers_AWUSER),
        .S_AXI_awvalid(xbar_to_m03_couplers_AWVALID),
        .S_AXI_bid(xbar_to_m03_couplers_BID),
        .S_AXI_bready(xbar_to_m03_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m03_couplers_BRESP),
        .S_AXI_buser(xbar_to_m03_couplers_BUSER),
        .S_AXI_bvalid(xbar_to_m03_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m03_couplers_RDATA),
        .S_AXI_rid(xbar_to_m03_couplers_RID),
        .S_AXI_rlast(xbar_to_m03_couplers_RLAST),
        .S_AXI_rready(xbar_to_m03_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m03_couplers_RRESP),
        .S_AXI_ruser(xbar_to_m03_couplers_RUSER),
        .S_AXI_rvalid(xbar_to_m03_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m03_couplers_WDATA),
        .S_AXI_wlast(xbar_to_m03_couplers_WLAST),
        .S_AXI_wready(xbar_to_m03_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m03_couplers_WSTRB),
        .S_AXI_wuser(xbar_to_m03_couplers_WUSER),
        .S_AXI_wvalid(xbar_to_m03_couplers_WVALID));
  m04_couplers_imp_1SQAA73 m04_couplers
       (.M_ACLK(axi_interconnect_2_ACLK_net),
        .M_ARESETN(axi_interconnect_2_ARESETN_net),
        .M_AXI_araddr(m04_couplers_to_axi_interconnect_2_ARADDR),
        .M_AXI_arready(m04_couplers_to_axi_interconnect_2_ARREADY),
        .M_AXI_arvalid(m04_couplers_to_axi_interconnect_2_ARVALID),
        .M_AXI_awaddr(m04_couplers_to_axi_interconnect_2_AWADDR),
        .M_AXI_awready(m04_couplers_to_axi_interconnect_2_AWREADY),
        .M_AXI_awvalid(m04_couplers_to_axi_interconnect_2_AWVALID),
        .M_AXI_bready(m04_couplers_to_axi_interconnect_2_BREADY),
        .M_AXI_bresp(m04_couplers_to_axi_interconnect_2_BRESP),
        .M_AXI_bvalid(m04_couplers_to_axi_interconnect_2_BVALID),
        .M_AXI_rdata(m04_couplers_to_axi_interconnect_2_RDATA),
        .M_AXI_rready(m04_couplers_to_axi_interconnect_2_RREADY),
        .M_AXI_rresp(m04_couplers_to_axi_interconnect_2_RRESP),
        .M_AXI_rvalid(m04_couplers_to_axi_interconnect_2_RVALID),
        .M_AXI_wdata(m04_couplers_to_axi_interconnect_2_WDATA),
        .M_AXI_wready(m04_couplers_to_axi_interconnect_2_WREADY),
        .M_AXI_wstrb(m04_couplers_to_axi_interconnect_2_WSTRB),
        .M_AXI_wvalid(m04_couplers_to_axi_interconnect_2_WVALID),
        .S_ACLK(axi_interconnect_2_ACLK_net),
        .S_ARESETN(axi_interconnect_2_ARESETN_net),
        .S_AXI_araddr(xbar_to_m04_couplers_ARADDR),
        .S_AXI_arburst(xbar_to_m04_couplers_ARBURST),
        .S_AXI_arcache(xbar_to_m04_couplers_ARCACHE),
        .S_AXI_arid(xbar_to_m04_couplers_ARID),
        .S_AXI_arlen(xbar_to_m04_couplers_ARLEN),
        .S_AXI_arlock(xbar_to_m04_couplers_ARLOCK),
        .S_AXI_arprot(xbar_to_m04_couplers_ARPROT),
        .S_AXI_arqos(xbar_to_m04_couplers_ARQOS),
        .S_AXI_arready(xbar_to_m04_couplers_ARREADY),
        .S_AXI_arregion(xbar_to_m04_couplers_ARREGION),
        .S_AXI_arsize(xbar_to_m04_couplers_ARSIZE),
        .S_AXI_arvalid(xbar_to_m04_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m04_couplers_AWADDR),
        .S_AXI_awburst(xbar_to_m04_couplers_AWBURST),
        .S_AXI_awcache(xbar_to_m04_couplers_AWCACHE),
        .S_AXI_awid(xbar_to_m04_couplers_AWID),
        .S_AXI_awlen(xbar_to_m04_couplers_AWLEN),
        .S_AXI_awlock(xbar_to_m04_couplers_AWLOCK),
        .S_AXI_awprot(xbar_to_m04_couplers_AWPROT),
        .S_AXI_awqos(xbar_to_m04_couplers_AWQOS),
        .S_AXI_awready(xbar_to_m04_couplers_AWREADY),
        .S_AXI_awregion(xbar_to_m04_couplers_AWREGION),
        .S_AXI_awsize(xbar_to_m04_couplers_AWSIZE),
        .S_AXI_awvalid(xbar_to_m04_couplers_AWVALID),
        .S_AXI_bid(xbar_to_m04_couplers_BID),
        .S_AXI_bready(xbar_to_m04_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m04_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m04_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m04_couplers_RDATA),
        .S_AXI_rid(xbar_to_m04_couplers_RID),
        .S_AXI_rlast(xbar_to_m04_couplers_RLAST),
        .S_AXI_rready(xbar_to_m04_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m04_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m04_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m04_couplers_WDATA),
        .S_AXI_wlast(xbar_to_m04_couplers_WLAST),
        .S_AXI_wready(xbar_to_m04_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m04_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m04_couplers_WVALID));
  m05_couplers_imp_3Z8KUG m05_couplers
       (.M_ACLK(axi_interconnect_2_ACLK_net),
        .M_ARESETN(axi_interconnect_2_ARESETN_net),
        .M_AXI_araddr(m05_couplers_to_axi_interconnect_2_ARADDR),
        .M_AXI_arready(m05_couplers_to_axi_interconnect_2_ARREADY),
        .M_AXI_arvalid(m05_couplers_to_axi_interconnect_2_ARVALID),
        .M_AXI_awaddr(m05_couplers_to_axi_interconnect_2_AWADDR),
        .M_AXI_awready(m05_couplers_to_axi_interconnect_2_AWREADY),
        .M_AXI_awvalid(m05_couplers_to_axi_interconnect_2_AWVALID),
        .M_AXI_bready(m05_couplers_to_axi_interconnect_2_BREADY),
        .M_AXI_bresp(m05_couplers_to_axi_interconnect_2_BRESP),
        .M_AXI_bvalid(m05_couplers_to_axi_interconnect_2_BVALID),
        .M_AXI_rdata(m05_couplers_to_axi_interconnect_2_RDATA),
        .M_AXI_rready(m05_couplers_to_axi_interconnect_2_RREADY),
        .M_AXI_rresp(m05_couplers_to_axi_interconnect_2_RRESP),
        .M_AXI_rvalid(m05_couplers_to_axi_interconnect_2_RVALID),
        .M_AXI_wdata(m05_couplers_to_axi_interconnect_2_WDATA),
        .M_AXI_wready(m05_couplers_to_axi_interconnect_2_WREADY),
        .M_AXI_wstrb(m05_couplers_to_axi_interconnect_2_WSTRB),
        .M_AXI_wvalid(m05_couplers_to_axi_interconnect_2_WVALID),
        .S_ACLK(axi_interconnect_2_ACLK_net),
        .S_ARESETN(axi_interconnect_2_ARESETN_net),
        .S_AXI_araddr(xbar_to_m05_couplers_ARADDR),
        .S_AXI_arburst(xbar_to_m05_couplers_ARBURST),
        .S_AXI_arcache(xbar_to_m05_couplers_ARCACHE),
        .S_AXI_arid(xbar_to_m05_couplers_ARID),
        .S_AXI_arlen(xbar_to_m05_couplers_ARLEN),
        .S_AXI_arlock(xbar_to_m05_couplers_ARLOCK),
        .S_AXI_arprot(xbar_to_m05_couplers_ARPROT),
        .S_AXI_arqos(xbar_to_m05_couplers_ARQOS),
        .S_AXI_arready(xbar_to_m05_couplers_ARREADY),
        .S_AXI_arregion(xbar_to_m05_couplers_ARREGION),
        .S_AXI_arsize(xbar_to_m05_couplers_ARSIZE),
        .S_AXI_arvalid(xbar_to_m05_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m05_couplers_AWADDR),
        .S_AXI_awburst(xbar_to_m05_couplers_AWBURST),
        .S_AXI_awcache(xbar_to_m05_couplers_AWCACHE),
        .S_AXI_awid(xbar_to_m05_couplers_AWID),
        .S_AXI_awlen(xbar_to_m05_couplers_AWLEN),
        .S_AXI_awlock(xbar_to_m05_couplers_AWLOCK),
        .S_AXI_awprot(xbar_to_m05_couplers_AWPROT),
        .S_AXI_awqos(xbar_to_m05_couplers_AWQOS),
        .S_AXI_awready(xbar_to_m05_couplers_AWREADY),
        .S_AXI_awregion(xbar_to_m05_couplers_AWREGION),
        .S_AXI_awsize(xbar_to_m05_couplers_AWSIZE),
        .S_AXI_awvalid(xbar_to_m05_couplers_AWVALID),
        .S_AXI_bid(xbar_to_m05_couplers_BID),
        .S_AXI_bready(xbar_to_m05_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m05_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m05_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m05_couplers_RDATA),
        .S_AXI_rid(xbar_to_m05_couplers_RID),
        .S_AXI_rlast(xbar_to_m05_couplers_RLAST),
        .S_AXI_rready(xbar_to_m05_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m05_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m05_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m05_couplers_WDATA),
        .S_AXI_wlast(xbar_to_m05_couplers_WLAST),
        .S_AXI_wready(xbar_to_m05_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m05_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m05_couplers_WVALID));
  m06_couplers_imp_1XX6Y5S m06_couplers
       (.M_ACLK(axi_interconnect_2_ACLK_net),
        .M_ARESETN(axi_interconnect_2_ARESETN_net),
        .M_AXI_araddr(m06_couplers_to_axi_interconnect_2_ARADDR),
        .M_AXI_arready(m06_couplers_to_axi_interconnect_2_ARREADY),
        .M_AXI_arvalid(m06_couplers_to_axi_interconnect_2_ARVALID),
        .M_AXI_awaddr(m06_couplers_to_axi_interconnect_2_AWADDR),
        .M_AXI_awready(m06_couplers_to_axi_interconnect_2_AWREADY),
        .M_AXI_awvalid(m06_couplers_to_axi_interconnect_2_AWVALID),
        .M_AXI_bready(m06_couplers_to_axi_interconnect_2_BREADY),
        .M_AXI_bresp(m06_couplers_to_axi_interconnect_2_BRESP),
        .M_AXI_bvalid(m06_couplers_to_axi_interconnect_2_BVALID),
        .M_AXI_rdata(m06_couplers_to_axi_interconnect_2_RDATA),
        .M_AXI_rready(m06_couplers_to_axi_interconnect_2_RREADY),
        .M_AXI_rresp(m06_couplers_to_axi_interconnect_2_RRESP),
        .M_AXI_rvalid(m06_couplers_to_axi_interconnect_2_RVALID),
        .M_AXI_wdata(m06_couplers_to_axi_interconnect_2_WDATA),
        .M_AXI_wready(m06_couplers_to_axi_interconnect_2_WREADY),
        .M_AXI_wstrb(m06_couplers_to_axi_interconnect_2_WSTRB),
        .M_AXI_wvalid(m06_couplers_to_axi_interconnect_2_WVALID),
        .S_ACLK(axi_interconnect_2_ACLK_net),
        .S_ARESETN(axi_interconnect_2_ARESETN_net),
        .S_AXI_araddr(xbar_to_m06_couplers_ARADDR),
        .S_AXI_arburst(xbar_to_m06_couplers_ARBURST),
        .S_AXI_arcache(xbar_to_m06_couplers_ARCACHE),
        .S_AXI_arid(xbar_to_m06_couplers_ARID),
        .S_AXI_arlen(xbar_to_m06_couplers_ARLEN),
        .S_AXI_arlock(xbar_to_m06_couplers_ARLOCK),
        .S_AXI_arprot(xbar_to_m06_couplers_ARPROT),
        .S_AXI_arqos(xbar_to_m06_couplers_ARQOS),
        .S_AXI_arready(xbar_to_m06_couplers_ARREADY),
        .S_AXI_arregion(xbar_to_m06_couplers_ARREGION),
        .S_AXI_arsize(xbar_to_m06_couplers_ARSIZE),
        .S_AXI_arvalid(xbar_to_m06_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m06_couplers_AWADDR),
        .S_AXI_awburst(xbar_to_m06_couplers_AWBURST),
        .S_AXI_awcache(xbar_to_m06_couplers_AWCACHE),
        .S_AXI_awid(xbar_to_m06_couplers_AWID),
        .S_AXI_awlen(xbar_to_m06_couplers_AWLEN),
        .S_AXI_awlock(xbar_to_m06_couplers_AWLOCK),
        .S_AXI_awprot(xbar_to_m06_couplers_AWPROT),
        .S_AXI_awqos(xbar_to_m06_couplers_AWQOS),
        .S_AXI_awready(xbar_to_m06_couplers_AWREADY),
        .S_AXI_awregion(xbar_to_m06_couplers_AWREGION),
        .S_AXI_awsize(xbar_to_m06_couplers_AWSIZE),
        .S_AXI_awvalid(xbar_to_m06_couplers_AWVALID),
        .S_AXI_bid(xbar_to_m06_couplers_BID),
        .S_AXI_bready(xbar_to_m06_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m06_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m06_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m06_couplers_RDATA),
        .S_AXI_rid(xbar_to_m06_couplers_RID),
        .S_AXI_rlast(xbar_to_m06_couplers_RLAST),
        .S_AXI_rready(xbar_to_m06_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m06_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m06_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m06_couplers_WDATA),
        .S_AXI_wlast(xbar_to_m06_couplers_WLAST),
        .S_AXI_wready(xbar_to_m06_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m06_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m06_couplers_WVALID));
  s00_couplers_imp_1LVCP87 s00_couplers
       (.M_ACLK(axi_interconnect_2_ACLK_net),
        .M_ARESETN(axi_interconnect_2_ARESETN_net),
        .M_AXI_araddr(s00_couplers_to_xbar_ARADDR),
        .M_AXI_arburst(s00_couplers_to_xbar_ARBURST),
        .M_AXI_arcache(s00_couplers_to_xbar_ARCACHE),
        .M_AXI_arid(s00_couplers_to_xbar_ARID),
        .M_AXI_arlen(s00_couplers_to_xbar_ARLEN),
        .M_AXI_arlock(s00_couplers_to_xbar_ARLOCK),
        .M_AXI_arprot(s00_couplers_to_xbar_ARPROT),
        .M_AXI_arqos(s00_couplers_to_xbar_ARQOS),
        .M_AXI_arready(s00_couplers_to_xbar_ARREADY),
        .M_AXI_arsize(s00_couplers_to_xbar_ARSIZE),
        .M_AXI_aruser(s00_couplers_to_xbar_ARUSER),
        .M_AXI_arvalid(s00_couplers_to_xbar_ARVALID),
        .M_AXI_awaddr(s00_couplers_to_xbar_AWADDR),
        .M_AXI_awburst(s00_couplers_to_xbar_AWBURST),
        .M_AXI_awcache(s00_couplers_to_xbar_AWCACHE),
        .M_AXI_awid(s00_couplers_to_xbar_AWID),
        .M_AXI_awlen(s00_couplers_to_xbar_AWLEN),
        .M_AXI_awlock(s00_couplers_to_xbar_AWLOCK),
        .M_AXI_awprot(s00_couplers_to_xbar_AWPROT),
        .M_AXI_awqos(s00_couplers_to_xbar_AWQOS),
        .M_AXI_awready(s00_couplers_to_xbar_AWREADY),
        .M_AXI_awsize(s00_couplers_to_xbar_AWSIZE),
        .M_AXI_awuser(s00_couplers_to_xbar_AWUSER),
        .M_AXI_awvalid(s00_couplers_to_xbar_AWVALID),
        .M_AXI_bid(s00_couplers_to_xbar_BID),
        .M_AXI_bready(s00_couplers_to_xbar_BREADY),
        .M_AXI_bresp(s00_couplers_to_xbar_BRESP),
        .M_AXI_buser(s00_couplers_to_xbar_BUSER),
        .M_AXI_bvalid(s00_couplers_to_xbar_BVALID),
        .M_AXI_rdata(s00_couplers_to_xbar_RDATA),
        .M_AXI_rid(s00_couplers_to_xbar_RID),
        .M_AXI_rlast(s00_couplers_to_xbar_RLAST),
        .M_AXI_rready(s00_couplers_to_xbar_RREADY),
        .M_AXI_rresp(s00_couplers_to_xbar_RRESP),
        .M_AXI_ruser(s00_couplers_to_xbar_RUSER),
        .M_AXI_rvalid(s00_couplers_to_xbar_RVALID),
        .M_AXI_wdata(s00_couplers_to_xbar_WDATA),
        .M_AXI_wlast(s00_couplers_to_xbar_WLAST),
        .M_AXI_wready(s00_couplers_to_xbar_WREADY),
        .M_AXI_wstrb(s00_couplers_to_xbar_WSTRB),
        .M_AXI_wuser(s00_couplers_to_xbar_WUSER),
        .M_AXI_wvalid(s00_couplers_to_xbar_WVALID),
        .S_ACLK(axi_interconnect_2_ACLK_net),
        .S_ARESETN(axi_interconnect_2_ARESETN_net),
        .S_AXI_araddr(axi_interconnect_2_to_s00_couplers_ARADDR),
        .S_AXI_arburst(axi_interconnect_2_to_s00_couplers_ARBURST),
        .S_AXI_arcache(axi_interconnect_2_to_s00_couplers_ARCACHE),
        .S_AXI_arid(axi_interconnect_2_to_s00_couplers_ARID),
        .S_AXI_arlen(axi_interconnect_2_to_s00_couplers_ARLEN),
        .S_AXI_arlock(axi_interconnect_2_to_s00_couplers_ARLOCK),
        .S_AXI_arprot(axi_interconnect_2_to_s00_couplers_ARPROT),
        .S_AXI_arqos(axi_interconnect_2_to_s00_couplers_ARQOS),
        .S_AXI_arready(axi_interconnect_2_to_s00_couplers_ARREADY),
        .S_AXI_arsize(axi_interconnect_2_to_s00_couplers_ARSIZE),
        .S_AXI_aruser(axi_interconnect_2_to_s00_couplers_ARUSER),
        .S_AXI_arvalid(axi_interconnect_2_to_s00_couplers_ARVALID),
        .S_AXI_awaddr(axi_interconnect_2_to_s00_couplers_AWADDR),
        .S_AXI_awburst(axi_interconnect_2_to_s00_couplers_AWBURST),
        .S_AXI_awcache(axi_interconnect_2_to_s00_couplers_AWCACHE),
        .S_AXI_awid(axi_interconnect_2_to_s00_couplers_AWID),
        .S_AXI_awlen(axi_interconnect_2_to_s00_couplers_AWLEN),
        .S_AXI_awlock(axi_interconnect_2_to_s00_couplers_AWLOCK),
        .S_AXI_awprot(axi_interconnect_2_to_s00_couplers_AWPROT),
        .S_AXI_awqos(axi_interconnect_2_to_s00_couplers_AWQOS),
        .S_AXI_awready(axi_interconnect_2_to_s00_couplers_AWREADY),
        .S_AXI_awsize(axi_interconnect_2_to_s00_couplers_AWSIZE),
        .S_AXI_awuser(axi_interconnect_2_to_s00_couplers_AWUSER),
        .S_AXI_awvalid(axi_interconnect_2_to_s00_couplers_AWVALID),
        .S_AXI_bid(axi_interconnect_2_to_s00_couplers_BID),
        .S_AXI_bready(axi_interconnect_2_to_s00_couplers_BREADY),
        .S_AXI_bresp(axi_interconnect_2_to_s00_couplers_BRESP),
        .S_AXI_buser(axi_interconnect_2_to_s00_couplers_BUSER),
        .S_AXI_bvalid(axi_interconnect_2_to_s00_couplers_BVALID),
        .S_AXI_rdata(axi_interconnect_2_to_s00_couplers_RDATA),
        .S_AXI_rid(axi_interconnect_2_to_s00_couplers_RID),
        .S_AXI_rlast(axi_interconnect_2_to_s00_couplers_RLAST),
        .S_AXI_rready(axi_interconnect_2_to_s00_couplers_RREADY),
        .S_AXI_rresp(axi_interconnect_2_to_s00_couplers_RRESP),
        .S_AXI_ruser(axi_interconnect_2_to_s00_couplers_RUSER),
        .S_AXI_rvalid(axi_interconnect_2_to_s00_couplers_RVALID),
        .S_AXI_wdata(axi_interconnect_2_to_s00_couplers_WDATA),
        .S_AXI_wlast(axi_interconnect_2_to_s00_couplers_WLAST),
        .S_AXI_wready(axi_interconnect_2_to_s00_couplers_WREADY),
        .S_AXI_wstrb(axi_interconnect_2_to_s00_couplers_WSTRB),
        .S_AXI_wuser(axi_interconnect_2_to_s00_couplers_WUSER),
        .S_AXI_wvalid(axi_interconnect_2_to_s00_couplers_WVALID));
  gpn_bundle_block_xbar_0 xbar
       (.aclk(axi_interconnect_2_ACLK_net),
        .aresetn(axi_interconnect_2_ARESETN_net),
        .m_axi_araddr({xbar_to_m06_couplers_ARADDR,xbar_to_m05_couplers_ARADDR,xbar_to_m04_couplers_ARADDR,xbar_to_m03_couplers_ARADDR,xbar_to_m02_couplers_ARADDR,xbar_to_m01_couplers_ARADDR,xbar_to_m00_couplers_ARADDR}),
        .m_axi_arburst({xbar_to_m06_couplers_ARBURST,xbar_to_m05_couplers_ARBURST,xbar_to_m04_couplers_ARBURST,xbar_to_m03_couplers_ARBURST,xbar_to_m02_couplers_ARBURST,xbar_to_m01_couplers_ARBURST,xbar_to_m00_couplers_ARBURST}),
        .m_axi_arcache({xbar_to_m06_couplers_ARCACHE,xbar_to_m05_couplers_ARCACHE,xbar_to_m04_couplers_ARCACHE,xbar_to_m03_couplers_ARCACHE,xbar_to_m02_couplers_ARCACHE,xbar_to_m01_couplers_ARCACHE,xbar_to_m00_couplers_ARCACHE}),
        .m_axi_arid({xbar_to_m06_couplers_ARID,xbar_to_m05_couplers_ARID,xbar_to_m04_couplers_ARID,xbar_to_m03_couplers_ARID,xbar_to_m02_couplers_ARID,xbar_to_m01_couplers_ARID,xbar_to_m00_couplers_ARID}),
        .m_axi_arlen({xbar_to_m06_couplers_ARLEN,xbar_to_m05_couplers_ARLEN,xbar_to_m04_couplers_ARLEN,xbar_to_m03_couplers_ARLEN,xbar_to_m02_couplers_ARLEN,xbar_to_m01_couplers_ARLEN,xbar_to_m00_couplers_ARLEN}),
        .m_axi_arlock({xbar_to_m06_couplers_ARLOCK,xbar_to_m05_couplers_ARLOCK,xbar_to_m04_couplers_ARLOCK,xbar_to_m03_couplers_ARLOCK,xbar_to_m02_couplers_ARLOCK,xbar_to_m01_couplers_ARLOCK,xbar_to_m00_couplers_ARLOCK}),
        .m_axi_arprot({xbar_to_m06_couplers_ARPROT,xbar_to_m05_couplers_ARPROT,xbar_to_m04_couplers_ARPROT,xbar_to_m03_couplers_ARPROT,xbar_to_m02_couplers_ARPROT,xbar_to_m01_couplers_ARPROT,xbar_to_m00_couplers_ARPROT}),
        .m_axi_arqos({xbar_to_m06_couplers_ARQOS,xbar_to_m05_couplers_ARQOS,xbar_to_m04_couplers_ARQOS,xbar_to_m03_couplers_ARQOS,xbar_to_m02_couplers_ARQOS,xbar_to_m01_couplers_ARQOS,xbar_to_m00_couplers_ARQOS}),
        .m_axi_arready({xbar_to_m06_couplers_ARREADY,xbar_to_m05_couplers_ARREADY,xbar_to_m04_couplers_ARREADY,xbar_to_m03_couplers_ARREADY,xbar_to_m02_couplers_ARREADY,xbar_to_m01_couplers_ARREADY,xbar_to_m00_couplers_ARREADY}),
        .m_axi_arregion({xbar_to_m06_couplers_ARREGION,xbar_to_m05_couplers_ARREGION,xbar_to_m04_couplers_ARREGION,xbar_to_m03_couplers_ARREGION,xbar_to_m02_couplers_ARREGION,xbar_to_m01_couplers_ARREGION,xbar_to_m00_couplers_ARREGION}),
        .m_axi_arsize({xbar_to_m06_couplers_ARSIZE,xbar_to_m05_couplers_ARSIZE,xbar_to_m04_couplers_ARSIZE,xbar_to_m03_couplers_ARSIZE,xbar_to_m02_couplers_ARSIZE,xbar_to_m01_couplers_ARSIZE,xbar_to_m00_couplers_ARSIZE}),
        .m_axi_aruser({xbar_to_m03_couplers_ARUSER,NLW_xbar_m_axi_aruser_UNCONNECTED[11:4],xbar_to_m00_couplers_ARUSER}),
        .m_axi_arvalid({xbar_to_m06_couplers_ARVALID,xbar_to_m05_couplers_ARVALID,xbar_to_m04_couplers_ARVALID,xbar_to_m03_couplers_ARVALID,xbar_to_m02_couplers_ARVALID,xbar_to_m01_couplers_ARVALID,xbar_to_m00_couplers_ARVALID}),
        .m_axi_awaddr({xbar_to_m06_couplers_AWADDR,xbar_to_m05_couplers_AWADDR,xbar_to_m04_couplers_AWADDR,xbar_to_m03_couplers_AWADDR,xbar_to_m02_couplers_AWADDR,xbar_to_m01_couplers_AWADDR,xbar_to_m00_couplers_AWADDR}),
        .m_axi_awburst({xbar_to_m06_couplers_AWBURST,xbar_to_m05_couplers_AWBURST,xbar_to_m04_couplers_AWBURST,xbar_to_m03_couplers_AWBURST,xbar_to_m02_couplers_AWBURST,xbar_to_m01_couplers_AWBURST,xbar_to_m00_couplers_AWBURST}),
        .m_axi_awcache({xbar_to_m06_couplers_AWCACHE,xbar_to_m05_couplers_AWCACHE,xbar_to_m04_couplers_AWCACHE,xbar_to_m03_couplers_AWCACHE,xbar_to_m02_couplers_AWCACHE,xbar_to_m01_couplers_AWCACHE,xbar_to_m00_couplers_AWCACHE}),
        .m_axi_awid({xbar_to_m06_couplers_AWID,xbar_to_m05_couplers_AWID,xbar_to_m04_couplers_AWID,xbar_to_m03_couplers_AWID,xbar_to_m02_couplers_AWID,xbar_to_m01_couplers_AWID,xbar_to_m00_couplers_AWID}),
        .m_axi_awlen({xbar_to_m06_couplers_AWLEN,xbar_to_m05_couplers_AWLEN,xbar_to_m04_couplers_AWLEN,xbar_to_m03_couplers_AWLEN,xbar_to_m02_couplers_AWLEN,xbar_to_m01_couplers_AWLEN,xbar_to_m00_couplers_AWLEN}),
        .m_axi_awlock({xbar_to_m06_couplers_AWLOCK,xbar_to_m05_couplers_AWLOCK,xbar_to_m04_couplers_AWLOCK,xbar_to_m03_couplers_AWLOCK,xbar_to_m02_couplers_AWLOCK,xbar_to_m01_couplers_AWLOCK,xbar_to_m00_couplers_AWLOCK}),
        .m_axi_awprot({xbar_to_m06_couplers_AWPROT,xbar_to_m05_couplers_AWPROT,xbar_to_m04_couplers_AWPROT,xbar_to_m03_couplers_AWPROT,xbar_to_m02_couplers_AWPROT,xbar_to_m01_couplers_AWPROT,xbar_to_m00_couplers_AWPROT}),
        .m_axi_awqos({xbar_to_m06_couplers_AWQOS,xbar_to_m05_couplers_AWQOS,xbar_to_m04_couplers_AWQOS,xbar_to_m03_couplers_AWQOS,xbar_to_m02_couplers_AWQOS,xbar_to_m01_couplers_AWQOS,xbar_to_m00_couplers_AWQOS}),
        .m_axi_awready({xbar_to_m06_couplers_AWREADY,xbar_to_m05_couplers_AWREADY,xbar_to_m04_couplers_AWREADY,xbar_to_m03_couplers_AWREADY,xbar_to_m02_couplers_AWREADY,xbar_to_m01_couplers_AWREADY,xbar_to_m00_couplers_AWREADY}),
        .m_axi_awregion({xbar_to_m06_couplers_AWREGION,xbar_to_m05_couplers_AWREGION,xbar_to_m04_couplers_AWREGION,xbar_to_m03_couplers_AWREGION,xbar_to_m02_couplers_AWREGION,xbar_to_m01_couplers_AWREGION,xbar_to_m00_couplers_AWREGION}),
        .m_axi_awsize({xbar_to_m06_couplers_AWSIZE,xbar_to_m05_couplers_AWSIZE,xbar_to_m04_couplers_AWSIZE,xbar_to_m03_couplers_AWSIZE,xbar_to_m02_couplers_AWSIZE,xbar_to_m01_couplers_AWSIZE,xbar_to_m00_couplers_AWSIZE}),
        .m_axi_awuser({xbar_to_m03_couplers_AWUSER,NLW_xbar_m_axi_awuser_UNCONNECTED[11:4],xbar_to_m00_couplers_AWUSER}),
        .m_axi_awvalid({xbar_to_m06_couplers_AWVALID,xbar_to_m05_couplers_AWVALID,xbar_to_m04_couplers_AWVALID,xbar_to_m03_couplers_AWVALID,xbar_to_m02_couplers_AWVALID,xbar_to_m01_couplers_AWVALID,xbar_to_m00_couplers_AWVALID}),
        .m_axi_bid({xbar_to_m06_couplers_BID,xbar_to_m05_couplers_BID,xbar_to_m04_couplers_BID,xbar_to_m03_couplers_BID,xbar_to_m02_couplers_BID,xbar_to_m01_couplers_BID,xbar_to_m00_couplers_BID}),
        .m_axi_bready({xbar_to_m06_couplers_BREADY,xbar_to_m05_couplers_BREADY,xbar_to_m04_couplers_BREADY,xbar_to_m03_couplers_BREADY,xbar_to_m02_couplers_BREADY,xbar_to_m01_couplers_BREADY,xbar_to_m00_couplers_BREADY}),
        .m_axi_bresp({xbar_to_m06_couplers_BRESP,xbar_to_m05_couplers_BRESP,xbar_to_m04_couplers_BRESP,xbar_to_m03_couplers_BRESP,xbar_to_m02_couplers_BRESP,xbar_to_m01_couplers_BRESP,xbar_to_m00_couplers_BRESP}),
        .m_axi_buser({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,xbar_to_m03_couplers_BUSER,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,xbar_to_m00_couplers_BUSER}),
        .m_axi_bvalid({xbar_to_m06_couplers_BVALID,xbar_to_m05_couplers_BVALID,xbar_to_m04_couplers_BVALID,xbar_to_m03_couplers_BVALID,xbar_to_m02_couplers_BVALID,xbar_to_m01_couplers_BVALID,xbar_to_m00_couplers_BVALID}),
        .m_axi_rdata({xbar_to_m06_couplers_RDATA,xbar_to_m05_couplers_RDATA,xbar_to_m04_couplers_RDATA,xbar_to_m03_couplers_RDATA,xbar_to_m02_couplers_RDATA,xbar_to_m01_couplers_RDATA,xbar_to_m00_couplers_RDATA}),
        .m_axi_rid({xbar_to_m06_couplers_RID,xbar_to_m05_couplers_RID,xbar_to_m04_couplers_RID,xbar_to_m03_couplers_RID,xbar_to_m02_couplers_RID,xbar_to_m01_couplers_RID,xbar_to_m00_couplers_RID}),
        .m_axi_rlast({xbar_to_m06_couplers_RLAST,xbar_to_m05_couplers_RLAST,xbar_to_m04_couplers_RLAST,xbar_to_m03_couplers_RLAST,xbar_to_m02_couplers_RLAST,xbar_to_m01_couplers_RLAST,xbar_to_m00_couplers_RLAST}),
        .m_axi_rready({xbar_to_m06_couplers_RREADY,xbar_to_m05_couplers_RREADY,xbar_to_m04_couplers_RREADY,xbar_to_m03_couplers_RREADY,xbar_to_m02_couplers_RREADY,xbar_to_m01_couplers_RREADY,xbar_to_m00_couplers_RREADY}),
        .m_axi_rresp({xbar_to_m06_couplers_RRESP,xbar_to_m05_couplers_RRESP,xbar_to_m04_couplers_RRESP,xbar_to_m03_couplers_RRESP,xbar_to_m02_couplers_RRESP,xbar_to_m01_couplers_RRESP,xbar_to_m00_couplers_RRESP}),
        .m_axi_ruser({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,xbar_to_m03_couplers_RUSER,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,xbar_to_m00_couplers_RUSER}),
        .m_axi_rvalid({xbar_to_m06_couplers_RVALID,xbar_to_m05_couplers_RVALID,xbar_to_m04_couplers_RVALID,xbar_to_m03_couplers_RVALID,xbar_to_m02_couplers_RVALID,xbar_to_m01_couplers_RVALID,xbar_to_m00_couplers_RVALID}),
        .m_axi_wdata({xbar_to_m06_couplers_WDATA,xbar_to_m05_couplers_WDATA,xbar_to_m04_couplers_WDATA,xbar_to_m03_couplers_WDATA,xbar_to_m02_couplers_WDATA,xbar_to_m01_couplers_WDATA,xbar_to_m00_couplers_WDATA}),
        .m_axi_wlast({xbar_to_m06_couplers_WLAST,xbar_to_m05_couplers_WLAST,xbar_to_m04_couplers_WLAST,xbar_to_m03_couplers_WLAST,xbar_to_m02_couplers_WLAST,xbar_to_m01_couplers_WLAST,xbar_to_m00_couplers_WLAST}),
        .m_axi_wready({xbar_to_m06_couplers_WREADY,xbar_to_m05_couplers_WREADY,xbar_to_m04_couplers_WREADY,xbar_to_m03_couplers_WREADY,xbar_to_m02_couplers_WREADY,xbar_to_m01_couplers_WREADY,xbar_to_m00_couplers_WREADY}),
        .m_axi_wstrb({xbar_to_m06_couplers_WSTRB,xbar_to_m05_couplers_WSTRB,xbar_to_m04_couplers_WSTRB,xbar_to_m03_couplers_WSTRB,xbar_to_m02_couplers_WSTRB,xbar_to_m01_couplers_WSTRB,xbar_to_m00_couplers_WSTRB}),
        .m_axi_wuser({xbar_to_m03_couplers_WUSER,NLW_xbar_m_axi_wuser_UNCONNECTED[11:4],xbar_to_m00_couplers_WUSER}),
        .m_axi_wvalid({xbar_to_m06_couplers_WVALID,xbar_to_m05_couplers_WVALID,xbar_to_m04_couplers_WVALID,xbar_to_m03_couplers_WVALID,xbar_to_m02_couplers_WVALID,xbar_to_m01_couplers_WVALID,xbar_to_m00_couplers_WVALID}),
        .s_axi_araddr(s00_couplers_to_xbar_ARADDR),
        .s_axi_arburst(s00_couplers_to_xbar_ARBURST),
        .s_axi_arcache(s00_couplers_to_xbar_ARCACHE),
        .s_axi_arid(s00_couplers_to_xbar_ARID),
        .s_axi_arlen(s00_couplers_to_xbar_ARLEN),
        .s_axi_arlock(s00_couplers_to_xbar_ARLOCK),
        .s_axi_arprot(s00_couplers_to_xbar_ARPROT),
        .s_axi_arqos(s00_couplers_to_xbar_ARQOS),
        .s_axi_arready(s00_couplers_to_xbar_ARREADY),
        .s_axi_arsize(s00_couplers_to_xbar_ARSIZE),
        .s_axi_aruser(s00_couplers_to_xbar_ARUSER),
        .s_axi_arvalid(s00_couplers_to_xbar_ARVALID),
        .s_axi_awaddr(s00_couplers_to_xbar_AWADDR),
        .s_axi_awburst(s00_couplers_to_xbar_AWBURST),
        .s_axi_awcache(s00_couplers_to_xbar_AWCACHE),
        .s_axi_awid(s00_couplers_to_xbar_AWID),
        .s_axi_awlen(s00_couplers_to_xbar_AWLEN),
        .s_axi_awlock(s00_couplers_to_xbar_AWLOCK),
        .s_axi_awprot(s00_couplers_to_xbar_AWPROT),
        .s_axi_awqos(s00_couplers_to_xbar_AWQOS),
        .s_axi_awready(s00_couplers_to_xbar_AWREADY),
        .s_axi_awsize(s00_couplers_to_xbar_AWSIZE),
        .s_axi_awuser(s00_couplers_to_xbar_AWUSER),
        .s_axi_awvalid(s00_couplers_to_xbar_AWVALID),
        .s_axi_bid(s00_couplers_to_xbar_BID),
        .s_axi_bready(s00_couplers_to_xbar_BREADY),
        .s_axi_bresp(s00_couplers_to_xbar_BRESP),
        .s_axi_buser(s00_couplers_to_xbar_BUSER),
        .s_axi_bvalid(s00_couplers_to_xbar_BVALID),
        .s_axi_rdata(s00_couplers_to_xbar_RDATA),
        .s_axi_rid(s00_couplers_to_xbar_RID),
        .s_axi_rlast(s00_couplers_to_xbar_RLAST),
        .s_axi_rready(s00_couplers_to_xbar_RREADY),
        .s_axi_rresp(s00_couplers_to_xbar_RRESP),
        .s_axi_ruser(s00_couplers_to_xbar_RUSER),
        .s_axi_rvalid(s00_couplers_to_xbar_RVALID),
        .s_axi_wdata(s00_couplers_to_xbar_WDATA),
        .s_axi_wlast(s00_couplers_to_xbar_WLAST),
        .s_axi_wready(s00_couplers_to_xbar_WREADY),
        .s_axi_wstrb(s00_couplers_to_xbar_WSTRB),
        .s_axi_wuser(s00_couplers_to_xbar_WUSER),
        .s_axi_wvalid(s00_couplers_to_xbar_WVALID));
endmodule

module gpn_bundle_block_axi_interconnect_0_3
   (ACLK,
    ARESETN,
    M00_ACLK,
    M00_ARESETN,
    M00_AXI_araddr,
    M00_AXI_arburst,
    M00_AXI_arcache,
    M00_AXI_arid,
    M00_AXI_arlen,
    M00_AXI_arlock,
    M00_AXI_arprot,
    M00_AXI_arqos,
    M00_AXI_arready,
    M00_AXI_arregion,
    M00_AXI_arsize,
    M00_AXI_aruser,
    M00_AXI_arvalid,
    M00_AXI_awaddr,
    M00_AXI_awburst,
    M00_AXI_awcache,
    M00_AXI_awid,
    M00_AXI_awlen,
    M00_AXI_awlock,
    M00_AXI_awprot,
    M00_AXI_awqos,
    M00_AXI_awready,
    M00_AXI_awregion,
    M00_AXI_awsize,
    M00_AXI_awuser,
    M00_AXI_awvalid,
    M00_AXI_bid,
    M00_AXI_bready,
    M00_AXI_bresp,
    M00_AXI_buser,
    M00_AXI_bvalid,
    M00_AXI_rdata,
    M00_AXI_rid,
    M00_AXI_rlast,
    M00_AXI_rready,
    M00_AXI_rresp,
    M00_AXI_ruser,
    M00_AXI_rvalid,
    M00_AXI_wdata,
    M00_AXI_wlast,
    M00_AXI_wready,
    M00_AXI_wstrb,
    M00_AXI_wuser,
    M00_AXI_wvalid,
    S00_ACLK,
    S00_ARESETN,
    S00_AXI_araddr,
    S00_AXI_arburst,
    S00_AXI_arcache,
    S00_AXI_arid,
    S00_AXI_arlen,
    S00_AXI_arlock,
    S00_AXI_arprot,
    S00_AXI_arqos,
    S00_AXI_arready,
    S00_AXI_arregion,
    S00_AXI_arsize,
    S00_AXI_aruser,
    S00_AXI_arvalid,
    S00_AXI_awaddr,
    S00_AXI_awburst,
    S00_AXI_awcache,
    S00_AXI_awid,
    S00_AXI_awlen,
    S00_AXI_awlock,
    S00_AXI_awprot,
    S00_AXI_awqos,
    S00_AXI_awready,
    S00_AXI_awregion,
    S00_AXI_awsize,
    S00_AXI_awuser,
    S00_AXI_awvalid,
    S00_AXI_bid,
    S00_AXI_bready,
    S00_AXI_bresp,
    S00_AXI_buser,
    S00_AXI_bvalid,
    S00_AXI_rdata,
    S00_AXI_rid,
    S00_AXI_rlast,
    S00_AXI_rready,
    S00_AXI_rresp,
    S00_AXI_ruser,
    S00_AXI_rvalid,
    S00_AXI_wdata,
    S00_AXI_wlast,
    S00_AXI_wready,
    S00_AXI_wstrb,
    S00_AXI_wuser,
    S00_AXI_wvalid);
  input ACLK;
  input ARESETN;
  input M00_ACLK;
  input M00_ARESETN;
  output [31:0]M00_AXI_araddr;
  output [1:0]M00_AXI_arburst;
  output [3:0]M00_AXI_arcache;
  output [5:0]M00_AXI_arid;
  output [7:0]M00_AXI_arlen;
  output [0:0]M00_AXI_arlock;
  output [2:0]M00_AXI_arprot;
  output [3:0]M00_AXI_arqos;
  input M00_AXI_arready;
  output [3:0]M00_AXI_arregion;
  output [2:0]M00_AXI_arsize;
  output [3:0]M00_AXI_aruser;
  output M00_AXI_arvalid;
  output [31:0]M00_AXI_awaddr;
  output [1:0]M00_AXI_awburst;
  output [3:0]M00_AXI_awcache;
  output [5:0]M00_AXI_awid;
  output [7:0]M00_AXI_awlen;
  output [0:0]M00_AXI_awlock;
  output [2:0]M00_AXI_awprot;
  output [3:0]M00_AXI_awqos;
  input M00_AXI_awready;
  output [3:0]M00_AXI_awregion;
  output [2:0]M00_AXI_awsize;
  output [3:0]M00_AXI_awuser;
  output M00_AXI_awvalid;
  input [5:0]M00_AXI_bid;
  output M00_AXI_bready;
  input [1:0]M00_AXI_bresp;
  input [3:0]M00_AXI_buser;
  input M00_AXI_bvalid;
  input [31:0]M00_AXI_rdata;
  input [5:0]M00_AXI_rid;
  input M00_AXI_rlast;
  output M00_AXI_rready;
  input [1:0]M00_AXI_rresp;
  input [3:0]M00_AXI_ruser;
  input M00_AXI_rvalid;
  output [31:0]M00_AXI_wdata;
  output M00_AXI_wlast;
  input M00_AXI_wready;
  output [3:0]M00_AXI_wstrb;
  output [3:0]M00_AXI_wuser;
  output M00_AXI_wvalid;
  input S00_ACLK;
  input S00_ARESETN;
  input [31:0]S00_AXI_araddr;
  input [1:0]S00_AXI_arburst;
  input [3:0]S00_AXI_arcache;
  input [5:0]S00_AXI_arid;
  input [7:0]S00_AXI_arlen;
  input [0:0]S00_AXI_arlock;
  input [2:0]S00_AXI_arprot;
  input [3:0]S00_AXI_arqos;
  output [0:0]S00_AXI_arready;
  input [3:0]S00_AXI_arregion;
  input [2:0]S00_AXI_arsize;
  input [3:0]S00_AXI_aruser;
  input [0:0]S00_AXI_arvalid;
  input [31:0]S00_AXI_awaddr;
  input [1:0]S00_AXI_awburst;
  input [3:0]S00_AXI_awcache;
  input [5:0]S00_AXI_awid;
  input [7:0]S00_AXI_awlen;
  input [0:0]S00_AXI_awlock;
  input [2:0]S00_AXI_awprot;
  input [3:0]S00_AXI_awqos;
  output [0:0]S00_AXI_awready;
  input [3:0]S00_AXI_awregion;
  input [2:0]S00_AXI_awsize;
  input [3:0]S00_AXI_awuser;
  input [0:0]S00_AXI_awvalid;
  output [5:0]S00_AXI_bid;
  input [0:0]S00_AXI_bready;
  output [1:0]S00_AXI_bresp;
  output [3:0]S00_AXI_buser;
  output [0:0]S00_AXI_bvalid;
  output [31:0]S00_AXI_rdata;
  output [5:0]S00_AXI_rid;
  output [0:0]S00_AXI_rlast;
  input [0:0]S00_AXI_rready;
  output [1:0]S00_AXI_rresp;
  output [3:0]S00_AXI_ruser;
  output [0:0]S00_AXI_rvalid;
  input [31:0]S00_AXI_wdata;
  input [0:0]S00_AXI_wlast;
  output [0:0]S00_AXI_wready;
  input [3:0]S00_AXI_wstrb;
  input [3:0]S00_AXI_wuser;
  input [0:0]S00_AXI_wvalid;

  wire S00_ACLK_1;
  wire S00_ARESETN_1;
  wire axi_interconnect_1_ACLK_net;
  wire axi_interconnect_1_ARESETN_net;
  wire [31:0]axi_interconnect_1_to_s00_couplers_ARADDR;
  wire [1:0]axi_interconnect_1_to_s00_couplers_ARBURST;
  wire [3:0]axi_interconnect_1_to_s00_couplers_ARCACHE;
  wire [5:0]axi_interconnect_1_to_s00_couplers_ARID;
  wire [7:0]axi_interconnect_1_to_s00_couplers_ARLEN;
  wire [0:0]axi_interconnect_1_to_s00_couplers_ARLOCK;
  wire [2:0]axi_interconnect_1_to_s00_couplers_ARPROT;
  wire [3:0]axi_interconnect_1_to_s00_couplers_ARQOS;
  wire [0:0]axi_interconnect_1_to_s00_couplers_ARREADY;
  wire [3:0]axi_interconnect_1_to_s00_couplers_ARREGION;
  wire [2:0]axi_interconnect_1_to_s00_couplers_ARSIZE;
  wire [3:0]axi_interconnect_1_to_s00_couplers_ARUSER;
  wire [0:0]axi_interconnect_1_to_s00_couplers_ARVALID;
  wire [31:0]axi_interconnect_1_to_s00_couplers_AWADDR;
  wire [1:0]axi_interconnect_1_to_s00_couplers_AWBURST;
  wire [3:0]axi_interconnect_1_to_s00_couplers_AWCACHE;
  wire [5:0]axi_interconnect_1_to_s00_couplers_AWID;
  wire [7:0]axi_interconnect_1_to_s00_couplers_AWLEN;
  wire [0:0]axi_interconnect_1_to_s00_couplers_AWLOCK;
  wire [2:0]axi_interconnect_1_to_s00_couplers_AWPROT;
  wire [3:0]axi_interconnect_1_to_s00_couplers_AWQOS;
  wire [0:0]axi_interconnect_1_to_s00_couplers_AWREADY;
  wire [3:0]axi_interconnect_1_to_s00_couplers_AWREGION;
  wire [2:0]axi_interconnect_1_to_s00_couplers_AWSIZE;
  wire [3:0]axi_interconnect_1_to_s00_couplers_AWUSER;
  wire [0:0]axi_interconnect_1_to_s00_couplers_AWVALID;
  wire [5:0]axi_interconnect_1_to_s00_couplers_BID;
  wire [0:0]axi_interconnect_1_to_s00_couplers_BREADY;
  wire [1:0]axi_interconnect_1_to_s00_couplers_BRESP;
  wire [3:0]axi_interconnect_1_to_s00_couplers_BUSER;
  wire [0:0]axi_interconnect_1_to_s00_couplers_BVALID;
  wire [31:0]axi_interconnect_1_to_s00_couplers_RDATA;
  wire [5:0]axi_interconnect_1_to_s00_couplers_RID;
  wire [0:0]axi_interconnect_1_to_s00_couplers_RLAST;
  wire [0:0]axi_interconnect_1_to_s00_couplers_RREADY;
  wire [1:0]axi_interconnect_1_to_s00_couplers_RRESP;
  wire [3:0]axi_interconnect_1_to_s00_couplers_RUSER;
  wire [0:0]axi_interconnect_1_to_s00_couplers_RVALID;
  wire [31:0]axi_interconnect_1_to_s00_couplers_WDATA;
  wire [0:0]axi_interconnect_1_to_s00_couplers_WLAST;
  wire [0:0]axi_interconnect_1_to_s00_couplers_WREADY;
  wire [3:0]axi_interconnect_1_to_s00_couplers_WSTRB;
  wire [3:0]axi_interconnect_1_to_s00_couplers_WUSER;
  wire [0:0]axi_interconnect_1_to_s00_couplers_WVALID;
  wire [31:0]s00_couplers_to_axi_interconnect_1_ARADDR;
  wire [1:0]s00_couplers_to_axi_interconnect_1_ARBURST;
  wire [3:0]s00_couplers_to_axi_interconnect_1_ARCACHE;
  wire [5:0]s00_couplers_to_axi_interconnect_1_ARID;
  wire [7:0]s00_couplers_to_axi_interconnect_1_ARLEN;
  wire [0:0]s00_couplers_to_axi_interconnect_1_ARLOCK;
  wire [2:0]s00_couplers_to_axi_interconnect_1_ARPROT;
  wire [3:0]s00_couplers_to_axi_interconnect_1_ARQOS;
  wire s00_couplers_to_axi_interconnect_1_ARREADY;
  wire [3:0]s00_couplers_to_axi_interconnect_1_ARREGION;
  wire [2:0]s00_couplers_to_axi_interconnect_1_ARSIZE;
  wire [3:0]s00_couplers_to_axi_interconnect_1_ARUSER;
  wire s00_couplers_to_axi_interconnect_1_ARVALID;
  wire [31:0]s00_couplers_to_axi_interconnect_1_AWADDR;
  wire [1:0]s00_couplers_to_axi_interconnect_1_AWBURST;
  wire [3:0]s00_couplers_to_axi_interconnect_1_AWCACHE;
  wire [5:0]s00_couplers_to_axi_interconnect_1_AWID;
  wire [7:0]s00_couplers_to_axi_interconnect_1_AWLEN;
  wire [0:0]s00_couplers_to_axi_interconnect_1_AWLOCK;
  wire [2:0]s00_couplers_to_axi_interconnect_1_AWPROT;
  wire [3:0]s00_couplers_to_axi_interconnect_1_AWQOS;
  wire s00_couplers_to_axi_interconnect_1_AWREADY;
  wire [3:0]s00_couplers_to_axi_interconnect_1_AWREGION;
  wire [2:0]s00_couplers_to_axi_interconnect_1_AWSIZE;
  wire [3:0]s00_couplers_to_axi_interconnect_1_AWUSER;
  wire s00_couplers_to_axi_interconnect_1_AWVALID;
  wire [5:0]s00_couplers_to_axi_interconnect_1_BID;
  wire s00_couplers_to_axi_interconnect_1_BREADY;
  wire [1:0]s00_couplers_to_axi_interconnect_1_BRESP;
  wire [3:0]s00_couplers_to_axi_interconnect_1_BUSER;
  wire s00_couplers_to_axi_interconnect_1_BVALID;
  wire [31:0]s00_couplers_to_axi_interconnect_1_RDATA;
  wire [5:0]s00_couplers_to_axi_interconnect_1_RID;
  wire s00_couplers_to_axi_interconnect_1_RLAST;
  wire s00_couplers_to_axi_interconnect_1_RREADY;
  wire [1:0]s00_couplers_to_axi_interconnect_1_RRESP;
  wire [3:0]s00_couplers_to_axi_interconnect_1_RUSER;
  wire s00_couplers_to_axi_interconnect_1_RVALID;
  wire [31:0]s00_couplers_to_axi_interconnect_1_WDATA;
  wire s00_couplers_to_axi_interconnect_1_WLAST;
  wire s00_couplers_to_axi_interconnect_1_WREADY;
  wire [3:0]s00_couplers_to_axi_interconnect_1_WSTRB;
  wire [3:0]s00_couplers_to_axi_interconnect_1_WUSER;
  wire s00_couplers_to_axi_interconnect_1_WVALID;

  assign M00_AXI_araddr[31:0] = s00_couplers_to_axi_interconnect_1_ARADDR;
  assign M00_AXI_arburst[1:0] = s00_couplers_to_axi_interconnect_1_ARBURST;
  assign M00_AXI_arcache[3:0] = s00_couplers_to_axi_interconnect_1_ARCACHE;
  assign M00_AXI_arid[5:0] = s00_couplers_to_axi_interconnect_1_ARID;
  assign M00_AXI_arlen[7:0] = s00_couplers_to_axi_interconnect_1_ARLEN;
  assign M00_AXI_arlock[0] = s00_couplers_to_axi_interconnect_1_ARLOCK;
  assign M00_AXI_arprot[2:0] = s00_couplers_to_axi_interconnect_1_ARPROT;
  assign M00_AXI_arqos[3:0] = s00_couplers_to_axi_interconnect_1_ARQOS;
  assign M00_AXI_arregion[3:0] = s00_couplers_to_axi_interconnect_1_ARREGION;
  assign M00_AXI_arsize[2:0] = s00_couplers_to_axi_interconnect_1_ARSIZE;
  assign M00_AXI_aruser[3:0] = s00_couplers_to_axi_interconnect_1_ARUSER;
  assign M00_AXI_arvalid = s00_couplers_to_axi_interconnect_1_ARVALID;
  assign M00_AXI_awaddr[31:0] = s00_couplers_to_axi_interconnect_1_AWADDR;
  assign M00_AXI_awburst[1:0] = s00_couplers_to_axi_interconnect_1_AWBURST;
  assign M00_AXI_awcache[3:0] = s00_couplers_to_axi_interconnect_1_AWCACHE;
  assign M00_AXI_awid[5:0] = s00_couplers_to_axi_interconnect_1_AWID;
  assign M00_AXI_awlen[7:0] = s00_couplers_to_axi_interconnect_1_AWLEN;
  assign M00_AXI_awlock[0] = s00_couplers_to_axi_interconnect_1_AWLOCK;
  assign M00_AXI_awprot[2:0] = s00_couplers_to_axi_interconnect_1_AWPROT;
  assign M00_AXI_awqos[3:0] = s00_couplers_to_axi_interconnect_1_AWQOS;
  assign M00_AXI_awregion[3:0] = s00_couplers_to_axi_interconnect_1_AWREGION;
  assign M00_AXI_awsize[2:0] = s00_couplers_to_axi_interconnect_1_AWSIZE;
  assign M00_AXI_awuser[3:0] = s00_couplers_to_axi_interconnect_1_AWUSER;
  assign M00_AXI_awvalid = s00_couplers_to_axi_interconnect_1_AWVALID;
  assign M00_AXI_bready = s00_couplers_to_axi_interconnect_1_BREADY;
  assign M00_AXI_rready = s00_couplers_to_axi_interconnect_1_RREADY;
  assign M00_AXI_wdata[31:0] = s00_couplers_to_axi_interconnect_1_WDATA;
  assign M00_AXI_wlast = s00_couplers_to_axi_interconnect_1_WLAST;
  assign M00_AXI_wstrb[3:0] = s00_couplers_to_axi_interconnect_1_WSTRB;
  assign M00_AXI_wuser[3:0] = s00_couplers_to_axi_interconnect_1_WUSER;
  assign M00_AXI_wvalid = s00_couplers_to_axi_interconnect_1_WVALID;
  assign S00_ACLK_1 = S00_ACLK;
  assign S00_ARESETN_1 = S00_ARESETN;
  assign S00_AXI_arready[0] = axi_interconnect_1_to_s00_couplers_ARREADY;
  assign S00_AXI_awready[0] = axi_interconnect_1_to_s00_couplers_AWREADY;
  assign S00_AXI_bid[5:0] = axi_interconnect_1_to_s00_couplers_BID;
  assign S00_AXI_bresp[1:0] = axi_interconnect_1_to_s00_couplers_BRESP;
  assign S00_AXI_buser[3:0] = axi_interconnect_1_to_s00_couplers_BUSER;
  assign S00_AXI_bvalid[0] = axi_interconnect_1_to_s00_couplers_BVALID;
  assign S00_AXI_rdata[31:0] = axi_interconnect_1_to_s00_couplers_RDATA;
  assign S00_AXI_rid[5:0] = axi_interconnect_1_to_s00_couplers_RID;
  assign S00_AXI_rlast[0] = axi_interconnect_1_to_s00_couplers_RLAST;
  assign S00_AXI_rresp[1:0] = axi_interconnect_1_to_s00_couplers_RRESP;
  assign S00_AXI_ruser[3:0] = axi_interconnect_1_to_s00_couplers_RUSER;
  assign S00_AXI_rvalid[0] = axi_interconnect_1_to_s00_couplers_RVALID;
  assign S00_AXI_wready[0] = axi_interconnect_1_to_s00_couplers_WREADY;
  assign axi_interconnect_1_ACLK_net = M00_ACLK;
  assign axi_interconnect_1_ARESETN_net = M00_ARESETN;
  assign axi_interconnect_1_to_s00_couplers_ARADDR = S00_AXI_araddr[31:0];
  assign axi_interconnect_1_to_s00_couplers_ARBURST = S00_AXI_arburst[1:0];
  assign axi_interconnect_1_to_s00_couplers_ARCACHE = S00_AXI_arcache[3:0];
  assign axi_interconnect_1_to_s00_couplers_ARID = S00_AXI_arid[5:0];
  assign axi_interconnect_1_to_s00_couplers_ARLEN = S00_AXI_arlen[7:0];
  assign axi_interconnect_1_to_s00_couplers_ARLOCK = S00_AXI_arlock[0];
  assign axi_interconnect_1_to_s00_couplers_ARPROT = S00_AXI_arprot[2:0];
  assign axi_interconnect_1_to_s00_couplers_ARQOS = S00_AXI_arqos[3:0];
  assign axi_interconnect_1_to_s00_couplers_ARREGION = S00_AXI_arregion[3:0];
  assign axi_interconnect_1_to_s00_couplers_ARSIZE = S00_AXI_arsize[2:0];
  assign axi_interconnect_1_to_s00_couplers_ARUSER = S00_AXI_aruser[3:0];
  assign axi_interconnect_1_to_s00_couplers_ARVALID = S00_AXI_arvalid[0];
  assign axi_interconnect_1_to_s00_couplers_AWADDR = S00_AXI_awaddr[31:0];
  assign axi_interconnect_1_to_s00_couplers_AWBURST = S00_AXI_awburst[1:0];
  assign axi_interconnect_1_to_s00_couplers_AWCACHE = S00_AXI_awcache[3:0];
  assign axi_interconnect_1_to_s00_couplers_AWID = S00_AXI_awid[5:0];
  assign axi_interconnect_1_to_s00_couplers_AWLEN = S00_AXI_awlen[7:0];
  assign axi_interconnect_1_to_s00_couplers_AWLOCK = S00_AXI_awlock[0];
  assign axi_interconnect_1_to_s00_couplers_AWPROT = S00_AXI_awprot[2:0];
  assign axi_interconnect_1_to_s00_couplers_AWQOS = S00_AXI_awqos[3:0];
  assign axi_interconnect_1_to_s00_couplers_AWREGION = S00_AXI_awregion[3:0];
  assign axi_interconnect_1_to_s00_couplers_AWSIZE = S00_AXI_awsize[2:0];
  assign axi_interconnect_1_to_s00_couplers_AWUSER = S00_AXI_awuser[3:0];
  assign axi_interconnect_1_to_s00_couplers_AWVALID = S00_AXI_awvalid[0];
  assign axi_interconnect_1_to_s00_couplers_BREADY = S00_AXI_bready[0];
  assign axi_interconnect_1_to_s00_couplers_RREADY = S00_AXI_rready[0];
  assign axi_interconnect_1_to_s00_couplers_WDATA = S00_AXI_wdata[31:0];
  assign axi_interconnect_1_to_s00_couplers_WLAST = S00_AXI_wlast[0];
  assign axi_interconnect_1_to_s00_couplers_WSTRB = S00_AXI_wstrb[3:0];
  assign axi_interconnect_1_to_s00_couplers_WUSER = S00_AXI_wuser[3:0];
  assign axi_interconnect_1_to_s00_couplers_WVALID = S00_AXI_wvalid[0];
  assign s00_couplers_to_axi_interconnect_1_ARREADY = M00_AXI_arready;
  assign s00_couplers_to_axi_interconnect_1_AWREADY = M00_AXI_awready;
  assign s00_couplers_to_axi_interconnect_1_BID = M00_AXI_bid[5:0];
  assign s00_couplers_to_axi_interconnect_1_BRESP = M00_AXI_bresp[1:0];
  assign s00_couplers_to_axi_interconnect_1_BUSER = M00_AXI_buser[3:0];
  assign s00_couplers_to_axi_interconnect_1_BVALID = M00_AXI_bvalid;
  assign s00_couplers_to_axi_interconnect_1_RDATA = M00_AXI_rdata[31:0];
  assign s00_couplers_to_axi_interconnect_1_RID = M00_AXI_rid[5:0];
  assign s00_couplers_to_axi_interconnect_1_RLAST = M00_AXI_rlast;
  assign s00_couplers_to_axi_interconnect_1_RRESP = M00_AXI_rresp[1:0];
  assign s00_couplers_to_axi_interconnect_1_RUSER = M00_AXI_ruser[3:0];
  assign s00_couplers_to_axi_interconnect_1_RVALID = M00_AXI_rvalid;
  assign s00_couplers_to_axi_interconnect_1_WREADY = M00_AXI_wready;
  s00_couplers_imp_154BRN3 s00_couplers
       (.M_ACLK(axi_interconnect_1_ACLK_net),
        .M_ARESETN(axi_interconnect_1_ARESETN_net),
        .M_AXI_araddr(s00_couplers_to_axi_interconnect_1_ARADDR),
        .M_AXI_arburst(s00_couplers_to_axi_interconnect_1_ARBURST),
        .M_AXI_arcache(s00_couplers_to_axi_interconnect_1_ARCACHE),
        .M_AXI_arid(s00_couplers_to_axi_interconnect_1_ARID),
        .M_AXI_arlen(s00_couplers_to_axi_interconnect_1_ARLEN),
        .M_AXI_arlock(s00_couplers_to_axi_interconnect_1_ARLOCK),
        .M_AXI_arprot(s00_couplers_to_axi_interconnect_1_ARPROT),
        .M_AXI_arqos(s00_couplers_to_axi_interconnect_1_ARQOS),
        .M_AXI_arready(s00_couplers_to_axi_interconnect_1_ARREADY),
        .M_AXI_arregion(s00_couplers_to_axi_interconnect_1_ARREGION),
        .M_AXI_arsize(s00_couplers_to_axi_interconnect_1_ARSIZE),
        .M_AXI_aruser(s00_couplers_to_axi_interconnect_1_ARUSER),
        .M_AXI_arvalid(s00_couplers_to_axi_interconnect_1_ARVALID),
        .M_AXI_awaddr(s00_couplers_to_axi_interconnect_1_AWADDR),
        .M_AXI_awburst(s00_couplers_to_axi_interconnect_1_AWBURST),
        .M_AXI_awcache(s00_couplers_to_axi_interconnect_1_AWCACHE),
        .M_AXI_awid(s00_couplers_to_axi_interconnect_1_AWID),
        .M_AXI_awlen(s00_couplers_to_axi_interconnect_1_AWLEN),
        .M_AXI_awlock(s00_couplers_to_axi_interconnect_1_AWLOCK),
        .M_AXI_awprot(s00_couplers_to_axi_interconnect_1_AWPROT),
        .M_AXI_awqos(s00_couplers_to_axi_interconnect_1_AWQOS),
        .M_AXI_awready(s00_couplers_to_axi_interconnect_1_AWREADY),
        .M_AXI_awregion(s00_couplers_to_axi_interconnect_1_AWREGION),
        .M_AXI_awsize(s00_couplers_to_axi_interconnect_1_AWSIZE),
        .M_AXI_awuser(s00_couplers_to_axi_interconnect_1_AWUSER),
        .M_AXI_awvalid(s00_couplers_to_axi_interconnect_1_AWVALID),
        .M_AXI_bid(s00_couplers_to_axi_interconnect_1_BID),
        .M_AXI_bready(s00_couplers_to_axi_interconnect_1_BREADY),
        .M_AXI_bresp(s00_couplers_to_axi_interconnect_1_BRESP),
        .M_AXI_buser(s00_couplers_to_axi_interconnect_1_BUSER),
        .M_AXI_bvalid(s00_couplers_to_axi_interconnect_1_BVALID),
        .M_AXI_rdata(s00_couplers_to_axi_interconnect_1_RDATA),
        .M_AXI_rid(s00_couplers_to_axi_interconnect_1_RID),
        .M_AXI_rlast(s00_couplers_to_axi_interconnect_1_RLAST),
        .M_AXI_rready(s00_couplers_to_axi_interconnect_1_RREADY),
        .M_AXI_rresp(s00_couplers_to_axi_interconnect_1_RRESP),
        .M_AXI_ruser(s00_couplers_to_axi_interconnect_1_RUSER),
        .M_AXI_rvalid(s00_couplers_to_axi_interconnect_1_RVALID),
        .M_AXI_wdata(s00_couplers_to_axi_interconnect_1_WDATA),
        .M_AXI_wlast(s00_couplers_to_axi_interconnect_1_WLAST),
        .M_AXI_wready(s00_couplers_to_axi_interconnect_1_WREADY),
        .M_AXI_wstrb(s00_couplers_to_axi_interconnect_1_WSTRB),
        .M_AXI_wuser(s00_couplers_to_axi_interconnect_1_WUSER),
        .M_AXI_wvalid(s00_couplers_to_axi_interconnect_1_WVALID),
        .S_ACLK(S00_ACLK_1),
        .S_ARESETN(S00_ARESETN_1),
        .S_AXI_araddr(axi_interconnect_1_to_s00_couplers_ARADDR),
        .S_AXI_arburst(axi_interconnect_1_to_s00_couplers_ARBURST),
        .S_AXI_arcache(axi_interconnect_1_to_s00_couplers_ARCACHE),
        .S_AXI_arid(axi_interconnect_1_to_s00_couplers_ARID),
        .S_AXI_arlen(axi_interconnect_1_to_s00_couplers_ARLEN),
        .S_AXI_arlock(axi_interconnect_1_to_s00_couplers_ARLOCK),
        .S_AXI_arprot(axi_interconnect_1_to_s00_couplers_ARPROT),
        .S_AXI_arqos(axi_interconnect_1_to_s00_couplers_ARQOS),
        .S_AXI_arready(axi_interconnect_1_to_s00_couplers_ARREADY),
        .S_AXI_arregion(axi_interconnect_1_to_s00_couplers_ARREGION),
        .S_AXI_arsize(axi_interconnect_1_to_s00_couplers_ARSIZE),
        .S_AXI_aruser(axi_interconnect_1_to_s00_couplers_ARUSER),
        .S_AXI_arvalid(axi_interconnect_1_to_s00_couplers_ARVALID),
        .S_AXI_awaddr(axi_interconnect_1_to_s00_couplers_AWADDR),
        .S_AXI_awburst(axi_interconnect_1_to_s00_couplers_AWBURST),
        .S_AXI_awcache(axi_interconnect_1_to_s00_couplers_AWCACHE),
        .S_AXI_awid(axi_interconnect_1_to_s00_couplers_AWID),
        .S_AXI_awlen(axi_interconnect_1_to_s00_couplers_AWLEN),
        .S_AXI_awlock(axi_interconnect_1_to_s00_couplers_AWLOCK),
        .S_AXI_awprot(axi_interconnect_1_to_s00_couplers_AWPROT),
        .S_AXI_awqos(axi_interconnect_1_to_s00_couplers_AWQOS),
        .S_AXI_awready(axi_interconnect_1_to_s00_couplers_AWREADY),
        .S_AXI_awregion(axi_interconnect_1_to_s00_couplers_AWREGION),
        .S_AXI_awsize(axi_interconnect_1_to_s00_couplers_AWSIZE),
        .S_AXI_awuser(axi_interconnect_1_to_s00_couplers_AWUSER),
        .S_AXI_awvalid(axi_interconnect_1_to_s00_couplers_AWVALID),
        .S_AXI_bid(axi_interconnect_1_to_s00_couplers_BID),
        .S_AXI_bready(axi_interconnect_1_to_s00_couplers_BREADY),
        .S_AXI_bresp(axi_interconnect_1_to_s00_couplers_BRESP),
        .S_AXI_buser(axi_interconnect_1_to_s00_couplers_BUSER),
        .S_AXI_bvalid(axi_interconnect_1_to_s00_couplers_BVALID),
        .S_AXI_rdata(axi_interconnect_1_to_s00_couplers_RDATA),
        .S_AXI_rid(axi_interconnect_1_to_s00_couplers_RID),
        .S_AXI_rlast(axi_interconnect_1_to_s00_couplers_RLAST),
        .S_AXI_rready(axi_interconnect_1_to_s00_couplers_RREADY),
        .S_AXI_rresp(axi_interconnect_1_to_s00_couplers_RRESP),
        .S_AXI_ruser(axi_interconnect_1_to_s00_couplers_RUSER),
        .S_AXI_rvalid(axi_interconnect_1_to_s00_couplers_RVALID),
        .S_AXI_wdata(axi_interconnect_1_to_s00_couplers_WDATA),
        .S_AXI_wlast(axi_interconnect_1_to_s00_couplers_WLAST),
        .S_AXI_wready(axi_interconnect_1_to_s00_couplers_WREADY),
        .S_AXI_wstrb(axi_interconnect_1_to_s00_couplers_WSTRB),
        .S_AXI_wuser(axi_interconnect_1_to_s00_couplers_WUSER),
        .S_AXI_wvalid(axi_interconnect_1_to_s00_couplers_WVALID));
endmodule

module gpn_bundle_block_axi_interconnect_1_0
   (ACLK,
    ARESETN,
    M00_ACLK,
    M00_ARESETN,
    M00_AXI_araddr,
    M00_AXI_arburst,
    M00_AXI_arcache,
    M00_AXI_arid,
    M00_AXI_arlen,
    M00_AXI_arlock,
    M00_AXI_arprot,
    M00_AXI_arqos,
    M00_AXI_arready,
    M00_AXI_arregion,
    M00_AXI_arsize,
    M00_AXI_aruser,
    M00_AXI_arvalid,
    M00_AXI_awaddr,
    M00_AXI_awburst,
    M00_AXI_awcache,
    M00_AXI_awid,
    M00_AXI_awlen,
    M00_AXI_awlock,
    M00_AXI_awprot,
    M00_AXI_awqos,
    M00_AXI_awready,
    M00_AXI_awregion,
    M00_AXI_awsize,
    M00_AXI_awuser,
    M00_AXI_awvalid,
    M00_AXI_bid,
    M00_AXI_bready,
    M00_AXI_bresp,
    M00_AXI_buser,
    M00_AXI_bvalid,
    M00_AXI_rdata,
    M00_AXI_rid,
    M00_AXI_rlast,
    M00_AXI_rready,
    M00_AXI_rresp,
    M00_AXI_ruser,
    M00_AXI_rvalid,
    M00_AXI_wdata,
    M00_AXI_wlast,
    M00_AXI_wready,
    M00_AXI_wstrb,
    M00_AXI_wuser,
    M00_AXI_wvalid,
    S00_ACLK,
    S00_ARESETN,
    S00_AXI_araddr,
    S00_AXI_arburst,
    S00_AXI_arcache,
    S00_AXI_arid,
    S00_AXI_arlen,
    S00_AXI_arlock,
    S00_AXI_arprot,
    S00_AXI_arqos,
    S00_AXI_arready,
    S00_AXI_arregion,
    S00_AXI_arsize,
    S00_AXI_aruser,
    S00_AXI_arvalid,
    S00_AXI_awaddr,
    S00_AXI_awburst,
    S00_AXI_awcache,
    S00_AXI_awid,
    S00_AXI_awlen,
    S00_AXI_awlock,
    S00_AXI_awprot,
    S00_AXI_awqos,
    S00_AXI_awready,
    S00_AXI_awregion,
    S00_AXI_awsize,
    S00_AXI_awuser,
    S00_AXI_awvalid,
    S00_AXI_bid,
    S00_AXI_bready,
    S00_AXI_bresp,
    S00_AXI_buser,
    S00_AXI_bvalid,
    S00_AXI_rdata,
    S00_AXI_rid,
    S00_AXI_rlast,
    S00_AXI_rready,
    S00_AXI_rresp,
    S00_AXI_ruser,
    S00_AXI_rvalid,
    S00_AXI_wdata,
    S00_AXI_wlast,
    S00_AXI_wready,
    S00_AXI_wstrb,
    S00_AXI_wuser,
    S00_AXI_wvalid);
  input ACLK;
  input ARESETN;
  input M00_ACLK;
  input M00_ARESETN;
  output [31:0]M00_AXI_araddr;
  output [1:0]M00_AXI_arburst;
  output [3:0]M00_AXI_arcache;
  output [5:0]M00_AXI_arid;
  output [7:0]M00_AXI_arlen;
  output [0:0]M00_AXI_arlock;
  output [2:0]M00_AXI_arprot;
  output [3:0]M00_AXI_arqos;
  input M00_AXI_arready;
  output [3:0]M00_AXI_arregion;
  output [2:0]M00_AXI_arsize;
  output [3:0]M00_AXI_aruser;
  output M00_AXI_arvalid;
  output [31:0]M00_AXI_awaddr;
  output [1:0]M00_AXI_awburst;
  output [3:0]M00_AXI_awcache;
  output [5:0]M00_AXI_awid;
  output [7:0]M00_AXI_awlen;
  output [0:0]M00_AXI_awlock;
  output [2:0]M00_AXI_awprot;
  output [3:0]M00_AXI_awqos;
  input M00_AXI_awready;
  output [3:0]M00_AXI_awregion;
  output [2:0]M00_AXI_awsize;
  output [3:0]M00_AXI_awuser;
  output M00_AXI_awvalid;
  input [5:0]M00_AXI_bid;
  output M00_AXI_bready;
  input [1:0]M00_AXI_bresp;
  input [3:0]M00_AXI_buser;
  input M00_AXI_bvalid;
  input [31:0]M00_AXI_rdata;
  input [5:0]M00_AXI_rid;
  input M00_AXI_rlast;
  output M00_AXI_rready;
  input [1:0]M00_AXI_rresp;
  input [3:0]M00_AXI_ruser;
  input M00_AXI_rvalid;
  output [31:0]M00_AXI_wdata;
  output M00_AXI_wlast;
  input M00_AXI_wready;
  output [3:0]M00_AXI_wstrb;
  output [3:0]M00_AXI_wuser;
  output M00_AXI_wvalid;
  input S00_ACLK;
  input S00_ARESETN;
  input [31:0]S00_AXI_araddr;
  input [1:0]S00_AXI_arburst;
  input [3:0]S00_AXI_arcache;
  input [5:0]S00_AXI_arid;
  input [7:0]S00_AXI_arlen;
  input [0:0]S00_AXI_arlock;
  input [2:0]S00_AXI_arprot;
  input [3:0]S00_AXI_arqos;
  output S00_AXI_arready;
  input [3:0]S00_AXI_arregion;
  input [2:0]S00_AXI_arsize;
  input [3:0]S00_AXI_aruser;
  input S00_AXI_arvalid;
  input [31:0]S00_AXI_awaddr;
  input [1:0]S00_AXI_awburst;
  input [3:0]S00_AXI_awcache;
  input [5:0]S00_AXI_awid;
  input [7:0]S00_AXI_awlen;
  input [0:0]S00_AXI_awlock;
  input [2:0]S00_AXI_awprot;
  input [3:0]S00_AXI_awqos;
  output S00_AXI_awready;
  input [3:0]S00_AXI_awregion;
  input [2:0]S00_AXI_awsize;
  input [3:0]S00_AXI_awuser;
  input S00_AXI_awvalid;
  output [5:0]S00_AXI_bid;
  input S00_AXI_bready;
  output [1:0]S00_AXI_bresp;
  output [3:0]S00_AXI_buser;
  output S00_AXI_bvalid;
  output [31:0]S00_AXI_rdata;
  output [5:0]S00_AXI_rid;
  output S00_AXI_rlast;
  input S00_AXI_rready;
  output [1:0]S00_AXI_rresp;
  output [3:0]S00_AXI_ruser;
  output S00_AXI_rvalid;
  input [31:0]S00_AXI_wdata;
  input S00_AXI_wlast;
  output S00_AXI_wready;
  input [3:0]S00_AXI_wstrb;
  input [3:0]S00_AXI_wuser;
  input S00_AXI_wvalid;

  wire S00_ACLK_1;
  wire S00_ARESETN_1;
  wire axi_interconnect_3_ACLK_net;
  wire axi_interconnect_3_ARESETN_net;
  wire [31:0]axi_interconnect_3_to_s00_couplers_ARADDR;
  wire [1:0]axi_interconnect_3_to_s00_couplers_ARBURST;
  wire [3:0]axi_interconnect_3_to_s00_couplers_ARCACHE;
  wire [5:0]axi_interconnect_3_to_s00_couplers_ARID;
  wire [7:0]axi_interconnect_3_to_s00_couplers_ARLEN;
  wire [0:0]axi_interconnect_3_to_s00_couplers_ARLOCK;
  wire [2:0]axi_interconnect_3_to_s00_couplers_ARPROT;
  wire [3:0]axi_interconnect_3_to_s00_couplers_ARQOS;
  wire axi_interconnect_3_to_s00_couplers_ARREADY;
  wire [3:0]axi_interconnect_3_to_s00_couplers_ARREGION;
  wire [2:0]axi_interconnect_3_to_s00_couplers_ARSIZE;
  wire [3:0]axi_interconnect_3_to_s00_couplers_ARUSER;
  wire axi_interconnect_3_to_s00_couplers_ARVALID;
  wire [31:0]axi_interconnect_3_to_s00_couplers_AWADDR;
  wire [1:0]axi_interconnect_3_to_s00_couplers_AWBURST;
  wire [3:0]axi_interconnect_3_to_s00_couplers_AWCACHE;
  wire [5:0]axi_interconnect_3_to_s00_couplers_AWID;
  wire [7:0]axi_interconnect_3_to_s00_couplers_AWLEN;
  wire [0:0]axi_interconnect_3_to_s00_couplers_AWLOCK;
  wire [2:0]axi_interconnect_3_to_s00_couplers_AWPROT;
  wire [3:0]axi_interconnect_3_to_s00_couplers_AWQOS;
  wire axi_interconnect_3_to_s00_couplers_AWREADY;
  wire [3:0]axi_interconnect_3_to_s00_couplers_AWREGION;
  wire [2:0]axi_interconnect_3_to_s00_couplers_AWSIZE;
  wire [3:0]axi_interconnect_3_to_s00_couplers_AWUSER;
  wire axi_interconnect_3_to_s00_couplers_AWVALID;
  wire [5:0]axi_interconnect_3_to_s00_couplers_BID;
  wire axi_interconnect_3_to_s00_couplers_BREADY;
  wire [1:0]axi_interconnect_3_to_s00_couplers_BRESP;
  wire [3:0]axi_interconnect_3_to_s00_couplers_BUSER;
  wire axi_interconnect_3_to_s00_couplers_BVALID;
  wire [31:0]axi_interconnect_3_to_s00_couplers_RDATA;
  wire [5:0]axi_interconnect_3_to_s00_couplers_RID;
  wire axi_interconnect_3_to_s00_couplers_RLAST;
  wire axi_interconnect_3_to_s00_couplers_RREADY;
  wire [1:0]axi_interconnect_3_to_s00_couplers_RRESP;
  wire [3:0]axi_interconnect_3_to_s00_couplers_RUSER;
  wire axi_interconnect_3_to_s00_couplers_RVALID;
  wire [31:0]axi_interconnect_3_to_s00_couplers_WDATA;
  wire axi_interconnect_3_to_s00_couplers_WLAST;
  wire axi_interconnect_3_to_s00_couplers_WREADY;
  wire [3:0]axi_interconnect_3_to_s00_couplers_WSTRB;
  wire [3:0]axi_interconnect_3_to_s00_couplers_WUSER;
  wire axi_interconnect_3_to_s00_couplers_WVALID;
  wire [31:0]s00_couplers_to_axi_interconnect_3_ARADDR;
  wire [1:0]s00_couplers_to_axi_interconnect_3_ARBURST;
  wire [3:0]s00_couplers_to_axi_interconnect_3_ARCACHE;
  wire [5:0]s00_couplers_to_axi_interconnect_3_ARID;
  wire [7:0]s00_couplers_to_axi_interconnect_3_ARLEN;
  wire [0:0]s00_couplers_to_axi_interconnect_3_ARLOCK;
  wire [2:0]s00_couplers_to_axi_interconnect_3_ARPROT;
  wire [3:0]s00_couplers_to_axi_interconnect_3_ARQOS;
  wire s00_couplers_to_axi_interconnect_3_ARREADY;
  wire [3:0]s00_couplers_to_axi_interconnect_3_ARREGION;
  wire [2:0]s00_couplers_to_axi_interconnect_3_ARSIZE;
  wire [3:0]s00_couplers_to_axi_interconnect_3_ARUSER;
  wire s00_couplers_to_axi_interconnect_3_ARVALID;
  wire [31:0]s00_couplers_to_axi_interconnect_3_AWADDR;
  wire [1:0]s00_couplers_to_axi_interconnect_3_AWBURST;
  wire [3:0]s00_couplers_to_axi_interconnect_3_AWCACHE;
  wire [5:0]s00_couplers_to_axi_interconnect_3_AWID;
  wire [7:0]s00_couplers_to_axi_interconnect_3_AWLEN;
  wire [0:0]s00_couplers_to_axi_interconnect_3_AWLOCK;
  wire [2:0]s00_couplers_to_axi_interconnect_3_AWPROT;
  wire [3:0]s00_couplers_to_axi_interconnect_3_AWQOS;
  wire s00_couplers_to_axi_interconnect_3_AWREADY;
  wire [3:0]s00_couplers_to_axi_interconnect_3_AWREGION;
  wire [2:0]s00_couplers_to_axi_interconnect_3_AWSIZE;
  wire [3:0]s00_couplers_to_axi_interconnect_3_AWUSER;
  wire s00_couplers_to_axi_interconnect_3_AWVALID;
  wire [5:0]s00_couplers_to_axi_interconnect_3_BID;
  wire s00_couplers_to_axi_interconnect_3_BREADY;
  wire [1:0]s00_couplers_to_axi_interconnect_3_BRESP;
  wire [3:0]s00_couplers_to_axi_interconnect_3_BUSER;
  wire s00_couplers_to_axi_interconnect_3_BVALID;
  wire [31:0]s00_couplers_to_axi_interconnect_3_RDATA;
  wire [5:0]s00_couplers_to_axi_interconnect_3_RID;
  wire s00_couplers_to_axi_interconnect_3_RLAST;
  wire s00_couplers_to_axi_interconnect_3_RREADY;
  wire [1:0]s00_couplers_to_axi_interconnect_3_RRESP;
  wire [3:0]s00_couplers_to_axi_interconnect_3_RUSER;
  wire s00_couplers_to_axi_interconnect_3_RVALID;
  wire [31:0]s00_couplers_to_axi_interconnect_3_WDATA;
  wire s00_couplers_to_axi_interconnect_3_WLAST;
  wire s00_couplers_to_axi_interconnect_3_WREADY;
  wire [3:0]s00_couplers_to_axi_interconnect_3_WSTRB;
  wire [3:0]s00_couplers_to_axi_interconnect_3_WUSER;
  wire s00_couplers_to_axi_interconnect_3_WVALID;

  assign M00_AXI_araddr[31:0] = s00_couplers_to_axi_interconnect_3_ARADDR;
  assign M00_AXI_arburst[1:0] = s00_couplers_to_axi_interconnect_3_ARBURST;
  assign M00_AXI_arcache[3:0] = s00_couplers_to_axi_interconnect_3_ARCACHE;
  assign M00_AXI_arid[5:0] = s00_couplers_to_axi_interconnect_3_ARID;
  assign M00_AXI_arlen[7:0] = s00_couplers_to_axi_interconnect_3_ARLEN;
  assign M00_AXI_arlock[0] = s00_couplers_to_axi_interconnect_3_ARLOCK;
  assign M00_AXI_arprot[2:0] = s00_couplers_to_axi_interconnect_3_ARPROT;
  assign M00_AXI_arqos[3:0] = s00_couplers_to_axi_interconnect_3_ARQOS;
  assign M00_AXI_arregion[3:0] = s00_couplers_to_axi_interconnect_3_ARREGION;
  assign M00_AXI_arsize[2:0] = s00_couplers_to_axi_interconnect_3_ARSIZE;
  assign M00_AXI_aruser[3:0] = s00_couplers_to_axi_interconnect_3_ARUSER;
  assign M00_AXI_arvalid = s00_couplers_to_axi_interconnect_3_ARVALID;
  assign M00_AXI_awaddr[31:0] = s00_couplers_to_axi_interconnect_3_AWADDR;
  assign M00_AXI_awburst[1:0] = s00_couplers_to_axi_interconnect_3_AWBURST;
  assign M00_AXI_awcache[3:0] = s00_couplers_to_axi_interconnect_3_AWCACHE;
  assign M00_AXI_awid[5:0] = s00_couplers_to_axi_interconnect_3_AWID;
  assign M00_AXI_awlen[7:0] = s00_couplers_to_axi_interconnect_3_AWLEN;
  assign M00_AXI_awlock[0] = s00_couplers_to_axi_interconnect_3_AWLOCK;
  assign M00_AXI_awprot[2:0] = s00_couplers_to_axi_interconnect_3_AWPROT;
  assign M00_AXI_awqos[3:0] = s00_couplers_to_axi_interconnect_3_AWQOS;
  assign M00_AXI_awregion[3:0] = s00_couplers_to_axi_interconnect_3_AWREGION;
  assign M00_AXI_awsize[2:0] = s00_couplers_to_axi_interconnect_3_AWSIZE;
  assign M00_AXI_awuser[3:0] = s00_couplers_to_axi_interconnect_3_AWUSER;
  assign M00_AXI_awvalid = s00_couplers_to_axi_interconnect_3_AWVALID;
  assign M00_AXI_bready = s00_couplers_to_axi_interconnect_3_BREADY;
  assign M00_AXI_rready = s00_couplers_to_axi_interconnect_3_RREADY;
  assign M00_AXI_wdata[31:0] = s00_couplers_to_axi_interconnect_3_WDATA;
  assign M00_AXI_wlast = s00_couplers_to_axi_interconnect_3_WLAST;
  assign M00_AXI_wstrb[3:0] = s00_couplers_to_axi_interconnect_3_WSTRB;
  assign M00_AXI_wuser[3:0] = s00_couplers_to_axi_interconnect_3_WUSER;
  assign M00_AXI_wvalid = s00_couplers_to_axi_interconnect_3_WVALID;
  assign S00_ACLK_1 = S00_ACLK;
  assign S00_ARESETN_1 = S00_ARESETN;
  assign S00_AXI_arready = axi_interconnect_3_to_s00_couplers_ARREADY;
  assign S00_AXI_awready = axi_interconnect_3_to_s00_couplers_AWREADY;
  assign S00_AXI_bid[5:0] = axi_interconnect_3_to_s00_couplers_BID;
  assign S00_AXI_bresp[1:0] = axi_interconnect_3_to_s00_couplers_BRESP;
  assign S00_AXI_buser[3:0] = axi_interconnect_3_to_s00_couplers_BUSER;
  assign S00_AXI_bvalid = axi_interconnect_3_to_s00_couplers_BVALID;
  assign S00_AXI_rdata[31:0] = axi_interconnect_3_to_s00_couplers_RDATA;
  assign S00_AXI_rid[5:0] = axi_interconnect_3_to_s00_couplers_RID;
  assign S00_AXI_rlast = axi_interconnect_3_to_s00_couplers_RLAST;
  assign S00_AXI_rresp[1:0] = axi_interconnect_3_to_s00_couplers_RRESP;
  assign S00_AXI_ruser[3:0] = axi_interconnect_3_to_s00_couplers_RUSER;
  assign S00_AXI_rvalid = axi_interconnect_3_to_s00_couplers_RVALID;
  assign S00_AXI_wready = axi_interconnect_3_to_s00_couplers_WREADY;
  assign axi_interconnect_3_ACLK_net = M00_ACLK;
  assign axi_interconnect_3_ARESETN_net = M00_ARESETN;
  assign axi_interconnect_3_to_s00_couplers_ARADDR = S00_AXI_araddr[31:0];
  assign axi_interconnect_3_to_s00_couplers_ARBURST = S00_AXI_arburst[1:0];
  assign axi_interconnect_3_to_s00_couplers_ARCACHE = S00_AXI_arcache[3:0];
  assign axi_interconnect_3_to_s00_couplers_ARID = S00_AXI_arid[5:0];
  assign axi_interconnect_3_to_s00_couplers_ARLEN = S00_AXI_arlen[7:0];
  assign axi_interconnect_3_to_s00_couplers_ARLOCK = S00_AXI_arlock[0];
  assign axi_interconnect_3_to_s00_couplers_ARPROT = S00_AXI_arprot[2:0];
  assign axi_interconnect_3_to_s00_couplers_ARQOS = S00_AXI_arqos[3:0];
  assign axi_interconnect_3_to_s00_couplers_ARREGION = S00_AXI_arregion[3:0];
  assign axi_interconnect_3_to_s00_couplers_ARSIZE = S00_AXI_arsize[2:0];
  assign axi_interconnect_3_to_s00_couplers_ARUSER = S00_AXI_aruser[3:0];
  assign axi_interconnect_3_to_s00_couplers_ARVALID = S00_AXI_arvalid;
  assign axi_interconnect_3_to_s00_couplers_AWADDR = S00_AXI_awaddr[31:0];
  assign axi_interconnect_3_to_s00_couplers_AWBURST = S00_AXI_awburst[1:0];
  assign axi_interconnect_3_to_s00_couplers_AWCACHE = S00_AXI_awcache[3:0];
  assign axi_interconnect_3_to_s00_couplers_AWID = S00_AXI_awid[5:0];
  assign axi_interconnect_3_to_s00_couplers_AWLEN = S00_AXI_awlen[7:0];
  assign axi_interconnect_3_to_s00_couplers_AWLOCK = S00_AXI_awlock[0];
  assign axi_interconnect_3_to_s00_couplers_AWPROT = S00_AXI_awprot[2:0];
  assign axi_interconnect_3_to_s00_couplers_AWQOS = S00_AXI_awqos[3:0];
  assign axi_interconnect_3_to_s00_couplers_AWREGION = S00_AXI_awregion[3:0];
  assign axi_interconnect_3_to_s00_couplers_AWSIZE = S00_AXI_awsize[2:0];
  assign axi_interconnect_3_to_s00_couplers_AWUSER = S00_AXI_awuser[3:0];
  assign axi_interconnect_3_to_s00_couplers_AWVALID = S00_AXI_awvalid;
  assign axi_interconnect_3_to_s00_couplers_BREADY = S00_AXI_bready;
  assign axi_interconnect_3_to_s00_couplers_RREADY = S00_AXI_rready;
  assign axi_interconnect_3_to_s00_couplers_WDATA = S00_AXI_wdata[31:0];
  assign axi_interconnect_3_to_s00_couplers_WLAST = S00_AXI_wlast;
  assign axi_interconnect_3_to_s00_couplers_WSTRB = S00_AXI_wstrb[3:0];
  assign axi_interconnect_3_to_s00_couplers_WUSER = S00_AXI_wuser[3:0];
  assign axi_interconnect_3_to_s00_couplers_WVALID = S00_AXI_wvalid;
  assign s00_couplers_to_axi_interconnect_3_ARREADY = M00_AXI_arready;
  assign s00_couplers_to_axi_interconnect_3_AWREADY = M00_AXI_awready;
  assign s00_couplers_to_axi_interconnect_3_BID = M00_AXI_bid[5:0];
  assign s00_couplers_to_axi_interconnect_3_BRESP = M00_AXI_bresp[1:0];
  assign s00_couplers_to_axi_interconnect_3_BUSER = M00_AXI_buser[3:0];
  assign s00_couplers_to_axi_interconnect_3_BVALID = M00_AXI_bvalid;
  assign s00_couplers_to_axi_interconnect_3_RDATA = M00_AXI_rdata[31:0];
  assign s00_couplers_to_axi_interconnect_3_RID = M00_AXI_rid[5:0];
  assign s00_couplers_to_axi_interconnect_3_RLAST = M00_AXI_rlast;
  assign s00_couplers_to_axi_interconnect_3_RRESP = M00_AXI_rresp[1:0];
  assign s00_couplers_to_axi_interconnect_3_RUSER = M00_AXI_ruser[3:0];
  assign s00_couplers_to_axi_interconnect_3_RVALID = M00_AXI_rvalid;
  assign s00_couplers_to_axi_interconnect_3_WREADY = M00_AXI_wready;
  s00_couplers_imp_1TQTEWV s00_couplers
       (.M_ACLK(axi_interconnect_3_ACLK_net),
        .M_ARESETN(axi_interconnect_3_ARESETN_net),
        .M_AXI_araddr(s00_couplers_to_axi_interconnect_3_ARADDR),
        .M_AXI_arburst(s00_couplers_to_axi_interconnect_3_ARBURST),
        .M_AXI_arcache(s00_couplers_to_axi_interconnect_3_ARCACHE),
        .M_AXI_arid(s00_couplers_to_axi_interconnect_3_ARID),
        .M_AXI_arlen(s00_couplers_to_axi_interconnect_3_ARLEN),
        .M_AXI_arlock(s00_couplers_to_axi_interconnect_3_ARLOCK),
        .M_AXI_arprot(s00_couplers_to_axi_interconnect_3_ARPROT),
        .M_AXI_arqos(s00_couplers_to_axi_interconnect_3_ARQOS),
        .M_AXI_arready(s00_couplers_to_axi_interconnect_3_ARREADY),
        .M_AXI_arregion(s00_couplers_to_axi_interconnect_3_ARREGION),
        .M_AXI_arsize(s00_couplers_to_axi_interconnect_3_ARSIZE),
        .M_AXI_aruser(s00_couplers_to_axi_interconnect_3_ARUSER),
        .M_AXI_arvalid(s00_couplers_to_axi_interconnect_3_ARVALID),
        .M_AXI_awaddr(s00_couplers_to_axi_interconnect_3_AWADDR),
        .M_AXI_awburst(s00_couplers_to_axi_interconnect_3_AWBURST),
        .M_AXI_awcache(s00_couplers_to_axi_interconnect_3_AWCACHE),
        .M_AXI_awid(s00_couplers_to_axi_interconnect_3_AWID),
        .M_AXI_awlen(s00_couplers_to_axi_interconnect_3_AWLEN),
        .M_AXI_awlock(s00_couplers_to_axi_interconnect_3_AWLOCK),
        .M_AXI_awprot(s00_couplers_to_axi_interconnect_3_AWPROT),
        .M_AXI_awqos(s00_couplers_to_axi_interconnect_3_AWQOS),
        .M_AXI_awready(s00_couplers_to_axi_interconnect_3_AWREADY),
        .M_AXI_awregion(s00_couplers_to_axi_interconnect_3_AWREGION),
        .M_AXI_awsize(s00_couplers_to_axi_interconnect_3_AWSIZE),
        .M_AXI_awuser(s00_couplers_to_axi_interconnect_3_AWUSER),
        .M_AXI_awvalid(s00_couplers_to_axi_interconnect_3_AWVALID),
        .M_AXI_bid(s00_couplers_to_axi_interconnect_3_BID),
        .M_AXI_bready(s00_couplers_to_axi_interconnect_3_BREADY),
        .M_AXI_bresp(s00_couplers_to_axi_interconnect_3_BRESP),
        .M_AXI_buser(s00_couplers_to_axi_interconnect_3_BUSER),
        .M_AXI_bvalid(s00_couplers_to_axi_interconnect_3_BVALID),
        .M_AXI_rdata(s00_couplers_to_axi_interconnect_3_RDATA),
        .M_AXI_rid(s00_couplers_to_axi_interconnect_3_RID),
        .M_AXI_rlast(s00_couplers_to_axi_interconnect_3_RLAST),
        .M_AXI_rready(s00_couplers_to_axi_interconnect_3_RREADY),
        .M_AXI_rresp(s00_couplers_to_axi_interconnect_3_RRESP),
        .M_AXI_ruser(s00_couplers_to_axi_interconnect_3_RUSER),
        .M_AXI_rvalid(s00_couplers_to_axi_interconnect_3_RVALID),
        .M_AXI_wdata(s00_couplers_to_axi_interconnect_3_WDATA),
        .M_AXI_wlast(s00_couplers_to_axi_interconnect_3_WLAST),
        .M_AXI_wready(s00_couplers_to_axi_interconnect_3_WREADY),
        .M_AXI_wstrb(s00_couplers_to_axi_interconnect_3_WSTRB),
        .M_AXI_wuser(s00_couplers_to_axi_interconnect_3_WUSER),
        .M_AXI_wvalid(s00_couplers_to_axi_interconnect_3_WVALID),
        .S_ACLK(S00_ACLK_1),
        .S_ARESETN(S00_ARESETN_1),
        .S_AXI_araddr(axi_interconnect_3_to_s00_couplers_ARADDR),
        .S_AXI_arburst(axi_interconnect_3_to_s00_couplers_ARBURST),
        .S_AXI_arcache(axi_interconnect_3_to_s00_couplers_ARCACHE),
        .S_AXI_arid(axi_interconnect_3_to_s00_couplers_ARID),
        .S_AXI_arlen(axi_interconnect_3_to_s00_couplers_ARLEN),
        .S_AXI_arlock(axi_interconnect_3_to_s00_couplers_ARLOCK),
        .S_AXI_arprot(axi_interconnect_3_to_s00_couplers_ARPROT),
        .S_AXI_arqos(axi_interconnect_3_to_s00_couplers_ARQOS),
        .S_AXI_arready(axi_interconnect_3_to_s00_couplers_ARREADY),
        .S_AXI_arregion(axi_interconnect_3_to_s00_couplers_ARREGION),
        .S_AXI_arsize(axi_interconnect_3_to_s00_couplers_ARSIZE),
        .S_AXI_aruser(axi_interconnect_3_to_s00_couplers_ARUSER),
        .S_AXI_arvalid(axi_interconnect_3_to_s00_couplers_ARVALID),
        .S_AXI_awaddr(axi_interconnect_3_to_s00_couplers_AWADDR),
        .S_AXI_awburst(axi_interconnect_3_to_s00_couplers_AWBURST),
        .S_AXI_awcache(axi_interconnect_3_to_s00_couplers_AWCACHE),
        .S_AXI_awid(axi_interconnect_3_to_s00_couplers_AWID),
        .S_AXI_awlen(axi_interconnect_3_to_s00_couplers_AWLEN),
        .S_AXI_awlock(axi_interconnect_3_to_s00_couplers_AWLOCK),
        .S_AXI_awprot(axi_interconnect_3_to_s00_couplers_AWPROT),
        .S_AXI_awqos(axi_interconnect_3_to_s00_couplers_AWQOS),
        .S_AXI_awready(axi_interconnect_3_to_s00_couplers_AWREADY),
        .S_AXI_awregion(axi_interconnect_3_to_s00_couplers_AWREGION),
        .S_AXI_awsize(axi_interconnect_3_to_s00_couplers_AWSIZE),
        .S_AXI_awuser(axi_interconnect_3_to_s00_couplers_AWUSER),
        .S_AXI_awvalid(axi_interconnect_3_to_s00_couplers_AWVALID),
        .S_AXI_bid(axi_interconnect_3_to_s00_couplers_BID),
        .S_AXI_bready(axi_interconnect_3_to_s00_couplers_BREADY),
        .S_AXI_bresp(axi_interconnect_3_to_s00_couplers_BRESP),
        .S_AXI_buser(axi_interconnect_3_to_s00_couplers_BUSER),
        .S_AXI_bvalid(axi_interconnect_3_to_s00_couplers_BVALID),
        .S_AXI_rdata(axi_interconnect_3_to_s00_couplers_RDATA),
        .S_AXI_rid(axi_interconnect_3_to_s00_couplers_RID),
        .S_AXI_rlast(axi_interconnect_3_to_s00_couplers_RLAST),
        .S_AXI_rready(axi_interconnect_3_to_s00_couplers_RREADY),
        .S_AXI_rresp(axi_interconnect_3_to_s00_couplers_RRESP),
        .S_AXI_ruser(axi_interconnect_3_to_s00_couplers_RUSER),
        .S_AXI_rvalid(axi_interconnect_3_to_s00_couplers_RVALID),
        .S_AXI_wdata(axi_interconnect_3_to_s00_couplers_WDATA),
        .S_AXI_wlast(axi_interconnect_3_to_s00_couplers_WLAST),
        .S_AXI_wready(axi_interconnect_3_to_s00_couplers_WREADY),
        .S_AXI_wstrb(axi_interconnect_3_to_s00_couplers_WSTRB),
        .S_AXI_wuser(axi_interconnect_3_to_s00_couplers_WUSER),
        .S_AXI_wvalid(axi_interconnect_3_to_s00_couplers_WVALID));
endmodule

module m00_couplers_imp_1ICGZB5
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arburst,
    M_AXI_arcache,
    M_AXI_arid,
    M_AXI_arlen,
    M_AXI_arlock,
    M_AXI_arprot,
    M_AXI_arqos,
    M_AXI_arready,
    M_AXI_arregion,
    M_AXI_arsize,
    M_AXI_aruser,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awburst,
    M_AXI_awcache,
    M_AXI_awid,
    M_AXI_awlen,
    M_AXI_awlock,
    M_AXI_awprot,
    M_AXI_awqos,
    M_AXI_awready,
    M_AXI_awregion,
    M_AXI_awsize,
    M_AXI_awuser,
    M_AXI_awvalid,
    M_AXI_bid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_buser,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rid,
    M_AXI_rlast,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_ruser,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wlast,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wuser,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arid,
    S_AXI_arlen,
    S_AXI_arlock,
    S_AXI_arprot,
    S_AXI_arqos,
    S_AXI_arready,
    S_AXI_arregion,
    S_AXI_arsize,
    S_AXI_aruser,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awid,
    S_AXI_awlen,
    S_AXI_awlock,
    S_AXI_awprot,
    S_AXI_awqos,
    S_AXI_awready,
    S_AXI_awregion,
    S_AXI_awsize,
    S_AXI_awuser,
    S_AXI_awvalid,
    S_AXI_bid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_buser,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rid,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_ruser,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wuser,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [1:0]M_AXI_arburst;
  output [3:0]M_AXI_arcache;
  output [5:0]M_AXI_arid;
  output [7:0]M_AXI_arlen;
  output [0:0]M_AXI_arlock;
  output [2:0]M_AXI_arprot;
  output [3:0]M_AXI_arqos;
  input [0:0]M_AXI_arready;
  output [3:0]M_AXI_arregion;
  output [2:0]M_AXI_arsize;
  output [3:0]M_AXI_aruser;
  output [0:0]M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [1:0]M_AXI_awburst;
  output [3:0]M_AXI_awcache;
  output [5:0]M_AXI_awid;
  output [7:0]M_AXI_awlen;
  output [0:0]M_AXI_awlock;
  output [2:0]M_AXI_awprot;
  output [3:0]M_AXI_awqos;
  input [0:0]M_AXI_awready;
  output [3:0]M_AXI_awregion;
  output [2:0]M_AXI_awsize;
  output [3:0]M_AXI_awuser;
  output [0:0]M_AXI_awvalid;
  input [5:0]M_AXI_bid;
  output [0:0]M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input [3:0]M_AXI_buser;
  input [0:0]M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  input [5:0]M_AXI_rid;
  input [0:0]M_AXI_rlast;
  output [0:0]M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input [3:0]M_AXI_ruser;
  input [0:0]M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  output [0:0]M_AXI_wlast;
  input [0:0]M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output [3:0]M_AXI_wuser;
  output [0:0]M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [5:0]S_AXI_arid;
  input [7:0]S_AXI_arlen;
  input [0:0]S_AXI_arlock;
  input [2:0]S_AXI_arprot;
  input [3:0]S_AXI_arqos;
  output [0:0]S_AXI_arready;
  input [3:0]S_AXI_arregion;
  input [2:0]S_AXI_arsize;
  input [3:0]S_AXI_aruser;
  input [0:0]S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [5:0]S_AXI_awid;
  input [7:0]S_AXI_awlen;
  input [0:0]S_AXI_awlock;
  input [2:0]S_AXI_awprot;
  input [3:0]S_AXI_awqos;
  output [0:0]S_AXI_awready;
  input [3:0]S_AXI_awregion;
  input [2:0]S_AXI_awsize;
  input [3:0]S_AXI_awuser;
  input [0:0]S_AXI_awvalid;
  output [5:0]S_AXI_bid;
  input [0:0]S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output [3:0]S_AXI_buser;
  output [0:0]S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  output [5:0]S_AXI_rid;
  output [0:0]S_AXI_rlast;
  input [0:0]S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output [3:0]S_AXI_ruser;
  output [0:0]S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  input [0:0]S_AXI_wlast;
  output [0:0]S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input [3:0]S_AXI_wuser;
  input [0:0]S_AXI_wvalid;

  wire [31:0]m00_couplers_to_m00_couplers_ARADDR;
  wire [1:0]m00_couplers_to_m00_couplers_ARBURST;
  wire [3:0]m00_couplers_to_m00_couplers_ARCACHE;
  wire [5:0]m00_couplers_to_m00_couplers_ARID;
  wire [7:0]m00_couplers_to_m00_couplers_ARLEN;
  wire [0:0]m00_couplers_to_m00_couplers_ARLOCK;
  wire [2:0]m00_couplers_to_m00_couplers_ARPROT;
  wire [3:0]m00_couplers_to_m00_couplers_ARQOS;
  wire [0:0]m00_couplers_to_m00_couplers_ARREADY;
  wire [3:0]m00_couplers_to_m00_couplers_ARREGION;
  wire [2:0]m00_couplers_to_m00_couplers_ARSIZE;
  wire [3:0]m00_couplers_to_m00_couplers_ARUSER;
  wire [0:0]m00_couplers_to_m00_couplers_ARVALID;
  wire [31:0]m00_couplers_to_m00_couplers_AWADDR;
  wire [1:0]m00_couplers_to_m00_couplers_AWBURST;
  wire [3:0]m00_couplers_to_m00_couplers_AWCACHE;
  wire [5:0]m00_couplers_to_m00_couplers_AWID;
  wire [7:0]m00_couplers_to_m00_couplers_AWLEN;
  wire [0:0]m00_couplers_to_m00_couplers_AWLOCK;
  wire [2:0]m00_couplers_to_m00_couplers_AWPROT;
  wire [3:0]m00_couplers_to_m00_couplers_AWQOS;
  wire [0:0]m00_couplers_to_m00_couplers_AWREADY;
  wire [3:0]m00_couplers_to_m00_couplers_AWREGION;
  wire [2:0]m00_couplers_to_m00_couplers_AWSIZE;
  wire [3:0]m00_couplers_to_m00_couplers_AWUSER;
  wire [0:0]m00_couplers_to_m00_couplers_AWVALID;
  wire [5:0]m00_couplers_to_m00_couplers_BID;
  wire [0:0]m00_couplers_to_m00_couplers_BREADY;
  wire [1:0]m00_couplers_to_m00_couplers_BRESP;
  wire [3:0]m00_couplers_to_m00_couplers_BUSER;
  wire [0:0]m00_couplers_to_m00_couplers_BVALID;
  wire [31:0]m00_couplers_to_m00_couplers_RDATA;
  wire [5:0]m00_couplers_to_m00_couplers_RID;
  wire [0:0]m00_couplers_to_m00_couplers_RLAST;
  wire [0:0]m00_couplers_to_m00_couplers_RREADY;
  wire [1:0]m00_couplers_to_m00_couplers_RRESP;
  wire [3:0]m00_couplers_to_m00_couplers_RUSER;
  wire [0:0]m00_couplers_to_m00_couplers_RVALID;
  wire [31:0]m00_couplers_to_m00_couplers_WDATA;
  wire [0:0]m00_couplers_to_m00_couplers_WLAST;
  wire [0:0]m00_couplers_to_m00_couplers_WREADY;
  wire [3:0]m00_couplers_to_m00_couplers_WSTRB;
  wire [3:0]m00_couplers_to_m00_couplers_WUSER;
  wire [0:0]m00_couplers_to_m00_couplers_WVALID;

  assign M_AXI_araddr[31:0] = m00_couplers_to_m00_couplers_ARADDR;
  assign M_AXI_arburst[1:0] = m00_couplers_to_m00_couplers_ARBURST;
  assign M_AXI_arcache[3:0] = m00_couplers_to_m00_couplers_ARCACHE;
  assign M_AXI_arid[5:0] = m00_couplers_to_m00_couplers_ARID;
  assign M_AXI_arlen[7:0] = m00_couplers_to_m00_couplers_ARLEN;
  assign M_AXI_arlock[0] = m00_couplers_to_m00_couplers_ARLOCK;
  assign M_AXI_arprot[2:0] = m00_couplers_to_m00_couplers_ARPROT;
  assign M_AXI_arqos[3:0] = m00_couplers_to_m00_couplers_ARQOS;
  assign M_AXI_arregion[3:0] = m00_couplers_to_m00_couplers_ARREGION;
  assign M_AXI_arsize[2:0] = m00_couplers_to_m00_couplers_ARSIZE;
  assign M_AXI_aruser[3:0] = m00_couplers_to_m00_couplers_ARUSER;
  assign M_AXI_arvalid[0] = m00_couplers_to_m00_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = m00_couplers_to_m00_couplers_AWADDR;
  assign M_AXI_awburst[1:0] = m00_couplers_to_m00_couplers_AWBURST;
  assign M_AXI_awcache[3:0] = m00_couplers_to_m00_couplers_AWCACHE;
  assign M_AXI_awid[5:0] = m00_couplers_to_m00_couplers_AWID;
  assign M_AXI_awlen[7:0] = m00_couplers_to_m00_couplers_AWLEN;
  assign M_AXI_awlock[0] = m00_couplers_to_m00_couplers_AWLOCK;
  assign M_AXI_awprot[2:0] = m00_couplers_to_m00_couplers_AWPROT;
  assign M_AXI_awqos[3:0] = m00_couplers_to_m00_couplers_AWQOS;
  assign M_AXI_awregion[3:0] = m00_couplers_to_m00_couplers_AWREGION;
  assign M_AXI_awsize[2:0] = m00_couplers_to_m00_couplers_AWSIZE;
  assign M_AXI_awuser[3:0] = m00_couplers_to_m00_couplers_AWUSER;
  assign M_AXI_awvalid[0] = m00_couplers_to_m00_couplers_AWVALID;
  assign M_AXI_bready[0] = m00_couplers_to_m00_couplers_BREADY;
  assign M_AXI_rready[0] = m00_couplers_to_m00_couplers_RREADY;
  assign M_AXI_wdata[31:0] = m00_couplers_to_m00_couplers_WDATA;
  assign M_AXI_wlast[0] = m00_couplers_to_m00_couplers_WLAST;
  assign M_AXI_wstrb[3:0] = m00_couplers_to_m00_couplers_WSTRB;
  assign M_AXI_wuser[3:0] = m00_couplers_to_m00_couplers_WUSER;
  assign M_AXI_wvalid[0] = m00_couplers_to_m00_couplers_WVALID;
  assign S_AXI_arready[0] = m00_couplers_to_m00_couplers_ARREADY;
  assign S_AXI_awready[0] = m00_couplers_to_m00_couplers_AWREADY;
  assign S_AXI_bid[5:0] = m00_couplers_to_m00_couplers_BID;
  assign S_AXI_bresp[1:0] = m00_couplers_to_m00_couplers_BRESP;
  assign S_AXI_buser[3:0] = m00_couplers_to_m00_couplers_BUSER;
  assign S_AXI_bvalid[0] = m00_couplers_to_m00_couplers_BVALID;
  assign S_AXI_rdata[31:0] = m00_couplers_to_m00_couplers_RDATA;
  assign S_AXI_rid[5:0] = m00_couplers_to_m00_couplers_RID;
  assign S_AXI_rlast[0] = m00_couplers_to_m00_couplers_RLAST;
  assign S_AXI_rresp[1:0] = m00_couplers_to_m00_couplers_RRESP;
  assign S_AXI_ruser[3:0] = m00_couplers_to_m00_couplers_RUSER;
  assign S_AXI_rvalid[0] = m00_couplers_to_m00_couplers_RVALID;
  assign S_AXI_wready[0] = m00_couplers_to_m00_couplers_WREADY;
  assign m00_couplers_to_m00_couplers_ARADDR = S_AXI_araddr[31:0];
  assign m00_couplers_to_m00_couplers_ARBURST = S_AXI_arburst[1:0];
  assign m00_couplers_to_m00_couplers_ARCACHE = S_AXI_arcache[3:0];
  assign m00_couplers_to_m00_couplers_ARID = S_AXI_arid[5:0];
  assign m00_couplers_to_m00_couplers_ARLEN = S_AXI_arlen[7:0];
  assign m00_couplers_to_m00_couplers_ARLOCK = S_AXI_arlock[0];
  assign m00_couplers_to_m00_couplers_ARPROT = S_AXI_arprot[2:0];
  assign m00_couplers_to_m00_couplers_ARQOS = S_AXI_arqos[3:0];
  assign m00_couplers_to_m00_couplers_ARREADY = M_AXI_arready[0];
  assign m00_couplers_to_m00_couplers_ARREGION = S_AXI_arregion[3:0];
  assign m00_couplers_to_m00_couplers_ARSIZE = S_AXI_arsize[2:0];
  assign m00_couplers_to_m00_couplers_ARUSER = S_AXI_aruser[3:0];
  assign m00_couplers_to_m00_couplers_ARVALID = S_AXI_arvalid[0];
  assign m00_couplers_to_m00_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign m00_couplers_to_m00_couplers_AWBURST = S_AXI_awburst[1:0];
  assign m00_couplers_to_m00_couplers_AWCACHE = S_AXI_awcache[3:0];
  assign m00_couplers_to_m00_couplers_AWID = S_AXI_awid[5:0];
  assign m00_couplers_to_m00_couplers_AWLEN = S_AXI_awlen[7:0];
  assign m00_couplers_to_m00_couplers_AWLOCK = S_AXI_awlock[0];
  assign m00_couplers_to_m00_couplers_AWPROT = S_AXI_awprot[2:0];
  assign m00_couplers_to_m00_couplers_AWQOS = S_AXI_awqos[3:0];
  assign m00_couplers_to_m00_couplers_AWREADY = M_AXI_awready[0];
  assign m00_couplers_to_m00_couplers_AWREGION = S_AXI_awregion[3:0];
  assign m00_couplers_to_m00_couplers_AWSIZE = S_AXI_awsize[2:0];
  assign m00_couplers_to_m00_couplers_AWUSER = S_AXI_awuser[3:0];
  assign m00_couplers_to_m00_couplers_AWVALID = S_AXI_awvalid[0];
  assign m00_couplers_to_m00_couplers_BID = M_AXI_bid[5:0];
  assign m00_couplers_to_m00_couplers_BREADY = S_AXI_bready[0];
  assign m00_couplers_to_m00_couplers_BRESP = M_AXI_bresp[1:0];
  assign m00_couplers_to_m00_couplers_BUSER = M_AXI_buser[3:0];
  assign m00_couplers_to_m00_couplers_BVALID = M_AXI_bvalid[0];
  assign m00_couplers_to_m00_couplers_RDATA = M_AXI_rdata[31:0];
  assign m00_couplers_to_m00_couplers_RID = M_AXI_rid[5:0];
  assign m00_couplers_to_m00_couplers_RLAST = M_AXI_rlast[0];
  assign m00_couplers_to_m00_couplers_RREADY = S_AXI_rready[0];
  assign m00_couplers_to_m00_couplers_RRESP = M_AXI_rresp[1:0];
  assign m00_couplers_to_m00_couplers_RUSER = M_AXI_ruser[3:0];
  assign m00_couplers_to_m00_couplers_RVALID = M_AXI_rvalid[0];
  assign m00_couplers_to_m00_couplers_WDATA = S_AXI_wdata[31:0];
  assign m00_couplers_to_m00_couplers_WLAST = S_AXI_wlast[0];
  assign m00_couplers_to_m00_couplers_WREADY = M_AXI_wready[0];
  assign m00_couplers_to_m00_couplers_WSTRB = S_AXI_wstrb[3:0];
  assign m00_couplers_to_m00_couplers_WUSER = S_AXI_wuser[3:0];
  assign m00_couplers_to_m00_couplers_WVALID = S_AXI_wvalid[0];
endmodule

module m01_couplers_imp_A785EE
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arid,
    S_AXI_arlen,
    S_AXI_arlock,
    S_AXI_arprot,
    S_AXI_arqos,
    S_AXI_arready,
    S_AXI_arregion,
    S_AXI_arsize,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awid,
    S_AXI_awlen,
    S_AXI_awlock,
    S_AXI_awprot,
    S_AXI_awqos,
    S_AXI_awready,
    S_AXI_awregion,
    S_AXI_awsize,
    S_AXI_awvalid,
    S_AXI_bid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rid,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  input M_AXI_arready;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  input M_AXI_awready;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [5:0]S_AXI_arid;
  input [7:0]S_AXI_arlen;
  input [0:0]S_AXI_arlock;
  input [2:0]S_AXI_arprot;
  input [3:0]S_AXI_arqos;
  output S_AXI_arready;
  input [3:0]S_AXI_arregion;
  input [2:0]S_AXI_arsize;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [5:0]S_AXI_awid;
  input [7:0]S_AXI_awlen;
  input [0:0]S_AXI_awlock;
  input [2:0]S_AXI_awprot;
  input [3:0]S_AXI_awqos;
  output S_AXI_awready;
  input [3:0]S_AXI_awregion;
  input [2:0]S_AXI_awsize;
  input S_AXI_awvalid;
  output [5:0]S_AXI_bid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  output [5:0]S_AXI_rid;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  input S_AXI_wlast;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire S_ACLK_1;
  wire S_ARESETN_1;
  wire [31:0]auto_pc_to_m01_couplers_ARADDR;
  wire auto_pc_to_m01_couplers_ARREADY;
  wire auto_pc_to_m01_couplers_ARVALID;
  wire [31:0]auto_pc_to_m01_couplers_AWADDR;
  wire auto_pc_to_m01_couplers_AWREADY;
  wire auto_pc_to_m01_couplers_AWVALID;
  wire auto_pc_to_m01_couplers_BREADY;
  wire [1:0]auto_pc_to_m01_couplers_BRESP;
  wire auto_pc_to_m01_couplers_BVALID;
  wire [31:0]auto_pc_to_m01_couplers_RDATA;
  wire auto_pc_to_m01_couplers_RREADY;
  wire [1:0]auto_pc_to_m01_couplers_RRESP;
  wire auto_pc_to_m01_couplers_RVALID;
  wire [31:0]auto_pc_to_m01_couplers_WDATA;
  wire auto_pc_to_m01_couplers_WREADY;
  wire [3:0]auto_pc_to_m01_couplers_WSTRB;
  wire auto_pc_to_m01_couplers_WVALID;
  wire [31:0]m01_couplers_to_auto_pc_ARADDR;
  wire [1:0]m01_couplers_to_auto_pc_ARBURST;
  wire [3:0]m01_couplers_to_auto_pc_ARCACHE;
  wire [5:0]m01_couplers_to_auto_pc_ARID;
  wire [7:0]m01_couplers_to_auto_pc_ARLEN;
  wire [0:0]m01_couplers_to_auto_pc_ARLOCK;
  wire [2:0]m01_couplers_to_auto_pc_ARPROT;
  wire [3:0]m01_couplers_to_auto_pc_ARQOS;
  wire m01_couplers_to_auto_pc_ARREADY;
  wire [3:0]m01_couplers_to_auto_pc_ARREGION;
  wire [2:0]m01_couplers_to_auto_pc_ARSIZE;
  wire m01_couplers_to_auto_pc_ARVALID;
  wire [31:0]m01_couplers_to_auto_pc_AWADDR;
  wire [1:0]m01_couplers_to_auto_pc_AWBURST;
  wire [3:0]m01_couplers_to_auto_pc_AWCACHE;
  wire [5:0]m01_couplers_to_auto_pc_AWID;
  wire [7:0]m01_couplers_to_auto_pc_AWLEN;
  wire [0:0]m01_couplers_to_auto_pc_AWLOCK;
  wire [2:0]m01_couplers_to_auto_pc_AWPROT;
  wire [3:0]m01_couplers_to_auto_pc_AWQOS;
  wire m01_couplers_to_auto_pc_AWREADY;
  wire [3:0]m01_couplers_to_auto_pc_AWREGION;
  wire [2:0]m01_couplers_to_auto_pc_AWSIZE;
  wire m01_couplers_to_auto_pc_AWVALID;
  wire [5:0]m01_couplers_to_auto_pc_BID;
  wire m01_couplers_to_auto_pc_BREADY;
  wire [1:0]m01_couplers_to_auto_pc_BRESP;
  wire m01_couplers_to_auto_pc_BVALID;
  wire [31:0]m01_couplers_to_auto_pc_RDATA;
  wire [5:0]m01_couplers_to_auto_pc_RID;
  wire m01_couplers_to_auto_pc_RLAST;
  wire m01_couplers_to_auto_pc_RREADY;
  wire [1:0]m01_couplers_to_auto_pc_RRESP;
  wire m01_couplers_to_auto_pc_RVALID;
  wire [31:0]m01_couplers_to_auto_pc_WDATA;
  wire m01_couplers_to_auto_pc_WLAST;
  wire m01_couplers_to_auto_pc_WREADY;
  wire [3:0]m01_couplers_to_auto_pc_WSTRB;
  wire m01_couplers_to_auto_pc_WVALID;

  assign M_AXI_araddr[31:0] = auto_pc_to_m01_couplers_ARADDR;
  assign M_AXI_arvalid = auto_pc_to_m01_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = auto_pc_to_m01_couplers_AWADDR;
  assign M_AXI_awvalid = auto_pc_to_m01_couplers_AWVALID;
  assign M_AXI_bready = auto_pc_to_m01_couplers_BREADY;
  assign M_AXI_rready = auto_pc_to_m01_couplers_RREADY;
  assign M_AXI_wdata[31:0] = auto_pc_to_m01_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = auto_pc_to_m01_couplers_WSTRB;
  assign M_AXI_wvalid = auto_pc_to_m01_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN;
  assign S_AXI_arready = m01_couplers_to_auto_pc_ARREADY;
  assign S_AXI_awready = m01_couplers_to_auto_pc_AWREADY;
  assign S_AXI_bid[5:0] = m01_couplers_to_auto_pc_BID;
  assign S_AXI_bresp[1:0] = m01_couplers_to_auto_pc_BRESP;
  assign S_AXI_bvalid = m01_couplers_to_auto_pc_BVALID;
  assign S_AXI_rdata[31:0] = m01_couplers_to_auto_pc_RDATA;
  assign S_AXI_rid[5:0] = m01_couplers_to_auto_pc_RID;
  assign S_AXI_rlast = m01_couplers_to_auto_pc_RLAST;
  assign S_AXI_rresp[1:0] = m01_couplers_to_auto_pc_RRESP;
  assign S_AXI_rvalid = m01_couplers_to_auto_pc_RVALID;
  assign S_AXI_wready = m01_couplers_to_auto_pc_WREADY;
  assign auto_pc_to_m01_couplers_ARREADY = M_AXI_arready;
  assign auto_pc_to_m01_couplers_AWREADY = M_AXI_awready;
  assign auto_pc_to_m01_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_pc_to_m01_couplers_BVALID = M_AXI_bvalid;
  assign auto_pc_to_m01_couplers_RDATA = M_AXI_rdata[31:0];
  assign auto_pc_to_m01_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_pc_to_m01_couplers_RVALID = M_AXI_rvalid;
  assign auto_pc_to_m01_couplers_WREADY = M_AXI_wready;
  assign m01_couplers_to_auto_pc_ARADDR = S_AXI_araddr[31:0];
  assign m01_couplers_to_auto_pc_ARBURST = S_AXI_arburst[1:0];
  assign m01_couplers_to_auto_pc_ARCACHE = S_AXI_arcache[3:0];
  assign m01_couplers_to_auto_pc_ARID = S_AXI_arid[5:0];
  assign m01_couplers_to_auto_pc_ARLEN = S_AXI_arlen[7:0];
  assign m01_couplers_to_auto_pc_ARLOCK = S_AXI_arlock[0];
  assign m01_couplers_to_auto_pc_ARPROT = S_AXI_arprot[2:0];
  assign m01_couplers_to_auto_pc_ARQOS = S_AXI_arqos[3:0];
  assign m01_couplers_to_auto_pc_ARREGION = S_AXI_arregion[3:0];
  assign m01_couplers_to_auto_pc_ARSIZE = S_AXI_arsize[2:0];
  assign m01_couplers_to_auto_pc_ARVALID = S_AXI_arvalid;
  assign m01_couplers_to_auto_pc_AWADDR = S_AXI_awaddr[31:0];
  assign m01_couplers_to_auto_pc_AWBURST = S_AXI_awburst[1:0];
  assign m01_couplers_to_auto_pc_AWCACHE = S_AXI_awcache[3:0];
  assign m01_couplers_to_auto_pc_AWID = S_AXI_awid[5:0];
  assign m01_couplers_to_auto_pc_AWLEN = S_AXI_awlen[7:0];
  assign m01_couplers_to_auto_pc_AWLOCK = S_AXI_awlock[0];
  assign m01_couplers_to_auto_pc_AWPROT = S_AXI_awprot[2:0];
  assign m01_couplers_to_auto_pc_AWQOS = S_AXI_awqos[3:0];
  assign m01_couplers_to_auto_pc_AWREGION = S_AXI_awregion[3:0];
  assign m01_couplers_to_auto_pc_AWSIZE = S_AXI_awsize[2:0];
  assign m01_couplers_to_auto_pc_AWVALID = S_AXI_awvalid;
  assign m01_couplers_to_auto_pc_BREADY = S_AXI_bready;
  assign m01_couplers_to_auto_pc_RREADY = S_AXI_rready;
  assign m01_couplers_to_auto_pc_WDATA = S_AXI_wdata[31:0];
  assign m01_couplers_to_auto_pc_WLAST = S_AXI_wlast;
  assign m01_couplers_to_auto_pc_WSTRB = S_AXI_wstrb[3:0];
  assign m01_couplers_to_auto_pc_WVALID = S_AXI_wvalid;
  gpn_bundle_block_auto_pc_0 auto_pc
       (.aclk(S_ACLK_1),
        .aresetn(S_ARESETN_1),
        .m_axi_araddr(auto_pc_to_m01_couplers_ARADDR),
        .m_axi_arready(auto_pc_to_m01_couplers_ARREADY),
        .m_axi_arvalid(auto_pc_to_m01_couplers_ARVALID),
        .m_axi_awaddr(auto_pc_to_m01_couplers_AWADDR),
        .m_axi_awready(auto_pc_to_m01_couplers_AWREADY),
        .m_axi_awvalid(auto_pc_to_m01_couplers_AWVALID),
        .m_axi_bready(auto_pc_to_m01_couplers_BREADY),
        .m_axi_bresp(auto_pc_to_m01_couplers_BRESP),
        .m_axi_bvalid(auto_pc_to_m01_couplers_BVALID),
        .m_axi_rdata(auto_pc_to_m01_couplers_RDATA),
        .m_axi_rready(auto_pc_to_m01_couplers_RREADY),
        .m_axi_rresp(auto_pc_to_m01_couplers_RRESP),
        .m_axi_rvalid(auto_pc_to_m01_couplers_RVALID),
        .m_axi_wdata(auto_pc_to_m01_couplers_WDATA),
        .m_axi_wready(auto_pc_to_m01_couplers_WREADY),
        .m_axi_wstrb(auto_pc_to_m01_couplers_WSTRB),
        .m_axi_wvalid(auto_pc_to_m01_couplers_WVALID),
        .s_axi_araddr(m01_couplers_to_auto_pc_ARADDR),
        .s_axi_arburst(m01_couplers_to_auto_pc_ARBURST),
        .s_axi_arcache(m01_couplers_to_auto_pc_ARCACHE),
        .s_axi_arid(m01_couplers_to_auto_pc_ARID),
        .s_axi_arlen(m01_couplers_to_auto_pc_ARLEN),
        .s_axi_arlock(m01_couplers_to_auto_pc_ARLOCK),
        .s_axi_arprot(m01_couplers_to_auto_pc_ARPROT),
        .s_axi_arqos(m01_couplers_to_auto_pc_ARQOS),
        .s_axi_arready(m01_couplers_to_auto_pc_ARREADY),
        .s_axi_arregion(m01_couplers_to_auto_pc_ARREGION),
        .s_axi_arsize(m01_couplers_to_auto_pc_ARSIZE),
        .s_axi_arvalid(m01_couplers_to_auto_pc_ARVALID),
        .s_axi_awaddr(m01_couplers_to_auto_pc_AWADDR),
        .s_axi_awburst(m01_couplers_to_auto_pc_AWBURST),
        .s_axi_awcache(m01_couplers_to_auto_pc_AWCACHE),
        .s_axi_awid(m01_couplers_to_auto_pc_AWID),
        .s_axi_awlen(m01_couplers_to_auto_pc_AWLEN),
        .s_axi_awlock(m01_couplers_to_auto_pc_AWLOCK),
        .s_axi_awprot(m01_couplers_to_auto_pc_AWPROT),
        .s_axi_awqos(m01_couplers_to_auto_pc_AWQOS),
        .s_axi_awready(m01_couplers_to_auto_pc_AWREADY),
        .s_axi_awregion(m01_couplers_to_auto_pc_AWREGION),
        .s_axi_awsize(m01_couplers_to_auto_pc_AWSIZE),
        .s_axi_awvalid(m01_couplers_to_auto_pc_AWVALID),
        .s_axi_bid(m01_couplers_to_auto_pc_BID),
        .s_axi_bready(m01_couplers_to_auto_pc_BREADY),
        .s_axi_bresp(m01_couplers_to_auto_pc_BRESP),
        .s_axi_bvalid(m01_couplers_to_auto_pc_BVALID),
        .s_axi_rdata(m01_couplers_to_auto_pc_RDATA),
        .s_axi_rid(m01_couplers_to_auto_pc_RID),
        .s_axi_rlast(m01_couplers_to_auto_pc_RLAST),
        .s_axi_rready(m01_couplers_to_auto_pc_RREADY),
        .s_axi_rresp(m01_couplers_to_auto_pc_RRESP),
        .s_axi_rvalid(m01_couplers_to_auto_pc_RVALID),
        .s_axi_wdata(m01_couplers_to_auto_pc_WDATA),
        .s_axi_wlast(m01_couplers_to_auto_pc_WLAST),
        .s_axi_wready(m01_couplers_to_auto_pc_WREADY),
        .s_axi_wstrb(m01_couplers_to_auto_pc_WSTRB),
        .s_axi_wvalid(m01_couplers_to_auto_pc_WVALID));
endmodule

module m02_couplers_imp_1NJDMEM
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arid,
    S_AXI_arlen,
    S_AXI_arlock,
    S_AXI_arprot,
    S_AXI_arqos,
    S_AXI_arready,
    S_AXI_arregion,
    S_AXI_arsize,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awid,
    S_AXI_awlen,
    S_AXI_awlock,
    S_AXI_awprot,
    S_AXI_awqos,
    S_AXI_awready,
    S_AXI_awregion,
    S_AXI_awsize,
    S_AXI_awvalid,
    S_AXI_bid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rid,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  input M_AXI_arready;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  input M_AXI_awready;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [5:0]S_AXI_arid;
  input [7:0]S_AXI_arlen;
  input [0:0]S_AXI_arlock;
  input [2:0]S_AXI_arprot;
  input [3:0]S_AXI_arqos;
  output S_AXI_arready;
  input [3:0]S_AXI_arregion;
  input [2:0]S_AXI_arsize;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [5:0]S_AXI_awid;
  input [7:0]S_AXI_awlen;
  input [0:0]S_AXI_awlock;
  input [2:0]S_AXI_awprot;
  input [3:0]S_AXI_awqos;
  output S_AXI_awready;
  input [3:0]S_AXI_awregion;
  input [2:0]S_AXI_awsize;
  input S_AXI_awvalid;
  output [5:0]S_AXI_bid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  output [5:0]S_AXI_rid;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  input S_AXI_wlast;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire S_ACLK_1;
  wire S_ARESETN_1;
  wire [31:0]auto_pc_to_m02_couplers_ARADDR;
  wire auto_pc_to_m02_couplers_ARREADY;
  wire auto_pc_to_m02_couplers_ARVALID;
  wire [31:0]auto_pc_to_m02_couplers_AWADDR;
  wire auto_pc_to_m02_couplers_AWREADY;
  wire auto_pc_to_m02_couplers_AWVALID;
  wire auto_pc_to_m02_couplers_BREADY;
  wire [1:0]auto_pc_to_m02_couplers_BRESP;
  wire auto_pc_to_m02_couplers_BVALID;
  wire [31:0]auto_pc_to_m02_couplers_RDATA;
  wire auto_pc_to_m02_couplers_RREADY;
  wire [1:0]auto_pc_to_m02_couplers_RRESP;
  wire auto_pc_to_m02_couplers_RVALID;
  wire [31:0]auto_pc_to_m02_couplers_WDATA;
  wire auto_pc_to_m02_couplers_WREADY;
  wire [3:0]auto_pc_to_m02_couplers_WSTRB;
  wire auto_pc_to_m02_couplers_WVALID;
  wire [31:0]m02_couplers_to_auto_pc_ARADDR;
  wire [1:0]m02_couplers_to_auto_pc_ARBURST;
  wire [3:0]m02_couplers_to_auto_pc_ARCACHE;
  wire [5:0]m02_couplers_to_auto_pc_ARID;
  wire [7:0]m02_couplers_to_auto_pc_ARLEN;
  wire [0:0]m02_couplers_to_auto_pc_ARLOCK;
  wire [2:0]m02_couplers_to_auto_pc_ARPROT;
  wire [3:0]m02_couplers_to_auto_pc_ARQOS;
  wire m02_couplers_to_auto_pc_ARREADY;
  wire [3:0]m02_couplers_to_auto_pc_ARREGION;
  wire [2:0]m02_couplers_to_auto_pc_ARSIZE;
  wire m02_couplers_to_auto_pc_ARVALID;
  wire [31:0]m02_couplers_to_auto_pc_AWADDR;
  wire [1:0]m02_couplers_to_auto_pc_AWBURST;
  wire [3:0]m02_couplers_to_auto_pc_AWCACHE;
  wire [5:0]m02_couplers_to_auto_pc_AWID;
  wire [7:0]m02_couplers_to_auto_pc_AWLEN;
  wire [0:0]m02_couplers_to_auto_pc_AWLOCK;
  wire [2:0]m02_couplers_to_auto_pc_AWPROT;
  wire [3:0]m02_couplers_to_auto_pc_AWQOS;
  wire m02_couplers_to_auto_pc_AWREADY;
  wire [3:0]m02_couplers_to_auto_pc_AWREGION;
  wire [2:0]m02_couplers_to_auto_pc_AWSIZE;
  wire m02_couplers_to_auto_pc_AWVALID;
  wire [5:0]m02_couplers_to_auto_pc_BID;
  wire m02_couplers_to_auto_pc_BREADY;
  wire [1:0]m02_couplers_to_auto_pc_BRESP;
  wire m02_couplers_to_auto_pc_BVALID;
  wire [31:0]m02_couplers_to_auto_pc_RDATA;
  wire [5:0]m02_couplers_to_auto_pc_RID;
  wire m02_couplers_to_auto_pc_RLAST;
  wire m02_couplers_to_auto_pc_RREADY;
  wire [1:0]m02_couplers_to_auto_pc_RRESP;
  wire m02_couplers_to_auto_pc_RVALID;
  wire [31:0]m02_couplers_to_auto_pc_WDATA;
  wire m02_couplers_to_auto_pc_WLAST;
  wire m02_couplers_to_auto_pc_WREADY;
  wire [3:0]m02_couplers_to_auto_pc_WSTRB;
  wire m02_couplers_to_auto_pc_WVALID;

  assign M_AXI_araddr[31:0] = auto_pc_to_m02_couplers_ARADDR;
  assign M_AXI_arvalid = auto_pc_to_m02_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = auto_pc_to_m02_couplers_AWADDR;
  assign M_AXI_awvalid = auto_pc_to_m02_couplers_AWVALID;
  assign M_AXI_bready = auto_pc_to_m02_couplers_BREADY;
  assign M_AXI_rready = auto_pc_to_m02_couplers_RREADY;
  assign M_AXI_wdata[31:0] = auto_pc_to_m02_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = auto_pc_to_m02_couplers_WSTRB;
  assign M_AXI_wvalid = auto_pc_to_m02_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN;
  assign S_AXI_arready = m02_couplers_to_auto_pc_ARREADY;
  assign S_AXI_awready = m02_couplers_to_auto_pc_AWREADY;
  assign S_AXI_bid[5:0] = m02_couplers_to_auto_pc_BID;
  assign S_AXI_bresp[1:0] = m02_couplers_to_auto_pc_BRESP;
  assign S_AXI_bvalid = m02_couplers_to_auto_pc_BVALID;
  assign S_AXI_rdata[31:0] = m02_couplers_to_auto_pc_RDATA;
  assign S_AXI_rid[5:0] = m02_couplers_to_auto_pc_RID;
  assign S_AXI_rlast = m02_couplers_to_auto_pc_RLAST;
  assign S_AXI_rresp[1:0] = m02_couplers_to_auto_pc_RRESP;
  assign S_AXI_rvalid = m02_couplers_to_auto_pc_RVALID;
  assign S_AXI_wready = m02_couplers_to_auto_pc_WREADY;
  assign auto_pc_to_m02_couplers_ARREADY = M_AXI_arready;
  assign auto_pc_to_m02_couplers_AWREADY = M_AXI_awready;
  assign auto_pc_to_m02_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_pc_to_m02_couplers_BVALID = M_AXI_bvalid;
  assign auto_pc_to_m02_couplers_RDATA = M_AXI_rdata[31:0];
  assign auto_pc_to_m02_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_pc_to_m02_couplers_RVALID = M_AXI_rvalid;
  assign auto_pc_to_m02_couplers_WREADY = M_AXI_wready;
  assign m02_couplers_to_auto_pc_ARADDR = S_AXI_araddr[31:0];
  assign m02_couplers_to_auto_pc_ARBURST = S_AXI_arburst[1:0];
  assign m02_couplers_to_auto_pc_ARCACHE = S_AXI_arcache[3:0];
  assign m02_couplers_to_auto_pc_ARID = S_AXI_arid[5:0];
  assign m02_couplers_to_auto_pc_ARLEN = S_AXI_arlen[7:0];
  assign m02_couplers_to_auto_pc_ARLOCK = S_AXI_arlock[0];
  assign m02_couplers_to_auto_pc_ARPROT = S_AXI_arprot[2:0];
  assign m02_couplers_to_auto_pc_ARQOS = S_AXI_arqos[3:0];
  assign m02_couplers_to_auto_pc_ARREGION = S_AXI_arregion[3:0];
  assign m02_couplers_to_auto_pc_ARSIZE = S_AXI_arsize[2:0];
  assign m02_couplers_to_auto_pc_ARVALID = S_AXI_arvalid;
  assign m02_couplers_to_auto_pc_AWADDR = S_AXI_awaddr[31:0];
  assign m02_couplers_to_auto_pc_AWBURST = S_AXI_awburst[1:0];
  assign m02_couplers_to_auto_pc_AWCACHE = S_AXI_awcache[3:0];
  assign m02_couplers_to_auto_pc_AWID = S_AXI_awid[5:0];
  assign m02_couplers_to_auto_pc_AWLEN = S_AXI_awlen[7:0];
  assign m02_couplers_to_auto_pc_AWLOCK = S_AXI_awlock[0];
  assign m02_couplers_to_auto_pc_AWPROT = S_AXI_awprot[2:0];
  assign m02_couplers_to_auto_pc_AWQOS = S_AXI_awqos[3:0];
  assign m02_couplers_to_auto_pc_AWREGION = S_AXI_awregion[3:0];
  assign m02_couplers_to_auto_pc_AWSIZE = S_AXI_awsize[2:0];
  assign m02_couplers_to_auto_pc_AWVALID = S_AXI_awvalid;
  assign m02_couplers_to_auto_pc_BREADY = S_AXI_bready;
  assign m02_couplers_to_auto_pc_RREADY = S_AXI_rready;
  assign m02_couplers_to_auto_pc_WDATA = S_AXI_wdata[31:0];
  assign m02_couplers_to_auto_pc_WLAST = S_AXI_wlast;
  assign m02_couplers_to_auto_pc_WSTRB = S_AXI_wstrb[3:0];
  assign m02_couplers_to_auto_pc_WVALID = S_AXI_wvalid;
  gpn_bundle_block_auto_pc_1 auto_pc
       (.aclk(S_ACLK_1),
        .aresetn(S_ARESETN_1),
        .m_axi_araddr(auto_pc_to_m02_couplers_ARADDR),
        .m_axi_arready(auto_pc_to_m02_couplers_ARREADY),
        .m_axi_arvalid(auto_pc_to_m02_couplers_ARVALID),
        .m_axi_awaddr(auto_pc_to_m02_couplers_AWADDR),
        .m_axi_awready(auto_pc_to_m02_couplers_AWREADY),
        .m_axi_awvalid(auto_pc_to_m02_couplers_AWVALID),
        .m_axi_bready(auto_pc_to_m02_couplers_BREADY),
        .m_axi_bresp(auto_pc_to_m02_couplers_BRESP),
        .m_axi_bvalid(auto_pc_to_m02_couplers_BVALID),
        .m_axi_rdata(auto_pc_to_m02_couplers_RDATA),
        .m_axi_rready(auto_pc_to_m02_couplers_RREADY),
        .m_axi_rresp(auto_pc_to_m02_couplers_RRESP),
        .m_axi_rvalid(auto_pc_to_m02_couplers_RVALID),
        .m_axi_wdata(auto_pc_to_m02_couplers_WDATA),
        .m_axi_wready(auto_pc_to_m02_couplers_WREADY),
        .m_axi_wstrb(auto_pc_to_m02_couplers_WSTRB),
        .m_axi_wvalid(auto_pc_to_m02_couplers_WVALID),
        .s_axi_araddr(m02_couplers_to_auto_pc_ARADDR),
        .s_axi_arburst(m02_couplers_to_auto_pc_ARBURST),
        .s_axi_arcache(m02_couplers_to_auto_pc_ARCACHE),
        .s_axi_arid(m02_couplers_to_auto_pc_ARID),
        .s_axi_arlen(m02_couplers_to_auto_pc_ARLEN),
        .s_axi_arlock(m02_couplers_to_auto_pc_ARLOCK),
        .s_axi_arprot(m02_couplers_to_auto_pc_ARPROT),
        .s_axi_arqos(m02_couplers_to_auto_pc_ARQOS),
        .s_axi_arready(m02_couplers_to_auto_pc_ARREADY),
        .s_axi_arregion(m02_couplers_to_auto_pc_ARREGION),
        .s_axi_arsize(m02_couplers_to_auto_pc_ARSIZE),
        .s_axi_arvalid(m02_couplers_to_auto_pc_ARVALID),
        .s_axi_awaddr(m02_couplers_to_auto_pc_AWADDR),
        .s_axi_awburst(m02_couplers_to_auto_pc_AWBURST),
        .s_axi_awcache(m02_couplers_to_auto_pc_AWCACHE),
        .s_axi_awid(m02_couplers_to_auto_pc_AWID),
        .s_axi_awlen(m02_couplers_to_auto_pc_AWLEN),
        .s_axi_awlock(m02_couplers_to_auto_pc_AWLOCK),
        .s_axi_awprot(m02_couplers_to_auto_pc_AWPROT),
        .s_axi_awqos(m02_couplers_to_auto_pc_AWQOS),
        .s_axi_awready(m02_couplers_to_auto_pc_AWREADY),
        .s_axi_awregion(m02_couplers_to_auto_pc_AWREGION),
        .s_axi_awsize(m02_couplers_to_auto_pc_AWSIZE),
        .s_axi_awvalid(m02_couplers_to_auto_pc_AWVALID),
        .s_axi_bid(m02_couplers_to_auto_pc_BID),
        .s_axi_bready(m02_couplers_to_auto_pc_BREADY),
        .s_axi_bresp(m02_couplers_to_auto_pc_BRESP),
        .s_axi_bvalid(m02_couplers_to_auto_pc_BVALID),
        .s_axi_rdata(m02_couplers_to_auto_pc_RDATA),
        .s_axi_rid(m02_couplers_to_auto_pc_RID),
        .s_axi_rlast(m02_couplers_to_auto_pc_RLAST),
        .s_axi_rready(m02_couplers_to_auto_pc_RREADY),
        .s_axi_rresp(m02_couplers_to_auto_pc_RRESP),
        .s_axi_rvalid(m02_couplers_to_auto_pc_RVALID),
        .s_axi_wdata(m02_couplers_to_auto_pc_WDATA),
        .s_axi_wlast(m02_couplers_to_auto_pc_WLAST),
        .s_axi_wready(m02_couplers_to_auto_pc_WREADY),
        .s_axi_wstrb(m02_couplers_to_auto_pc_WSTRB),
        .s_axi_wvalid(m02_couplers_to_auto_pc_WVALID));
endmodule

module m03_couplers_imp_DQTQ2H
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arburst,
    M_AXI_arcache,
    M_AXI_arid,
    M_AXI_arlen,
    M_AXI_arlock,
    M_AXI_arprot,
    M_AXI_arqos,
    M_AXI_arready,
    M_AXI_arregion,
    M_AXI_arsize,
    M_AXI_aruser,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awburst,
    M_AXI_awcache,
    M_AXI_awid,
    M_AXI_awlen,
    M_AXI_awlock,
    M_AXI_awprot,
    M_AXI_awqos,
    M_AXI_awready,
    M_AXI_awregion,
    M_AXI_awsize,
    M_AXI_awuser,
    M_AXI_awvalid,
    M_AXI_bid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_buser,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rid,
    M_AXI_rlast,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_ruser,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wlast,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wuser,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arid,
    S_AXI_arlen,
    S_AXI_arlock,
    S_AXI_arprot,
    S_AXI_arqos,
    S_AXI_arready,
    S_AXI_arregion,
    S_AXI_arsize,
    S_AXI_aruser,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awid,
    S_AXI_awlen,
    S_AXI_awlock,
    S_AXI_awprot,
    S_AXI_awqos,
    S_AXI_awready,
    S_AXI_awregion,
    S_AXI_awsize,
    S_AXI_awuser,
    S_AXI_awvalid,
    S_AXI_bid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_buser,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rid,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_ruser,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wuser,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [1:0]M_AXI_arburst;
  output [3:0]M_AXI_arcache;
  output [5:0]M_AXI_arid;
  output [7:0]M_AXI_arlen;
  output [0:0]M_AXI_arlock;
  output [2:0]M_AXI_arprot;
  output [3:0]M_AXI_arqos;
  input M_AXI_arready;
  output [3:0]M_AXI_arregion;
  output [2:0]M_AXI_arsize;
  output [3:0]M_AXI_aruser;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [1:0]M_AXI_awburst;
  output [3:0]M_AXI_awcache;
  output [5:0]M_AXI_awid;
  output [7:0]M_AXI_awlen;
  output [0:0]M_AXI_awlock;
  output [2:0]M_AXI_awprot;
  output [3:0]M_AXI_awqos;
  input M_AXI_awready;
  output [3:0]M_AXI_awregion;
  output [2:0]M_AXI_awsize;
  output [3:0]M_AXI_awuser;
  output M_AXI_awvalid;
  input [5:0]M_AXI_bid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input [3:0]M_AXI_buser;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  input [5:0]M_AXI_rid;
  input M_AXI_rlast;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input [3:0]M_AXI_ruser;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  output M_AXI_wlast;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output [3:0]M_AXI_wuser;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [5:0]S_AXI_arid;
  input [7:0]S_AXI_arlen;
  input [0:0]S_AXI_arlock;
  input [2:0]S_AXI_arprot;
  input [3:0]S_AXI_arqos;
  output S_AXI_arready;
  input [3:0]S_AXI_arregion;
  input [2:0]S_AXI_arsize;
  input [3:0]S_AXI_aruser;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [5:0]S_AXI_awid;
  input [7:0]S_AXI_awlen;
  input [0:0]S_AXI_awlock;
  input [2:0]S_AXI_awprot;
  input [3:0]S_AXI_awqos;
  output S_AXI_awready;
  input [3:0]S_AXI_awregion;
  input [2:0]S_AXI_awsize;
  input [3:0]S_AXI_awuser;
  input S_AXI_awvalid;
  output [5:0]S_AXI_bid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output [3:0]S_AXI_buser;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  output [5:0]S_AXI_rid;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output [3:0]S_AXI_ruser;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  input S_AXI_wlast;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input [3:0]S_AXI_wuser;
  input S_AXI_wvalid;

  wire [31:0]m03_couplers_to_m03_couplers_ARADDR;
  wire [1:0]m03_couplers_to_m03_couplers_ARBURST;
  wire [3:0]m03_couplers_to_m03_couplers_ARCACHE;
  wire [5:0]m03_couplers_to_m03_couplers_ARID;
  wire [7:0]m03_couplers_to_m03_couplers_ARLEN;
  wire [0:0]m03_couplers_to_m03_couplers_ARLOCK;
  wire [2:0]m03_couplers_to_m03_couplers_ARPROT;
  wire [3:0]m03_couplers_to_m03_couplers_ARQOS;
  wire m03_couplers_to_m03_couplers_ARREADY;
  wire [3:0]m03_couplers_to_m03_couplers_ARREGION;
  wire [2:0]m03_couplers_to_m03_couplers_ARSIZE;
  wire [3:0]m03_couplers_to_m03_couplers_ARUSER;
  wire m03_couplers_to_m03_couplers_ARVALID;
  wire [31:0]m03_couplers_to_m03_couplers_AWADDR;
  wire [1:0]m03_couplers_to_m03_couplers_AWBURST;
  wire [3:0]m03_couplers_to_m03_couplers_AWCACHE;
  wire [5:0]m03_couplers_to_m03_couplers_AWID;
  wire [7:0]m03_couplers_to_m03_couplers_AWLEN;
  wire [0:0]m03_couplers_to_m03_couplers_AWLOCK;
  wire [2:0]m03_couplers_to_m03_couplers_AWPROT;
  wire [3:0]m03_couplers_to_m03_couplers_AWQOS;
  wire m03_couplers_to_m03_couplers_AWREADY;
  wire [3:0]m03_couplers_to_m03_couplers_AWREGION;
  wire [2:0]m03_couplers_to_m03_couplers_AWSIZE;
  wire [3:0]m03_couplers_to_m03_couplers_AWUSER;
  wire m03_couplers_to_m03_couplers_AWVALID;
  wire [5:0]m03_couplers_to_m03_couplers_BID;
  wire m03_couplers_to_m03_couplers_BREADY;
  wire [1:0]m03_couplers_to_m03_couplers_BRESP;
  wire [3:0]m03_couplers_to_m03_couplers_BUSER;
  wire m03_couplers_to_m03_couplers_BVALID;
  wire [31:0]m03_couplers_to_m03_couplers_RDATA;
  wire [5:0]m03_couplers_to_m03_couplers_RID;
  wire m03_couplers_to_m03_couplers_RLAST;
  wire m03_couplers_to_m03_couplers_RREADY;
  wire [1:0]m03_couplers_to_m03_couplers_RRESP;
  wire [3:0]m03_couplers_to_m03_couplers_RUSER;
  wire m03_couplers_to_m03_couplers_RVALID;
  wire [31:0]m03_couplers_to_m03_couplers_WDATA;
  wire m03_couplers_to_m03_couplers_WLAST;
  wire m03_couplers_to_m03_couplers_WREADY;
  wire [3:0]m03_couplers_to_m03_couplers_WSTRB;
  wire [3:0]m03_couplers_to_m03_couplers_WUSER;
  wire m03_couplers_to_m03_couplers_WVALID;

  assign M_AXI_araddr[31:0] = m03_couplers_to_m03_couplers_ARADDR;
  assign M_AXI_arburst[1:0] = m03_couplers_to_m03_couplers_ARBURST;
  assign M_AXI_arcache[3:0] = m03_couplers_to_m03_couplers_ARCACHE;
  assign M_AXI_arid[5:0] = m03_couplers_to_m03_couplers_ARID;
  assign M_AXI_arlen[7:0] = m03_couplers_to_m03_couplers_ARLEN;
  assign M_AXI_arlock[0] = m03_couplers_to_m03_couplers_ARLOCK;
  assign M_AXI_arprot[2:0] = m03_couplers_to_m03_couplers_ARPROT;
  assign M_AXI_arqos[3:0] = m03_couplers_to_m03_couplers_ARQOS;
  assign M_AXI_arregion[3:0] = m03_couplers_to_m03_couplers_ARREGION;
  assign M_AXI_arsize[2:0] = m03_couplers_to_m03_couplers_ARSIZE;
  assign M_AXI_aruser[3:0] = m03_couplers_to_m03_couplers_ARUSER;
  assign M_AXI_arvalid = m03_couplers_to_m03_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = m03_couplers_to_m03_couplers_AWADDR;
  assign M_AXI_awburst[1:0] = m03_couplers_to_m03_couplers_AWBURST;
  assign M_AXI_awcache[3:0] = m03_couplers_to_m03_couplers_AWCACHE;
  assign M_AXI_awid[5:0] = m03_couplers_to_m03_couplers_AWID;
  assign M_AXI_awlen[7:0] = m03_couplers_to_m03_couplers_AWLEN;
  assign M_AXI_awlock[0] = m03_couplers_to_m03_couplers_AWLOCK;
  assign M_AXI_awprot[2:0] = m03_couplers_to_m03_couplers_AWPROT;
  assign M_AXI_awqos[3:0] = m03_couplers_to_m03_couplers_AWQOS;
  assign M_AXI_awregion[3:0] = m03_couplers_to_m03_couplers_AWREGION;
  assign M_AXI_awsize[2:0] = m03_couplers_to_m03_couplers_AWSIZE;
  assign M_AXI_awuser[3:0] = m03_couplers_to_m03_couplers_AWUSER;
  assign M_AXI_awvalid = m03_couplers_to_m03_couplers_AWVALID;
  assign M_AXI_bready = m03_couplers_to_m03_couplers_BREADY;
  assign M_AXI_rready = m03_couplers_to_m03_couplers_RREADY;
  assign M_AXI_wdata[31:0] = m03_couplers_to_m03_couplers_WDATA;
  assign M_AXI_wlast = m03_couplers_to_m03_couplers_WLAST;
  assign M_AXI_wstrb[3:0] = m03_couplers_to_m03_couplers_WSTRB;
  assign M_AXI_wuser[3:0] = m03_couplers_to_m03_couplers_WUSER;
  assign M_AXI_wvalid = m03_couplers_to_m03_couplers_WVALID;
  assign S_AXI_arready = m03_couplers_to_m03_couplers_ARREADY;
  assign S_AXI_awready = m03_couplers_to_m03_couplers_AWREADY;
  assign S_AXI_bid[5:0] = m03_couplers_to_m03_couplers_BID;
  assign S_AXI_bresp[1:0] = m03_couplers_to_m03_couplers_BRESP;
  assign S_AXI_buser[3:0] = m03_couplers_to_m03_couplers_BUSER;
  assign S_AXI_bvalid = m03_couplers_to_m03_couplers_BVALID;
  assign S_AXI_rdata[31:0] = m03_couplers_to_m03_couplers_RDATA;
  assign S_AXI_rid[5:0] = m03_couplers_to_m03_couplers_RID;
  assign S_AXI_rlast = m03_couplers_to_m03_couplers_RLAST;
  assign S_AXI_rresp[1:0] = m03_couplers_to_m03_couplers_RRESP;
  assign S_AXI_ruser[3:0] = m03_couplers_to_m03_couplers_RUSER;
  assign S_AXI_rvalid = m03_couplers_to_m03_couplers_RVALID;
  assign S_AXI_wready = m03_couplers_to_m03_couplers_WREADY;
  assign m03_couplers_to_m03_couplers_ARADDR = S_AXI_araddr[31:0];
  assign m03_couplers_to_m03_couplers_ARBURST = S_AXI_arburst[1:0];
  assign m03_couplers_to_m03_couplers_ARCACHE = S_AXI_arcache[3:0];
  assign m03_couplers_to_m03_couplers_ARID = S_AXI_arid[5:0];
  assign m03_couplers_to_m03_couplers_ARLEN = S_AXI_arlen[7:0];
  assign m03_couplers_to_m03_couplers_ARLOCK = S_AXI_arlock[0];
  assign m03_couplers_to_m03_couplers_ARPROT = S_AXI_arprot[2:0];
  assign m03_couplers_to_m03_couplers_ARQOS = S_AXI_arqos[3:0];
  assign m03_couplers_to_m03_couplers_ARREADY = M_AXI_arready;
  assign m03_couplers_to_m03_couplers_ARREGION = S_AXI_arregion[3:0];
  assign m03_couplers_to_m03_couplers_ARSIZE = S_AXI_arsize[2:0];
  assign m03_couplers_to_m03_couplers_ARUSER = S_AXI_aruser[3:0];
  assign m03_couplers_to_m03_couplers_ARVALID = S_AXI_arvalid;
  assign m03_couplers_to_m03_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign m03_couplers_to_m03_couplers_AWBURST = S_AXI_awburst[1:0];
  assign m03_couplers_to_m03_couplers_AWCACHE = S_AXI_awcache[3:0];
  assign m03_couplers_to_m03_couplers_AWID = S_AXI_awid[5:0];
  assign m03_couplers_to_m03_couplers_AWLEN = S_AXI_awlen[7:0];
  assign m03_couplers_to_m03_couplers_AWLOCK = S_AXI_awlock[0];
  assign m03_couplers_to_m03_couplers_AWPROT = S_AXI_awprot[2:0];
  assign m03_couplers_to_m03_couplers_AWQOS = S_AXI_awqos[3:0];
  assign m03_couplers_to_m03_couplers_AWREADY = M_AXI_awready;
  assign m03_couplers_to_m03_couplers_AWREGION = S_AXI_awregion[3:0];
  assign m03_couplers_to_m03_couplers_AWSIZE = S_AXI_awsize[2:0];
  assign m03_couplers_to_m03_couplers_AWUSER = S_AXI_awuser[3:0];
  assign m03_couplers_to_m03_couplers_AWVALID = S_AXI_awvalid;
  assign m03_couplers_to_m03_couplers_BID = M_AXI_bid[5:0];
  assign m03_couplers_to_m03_couplers_BREADY = S_AXI_bready;
  assign m03_couplers_to_m03_couplers_BRESP = M_AXI_bresp[1:0];
  assign m03_couplers_to_m03_couplers_BUSER = M_AXI_buser[3:0];
  assign m03_couplers_to_m03_couplers_BVALID = M_AXI_bvalid;
  assign m03_couplers_to_m03_couplers_RDATA = M_AXI_rdata[31:0];
  assign m03_couplers_to_m03_couplers_RID = M_AXI_rid[5:0];
  assign m03_couplers_to_m03_couplers_RLAST = M_AXI_rlast;
  assign m03_couplers_to_m03_couplers_RREADY = S_AXI_rready;
  assign m03_couplers_to_m03_couplers_RRESP = M_AXI_rresp[1:0];
  assign m03_couplers_to_m03_couplers_RUSER = M_AXI_ruser[3:0];
  assign m03_couplers_to_m03_couplers_RVALID = M_AXI_rvalid;
  assign m03_couplers_to_m03_couplers_WDATA = S_AXI_wdata[31:0];
  assign m03_couplers_to_m03_couplers_WLAST = S_AXI_wlast;
  assign m03_couplers_to_m03_couplers_WREADY = M_AXI_wready;
  assign m03_couplers_to_m03_couplers_WSTRB = S_AXI_wstrb[3:0];
  assign m03_couplers_to_m03_couplers_WUSER = S_AXI_wuser[3:0];
  assign m03_couplers_to_m03_couplers_WVALID = S_AXI_wvalid;
endmodule

module m04_couplers_imp_1SQAA73
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arid,
    S_AXI_arlen,
    S_AXI_arlock,
    S_AXI_arprot,
    S_AXI_arqos,
    S_AXI_arready,
    S_AXI_arregion,
    S_AXI_arsize,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awid,
    S_AXI_awlen,
    S_AXI_awlock,
    S_AXI_awprot,
    S_AXI_awqos,
    S_AXI_awready,
    S_AXI_awregion,
    S_AXI_awsize,
    S_AXI_awvalid,
    S_AXI_bid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rid,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  input M_AXI_arready;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  input M_AXI_awready;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [5:0]S_AXI_arid;
  input [7:0]S_AXI_arlen;
  input [0:0]S_AXI_arlock;
  input [2:0]S_AXI_arprot;
  input [3:0]S_AXI_arqos;
  output S_AXI_arready;
  input [3:0]S_AXI_arregion;
  input [2:0]S_AXI_arsize;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [5:0]S_AXI_awid;
  input [7:0]S_AXI_awlen;
  input [0:0]S_AXI_awlock;
  input [2:0]S_AXI_awprot;
  input [3:0]S_AXI_awqos;
  output S_AXI_awready;
  input [3:0]S_AXI_awregion;
  input [2:0]S_AXI_awsize;
  input S_AXI_awvalid;
  output [5:0]S_AXI_bid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  output [5:0]S_AXI_rid;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  input S_AXI_wlast;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire S_ACLK_1;
  wire S_ARESETN_1;
  wire [31:0]auto_pc_to_m04_couplers_ARADDR;
  wire auto_pc_to_m04_couplers_ARREADY;
  wire auto_pc_to_m04_couplers_ARVALID;
  wire [31:0]auto_pc_to_m04_couplers_AWADDR;
  wire auto_pc_to_m04_couplers_AWREADY;
  wire auto_pc_to_m04_couplers_AWVALID;
  wire auto_pc_to_m04_couplers_BREADY;
  wire [1:0]auto_pc_to_m04_couplers_BRESP;
  wire auto_pc_to_m04_couplers_BVALID;
  wire [31:0]auto_pc_to_m04_couplers_RDATA;
  wire auto_pc_to_m04_couplers_RREADY;
  wire [1:0]auto_pc_to_m04_couplers_RRESP;
  wire auto_pc_to_m04_couplers_RVALID;
  wire [31:0]auto_pc_to_m04_couplers_WDATA;
  wire auto_pc_to_m04_couplers_WREADY;
  wire [3:0]auto_pc_to_m04_couplers_WSTRB;
  wire auto_pc_to_m04_couplers_WVALID;
  wire [31:0]m04_couplers_to_auto_pc_ARADDR;
  wire [1:0]m04_couplers_to_auto_pc_ARBURST;
  wire [3:0]m04_couplers_to_auto_pc_ARCACHE;
  wire [5:0]m04_couplers_to_auto_pc_ARID;
  wire [7:0]m04_couplers_to_auto_pc_ARLEN;
  wire [0:0]m04_couplers_to_auto_pc_ARLOCK;
  wire [2:0]m04_couplers_to_auto_pc_ARPROT;
  wire [3:0]m04_couplers_to_auto_pc_ARQOS;
  wire m04_couplers_to_auto_pc_ARREADY;
  wire [3:0]m04_couplers_to_auto_pc_ARREGION;
  wire [2:0]m04_couplers_to_auto_pc_ARSIZE;
  wire m04_couplers_to_auto_pc_ARVALID;
  wire [31:0]m04_couplers_to_auto_pc_AWADDR;
  wire [1:0]m04_couplers_to_auto_pc_AWBURST;
  wire [3:0]m04_couplers_to_auto_pc_AWCACHE;
  wire [5:0]m04_couplers_to_auto_pc_AWID;
  wire [7:0]m04_couplers_to_auto_pc_AWLEN;
  wire [0:0]m04_couplers_to_auto_pc_AWLOCK;
  wire [2:0]m04_couplers_to_auto_pc_AWPROT;
  wire [3:0]m04_couplers_to_auto_pc_AWQOS;
  wire m04_couplers_to_auto_pc_AWREADY;
  wire [3:0]m04_couplers_to_auto_pc_AWREGION;
  wire [2:0]m04_couplers_to_auto_pc_AWSIZE;
  wire m04_couplers_to_auto_pc_AWVALID;
  wire [5:0]m04_couplers_to_auto_pc_BID;
  wire m04_couplers_to_auto_pc_BREADY;
  wire [1:0]m04_couplers_to_auto_pc_BRESP;
  wire m04_couplers_to_auto_pc_BVALID;
  wire [31:0]m04_couplers_to_auto_pc_RDATA;
  wire [5:0]m04_couplers_to_auto_pc_RID;
  wire m04_couplers_to_auto_pc_RLAST;
  wire m04_couplers_to_auto_pc_RREADY;
  wire [1:0]m04_couplers_to_auto_pc_RRESP;
  wire m04_couplers_to_auto_pc_RVALID;
  wire [31:0]m04_couplers_to_auto_pc_WDATA;
  wire m04_couplers_to_auto_pc_WLAST;
  wire m04_couplers_to_auto_pc_WREADY;
  wire [3:0]m04_couplers_to_auto_pc_WSTRB;
  wire m04_couplers_to_auto_pc_WVALID;

  assign M_AXI_araddr[31:0] = auto_pc_to_m04_couplers_ARADDR;
  assign M_AXI_arvalid = auto_pc_to_m04_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = auto_pc_to_m04_couplers_AWADDR;
  assign M_AXI_awvalid = auto_pc_to_m04_couplers_AWVALID;
  assign M_AXI_bready = auto_pc_to_m04_couplers_BREADY;
  assign M_AXI_rready = auto_pc_to_m04_couplers_RREADY;
  assign M_AXI_wdata[31:0] = auto_pc_to_m04_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = auto_pc_to_m04_couplers_WSTRB;
  assign M_AXI_wvalid = auto_pc_to_m04_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN;
  assign S_AXI_arready = m04_couplers_to_auto_pc_ARREADY;
  assign S_AXI_awready = m04_couplers_to_auto_pc_AWREADY;
  assign S_AXI_bid[5:0] = m04_couplers_to_auto_pc_BID;
  assign S_AXI_bresp[1:0] = m04_couplers_to_auto_pc_BRESP;
  assign S_AXI_bvalid = m04_couplers_to_auto_pc_BVALID;
  assign S_AXI_rdata[31:0] = m04_couplers_to_auto_pc_RDATA;
  assign S_AXI_rid[5:0] = m04_couplers_to_auto_pc_RID;
  assign S_AXI_rlast = m04_couplers_to_auto_pc_RLAST;
  assign S_AXI_rresp[1:0] = m04_couplers_to_auto_pc_RRESP;
  assign S_AXI_rvalid = m04_couplers_to_auto_pc_RVALID;
  assign S_AXI_wready = m04_couplers_to_auto_pc_WREADY;
  assign auto_pc_to_m04_couplers_ARREADY = M_AXI_arready;
  assign auto_pc_to_m04_couplers_AWREADY = M_AXI_awready;
  assign auto_pc_to_m04_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_pc_to_m04_couplers_BVALID = M_AXI_bvalid;
  assign auto_pc_to_m04_couplers_RDATA = M_AXI_rdata[31:0];
  assign auto_pc_to_m04_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_pc_to_m04_couplers_RVALID = M_AXI_rvalid;
  assign auto_pc_to_m04_couplers_WREADY = M_AXI_wready;
  assign m04_couplers_to_auto_pc_ARADDR = S_AXI_araddr[31:0];
  assign m04_couplers_to_auto_pc_ARBURST = S_AXI_arburst[1:0];
  assign m04_couplers_to_auto_pc_ARCACHE = S_AXI_arcache[3:0];
  assign m04_couplers_to_auto_pc_ARID = S_AXI_arid[5:0];
  assign m04_couplers_to_auto_pc_ARLEN = S_AXI_arlen[7:0];
  assign m04_couplers_to_auto_pc_ARLOCK = S_AXI_arlock[0];
  assign m04_couplers_to_auto_pc_ARPROT = S_AXI_arprot[2:0];
  assign m04_couplers_to_auto_pc_ARQOS = S_AXI_arqos[3:0];
  assign m04_couplers_to_auto_pc_ARREGION = S_AXI_arregion[3:0];
  assign m04_couplers_to_auto_pc_ARSIZE = S_AXI_arsize[2:0];
  assign m04_couplers_to_auto_pc_ARVALID = S_AXI_arvalid;
  assign m04_couplers_to_auto_pc_AWADDR = S_AXI_awaddr[31:0];
  assign m04_couplers_to_auto_pc_AWBURST = S_AXI_awburst[1:0];
  assign m04_couplers_to_auto_pc_AWCACHE = S_AXI_awcache[3:0];
  assign m04_couplers_to_auto_pc_AWID = S_AXI_awid[5:0];
  assign m04_couplers_to_auto_pc_AWLEN = S_AXI_awlen[7:0];
  assign m04_couplers_to_auto_pc_AWLOCK = S_AXI_awlock[0];
  assign m04_couplers_to_auto_pc_AWPROT = S_AXI_awprot[2:0];
  assign m04_couplers_to_auto_pc_AWQOS = S_AXI_awqos[3:0];
  assign m04_couplers_to_auto_pc_AWREGION = S_AXI_awregion[3:0];
  assign m04_couplers_to_auto_pc_AWSIZE = S_AXI_awsize[2:0];
  assign m04_couplers_to_auto_pc_AWVALID = S_AXI_awvalid;
  assign m04_couplers_to_auto_pc_BREADY = S_AXI_bready;
  assign m04_couplers_to_auto_pc_RREADY = S_AXI_rready;
  assign m04_couplers_to_auto_pc_WDATA = S_AXI_wdata[31:0];
  assign m04_couplers_to_auto_pc_WLAST = S_AXI_wlast;
  assign m04_couplers_to_auto_pc_WSTRB = S_AXI_wstrb[3:0];
  assign m04_couplers_to_auto_pc_WVALID = S_AXI_wvalid;
  gpn_bundle_block_auto_pc_2 auto_pc
       (.aclk(S_ACLK_1),
        .aresetn(S_ARESETN_1),
        .m_axi_araddr(auto_pc_to_m04_couplers_ARADDR),
        .m_axi_arready(auto_pc_to_m04_couplers_ARREADY),
        .m_axi_arvalid(auto_pc_to_m04_couplers_ARVALID),
        .m_axi_awaddr(auto_pc_to_m04_couplers_AWADDR),
        .m_axi_awready(auto_pc_to_m04_couplers_AWREADY),
        .m_axi_awvalid(auto_pc_to_m04_couplers_AWVALID),
        .m_axi_bready(auto_pc_to_m04_couplers_BREADY),
        .m_axi_bresp(auto_pc_to_m04_couplers_BRESP),
        .m_axi_bvalid(auto_pc_to_m04_couplers_BVALID),
        .m_axi_rdata(auto_pc_to_m04_couplers_RDATA),
        .m_axi_rready(auto_pc_to_m04_couplers_RREADY),
        .m_axi_rresp(auto_pc_to_m04_couplers_RRESP),
        .m_axi_rvalid(auto_pc_to_m04_couplers_RVALID),
        .m_axi_wdata(auto_pc_to_m04_couplers_WDATA),
        .m_axi_wready(auto_pc_to_m04_couplers_WREADY),
        .m_axi_wstrb(auto_pc_to_m04_couplers_WSTRB),
        .m_axi_wvalid(auto_pc_to_m04_couplers_WVALID),
        .s_axi_araddr(m04_couplers_to_auto_pc_ARADDR),
        .s_axi_arburst(m04_couplers_to_auto_pc_ARBURST),
        .s_axi_arcache(m04_couplers_to_auto_pc_ARCACHE),
        .s_axi_arid(m04_couplers_to_auto_pc_ARID),
        .s_axi_arlen(m04_couplers_to_auto_pc_ARLEN),
        .s_axi_arlock(m04_couplers_to_auto_pc_ARLOCK),
        .s_axi_arprot(m04_couplers_to_auto_pc_ARPROT),
        .s_axi_arqos(m04_couplers_to_auto_pc_ARQOS),
        .s_axi_arready(m04_couplers_to_auto_pc_ARREADY),
        .s_axi_arregion(m04_couplers_to_auto_pc_ARREGION),
        .s_axi_arsize(m04_couplers_to_auto_pc_ARSIZE),
        .s_axi_arvalid(m04_couplers_to_auto_pc_ARVALID),
        .s_axi_awaddr(m04_couplers_to_auto_pc_AWADDR),
        .s_axi_awburst(m04_couplers_to_auto_pc_AWBURST),
        .s_axi_awcache(m04_couplers_to_auto_pc_AWCACHE),
        .s_axi_awid(m04_couplers_to_auto_pc_AWID),
        .s_axi_awlen(m04_couplers_to_auto_pc_AWLEN),
        .s_axi_awlock(m04_couplers_to_auto_pc_AWLOCK),
        .s_axi_awprot(m04_couplers_to_auto_pc_AWPROT),
        .s_axi_awqos(m04_couplers_to_auto_pc_AWQOS),
        .s_axi_awready(m04_couplers_to_auto_pc_AWREADY),
        .s_axi_awregion(m04_couplers_to_auto_pc_AWREGION),
        .s_axi_awsize(m04_couplers_to_auto_pc_AWSIZE),
        .s_axi_awvalid(m04_couplers_to_auto_pc_AWVALID),
        .s_axi_bid(m04_couplers_to_auto_pc_BID),
        .s_axi_bready(m04_couplers_to_auto_pc_BREADY),
        .s_axi_bresp(m04_couplers_to_auto_pc_BRESP),
        .s_axi_bvalid(m04_couplers_to_auto_pc_BVALID),
        .s_axi_rdata(m04_couplers_to_auto_pc_RDATA),
        .s_axi_rid(m04_couplers_to_auto_pc_RID),
        .s_axi_rlast(m04_couplers_to_auto_pc_RLAST),
        .s_axi_rready(m04_couplers_to_auto_pc_RREADY),
        .s_axi_rresp(m04_couplers_to_auto_pc_RRESP),
        .s_axi_rvalid(m04_couplers_to_auto_pc_RVALID),
        .s_axi_wdata(m04_couplers_to_auto_pc_WDATA),
        .s_axi_wlast(m04_couplers_to_auto_pc_WLAST),
        .s_axi_wready(m04_couplers_to_auto_pc_WREADY),
        .s_axi_wstrb(m04_couplers_to_auto_pc_WSTRB),
        .s_axi_wvalid(m04_couplers_to_auto_pc_WVALID));
endmodule

module m05_couplers_imp_3Z8KUG
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arid,
    S_AXI_arlen,
    S_AXI_arlock,
    S_AXI_arprot,
    S_AXI_arqos,
    S_AXI_arready,
    S_AXI_arregion,
    S_AXI_arsize,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awid,
    S_AXI_awlen,
    S_AXI_awlock,
    S_AXI_awprot,
    S_AXI_awqos,
    S_AXI_awready,
    S_AXI_awregion,
    S_AXI_awsize,
    S_AXI_awvalid,
    S_AXI_bid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rid,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  input M_AXI_arready;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  input M_AXI_awready;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [5:0]S_AXI_arid;
  input [7:0]S_AXI_arlen;
  input [0:0]S_AXI_arlock;
  input [2:0]S_AXI_arprot;
  input [3:0]S_AXI_arqos;
  output S_AXI_arready;
  input [3:0]S_AXI_arregion;
  input [2:0]S_AXI_arsize;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [5:0]S_AXI_awid;
  input [7:0]S_AXI_awlen;
  input [0:0]S_AXI_awlock;
  input [2:0]S_AXI_awprot;
  input [3:0]S_AXI_awqos;
  output S_AXI_awready;
  input [3:0]S_AXI_awregion;
  input [2:0]S_AXI_awsize;
  input S_AXI_awvalid;
  output [5:0]S_AXI_bid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  output [5:0]S_AXI_rid;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  input S_AXI_wlast;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire S_ACLK_1;
  wire S_ARESETN_1;
  wire [31:0]auto_pc_to_m05_couplers_ARADDR;
  wire auto_pc_to_m05_couplers_ARREADY;
  wire auto_pc_to_m05_couplers_ARVALID;
  wire [31:0]auto_pc_to_m05_couplers_AWADDR;
  wire auto_pc_to_m05_couplers_AWREADY;
  wire auto_pc_to_m05_couplers_AWVALID;
  wire auto_pc_to_m05_couplers_BREADY;
  wire [1:0]auto_pc_to_m05_couplers_BRESP;
  wire auto_pc_to_m05_couplers_BVALID;
  wire [31:0]auto_pc_to_m05_couplers_RDATA;
  wire auto_pc_to_m05_couplers_RREADY;
  wire [1:0]auto_pc_to_m05_couplers_RRESP;
  wire auto_pc_to_m05_couplers_RVALID;
  wire [31:0]auto_pc_to_m05_couplers_WDATA;
  wire auto_pc_to_m05_couplers_WREADY;
  wire [3:0]auto_pc_to_m05_couplers_WSTRB;
  wire auto_pc_to_m05_couplers_WVALID;
  wire [31:0]m05_couplers_to_auto_pc_ARADDR;
  wire [1:0]m05_couplers_to_auto_pc_ARBURST;
  wire [3:0]m05_couplers_to_auto_pc_ARCACHE;
  wire [5:0]m05_couplers_to_auto_pc_ARID;
  wire [7:0]m05_couplers_to_auto_pc_ARLEN;
  wire [0:0]m05_couplers_to_auto_pc_ARLOCK;
  wire [2:0]m05_couplers_to_auto_pc_ARPROT;
  wire [3:0]m05_couplers_to_auto_pc_ARQOS;
  wire m05_couplers_to_auto_pc_ARREADY;
  wire [3:0]m05_couplers_to_auto_pc_ARREGION;
  wire [2:0]m05_couplers_to_auto_pc_ARSIZE;
  wire m05_couplers_to_auto_pc_ARVALID;
  wire [31:0]m05_couplers_to_auto_pc_AWADDR;
  wire [1:0]m05_couplers_to_auto_pc_AWBURST;
  wire [3:0]m05_couplers_to_auto_pc_AWCACHE;
  wire [5:0]m05_couplers_to_auto_pc_AWID;
  wire [7:0]m05_couplers_to_auto_pc_AWLEN;
  wire [0:0]m05_couplers_to_auto_pc_AWLOCK;
  wire [2:0]m05_couplers_to_auto_pc_AWPROT;
  wire [3:0]m05_couplers_to_auto_pc_AWQOS;
  wire m05_couplers_to_auto_pc_AWREADY;
  wire [3:0]m05_couplers_to_auto_pc_AWREGION;
  wire [2:0]m05_couplers_to_auto_pc_AWSIZE;
  wire m05_couplers_to_auto_pc_AWVALID;
  wire [5:0]m05_couplers_to_auto_pc_BID;
  wire m05_couplers_to_auto_pc_BREADY;
  wire [1:0]m05_couplers_to_auto_pc_BRESP;
  wire m05_couplers_to_auto_pc_BVALID;
  wire [31:0]m05_couplers_to_auto_pc_RDATA;
  wire [5:0]m05_couplers_to_auto_pc_RID;
  wire m05_couplers_to_auto_pc_RLAST;
  wire m05_couplers_to_auto_pc_RREADY;
  wire [1:0]m05_couplers_to_auto_pc_RRESP;
  wire m05_couplers_to_auto_pc_RVALID;
  wire [31:0]m05_couplers_to_auto_pc_WDATA;
  wire m05_couplers_to_auto_pc_WLAST;
  wire m05_couplers_to_auto_pc_WREADY;
  wire [3:0]m05_couplers_to_auto_pc_WSTRB;
  wire m05_couplers_to_auto_pc_WVALID;

  assign M_AXI_araddr[31:0] = auto_pc_to_m05_couplers_ARADDR;
  assign M_AXI_arvalid = auto_pc_to_m05_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = auto_pc_to_m05_couplers_AWADDR;
  assign M_AXI_awvalid = auto_pc_to_m05_couplers_AWVALID;
  assign M_AXI_bready = auto_pc_to_m05_couplers_BREADY;
  assign M_AXI_rready = auto_pc_to_m05_couplers_RREADY;
  assign M_AXI_wdata[31:0] = auto_pc_to_m05_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = auto_pc_to_m05_couplers_WSTRB;
  assign M_AXI_wvalid = auto_pc_to_m05_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN;
  assign S_AXI_arready = m05_couplers_to_auto_pc_ARREADY;
  assign S_AXI_awready = m05_couplers_to_auto_pc_AWREADY;
  assign S_AXI_bid[5:0] = m05_couplers_to_auto_pc_BID;
  assign S_AXI_bresp[1:0] = m05_couplers_to_auto_pc_BRESP;
  assign S_AXI_bvalid = m05_couplers_to_auto_pc_BVALID;
  assign S_AXI_rdata[31:0] = m05_couplers_to_auto_pc_RDATA;
  assign S_AXI_rid[5:0] = m05_couplers_to_auto_pc_RID;
  assign S_AXI_rlast = m05_couplers_to_auto_pc_RLAST;
  assign S_AXI_rresp[1:0] = m05_couplers_to_auto_pc_RRESP;
  assign S_AXI_rvalid = m05_couplers_to_auto_pc_RVALID;
  assign S_AXI_wready = m05_couplers_to_auto_pc_WREADY;
  assign auto_pc_to_m05_couplers_ARREADY = M_AXI_arready;
  assign auto_pc_to_m05_couplers_AWREADY = M_AXI_awready;
  assign auto_pc_to_m05_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_pc_to_m05_couplers_BVALID = M_AXI_bvalid;
  assign auto_pc_to_m05_couplers_RDATA = M_AXI_rdata[31:0];
  assign auto_pc_to_m05_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_pc_to_m05_couplers_RVALID = M_AXI_rvalid;
  assign auto_pc_to_m05_couplers_WREADY = M_AXI_wready;
  assign m05_couplers_to_auto_pc_ARADDR = S_AXI_araddr[31:0];
  assign m05_couplers_to_auto_pc_ARBURST = S_AXI_arburst[1:0];
  assign m05_couplers_to_auto_pc_ARCACHE = S_AXI_arcache[3:0];
  assign m05_couplers_to_auto_pc_ARID = S_AXI_arid[5:0];
  assign m05_couplers_to_auto_pc_ARLEN = S_AXI_arlen[7:0];
  assign m05_couplers_to_auto_pc_ARLOCK = S_AXI_arlock[0];
  assign m05_couplers_to_auto_pc_ARPROT = S_AXI_arprot[2:0];
  assign m05_couplers_to_auto_pc_ARQOS = S_AXI_arqos[3:0];
  assign m05_couplers_to_auto_pc_ARREGION = S_AXI_arregion[3:0];
  assign m05_couplers_to_auto_pc_ARSIZE = S_AXI_arsize[2:0];
  assign m05_couplers_to_auto_pc_ARVALID = S_AXI_arvalid;
  assign m05_couplers_to_auto_pc_AWADDR = S_AXI_awaddr[31:0];
  assign m05_couplers_to_auto_pc_AWBURST = S_AXI_awburst[1:0];
  assign m05_couplers_to_auto_pc_AWCACHE = S_AXI_awcache[3:0];
  assign m05_couplers_to_auto_pc_AWID = S_AXI_awid[5:0];
  assign m05_couplers_to_auto_pc_AWLEN = S_AXI_awlen[7:0];
  assign m05_couplers_to_auto_pc_AWLOCK = S_AXI_awlock[0];
  assign m05_couplers_to_auto_pc_AWPROT = S_AXI_awprot[2:0];
  assign m05_couplers_to_auto_pc_AWQOS = S_AXI_awqos[3:0];
  assign m05_couplers_to_auto_pc_AWREGION = S_AXI_awregion[3:0];
  assign m05_couplers_to_auto_pc_AWSIZE = S_AXI_awsize[2:0];
  assign m05_couplers_to_auto_pc_AWVALID = S_AXI_awvalid;
  assign m05_couplers_to_auto_pc_BREADY = S_AXI_bready;
  assign m05_couplers_to_auto_pc_RREADY = S_AXI_rready;
  assign m05_couplers_to_auto_pc_WDATA = S_AXI_wdata[31:0];
  assign m05_couplers_to_auto_pc_WLAST = S_AXI_wlast;
  assign m05_couplers_to_auto_pc_WSTRB = S_AXI_wstrb[3:0];
  assign m05_couplers_to_auto_pc_WVALID = S_AXI_wvalid;
  gpn_bundle_block_auto_pc_3 auto_pc
       (.aclk(S_ACLK_1),
        .aresetn(S_ARESETN_1),
        .m_axi_araddr(auto_pc_to_m05_couplers_ARADDR),
        .m_axi_arready(auto_pc_to_m05_couplers_ARREADY),
        .m_axi_arvalid(auto_pc_to_m05_couplers_ARVALID),
        .m_axi_awaddr(auto_pc_to_m05_couplers_AWADDR),
        .m_axi_awready(auto_pc_to_m05_couplers_AWREADY),
        .m_axi_awvalid(auto_pc_to_m05_couplers_AWVALID),
        .m_axi_bready(auto_pc_to_m05_couplers_BREADY),
        .m_axi_bresp(auto_pc_to_m05_couplers_BRESP),
        .m_axi_bvalid(auto_pc_to_m05_couplers_BVALID),
        .m_axi_rdata(auto_pc_to_m05_couplers_RDATA),
        .m_axi_rready(auto_pc_to_m05_couplers_RREADY),
        .m_axi_rresp(auto_pc_to_m05_couplers_RRESP),
        .m_axi_rvalid(auto_pc_to_m05_couplers_RVALID),
        .m_axi_wdata(auto_pc_to_m05_couplers_WDATA),
        .m_axi_wready(auto_pc_to_m05_couplers_WREADY),
        .m_axi_wstrb(auto_pc_to_m05_couplers_WSTRB),
        .m_axi_wvalid(auto_pc_to_m05_couplers_WVALID),
        .s_axi_araddr(m05_couplers_to_auto_pc_ARADDR),
        .s_axi_arburst(m05_couplers_to_auto_pc_ARBURST),
        .s_axi_arcache(m05_couplers_to_auto_pc_ARCACHE),
        .s_axi_arid(m05_couplers_to_auto_pc_ARID),
        .s_axi_arlen(m05_couplers_to_auto_pc_ARLEN),
        .s_axi_arlock(m05_couplers_to_auto_pc_ARLOCK),
        .s_axi_arprot(m05_couplers_to_auto_pc_ARPROT),
        .s_axi_arqos(m05_couplers_to_auto_pc_ARQOS),
        .s_axi_arready(m05_couplers_to_auto_pc_ARREADY),
        .s_axi_arregion(m05_couplers_to_auto_pc_ARREGION),
        .s_axi_arsize(m05_couplers_to_auto_pc_ARSIZE),
        .s_axi_arvalid(m05_couplers_to_auto_pc_ARVALID),
        .s_axi_awaddr(m05_couplers_to_auto_pc_AWADDR),
        .s_axi_awburst(m05_couplers_to_auto_pc_AWBURST),
        .s_axi_awcache(m05_couplers_to_auto_pc_AWCACHE),
        .s_axi_awid(m05_couplers_to_auto_pc_AWID),
        .s_axi_awlen(m05_couplers_to_auto_pc_AWLEN),
        .s_axi_awlock(m05_couplers_to_auto_pc_AWLOCK),
        .s_axi_awprot(m05_couplers_to_auto_pc_AWPROT),
        .s_axi_awqos(m05_couplers_to_auto_pc_AWQOS),
        .s_axi_awready(m05_couplers_to_auto_pc_AWREADY),
        .s_axi_awregion(m05_couplers_to_auto_pc_AWREGION),
        .s_axi_awsize(m05_couplers_to_auto_pc_AWSIZE),
        .s_axi_awvalid(m05_couplers_to_auto_pc_AWVALID),
        .s_axi_bid(m05_couplers_to_auto_pc_BID),
        .s_axi_bready(m05_couplers_to_auto_pc_BREADY),
        .s_axi_bresp(m05_couplers_to_auto_pc_BRESP),
        .s_axi_bvalid(m05_couplers_to_auto_pc_BVALID),
        .s_axi_rdata(m05_couplers_to_auto_pc_RDATA),
        .s_axi_rid(m05_couplers_to_auto_pc_RID),
        .s_axi_rlast(m05_couplers_to_auto_pc_RLAST),
        .s_axi_rready(m05_couplers_to_auto_pc_RREADY),
        .s_axi_rresp(m05_couplers_to_auto_pc_RRESP),
        .s_axi_rvalid(m05_couplers_to_auto_pc_RVALID),
        .s_axi_wdata(m05_couplers_to_auto_pc_WDATA),
        .s_axi_wlast(m05_couplers_to_auto_pc_WLAST),
        .s_axi_wready(m05_couplers_to_auto_pc_WREADY),
        .s_axi_wstrb(m05_couplers_to_auto_pc_WSTRB),
        .s_axi_wvalid(m05_couplers_to_auto_pc_WVALID));
endmodule

module m06_couplers_imp_1XX6Y5S
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arid,
    S_AXI_arlen,
    S_AXI_arlock,
    S_AXI_arprot,
    S_AXI_arqos,
    S_AXI_arready,
    S_AXI_arregion,
    S_AXI_arsize,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awid,
    S_AXI_awlen,
    S_AXI_awlock,
    S_AXI_awprot,
    S_AXI_awqos,
    S_AXI_awready,
    S_AXI_awregion,
    S_AXI_awsize,
    S_AXI_awvalid,
    S_AXI_bid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rid,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  input M_AXI_arready;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  input M_AXI_awready;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [5:0]S_AXI_arid;
  input [7:0]S_AXI_arlen;
  input [0:0]S_AXI_arlock;
  input [2:0]S_AXI_arprot;
  input [3:0]S_AXI_arqos;
  output S_AXI_arready;
  input [3:0]S_AXI_arregion;
  input [2:0]S_AXI_arsize;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [5:0]S_AXI_awid;
  input [7:0]S_AXI_awlen;
  input [0:0]S_AXI_awlock;
  input [2:0]S_AXI_awprot;
  input [3:0]S_AXI_awqos;
  output S_AXI_awready;
  input [3:0]S_AXI_awregion;
  input [2:0]S_AXI_awsize;
  input S_AXI_awvalid;
  output [5:0]S_AXI_bid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  output [5:0]S_AXI_rid;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  input S_AXI_wlast;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire S_ACLK_1;
  wire S_ARESETN_1;
  wire [31:0]auto_pc_to_m06_couplers_ARADDR;
  wire auto_pc_to_m06_couplers_ARREADY;
  wire auto_pc_to_m06_couplers_ARVALID;
  wire [31:0]auto_pc_to_m06_couplers_AWADDR;
  wire auto_pc_to_m06_couplers_AWREADY;
  wire auto_pc_to_m06_couplers_AWVALID;
  wire auto_pc_to_m06_couplers_BREADY;
  wire [1:0]auto_pc_to_m06_couplers_BRESP;
  wire auto_pc_to_m06_couplers_BVALID;
  wire [31:0]auto_pc_to_m06_couplers_RDATA;
  wire auto_pc_to_m06_couplers_RREADY;
  wire [1:0]auto_pc_to_m06_couplers_RRESP;
  wire auto_pc_to_m06_couplers_RVALID;
  wire [31:0]auto_pc_to_m06_couplers_WDATA;
  wire auto_pc_to_m06_couplers_WREADY;
  wire [3:0]auto_pc_to_m06_couplers_WSTRB;
  wire auto_pc_to_m06_couplers_WVALID;
  wire [31:0]m06_couplers_to_auto_pc_ARADDR;
  wire [1:0]m06_couplers_to_auto_pc_ARBURST;
  wire [3:0]m06_couplers_to_auto_pc_ARCACHE;
  wire [5:0]m06_couplers_to_auto_pc_ARID;
  wire [7:0]m06_couplers_to_auto_pc_ARLEN;
  wire [0:0]m06_couplers_to_auto_pc_ARLOCK;
  wire [2:0]m06_couplers_to_auto_pc_ARPROT;
  wire [3:0]m06_couplers_to_auto_pc_ARQOS;
  wire m06_couplers_to_auto_pc_ARREADY;
  wire [3:0]m06_couplers_to_auto_pc_ARREGION;
  wire [2:0]m06_couplers_to_auto_pc_ARSIZE;
  wire m06_couplers_to_auto_pc_ARVALID;
  wire [31:0]m06_couplers_to_auto_pc_AWADDR;
  wire [1:0]m06_couplers_to_auto_pc_AWBURST;
  wire [3:0]m06_couplers_to_auto_pc_AWCACHE;
  wire [5:0]m06_couplers_to_auto_pc_AWID;
  wire [7:0]m06_couplers_to_auto_pc_AWLEN;
  wire [0:0]m06_couplers_to_auto_pc_AWLOCK;
  wire [2:0]m06_couplers_to_auto_pc_AWPROT;
  wire [3:0]m06_couplers_to_auto_pc_AWQOS;
  wire m06_couplers_to_auto_pc_AWREADY;
  wire [3:0]m06_couplers_to_auto_pc_AWREGION;
  wire [2:0]m06_couplers_to_auto_pc_AWSIZE;
  wire m06_couplers_to_auto_pc_AWVALID;
  wire [5:0]m06_couplers_to_auto_pc_BID;
  wire m06_couplers_to_auto_pc_BREADY;
  wire [1:0]m06_couplers_to_auto_pc_BRESP;
  wire m06_couplers_to_auto_pc_BVALID;
  wire [31:0]m06_couplers_to_auto_pc_RDATA;
  wire [5:0]m06_couplers_to_auto_pc_RID;
  wire m06_couplers_to_auto_pc_RLAST;
  wire m06_couplers_to_auto_pc_RREADY;
  wire [1:0]m06_couplers_to_auto_pc_RRESP;
  wire m06_couplers_to_auto_pc_RVALID;
  wire [31:0]m06_couplers_to_auto_pc_WDATA;
  wire m06_couplers_to_auto_pc_WLAST;
  wire m06_couplers_to_auto_pc_WREADY;
  wire [3:0]m06_couplers_to_auto_pc_WSTRB;
  wire m06_couplers_to_auto_pc_WVALID;

  assign M_AXI_araddr[31:0] = auto_pc_to_m06_couplers_ARADDR;
  assign M_AXI_arvalid = auto_pc_to_m06_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = auto_pc_to_m06_couplers_AWADDR;
  assign M_AXI_awvalid = auto_pc_to_m06_couplers_AWVALID;
  assign M_AXI_bready = auto_pc_to_m06_couplers_BREADY;
  assign M_AXI_rready = auto_pc_to_m06_couplers_RREADY;
  assign M_AXI_wdata[31:0] = auto_pc_to_m06_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = auto_pc_to_m06_couplers_WSTRB;
  assign M_AXI_wvalid = auto_pc_to_m06_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN;
  assign S_AXI_arready = m06_couplers_to_auto_pc_ARREADY;
  assign S_AXI_awready = m06_couplers_to_auto_pc_AWREADY;
  assign S_AXI_bid[5:0] = m06_couplers_to_auto_pc_BID;
  assign S_AXI_bresp[1:0] = m06_couplers_to_auto_pc_BRESP;
  assign S_AXI_bvalid = m06_couplers_to_auto_pc_BVALID;
  assign S_AXI_rdata[31:0] = m06_couplers_to_auto_pc_RDATA;
  assign S_AXI_rid[5:0] = m06_couplers_to_auto_pc_RID;
  assign S_AXI_rlast = m06_couplers_to_auto_pc_RLAST;
  assign S_AXI_rresp[1:0] = m06_couplers_to_auto_pc_RRESP;
  assign S_AXI_rvalid = m06_couplers_to_auto_pc_RVALID;
  assign S_AXI_wready = m06_couplers_to_auto_pc_WREADY;
  assign auto_pc_to_m06_couplers_ARREADY = M_AXI_arready;
  assign auto_pc_to_m06_couplers_AWREADY = M_AXI_awready;
  assign auto_pc_to_m06_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_pc_to_m06_couplers_BVALID = M_AXI_bvalid;
  assign auto_pc_to_m06_couplers_RDATA = M_AXI_rdata[31:0];
  assign auto_pc_to_m06_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_pc_to_m06_couplers_RVALID = M_AXI_rvalid;
  assign auto_pc_to_m06_couplers_WREADY = M_AXI_wready;
  assign m06_couplers_to_auto_pc_ARADDR = S_AXI_araddr[31:0];
  assign m06_couplers_to_auto_pc_ARBURST = S_AXI_arburst[1:0];
  assign m06_couplers_to_auto_pc_ARCACHE = S_AXI_arcache[3:0];
  assign m06_couplers_to_auto_pc_ARID = S_AXI_arid[5:0];
  assign m06_couplers_to_auto_pc_ARLEN = S_AXI_arlen[7:0];
  assign m06_couplers_to_auto_pc_ARLOCK = S_AXI_arlock[0];
  assign m06_couplers_to_auto_pc_ARPROT = S_AXI_arprot[2:0];
  assign m06_couplers_to_auto_pc_ARQOS = S_AXI_arqos[3:0];
  assign m06_couplers_to_auto_pc_ARREGION = S_AXI_arregion[3:0];
  assign m06_couplers_to_auto_pc_ARSIZE = S_AXI_arsize[2:0];
  assign m06_couplers_to_auto_pc_ARVALID = S_AXI_arvalid;
  assign m06_couplers_to_auto_pc_AWADDR = S_AXI_awaddr[31:0];
  assign m06_couplers_to_auto_pc_AWBURST = S_AXI_awburst[1:0];
  assign m06_couplers_to_auto_pc_AWCACHE = S_AXI_awcache[3:0];
  assign m06_couplers_to_auto_pc_AWID = S_AXI_awid[5:0];
  assign m06_couplers_to_auto_pc_AWLEN = S_AXI_awlen[7:0];
  assign m06_couplers_to_auto_pc_AWLOCK = S_AXI_awlock[0];
  assign m06_couplers_to_auto_pc_AWPROT = S_AXI_awprot[2:0];
  assign m06_couplers_to_auto_pc_AWQOS = S_AXI_awqos[3:0];
  assign m06_couplers_to_auto_pc_AWREGION = S_AXI_awregion[3:0];
  assign m06_couplers_to_auto_pc_AWSIZE = S_AXI_awsize[2:0];
  assign m06_couplers_to_auto_pc_AWVALID = S_AXI_awvalid;
  assign m06_couplers_to_auto_pc_BREADY = S_AXI_bready;
  assign m06_couplers_to_auto_pc_RREADY = S_AXI_rready;
  assign m06_couplers_to_auto_pc_WDATA = S_AXI_wdata[31:0];
  assign m06_couplers_to_auto_pc_WLAST = S_AXI_wlast;
  assign m06_couplers_to_auto_pc_WSTRB = S_AXI_wstrb[3:0];
  assign m06_couplers_to_auto_pc_WVALID = S_AXI_wvalid;
  gpn_bundle_block_auto_pc_4 auto_pc
       (.aclk(S_ACLK_1),
        .aresetn(S_ARESETN_1),
        .m_axi_araddr(auto_pc_to_m06_couplers_ARADDR),
        .m_axi_arready(auto_pc_to_m06_couplers_ARREADY),
        .m_axi_arvalid(auto_pc_to_m06_couplers_ARVALID),
        .m_axi_awaddr(auto_pc_to_m06_couplers_AWADDR),
        .m_axi_awready(auto_pc_to_m06_couplers_AWREADY),
        .m_axi_awvalid(auto_pc_to_m06_couplers_AWVALID),
        .m_axi_bready(auto_pc_to_m06_couplers_BREADY),
        .m_axi_bresp(auto_pc_to_m06_couplers_BRESP),
        .m_axi_bvalid(auto_pc_to_m06_couplers_BVALID),
        .m_axi_rdata(auto_pc_to_m06_couplers_RDATA),
        .m_axi_rready(auto_pc_to_m06_couplers_RREADY),
        .m_axi_rresp(auto_pc_to_m06_couplers_RRESP),
        .m_axi_rvalid(auto_pc_to_m06_couplers_RVALID),
        .m_axi_wdata(auto_pc_to_m06_couplers_WDATA),
        .m_axi_wready(auto_pc_to_m06_couplers_WREADY),
        .m_axi_wstrb(auto_pc_to_m06_couplers_WSTRB),
        .m_axi_wvalid(auto_pc_to_m06_couplers_WVALID),
        .s_axi_araddr(m06_couplers_to_auto_pc_ARADDR),
        .s_axi_arburst(m06_couplers_to_auto_pc_ARBURST),
        .s_axi_arcache(m06_couplers_to_auto_pc_ARCACHE),
        .s_axi_arid(m06_couplers_to_auto_pc_ARID),
        .s_axi_arlen(m06_couplers_to_auto_pc_ARLEN),
        .s_axi_arlock(m06_couplers_to_auto_pc_ARLOCK),
        .s_axi_arprot(m06_couplers_to_auto_pc_ARPROT),
        .s_axi_arqos(m06_couplers_to_auto_pc_ARQOS),
        .s_axi_arready(m06_couplers_to_auto_pc_ARREADY),
        .s_axi_arregion(m06_couplers_to_auto_pc_ARREGION),
        .s_axi_arsize(m06_couplers_to_auto_pc_ARSIZE),
        .s_axi_arvalid(m06_couplers_to_auto_pc_ARVALID),
        .s_axi_awaddr(m06_couplers_to_auto_pc_AWADDR),
        .s_axi_awburst(m06_couplers_to_auto_pc_AWBURST),
        .s_axi_awcache(m06_couplers_to_auto_pc_AWCACHE),
        .s_axi_awid(m06_couplers_to_auto_pc_AWID),
        .s_axi_awlen(m06_couplers_to_auto_pc_AWLEN),
        .s_axi_awlock(m06_couplers_to_auto_pc_AWLOCK),
        .s_axi_awprot(m06_couplers_to_auto_pc_AWPROT),
        .s_axi_awqos(m06_couplers_to_auto_pc_AWQOS),
        .s_axi_awready(m06_couplers_to_auto_pc_AWREADY),
        .s_axi_awregion(m06_couplers_to_auto_pc_AWREGION),
        .s_axi_awsize(m06_couplers_to_auto_pc_AWSIZE),
        .s_axi_awvalid(m06_couplers_to_auto_pc_AWVALID),
        .s_axi_bid(m06_couplers_to_auto_pc_BID),
        .s_axi_bready(m06_couplers_to_auto_pc_BREADY),
        .s_axi_bresp(m06_couplers_to_auto_pc_BRESP),
        .s_axi_bvalid(m06_couplers_to_auto_pc_BVALID),
        .s_axi_rdata(m06_couplers_to_auto_pc_RDATA),
        .s_axi_rid(m06_couplers_to_auto_pc_RID),
        .s_axi_rlast(m06_couplers_to_auto_pc_RLAST),
        .s_axi_rready(m06_couplers_to_auto_pc_RREADY),
        .s_axi_rresp(m06_couplers_to_auto_pc_RRESP),
        .s_axi_rvalid(m06_couplers_to_auto_pc_RVALID),
        .s_axi_wdata(m06_couplers_to_auto_pc_WDATA),
        .s_axi_wlast(m06_couplers_to_auto_pc_WLAST),
        .s_axi_wready(m06_couplers_to_auto_pc_WREADY),
        .s_axi_wstrb(m06_couplers_to_auto_pc_WSTRB),
        .s_axi_wvalid(m06_couplers_to_auto_pc_WVALID));
endmodule

module s00_couplers_imp_154BRN3
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arburst,
    M_AXI_arcache,
    M_AXI_arid,
    M_AXI_arlen,
    M_AXI_arlock,
    M_AXI_arprot,
    M_AXI_arqos,
    M_AXI_arready,
    M_AXI_arregion,
    M_AXI_arsize,
    M_AXI_aruser,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awburst,
    M_AXI_awcache,
    M_AXI_awid,
    M_AXI_awlen,
    M_AXI_awlock,
    M_AXI_awprot,
    M_AXI_awqos,
    M_AXI_awready,
    M_AXI_awregion,
    M_AXI_awsize,
    M_AXI_awuser,
    M_AXI_awvalid,
    M_AXI_bid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_buser,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rid,
    M_AXI_rlast,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_ruser,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wlast,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wuser,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arid,
    S_AXI_arlen,
    S_AXI_arlock,
    S_AXI_arprot,
    S_AXI_arqos,
    S_AXI_arready,
    S_AXI_arregion,
    S_AXI_arsize,
    S_AXI_aruser,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awid,
    S_AXI_awlen,
    S_AXI_awlock,
    S_AXI_awprot,
    S_AXI_awqos,
    S_AXI_awready,
    S_AXI_awregion,
    S_AXI_awsize,
    S_AXI_awuser,
    S_AXI_awvalid,
    S_AXI_bid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_buser,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rid,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_ruser,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wuser,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [1:0]M_AXI_arburst;
  output [3:0]M_AXI_arcache;
  output [5:0]M_AXI_arid;
  output [7:0]M_AXI_arlen;
  output [0:0]M_AXI_arlock;
  output [2:0]M_AXI_arprot;
  output [3:0]M_AXI_arqos;
  input M_AXI_arready;
  output [3:0]M_AXI_arregion;
  output [2:0]M_AXI_arsize;
  output [3:0]M_AXI_aruser;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [1:0]M_AXI_awburst;
  output [3:0]M_AXI_awcache;
  output [5:0]M_AXI_awid;
  output [7:0]M_AXI_awlen;
  output [0:0]M_AXI_awlock;
  output [2:0]M_AXI_awprot;
  output [3:0]M_AXI_awqos;
  input M_AXI_awready;
  output [3:0]M_AXI_awregion;
  output [2:0]M_AXI_awsize;
  output [3:0]M_AXI_awuser;
  output M_AXI_awvalid;
  input [5:0]M_AXI_bid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input [3:0]M_AXI_buser;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  input [5:0]M_AXI_rid;
  input M_AXI_rlast;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input [3:0]M_AXI_ruser;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  output M_AXI_wlast;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output [3:0]M_AXI_wuser;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [5:0]S_AXI_arid;
  input [7:0]S_AXI_arlen;
  input [0:0]S_AXI_arlock;
  input [2:0]S_AXI_arprot;
  input [3:0]S_AXI_arqos;
  output [0:0]S_AXI_arready;
  input [3:0]S_AXI_arregion;
  input [2:0]S_AXI_arsize;
  input [3:0]S_AXI_aruser;
  input [0:0]S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [5:0]S_AXI_awid;
  input [7:0]S_AXI_awlen;
  input [0:0]S_AXI_awlock;
  input [2:0]S_AXI_awprot;
  input [3:0]S_AXI_awqos;
  output [0:0]S_AXI_awready;
  input [3:0]S_AXI_awregion;
  input [2:0]S_AXI_awsize;
  input [3:0]S_AXI_awuser;
  input [0:0]S_AXI_awvalid;
  output [5:0]S_AXI_bid;
  input [0:0]S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output [3:0]S_AXI_buser;
  output [0:0]S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  output [5:0]S_AXI_rid;
  output [0:0]S_AXI_rlast;
  input [0:0]S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output [3:0]S_AXI_ruser;
  output [0:0]S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  input [0:0]S_AXI_wlast;
  output [0:0]S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input [3:0]S_AXI_wuser;
  input [0:0]S_AXI_wvalid;

  wire M_ACLK_1;
  wire M_ARESETN_1;
  wire S_ACLK_1;
  wire S_ARESETN_1;
  wire [31:0]auto_cc_to_s00_couplers_ARADDR;
  wire [1:0]auto_cc_to_s00_couplers_ARBURST;
  wire [3:0]auto_cc_to_s00_couplers_ARCACHE;
  wire [5:0]auto_cc_to_s00_couplers_ARID;
  wire [7:0]auto_cc_to_s00_couplers_ARLEN;
  wire [0:0]auto_cc_to_s00_couplers_ARLOCK;
  wire [2:0]auto_cc_to_s00_couplers_ARPROT;
  wire [3:0]auto_cc_to_s00_couplers_ARQOS;
  wire auto_cc_to_s00_couplers_ARREADY;
  wire [3:0]auto_cc_to_s00_couplers_ARREGION;
  wire [2:0]auto_cc_to_s00_couplers_ARSIZE;
  wire [3:0]auto_cc_to_s00_couplers_ARUSER;
  wire auto_cc_to_s00_couplers_ARVALID;
  wire [31:0]auto_cc_to_s00_couplers_AWADDR;
  wire [1:0]auto_cc_to_s00_couplers_AWBURST;
  wire [3:0]auto_cc_to_s00_couplers_AWCACHE;
  wire [5:0]auto_cc_to_s00_couplers_AWID;
  wire [7:0]auto_cc_to_s00_couplers_AWLEN;
  wire [0:0]auto_cc_to_s00_couplers_AWLOCK;
  wire [2:0]auto_cc_to_s00_couplers_AWPROT;
  wire [3:0]auto_cc_to_s00_couplers_AWQOS;
  wire auto_cc_to_s00_couplers_AWREADY;
  wire [3:0]auto_cc_to_s00_couplers_AWREGION;
  wire [2:0]auto_cc_to_s00_couplers_AWSIZE;
  wire [3:0]auto_cc_to_s00_couplers_AWUSER;
  wire auto_cc_to_s00_couplers_AWVALID;
  wire [5:0]auto_cc_to_s00_couplers_BID;
  wire auto_cc_to_s00_couplers_BREADY;
  wire [1:0]auto_cc_to_s00_couplers_BRESP;
  wire [3:0]auto_cc_to_s00_couplers_BUSER;
  wire auto_cc_to_s00_couplers_BVALID;
  wire [31:0]auto_cc_to_s00_couplers_RDATA;
  wire [5:0]auto_cc_to_s00_couplers_RID;
  wire auto_cc_to_s00_couplers_RLAST;
  wire auto_cc_to_s00_couplers_RREADY;
  wire [1:0]auto_cc_to_s00_couplers_RRESP;
  wire [3:0]auto_cc_to_s00_couplers_RUSER;
  wire auto_cc_to_s00_couplers_RVALID;
  wire [31:0]auto_cc_to_s00_couplers_WDATA;
  wire auto_cc_to_s00_couplers_WLAST;
  wire auto_cc_to_s00_couplers_WREADY;
  wire [3:0]auto_cc_to_s00_couplers_WSTRB;
  wire [3:0]auto_cc_to_s00_couplers_WUSER;
  wire auto_cc_to_s00_couplers_WVALID;
  wire [31:0]s00_couplers_to_auto_cc_ARADDR;
  wire [1:0]s00_couplers_to_auto_cc_ARBURST;
  wire [3:0]s00_couplers_to_auto_cc_ARCACHE;
  wire [5:0]s00_couplers_to_auto_cc_ARID;
  wire [7:0]s00_couplers_to_auto_cc_ARLEN;
  wire [0:0]s00_couplers_to_auto_cc_ARLOCK;
  wire [2:0]s00_couplers_to_auto_cc_ARPROT;
  wire [3:0]s00_couplers_to_auto_cc_ARQOS;
  wire s00_couplers_to_auto_cc_ARREADY;
  wire [3:0]s00_couplers_to_auto_cc_ARREGION;
  wire [2:0]s00_couplers_to_auto_cc_ARSIZE;
  wire [3:0]s00_couplers_to_auto_cc_ARUSER;
  wire [0:0]s00_couplers_to_auto_cc_ARVALID;
  wire [31:0]s00_couplers_to_auto_cc_AWADDR;
  wire [1:0]s00_couplers_to_auto_cc_AWBURST;
  wire [3:0]s00_couplers_to_auto_cc_AWCACHE;
  wire [5:0]s00_couplers_to_auto_cc_AWID;
  wire [7:0]s00_couplers_to_auto_cc_AWLEN;
  wire [0:0]s00_couplers_to_auto_cc_AWLOCK;
  wire [2:0]s00_couplers_to_auto_cc_AWPROT;
  wire [3:0]s00_couplers_to_auto_cc_AWQOS;
  wire s00_couplers_to_auto_cc_AWREADY;
  wire [3:0]s00_couplers_to_auto_cc_AWREGION;
  wire [2:0]s00_couplers_to_auto_cc_AWSIZE;
  wire [3:0]s00_couplers_to_auto_cc_AWUSER;
  wire [0:0]s00_couplers_to_auto_cc_AWVALID;
  wire [5:0]s00_couplers_to_auto_cc_BID;
  wire [0:0]s00_couplers_to_auto_cc_BREADY;
  wire [1:0]s00_couplers_to_auto_cc_BRESP;
  wire [3:0]s00_couplers_to_auto_cc_BUSER;
  wire s00_couplers_to_auto_cc_BVALID;
  wire [31:0]s00_couplers_to_auto_cc_RDATA;
  wire [5:0]s00_couplers_to_auto_cc_RID;
  wire s00_couplers_to_auto_cc_RLAST;
  wire [0:0]s00_couplers_to_auto_cc_RREADY;
  wire [1:0]s00_couplers_to_auto_cc_RRESP;
  wire [3:0]s00_couplers_to_auto_cc_RUSER;
  wire s00_couplers_to_auto_cc_RVALID;
  wire [31:0]s00_couplers_to_auto_cc_WDATA;
  wire [0:0]s00_couplers_to_auto_cc_WLAST;
  wire s00_couplers_to_auto_cc_WREADY;
  wire [3:0]s00_couplers_to_auto_cc_WSTRB;
  wire [3:0]s00_couplers_to_auto_cc_WUSER;
  wire [0:0]s00_couplers_to_auto_cc_WVALID;

  assign M_ACLK_1 = M_ACLK;
  assign M_ARESETN_1 = M_ARESETN;
  assign M_AXI_araddr[31:0] = auto_cc_to_s00_couplers_ARADDR;
  assign M_AXI_arburst[1:0] = auto_cc_to_s00_couplers_ARBURST;
  assign M_AXI_arcache[3:0] = auto_cc_to_s00_couplers_ARCACHE;
  assign M_AXI_arid[5:0] = auto_cc_to_s00_couplers_ARID;
  assign M_AXI_arlen[7:0] = auto_cc_to_s00_couplers_ARLEN;
  assign M_AXI_arlock[0] = auto_cc_to_s00_couplers_ARLOCK;
  assign M_AXI_arprot[2:0] = auto_cc_to_s00_couplers_ARPROT;
  assign M_AXI_arqos[3:0] = auto_cc_to_s00_couplers_ARQOS;
  assign M_AXI_arregion[3:0] = auto_cc_to_s00_couplers_ARREGION;
  assign M_AXI_arsize[2:0] = auto_cc_to_s00_couplers_ARSIZE;
  assign M_AXI_aruser[3:0] = auto_cc_to_s00_couplers_ARUSER;
  assign M_AXI_arvalid = auto_cc_to_s00_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = auto_cc_to_s00_couplers_AWADDR;
  assign M_AXI_awburst[1:0] = auto_cc_to_s00_couplers_AWBURST;
  assign M_AXI_awcache[3:0] = auto_cc_to_s00_couplers_AWCACHE;
  assign M_AXI_awid[5:0] = auto_cc_to_s00_couplers_AWID;
  assign M_AXI_awlen[7:0] = auto_cc_to_s00_couplers_AWLEN;
  assign M_AXI_awlock[0] = auto_cc_to_s00_couplers_AWLOCK;
  assign M_AXI_awprot[2:0] = auto_cc_to_s00_couplers_AWPROT;
  assign M_AXI_awqos[3:0] = auto_cc_to_s00_couplers_AWQOS;
  assign M_AXI_awregion[3:0] = auto_cc_to_s00_couplers_AWREGION;
  assign M_AXI_awsize[2:0] = auto_cc_to_s00_couplers_AWSIZE;
  assign M_AXI_awuser[3:0] = auto_cc_to_s00_couplers_AWUSER;
  assign M_AXI_awvalid = auto_cc_to_s00_couplers_AWVALID;
  assign M_AXI_bready = auto_cc_to_s00_couplers_BREADY;
  assign M_AXI_rready = auto_cc_to_s00_couplers_RREADY;
  assign M_AXI_wdata[31:0] = auto_cc_to_s00_couplers_WDATA;
  assign M_AXI_wlast = auto_cc_to_s00_couplers_WLAST;
  assign M_AXI_wstrb[3:0] = auto_cc_to_s00_couplers_WSTRB;
  assign M_AXI_wuser[3:0] = auto_cc_to_s00_couplers_WUSER;
  assign M_AXI_wvalid = auto_cc_to_s00_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN;
  assign S_AXI_arready[0] = s00_couplers_to_auto_cc_ARREADY;
  assign S_AXI_awready[0] = s00_couplers_to_auto_cc_AWREADY;
  assign S_AXI_bid[5:0] = s00_couplers_to_auto_cc_BID;
  assign S_AXI_bresp[1:0] = s00_couplers_to_auto_cc_BRESP;
  assign S_AXI_buser[3:0] = s00_couplers_to_auto_cc_BUSER;
  assign S_AXI_bvalid[0] = s00_couplers_to_auto_cc_BVALID;
  assign S_AXI_rdata[31:0] = s00_couplers_to_auto_cc_RDATA;
  assign S_AXI_rid[5:0] = s00_couplers_to_auto_cc_RID;
  assign S_AXI_rlast[0] = s00_couplers_to_auto_cc_RLAST;
  assign S_AXI_rresp[1:0] = s00_couplers_to_auto_cc_RRESP;
  assign S_AXI_ruser[3:0] = s00_couplers_to_auto_cc_RUSER;
  assign S_AXI_rvalid[0] = s00_couplers_to_auto_cc_RVALID;
  assign S_AXI_wready[0] = s00_couplers_to_auto_cc_WREADY;
  assign auto_cc_to_s00_couplers_ARREADY = M_AXI_arready;
  assign auto_cc_to_s00_couplers_AWREADY = M_AXI_awready;
  assign auto_cc_to_s00_couplers_BID = M_AXI_bid[5:0];
  assign auto_cc_to_s00_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_cc_to_s00_couplers_BUSER = M_AXI_buser[3:0];
  assign auto_cc_to_s00_couplers_BVALID = M_AXI_bvalid;
  assign auto_cc_to_s00_couplers_RDATA = M_AXI_rdata[31:0];
  assign auto_cc_to_s00_couplers_RID = M_AXI_rid[5:0];
  assign auto_cc_to_s00_couplers_RLAST = M_AXI_rlast;
  assign auto_cc_to_s00_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_cc_to_s00_couplers_RUSER = M_AXI_ruser[3:0];
  assign auto_cc_to_s00_couplers_RVALID = M_AXI_rvalid;
  assign auto_cc_to_s00_couplers_WREADY = M_AXI_wready;
  assign s00_couplers_to_auto_cc_ARADDR = S_AXI_araddr[31:0];
  assign s00_couplers_to_auto_cc_ARBURST = S_AXI_arburst[1:0];
  assign s00_couplers_to_auto_cc_ARCACHE = S_AXI_arcache[3:0];
  assign s00_couplers_to_auto_cc_ARID = S_AXI_arid[5:0];
  assign s00_couplers_to_auto_cc_ARLEN = S_AXI_arlen[7:0];
  assign s00_couplers_to_auto_cc_ARLOCK = S_AXI_arlock[0];
  assign s00_couplers_to_auto_cc_ARPROT = S_AXI_arprot[2:0];
  assign s00_couplers_to_auto_cc_ARQOS = S_AXI_arqos[3:0];
  assign s00_couplers_to_auto_cc_ARREGION = S_AXI_arregion[3:0];
  assign s00_couplers_to_auto_cc_ARSIZE = S_AXI_arsize[2:0];
  assign s00_couplers_to_auto_cc_ARUSER = S_AXI_aruser[3:0];
  assign s00_couplers_to_auto_cc_ARVALID = S_AXI_arvalid[0];
  assign s00_couplers_to_auto_cc_AWADDR = S_AXI_awaddr[31:0];
  assign s00_couplers_to_auto_cc_AWBURST = S_AXI_awburst[1:0];
  assign s00_couplers_to_auto_cc_AWCACHE = S_AXI_awcache[3:0];
  assign s00_couplers_to_auto_cc_AWID = S_AXI_awid[5:0];
  assign s00_couplers_to_auto_cc_AWLEN = S_AXI_awlen[7:0];
  assign s00_couplers_to_auto_cc_AWLOCK = S_AXI_awlock[0];
  assign s00_couplers_to_auto_cc_AWPROT = S_AXI_awprot[2:0];
  assign s00_couplers_to_auto_cc_AWQOS = S_AXI_awqos[3:0];
  assign s00_couplers_to_auto_cc_AWREGION = S_AXI_awregion[3:0];
  assign s00_couplers_to_auto_cc_AWSIZE = S_AXI_awsize[2:0];
  assign s00_couplers_to_auto_cc_AWUSER = S_AXI_awuser[3:0];
  assign s00_couplers_to_auto_cc_AWVALID = S_AXI_awvalid[0];
  assign s00_couplers_to_auto_cc_BREADY = S_AXI_bready[0];
  assign s00_couplers_to_auto_cc_RREADY = S_AXI_rready[0];
  assign s00_couplers_to_auto_cc_WDATA = S_AXI_wdata[31:0];
  assign s00_couplers_to_auto_cc_WLAST = S_AXI_wlast[0];
  assign s00_couplers_to_auto_cc_WSTRB = S_AXI_wstrb[3:0];
  assign s00_couplers_to_auto_cc_WUSER = S_AXI_wuser[3:0];
  assign s00_couplers_to_auto_cc_WVALID = S_AXI_wvalid[0];
  gpn_bundle_block_auto_cc_0 auto_cc
       (.m_axi_aclk(M_ACLK_1),
        .m_axi_araddr(auto_cc_to_s00_couplers_ARADDR),
        .m_axi_arburst(auto_cc_to_s00_couplers_ARBURST),
        .m_axi_arcache(auto_cc_to_s00_couplers_ARCACHE),
        .m_axi_aresetn(M_ARESETN_1),
        .m_axi_arid(auto_cc_to_s00_couplers_ARID),
        .m_axi_arlen(auto_cc_to_s00_couplers_ARLEN),
        .m_axi_arlock(auto_cc_to_s00_couplers_ARLOCK),
        .m_axi_arprot(auto_cc_to_s00_couplers_ARPROT),
        .m_axi_arqos(auto_cc_to_s00_couplers_ARQOS),
        .m_axi_arready(auto_cc_to_s00_couplers_ARREADY),
        .m_axi_arregion(auto_cc_to_s00_couplers_ARREGION),
        .m_axi_arsize(auto_cc_to_s00_couplers_ARSIZE),
        .m_axi_aruser(auto_cc_to_s00_couplers_ARUSER),
        .m_axi_arvalid(auto_cc_to_s00_couplers_ARVALID),
        .m_axi_awaddr(auto_cc_to_s00_couplers_AWADDR),
        .m_axi_awburst(auto_cc_to_s00_couplers_AWBURST),
        .m_axi_awcache(auto_cc_to_s00_couplers_AWCACHE),
        .m_axi_awid(auto_cc_to_s00_couplers_AWID),
        .m_axi_awlen(auto_cc_to_s00_couplers_AWLEN),
        .m_axi_awlock(auto_cc_to_s00_couplers_AWLOCK),
        .m_axi_awprot(auto_cc_to_s00_couplers_AWPROT),
        .m_axi_awqos(auto_cc_to_s00_couplers_AWQOS),
        .m_axi_awready(auto_cc_to_s00_couplers_AWREADY),
        .m_axi_awregion(auto_cc_to_s00_couplers_AWREGION),
        .m_axi_awsize(auto_cc_to_s00_couplers_AWSIZE),
        .m_axi_awuser(auto_cc_to_s00_couplers_AWUSER),
        .m_axi_awvalid(auto_cc_to_s00_couplers_AWVALID),
        .m_axi_bid(auto_cc_to_s00_couplers_BID),
        .m_axi_bready(auto_cc_to_s00_couplers_BREADY),
        .m_axi_bresp(auto_cc_to_s00_couplers_BRESP),
        .m_axi_buser(auto_cc_to_s00_couplers_BUSER),
        .m_axi_bvalid(auto_cc_to_s00_couplers_BVALID),
        .m_axi_rdata(auto_cc_to_s00_couplers_RDATA),
        .m_axi_rid(auto_cc_to_s00_couplers_RID),
        .m_axi_rlast(auto_cc_to_s00_couplers_RLAST),
        .m_axi_rready(auto_cc_to_s00_couplers_RREADY),
        .m_axi_rresp(auto_cc_to_s00_couplers_RRESP),
        .m_axi_ruser(auto_cc_to_s00_couplers_RUSER),
        .m_axi_rvalid(auto_cc_to_s00_couplers_RVALID),
        .m_axi_wdata(auto_cc_to_s00_couplers_WDATA),
        .m_axi_wlast(auto_cc_to_s00_couplers_WLAST),
        .m_axi_wready(auto_cc_to_s00_couplers_WREADY),
        .m_axi_wstrb(auto_cc_to_s00_couplers_WSTRB),
        .m_axi_wuser(auto_cc_to_s00_couplers_WUSER),
        .m_axi_wvalid(auto_cc_to_s00_couplers_WVALID),
        .s_axi_aclk(S_ACLK_1),
        .s_axi_araddr(s00_couplers_to_auto_cc_ARADDR),
        .s_axi_arburst(s00_couplers_to_auto_cc_ARBURST),
        .s_axi_arcache(s00_couplers_to_auto_cc_ARCACHE),
        .s_axi_aresetn(S_ARESETN_1),
        .s_axi_arid(s00_couplers_to_auto_cc_ARID),
        .s_axi_arlen(s00_couplers_to_auto_cc_ARLEN),
        .s_axi_arlock(s00_couplers_to_auto_cc_ARLOCK),
        .s_axi_arprot(s00_couplers_to_auto_cc_ARPROT),
        .s_axi_arqos(s00_couplers_to_auto_cc_ARQOS),
        .s_axi_arready(s00_couplers_to_auto_cc_ARREADY),
        .s_axi_arregion(s00_couplers_to_auto_cc_ARREGION),
        .s_axi_arsize(s00_couplers_to_auto_cc_ARSIZE),
        .s_axi_aruser(s00_couplers_to_auto_cc_ARUSER),
        .s_axi_arvalid(s00_couplers_to_auto_cc_ARVALID),
        .s_axi_awaddr(s00_couplers_to_auto_cc_AWADDR),
        .s_axi_awburst(s00_couplers_to_auto_cc_AWBURST),
        .s_axi_awcache(s00_couplers_to_auto_cc_AWCACHE),
        .s_axi_awid(s00_couplers_to_auto_cc_AWID),
        .s_axi_awlen(s00_couplers_to_auto_cc_AWLEN),
        .s_axi_awlock(s00_couplers_to_auto_cc_AWLOCK),
        .s_axi_awprot(s00_couplers_to_auto_cc_AWPROT),
        .s_axi_awqos(s00_couplers_to_auto_cc_AWQOS),
        .s_axi_awready(s00_couplers_to_auto_cc_AWREADY),
        .s_axi_awregion(s00_couplers_to_auto_cc_AWREGION),
        .s_axi_awsize(s00_couplers_to_auto_cc_AWSIZE),
        .s_axi_awuser(s00_couplers_to_auto_cc_AWUSER),
        .s_axi_awvalid(s00_couplers_to_auto_cc_AWVALID),
        .s_axi_bid(s00_couplers_to_auto_cc_BID),
        .s_axi_bready(s00_couplers_to_auto_cc_BREADY),
        .s_axi_bresp(s00_couplers_to_auto_cc_BRESP),
        .s_axi_buser(s00_couplers_to_auto_cc_BUSER),
        .s_axi_bvalid(s00_couplers_to_auto_cc_BVALID),
        .s_axi_rdata(s00_couplers_to_auto_cc_RDATA),
        .s_axi_rid(s00_couplers_to_auto_cc_RID),
        .s_axi_rlast(s00_couplers_to_auto_cc_RLAST),
        .s_axi_rready(s00_couplers_to_auto_cc_RREADY),
        .s_axi_rresp(s00_couplers_to_auto_cc_RRESP),
        .s_axi_ruser(s00_couplers_to_auto_cc_RUSER),
        .s_axi_rvalid(s00_couplers_to_auto_cc_RVALID),
        .s_axi_wdata(s00_couplers_to_auto_cc_WDATA),
        .s_axi_wlast(s00_couplers_to_auto_cc_WLAST),
        .s_axi_wready(s00_couplers_to_auto_cc_WREADY),
        .s_axi_wstrb(s00_couplers_to_auto_cc_WSTRB),
        .s_axi_wuser(s00_couplers_to_auto_cc_WUSER),
        .s_axi_wvalid(s00_couplers_to_auto_cc_WVALID));
endmodule

module s00_couplers_imp_1LVCP87
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arburst,
    M_AXI_arcache,
    M_AXI_arid,
    M_AXI_arlen,
    M_AXI_arlock,
    M_AXI_arprot,
    M_AXI_arqos,
    M_AXI_arready,
    M_AXI_arsize,
    M_AXI_aruser,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awburst,
    M_AXI_awcache,
    M_AXI_awid,
    M_AXI_awlen,
    M_AXI_awlock,
    M_AXI_awprot,
    M_AXI_awqos,
    M_AXI_awready,
    M_AXI_awsize,
    M_AXI_awuser,
    M_AXI_awvalid,
    M_AXI_bid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_buser,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rid,
    M_AXI_rlast,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_ruser,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wlast,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wuser,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arid,
    S_AXI_arlen,
    S_AXI_arlock,
    S_AXI_arprot,
    S_AXI_arqos,
    S_AXI_arready,
    S_AXI_arsize,
    S_AXI_aruser,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awid,
    S_AXI_awlen,
    S_AXI_awlock,
    S_AXI_awprot,
    S_AXI_awqos,
    S_AXI_awready,
    S_AXI_awsize,
    S_AXI_awuser,
    S_AXI_awvalid,
    S_AXI_bid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_buser,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rid,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_ruser,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wuser,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [1:0]M_AXI_arburst;
  output [3:0]M_AXI_arcache;
  output [5:0]M_AXI_arid;
  output [7:0]M_AXI_arlen;
  output M_AXI_arlock;
  output [2:0]M_AXI_arprot;
  output [3:0]M_AXI_arqos;
  input M_AXI_arready;
  output [2:0]M_AXI_arsize;
  output [3:0]M_AXI_aruser;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [1:0]M_AXI_awburst;
  output [3:0]M_AXI_awcache;
  output [5:0]M_AXI_awid;
  output [7:0]M_AXI_awlen;
  output M_AXI_awlock;
  output [2:0]M_AXI_awprot;
  output [3:0]M_AXI_awqos;
  input M_AXI_awready;
  output [2:0]M_AXI_awsize;
  output [3:0]M_AXI_awuser;
  output M_AXI_awvalid;
  input [5:0]M_AXI_bid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input [3:0]M_AXI_buser;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  input [5:0]M_AXI_rid;
  input M_AXI_rlast;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input [3:0]M_AXI_ruser;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  output M_AXI_wlast;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output [3:0]M_AXI_wuser;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [5:0]S_AXI_arid;
  input [7:0]S_AXI_arlen;
  input S_AXI_arlock;
  input [2:0]S_AXI_arprot;
  input [3:0]S_AXI_arqos;
  output S_AXI_arready;
  input [2:0]S_AXI_arsize;
  input [3:0]S_AXI_aruser;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [5:0]S_AXI_awid;
  input [7:0]S_AXI_awlen;
  input S_AXI_awlock;
  input [2:0]S_AXI_awprot;
  input [3:0]S_AXI_awqos;
  output S_AXI_awready;
  input [2:0]S_AXI_awsize;
  input [3:0]S_AXI_awuser;
  input S_AXI_awvalid;
  output [5:0]S_AXI_bid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output [3:0]S_AXI_buser;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  output [5:0]S_AXI_rid;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output [3:0]S_AXI_ruser;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  input S_AXI_wlast;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input [3:0]S_AXI_wuser;
  input S_AXI_wvalid;

  wire [31:0]s00_couplers_to_s00_couplers_ARADDR;
  wire [1:0]s00_couplers_to_s00_couplers_ARBURST;
  wire [3:0]s00_couplers_to_s00_couplers_ARCACHE;
  wire [5:0]s00_couplers_to_s00_couplers_ARID;
  wire [7:0]s00_couplers_to_s00_couplers_ARLEN;
  wire s00_couplers_to_s00_couplers_ARLOCK;
  wire [2:0]s00_couplers_to_s00_couplers_ARPROT;
  wire [3:0]s00_couplers_to_s00_couplers_ARQOS;
  wire s00_couplers_to_s00_couplers_ARREADY;
  wire [2:0]s00_couplers_to_s00_couplers_ARSIZE;
  wire [3:0]s00_couplers_to_s00_couplers_ARUSER;
  wire s00_couplers_to_s00_couplers_ARVALID;
  wire [31:0]s00_couplers_to_s00_couplers_AWADDR;
  wire [1:0]s00_couplers_to_s00_couplers_AWBURST;
  wire [3:0]s00_couplers_to_s00_couplers_AWCACHE;
  wire [5:0]s00_couplers_to_s00_couplers_AWID;
  wire [7:0]s00_couplers_to_s00_couplers_AWLEN;
  wire s00_couplers_to_s00_couplers_AWLOCK;
  wire [2:0]s00_couplers_to_s00_couplers_AWPROT;
  wire [3:0]s00_couplers_to_s00_couplers_AWQOS;
  wire s00_couplers_to_s00_couplers_AWREADY;
  wire [2:0]s00_couplers_to_s00_couplers_AWSIZE;
  wire [3:0]s00_couplers_to_s00_couplers_AWUSER;
  wire s00_couplers_to_s00_couplers_AWVALID;
  wire [5:0]s00_couplers_to_s00_couplers_BID;
  wire s00_couplers_to_s00_couplers_BREADY;
  wire [1:0]s00_couplers_to_s00_couplers_BRESP;
  wire [3:0]s00_couplers_to_s00_couplers_BUSER;
  wire s00_couplers_to_s00_couplers_BVALID;
  wire [31:0]s00_couplers_to_s00_couplers_RDATA;
  wire [5:0]s00_couplers_to_s00_couplers_RID;
  wire s00_couplers_to_s00_couplers_RLAST;
  wire s00_couplers_to_s00_couplers_RREADY;
  wire [1:0]s00_couplers_to_s00_couplers_RRESP;
  wire [3:0]s00_couplers_to_s00_couplers_RUSER;
  wire s00_couplers_to_s00_couplers_RVALID;
  wire [31:0]s00_couplers_to_s00_couplers_WDATA;
  wire s00_couplers_to_s00_couplers_WLAST;
  wire s00_couplers_to_s00_couplers_WREADY;
  wire [3:0]s00_couplers_to_s00_couplers_WSTRB;
  wire [3:0]s00_couplers_to_s00_couplers_WUSER;
  wire s00_couplers_to_s00_couplers_WVALID;

  assign M_AXI_araddr[31:0] = s00_couplers_to_s00_couplers_ARADDR;
  assign M_AXI_arburst[1:0] = s00_couplers_to_s00_couplers_ARBURST;
  assign M_AXI_arcache[3:0] = s00_couplers_to_s00_couplers_ARCACHE;
  assign M_AXI_arid[5:0] = s00_couplers_to_s00_couplers_ARID;
  assign M_AXI_arlen[7:0] = s00_couplers_to_s00_couplers_ARLEN;
  assign M_AXI_arlock = s00_couplers_to_s00_couplers_ARLOCK;
  assign M_AXI_arprot[2:0] = s00_couplers_to_s00_couplers_ARPROT;
  assign M_AXI_arqos[3:0] = s00_couplers_to_s00_couplers_ARQOS;
  assign M_AXI_arsize[2:0] = s00_couplers_to_s00_couplers_ARSIZE;
  assign M_AXI_aruser[3:0] = s00_couplers_to_s00_couplers_ARUSER;
  assign M_AXI_arvalid = s00_couplers_to_s00_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = s00_couplers_to_s00_couplers_AWADDR;
  assign M_AXI_awburst[1:0] = s00_couplers_to_s00_couplers_AWBURST;
  assign M_AXI_awcache[3:0] = s00_couplers_to_s00_couplers_AWCACHE;
  assign M_AXI_awid[5:0] = s00_couplers_to_s00_couplers_AWID;
  assign M_AXI_awlen[7:0] = s00_couplers_to_s00_couplers_AWLEN;
  assign M_AXI_awlock = s00_couplers_to_s00_couplers_AWLOCK;
  assign M_AXI_awprot[2:0] = s00_couplers_to_s00_couplers_AWPROT;
  assign M_AXI_awqos[3:0] = s00_couplers_to_s00_couplers_AWQOS;
  assign M_AXI_awsize[2:0] = s00_couplers_to_s00_couplers_AWSIZE;
  assign M_AXI_awuser[3:0] = s00_couplers_to_s00_couplers_AWUSER;
  assign M_AXI_awvalid = s00_couplers_to_s00_couplers_AWVALID;
  assign M_AXI_bready = s00_couplers_to_s00_couplers_BREADY;
  assign M_AXI_rready = s00_couplers_to_s00_couplers_RREADY;
  assign M_AXI_wdata[31:0] = s00_couplers_to_s00_couplers_WDATA;
  assign M_AXI_wlast = s00_couplers_to_s00_couplers_WLAST;
  assign M_AXI_wstrb[3:0] = s00_couplers_to_s00_couplers_WSTRB;
  assign M_AXI_wuser[3:0] = s00_couplers_to_s00_couplers_WUSER;
  assign M_AXI_wvalid = s00_couplers_to_s00_couplers_WVALID;
  assign S_AXI_arready = s00_couplers_to_s00_couplers_ARREADY;
  assign S_AXI_awready = s00_couplers_to_s00_couplers_AWREADY;
  assign S_AXI_bid[5:0] = s00_couplers_to_s00_couplers_BID;
  assign S_AXI_bresp[1:0] = s00_couplers_to_s00_couplers_BRESP;
  assign S_AXI_buser[3:0] = s00_couplers_to_s00_couplers_BUSER;
  assign S_AXI_bvalid = s00_couplers_to_s00_couplers_BVALID;
  assign S_AXI_rdata[31:0] = s00_couplers_to_s00_couplers_RDATA;
  assign S_AXI_rid[5:0] = s00_couplers_to_s00_couplers_RID;
  assign S_AXI_rlast = s00_couplers_to_s00_couplers_RLAST;
  assign S_AXI_rresp[1:0] = s00_couplers_to_s00_couplers_RRESP;
  assign S_AXI_ruser[3:0] = s00_couplers_to_s00_couplers_RUSER;
  assign S_AXI_rvalid = s00_couplers_to_s00_couplers_RVALID;
  assign S_AXI_wready = s00_couplers_to_s00_couplers_WREADY;
  assign s00_couplers_to_s00_couplers_ARADDR = S_AXI_araddr[31:0];
  assign s00_couplers_to_s00_couplers_ARBURST = S_AXI_arburst[1:0];
  assign s00_couplers_to_s00_couplers_ARCACHE = S_AXI_arcache[3:0];
  assign s00_couplers_to_s00_couplers_ARID = S_AXI_arid[5:0];
  assign s00_couplers_to_s00_couplers_ARLEN = S_AXI_arlen[7:0];
  assign s00_couplers_to_s00_couplers_ARLOCK = S_AXI_arlock;
  assign s00_couplers_to_s00_couplers_ARPROT = S_AXI_arprot[2:0];
  assign s00_couplers_to_s00_couplers_ARQOS = S_AXI_arqos[3:0];
  assign s00_couplers_to_s00_couplers_ARREADY = M_AXI_arready;
  assign s00_couplers_to_s00_couplers_ARSIZE = S_AXI_arsize[2:0];
  assign s00_couplers_to_s00_couplers_ARUSER = S_AXI_aruser[3:0];
  assign s00_couplers_to_s00_couplers_ARVALID = S_AXI_arvalid;
  assign s00_couplers_to_s00_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign s00_couplers_to_s00_couplers_AWBURST = S_AXI_awburst[1:0];
  assign s00_couplers_to_s00_couplers_AWCACHE = S_AXI_awcache[3:0];
  assign s00_couplers_to_s00_couplers_AWID = S_AXI_awid[5:0];
  assign s00_couplers_to_s00_couplers_AWLEN = S_AXI_awlen[7:0];
  assign s00_couplers_to_s00_couplers_AWLOCK = S_AXI_awlock;
  assign s00_couplers_to_s00_couplers_AWPROT = S_AXI_awprot[2:0];
  assign s00_couplers_to_s00_couplers_AWQOS = S_AXI_awqos[3:0];
  assign s00_couplers_to_s00_couplers_AWREADY = M_AXI_awready;
  assign s00_couplers_to_s00_couplers_AWSIZE = S_AXI_awsize[2:0];
  assign s00_couplers_to_s00_couplers_AWUSER = S_AXI_awuser[3:0];
  assign s00_couplers_to_s00_couplers_AWVALID = S_AXI_awvalid;
  assign s00_couplers_to_s00_couplers_BID = M_AXI_bid[5:0];
  assign s00_couplers_to_s00_couplers_BREADY = S_AXI_bready;
  assign s00_couplers_to_s00_couplers_BRESP = M_AXI_bresp[1:0];
  assign s00_couplers_to_s00_couplers_BUSER = M_AXI_buser[3:0];
  assign s00_couplers_to_s00_couplers_BVALID = M_AXI_bvalid;
  assign s00_couplers_to_s00_couplers_RDATA = M_AXI_rdata[31:0];
  assign s00_couplers_to_s00_couplers_RID = M_AXI_rid[5:0];
  assign s00_couplers_to_s00_couplers_RLAST = M_AXI_rlast;
  assign s00_couplers_to_s00_couplers_RREADY = S_AXI_rready;
  assign s00_couplers_to_s00_couplers_RRESP = M_AXI_rresp[1:0];
  assign s00_couplers_to_s00_couplers_RUSER = M_AXI_ruser[3:0];
  assign s00_couplers_to_s00_couplers_RVALID = M_AXI_rvalid;
  assign s00_couplers_to_s00_couplers_WDATA = S_AXI_wdata[31:0];
  assign s00_couplers_to_s00_couplers_WLAST = S_AXI_wlast;
  assign s00_couplers_to_s00_couplers_WREADY = M_AXI_wready;
  assign s00_couplers_to_s00_couplers_WSTRB = S_AXI_wstrb[3:0];
  assign s00_couplers_to_s00_couplers_WUSER = S_AXI_wuser[3:0];
  assign s00_couplers_to_s00_couplers_WVALID = S_AXI_wvalid;
endmodule

module s00_couplers_imp_1TQTEWV
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arburst,
    M_AXI_arcache,
    M_AXI_arid,
    M_AXI_arlen,
    M_AXI_arlock,
    M_AXI_arprot,
    M_AXI_arqos,
    M_AXI_arready,
    M_AXI_arregion,
    M_AXI_arsize,
    M_AXI_aruser,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awburst,
    M_AXI_awcache,
    M_AXI_awid,
    M_AXI_awlen,
    M_AXI_awlock,
    M_AXI_awprot,
    M_AXI_awqos,
    M_AXI_awready,
    M_AXI_awregion,
    M_AXI_awsize,
    M_AXI_awuser,
    M_AXI_awvalid,
    M_AXI_bid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_buser,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rid,
    M_AXI_rlast,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_ruser,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wlast,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wuser,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arid,
    S_AXI_arlen,
    S_AXI_arlock,
    S_AXI_arprot,
    S_AXI_arqos,
    S_AXI_arready,
    S_AXI_arregion,
    S_AXI_arsize,
    S_AXI_aruser,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awid,
    S_AXI_awlen,
    S_AXI_awlock,
    S_AXI_awprot,
    S_AXI_awqos,
    S_AXI_awready,
    S_AXI_awregion,
    S_AXI_awsize,
    S_AXI_awuser,
    S_AXI_awvalid,
    S_AXI_bid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_buser,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rid,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_ruser,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wuser,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [1:0]M_AXI_arburst;
  output [3:0]M_AXI_arcache;
  output [5:0]M_AXI_arid;
  output [7:0]M_AXI_arlen;
  output [0:0]M_AXI_arlock;
  output [2:0]M_AXI_arprot;
  output [3:0]M_AXI_arqos;
  input M_AXI_arready;
  output [3:0]M_AXI_arregion;
  output [2:0]M_AXI_arsize;
  output [3:0]M_AXI_aruser;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [1:0]M_AXI_awburst;
  output [3:0]M_AXI_awcache;
  output [5:0]M_AXI_awid;
  output [7:0]M_AXI_awlen;
  output [0:0]M_AXI_awlock;
  output [2:0]M_AXI_awprot;
  output [3:0]M_AXI_awqos;
  input M_AXI_awready;
  output [3:0]M_AXI_awregion;
  output [2:0]M_AXI_awsize;
  output [3:0]M_AXI_awuser;
  output M_AXI_awvalid;
  input [5:0]M_AXI_bid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input [3:0]M_AXI_buser;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  input [5:0]M_AXI_rid;
  input M_AXI_rlast;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input [3:0]M_AXI_ruser;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  output M_AXI_wlast;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output [3:0]M_AXI_wuser;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [5:0]S_AXI_arid;
  input [7:0]S_AXI_arlen;
  input [0:0]S_AXI_arlock;
  input [2:0]S_AXI_arprot;
  input [3:0]S_AXI_arqos;
  output S_AXI_arready;
  input [3:0]S_AXI_arregion;
  input [2:0]S_AXI_arsize;
  input [3:0]S_AXI_aruser;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [5:0]S_AXI_awid;
  input [7:0]S_AXI_awlen;
  input [0:0]S_AXI_awlock;
  input [2:0]S_AXI_awprot;
  input [3:0]S_AXI_awqos;
  output S_AXI_awready;
  input [3:0]S_AXI_awregion;
  input [2:0]S_AXI_awsize;
  input [3:0]S_AXI_awuser;
  input S_AXI_awvalid;
  output [5:0]S_AXI_bid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output [3:0]S_AXI_buser;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  output [5:0]S_AXI_rid;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output [3:0]S_AXI_ruser;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  input S_AXI_wlast;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input [3:0]S_AXI_wuser;
  input S_AXI_wvalid;

  wire M_ACLK_1;
  wire M_ARESETN_1;
  wire S_ACLK_1;
  wire S_ARESETN_1;
  wire [31:0]auto_cc_to_s00_couplers_ARADDR;
  wire [1:0]auto_cc_to_s00_couplers_ARBURST;
  wire [3:0]auto_cc_to_s00_couplers_ARCACHE;
  wire [5:0]auto_cc_to_s00_couplers_ARID;
  wire [7:0]auto_cc_to_s00_couplers_ARLEN;
  wire [0:0]auto_cc_to_s00_couplers_ARLOCK;
  wire [2:0]auto_cc_to_s00_couplers_ARPROT;
  wire [3:0]auto_cc_to_s00_couplers_ARQOS;
  wire auto_cc_to_s00_couplers_ARREADY;
  wire [3:0]auto_cc_to_s00_couplers_ARREGION;
  wire [2:0]auto_cc_to_s00_couplers_ARSIZE;
  wire [3:0]auto_cc_to_s00_couplers_ARUSER;
  wire auto_cc_to_s00_couplers_ARVALID;
  wire [31:0]auto_cc_to_s00_couplers_AWADDR;
  wire [1:0]auto_cc_to_s00_couplers_AWBURST;
  wire [3:0]auto_cc_to_s00_couplers_AWCACHE;
  wire [5:0]auto_cc_to_s00_couplers_AWID;
  wire [7:0]auto_cc_to_s00_couplers_AWLEN;
  wire [0:0]auto_cc_to_s00_couplers_AWLOCK;
  wire [2:0]auto_cc_to_s00_couplers_AWPROT;
  wire [3:0]auto_cc_to_s00_couplers_AWQOS;
  wire auto_cc_to_s00_couplers_AWREADY;
  wire [3:0]auto_cc_to_s00_couplers_AWREGION;
  wire [2:0]auto_cc_to_s00_couplers_AWSIZE;
  wire [3:0]auto_cc_to_s00_couplers_AWUSER;
  wire auto_cc_to_s00_couplers_AWVALID;
  wire [5:0]auto_cc_to_s00_couplers_BID;
  wire auto_cc_to_s00_couplers_BREADY;
  wire [1:0]auto_cc_to_s00_couplers_BRESP;
  wire [3:0]auto_cc_to_s00_couplers_BUSER;
  wire auto_cc_to_s00_couplers_BVALID;
  wire [31:0]auto_cc_to_s00_couplers_RDATA;
  wire [5:0]auto_cc_to_s00_couplers_RID;
  wire auto_cc_to_s00_couplers_RLAST;
  wire auto_cc_to_s00_couplers_RREADY;
  wire [1:0]auto_cc_to_s00_couplers_RRESP;
  wire [3:0]auto_cc_to_s00_couplers_RUSER;
  wire auto_cc_to_s00_couplers_RVALID;
  wire [31:0]auto_cc_to_s00_couplers_WDATA;
  wire auto_cc_to_s00_couplers_WLAST;
  wire auto_cc_to_s00_couplers_WREADY;
  wire [3:0]auto_cc_to_s00_couplers_WSTRB;
  wire [3:0]auto_cc_to_s00_couplers_WUSER;
  wire auto_cc_to_s00_couplers_WVALID;
  wire [31:0]s00_couplers_to_auto_cc_ARADDR;
  wire [1:0]s00_couplers_to_auto_cc_ARBURST;
  wire [3:0]s00_couplers_to_auto_cc_ARCACHE;
  wire [5:0]s00_couplers_to_auto_cc_ARID;
  wire [7:0]s00_couplers_to_auto_cc_ARLEN;
  wire [0:0]s00_couplers_to_auto_cc_ARLOCK;
  wire [2:0]s00_couplers_to_auto_cc_ARPROT;
  wire [3:0]s00_couplers_to_auto_cc_ARQOS;
  wire s00_couplers_to_auto_cc_ARREADY;
  wire [3:0]s00_couplers_to_auto_cc_ARREGION;
  wire [2:0]s00_couplers_to_auto_cc_ARSIZE;
  wire [3:0]s00_couplers_to_auto_cc_ARUSER;
  wire s00_couplers_to_auto_cc_ARVALID;
  wire [31:0]s00_couplers_to_auto_cc_AWADDR;
  wire [1:0]s00_couplers_to_auto_cc_AWBURST;
  wire [3:0]s00_couplers_to_auto_cc_AWCACHE;
  wire [5:0]s00_couplers_to_auto_cc_AWID;
  wire [7:0]s00_couplers_to_auto_cc_AWLEN;
  wire [0:0]s00_couplers_to_auto_cc_AWLOCK;
  wire [2:0]s00_couplers_to_auto_cc_AWPROT;
  wire [3:0]s00_couplers_to_auto_cc_AWQOS;
  wire s00_couplers_to_auto_cc_AWREADY;
  wire [3:0]s00_couplers_to_auto_cc_AWREGION;
  wire [2:0]s00_couplers_to_auto_cc_AWSIZE;
  wire [3:0]s00_couplers_to_auto_cc_AWUSER;
  wire s00_couplers_to_auto_cc_AWVALID;
  wire [5:0]s00_couplers_to_auto_cc_BID;
  wire s00_couplers_to_auto_cc_BREADY;
  wire [1:0]s00_couplers_to_auto_cc_BRESP;
  wire [3:0]s00_couplers_to_auto_cc_BUSER;
  wire s00_couplers_to_auto_cc_BVALID;
  wire [31:0]s00_couplers_to_auto_cc_RDATA;
  wire [5:0]s00_couplers_to_auto_cc_RID;
  wire s00_couplers_to_auto_cc_RLAST;
  wire s00_couplers_to_auto_cc_RREADY;
  wire [1:0]s00_couplers_to_auto_cc_RRESP;
  wire [3:0]s00_couplers_to_auto_cc_RUSER;
  wire s00_couplers_to_auto_cc_RVALID;
  wire [31:0]s00_couplers_to_auto_cc_WDATA;
  wire s00_couplers_to_auto_cc_WLAST;
  wire s00_couplers_to_auto_cc_WREADY;
  wire [3:0]s00_couplers_to_auto_cc_WSTRB;
  wire [3:0]s00_couplers_to_auto_cc_WUSER;
  wire s00_couplers_to_auto_cc_WVALID;

  assign M_ACLK_1 = M_ACLK;
  assign M_ARESETN_1 = M_ARESETN;
  assign M_AXI_araddr[31:0] = auto_cc_to_s00_couplers_ARADDR;
  assign M_AXI_arburst[1:0] = auto_cc_to_s00_couplers_ARBURST;
  assign M_AXI_arcache[3:0] = auto_cc_to_s00_couplers_ARCACHE;
  assign M_AXI_arid[5:0] = auto_cc_to_s00_couplers_ARID;
  assign M_AXI_arlen[7:0] = auto_cc_to_s00_couplers_ARLEN;
  assign M_AXI_arlock[0] = auto_cc_to_s00_couplers_ARLOCK;
  assign M_AXI_arprot[2:0] = auto_cc_to_s00_couplers_ARPROT;
  assign M_AXI_arqos[3:0] = auto_cc_to_s00_couplers_ARQOS;
  assign M_AXI_arregion[3:0] = auto_cc_to_s00_couplers_ARREGION;
  assign M_AXI_arsize[2:0] = auto_cc_to_s00_couplers_ARSIZE;
  assign M_AXI_aruser[3:0] = auto_cc_to_s00_couplers_ARUSER;
  assign M_AXI_arvalid = auto_cc_to_s00_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = auto_cc_to_s00_couplers_AWADDR;
  assign M_AXI_awburst[1:0] = auto_cc_to_s00_couplers_AWBURST;
  assign M_AXI_awcache[3:0] = auto_cc_to_s00_couplers_AWCACHE;
  assign M_AXI_awid[5:0] = auto_cc_to_s00_couplers_AWID;
  assign M_AXI_awlen[7:0] = auto_cc_to_s00_couplers_AWLEN;
  assign M_AXI_awlock[0] = auto_cc_to_s00_couplers_AWLOCK;
  assign M_AXI_awprot[2:0] = auto_cc_to_s00_couplers_AWPROT;
  assign M_AXI_awqos[3:0] = auto_cc_to_s00_couplers_AWQOS;
  assign M_AXI_awregion[3:0] = auto_cc_to_s00_couplers_AWREGION;
  assign M_AXI_awsize[2:0] = auto_cc_to_s00_couplers_AWSIZE;
  assign M_AXI_awuser[3:0] = auto_cc_to_s00_couplers_AWUSER;
  assign M_AXI_awvalid = auto_cc_to_s00_couplers_AWVALID;
  assign M_AXI_bready = auto_cc_to_s00_couplers_BREADY;
  assign M_AXI_rready = auto_cc_to_s00_couplers_RREADY;
  assign M_AXI_wdata[31:0] = auto_cc_to_s00_couplers_WDATA;
  assign M_AXI_wlast = auto_cc_to_s00_couplers_WLAST;
  assign M_AXI_wstrb[3:0] = auto_cc_to_s00_couplers_WSTRB;
  assign M_AXI_wuser[3:0] = auto_cc_to_s00_couplers_WUSER;
  assign M_AXI_wvalid = auto_cc_to_s00_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN;
  assign S_AXI_arready = s00_couplers_to_auto_cc_ARREADY;
  assign S_AXI_awready = s00_couplers_to_auto_cc_AWREADY;
  assign S_AXI_bid[5:0] = s00_couplers_to_auto_cc_BID;
  assign S_AXI_bresp[1:0] = s00_couplers_to_auto_cc_BRESP;
  assign S_AXI_buser[3:0] = s00_couplers_to_auto_cc_BUSER;
  assign S_AXI_bvalid = s00_couplers_to_auto_cc_BVALID;
  assign S_AXI_rdata[31:0] = s00_couplers_to_auto_cc_RDATA;
  assign S_AXI_rid[5:0] = s00_couplers_to_auto_cc_RID;
  assign S_AXI_rlast = s00_couplers_to_auto_cc_RLAST;
  assign S_AXI_rresp[1:0] = s00_couplers_to_auto_cc_RRESP;
  assign S_AXI_ruser[3:0] = s00_couplers_to_auto_cc_RUSER;
  assign S_AXI_rvalid = s00_couplers_to_auto_cc_RVALID;
  assign S_AXI_wready = s00_couplers_to_auto_cc_WREADY;
  assign auto_cc_to_s00_couplers_ARREADY = M_AXI_arready;
  assign auto_cc_to_s00_couplers_AWREADY = M_AXI_awready;
  assign auto_cc_to_s00_couplers_BID = M_AXI_bid[5:0];
  assign auto_cc_to_s00_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_cc_to_s00_couplers_BUSER = M_AXI_buser[3:0];
  assign auto_cc_to_s00_couplers_BVALID = M_AXI_bvalid;
  assign auto_cc_to_s00_couplers_RDATA = M_AXI_rdata[31:0];
  assign auto_cc_to_s00_couplers_RID = M_AXI_rid[5:0];
  assign auto_cc_to_s00_couplers_RLAST = M_AXI_rlast;
  assign auto_cc_to_s00_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_cc_to_s00_couplers_RUSER = M_AXI_ruser[3:0];
  assign auto_cc_to_s00_couplers_RVALID = M_AXI_rvalid;
  assign auto_cc_to_s00_couplers_WREADY = M_AXI_wready;
  assign s00_couplers_to_auto_cc_ARADDR = S_AXI_araddr[31:0];
  assign s00_couplers_to_auto_cc_ARBURST = S_AXI_arburst[1:0];
  assign s00_couplers_to_auto_cc_ARCACHE = S_AXI_arcache[3:0];
  assign s00_couplers_to_auto_cc_ARID = S_AXI_arid[5:0];
  assign s00_couplers_to_auto_cc_ARLEN = S_AXI_arlen[7:0];
  assign s00_couplers_to_auto_cc_ARLOCK = S_AXI_arlock[0];
  assign s00_couplers_to_auto_cc_ARPROT = S_AXI_arprot[2:0];
  assign s00_couplers_to_auto_cc_ARQOS = S_AXI_arqos[3:0];
  assign s00_couplers_to_auto_cc_ARREGION = S_AXI_arregion[3:0];
  assign s00_couplers_to_auto_cc_ARSIZE = S_AXI_arsize[2:0];
  assign s00_couplers_to_auto_cc_ARUSER = S_AXI_aruser[3:0];
  assign s00_couplers_to_auto_cc_ARVALID = S_AXI_arvalid;
  assign s00_couplers_to_auto_cc_AWADDR = S_AXI_awaddr[31:0];
  assign s00_couplers_to_auto_cc_AWBURST = S_AXI_awburst[1:0];
  assign s00_couplers_to_auto_cc_AWCACHE = S_AXI_awcache[3:0];
  assign s00_couplers_to_auto_cc_AWID = S_AXI_awid[5:0];
  assign s00_couplers_to_auto_cc_AWLEN = S_AXI_awlen[7:0];
  assign s00_couplers_to_auto_cc_AWLOCK = S_AXI_awlock[0];
  assign s00_couplers_to_auto_cc_AWPROT = S_AXI_awprot[2:0];
  assign s00_couplers_to_auto_cc_AWQOS = S_AXI_awqos[3:0];
  assign s00_couplers_to_auto_cc_AWREGION = S_AXI_awregion[3:0];
  assign s00_couplers_to_auto_cc_AWSIZE = S_AXI_awsize[2:0];
  assign s00_couplers_to_auto_cc_AWUSER = S_AXI_awuser[3:0];
  assign s00_couplers_to_auto_cc_AWVALID = S_AXI_awvalid;
  assign s00_couplers_to_auto_cc_BREADY = S_AXI_bready;
  assign s00_couplers_to_auto_cc_RREADY = S_AXI_rready;
  assign s00_couplers_to_auto_cc_WDATA = S_AXI_wdata[31:0];
  assign s00_couplers_to_auto_cc_WLAST = S_AXI_wlast;
  assign s00_couplers_to_auto_cc_WSTRB = S_AXI_wstrb[3:0];
  assign s00_couplers_to_auto_cc_WUSER = S_AXI_wuser[3:0];
  assign s00_couplers_to_auto_cc_WVALID = S_AXI_wvalid;
  gpn_bundle_block_auto_cc_1 auto_cc
       (.m_axi_aclk(M_ACLK_1),
        .m_axi_araddr(auto_cc_to_s00_couplers_ARADDR),
        .m_axi_arburst(auto_cc_to_s00_couplers_ARBURST),
        .m_axi_arcache(auto_cc_to_s00_couplers_ARCACHE),
        .m_axi_aresetn(M_ARESETN_1),
        .m_axi_arid(auto_cc_to_s00_couplers_ARID),
        .m_axi_arlen(auto_cc_to_s00_couplers_ARLEN),
        .m_axi_arlock(auto_cc_to_s00_couplers_ARLOCK),
        .m_axi_arprot(auto_cc_to_s00_couplers_ARPROT),
        .m_axi_arqos(auto_cc_to_s00_couplers_ARQOS),
        .m_axi_arready(auto_cc_to_s00_couplers_ARREADY),
        .m_axi_arregion(auto_cc_to_s00_couplers_ARREGION),
        .m_axi_arsize(auto_cc_to_s00_couplers_ARSIZE),
        .m_axi_aruser(auto_cc_to_s00_couplers_ARUSER),
        .m_axi_arvalid(auto_cc_to_s00_couplers_ARVALID),
        .m_axi_awaddr(auto_cc_to_s00_couplers_AWADDR),
        .m_axi_awburst(auto_cc_to_s00_couplers_AWBURST),
        .m_axi_awcache(auto_cc_to_s00_couplers_AWCACHE),
        .m_axi_awid(auto_cc_to_s00_couplers_AWID),
        .m_axi_awlen(auto_cc_to_s00_couplers_AWLEN),
        .m_axi_awlock(auto_cc_to_s00_couplers_AWLOCK),
        .m_axi_awprot(auto_cc_to_s00_couplers_AWPROT),
        .m_axi_awqos(auto_cc_to_s00_couplers_AWQOS),
        .m_axi_awready(auto_cc_to_s00_couplers_AWREADY),
        .m_axi_awregion(auto_cc_to_s00_couplers_AWREGION),
        .m_axi_awsize(auto_cc_to_s00_couplers_AWSIZE),
        .m_axi_awuser(auto_cc_to_s00_couplers_AWUSER),
        .m_axi_awvalid(auto_cc_to_s00_couplers_AWVALID),
        .m_axi_bid(auto_cc_to_s00_couplers_BID),
        .m_axi_bready(auto_cc_to_s00_couplers_BREADY),
        .m_axi_bresp(auto_cc_to_s00_couplers_BRESP),
        .m_axi_buser(auto_cc_to_s00_couplers_BUSER),
        .m_axi_bvalid(auto_cc_to_s00_couplers_BVALID),
        .m_axi_rdata(auto_cc_to_s00_couplers_RDATA),
        .m_axi_rid(auto_cc_to_s00_couplers_RID),
        .m_axi_rlast(auto_cc_to_s00_couplers_RLAST),
        .m_axi_rready(auto_cc_to_s00_couplers_RREADY),
        .m_axi_rresp(auto_cc_to_s00_couplers_RRESP),
        .m_axi_ruser(auto_cc_to_s00_couplers_RUSER),
        .m_axi_rvalid(auto_cc_to_s00_couplers_RVALID),
        .m_axi_wdata(auto_cc_to_s00_couplers_WDATA),
        .m_axi_wlast(auto_cc_to_s00_couplers_WLAST),
        .m_axi_wready(auto_cc_to_s00_couplers_WREADY),
        .m_axi_wstrb(auto_cc_to_s00_couplers_WSTRB),
        .m_axi_wuser(auto_cc_to_s00_couplers_WUSER),
        .m_axi_wvalid(auto_cc_to_s00_couplers_WVALID),
        .s_axi_aclk(S_ACLK_1),
        .s_axi_araddr(s00_couplers_to_auto_cc_ARADDR),
        .s_axi_arburst(s00_couplers_to_auto_cc_ARBURST),
        .s_axi_arcache(s00_couplers_to_auto_cc_ARCACHE),
        .s_axi_aresetn(S_ARESETN_1),
        .s_axi_arid(s00_couplers_to_auto_cc_ARID),
        .s_axi_arlen(s00_couplers_to_auto_cc_ARLEN),
        .s_axi_arlock(s00_couplers_to_auto_cc_ARLOCK),
        .s_axi_arprot(s00_couplers_to_auto_cc_ARPROT),
        .s_axi_arqos(s00_couplers_to_auto_cc_ARQOS),
        .s_axi_arready(s00_couplers_to_auto_cc_ARREADY),
        .s_axi_arregion(s00_couplers_to_auto_cc_ARREGION),
        .s_axi_arsize(s00_couplers_to_auto_cc_ARSIZE),
        .s_axi_aruser(s00_couplers_to_auto_cc_ARUSER),
        .s_axi_arvalid(s00_couplers_to_auto_cc_ARVALID),
        .s_axi_awaddr(s00_couplers_to_auto_cc_AWADDR),
        .s_axi_awburst(s00_couplers_to_auto_cc_AWBURST),
        .s_axi_awcache(s00_couplers_to_auto_cc_AWCACHE),
        .s_axi_awid(s00_couplers_to_auto_cc_AWID),
        .s_axi_awlen(s00_couplers_to_auto_cc_AWLEN),
        .s_axi_awlock(s00_couplers_to_auto_cc_AWLOCK),
        .s_axi_awprot(s00_couplers_to_auto_cc_AWPROT),
        .s_axi_awqos(s00_couplers_to_auto_cc_AWQOS),
        .s_axi_awready(s00_couplers_to_auto_cc_AWREADY),
        .s_axi_awregion(s00_couplers_to_auto_cc_AWREGION),
        .s_axi_awsize(s00_couplers_to_auto_cc_AWSIZE),
        .s_axi_awuser(s00_couplers_to_auto_cc_AWUSER),
        .s_axi_awvalid(s00_couplers_to_auto_cc_AWVALID),
        .s_axi_bid(s00_couplers_to_auto_cc_BID),
        .s_axi_bready(s00_couplers_to_auto_cc_BREADY),
        .s_axi_bresp(s00_couplers_to_auto_cc_BRESP),
        .s_axi_buser(s00_couplers_to_auto_cc_BUSER),
        .s_axi_bvalid(s00_couplers_to_auto_cc_BVALID),
        .s_axi_rdata(s00_couplers_to_auto_cc_RDATA),
        .s_axi_rid(s00_couplers_to_auto_cc_RID),
        .s_axi_rlast(s00_couplers_to_auto_cc_RLAST),
        .s_axi_rready(s00_couplers_to_auto_cc_RREADY),
        .s_axi_rresp(s00_couplers_to_auto_cc_RRESP),
        .s_axi_ruser(s00_couplers_to_auto_cc_RUSER),
        .s_axi_rvalid(s00_couplers_to_auto_cc_RVALID),
        .s_axi_wdata(s00_couplers_to_auto_cc_WDATA),
        .s_axi_wlast(s00_couplers_to_auto_cc_WLAST),
        .s_axi_wready(s00_couplers_to_auto_cc_WREADY),
        .s_axi_wstrb(s00_couplers_to_auto_cc_WSTRB),
        .s_axi_wuser(s00_couplers_to_auto_cc_WUSER),
        .s_axi_wvalid(s00_couplers_to_auto_cc_WVALID));
endmodule
