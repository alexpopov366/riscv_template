
//  Xilinx UltraRAM True Dual Port Mode - Byte write.  This code implements 
//  a parameterizable UltraRAM block with write/read on both ports in 
//  No change behavior on both the ports . The behavior of this RAM is 
//  when data is written, the output of RAM is unchanged w.r.t each port. 
//  Only when write is inactive data corresponding to the address is 
//  presented on the output port.
//
module xilinx_ultraram_true_dual_port_bytewrite #(
  parameter AWIDTH  = 12,  // Address Width
  parameter NUM_COL = 4,   // Number of columns
  parameter DWIDTH  = 32,  // Data Width, (Byte * NUM_COL) 
  parameter NBPIPE  = 1    // Number of pipeline Registers
 ) ( 
    input clk,                    // Clock
    // Port A
    input rsta,                   // Reset
    input [NUM_COL-1:0] wea,      // Write Enable
    input regcea,                 // Output Register Enable
    input mem_ena,                // Memory Enable
    input [DWIDTH-1:0] dina,      // Data Input  
    input [AWIDTH-1:0] addra,     // Address Input
    output reg [DWIDTH-1:0] douta,// Data Output

    // Port B
    input rstb,                   // Reset
    input [NUM_COL-1:0] web,      // Write Enable
    input regceb,                 // Output Register Enable
    input mem_enb,                // Memory Enable
    input [DWIDTH-1:0] dinb,      // Data Input  
    input [AWIDTH-1:0] addrb,     // Address Input
    output reg [DWIDTH-1:0] doutb // Data Output
   );

(* ram_style = "ultra" *)
reg [DWIDTH-1:0] mem[(1<<AWIDTH)-1:0];        // Memory Declaration
reg [DWIDTH-1:0] memrega;
reg [DWIDTH-1:0] memregb;              

integer          i;
localparam CWIDTH = DWIDTH/NUM_COL;

// RAM : Read has one latency, Write has one latency as well.
always @ (posedge clk)
begin
 if(mem_ena) 
  begin
  for(i = 0;i<NUM_COL;i=i+1) 
	 if(wea[i])
       mem[addra][i*CWIDTH +: CWIDTH] <= dina[i*CWIDTH +: CWIDTH];
  end     
end

always @ (posedge clk)
begin
 if(mem_ena)
 begin
     if(~|wea)
      memrega <= mem[addra];
 end
end

always @*
begin
     douta <= memrega;
end


// RAM : Read has one latency, Write has one latency as well.
always @ (posedge clk)
begin
 if(mem_enb) 
  begin
  for(i = 0;i<NUM_COL;i=i+1) 
	 if(web[i])
       mem[addrb][i*CWIDTH +: CWIDTH] <= dinb[i*CWIDTH +: CWIDTH];
  end     
end

always @ (posedge clk)
begin
 if(mem_enb)
 begin
     if(~|web)
      memregb <= mem[addrb];
 end
end

always @*
begin
     doutb <= memregb;
end


endmodule
						