// (c) Copyright 1995-2022 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.


// IP VLNV: user.org:user:local_mem:1.0
// IP Revision: 39

`timescale 1ns/1ps

(* IP_DEFINITION_SOURCE = "package_project" *)
(* DowngradeIPIdentifiedWarnings = "yes" *)
module gpn_bundle_block_local_mem_0_0 (
  clk,
  rstn,
  portA_en,
  portA_be,
  portA_addr,
  portA_data_in,
  portA_data_out,
  portB_en,
  portB_be,
  portB_addr,
  portB_data_in,
  portB_data_out
);

(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME clk, ASSOCIATED_RESET rstn, ASSOCIATED_BUSIF portA:portB, FREQ_HZ 200000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN gpn_bundle_block_kernel_clk, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 clk CLK" *)
input wire clk;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME rstn, POLARITY ACTIVE_LOW, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 rstn RST" *)
input wire rstn;
(* X_INTERFACE_INFO = "user.org:interface:local_memory_interface:1.0 portA en" *)
input wire portA_en;
(* X_INTERFACE_INFO = "user.org:interface:local_memory_interface:1.0 portA be" *)
input wire [3 : 0] portA_be;
(* X_INTERFACE_INFO = "user.org:interface:local_memory_interface:1.0 portA addr" *)
input wire [29 : 0] portA_addr;
(* X_INTERFACE_INFO = "user.org:interface:local_memory_interface:1.0 portA data_in" *)
input wire [31 : 0] portA_data_in;
(* X_INTERFACE_INFO = "user.org:interface:local_memory_interface:1.0 portA data_out" *)
output wire [31 : 0] portA_data_out;
(* X_INTERFACE_INFO = "user.org:interface:local_memory_interface:1.0 portB en" *)
input wire portB_en;
(* X_INTERFACE_INFO = "user.org:interface:local_memory_interface:1.0 portB be" *)
input wire [3 : 0] portB_be;
(* X_INTERFACE_INFO = "user.org:interface:local_memory_interface:1.0 portB addr" *)
input wire [29 : 0] portB_addr;
(* X_INTERFACE_INFO = "user.org:interface:local_memory_interface:1.0 portB data_in" *)
input wire [31 : 0] portB_data_in;
(* X_INTERFACE_INFO = "user.org:interface:local_memory_interface:1.0 portB data_out" *)
output wire [31 : 0] portB_data_out;

  local_mem #(
    .RAM_SIZE(64),
    .preload_file(""),
    .USE_PRELOAD_FILE(0),
    .BRAM_STYLE("bram")
  ) inst (
    .clk(clk),
    .rstn(rstn),
    .portA_en(portA_en),
    .portA_be(portA_be),
    .portA_addr(portA_addr),
    .portA_data_in(portA_data_in),
    .portA_data_out(portA_data_out),
    .portB_en(portB_en),
    .portB_be(portB_be),
    .portB_addr(portB_addr),
    .portB_data_in(portB_data_in),
    .portB_data_out(portB_data_out)
  );
endmodule
