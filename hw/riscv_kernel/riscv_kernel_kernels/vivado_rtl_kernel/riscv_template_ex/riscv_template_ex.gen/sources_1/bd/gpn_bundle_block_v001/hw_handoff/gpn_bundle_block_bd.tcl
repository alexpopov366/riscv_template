
################################################################
# This is a generated script based on design: gpn_bundle_block
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

namespace eval _tcl {
proc get_script_folder {} {
   set script_path [file normalize [info script]]
   set script_folder [file dirname $script_path]
   return $script_folder
}
}
variable script_folder
set script_folder [_tcl::get_script_folder]

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2020.2
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   catch {common::send_gid_msg -ssname BD::TCL -id 2041 -severity "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."}

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source gpn_bundle_block_script.tcl

# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./myproj/project_1.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
   create_project project_1 myproj -part xcu200-fsgd2104-2-e
}


# CHANGE DESIGN NAME HERE
variable design_name
set design_name gpn_bundle_block

# If you do not already have an existing IP Integrator design open,
# you can create a design using the following command:
#    create_bd_design $design_name

# Creating design if needed
set errMsg ""
set nRet 0

set cur_design [current_bd_design -quiet]
set list_cells [get_bd_cells -quiet]

if { ${design_name} eq "" } {
   # USE CASES:
   #    1) Design_name not set

   set errMsg "Please set the variable <design_name> to a non-empty value."
   set nRet 1

} elseif { ${cur_design} ne "" && ${list_cells} eq "" } {
   # USE CASES:
   #    2): Current design opened AND is empty AND names same.
   #    3): Current design opened AND is empty AND names diff; design_name NOT in project.
   #    4): Current design opened AND is empty AND names diff; design_name exists in project.

   if { $cur_design ne $design_name } {
      common::send_gid_msg -ssname BD::TCL -id 2001 -severity "INFO" "Changing value of <design_name> from <$design_name> to <$cur_design> since current design is empty."
      set design_name [get_property NAME $cur_design]
   }
   common::send_gid_msg -ssname BD::TCL -id 2002 -severity "INFO" "Constructing design in IPI design <$cur_design>..."

} elseif { ${cur_design} ne "" && $list_cells ne "" && $cur_design eq $design_name } {
   # USE CASES:
   #    5) Current design opened AND has components AND same names.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 1
} elseif { [get_files -quiet ${design_name}.bd] ne "" } {
   # USE CASES: 
   #    6) Current opened design, has components, but diff names, design_name exists in project.
   #    7) No opened design, design_name exists in project.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 2

} else {
   # USE CASES:
   #    8) No opened design, design_name not in project.
   #    9) Current opened design, has components, but diff names, design_name not in project.

   common::send_gid_msg -ssname BD::TCL -id 2003 -severity "INFO" "Currently there is no design <$design_name> in project, so creating one..."

   create_bd_design $design_name

   common::send_gid_msg -ssname BD::TCL -id 2004 -severity "INFO" "Making design <$design_name> as current_bd_design."
   current_bd_design $design_name

}

common::send_gid_msg -ssname BD::TCL -id 2005 -severity "INFO" "Currently the variable <design_name> is equal to \"$design_name\"."

if { $nRet != 0 } {
   catch {common::send_gid_msg -ssname BD::TCL -id 2006 -severity "ERROR" $errMsg}
   return $nRet
}

##################################################################
# DESIGN PROCs
##################################################################



# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  variable script_folder
  variable design_name

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2090 -severity "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2091 -severity "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports
  set external_memory_bus [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 external_memory_bus ]
  set_property -dict [ list \
   CONFIG.ADDR_WIDTH {32} \
   CONFIG.DATA_WIDTH {32} \
   CONFIG.FREQ_HZ {300000000} \
   CONFIG.NUM_READ_OUTSTANDING {2} \
   CONFIG.NUM_WRITE_OUTSTANDING {2} \
   CONFIG.PROTOCOL {AXI4} \
   ] $external_memory_bus

  set global_memory_bus [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 global_memory_bus ]
  set_property -dict [ list \
   CONFIG.ADDR_WIDTH {32} \
   CONFIG.DATA_WIDTH {32} \
   CONFIG.FREQ_HZ {300000000} \
   CONFIG.HAS_BRESP {0} \
   CONFIG.HAS_BURST {0} \
   CONFIG.HAS_CACHE {0} \
   CONFIG.HAS_LOCK {0} \
   CONFIG.HAS_PROT {0} \
   CONFIG.HAS_QOS {0} \
   CONFIG.HAS_REGION {0} \
   CONFIG.HAS_RRESP {0} \
   CONFIG.NUM_READ_OUTSTANDING {2} \
   CONFIG.NUM_WRITE_OUTSTANDING {2} \
   CONFIG.PROTOCOL {AXI4} \
   ] $global_memory_bus


  # Create ports
  set ext_clk [ create_bd_port -dir I -type clk -freq_hz 300000000 ext_clk ]
  set_property -dict [ list \
   CONFIG.ASSOCIATED_BUSIF {global_memory_bus:external_memory_bus} \
   CONFIG.ASSOCIATED_RESET {ext_rstn:ext_rstn} \
 ] $ext_clk
  set ext_rstn [ create_bd_port -dir I -type rst ext_rstn ]
  set gpc2host_mq [ create_bd_port -dir O -from 31 -to 0 gpc2host_mq ]
  set gpc_control_reg [ create_bd_port -dir I -from 31 -to 0 gpc_control_reg ]
  set gpc_reset [ create_bd_port -dir I -type rst gpc_reset ]
  set_property -dict [ list \
   CONFIG.POLARITY {ACTIVE_HIGH} \
 ] $gpc_reset
  set gpc_start [ create_bd_port -dir I -from 0 -to 0 gpc_start ]
  set gpc_state_reg [ create_bd_port -dir O -from 0 -to 0 gpc_state_reg ]
  set host2gpc_mq [ create_bd_port -dir I -from 31 -to 0 host2gpc_mq ]
  set kernel_clk [ create_bd_port -dir I -type clk -freq_hz 200000000 kernel_clk ]
  set_property -dict [ list \
   CONFIG.ASSOCIATED_RESET {kernel_rstn:gpc_reset} \
   CONFIG.CLK_DOMAIN {gpn_bundle_block_kernel_clk} \
 ] $kernel_clk
  set kernel_rstn [ create_bd_port -dir I -type rst kernel_rstn ]
  set mq_ctrl [ create_bd_port -dir O -from 1 -to 0 mq_ctrl ]
  set mq_sr [ create_bd_port -dir I -from 2 -to 0 mq_sr ]

  # Create instance: axi_interconnect_1, and set properties
  set axi_interconnect_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 axi_interconnect_1 ]
  set_property -dict [ list \
   CONFIG.ENABLE_ADVANCED_OPTIONS {1} \
   CONFIG.NUM_MI {1} \
   CONFIG.S00_HAS_DATA_FIFO {0} \
   CONFIG.S00_HAS_REGSLICE {0} \
 ] $axi_interconnect_1

  # Create instance: axi_interconnect_2, and set properties
  set axi_interconnect_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 axi_interconnect_2 ]
  set_property -dict [ list \
   CONFIG.ENABLE_ADVANCED_OPTIONS {1} \
   CONFIG.M00_HAS_DATA_FIFO {0} \
   CONFIG.M00_HAS_REGSLICE {0} \
   CONFIG.NUM_MI {7} \
   CONFIG.S00_HAS_DATA_FIFO {0} \
   CONFIG.S00_HAS_REGSLICE {0} \
 ] $axi_interconnect_2

  # Create instance: axi_interconnect_3, and set properties
  set axi_interconnect_3 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 axi_interconnect_3 ]
  set_property -dict [ list \
   CONFIG.ENABLE_ADVANCED_OPTIONS {1} \
   CONFIG.NUM_MI {1} \
   CONFIG.S00_HAS_DATA_FIFO {0} \
   CONFIG.S00_HAS_REGSLICE {0} \
 ] $axi_interconnect_3

  # Create instance: gpc2host_mq, and set properties
  set gpc2host_mq [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_gpio:2.0 gpc2host_mq ]
  set_property -dict [ list \
   CONFIG.C_ALL_OUTPUTS {1} \
 ] $gpc2host_mq

  # Create instance: gpc_start, and set properties
  set gpc_start [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_gpio:2.0 gpc_start ]
  set_property -dict [ list \
   CONFIG.C_ALL_INPUTS {1} \
   CONFIG.C_ALL_INPUTS_2 {1} \
   CONFIG.C_GPIO_WIDTH {1} \
   CONFIG.C_IS_DUAL {1} \
 ] $gpc_start

  # Create instance: gpc_state, and set properties
  set gpc_state [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_gpio:2.0 gpc_state ]
  set_property -dict [ list \
   CONFIG.C_ALL_OUTPUTS {1} \
   CONFIG.C_DOUT_DEFAULT {0x00000001} \
   CONFIG.C_GPIO_WIDTH {1} \
 ] $gpc_state

  # Create instance: hpst2gpc, and set properties
  set hpst2gpc [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_gpio:2.0 hpst2gpc ]
  set_property -dict [ list \
   CONFIG.C_ALL_INPUTS {1} \
 ] $hpst2gpc

  # Create instance: local_mem_0, and set properties
  set local_mem_0 [ create_bd_cell -type ip -vlnv user.org:user:local_mem:1.0 local_mem_0 ]
  set_property -dict [ list \
   CONFIG.BRAM_STYLE {bram} \
   CONFIG.RAM_SIZE {64} \
 ] $local_mem_0

  # Create instance: local_rom_0, and set properties
  set local_rom_0 [ create_bd_cell -type ip -vlnv bmstu:user:local_rom:1.1 local_rom_0 ]
  set_property -dict [ list \
   CONFIG.RAM_SIZE {128} \
   CONFIG.USE_PRELOAD_FILE {1} \
   CONFIG.preload_file {/home/user/MPS/taiga_booloader_v01/bootloader.hw_init} \
 ] $local_rom_0

  # Create instance: mq_st, and set properties
  set mq_st [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_gpio:2.0 mq_st ]
  set_property -dict [ list \
   CONFIG.C_ALL_INPUTS {1} \
   CONFIG.C_ALL_OUTPUTS {0} \
   CONFIG.C_ALL_OUTPUTS_2 {1} \
   CONFIG.C_GPIO2_WIDTH {2} \
   CONFIG.C_GPIO_WIDTH {3} \
   CONFIG.C_IS_DUAL {1} \
 ] $mq_st

  # Create instance: proc_sys_reset_0, and set properties
  set proc_sys_reset_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 proc_sys_reset_0 ]
  set_property -dict [ list \
   CONFIG.C_EXT_RST_WIDTH {1} \
 ] $proc_sys_reset_0

  # Create instance: taiga_wrapper_xilinx_0, and set properties
  set taiga_wrapper_xilinx_0 [ create_bd_cell -type ip -vlnv user.org:user:taiga_wrapper_xilinx:1.2 taiga_wrapper_xilinx_0 ]

  # Create interface connections
  connect_bd_intf_net -intf_net axi_interconnect_0_M01_AXI [get_bd_intf_pins axi_interconnect_2/M01_AXI] [get_bd_intf_pins gpc_start/S_AXI]
  connect_bd_intf_net -intf_net axi_interconnect_0_M02_AXI [get_bd_intf_pins axi_interconnect_2/M02_AXI] [get_bd_intf_pins gpc_state/S_AXI]
  connect_bd_intf_net -intf_net axi_interconnect_1_M00_AXI [get_bd_intf_ports global_memory_bus] [get_bd_intf_pins axi_interconnect_1/M00_AXI]
  connect_bd_intf_net -intf_net axi_interconnect_2_M03_AXI [get_bd_intf_pins axi_interconnect_2/M03_AXI] [get_bd_intf_pins axi_interconnect_3/S00_AXI]
  connect_bd_intf_net -intf_net axi_interconnect_2_M04_AXI [get_bd_intf_pins axi_interconnect_2/M04_AXI] [get_bd_intf_pins gpc2host_mq/S_AXI]
  connect_bd_intf_net -intf_net axi_interconnect_2_M05_AXI [get_bd_intf_pins axi_interconnect_2/M05_AXI] [get_bd_intf_pins hpst2gpc/S_AXI]
  connect_bd_intf_net -intf_net axi_interconnect_2_M06_AXI [get_bd_intf_pins axi_interconnect_2/M06_AXI] [get_bd_intf_pins mq_st/S_AXI]
  connect_bd_intf_net -intf_net axi_interconnect_3_M00_AXI [get_bd_intf_ports external_memory_bus] [get_bd_intf_pins axi_interconnect_3/M00_AXI]
  connect_bd_intf_net -intf_net gpn0_local_memory_bus [get_bd_intf_pins axi_interconnect_1/S00_AXI] [get_bd_intf_pins axi_interconnect_2/M00_AXI]
  connect_bd_intf_net -intf_net taiga_wrapper_xilinx_0_data_bram [get_bd_intf_pins local_mem_0/portA] [get_bd_intf_pins taiga_wrapper_xilinx_0/data_bram]
  connect_bd_intf_net -intf_net taiga_wrapper_xilinx_0_instruction_bram [get_bd_intf_pins local_mem_0/portB] [get_bd_intf_pins taiga_wrapper_xilinx_0/instruction_bram]
  connect_bd_intf_net -intf_net taiga_wrapper_xilinx_0_instruction_rom [get_bd_intf_pins local_rom_0/portA] [get_bd_intf_pins taiga_wrapper_xilinx_0/instruction_rom]
  connect_bd_intf_net -intf_net taiga_wrapper_xilinx_0_m_axi [get_bd_intf_pins axi_interconnect_2/S00_AXI] [get_bd_intf_pins taiga_wrapper_xilinx_0/m_axi]

  # Create port connections
  connect_bd_net -net M00_ACLK_0_1 [get_bd_ports ext_clk] [get_bd_pins axi_interconnect_1/M00_ACLK] [get_bd_pins axi_interconnect_3/M00_ACLK]
  connect_bd_net -net M00_ARESETN_0_1 [get_bd_ports ext_rstn] [get_bd_pins axi_interconnect_1/M00_ARESETN] [get_bd_pins axi_interconnect_3/M00_ARESETN]
  connect_bd_net -net axi_gpio_1_gpio_io_o [get_bd_ports gpc_state_reg] [get_bd_pins gpc_state/gpio_io_o]
  connect_bd_net -net axi_gpio_2_gpio_io_o [get_bd_ports gpc2host_mq] [get_bd_pins gpc2host_mq/gpio_io_o]
  connect_bd_net -net clk_0_1 [get_bd_ports kernel_clk] [get_bd_pins axi_interconnect_1/ACLK] [get_bd_pins axi_interconnect_1/S00_ACLK] [get_bd_pins axi_interconnect_2/ACLK] [get_bd_pins axi_interconnect_2/M00_ACLK] [get_bd_pins axi_interconnect_2/M01_ACLK] [get_bd_pins axi_interconnect_2/M02_ACLK] [get_bd_pins axi_interconnect_2/M03_ACLK] [get_bd_pins axi_interconnect_2/M04_ACLK] [get_bd_pins axi_interconnect_2/M05_ACLK] [get_bd_pins axi_interconnect_2/M06_ACLK] [get_bd_pins axi_interconnect_2/S00_ACLK] [get_bd_pins axi_interconnect_3/ACLK] [get_bd_pins axi_interconnect_3/S00_ACLK] [get_bd_pins gpc2host_mq/s_axi_aclk] [get_bd_pins gpc_start/s_axi_aclk] [get_bd_pins gpc_state/s_axi_aclk] [get_bd_pins hpst2gpc/s_axi_aclk] [get_bd_pins local_mem_0/clk] [get_bd_pins local_rom_0/clk] [get_bd_pins mq_st/s_axi_aclk] [get_bd_pins proc_sys_reset_0/slowest_sync_clk] [get_bd_pins taiga_wrapper_xilinx_0/clk]
  connect_bd_net -net gpio_io_i_0_1 [get_bd_ports gpc_start] [get_bd_pins gpc_start/gpio_io_i]
  connect_bd_net -net gpio_io_i_0_2 [get_bd_ports mq_sr] [get_bd_pins mq_st/gpio_io_i]
  connect_bd_net -net gpio_io_i_0_3 [get_bd_ports host2gpc_mq] [get_bd_pins hpst2gpc/gpio_io_i]
  connect_bd_net -net gpn_control_reg_1 [get_bd_ports gpc_control_reg] [get_bd_pins gpc_start/gpio2_io_i]
  connect_bd_net -net gpn_reset_1 [get_bd_ports gpc_reset] [get_bd_pins proc_sys_reset_0/ext_reset_in]
  connect_bd_net -net kernel_rstn_1 [get_bd_ports kernel_rstn] [get_bd_pins axi_interconnect_1/ARESETN] [get_bd_pins axi_interconnect_1/S00_ARESETN] [get_bd_pins axi_interconnect_2/ARESETN] [get_bd_pins axi_interconnect_2/M00_ARESETN] [get_bd_pins axi_interconnect_2/M01_ARESETN] [get_bd_pins axi_interconnect_2/M02_ARESETN] [get_bd_pins axi_interconnect_2/M03_ARESETN] [get_bd_pins axi_interconnect_2/M04_ARESETN] [get_bd_pins axi_interconnect_2/M05_ARESETN] [get_bd_pins axi_interconnect_2/M06_ARESETN] [get_bd_pins axi_interconnect_2/S00_ARESETN] [get_bd_pins axi_interconnect_3/ARESETN] [get_bd_pins axi_interconnect_3/S00_ARESETN] [get_bd_pins gpc_start/s_axi_aresetn] [get_bd_pins local_mem_0/rstn] [get_bd_pins local_rom_0/rstn]
  connect_bd_net -net mq_st_gpio2_io_o [get_bd_ports mq_ctrl] [get_bd_pins mq_st/gpio2_io_o]
  connect_bd_net -net proc_sys_reset_0_peripheral_aresetn [get_bd_pins gpc2host_mq/s_axi_aresetn] [get_bd_pins gpc_state/s_axi_aresetn] [get_bd_pins hpst2gpc/s_axi_aresetn] [get_bd_pins mq_st/s_axi_aresetn] [get_bd_pins proc_sys_reset_0/peripheral_aresetn]
  connect_bd_net -net proc_sys_reset_0_peripheral_reset [get_bd_pins proc_sys_reset_0/peripheral_reset] [get_bd_pins taiga_wrapper_xilinx_0/rst]

  # Create address segments
  assign_bd_address -offset 0xA0030000 -range 0x00001000 -target_address_space [get_bd_addr_spaces taiga_wrapper_xilinx_0/m_axi] [get_bd_addr_segs gpc_start/S_AXI/Reg] -force
  assign_bd_address -offset 0xA0020000 -range 0x00001000 -target_address_space [get_bd_addr_spaces taiga_wrapper_xilinx_0/m_axi] [get_bd_addr_segs gpc_state/S_AXI/Reg] -force
  assign_bd_address -offset 0xA0040000 -range 0x00001000 -target_address_space [get_bd_addr_spaces taiga_wrapper_xilinx_0/m_axi] [get_bd_addr_segs gpc2host_mq/S_AXI/Reg] -force
  assign_bd_address -offset 0xA0050000 -range 0x00001000 -target_address_space [get_bd_addr_spaces taiga_wrapper_xilinx_0/m_axi] [get_bd_addr_segs hpst2gpc/S_AXI/Reg] -force
  assign_bd_address -offset 0xA0060000 -range 0x00001000 -target_address_space [get_bd_addr_spaces taiga_wrapper_xilinx_0/m_axi] [get_bd_addr_segs mq_st/S_AXI/Reg] -force
  assign_bd_address -offset 0xA8000000 -range 0x08000000 -target_address_space [get_bd_addr_spaces taiga_wrapper_xilinx_0/m_axi] [get_bd_addr_segs external_memory_bus/Reg] -force
  assign_bd_address -offset 0xA0000000 -range 0x00020000 -target_address_space [get_bd_addr_spaces taiga_wrapper_xilinx_0/m_axi] [get_bd_addr_segs global_memory_bus/Reg] -force


  # Restore current instance
  current_bd_instance $oldCurInst

  validate_bd_design
  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


