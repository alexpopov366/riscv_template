-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
-- Date        : Mon Aug 29 21:30:26 2022
-- Host        : dl580 running 64-bit Ubuntu 18.04.6 LTS
-- Command     : write_vhdl -force -mode synth_stub
--               /home/user/MPS/riscv_kernel/riscv_kernel_kernels/vivado_rtl_kernel/riscv_template_ex/riscv_template_ex.gen/sources_1/bd/gpn_bundle_block_v001/ip/gpn_bundle_block_local_mem_0_0/gpn_bundle_block_local_mem_0_0_stub.vhdl
-- Design      : gpn_bundle_block_local_mem_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcu200-fsgd2104-2-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity gpn_bundle_block_local_mem_0_0 is
  Port ( 
    clk : in STD_LOGIC;
    rstn : in STD_LOGIC;
    portA_en : in STD_LOGIC;
    portA_be : in STD_LOGIC_VECTOR ( 3 downto 0 );
    portA_addr : in STD_LOGIC_VECTOR ( 29 downto 0 );
    portA_data_in : in STD_LOGIC_VECTOR ( 31 downto 0 );
    portA_data_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    portB_en : in STD_LOGIC;
    portB_be : in STD_LOGIC_VECTOR ( 3 downto 0 );
    portB_addr : in STD_LOGIC_VECTOR ( 29 downto 0 );
    portB_data_in : in STD_LOGIC_VECTOR ( 31 downto 0 );
    portB_data_out : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );

end gpn_bundle_block_local_mem_0_0;

architecture stub of gpn_bundle_block_local_mem_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk,rstn,portA_en,portA_be[3:0],portA_addr[29:0],portA_data_in[31:0],portA_data_out[31:0],portB_en,portB_be[3:0],portB_addr[29:0],portB_data_in[31:0],portB_data_out[31:0]";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "local_mem,Vivado 2020.2";
begin
end;
