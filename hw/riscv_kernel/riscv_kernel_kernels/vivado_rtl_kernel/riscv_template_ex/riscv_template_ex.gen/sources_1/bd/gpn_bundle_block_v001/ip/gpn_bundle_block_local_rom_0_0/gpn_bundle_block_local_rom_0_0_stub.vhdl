-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
-- Date        : Sun Aug 28 18:14:54 2022
-- Host        : dl580 running 64-bit Ubuntu 18.04.6 LTS
-- Command     : write_vhdl -force -mode synth_stub -rename_top gpn_bundle_block_local_rom_0_0 -prefix
--               gpn_bundle_block_local_rom_0_0_ gpn_bundle_block_local_rom_0_0_stub.vhdl
-- Design      : gpn_bundle_block_local_rom_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcu200-fsgd2104-2-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity gpn_bundle_block_local_rom_0_0 is
  Port ( 
    clk : in STD_LOGIC;
    rstn : in STD_LOGIC;
    porta_addr : in STD_LOGIC_VECTOR ( 29 downto 0 );
    porta_en : in STD_LOGIC;
    porta_be : in STD_LOGIC_VECTOR ( 3 downto 0 );
    porta_data_in : in STD_LOGIC_VECTOR ( 31 downto 0 );
    porta_data_out : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );

end gpn_bundle_block_local_rom_0_0;

architecture stub of gpn_bundle_block_local_rom_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk,rstn,porta_addr[29:0],porta_en,porta_be[3:0],porta_data_in[31:0],porta_data_out[31:0]";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "local_rom,Vivado 2020.2";
begin
end;
