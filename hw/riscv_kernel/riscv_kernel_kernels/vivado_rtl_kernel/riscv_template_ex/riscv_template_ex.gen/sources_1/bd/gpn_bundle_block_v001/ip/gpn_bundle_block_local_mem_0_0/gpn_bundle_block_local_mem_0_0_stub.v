// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Mon Aug 29 21:30:25 2022
// Host        : dl580 running 64-bit Ubuntu 18.04.6 LTS
// Command     : write_verilog -force -mode synth_stub
//               /home/user/MPS/riscv_kernel/riscv_kernel_kernels/vivado_rtl_kernel/riscv_template_ex/riscv_template_ex.gen/sources_1/bd/gpn_bundle_block_v001/ip/gpn_bundle_block_local_mem_0_0/gpn_bundle_block_local_mem_0_0_stub.v
// Design      : gpn_bundle_block_local_mem_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xcu200-fsgd2104-2-e
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "local_mem,Vivado 2020.2" *)
module gpn_bundle_block_local_mem_0_0(clk, rstn, portA_en, portA_be, portA_addr, 
  portA_data_in, portA_data_out, portB_en, portB_be, portB_addr, portB_data_in, portB_data_out)
/* synthesis syn_black_box black_box_pad_pin="clk,rstn,portA_en,portA_be[3:0],portA_addr[29:0],portA_data_in[31:0],portA_data_out[31:0],portB_en,portB_be[3:0],portB_addr[29:0],portB_data_in[31:0],portB_data_out[31:0]" */;
  input clk;
  input rstn;
  input portA_en;
  input [3:0]portA_be;
  input [29:0]portA_addr;
  input [31:0]portA_data_in;
  output [31:0]portA_data_out;
  input portB_en;
  input [3:0]portB_be;
  input [29:0]portB_addr;
  input [31:0]portB_data_in;
  output [31:0]portB_data_out;
endmodule
