//Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
//Date        : Mon Aug 29 21:26:25 2022
//Host        : dl580 running 64-bit Ubuntu 18.04.6 LTS
//Command     : generate_target gpn_bundle_block_wrapper.bd
//Design      : gpn_bundle_block_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module gpn_bundle_block_wrapper
   (ext_clk,
    ext_rstn,
    external_memory_bus_araddr,
    external_memory_bus_arburst,
    external_memory_bus_arcache,
    external_memory_bus_arid,
    external_memory_bus_arlen,
    external_memory_bus_arlock,
    external_memory_bus_arprot,
    external_memory_bus_arqos,
    external_memory_bus_arready,
    external_memory_bus_arregion,
    external_memory_bus_arsize,
    external_memory_bus_aruser,
    external_memory_bus_arvalid,
    external_memory_bus_awaddr,
    external_memory_bus_awburst,
    external_memory_bus_awcache,
    external_memory_bus_awid,
    external_memory_bus_awlen,
    external_memory_bus_awlock,
    external_memory_bus_awprot,
    external_memory_bus_awqos,
    external_memory_bus_awready,
    external_memory_bus_awregion,
    external_memory_bus_awsize,
    external_memory_bus_awuser,
    external_memory_bus_awvalid,
    external_memory_bus_bid,
    external_memory_bus_bready,
    external_memory_bus_bresp,
    external_memory_bus_buser,
    external_memory_bus_bvalid,
    external_memory_bus_rdata,
    external_memory_bus_rid,
    external_memory_bus_rlast,
    external_memory_bus_rready,
    external_memory_bus_rresp,
    external_memory_bus_ruser,
    external_memory_bus_rvalid,
    external_memory_bus_wdata,
    external_memory_bus_wlast,
    external_memory_bus_wready,
    external_memory_bus_wstrb,
    external_memory_bus_wuser,
    external_memory_bus_wvalid,
    global_memory_bus_araddr,
    global_memory_bus_arburst,
    global_memory_bus_arcache,
    global_memory_bus_arid,
    global_memory_bus_arlen,
    global_memory_bus_arlock,
    global_memory_bus_arprot,
    global_memory_bus_arqos,
    global_memory_bus_arready,
    global_memory_bus_arregion,
    global_memory_bus_arsize,
    global_memory_bus_aruser,
    global_memory_bus_arvalid,
    global_memory_bus_awaddr,
    global_memory_bus_awburst,
    global_memory_bus_awcache,
    global_memory_bus_awid,
    global_memory_bus_awlen,
    global_memory_bus_awlock,
    global_memory_bus_awprot,
    global_memory_bus_awqos,
    global_memory_bus_awready,
    global_memory_bus_awregion,
    global_memory_bus_awsize,
    global_memory_bus_awuser,
    global_memory_bus_awvalid,
    global_memory_bus_bid,
    global_memory_bus_bready,
    global_memory_bus_bresp,
    global_memory_bus_buser,
    global_memory_bus_bvalid,
    global_memory_bus_rdata,
    global_memory_bus_rid,
    global_memory_bus_rlast,
    global_memory_bus_rready,
    global_memory_bus_rresp,
    global_memory_bus_ruser,
    global_memory_bus_rvalid,
    global_memory_bus_wdata,
    global_memory_bus_wlast,
    global_memory_bus_wready,
    global_memory_bus_wstrb,
    global_memory_bus_wuser,
    global_memory_bus_wvalid,
    gpc2host_mq,
    gpc_control_reg,
    gpc_reset,
    gpc_start,
    gpc_state_reg,
    host2gpc_mq,
    kernel_clk,
    kernel_rstn,
    mq_ctrl,
    mq_sr);
  input ext_clk;
  input ext_rstn;
  output [31:0]external_memory_bus_araddr;
  output [1:0]external_memory_bus_arburst;
  output [3:0]external_memory_bus_arcache;
  output [5:0]external_memory_bus_arid;
  output [7:0]external_memory_bus_arlen;
  output [0:0]external_memory_bus_arlock;
  output [2:0]external_memory_bus_arprot;
  output [3:0]external_memory_bus_arqos;
  input external_memory_bus_arready;
  output [3:0]external_memory_bus_arregion;
  output [2:0]external_memory_bus_arsize;
  output [3:0]external_memory_bus_aruser;
  output external_memory_bus_arvalid;
  output [31:0]external_memory_bus_awaddr;
  output [1:0]external_memory_bus_awburst;
  output [3:0]external_memory_bus_awcache;
  output [5:0]external_memory_bus_awid;
  output [7:0]external_memory_bus_awlen;
  output [0:0]external_memory_bus_awlock;
  output [2:0]external_memory_bus_awprot;
  output [3:0]external_memory_bus_awqos;
  input external_memory_bus_awready;
  output [3:0]external_memory_bus_awregion;
  output [2:0]external_memory_bus_awsize;
  output [3:0]external_memory_bus_awuser;
  output external_memory_bus_awvalid;
  input [5:0]external_memory_bus_bid;
  output external_memory_bus_bready;
  input [1:0]external_memory_bus_bresp;
  input [3:0]external_memory_bus_buser;
  input external_memory_bus_bvalid;
  input [31:0]external_memory_bus_rdata;
  input [5:0]external_memory_bus_rid;
  input external_memory_bus_rlast;
  output external_memory_bus_rready;
  input [1:0]external_memory_bus_rresp;
  input [3:0]external_memory_bus_ruser;
  input external_memory_bus_rvalid;
  output [31:0]external_memory_bus_wdata;
  output external_memory_bus_wlast;
  input external_memory_bus_wready;
  output [3:0]external_memory_bus_wstrb;
  output [3:0]external_memory_bus_wuser;
  output external_memory_bus_wvalid;
  output [31:0]global_memory_bus_araddr;
  output [1:0]global_memory_bus_arburst;
  output [3:0]global_memory_bus_arcache;
  output [5:0]global_memory_bus_arid;
  output [7:0]global_memory_bus_arlen;
  output [0:0]global_memory_bus_arlock;
  output [2:0]global_memory_bus_arprot;
  output [3:0]global_memory_bus_arqos;
  input global_memory_bus_arready;
  output [3:0]global_memory_bus_arregion;
  output [2:0]global_memory_bus_arsize;
  output [3:0]global_memory_bus_aruser;
  output global_memory_bus_arvalid;
  output [31:0]global_memory_bus_awaddr;
  output [1:0]global_memory_bus_awburst;
  output [3:0]global_memory_bus_awcache;
  output [5:0]global_memory_bus_awid;
  output [7:0]global_memory_bus_awlen;
  output [0:0]global_memory_bus_awlock;
  output [2:0]global_memory_bus_awprot;
  output [3:0]global_memory_bus_awqos;
  input global_memory_bus_awready;
  output [3:0]global_memory_bus_awregion;
  output [2:0]global_memory_bus_awsize;
  output [3:0]global_memory_bus_awuser;
  output global_memory_bus_awvalid;
  input [5:0]global_memory_bus_bid;
  output global_memory_bus_bready;
  input [1:0]global_memory_bus_bresp;
  input [3:0]global_memory_bus_buser;
  input global_memory_bus_bvalid;
  input [31:0]global_memory_bus_rdata;
  input [5:0]global_memory_bus_rid;
  input global_memory_bus_rlast;
  output global_memory_bus_rready;
  input [1:0]global_memory_bus_rresp;
  input [3:0]global_memory_bus_ruser;
  input global_memory_bus_rvalid;
  output [31:0]global_memory_bus_wdata;
  output global_memory_bus_wlast;
  input global_memory_bus_wready;
  output [3:0]global_memory_bus_wstrb;
  output [3:0]global_memory_bus_wuser;
  output global_memory_bus_wvalid;
  output [31:0]gpc2host_mq;
  input [31:0]gpc_control_reg;
  input gpc_reset;
  input [0:0]gpc_start;
  output [0:0]gpc_state_reg;
  input [31:0]host2gpc_mq;
  input kernel_clk;
  input kernel_rstn;
  output [1:0]mq_ctrl;
  input [2:0]mq_sr;

  wire ext_clk;
  wire ext_rstn;
  wire [31:0]external_memory_bus_araddr;
  wire [1:0]external_memory_bus_arburst;
  wire [3:0]external_memory_bus_arcache;
  wire [5:0]external_memory_bus_arid;
  wire [7:0]external_memory_bus_arlen;
  wire [0:0]external_memory_bus_arlock;
  wire [2:0]external_memory_bus_arprot;
  wire [3:0]external_memory_bus_arqos;
  wire external_memory_bus_arready;
  wire [3:0]external_memory_bus_arregion;
  wire [2:0]external_memory_bus_arsize;
  wire [3:0]external_memory_bus_aruser;
  wire external_memory_bus_arvalid;
  wire [31:0]external_memory_bus_awaddr;
  wire [1:0]external_memory_bus_awburst;
  wire [3:0]external_memory_bus_awcache;
  wire [5:0]external_memory_bus_awid;
  wire [7:0]external_memory_bus_awlen;
  wire [0:0]external_memory_bus_awlock;
  wire [2:0]external_memory_bus_awprot;
  wire [3:0]external_memory_bus_awqos;
  wire external_memory_bus_awready;
  wire [3:0]external_memory_bus_awregion;
  wire [2:0]external_memory_bus_awsize;
  wire [3:0]external_memory_bus_awuser;
  wire external_memory_bus_awvalid;
  wire [5:0]external_memory_bus_bid;
  wire external_memory_bus_bready;
  wire [1:0]external_memory_bus_bresp;
  wire [3:0]external_memory_bus_buser;
  wire external_memory_bus_bvalid;
  wire [31:0]external_memory_bus_rdata;
  wire [5:0]external_memory_bus_rid;
  wire external_memory_bus_rlast;
  wire external_memory_bus_rready;
  wire [1:0]external_memory_bus_rresp;
  wire [3:0]external_memory_bus_ruser;
  wire external_memory_bus_rvalid;
  wire [31:0]external_memory_bus_wdata;
  wire external_memory_bus_wlast;
  wire external_memory_bus_wready;
  wire [3:0]external_memory_bus_wstrb;
  wire [3:0]external_memory_bus_wuser;
  wire external_memory_bus_wvalid;
  wire [31:0]global_memory_bus_araddr;
  wire [1:0]global_memory_bus_arburst;
  wire [3:0]global_memory_bus_arcache;
  wire [5:0]global_memory_bus_arid;
  wire [7:0]global_memory_bus_arlen;
  wire [0:0]global_memory_bus_arlock;
  wire [2:0]global_memory_bus_arprot;
  wire [3:0]global_memory_bus_arqos;
  wire global_memory_bus_arready;
  wire [3:0]global_memory_bus_arregion;
  wire [2:0]global_memory_bus_arsize;
  wire [3:0]global_memory_bus_aruser;
  wire global_memory_bus_arvalid;
  wire [31:0]global_memory_bus_awaddr;
  wire [1:0]global_memory_bus_awburst;
  wire [3:0]global_memory_bus_awcache;
  wire [5:0]global_memory_bus_awid;
  wire [7:0]global_memory_bus_awlen;
  wire [0:0]global_memory_bus_awlock;
  wire [2:0]global_memory_bus_awprot;
  wire [3:0]global_memory_bus_awqos;
  wire global_memory_bus_awready;
  wire [3:0]global_memory_bus_awregion;
  wire [2:0]global_memory_bus_awsize;
  wire [3:0]global_memory_bus_awuser;
  wire global_memory_bus_awvalid;
  wire [5:0]global_memory_bus_bid;
  wire global_memory_bus_bready;
  wire [1:0]global_memory_bus_bresp;
  wire [3:0]global_memory_bus_buser;
  wire global_memory_bus_bvalid;
  wire [31:0]global_memory_bus_rdata;
  wire [5:0]global_memory_bus_rid;
  wire global_memory_bus_rlast;
  wire global_memory_bus_rready;
  wire [1:0]global_memory_bus_rresp;
  wire [3:0]global_memory_bus_ruser;
  wire global_memory_bus_rvalid;
  wire [31:0]global_memory_bus_wdata;
  wire global_memory_bus_wlast;
  wire global_memory_bus_wready;
  wire [3:0]global_memory_bus_wstrb;
  wire [3:0]global_memory_bus_wuser;
  wire global_memory_bus_wvalid;
  wire [31:0]gpc2host_mq;
  wire [31:0]gpc_control_reg;
  wire gpc_reset;
  wire [0:0]gpc_start;
  wire [0:0]gpc_state_reg;
  wire [31:0]host2gpc_mq;
  wire kernel_clk;
  wire kernel_rstn;
  wire [1:0]mq_ctrl;
  wire [2:0]mq_sr;

  gpn_bundle_block gpn_bundle_block_i
       (.ext_clk(ext_clk),
        .ext_rstn(ext_rstn),
        .external_memory_bus_araddr(external_memory_bus_araddr),
        .external_memory_bus_arburst(external_memory_bus_arburst),
        .external_memory_bus_arcache(external_memory_bus_arcache),
        .external_memory_bus_arid(external_memory_bus_arid),
        .external_memory_bus_arlen(external_memory_bus_arlen),
        .external_memory_bus_arlock(external_memory_bus_arlock),
        .external_memory_bus_arprot(external_memory_bus_arprot),
        .external_memory_bus_arqos(external_memory_bus_arqos),
        .external_memory_bus_arready(external_memory_bus_arready),
        .external_memory_bus_arregion(external_memory_bus_arregion),
        .external_memory_bus_arsize(external_memory_bus_arsize),
        .external_memory_bus_aruser(external_memory_bus_aruser),
        .external_memory_bus_arvalid(external_memory_bus_arvalid),
        .external_memory_bus_awaddr(external_memory_bus_awaddr),
        .external_memory_bus_awburst(external_memory_bus_awburst),
        .external_memory_bus_awcache(external_memory_bus_awcache),
        .external_memory_bus_awid(external_memory_bus_awid),
        .external_memory_bus_awlen(external_memory_bus_awlen),
        .external_memory_bus_awlock(external_memory_bus_awlock),
        .external_memory_bus_awprot(external_memory_bus_awprot),
        .external_memory_bus_awqos(external_memory_bus_awqos),
        .external_memory_bus_awready(external_memory_bus_awready),
        .external_memory_bus_awregion(external_memory_bus_awregion),
        .external_memory_bus_awsize(external_memory_bus_awsize),
        .external_memory_bus_awuser(external_memory_bus_awuser),
        .external_memory_bus_awvalid(external_memory_bus_awvalid),
        .external_memory_bus_bid(external_memory_bus_bid),
        .external_memory_bus_bready(external_memory_bus_bready),
        .external_memory_bus_bresp(external_memory_bus_bresp),
        .external_memory_bus_buser(external_memory_bus_buser),
        .external_memory_bus_bvalid(external_memory_bus_bvalid),
        .external_memory_bus_rdata(external_memory_bus_rdata),
        .external_memory_bus_rid(external_memory_bus_rid),
        .external_memory_bus_rlast(external_memory_bus_rlast),
        .external_memory_bus_rready(external_memory_bus_rready),
        .external_memory_bus_rresp(external_memory_bus_rresp),
        .external_memory_bus_ruser(external_memory_bus_ruser),
        .external_memory_bus_rvalid(external_memory_bus_rvalid),
        .external_memory_bus_wdata(external_memory_bus_wdata),
        .external_memory_bus_wlast(external_memory_bus_wlast),
        .external_memory_bus_wready(external_memory_bus_wready),
        .external_memory_bus_wstrb(external_memory_bus_wstrb),
        .external_memory_bus_wuser(external_memory_bus_wuser),
        .external_memory_bus_wvalid(external_memory_bus_wvalid),
        .global_memory_bus_araddr(global_memory_bus_araddr),
        .global_memory_bus_arburst(global_memory_bus_arburst),
        .global_memory_bus_arcache(global_memory_bus_arcache),
        .global_memory_bus_arid(global_memory_bus_arid),
        .global_memory_bus_arlen(global_memory_bus_arlen),
        .global_memory_bus_arlock(global_memory_bus_arlock),
        .global_memory_bus_arprot(global_memory_bus_arprot),
        .global_memory_bus_arqos(global_memory_bus_arqos),
        .global_memory_bus_arready(global_memory_bus_arready),
        .global_memory_bus_arregion(global_memory_bus_arregion),
        .global_memory_bus_arsize(global_memory_bus_arsize),
        .global_memory_bus_aruser(global_memory_bus_aruser),
        .global_memory_bus_arvalid(global_memory_bus_arvalid),
        .global_memory_bus_awaddr(global_memory_bus_awaddr),
        .global_memory_bus_awburst(global_memory_bus_awburst),
        .global_memory_bus_awcache(global_memory_bus_awcache),
        .global_memory_bus_awid(global_memory_bus_awid),
        .global_memory_bus_awlen(global_memory_bus_awlen),
        .global_memory_bus_awlock(global_memory_bus_awlock),
        .global_memory_bus_awprot(global_memory_bus_awprot),
        .global_memory_bus_awqos(global_memory_bus_awqos),
        .global_memory_bus_awready(global_memory_bus_awready),
        .global_memory_bus_awregion(global_memory_bus_awregion),
        .global_memory_bus_awsize(global_memory_bus_awsize),
        .global_memory_bus_awuser(global_memory_bus_awuser),
        .global_memory_bus_awvalid(global_memory_bus_awvalid),
        .global_memory_bus_bid(global_memory_bus_bid),
        .global_memory_bus_bready(global_memory_bus_bready),
        .global_memory_bus_bresp(global_memory_bus_bresp),
        .global_memory_bus_buser(global_memory_bus_buser),
        .global_memory_bus_bvalid(global_memory_bus_bvalid),
        .global_memory_bus_rdata(global_memory_bus_rdata),
        .global_memory_bus_rid(global_memory_bus_rid),
        .global_memory_bus_rlast(global_memory_bus_rlast),
        .global_memory_bus_rready(global_memory_bus_rready),
        .global_memory_bus_rresp(global_memory_bus_rresp),
        .global_memory_bus_ruser(global_memory_bus_ruser),
        .global_memory_bus_rvalid(global_memory_bus_rvalid),
        .global_memory_bus_wdata(global_memory_bus_wdata),
        .global_memory_bus_wlast(global_memory_bus_wlast),
        .global_memory_bus_wready(global_memory_bus_wready),
        .global_memory_bus_wstrb(global_memory_bus_wstrb),
        .global_memory_bus_wuser(global_memory_bus_wuser),
        .global_memory_bus_wvalid(global_memory_bus_wvalid),
        .gpc2host_mq(gpc2host_mq),
        .gpc_control_reg(gpc_control_reg),
        .gpc_reset(gpc_reset),
        .gpc_start(gpc_start),
        .gpc_state_reg(gpc_state_reg),
        .host2gpc_mq(host2gpc_mq),
        .kernel_clk(kernel_clk),
        .kernel_rstn(kernel_rstn),
        .mq_ctrl(mq_ctrl),
        .mq_sr(mq_sr));
endmodule
