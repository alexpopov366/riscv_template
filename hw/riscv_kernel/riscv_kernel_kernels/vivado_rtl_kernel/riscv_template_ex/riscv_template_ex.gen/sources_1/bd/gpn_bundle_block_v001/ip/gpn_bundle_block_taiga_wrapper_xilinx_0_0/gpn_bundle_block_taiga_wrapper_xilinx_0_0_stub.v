// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Mon Aug 29 21:31:27 2022
// Host        : dl580 running 64-bit Ubuntu 18.04.6 LTS
// Command     : write_verilog -force -mode synth_stub
//               /home/user/MPS/riscv_kernel/riscv_kernel_kernels/vivado_rtl_kernel/riscv_template_ex/riscv_template_ex.gen/sources_1/bd/gpn_bundle_block_v001/ip/gpn_bundle_block_taiga_wrapper_xilinx_0_0/gpn_bundle_block_taiga_wrapper_xilinx_0_0_stub.v
// Design      : gpn_bundle_block_taiga_wrapper_xilinx_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xcu200-fsgd2104-2-e
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "taiga_wrapper_xilinx,Vivado 2020.2" *)
module gpn_bundle_block_taiga_wrapper_xilinx_0_0(clk, rst, instruction_bram_en, 
  instruction_bram_be, instruction_bram_addr, instruction_bram_data_in, 
  instruction_bram_data_out, instruction_rom_en, instruction_rom_be, 
  instruction_rom_addr, instruction_rom_data_in, instruction_rom_data_out, data_bram_en, 
  data_bram_be, data_bram_addr, data_bram_data_in, data_bram_data_out, m_axi_arready, 
  m_axi_arvalid, m_axi_araddr, m_axi_arlen, m_axi_arsize, m_axi_arburst, m_axi_arcache, 
  m_axi_arid, m_axi_arlock, m_axi_arprot, m_axi_arqos, m_axi_aruser, m_axi_rready, 
  m_axi_rvalid, m_axi_rdata, m_axi_rresp, m_axi_rlast, m_axi_rid, m_axi_ruser, m_axi_awready, 
  m_axi_awvalid, m_axi_awaddr, m_axi_awlen, m_axi_awsize, m_axi_awburst, m_axi_awcache, 
  m_axi_awid, m_axi_awlock, m_axi_awprot, m_axi_awqos, m_axi_awuser, m_axi_wready, 
  m_axi_wvalid, m_axi_wdata, m_axi_wstrb, m_axi_wlast, m_axi_wid, m_axi_wuser, m_axi_bready, 
  m_axi_bvalid, m_axi_bresp, m_axi_bid, m_axi_buser)
/* synthesis syn_black_box black_box_pad_pin="clk,rst,instruction_bram_en,instruction_bram_be[3:0],instruction_bram_addr[29:0],instruction_bram_data_in[31:0],instruction_bram_data_out[31:0],instruction_rom_en,instruction_rom_be[3:0],instruction_rom_addr[29:0],instruction_rom_data_in[31:0],instruction_rom_data_out[31:0],data_bram_en,data_bram_be[3:0],data_bram_addr[29:0],data_bram_data_in[31:0],data_bram_data_out[31:0],m_axi_arready,m_axi_arvalid,m_axi_araddr[31:0],m_axi_arlen[7:0],m_axi_arsize[2:0],m_axi_arburst[1:0],m_axi_arcache[3:0],m_axi_arid[5:0],m_axi_arlock,m_axi_arprot[2:0],m_axi_arqos[3:0],m_axi_aruser[3:0],m_axi_rready,m_axi_rvalid,m_axi_rdata[31:0],m_axi_rresp[1:0],m_axi_rlast,m_axi_rid[5:0],m_axi_ruser[3:0],m_axi_awready,m_axi_awvalid,m_axi_awaddr[31:0],m_axi_awlen[7:0],m_axi_awsize[2:0],m_axi_awburst[1:0],m_axi_awcache[3:0],m_axi_awid[5:0],m_axi_awlock,m_axi_awprot[2:0],m_axi_awqos[3:0],m_axi_awuser[3:0],m_axi_wready,m_axi_wvalid,m_axi_wdata[31:0],m_axi_wstrb[3:0],m_axi_wlast,m_axi_wid[5:0],m_axi_wuser[3:0],m_axi_bready,m_axi_bvalid,m_axi_bresp[1:0],m_axi_bid[5:0],m_axi_buser[3:0]" */;
  input clk;
  input rst;
  output instruction_bram_en;
  output [3:0]instruction_bram_be;
  output [29:0]instruction_bram_addr;
  output [31:0]instruction_bram_data_in;
  input [31:0]instruction_bram_data_out;
  output instruction_rom_en;
  output [3:0]instruction_rom_be;
  output [29:0]instruction_rom_addr;
  output [31:0]instruction_rom_data_in;
  input [31:0]instruction_rom_data_out;
  output data_bram_en;
  output [3:0]data_bram_be;
  output [29:0]data_bram_addr;
  output [31:0]data_bram_data_in;
  input [31:0]data_bram_data_out;
  input m_axi_arready;
  output m_axi_arvalid;
  output [31:0]m_axi_araddr;
  output [7:0]m_axi_arlen;
  output [2:0]m_axi_arsize;
  output [1:0]m_axi_arburst;
  output [3:0]m_axi_arcache;
  output [5:0]m_axi_arid;
  output m_axi_arlock;
  output [2:0]m_axi_arprot;
  output [3:0]m_axi_arqos;
  output [3:0]m_axi_aruser;
  output m_axi_rready;
  input m_axi_rvalid;
  input [31:0]m_axi_rdata;
  input [1:0]m_axi_rresp;
  input m_axi_rlast;
  input [5:0]m_axi_rid;
  input [3:0]m_axi_ruser;
  input m_axi_awready;
  output m_axi_awvalid;
  output [31:0]m_axi_awaddr;
  output [7:0]m_axi_awlen;
  output [2:0]m_axi_awsize;
  output [1:0]m_axi_awburst;
  output [3:0]m_axi_awcache;
  output [5:0]m_axi_awid;
  output m_axi_awlock;
  output [2:0]m_axi_awprot;
  output [3:0]m_axi_awqos;
  output [3:0]m_axi_awuser;
  input m_axi_wready;
  output m_axi_wvalid;
  output [31:0]m_axi_wdata;
  output [3:0]m_axi_wstrb;
  output m_axi_wlast;
  output [5:0]m_axi_wid;
  output [3:0]m_axi_wuser;
  output m_axi_bready;
  input m_axi_bvalid;
  input [1:0]m_axi_bresp;
  input [5:0]m_axi_bid;
  input [3:0]m_axi_buser;
endmodule
