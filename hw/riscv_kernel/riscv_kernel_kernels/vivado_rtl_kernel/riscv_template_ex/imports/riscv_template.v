// This is a generated file. Use and modify at your own risk.
//////////////////////////////////////////////////////////////////////////////// 
// default_nettype of none prevents implicit wire declaration.
`default_nettype none
`timescale 1 ns / 1 ps
// Top level of the kernel. Do not modify module name, parameters or ports.
module riscv_template #(

  parameter integer C_S_AXI_CONTROL_ADDR_WIDTH = 12 ,
  parameter integer C_S_AXI_CONTROL_DATA_WIDTH = 32 ,
  parameter integer C_GLOBAL_MEMORY_ADDR_WIDTH = 64,
  parameter integer C_GLOBAL_MEMORY_DATA_WIDTH = 32, 
  parameter integer C_EXTERNAL_MEMORY_ADDR_WIDTH = 64,
  parameter integer C_EXTERNAL_MEMORY_DATA_WIDTH = 32 
   
)
(
  // System Signals
  input  wire                                    ap_clk               ,
  input  wire                                    ap_rst_n             ,
  input  wire                                    ap_clk_2             ,
  input  wire                                    ap_rst_n_2           ,
  //  Note: A minimum subset of AXI4 memory mapped signals are declared.  AXI
  // signals omitted from these interfaces are automatically inferred with the
  // optimal values for Xilinx accleration platforms.  This allows Xilinx AXI4 Interconnects
  // within the system to be optimized by removing logic for AXI4 protocol
  // features that are not necessary. When adapting AXI4 masters within the RTL
  // kernel that have signals not declared below, it is suitable to add the
  // signals to the declarations below to connect them to the AXI4 Master.
  // 
  // List of ommited signals - effect
  // -------------------------------
  // ID - Transaction ID are used for multithreading and out of order
  // transactions.  This increases complexity. This saves logic and increases Fmax
  // in the system when ommited.
  // SIZE - Default value is log2(data width in bytes). Needed for subsize bursts.
  // This saves logic and increases Fmax in the system when ommited.
  // BURST - Default value (0b01) is incremental.  Wrap and fixed bursts are not
  // recommended. This saves logic and increases Fmax in the system when ommited.
  // LOCK - Not supported in AXI4
  // CACHE - Default value (0b0011) allows modifiable transactions. No benefit to
  // changing this.
  // PROT - Has no effect in current acceleration platforms.
  // QOS - Has no effect in current acceleration platforms.
  // REGION - Has no effect in current acceleration platforms.
  // USER - Has no effect in current acceleration platforms.
  // RESP - Not useful in most acceleration platforms.
  // 
  //IDs and USERs 
  /* 
  output wire [C_DDR4_BUS_ID_WIDTH-1 : 0]        ddr4_bus_arid         ,
  output wire [C_DDR4_BUS_ARUSER_WIDTH-1 : 0]    ddr4_bus_aruser       ,
  output wire [C_DDR4_BUS_ID_WIDTH-1 : 0]        ddr4_bus_awid         ,
  output wire [C_DDR4_BUS_AWUSER_WIDTH-1 : 0]    ddr4_bus_awuser       ,
  input wire  [C_DDR4_BUS_ID_WIDTH-1 : 0]        ddr4_bus_bid          ,
  input wire  [C_DDR4_BUS_BUSER_WIDTH-1 : 0]     ddr4_bus_buser        ,
  input wire  [C_DDR4_BUS_ID_WIDTH-1 : 0]        ddr4_bus_rid          ,
  input wire  [C_DDR4_BUS_RUSER_WIDTH-1 : 0]     ddr4_bus_ruser        ,
  output wire [C_DDR4_BUS_WUSER_WIDTH-1 : 0]     ddr4_bus_wuser        ,
  */
  
  // AXI4 master interface global_memory
  output wire                                    global_memory_awvalid,
  input  wire                                    global_memory_awready,
  output wire [C_GLOBAL_MEMORY_ADDR_WIDTH-1:0]   global_memory_awaddr ,
  output wire [8-1:0]                            global_memory_awlen  ,
  output wire                                    global_memory_wvalid ,
  input  wire                                    global_memory_wready ,
  output wire [C_GLOBAL_MEMORY_DATA_WIDTH-1:0]   global_memory_wdata  ,
  output wire [C_GLOBAL_MEMORY_DATA_WIDTH/8-1:0] global_memory_wstrb  ,
  output wire                                    global_memory_wlast  ,
  input  wire                                    global_memory_bvalid ,
  output wire                                    global_memory_bready ,
  output wire                                    global_memory_arvalid,
  input  wire                                    global_memory_arready,
  output wire [C_GLOBAL_MEMORY_ADDR_WIDTH-1:0]   global_memory_araddr ,
  output wire [8-1:0]                            global_memory_arlen  ,
  input  wire                                    global_memory_rvalid ,
  output wire                                    global_memory_rready ,
  input  wire [C_GLOBAL_MEMORY_DATA_WIDTH-1:0]   global_memory_rdata  ,
  input  wire                                    global_memory_rlast  ,
  // AXI4 master interface external_memory
  output wire                                    external_memory_awvalid,
  input  wire                                    external_memory_awready,
  output wire [C_EXTERNAL_MEMORY_ADDR_WIDTH-1:0] external_memory_awaddr ,
  output wire [8-1:0]                            external_memory_awlen  ,
  output wire                                    external_memory_wvalid ,
  input  wire                                    external_memory_wready ,
  output wire [C_EXTERNAL_MEMORY_DATA_WIDTH-1:0]   external_memory_wdata  ,
  output wire [C_EXTERNAL_MEMORY_DATA_WIDTH/8-1:0] external_memory_wstrb  ,
  output wire                                    external_memory_wlast  ,
  input  wire                                    external_memory_bvalid ,
  output wire                                    external_memory_bready ,
  output wire                                    external_memory_arvalid,
  input  wire                                    external_memory_arready,
  output wire [C_EXTERNAL_MEMORY_ADDR_WIDTH-1:0] external_memory_araddr ,
  output wire [8-1:0]                            external_memory_arlen  ,
  input  wire                                    external_memory_rvalid ,
  output wire                                    external_memory_rready ,
  input  wire [C_EXTERNAL_MEMORY_DATA_WIDTH-1:0] external_memory_rdata  ,
  input  wire                                    external_memory_rlast  ,  
  // AXI4-Lite slave interface
  input  wire                                    s_axi_control_awvalid,
  output wire                                    s_axi_control_awready,
  input  wire [C_S_AXI_CONTROL_ADDR_WIDTH-1:0]   s_axi_control_awaddr ,
  input  wire                                    s_axi_control_wvalid ,
  output wire                                    s_axi_control_wready ,
  input  wire [C_S_AXI_CONTROL_DATA_WIDTH-1:0]   s_axi_control_wdata  ,
  input  wire [C_S_AXI_CONTROL_DATA_WIDTH/8-1:0] s_axi_control_wstrb  ,
  input  wire                                    s_axi_control_arvalid,
  output wire                                    s_axi_control_arready,
  input  wire [C_S_AXI_CONTROL_ADDR_WIDTH-1:0]   s_axi_control_araddr ,
  output wire                                    s_axi_control_rvalid ,
  input  wire                                    s_axi_control_rready ,
  output wire [C_S_AXI_CONTROL_DATA_WIDTH-1:0]   s_axi_control_rdata  ,
  output wire [2-1:0]                            s_axi_control_rresp  ,
  output wire                                    s_axi_control_bvalid ,
  input  wire                                    s_axi_control_bready ,
  output wire [2-1:0]                            s_axi_control_bresp  ,
  output wire                                    interrupt            
);

///////////////////////////////////////////////////////////////////////////////
// Local Parameters
///////////////////////////////////////////////////////////////////////////////

localparam integer GPN_EXTERNAL_MEMORY_ADDR_WIDTH = 27;
localparam integer GPN_GLOBAL_MEMORY_ADDR_WIDTH = 17;

///////////////////////////////////////////////////////////////////////////////
// Wires and Variables
///////////////////////////////////////////////////////////////////////////////
(* DONT_TOUCH = "yes" *)
reg                                       areset                         = 1'b0;
wire                                      ap_start                      ;
reg                                       ap_start_int                  ;
reg                                       ap_idle                       ;
reg                                       ap_done                       ;
wire                                      ap_ready                      ;
wire [8-1:0]                              gpc_reset                     ;
wire [32-1:0]                             gpc_config                    ;
wire [64-1:0]                             ddr4_bus_ptr                  ;
wire [64-1:0]                             global_memory_ptr             ;
reg                                       gpc_reset_int                 ;
reg                                       gpc_reset_cdc                 ;
reg [32-1:0]                              gpc_config_int                ;
wire [GPN_GLOBAL_MEMORY_ADDR_WIDTH-1:0]   global_memory_araddr_int      ;
wire [GPN_GLOBAL_MEMORY_ADDR_WIDTH-1:0]   global_memory_awaddr_int      ;
wire [GPN_EXTERNAL_MEMORY_ADDR_WIDTH-1:0] external_memory_araddr_int      ;
wire [GPN_EXTERNAL_MEMORY_ADDR_WIDTH-1:0] external_memory_awaddr_int      ;
wire [8-1:0]                              gpc_state_reg                 ;
wire                                      ap_idle_int                   ;
reg                                       ap_idle_f1                    ;
reg                                       ap_idle_cdc                   ;
wire                                      ap_idle_rise                  ;
reg [1-1:0]                               ap_start_req                  ;
reg [7:0]                                 int_ap_start_counter          ;
wire [63:0]                               external_memory_ptr           ;
wire [31:0]                               host2gpc_mq_din               ;
wire [31:0]                               host2gpc_mq_dout              ;
wire                                      host2gpc_mq_write             ;
wire                                      host2gpc_mq_write_int         ;
reg                                       host2gpc_mq_write_int_ff      ;
wire                                      host2gpc_mq_next              ;
wire                                      host2gpc_mq_next_int          ;
reg                                       host2gpc_mq_next_int_ff       ;
wire [31:0]                               gpc2host_mq_din               ;
wire [31:0]                               gpc2host_mq_dout              ;
wire                                      gpc2host_mq_write             ;
wire                                      gpc2host_mq_write_int         ;
reg                                       gpc2host_mq_write_int_ff      ;
wire                                      gpc2host_mq_next              ;
wire                                      gpc2host_mq_next_int          ;
reg                                       gpc2host_mq_next_int_ff       ;
wire                                      host2gpc_mq_full              ;
wire                                      host2gpc_mq_afull             ;
wire                                      host2gpc_mq_empty             ;
wire                                      gpc2host_mq_full              ;
wire                                      gpc2host_mq_afull             ;
wire                                      gpc2host_mq_empty             ;
wire                                      mq_reset                      ;
wire [2:0]                                mq_sr                         ;
wire [1:0]                                mq_ctrl                       ;

//Reset FSM
localparam idle                     = 4'b0001;
localparam start_under_reset        = 4'b0010;
localparam wait_idle_from_gpn       = 4'b0100;
localparam done_from_gpn            = 4'b1000;
reg [3:0] kernel_state = idle;


// Register and invert reset signal.
always @(posedge ap_clk) begin
  areset <= ~ap_rst_n;
end

///////////////////////////////////////////////////////////////////////////////
// Begin control interface RTL.  Modifying not recommended.
///////////////////////////////////////////////////////////////////////////////

//GLOBAL MEMORY ARADDR
assign global_memory_araddr[C_GLOBAL_MEMORY_ADDR_WIDTH-1:GPN_GLOBAL_MEMORY_ADDR_WIDTH] = global_memory_ptr[C_GLOBAL_MEMORY_ADDR_WIDTH-1:GPN_GLOBAL_MEMORY_ADDR_WIDTH];
assign global_memory_araddr[GPN_GLOBAL_MEMORY_ADDR_WIDTH-1:0] = global_memory_araddr_int[GPN_GLOBAL_MEMORY_ADDR_WIDTH-1:0];
//GLOBAL MEMORY AWADDR
assign global_memory_awaddr[C_GLOBAL_MEMORY_ADDR_WIDTH-1:GPN_GLOBAL_MEMORY_ADDR_WIDTH] = global_memory_ptr[C_GLOBAL_MEMORY_ADDR_WIDTH-1:GPN_GLOBAL_MEMORY_ADDR_WIDTH];
assign global_memory_awaddr[GPN_GLOBAL_MEMORY_ADDR_WIDTH-1:0] = global_memory_awaddr_int[GPN_GLOBAL_MEMORY_ADDR_WIDTH-1:0];
//EXTERNAL MEMORY ARADDR
assign external_memory_araddr[C_EXTERNAL_MEMORY_ADDR_WIDTH-1:GPN_EXTERNAL_MEMORY_ADDR_WIDTH] = external_memory_ptr[C_EXTERNAL_MEMORY_ADDR_WIDTH-1:GPN_EXTERNAL_MEMORY_ADDR_WIDTH];
assign external_memory_araddr[GPN_EXTERNAL_MEMORY_ADDR_WIDTH-1:0] = external_memory_araddr_int[GPN_EXTERNAL_MEMORY_ADDR_WIDTH-1:0];
//EXTERNAL MEMORY AWADDR
assign external_memory_awaddr[C_EXTERNAL_MEMORY_ADDR_WIDTH-1:GPN_EXTERNAL_MEMORY_ADDR_WIDTH] = external_memory_ptr[C_EXTERNAL_MEMORY_ADDR_WIDTH-1:GPN_EXTERNAL_MEMORY_ADDR_WIDTH];
assign external_memory_awaddr[GPN_EXTERNAL_MEMORY_ADDR_WIDTH-1:0] = external_memory_awaddr_int[GPN_EXTERNAL_MEMORY_ADDR_WIDTH-1:0];


//RST FSM


always @(posedge ap_clk)
  if (~ap_rst_n) begin
        gpc_reset_int  <= 1'b1;
        ap_start_int <= 1'b0;
        ap_idle <= 1'b1;
        ap_done <= 1'b1;
  end
  else
     case (kernel_state)
        //Software raised udefined signals state 
        idle : begin
           if (gpc_reset[0]==1'b1 && ap_start==1'b1) //going to reset gpn
              kernel_state <= start_under_reset;
           else
              kernel_state <= idle; //normal work
            gpc_reset_int  <= 1'b0;
            ap_start_int <= ap_start;
            ap_idle <= ap_idle_cdc;
            ap_done <= ap_idle_rise;
        end
        //Start under reset has begun 
        start_under_reset : begin
           if (int_ap_start_counter[4]==1'b1)
              kernel_state <= wait_idle_from_gpn;
           else
              kernel_state <= start_under_reset;
            gpc_reset_int  <= 1'b1;
            ap_start_int <= 1'b1;
            ap_idle <= 1'b0;
            ap_done <= 1'b0;
        end
        //Start GPN bootloader 
        wait_idle_from_gpn : begin
           if (ap_idle_rise==1'b1) 
              kernel_state <= done_from_gpn;
           else
              kernel_state <= wait_idle_from_gpn;
            gpc_reset_int  <= 1'b0;
            ap_start_int <= 1'b1;
            ap_idle <= 1'b0;
            ap_done <= 1'b0;
        end
        //Finish GPN bootloader 
        done_from_gpn : begin
           if (ap_start==1'b0)
              kernel_state <= idle;
           else
              kernel_state <= done_from_gpn;
            gpc_reset_int  <= 1'b0;
            ap_start_int <= 1'b0;
            ap_idle <= 1'b1;
            ap_done <= 1'b1;
        end
        default: begin  // Fault Recovery
            gpc_reset_int  <= 1'b0;
            ap_start_int <= 1'b0;
            ap_idle <= 1'b1;
            ap_done <= 1'b1;
        end
     endcase


always @(posedge ap_clk) 
begin
    if (~ap_rst_n)
    begin
        ap_idle_cdc    <= 1;
        ap_idle_f1     <= 1;
    end
    else
    begin
        ap_idle_cdc <= gpc_state_reg[0];
        ap_idle_f1  <= ap_idle_cdc;
    end
end

assign ap_idle_rise     = ap_idle_cdc & ~ap_idle_f1;

// int_ap_start_counter

always @(posedge ap_clk) begin
    if (~ap_rst_n)
        int_ap_start_counter <= 0;
    else if (kernel_state==start_under_reset)
        int_ap_start_counter <= int_ap_start_counter + 1;
    else 
        int_ap_start_counter <= 0;
end


assign ap_ready         = ap_done;

//config cross domain

always @(posedge ap_clk) //_2
begin
    if (~ap_rst_n) //_2
    begin
        gpc_config_int <= 0;
        ap_start_req[0] <= 0;
        gpc_reset_cdc <= 0;
    end
    else
    begin
        gpc_config_int  <= gpc_config;
        ap_start_req[0] <= ap_start_int;
        gpc_reset_cdc <= gpc_reset_int;
    end
end

assign gpc2host_mq_next = gpc2host_mq_next_int & ~gpc2host_mq_next_int_ff;
assign host2gpc_mq_write = host2gpc_mq_write_int & ~host2gpc_mq_write_int_ff;

always @(posedge ap_clk) 
begin
    if (~ap_rst_n)
    begin
        gpc2host_mq_next_int_ff    <= 0;
        host2gpc_mq_write_int_ff   <= 0;
    end
    else
    begin
        gpc2host_mq_next_int_ff <= gpc2host_mq_next_int;
        host2gpc_mq_write_int_ff  <= host2gpc_mq_write_int;
    end
end 


//Host2GPC MQ
      QUEUE 
    # ( 
        .DATA_WIDTH(C_S_AXI_CONTROL_DATA_WIDTH),
        .ALMOST_FULL_LEVEL(255),
        .FIRST_WORD_FALL_THROUGH("TRUE"),
        .EMPTY_REG(0),
        .FULL_REG(0),
        .FIFO_TYPE("FIFO36E2"),
        .CLOCK_DOMAINS("COMMON")  // COMMON, INDEPENDENT
  )
    Host2GPC_MQ_inst
    (
       // Input Ports - Single Bit
       .CLK_IN     (ap_clk),  
       .CLK_INIT   (ap_clk),
       .CLK_OUT    (ap_clk), 
       .D_IN_VLD   (host2gpc_mq_write),
       .D_OUT_NXT  (host2gpc_mq_next),
       .RST        (gpc_reset),     
       // Input Ports - Busses
       .D_IN       (host2gpc_mq_din),
       // Output Ports - Single Bit
       .Q_EMPTY    (host2gpc_mq_empty), 
       .Q_FULL     (host2gpc_mq_full),  
       .Q_ALMOST_FULL     (host2gpc_mq_afull),  
       // Output Ports - Busses
       .D_OUT      (host2gpc_mq_dout)
       // InOut Ports - Single Bit
       // InOut Ports - Busses
    );

//GPC2Host MQ
      QUEUE 
    # ( 
        .DATA_WIDTH(C_S_AXI_CONTROL_DATA_WIDTH),
        .ALMOST_FULL_LEVEL(255),
        .ALMOST_EMPTY_LEVEL(127),
        .FIRST_WORD_FALL_THROUGH("TRUE"),
        .EMPTY_REG(0),
        .FULL_REG(0),
        .FIFO_TYPE("FIFO36E2"),
        .CLOCK_DOMAINS("COMMON")  // COMMON, INDEPENDENT
  )
    GPC2Host_MQ_inst
    (
       // Input Ports - Single Bit
       .CLK_IN     (ap_clk),  
       .CLK_INIT   (ap_clk),
       .CLK_OUT    (ap_clk), 
       .D_IN_VLD   (gpc2host_mq_write),
       .D_OUT_NXT  (gpc2host_mq_next),
       .RST        (gpc_reset),     
       // Input Ports - Busses
       .D_IN       (gpc2host_mq_din),
       // Output Ports - Single Bit
       .Q_EMPTY    (gpc2host_mq_empty), 
       .Q_FULL     (gpc2host_mq_full),  
       .Q_ALMOST_FULL     (gpc2host_mq_afull),  
       // Output Ports - Busses
       .D_OUT      (gpc2host_mq_dout)
       // InOut Ports - Single Bit
       // InOut Ports - Busses
    );
    
assign  mq_sr[0] = host2gpc_mq_empty;
assign  mq_sr[1] = gpc2host_mq_full;
assign  mq_sr[2] = gpc2host_mq_afull;
assign gpc2host_mq_write_int = mq_ctrl[0];
assign host2gpc_mq_next_int = mq_ctrl[1];
    
    
assign host2gpc_mq_next = host2gpc_mq_next_int & ~host2gpc_mq_next_int_ff;
assign gpc2host_mq_write = gpc2host_mq_write_int & ~gpc2host_mq_write_int_ff;

always @(posedge ap_clk)  //2
begin
    if (~ap_rst_n)
    begin
        host2gpc_mq_next_int_ff    <= 0;
        gpc2host_mq_write_int_ff   <= 0;
    end
    else
    begin
        host2gpc_mq_next_int_ff <= host2gpc_mq_next_int;
        gpc2host_mq_write_int_ff  <= gpc2host_mq_write_int;
    end
end   
    

// AXI4-Lite slave interface
riscv_template_control_s_axi #(

  .C_S_AXI_ADDR_WIDTH ( C_S_AXI_CONTROL_ADDR_WIDTH ),
  .C_S_AXI_DATA_WIDTH ( C_S_AXI_CONTROL_DATA_WIDTH )
)
inst_control_s_axi (
  .ACLK               ( ap_clk                ),
  .ARESET             ( areset                ),
  .ACLK_EN            ( 1'b1                  ),
  .AWVALID            ( s_axi_control_awvalid ),
  .AWREADY            ( s_axi_control_awready ),
  .AWADDR             ( s_axi_control_awaddr  ),
  .WVALID             ( s_axi_control_wvalid  ),
  .WREADY             ( s_axi_control_wready  ),
  .WDATA              ( s_axi_control_wdata   ),
  .WSTRB              ( s_axi_control_wstrb   ),
  .ARVALID            ( s_axi_control_arvalid ),
  .ARREADY            ( s_axi_control_arready ),
  .ARADDR             ( s_axi_control_araddr  ),
  .RVALID             ( s_axi_control_rvalid  ),
  .RREADY             ( s_axi_control_rready  ),
  .RDATA              ( s_axi_control_rdata   ),
  .RRESP              ( s_axi_control_rresp   ),
  .BVALID             ( s_axi_control_bvalid  ),
  .BREADY             ( s_axi_control_bready  ),
  .BRESP              ( s_axi_control_bresp   ),
  .interrupt          ( interrupt             ),
  .ap_start           ( ap_start              ),
  .ap_done            ( ap_done               ),
  .ap_ready           ( ap_ready              ),
  .ap_idle            ( ap_idle               ),
  .gpc_reset          ( gpc_reset             ),
  .gpc_config         ( gpc_config            ),
  .global_memory_ptr  ( global_memory_ptr     ),
  .external_memory_ptr( external_memory_ptr   ),
  .host2gpc_mq        ( host2gpc_mq_din       ),
  .host2gpc_mq_write  ( host2gpc_mq_write_int ),
  .gpc2host_mq        ( gpc2host_mq_dout      ),
  .gpc2host_mq_next   ( gpc2host_mq_next_int  ),
  .host2gpc_mq_full   ( host2gpc_mq_full      ),
  .host2gpc_mq_afull  ( host2gpc_mq_afull     ),
  .host2gpc_mq_empty  ( host2gpc_mq_empty     ),
  .gpc2host_mq_full   ( gpc2host_mq_full      ),
  .gpc2host_mq_afull  ( gpc2host_mq_afull     ),
  .gpc2host_mq_empty  ( gpc2host_mq_empty     ),
  .mq_reset           ( mq_reset              )
);

///////////////////////
//  GPN instantation
///////////////////////


  gpn_bundle_block gpn_bundle_block_i

       ( .ext_clk(ap_clk),
        .ext_rstn(ap_rst_n),
        .global_memory_bus_araddr(global_memory_araddr_int),
        .global_memory_bus_arburst(),
        .global_memory_bus_arcache(),
        .global_memory_bus_arid(),
        .global_memory_bus_arlen(global_memory_arlen),
        .global_memory_bus_arlock(),
        .global_memory_bus_arprot(),
        .global_memory_bus_arqos(),
        .global_memory_bus_arready(global_memory_arready),
        .global_memory_bus_arregion(),
        .global_memory_bus_arsize(),
        .global_memory_bus_aruser(),
        .global_memory_bus_arvalid(global_memory_arvalid),
        .global_memory_bus_awaddr(global_memory_awaddr_int),
        .global_memory_bus_awburst(),
        .global_memory_bus_awcache(),
        .global_memory_bus_awid(),
        .global_memory_bus_awlen(global_memory_awlen),
        .global_memory_bus_awlock(),
        .global_memory_bus_awprot(),
        .global_memory_bus_awqos(),
        .global_memory_bus_awready(global_memory_awready),
        .global_memory_bus_awregion(),
        .global_memory_bus_awsize(),
        .global_memory_bus_awuser(),
        .global_memory_bus_awvalid(global_memory_awvalid),
        .global_memory_bus_bid(),
        .global_memory_bus_bready(global_memory_bready),
        .global_memory_bus_bresp(2'b0),
        .global_memory_bus_buser(),
        .global_memory_bus_bvalid(global_memory_bvalid),
        .global_memory_bus_rdata(global_memory_rdata),
        .global_memory_bus_rid(),
        .global_memory_bus_rlast(global_memory_rlast),
        .global_memory_bus_rready(global_memory_rready),
        .global_memory_bus_rresp(2'b0),
        .global_memory_bus_ruser(),
        .global_memory_bus_rvalid(global_memory_rvalid),
        .global_memory_bus_wdata(global_memory_wdata),
        .global_memory_bus_wlast(global_memory_wlast),
        .global_memory_bus_wready(global_memory_wready),
        .global_memory_bus_wstrb(global_memory_wstrb),
        .global_memory_bus_wuser(),
        .global_memory_bus_wvalid(global_memory_wvalid),
        .external_memory_bus_araddr(external_memory_araddr_int),
        .external_memory_bus_arburst(),
        .external_memory_bus_arcache(),
        .external_memory_bus_arid(),
        .external_memory_bus_arlen(external_memory_arlen),
        .external_memory_bus_arlock(),
        .external_memory_bus_arprot(),
        .external_memory_bus_arqos(),
        .external_memory_bus_arready(external_memory_arready),
        .external_memory_bus_arregion(),
        .external_memory_bus_arsize(),
        .external_memory_bus_aruser(),
        .external_memory_bus_arvalid(external_memory_arvalid),
        .external_memory_bus_awaddr(external_memory_awaddr_int),
        .external_memory_bus_awburst(),
        .external_memory_bus_awcache(),
        .external_memory_bus_awid(),
        .external_memory_bus_awlen(external_memory_awlen),
        .external_memory_bus_awlock(),
        .external_memory_bus_awprot(),
        .external_memory_bus_awqos(),
        .external_memory_bus_awready(external_memory_awready),
        .external_memory_bus_awregion(),
        .external_memory_bus_awsize(),
        .external_memory_bus_awuser(),
        .external_memory_bus_awvalid(external_memory_awvalid),
        .external_memory_bus_bid(),
        .external_memory_bus_bready(external_memory_bready),
        .external_memory_bus_bresp(2'b0),
        .external_memory_bus_buser(),
        .external_memory_bus_bvalid(external_memory_bvalid),
        .external_memory_bus_rdata(external_memory_rdata),
        .external_memory_bus_rid(),
        .external_memory_bus_rlast(external_memory_rlast),
        .external_memory_bus_rready(external_memory_rready),
        .external_memory_bus_rresp(2'b0),
        .external_memory_bus_ruser(),
        .external_memory_bus_rvalid(external_memory_rvalid),
        .external_memory_bus_wdata(external_memory_wdata),
        .external_memory_bus_wlast(external_memory_wlast),
        .external_memory_bus_wready(external_memory_wready),
        .external_memory_bus_wstrb(external_memory_wstrb),
        .external_memory_bus_wuser(),
        .external_memory_bus_wvalid(external_memory_wvalid),
        .gpc2host_mq(gpc2host_mq_din),
        .mq_sr(mq_sr),
        .mq_ctrl(mq_ctrl),
        .host2gpc_mq(host2gpc_mq_dout),
        .gpc_reset(gpc_reset_cdc),
        .gpc_control_reg(gpc_config_int),
        .gpc_start(ap_start_req),
        .gpc_state_reg(gpc_state_reg),
        .kernel_clk(ap_clk), //_2
        .kernel_rstn(ap_rst_n)); //_2










endmodule
`default_nettype wire