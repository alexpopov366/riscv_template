// ==============================================================
// Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2020.2 (64-bit)
// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef __linux__

#include "xstatus.h"
#include "xparameters.h"
#include "xriscv_template.h"

extern XRiscv_template_Config XRiscv_template_ConfigTable[];

XRiscv_template_Config *XRiscv_template_LookupConfig(u16 DeviceId) {
	XRiscv_template_Config *ConfigPtr = NULL;

	int Index;

	for (Index = 0; Index < XPAR_XRISCV_TEMPLATE_NUM_INSTANCES; Index++) {
		if (XRiscv_template_ConfigTable[Index].DeviceId == DeviceId) {
			ConfigPtr = &XRiscv_template_ConfigTable[Index];
			break;
		}
	}

	return ConfigPtr;
}

int XRiscv_template_Initialize(XRiscv_template *InstancePtr, u16 DeviceId) {
	XRiscv_template_Config *ConfigPtr;

	Xil_AssertNonvoid(InstancePtr != NULL);

	ConfigPtr = XRiscv_template_LookupConfig(DeviceId);
	if (ConfigPtr == NULL) {
		InstancePtr->IsReady = 0;
		return (XST_DEVICE_NOT_FOUND);
	}

	return XRiscv_template_CfgInitialize(InstancePtr, ConfigPtr);
}

#endif

