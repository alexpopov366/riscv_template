# This script segment is generated automatically by AutoPilot

set axilite_register_dict [dict create]
set port_control {
gpn_reset { 
	dir I
	width 8
	depth 1
	mode ap_none
	offset 16
	offset_end 23
}
gpn_config { 
	dir I
	width 32
	depth 1
	mode ap_none
	offset 24
	offset_end 31
}
global_memory_ptr { 
	dir I
	width 64
	depth 1
	mode ap_none
	offset 32
	offset_end 43
}
external_memory_ptr { 
	dir I
	width 64
	depth 1
	mode ap_none
	offset 44
	offset_end 55
}
ap_start { }
ap_done { }
ap_ready { }
ap_idle { }
}
dict set axilite_register_dict control $port_control


