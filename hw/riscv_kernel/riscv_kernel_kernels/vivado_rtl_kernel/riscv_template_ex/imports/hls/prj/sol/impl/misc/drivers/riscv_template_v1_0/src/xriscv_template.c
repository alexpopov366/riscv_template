// ==============================================================
// Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2020.2 (64-bit)
// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// ==============================================================
/***************************** Include Files *********************************/
#include "xriscv_template.h"

/************************** Function Implementation *************************/
#ifndef __linux__
int XRiscv_template_CfgInitialize(XRiscv_template *InstancePtr, XRiscv_template_Config *ConfigPtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(ConfigPtr != NULL);

    InstancePtr->Control_BaseAddress = ConfigPtr->Control_BaseAddress;
    InstancePtr->IsReady = XIL_COMPONENT_IS_READY;

    return XST_SUCCESS;
}
#endif

void XRiscv_template_Start(XRiscv_template *InstancePtr) {
    u32 Data;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XRiscv_template_ReadReg(InstancePtr->Control_BaseAddress, XRISCV_TEMPLATE_CONTROL_ADDR_AP_CTRL) & 0x80;
    XRiscv_template_WriteReg(InstancePtr->Control_BaseAddress, XRISCV_TEMPLATE_CONTROL_ADDR_AP_CTRL, Data | 0x01);
}

u32 XRiscv_template_IsDone(XRiscv_template *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XRiscv_template_ReadReg(InstancePtr->Control_BaseAddress, XRISCV_TEMPLATE_CONTROL_ADDR_AP_CTRL);
    return (Data >> 1) & 0x1;
}

u32 XRiscv_template_IsIdle(XRiscv_template *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XRiscv_template_ReadReg(InstancePtr->Control_BaseAddress, XRISCV_TEMPLATE_CONTROL_ADDR_AP_CTRL);
    return (Data >> 2) & 0x1;
}

u32 XRiscv_template_IsReady(XRiscv_template *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XRiscv_template_ReadReg(InstancePtr->Control_BaseAddress, XRISCV_TEMPLATE_CONTROL_ADDR_AP_CTRL);
    // check ap_start to see if the pcore is ready for next input
    return !(Data & 0x1);
}

void XRiscv_template_EnableAutoRestart(XRiscv_template *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XRiscv_template_WriteReg(InstancePtr->Control_BaseAddress, XRISCV_TEMPLATE_CONTROL_ADDR_AP_CTRL, 0x80);
}

void XRiscv_template_DisableAutoRestart(XRiscv_template *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XRiscv_template_WriteReg(InstancePtr->Control_BaseAddress, XRISCV_TEMPLATE_CONTROL_ADDR_AP_CTRL, 0);
}

void XRiscv_template_Set_gpn_reset(XRiscv_template *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XRiscv_template_WriteReg(InstancePtr->Control_BaseAddress, XRISCV_TEMPLATE_CONTROL_ADDR_GPN_RESET_DATA, Data);
}

u32 XRiscv_template_Get_gpn_reset(XRiscv_template *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XRiscv_template_ReadReg(InstancePtr->Control_BaseAddress, XRISCV_TEMPLATE_CONTROL_ADDR_GPN_RESET_DATA);
    return Data;
}

void XRiscv_template_Set_gpn_config(XRiscv_template *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XRiscv_template_WriteReg(InstancePtr->Control_BaseAddress, XRISCV_TEMPLATE_CONTROL_ADDR_GPN_CONFIG_DATA, Data);
}

u32 XRiscv_template_Get_gpn_config(XRiscv_template *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XRiscv_template_ReadReg(InstancePtr->Control_BaseAddress, XRISCV_TEMPLATE_CONTROL_ADDR_GPN_CONFIG_DATA);
    return Data;
}

void XRiscv_template_Set_global_memory_ptr(XRiscv_template *InstancePtr, u64 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XRiscv_template_WriteReg(InstancePtr->Control_BaseAddress, XRISCV_TEMPLATE_CONTROL_ADDR_GLOBAL_MEMORY_PTR_DATA, (u32)(Data));
    XRiscv_template_WriteReg(InstancePtr->Control_BaseAddress, XRISCV_TEMPLATE_CONTROL_ADDR_GLOBAL_MEMORY_PTR_DATA + 4, (u32)(Data >> 32));
}

u64 XRiscv_template_Get_global_memory_ptr(XRiscv_template *InstancePtr) {
    u64 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XRiscv_template_ReadReg(InstancePtr->Control_BaseAddress, XRISCV_TEMPLATE_CONTROL_ADDR_GLOBAL_MEMORY_PTR_DATA);
    Data += (u64)XRiscv_template_ReadReg(InstancePtr->Control_BaseAddress, XRISCV_TEMPLATE_CONTROL_ADDR_GLOBAL_MEMORY_PTR_DATA + 4) << 32;
    return Data;
}

void XRiscv_template_Set_external_memory_ptr(XRiscv_template *InstancePtr, u64 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XRiscv_template_WriteReg(InstancePtr->Control_BaseAddress, XRISCV_TEMPLATE_CONTROL_ADDR_EXTERNAL_MEMORY_PTR_DATA, (u32)(Data));
    XRiscv_template_WriteReg(InstancePtr->Control_BaseAddress, XRISCV_TEMPLATE_CONTROL_ADDR_EXTERNAL_MEMORY_PTR_DATA + 4, (u32)(Data >> 32));
}

u64 XRiscv_template_Get_external_memory_ptr(XRiscv_template *InstancePtr) {
    u64 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XRiscv_template_ReadReg(InstancePtr->Control_BaseAddress, XRISCV_TEMPLATE_CONTROL_ADDR_EXTERNAL_MEMORY_PTR_DATA);
    Data += (u64)XRiscv_template_ReadReg(InstancePtr->Control_BaseAddress, XRISCV_TEMPLATE_CONTROL_ADDR_EXTERNAL_MEMORY_PTR_DATA + 4) << 32;
    return Data;
}

void XRiscv_template_InterruptGlobalEnable(XRiscv_template *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XRiscv_template_WriteReg(InstancePtr->Control_BaseAddress, XRISCV_TEMPLATE_CONTROL_ADDR_GIE, 1);
}

void XRiscv_template_InterruptGlobalDisable(XRiscv_template *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XRiscv_template_WriteReg(InstancePtr->Control_BaseAddress, XRISCV_TEMPLATE_CONTROL_ADDR_GIE, 0);
}

void XRiscv_template_InterruptEnable(XRiscv_template *InstancePtr, u32 Mask) {
    u32 Register;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Register =  XRiscv_template_ReadReg(InstancePtr->Control_BaseAddress, XRISCV_TEMPLATE_CONTROL_ADDR_IER);
    XRiscv_template_WriteReg(InstancePtr->Control_BaseAddress, XRISCV_TEMPLATE_CONTROL_ADDR_IER, Register | Mask);
}

void XRiscv_template_InterruptDisable(XRiscv_template *InstancePtr, u32 Mask) {
    u32 Register;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Register =  XRiscv_template_ReadReg(InstancePtr->Control_BaseAddress, XRISCV_TEMPLATE_CONTROL_ADDR_IER);
    XRiscv_template_WriteReg(InstancePtr->Control_BaseAddress, XRISCV_TEMPLATE_CONTROL_ADDR_IER, Register & (~Mask));
}

void XRiscv_template_InterruptClear(XRiscv_template *InstancePtr, u32 Mask) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XRiscv_template_WriteReg(InstancePtr->Control_BaseAddress, XRISCV_TEMPLATE_CONTROL_ADDR_ISR, Mask);
}

u32 XRiscv_template_InterruptGetEnabled(XRiscv_template *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XRiscv_template_ReadReg(InstancePtr->Control_BaseAddress, XRISCV_TEMPLATE_CONTROL_ADDR_IER);
}

u32 XRiscv_template_InterruptGetStatus(XRiscv_template *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XRiscv_template_ReadReg(InstancePtr->Control_BaseAddress, XRISCV_TEMPLATE_CONTROL_ADDR_ISR);
}

