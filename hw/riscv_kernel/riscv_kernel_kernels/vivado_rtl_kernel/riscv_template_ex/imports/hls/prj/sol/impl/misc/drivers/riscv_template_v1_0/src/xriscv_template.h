// ==============================================================
// Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2020.2 (64-bit)
// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef XRISCV_TEMPLATE_H
#define XRISCV_TEMPLATE_H

#ifdef __cplusplus
extern "C" {
#endif

/***************************** Include Files *********************************/
#ifndef __linux__
#include "xil_types.h"
#include "xil_assert.h"
#include "xstatus.h"
#include "xil_io.h"
#else
#include <stdint.h>
#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stddef.h>
#endif
#include "xriscv_template_hw.h"

/**************************** Type Definitions ******************************/
#ifdef __linux__
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
#else
typedef struct {
    u16 DeviceId;
    u32 Control_BaseAddress;
} XRiscv_template_Config;
#endif

typedef struct {
    u64 Control_BaseAddress;
    u32 IsReady;
} XRiscv_template;

typedef u32 word_type;

/***************** Macros (Inline Functions) Definitions *********************/
#ifndef __linux__
#define XRiscv_template_WriteReg(BaseAddress, RegOffset, Data) \
    Xil_Out32((BaseAddress) + (RegOffset), (u32)(Data))
#define XRiscv_template_ReadReg(BaseAddress, RegOffset) \
    Xil_In32((BaseAddress) + (RegOffset))
#else
#define XRiscv_template_WriteReg(BaseAddress, RegOffset, Data) \
    *(volatile u32*)((BaseAddress) + (RegOffset)) = (u32)(Data)
#define XRiscv_template_ReadReg(BaseAddress, RegOffset) \
    *(volatile u32*)((BaseAddress) + (RegOffset))

#define Xil_AssertVoid(expr)    assert(expr)
#define Xil_AssertNonvoid(expr) assert(expr)

#define XST_SUCCESS             0
#define XST_DEVICE_NOT_FOUND    2
#define XST_OPEN_DEVICE_FAILED  3
#define XIL_COMPONENT_IS_READY  1
#endif

/************************** Function Prototypes *****************************/
#ifndef __linux__
int XRiscv_template_Initialize(XRiscv_template *InstancePtr, u16 DeviceId);
XRiscv_template_Config* XRiscv_template_LookupConfig(u16 DeviceId);
int XRiscv_template_CfgInitialize(XRiscv_template *InstancePtr, XRiscv_template_Config *ConfigPtr);
#else
int XRiscv_template_Initialize(XRiscv_template *InstancePtr, const char* InstanceName);
int XRiscv_template_Release(XRiscv_template *InstancePtr);
#endif

void XRiscv_template_Start(XRiscv_template *InstancePtr);
u32 XRiscv_template_IsDone(XRiscv_template *InstancePtr);
u32 XRiscv_template_IsIdle(XRiscv_template *InstancePtr);
u32 XRiscv_template_IsReady(XRiscv_template *InstancePtr);
void XRiscv_template_EnableAutoRestart(XRiscv_template *InstancePtr);
void XRiscv_template_DisableAutoRestart(XRiscv_template *InstancePtr);

void XRiscv_template_Set_gpn_reset(XRiscv_template *InstancePtr, u32 Data);
u32 XRiscv_template_Get_gpn_reset(XRiscv_template *InstancePtr);
void XRiscv_template_Set_gpn_config(XRiscv_template *InstancePtr, u32 Data);
u32 XRiscv_template_Get_gpn_config(XRiscv_template *InstancePtr);
void XRiscv_template_Set_global_memory_ptr(XRiscv_template *InstancePtr, u64 Data);
u64 XRiscv_template_Get_global_memory_ptr(XRiscv_template *InstancePtr);
void XRiscv_template_Set_external_memory_ptr(XRiscv_template *InstancePtr, u64 Data);
u64 XRiscv_template_Get_external_memory_ptr(XRiscv_template *InstancePtr);

void XRiscv_template_InterruptGlobalEnable(XRiscv_template *InstancePtr);
void XRiscv_template_InterruptGlobalDisable(XRiscv_template *InstancePtr);
void XRiscv_template_InterruptEnable(XRiscv_template *InstancePtr, u32 Mask);
void XRiscv_template_InterruptDisable(XRiscv_template *InstancePtr, u32 Mask);
void XRiscv_template_InterruptClear(XRiscv_template *InstancePtr, u32 Mask);
u32 XRiscv_template_InterruptGetEnabled(XRiscv_template *InstancePtr);
u32 XRiscv_template_InterruptGetStatus(XRiscv_template *InstancePtr);

#ifdef __cplusplus
}
#endif

#endif
