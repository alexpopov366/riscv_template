set moduleName riscv_template
set isTopModule 1
set isCombinational 0
set isDatapathOnly 0
set isPipelined 0
set pipeline_type none
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {riscv_template}
set C_modelType { void 0 }
set C_modelArgList {
	{ global_memory int 512 regular {axi_master 2}  }
	{ external_memory int 512 regular {axi_master 2}  }
	{ gpn_reset uint 8 unused {axi_slave 0}  }
	{ gpn_config int 32 unused {axi_slave 0}  }
	{ global_memory_ptr int 64 regular {axi_slave 0}  }
	{ external_memory_ptr int 64 regular {axi_slave 0}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "global_memory", "interface" : "axi_master", "bitwidth" : 512, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "global_memory_ptr","cData": "int512","bit_use": { "low": 0,"up": 0},"offset": { "type": "dynamic","port_name": "global_memory_ptr","bundle": "control"},"direction": "READWRITE","cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}]} , 
 	{ "Name" : "external_memory", "interface" : "axi_master", "bitwidth" : 512, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "external_memory_ptr","cData": "int512","bit_use": { "low": 0,"up": 0},"offset": { "type": "dynamic","port_name": "external_memory_ptr","bundle": "control"},"direction": "READWRITE","cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}]} , 
 	{ "Name" : "gpn_reset", "interface" : "axi_slave", "bundle":"control","type":"ap_none","bitwidth" : 8, "direction" : "READONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "gpn_reset","cData": "char","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}], "offset" : {"in":16}, "offset_end" : {"in":23}} , 
 	{ "Name" : "gpn_config", "interface" : "axi_slave", "bundle":"control","type":"ap_none","bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "gpn_config","cData": "int","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}], "offset" : {"in":24}, "offset_end" : {"in":31}} , 
 	{ "Name" : "global_memory_ptr", "interface" : "axi_slave", "bundle":"control","type":"ap_none","bitwidth" : 64, "direction" : "READONLY", "offset" : {"in":32}, "offset_end" : {"in":43}} , 
 	{ "Name" : "external_memory_ptr", "interface" : "axi_slave", "bundle":"control","type":"ap_none","bitwidth" : 64, "direction" : "READONLY", "offset" : {"in":44}, "offset_end" : {"in":55}} ]}
# RTL Port declarations: 
set portNum 110
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst_n sc_in sc_logic 1 reset -1 active_low_sync } 
	{ m_axi_global_memory_AWVALID sc_out sc_logic 1 signal 0 } 
	{ m_axi_global_memory_AWREADY sc_in sc_logic 1 signal 0 } 
	{ m_axi_global_memory_AWADDR sc_out sc_lv 64 signal 0 } 
	{ m_axi_global_memory_AWID sc_out sc_lv 1 signal 0 } 
	{ m_axi_global_memory_AWLEN sc_out sc_lv 8 signal 0 } 
	{ m_axi_global_memory_AWSIZE sc_out sc_lv 3 signal 0 } 
	{ m_axi_global_memory_AWBURST sc_out sc_lv 2 signal 0 } 
	{ m_axi_global_memory_AWLOCK sc_out sc_lv 2 signal 0 } 
	{ m_axi_global_memory_AWCACHE sc_out sc_lv 4 signal 0 } 
	{ m_axi_global_memory_AWPROT sc_out sc_lv 3 signal 0 } 
	{ m_axi_global_memory_AWQOS sc_out sc_lv 4 signal 0 } 
	{ m_axi_global_memory_AWREGION sc_out sc_lv 4 signal 0 } 
	{ m_axi_global_memory_AWUSER sc_out sc_lv 1 signal 0 } 
	{ m_axi_global_memory_WVALID sc_out sc_logic 1 signal 0 } 
	{ m_axi_global_memory_WREADY sc_in sc_logic 1 signal 0 } 
	{ m_axi_global_memory_WDATA sc_out sc_lv 512 signal 0 } 
	{ m_axi_global_memory_WSTRB sc_out sc_lv 64 signal 0 } 
	{ m_axi_global_memory_WLAST sc_out sc_logic 1 signal 0 } 
	{ m_axi_global_memory_WID sc_out sc_lv 1 signal 0 } 
	{ m_axi_global_memory_WUSER sc_out sc_lv 1 signal 0 } 
	{ m_axi_global_memory_ARVALID sc_out sc_logic 1 signal 0 } 
	{ m_axi_global_memory_ARREADY sc_in sc_logic 1 signal 0 } 
	{ m_axi_global_memory_ARADDR sc_out sc_lv 64 signal 0 } 
	{ m_axi_global_memory_ARID sc_out sc_lv 1 signal 0 } 
	{ m_axi_global_memory_ARLEN sc_out sc_lv 8 signal 0 } 
	{ m_axi_global_memory_ARSIZE sc_out sc_lv 3 signal 0 } 
	{ m_axi_global_memory_ARBURST sc_out sc_lv 2 signal 0 } 
	{ m_axi_global_memory_ARLOCK sc_out sc_lv 2 signal 0 } 
	{ m_axi_global_memory_ARCACHE sc_out sc_lv 4 signal 0 } 
	{ m_axi_global_memory_ARPROT sc_out sc_lv 3 signal 0 } 
	{ m_axi_global_memory_ARQOS sc_out sc_lv 4 signal 0 } 
	{ m_axi_global_memory_ARREGION sc_out sc_lv 4 signal 0 } 
	{ m_axi_global_memory_ARUSER sc_out sc_lv 1 signal 0 } 
	{ m_axi_global_memory_RVALID sc_in sc_logic 1 signal 0 } 
	{ m_axi_global_memory_RREADY sc_out sc_logic 1 signal 0 } 
	{ m_axi_global_memory_RDATA sc_in sc_lv 512 signal 0 } 
	{ m_axi_global_memory_RLAST sc_in sc_logic 1 signal 0 } 
	{ m_axi_global_memory_RID sc_in sc_lv 1 signal 0 } 
	{ m_axi_global_memory_RUSER sc_in sc_lv 1 signal 0 } 
	{ m_axi_global_memory_RRESP sc_in sc_lv 2 signal 0 } 
	{ m_axi_global_memory_BVALID sc_in sc_logic 1 signal 0 } 
	{ m_axi_global_memory_BREADY sc_out sc_logic 1 signal 0 } 
	{ m_axi_global_memory_BRESP sc_in sc_lv 2 signal 0 } 
	{ m_axi_global_memory_BID sc_in sc_lv 1 signal 0 } 
	{ m_axi_global_memory_BUSER sc_in sc_lv 1 signal 0 } 
	{ m_axi_external_memory_AWVALID sc_out sc_logic 1 signal 1 } 
	{ m_axi_external_memory_AWREADY sc_in sc_logic 1 signal 1 } 
	{ m_axi_external_memory_AWADDR sc_out sc_lv 64 signal 1 } 
	{ m_axi_external_memory_AWID sc_out sc_lv 1 signal 1 } 
	{ m_axi_external_memory_AWLEN sc_out sc_lv 8 signal 1 } 
	{ m_axi_external_memory_AWSIZE sc_out sc_lv 3 signal 1 } 
	{ m_axi_external_memory_AWBURST sc_out sc_lv 2 signal 1 } 
	{ m_axi_external_memory_AWLOCK sc_out sc_lv 2 signal 1 } 
	{ m_axi_external_memory_AWCACHE sc_out sc_lv 4 signal 1 } 
	{ m_axi_external_memory_AWPROT sc_out sc_lv 3 signal 1 } 
	{ m_axi_external_memory_AWQOS sc_out sc_lv 4 signal 1 } 
	{ m_axi_external_memory_AWREGION sc_out sc_lv 4 signal 1 } 
	{ m_axi_external_memory_AWUSER sc_out sc_lv 1 signal 1 } 
	{ m_axi_external_memory_WVALID sc_out sc_logic 1 signal 1 } 
	{ m_axi_external_memory_WREADY sc_in sc_logic 1 signal 1 } 
	{ m_axi_external_memory_WDATA sc_out sc_lv 512 signal 1 } 
	{ m_axi_external_memory_WSTRB sc_out sc_lv 64 signal 1 } 
	{ m_axi_external_memory_WLAST sc_out sc_logic 1 signal 1 } 
	{ m_axi_external_memory_WID sc_out sc_lv 1 signal 1 } 
	{ m_axi_external_memory_WUSER sc_out sc_lv 1 signal 1 } 
	{ m_axi_external_memory_ARVALID sc_out sc_logic 1 signal 1 } 
	{ m_axi_external_memory_ARREADY sc_in sc_logic 1 signal 1 } 
	{ m_axi_external_memory_ARADDR sc_out sc_lv 64 signal 1 } 
	{ m_axi_external_memory_ARID sc_out sc_lv 1 signal 1 } 
	{ m_axi_external_memory_ARLEN sc_out sc_lv 8 signal 1 } 
	{ m_axi_external_memory_ARSIZE sc_out sc_lv 3 signal 1 } 
	{ m_axi_external_memory_ARBURST sc_out sc_lv 2 signal 1 } 
	{ m_axi_external_memory_ARLOCK sc_out sc_lv 2 signal 1 } 
	{ m_axi_external_memory_ARCACHE sc_out sc_lv 4 signal 1 } 
	{ m_axi_external_memory_ARPROT sc_out sc_lv 3 signal 1 } 
	{ m_axi_external_memory_ARQOS sc_out sc_lv 4 signal 1 } 
	{ m_axi_external_memory_ARREGION sc_out sc_lv 4 signal 1 } 
	{ m_axi_external_memory_ARUSER sc_out sc_lv 1 signal 1 } 
	{ m_axi_external_memory_RVALID sc_in sc_logic 1 signal 1 } 
	{ m_axi_external_memory_RREADY sc_out sc_logic 1 signal 1 } 
	{ m_axi_external_memory_RDATA sc_in sc_lv 512 signal 1 } 
	{ m_axi_external_memory_RLAST sc_in sc_logic 1 signal 1 } 
	{ m_axi_external_memory_RID sc_in sc_lv 1 signal 1 } 
	{ m_axi_external_memory_RUSER sc_in sc_lv 1 signal 1 } 
	{ m_axi_external_memory_RRESP sc_in sc_lv 2 signal 1 } 
	{ m_axi_external_memory_BVALID sc_in sc_logic 1 signal 1 } 
	{ m_axi_external_memory_BREADY sc_out sc_logic 1 signal 1 } 
	{ m_axi_external_memory_BRESP sc_in sc_lv 2 signal 1 } 
	{ m_axi_external_memory_BID sc_in sc_lv 1 signal 1 } 
	{ m_axi_external_memory_BUSER sc_in sc_lv 1 signal 1 } 
	{ s_axi_control_AWVALID sc_in sc_logic 1 signal -1 } 
	{ s_axi_control_AWREADY sc_out sc_logic 1 signal -1 } 
	{ s_axi_control_AWADDR sc_in sc_lv 6 signal -1 } 
	{ s_axi_control_WVALID sc_in sc_logic 1 signal -1 } 
	{ s_axi_control_WREADY sc_out sc_logic 1 signal -1 } 
	{ s_axi_control_WDATA sc_in sc_lv 32 signal -1 } 
	{ s_axi_control_WSTRB sc_in sc_lv 4 signal -1 } 
	{ s_axi_control_ARVALID sc_in sc_logic 1 signal -1 } 
	{ s_axi_control_ARREADY sc_out sc_logic 1 signal -1 } 
	{ s_axi_control_ARADDR sc_in sc_lv 6 signal -1 } 
	{ s_axi_control_RVALID sc_out sc_logic 1 signal -1 } 
	{ s_axi_control_RREADY sc_in sc_logic 1 signal -1 } 
	{ s_axi_control_RDATA sc_out sc_lv 32 signal -1 } 
	{ s_axi_control_RRESP sc_out sc_lv 2 signal -1 } 
	{ s_axi_control_BVALID sc_out sc_logic 1 signal -1 } 
	{ s_axi_control_BREADY sc_in sc_logic 1 signal -1 } 
	{ s_axi_control_BRESP sc_out sc_lv 2 signal -1 } 
	{ interrupt sc_out sc_logic 1 signal -1 } 
}
set NewPortList {[ 
	{ "name": "s_axi_control_AWADDR", "direction": "in", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "control", "role": "AWADDR" },"address":[{"name":"riscv_template","role":"start","value":"0","valid_bit":"0"},{"name":"riscv_template","role":"continue","value":"0","valid_bit":"4"},{"name":"riscv_template","role":"auto_start","value":"0","valid_bit":"7"},{"name":"gpn_reset","role":"data","value":"16"},{"name":"gpn_config","role":"data","value":"24"},{"name":"global_memory_ptr","role":"data","value":"32"},{"name":"external_memory_ptr","role":"data","value":"44"}] },
	{ "name": "s_axi_control_AWVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control", "role": "AWVALID" } },
	{ "name": "s_axi_control_AWREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control", "role": "AWREADY" } },
	{ "name": "s_axi_control_WVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control", "role": "WVALID" } },
	{ "name": "s_axi_control_WREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control", "role": "WREADY" } },
	{ "name": "s_axi_control_WDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "control", "role": "WDATA" } },
	{ "name": "s_axi_control_WSTRB", "direction": "in", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "control", "role": "WSTRB" } },
	{ "name": "s_axi_control_ARADDR", "direction": "in", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "control", "role": "ARADDR" },"address":[{"name":"riscv_template","role":"start","value":"0","valid_bit":"0"},{"name":"riscv_template","role":"done","value":"0","valid_bit":"1"},{"name":"riscv_template","role":"idle","value":"0","valid_bit":"2"},{"name":"riscv_template","role":"ready","value":"0","valid_bit":"3"},{"name":"riscv_template","role":"auto_start","value":"0","valid_bit":"7"}] },
	{ "name": "s_axi_control_ARVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control", "role": "ARVALID" } },
	{ "name": "s_axi_control_ARREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control", "role": "ARREADY" } },
	{ "name": "s_axi_control_RVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control", "role": "RVALID" } },
	{ "name": "s_axi_control_RREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control", "role": "RREADY" } },
	{ "name": "s_axi_control_RDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "control", "role": "RDATA" } },
	{ "name": "s_axi_control_RRESP", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "control", "role": "RRESP" } },
	{ "name": "s_axi_control_BVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control", "role": "BVALID" } },
	{ "name": "s_axi_control_BREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control", "role": "BREADY" } },
	{ "name": "s_axi_control_BRESP", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "control", "role": "BRESP" } },
	{ "name": "interrupt", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control", "role": "interrupt" } }, 
 	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst_n", "role": "default" }} , 
 	{ "name": "m_axi_global_memory_AWVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "global_memory", "role": "AWVALID" }} , 
 	{ "name": "m_axi_global_memory_AWREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "global_memory", "role": "AWREADY" }} , 
 	{ "name": "m_axi_global_memory_AWADDR", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "global_memory", "role": "AWADDR" }} , 
 	{ "name": "m_axi_global_memory_AWID", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "global_memory", "role": "AWID" }} , 
 	{ "name": "m_axi_global_memory_AWLEN", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "global_memory", "role": "AWLEN" }} , 
 	{ "name": "m_axi_global_memory_AWSIZE", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "global_memory", "role": "AWSIZE" }} , 
 	{ "name": "m_axi_global_memory_AWBURST", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "global_memory", "role": "AWBURST" }} , 
 	{ "name": "m_axi_global_memory_AWLOCK", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "global_memory", "role": "AWLOCK" }} , 
 	{ "name": "m_axi_global_memory_AWCACHE", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "global_memory", "role": "AWCACHE" }} , 
 	{ "name": "m_axi_global_memory_AWPROT", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "global_memory", "role": "AWPROT" }} , 
 	{ "name": "m_axi_global_memory_AWQOS", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "global_memory", "role": "AWQOS" }} , 
 	{ "name": "m_axi_global_memory_AWREGION", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "global_memory", "role": "AWREGION" }} , 
 	{ "name": "m_axi_global_memory_AWUSER", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "global_memory", "role": "AWUSER" }} , 
 	{ "name": "m_axi_global_memory_WVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "global_memory", "role": "WVALID" }} , 
 	{ "name": "m_axi_global_memory_WREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "global_memory", "role": "WREADY" }} , 
 	{ "name": "m_axi_global_memory_WDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":512, "type": "signal", "bundle":{"name": "global_memory", "role": "WDATA" }} , 
 	{ "name": "m_axi_global_memory_WSTRB", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "global_memory", "role": "WSTRB" }} , 
 	{ "name": "m_axi_global_memory_WLAST", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "global_memory", "role": "WLAST" }} , 
 	{ "name": "m_axi_global_memory_WID", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "global_memory", "role": "WID" }} , 
 	{ "name": "m_axi_global_memory_WUSER", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "global_memory", "role": "WUSER" }} , 
 	{ "name": "m_axi_global_memory_ARVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "global_memory", "role": "ARVALID" }} , 
 	{ "name": "m_axi_global_memory_ARREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "global_memory", "role": "ARREADY" }} , 
 	{ "name": "m_axi_global_memory_ARADDR", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "global_memory", "role": "ARADDR" }} , 
 	{ "name": "m_axi_global_memory_ARID", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "global_memory", "role": "ARID" }} , 
 	{ "name": "m_axi_global_memory_ARLEN", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "global_memory", "role": "ARLEN" }} , 
 	{ "name": "m_axi_global_memory_ARSIZE", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "global_memory", "role": "ARSIZE" }} , 
 	{ "name": "m_axi_global_memory_ARBURST", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "global_memory", "role": "ARBURST" }} , 
 	{ "name": "m_axi_global_memory_ARLOCK", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "global_memory", "role": "ARLOCK" }} , 
 	{ "name": "m_axi_global_memory_ARCACHE", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "global_memory", "role": "ARCACHE" }} , 
 	{ "name": "m_axi_global_memory_ARPROT", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "global_memory", "role": "ARPROT" }} , 
 	{ "name": "m_axi_global_memory_ARQOS", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "global_memory", "role": "ARQOS" }} , 
 	{ "name": "m_axi_global_memory_ARREGION", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "global_memory", "role": "ARREGION" }} , 
 	{ "name": "m_axi_global_memory_ARUSER", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "global_memory", "role": "ARUSER" }} , 
 	{ "name": "m_axi_global_memory_RVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "global_memory", "role": "RVALID" }} , 
 	{ "name": "m_axi_global_memory_RREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "global_memory", "role": "RREADY" }} , 
 	{ "name": "m_axi_global_memory_RDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":512, "type": "signal", "bundle":{"name": "global_memory", "role": "RDATA" }} , 
 	{ "name": "m_axi_global_memory_RLAST", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "global_memory", "role": "RLAST" }} , 
 	{ "name": "m_axi_global_memory_RID", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "global_memory", "role": "RID" }} , 
 	{ "name": "m_axi_global_memory_RUSER", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "global_memory", "role": "RUSER" }} , 
 	{ "name": "m_axi_global_memory_RRESP", "direction": "in", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "global_memory", "role": "RRESP" }} , 
 	{ "name": "m_axi_global_memory_BVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "global_memory", "role": "BVALID" }} , 
 	{ "name": "m_axi_global_memory_BREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "global_memory", "role": "BREADY" }} , 
 	{ "name": "m_axi_global_memory_BRESP", "direction": "in", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "global_memory", "role": "BRESP" }} , 
 	{ "name": "m_axi_global_memory_BID", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "global_memory", "role": "BID" }} , 
 	{ "name": "m_axi_global_memory_BUSER", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "global_memory", "role": "BUSER" }} , 
 	{ "name": "m_axi_external_memory_AWVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "external_memory", "role": "AWVALID" }} , 
 	{ "name": "m_axi_external_memory_AWREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "external_memory", "role": "AWREADY" }} , 
 	{ "name": "m_axi_external_memory_AWADDR", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "external_memory", "role": "AWADDR" }} , 
 	{ "name": "m_axi_external_memory_AWID", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "external_memory", "role": "AWID" }} , 
 	{ "name": "m_axi_external_memory_AWLEN", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "external_memory", "role": "AWLEN" }} , 
 	{ "name": "m_axi_external_memory_AWSIZE", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "external_memory", "role": "AWSIZE" }} , 
 	{ "name": "m_axi_external_memory_AWBURST", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "external_memory", "role": "AWBURST" }} , 
 	{ "name": "m_axi_external_memory_AWLOCK", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "external_memory", "role": "AWLOCK" }} , 
 	{ "name": "m_axi_external_memory_AWCACHE", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "external_memory", "role": "AWCACHE" }} , 
 	{ "name": "m_axi_external_memory_AWPROT", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "external_memory", "role": "AWPROT" }} , 
 	{ "name": "m_axi_external_memory_AWQOS", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "external_memory", "role": "AWQOS" }} , 
 	{ "name": "m_axi_external_memory_AWREGION", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "external_memory", "role": "AWREGION" }} , 
 	{ "name": "m_axi_external_memory_AWUSER", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "external_memory", "role": "AWUSER" }} , 
 	{ "name": "m_axi_external_memory_WVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "external_memory", "role": "WVALID" }} , 
 	{ "name": "m_axi_external_memory_WREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "external_memory", "role": "WREADY" }} , 
 	{ "name": "m_axi_external_memory_WDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":512, "type": "signal", "bundle":{"name": "external_memory", "role": "WDATA" }} , 
 	{ "name": "m_axi_external_memory_WSTRB", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "external_memory", "role": "WSTRB" }} , 
 	{ "name": "m_axi_external_memory_WLAST", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "external_memory", "role": "WLAST" }} , 
 	{ "name": "m_axi_external_memory_WID", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "external_memory", "role": "WID" }} , 
 	{ "name": "m_axi_external_memory_WUSER", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "external_memory", "role": "WUSER" }} , 
 	{ "name": "m_axi_external_memory_ARVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "external_memory", "role": "ARVALID" }} , 
 	{ "name": "m_axi_external_memory_ARREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "external_memory", "role": "ARREADY" }} , 
 	{ "name": "m_axi_external_memory_ARADDR", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "external_memory", "role": "ARADDR" }} , 
 	{ "name": "m_axi_external_memory_ARID", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "external_memory", "role": "ARID" }} , 
 	{ "name": "m_axi_external_memory_ARLEN", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "external_memory", "role": "ARLEN" }} , 
 	{ "name": "m_axi_external_memory_ARSIZE", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "external_memory", "role": "ARSIZE" }} , 
 	{ "name": "m_axi_external_memory_ARBURST", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "external_memory", "role": "ARBURST" }} , 
 	{ "name": "m_axi_external_memory_ARLOCK", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "external_memory", "role": "ARLOCK" }} , 
 	{ "name": "m_axi_external_memory_ARCACHE", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "external_memory", "role": "ARCACHE" }} , 
 	{ "name": "m_axi_external_memory_ARPROT", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "external_memory", "role": "ARPROT" }} , 
 	{ "name": "m_axi_external_memory_ARQOS", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "external_memory", "role": "ARQOS" }} , 
 	{ "name": "m_axi_external_memory_ARREGION", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "external_memory", "role": "ARREGION" }} , 
 	{ "name": "m_axi_external_memory_ARUSER", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "external_memory", "role": "ARUSER" }} , 
 	{ "name": "m_axi_external_memory_RVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "external_memory", "role": "RVALID" }} , 
 	{ "name": "m_axi_external_memory_RREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "external_memory", "role": "RREADY" }} , 
 	{ "name": "m_axi_external_memory_RDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":512, "type": "signal", "bundle":{"name": "external_memory", "role": "RDATA" }} , 
 	{ "name": "m_axi_external_memory_RLAST", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "external_memory", "role": "RLAST" }} , 
 	{ "name": "m_axi_external_memory_RID", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "external_memory", "role": "RID" }} , 
 	{ "name": "m_axi_external_memory_RUSER", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "external_memory", "role": "RUSER" }} , 
 	{ "name": "m_axi_external_memory_RRESP", "direction": "in", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "external_memory", "role": "RRESP" }} , 
 	{ "name": "m_axi_external_memory_BVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "external_memory", "role": "BVALID" }} , 
 	{ "name": "m_axi_external_memory_BREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "external_memory", "role": "BREADY" }} , 
 	{ "name": "m_axi_external_memory_BRESP", "direction": "in", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "external_memory", "role": "BRESP" }} , 
 	{ "name": "m_axi_external_memory_BID", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "external_memory", "role": "BID" }} , 
 	{ "name": "m_axi_external_memory_BUSER", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "external_memory", "role": "BUSER" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2", "3", "4", "5", "6", "7"],
		"CDFG" : "riscv_template",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "0", "ap_idle" : "1", "real_start" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "24798", "EstimateLatencyMax" : "24798",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"Port" : [
			{"Name" : "global_memory", "Type" : "MAXI", "Direction" : "IO",
				"BlockSignal" : [
					{"Name" : "global_memory_blk_n_AR", "Type" : "RtlSignal"},
					{"Name" : "global_memory_blk_n_R", "Type" : "RtlSignal"},
					{"Name" : "global_memory_blk_n_AW", "Type" : "RtlSignal"},
					{"Name" : "global_memory_blk_n_W", "Type" : "RtlSignal"},
					{"Name" : "global_memory_blk_n_B", "Type" : "RtlSignal"}]},
			{"Name" : "external_memory", "Type" : "MAXI", "Direction" : "IO",
				"BlockSignal" : [
					{"Name" : "external_memory_blk_n_AR", "Type" : "RtlSignal"},
					{"Name" : "external_memory_blk_n_R", "Type" : "RtlSignal"},
					{"Name" : "external_memory_blk_n_AW", "Type" : "RtlSignal"},
					{"Name" : "external_memory_blk_n_W", "Type" : "RtlSignal"},
					{"Name" : "external_memory_blk_n_B", "Type" : "RtlSignal"}]},
			{"Name" : "gpn_reset", "Type" : "None", "Direction" : "I"},
			{"Name" : "gpn_config", "Type" : "None", "Direction" : "I"},
			{"Name" : "global_memory_ptr", "Type" : "None", "Direction" : "I"},
			{"Name" : "external_memory_ptr", "Type" : "None", "Direction" : "I"}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.control_s_axi_U", "Parent" : "0"},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.global_memory_m_axi_U", "Parent" : "0"},
	{"ID" : "3", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.external_memory_m_axi_U", "Parent" : "0"},
	{"ID" : "4", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.global_memory_input_buffer_U", "Parent" : "0"},
	{"ID" : "5", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.global_memory_output_buffer_U", "Parent" : "0"},
	{"ID" : "6", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.external_memory_input_buffer_U", "Parent" : "0"},
	{"ID" : "7", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.external_memory_output_buffer_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	riscv_template {
		global_memory {Type IO LastRead 76 FirstWrite 77}
		external_memory {Type IO LastRead 151 FirstWrite 152}
		gpn_reset {Type I LastRead -1 FirstWrite -1}
		gpn_config {Type I LastRead -1 FirstWrite -1}
		global_memory_ptr {Type I LastRead 0 FirstWrite -1}
		external_memory_ptr {Type I LastRead 0 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "24798", "Max" : "24798"}
	, {"Name" : "Interval", "Min" : "24799", "Max" : "24799"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
	{"Pipeline" : "1", "EnableSignal" : "ap_enable_pp1"}
	{"Pipeline" : "2", "EnableSignal" : "ap_enable_pp2"}
	{"Pipeline" : "3", "EnableSignal" : "ap_enable_pp3"}
	{"Pipeline" : "4", "EnableSignal" : "ap_enable_pp4"}
	{"Pipeline" : "5", "EnableSignal" : "ap_enable_pp5"}
]}

set Spec2ImplPortList { 
	global_memory { m_axi {  { m_axi_global_memory_AWVALID VALID 1 1 }  { m_axi_global_memory_AWREADY READY 0 1 }  { m_axi_global_memory_AWADDR ADDR 1 64 }  { m_axi_global_memory_AWID ID 1 1 }  { m_axi_global_memory_AWLEN LEN 1 8 }  { m_axi_global_memory_AWSIZE SIZE 1 3 }  { m_axi_global_memory_AWBURST BURST 1 2 }  { m_axi_global_memory_AWLOCK LOCK 1 2 }  { m_axi_global_memory_AWCACHE CACHE 1 4 }  { m_axi_global_memory_AWPROT PROT 1 3 }  { m_axi_global_memory_AWQOS QOS 1 4 }  { m_axi_global_memory_AWREGION REGION 1 4 }  { m_axi_global_memory_AWUSER USER 1 1 }  { m_axi_global_memory_WVALID VALID 1 1 }  { m_axi_global_memory_WREADY READY 0 1 }  { m_axi_global_memory_WDATA DATA 1 512 }  { m_axi_global_memory_WSTRB STRB 1 64 }  { m_axi_global_memory_WLAST LAST 1 1 }  { m_axi_global_memory_WID ID 1 1 }  { m_axi_global_memory_WUSER USER 1 1 }  { m_axi_global_memory_ARVALID VALID 1 1 }  { m_axi_global_memory_ARREADY READY 0 1 }  { m_axi_global_memory_ARADDR ADDR 1 64 }  { m_axi_global_memory_ARID ID 1 1 }  { m_axi_global_memory_ARLEN LEN 1 8 }  { m_axi_global_memory_ARSIZE SIZE 1 3 }  { m_axi_global_memory_ARBURST BURST 1 2 }  { m_axi_global_memory_ARLOCK LOCK 1 2 }  { m_axi_global_memory_ARCACHE CACHE 1 4 }  { m_axi_global_memory_ARPROT PROT 1 3 }  { m_axi_global_memory_ARQOS QOS 1 4 }  { m_axi_global_memory_ARREGION REGION 1 4 }  { m_axi_global_memory_ARUSER USER 1 1 }  { m_axi_global_memory_RVALID VALID 0 1 }  { m_axi_global_memory_RREADY READY 1 1 }  { m_axi_global_memory_RDATA DATA 0 512 }  { m_axi_global_memory_RLAST LAST 0 1 }  { m_axi_global_memory_RID ID 0 1 }  { m_axi_global_memory_RUSER USER 0 1 }  { m_axi_global_memory_RRESP RESP 0 2 }  { m_axi_global_memory_BVALID VALID 0 1 }  { m_axi_global_memory_BREADY READY 1 1 }  { m_axi_global_memory_BRESP RESP 0 2 }  { m_axi_global_memory_BID ID 0 1 }  { m_axi_global_memory_BUSER USER 0 1 } } }
	external_memory { m_axi {  { m_axi_external_memory_AWVALID VALID 1 1 }  { m_axi_external_memory_AWREADY READY 0 1 }  { m_axi_external_memory_AWADDR ADDR 1 64 }  { m_axi_external_memory_AWID ID 1 1 }  { m_axi_external_memory_AWLEN LEN 1 8 }  { m_axi_external_memory_AWSIZE SIZE 1 3 }  { m_axi_external_memory_AWBURST BURST 1 2 }  { m_axi_external_memory_AWLOCK LOCK 1 2 }  { m_axi_external_memory_AWCACHE CACHE 1 4 }  { m_axi_external_memory_AWPROT PROT 1 3 }  { m_axi_external_memory_AWQOS QOS 1 4 }  { m_axi_external_memory_AWREGION REGION 1 4 }  { m_axi_external_memory_AWUSER USER 1 1 }  { m_axi_external_memory_WVALID VALID 1 1 }  { m_axi_external_memory_WREADY READY 0 1 }  { m_axi_external_memory_WDATA DATA 1 512 }  { m_axi_external_memory_WSTRB STRB 1 64 }  { m_axi_external_memory_WLAST LAST 1 1 }  { m_axi_external_memory_WID ID 1 1 }  { m_axi_external_memory_WUSER USER 1 1 }  { m_axi_external_memory_ARVALID VALID 1 1 }  { m_axi_external_memory_ARREADY READY 0 1 }  { m_axi_external_memory_ARADDR ADDR 1 64 }  { m_axi_external_memory_ARID ID 1 1 }  { m_axi_external_memory_ARLEN LEN 1 8 }  { m_axi_external_memory_ARSIZE SIZE 1 3 }  { m_axi_external_memory_ARBURST BURST 1 2 }  { m_axi_external_memory_ARLOCK LOCK 1 2 }  { m_axi_external_memory_ARCACHE CACHE 1 4 }  { m_axi_external_memory_ARPROT PROT 1 3 }  { m_axi_external_memory_ARQOS QOS 1 4 }  { m_axi_external_memory_ARREGION REGION 1 4 }  { m_axi_external_memory_ARUSER USER 1 1 }  { m_axi_external_memory_RVALID VALID 0 1 }  { m_axi_external_memory_RREADY READY 1 1 }  { m_axi_external_memory_RDATA DATA 0 512 }  { m_axi_external_memory_RLAST LAST 0 1 }  { m_axi_external_memory_RID ID 0 1 }  { m_axi_external_memory_RUSER USER 0 1 }  { m_axi_external_memory_RRESP RESP 0 2 }  { m_axi_external_memory_BVALID VALID 0 1 }  { m_axi_external_memory_BREADY READY 1 1 }  { m_axi_external_memory_BRESP RESP 0 2 }  { m_axi_external_memory_BID ID 0 1 }  { m_axi_external_memory_BUSER USER 0 1 } } }
}

set busDeadlockParameterList { 
	{ global_memory { NUM_READ_OUTSTANDING 16 NUM_WRITE_OUTSTANDING 16 MAX_READ_BURST_LENGTH 16 MAX_WRITE_BURST_LENGTH 16 } } \
	{ external_memory { NUM_READ_OUTSTANDING 16 NUM_WRITE_OUTSTANDING 16 MAX_READ_BURST_LENGTH 16 MAX_WRITE_BURST_LENGTH 16 } } \
}

# RTL port scheduling information:
set fifoSchedulingInfoList { 
}

# RTL bus port read request latency information:
set busReadReqLatencyList { 
	{ global_memory 64 }
	{ external_memory 64 }
}

# RTL bus port write response latency information:
set busWriteResLatencyList { 
	{ global_memory 64 }
	{ external_memory 64 }
}

# RTL array port load latency information:
set memoryLoadLatencyList { 
}
