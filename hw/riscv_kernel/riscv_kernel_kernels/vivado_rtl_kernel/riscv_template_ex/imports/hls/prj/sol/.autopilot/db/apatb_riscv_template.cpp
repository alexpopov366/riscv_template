#include <systemc>
#include <iostream>
#include <cstdlib>
#include <cstddef>
#include <stdint.h>
#include "SysCFileHandler.h"
#include "ap_int.h"
#include "ap_fixed.h"
#include <complex>
#include <stdbool.h>
#include "autopilot_cbe.h"
#include "hls_stream.h"
#include "hls_half.h"
#include "hls_signal_handler.h"

using namespace std;
using namespace sc_core;
using namespace sc_dt;

// wrapc file define:
#define AUTOTB_TVIN_global_memory "../tv/cdatafile/c.riscv_template.autotvin_global_memory.dat"
#define AUTOTB_TVOUT_global_memory "../tv/cdatafile/c.riscv_template.autotvout_global_memory.dat"
// wrapc file define:
#define AUTOTB_TVIN_external_memory "../tv/cdatafile/c.riscv_template.autotvin_external_memory.dat"
#define AUTOTB_TVOUT_external_memory "../tv/cdatafile/c.riscv_template.autotvout_external_memory.dat"
// wrapc file define:
#define AUTOTB_TVIN_gpn_reset "../tv/cdatafile/c.riscv_template.autotvin_gpn_reset.dat"
#define AUTOTB_TVOUT_gpn_reset "../tv/cdatafile/c.riscv_template.autotvout_gpn_reset.dat"
// wrapc file define:
#define AUTOTB_TVIN_gpn_config "../tv/cdatafile/c.riscv_template.autotvin_gpn_config.dat"
#define AUTOTB_TVOUT_gpn_config "../tv/cdatafile/c.riscv_template.autotvout_gpn_config.dat"
// wrapc file define:
#define AUTOTB_TVIN_global_memory_ptr "../tv/cdatafile/c.riscv_template.autotvin_global_memory_ptr.dat"
#define AUTOTB_TVOUT_global_memory_ptr "../tv/cdatafile/c.riscv_template.autotvout_global_memory_ptr.dat"
// wrapc file define:
#define AUTOTB_TVIN_external_memory_ptr "../tv/cdatafile/c.riscv_template.autotvin_external_memory_ptr.dat"
#define AUTOTB_TVOUT_external_memory_ptr "../tv/cdatafile/c.riscv_template.autotvout_external_memory_ptr.dat"

#define INTER_TCL "../tv/cdatafile/ref.tcl"

// tvout file define:
#define AUTOTB_TVOUT_PC_global_memory "../tv/rtldatafile/rtl.riscv_template.autotvout_global_memory.dat"
// tvout file define:
#define AUTOTB_TVOUT_PC_external_memory "../tv/rtldatafile/rtl.riscv_template.autotvout_external_memory.dat"
// tvout file define:
#define AUTOTB_TVOUT_PC_gpn_reset "../tv/rtldatafile/rtl.riscv_template.autotvout_gpn_reset.dat"
// tvout file define:
#define AUTOTB_TVOUT_PC_gpn_config "../tv/rtldatafile/rtl.riscv_template.autotvout_gpn_config.dat"
// tvout file define:
#define AUTOTB_TVOUT_PC_global_memory_ptr "../tv/rtldatafile/rtl.riscv_template.autotvout_global_memory_ptr.dat"
// tvout file define:
#define AUTOTB_TVOUT_PC_external_memory_ptr "../tv/rtldatafile/rtl.riscv_template.autotvout_external_memory_ptr.dat"

class INTER_TCL_FILE {
  public:
INTER_TCL_FILE(const char* name) {
  mName = name; 
  global_memory_depth = 0;
  external_memory_depth = 0;
  gpn_reset_depth = 0;
  gpn_config_depth = 0;
  global_memory_ptr_depth = 0;
  external_memory_ptr_depth = 0;
  trans_num =0;
}
~INTER_TCL_FILE() {
  mFile.open(mName);
  if (!mFile.good()) {
    cout << "Failed to open file ref.tcl" << endl;
    exit (1); 
  }
  string total_list = get_depth_list();
  mFile << "set depth_list {\n";
  mFile << total_list;
  mFile << "}\n";
  mFile << "set trans_num "<<trans_num<<endl;
  mFile.close();
}
string get_depth_list () {
  stringstream total_list;
  total_list << "{global_memory " << global_memory_depth << "}\n";
  total_list << "{external_memory " << external_memory_depth << "}\n";
  total_list << "{gpn_reset " << gpn_reset_depth << "}\n";
  total_list << "{gpn_config " << gpn_config_depth << "}\n";
  total_list << "{global_memory_ptr " << global_memory_ptr_depth << "}\n";
  total_list << "{external_memory_ptr " << external_memory_ptr_depth << "}\n";
  return total_list.str();
}
void set_num (int num , int* class_num) {
  (*class_num) = (*class_num) > num ? (*class_num) : num;
}
void set_string(std::string list, std::string* class_list) {
  (*class_list) = list;
}
  public:
    int global_memory_depth;
    int external_memory_depth;
    int gpn_reset_depth;
    int gpn_config_depth;
    int global_memory_ptr_depth;
    int external_memory_ptr_depth;
    int trans_num;
  private:
    ofstream mFile;
    const char* mName;
};

static void RTLOutputCheckAndReplacement(std::string &AESL_token, std::string PortName) {
  bool no_x = false;
  bool err = false;

  no_x = false;
  // search and replace 'X' with '0' from the 3rd char of token
  while (!no_x) {
    size_t x_found = AESL_token.find('X', 0);
    if (x_found != string::npos) {
      if (!err) { 
        cerr << "WARNING: [SIM 212-201] RTL produces unknown value 'X' on port" 
             << PortName << ", possible cause: There are uninitialized variables in the C design."
             << endl; 
        err = true;
      }
      AESL_token.replace(x_found, 1, "0");
    } else
      no_x = true;
  }
  no_x = false;
  // search and replace 'x' with '0' from the 3rd char of token
  while (!no_x) {
    size_t x_found = AESL_token.find('x', 2);
    if (x_found != string::npos) {
      if (!err) { 
        cerr << "WARNING: [SIM 212-201] RTL produces unknown value 'x' on port" 
             << PortName << ", possible cause: There are uninitialized variables in the C design."
             << endl; 
        err = true;
      }
      AESL_token.replace(x_found, 1, "0");
    } else
      no_x = true;
  }
}
struct __cosim_s40__ { char data[64]; };
extern "C" void riscv_template_hw_stub_wrapper(char, int, volatile void *, volatile void *);

extern "C" void apatb_riscv_template_hw(char __xlx_apatb_param_gpn_reset, int __xlx_apatb_param_gpn_config, volatile void * __xlx_apatb_param_global_memory_ptr, volatile void * __xlx_apatb_param_external_memory_ptr) {
  refine_signal_handler();
  fstream wrapc_switch_file_token;
  wrapc_switch_file_token.open(".hls_cosim_wrapc_switch.log");
  int AESL_i;
  if (wrapc_switch_file_token.good())
  {

    CodeState = ENTER_WRAPC_PC;
    static unsigned AESL_transaction_pc = 0;
    string AESL_token;
    string AESL_num;{
      static ifstream rtl_tv_out_file;
      if (!rtl_tv_out_file.is_open()) {
        rtl_tv_out_file.open(AUTOTB_TVOUT_PC_global_memory);
        if (rtl_tv_out_file.good()) {
          rtl_tv_out_file >> AESL_token;
          if (AESL_token != "[[[runtime]]]")
            exit(1);
        }
      }
  
      if (rtl_tv_out_file.good()) {
        rtl_tv_out_file >> AESL_token; 
        rtl_tv_out_file >> AESL_num;  // transaction number
        if (AESL_token != "[[transaction]]") {
          cerr << "Unexpected token: " << AESL_token << endl;
          exit(1);
        }
        if (atoi(AESL_num.c_str()) == AESL_transaction_pc) {
          std::vector<sc_bv<512> > global_memory_pc_buffer(1);
          int i = 0;

          rtl_tv_out_file >> AESL_token; //data
          while (AESL_token != "[[/transaction]]"){

            RTLOutputCheckAndReplacement(AESL_token, "global_memory");
  
            // push token into output port buffer
            if (AESL_token != "") {
              global_memory_pc_buffer[i] = AESL_token.c_str();;
              i++;
            }
  
            rtl_tv_out_file >> AESL_token; //data or [[/transaction]]
            if (AESL_token == "[[[/runtime]]]" || rtl_tv_out_file.eof())
              exit(1);
          }
          if (i > 0) {{
            int i = 0;
            for (int j = 0, e = 1; j < e; j += 1, ++i) {((long long*)__xlx_apatb_param_global_memory_ptr)[j*8+0] = global_memory_pc_buffer[i].range(63,0).to_int64();
((long long*)__xlx_apatb_param_global_memory_ptr)[j*8+1] = global_memory_pc_buffer[i].range(127,64).to_int64();
((long long*)__xlx_apatb_param_global_memory_ptr)[j*8+2] = global_memory_pc_buffer[i].range(191,128).to_int64();
((long long*)__xlx_apatb_param_global_memory_ptr)[j*8+3] = global_memory_pc_buffer[i].range(255,192).to_int64();
((long long*)__xlx_apatb_param_global_memory_ptr)[j*8+4] = global_memory_pc_buffer[i].range(319,256).to_int64();
((long long*)__xlx_apatb_param_global_memory_ptr)[j*8+5] = global_memory_pc_buffer[i].range(383,320).to_int64();
((long long*)__xlx_apatb_param_global_memory_ptr)[j*8+6] = global_memory_pc_buffer[i].range(447,384).to_int64();
((long long*)__xlx_apatb_param_global_memory_ptr)[j*8+7] = global_memory_pc_buffer[i].range(511,448).to_int64();
}}}
        } // end transaction
      } // end file is good
    } // end post check logic bolck
  {
      static ifstream rtl_tv_out_file;
      if (!rtl_tv_out_file.is_open()) {
        rtl_tv_out_file.open(AUTOTB_TVOUT_PC_external_memory);
        if (rtl_tv_out_file.good()) {
          rtl_tv_out_file >> AESL_token;
          if (AESL_token != "[[[runtime]]]")
            exit(1);
        }
      }
  
      if (rtl_tv_out_file.good()) {
        rtl_tv_out_file >> AESL_token; 
        rtl_tv_out_file >> AESL_num;  // transaction number
        if (AESL_token != "[[transaction]]") {
          cerr << "Unexpected token: " << AESL_token << endl;
          exit(1);
        }
        if (atoi(AESL_num.c_str()) == AESL_transaction_pc) {
          std::vector<sc_bv<512> > external_memory_pc_buffer(1);
          int i = 0;

          rtl_tv_out_file >> AESL_token; //data
          while (AESL_token != "[[/transaction]]"){

            RTLOutputCheckAndReplacement(AESL_token, "external_memory");
  
            // push token into output port buffer
            if (AESL_token != "") {
              external_memory_pc_buffer[i] = AESL_token.c_str();;
              i++;
            }
  
            rtl_tv_out_file >> AESL_token; //data or [[/transaction]]
            if (AESL_token == "[[[/runtime]]]" || rtl_tv_out_file.eof())
              exit(1);
          }
          if (i > 0) {{
            int i = 0;
            for (int j = 0, e = 1; j < e; j += 1, ++i) {((long long*)__xlx_apatb_param_external_memory_ptr)[j*8+0] = external_memory_pc_buffer[i].range(63,0).to_int64();
((long long*)__xlx_apatb_param_external_memory_ptr)[j*8+1] = external_memory_pc_buffer[i].range(127,64).to_int64();
((long long*)__xlx_apatb_param_external_memory_ptr)[j*8+2] = external_memory_pc_buffer[i].range(191,128).to_int64();
((long long*)__xlx_apatb_param_external_memory_ptr)[j*8+3] = external_memory_pc_buffer[i].range(255,192).to_int64();
((long long*)__xlx_apatb_param_external_memory_ptr)[j*8+4] = external_memory_pc_buffer[i].range(319,256).to_int64();
((long long*)__xlx_apatb_param_external_memory_ptr)[j*8+5] = external_memory_pc_buffer[i].range(383,320).to_int64();
((long long*)__xlx_apatb_param_external_memory_ptr)[j*8+6] = external_memory_pc_buffer[i].range(447,384).to_int64();
((long long*)__xlx_apatb_param_external_memory_ptr)[j*8+7] = external_memory_pc_buffer[i].range(511,448).to_int64();
}}}
        } // end transaction
      } // end file is good
    } // end post check logic bolck
  
    AESL_transaction_pc++;
    return ;
  }
static unsigned AESL_transaction;
static AESL_FILE_HANDLER aesl_fh;
static INTER_TCL_FILE tcl_file(INTER_TCL);
std::vector<char> __xlx_sprintf_buffer(1024);
CodeState = ENTER_WRAPC;
//global_memory
aesl_fh.touch(AUTOTB_TVIN_global_memory);
aesl_fh.touch(AUTOTB_TVOUT_global_memory);
//external_memory
aesl_fh.touch(AUTOTB_TVIN_external_memory);
aesl_fh.touch(AUTOTB_TVOUT_external_memory);
//gpn_reset
aesl_fh.touch(AUTOTB_TVIN_gpn_reset);
aesl_fh.touch(AUTOTB_TVOUT_gpn_reset);
//gpn_config
aesl_fh.touch(AUTOTB_TVIN_gpn_config);
aesl_fh.touch(AUTOTB_TVOUT_gpn_config);
//global_memory_ptr
aesl_fh.touch(AUTOTB_TVIN_global_memory_ptr);
aesl_fh.touch(AUTOTB_TVOUT_global_memory_ptr);
//external_memory_ptr
aesl_fh.touch(AUTOTB_TVIN_external_memory_ptr);
aesl_fh.touch(AUTOTB_TVOUT_external_memory_ptr);
CodeState = DUMP_INPUTS;
unsigned __xlx_offset_byte_param_global_memory_ptr = 0;
// print global_memory Transactions
{
  sprintf(__xlx_sprintf_buffer.data(), "[[transaction]] %d\n", AESL_transaction);
  aesl_fh.write(AUTOTB_TVIN_global_memory, __xlx_sprintf_buffer.data());
  {  __xlx_offset_byte_param_global_memory_ptr = 0*64;
  if (__xlx_apatb_param_global_memory_ptr) {
    for (int j = 0  - 0, e = 1 - 0; j != e; ++j) {
sc_bv<512> __xlx_tmp_lv;
__xlx_tmp_lv.range(63,0) = ((long long*)__xlx_apatb_param_global_memory_ptr)[j*8+0];
__xlx_tmp_lv.range(127,64) = ((long long*)__xlx_apatb_param_global_memory_ptr)[j*8+1];
__xlx_tmp_lv.range(191,128) = ((long long*)__xlx_apatb_param_global_memory_ptr)[j*8+2];
__xlx_tmp_lv.range(255,192) = ((long long*)__xlx_apatb_param_global_memory_ptr)[j*8+3];
__xlx_tmp_lv.range(319,256) = ((long long*)__xlx_apatb_param_global_memory_ptr)[j*8+4];
__xlx_tmp_lv.range(383,320) = ((long long*)__xlx_apatb_param_global_memory_ptr)[j*8+5];
__xlx_tmp_lv.range(447,384) = ((long long*)__xlx_apatb_param_global_memory_ptr)[j*8+6];
__xlx_tmp_lv.range(511,448) = ((long long*)__xlx_apatb_param_global_memory_ptr)[j*8+7];

    sprintf(__xlx_sprintf_buffer.data(), "%s\n", __xlx_tmp_lv.to_string(SC_HEX).c_str());
    aesl_fh.write(AUTOTB_TVIN_global_memory, __xlx_sprintf_buffer.data()); 
      }
  }
}
  tcl_file.set_num(1, &tcl_file.global_memory_depth);
  sprintf(__xlx_sprintf_buffer.data(), "[[/transaction]] \n");
  aesl_fh.write(AUTOTB_TVIN_global_memory, __xlx_sprintf_buffer.data());
}
unsigned __xlx_offset_byte_param_external_memory_ptr = 0;
// print external_memory Transactions
{
  sprintf(__xlx_sprintf_buffer.data(), "[[transaction]] %d\n", AESL_transaction);
  aesl_fh.write(AUTOTB_TVIN_external_memory, __xlx_sprintf_buffer.data());
  {  __xlx_offset_byte_param_external_memory_ptr = 0*64;
  if (__xlx_apatb_param_external_memory_ptr) {
    for (int j = 0  - 0, e = 1 - 0; j != e; ++j) {
sc_bv<512> __xlx_tmp_lv;
__xlx_tmp_lv.range(63,0) = ((long long*)__xlx_apatb_param_external_memory_ptr)[j*8+0];
__xlx_tmp_lv.range(127,64) = ((long long*)__xlx_apatb_param_external_memory_ptr)[j*8+1];
__xlx_tmp_lv.range(191,128) = ((long long*)__xlx_apatb_param_external_memory_ptr)[j*8+2];
__xlx_tmp_lv.range(255,192) = ((long long*)__xlx_apatb_param_external_memory_ptr)[j*8+3];
__xlx_tmp_lv.range(319,256) = ((long long*)__xlx_apatb_param_external_memory_ptr)[j*8+4];
__xlx_tmp_lv.range(383,320) = ((long long*)__xlx_apatb_param_external_memory_ptr)[j*8+5];
__xlx_tmp_lv.range(447,384) = ((long long*)__xlx_apatb_param_external_memory_ptr)[j*8+6];
__xlx_tmp_lv.range(511,448) = ((long long*)__xlx_apatb_param_external_memory_ptr)[j*8+7];

    sprintf(__xlx_sprintf_buffer.data(), "%s\n", __xlx_tmp_lv.to_string(SC_HEX).c_str());
    aesl_fh.write(AUTOTB_TVIN_external_memory, __xlx_sprintf_buffer.data()); 
      }
  }
}
  tcl_file.set_num(1, &tcl_file.external_memory_depth);
  sprintf(__xlx_sprintf_buffer.data(), "[[/transaction]] \n");
  aesl_fh.write(AUTOTB_TVIN_external_memory, __xlx_sprintf_buffer.data());
}
// print gpn_reset Transactions
{
  sprintf(__xlx_sprintf_buffer.data(), "[[transaction]] %d\n", AESL_transaction);
  aesl_fh.write(AUTOTB_TVIN_gpn_reset, __xlx_sprintf_buffer.data());
  {
    sc_bv<8> __xlx_tmp_lv = *((char*)&__xlx_apatb_param_gpn_reset);

    sprintf(__xlx_sprintf_buffer.data(), "%s\n", __xlx_tmp_lv.to_string(SC_HEX).c_str());
    aesl_fh.write(AUTOTB_TVIN_gpn_reset, __xlx_sprintf_buffer.data()); 
  }
  tcl_file.set_num(1, &tcl_file.gpn_reset_depth);
  sprintf(__xlx_sprintf_buffer.data(), "[[/transaction]] \n");
  aesl_fh.write(AUTOTB_TVIN_gpn_reset, __xlx_sprintf_buffer.data());
}
// print gpn_config Transactions
{
  sprintf(__xlx_sprintf_buffer.data(), "[[transaction]] %d\n", AESL_transaction);
  aesl_fh.write(AUTOTB_TVIN_gpn_config, __xlx_sprintf_buffer.data());
  {
    sc_bv<32> __xlx_tmp_lv = *((int*)&__xlx_apatb_param_gpn_config);

    sprintf(__xlx_sprintf_buffer.data(), "%s\n", __xlx_tmp_lv.to_string(SC_HEX).c_str());
    aesl_fh.write(AUTOTB_TVIN_gpn_config, __xlx_sprintf_buffer.data()); 
  }
  tcl_file.set_num(1, &tcl_file.gpn_config_depth);
  sprintf(__xlx_sprintf_buffer.data(), "[[/transaction]] \n");
  aesl_fh.write(AUTOTB_TVIN_gpn_config, __xlx_sprintf_buffer.data());
}
// print global_memory_ptr Transactions
{
  sprintf(__xlx_sprintf_buffer.data(), "[[transaction]] %d\n", AESL_transaction);
  aesl_fh.write(AUTOTB_TVIN_global_memory_ptr, __xlx_sprintf_buffer.data());
  {
    sc_bv<64> __xlx_tmp_lv = __xlx_offset_byte_param_global_memory_ptr;

    sprintf(__xlx_sprintf_buffer.data(), "%s\n", __xlx_tmp_lv.to_string(SC_HEX).c_str());
    aesl_fh.write(AUTOTB_TVIN_global_memory_ptr, __xlx_sprintf_buffer.data()); 
  }
  tcl_file.set_num(1, &tcl_file.global_memory_ptr_depth);
  sprintf(__xlx_sprintf_buffer.data(), "[[/transaction]] \n");
  aesl_fh.write(AUTOTB_TVIN_global_memory_ptr, __xlx_sprintf_buffer.data());
}
// print external_memory_ptr Transactions
{
  sprintf(__xlx_sprintf_buffer.data(), "[[transaction]] %d\n", AESL_transaction);
  aesl_fh.write(AUTOTB_TVIN_external_memory_ptr, __xlx_sprintf_buffer.data());
  {
    sc_bv<64> __xlx_tmp_lv = __xlx_offset_byte_param_external_memory_ptr;

    sprintf(__xlx_sprintf_buffer.data(), "%s\n", __xlx_tmp_lv.to_string(SC_HEX).c_str());
    aesl_fh.write(AUTOTB_TVIN_external_memory_ptr, __xlx_sprintf_buffer.data()); 
  }
  tcl_file.set_num(1, &tcl_file.external_memory_ptr_depth);
  sprintf(__xlx_sprintf_buffer.data(), "[[/transaction]] \n");
  aesl_fh.write(AUTOTB_TVIN_external_memory_ptr, __xlx_sprintf_buffer.data());
}
CodeState = CALL_C_DUT;
riscv_template_hw_stub_wrapper(__xlx_apatb_param_gpn_reset, __xlx_apatb_param_gpn_config, __xlx_apatb_param_global_memory_ptr, __xlx_apatb_param_external_memory_ptr);
CodeState = DUMP_OUTPUTS;
// print global_memory Transactions
{
  sprintf(__xlx_sprintf_buffer.data(), "[[transaction]] %d\n", AESL_transaction);
  aesl_fh.write(AUTOTB_TVOUT_global_memory, __xlx_sprintf_buffer.data());
  {  __xlx_offset_byte_param_global_memory_ptr = 0*64;
  if (__xlx_apatb_param_global_memory_ptr) {
    for (int j = 0  - 0, e = 1 - 0; j != e; ++j) {
sc_bv<512> __xlx_tmp_lv;
__xlx_tmp_lv.range(63,0) = ((long long*)__xlx_apatb_param_global_memory_ptr)[j*8+0];
__xlx_tmp_lv.range(127,64) = ((long long*)__xlx_apatb_param_global_memory_ptr)[j*8+1];
__xlx_tmp_lv.range(191,128) = ((long long*)__xlx_apatb_param_global_memory_ptr)[j*8+2];
__xlx_tmp_lv.range(255,192) = ((long long*)__xlx_apatb_param_global_memory_ptr)[j*8+3];
__xlx_tmp_lv.range(319,256) = ((long long*)__xlx_apatb_param_global_memory_ptr)[j*8+4];
__xlx_tmp_lv.range(383,320) = ((long long*)__xlx_apatb_param_global_memory_ptr)[j*8+5];
__xlx_tmp_lv.range(447,384) = ((long long*)__xlx_apatb_param_global_memory_ptr)[j*8+6];
__xlx_tmp_lv.range(511,448) = ((long long*)__xlx_apatb_param_global_memory_ptr)[j*8+7];

    sprintf(__xlx_sprintf_buffer.data(), "%s\n", __xlx_tmp_lv.to_string(SC_HEX).c_str());
    aesl_fh.write(AUTOTB_TVOUT_global_memory, __xlx_sprintf_buffer.data()); 
      }
  }
}
  tcl_file.set_num(1, &tcl_file.global_memory_depth);
  sprintf(__xlx_sprintf_buffer.data(), "[[/transaction]] \n");
  aesl_fh.write(AUTOTB_TVOUT_global_memory, __xlx_sprintf_buffer.data());
}
// print external_memory Transactions
{
  sprintf(__xlx_sprintf_buffer.data(), "[[transaction]] %d\n", AESL_transaction);
  aesl_fh.write(AUTOTB_TVOUT_external_memory, __xlx_sprintf_buffer.data());
  {  __xlx_offset_byte_param_external_memory_ptr = 0*64;
  if (__xlx_apatb_param_external_memory_ptr) {
    for (int j = 0  - 0, e = 1 - 0; j != e; ++j) {
sc_bv<512> __xlx_tmp_lv;
__xlx_tmp_lv.range(63,0) = ((long long*)__xlx_apatb_param_external_memory_ptr)[j*8+0];
__xlx_tmp_lv.range(127,64) = ((long long*)__xlx_apatb_param_external_memory_ptr)[j*8+1];
__xlx_tmp_lv.range(191,128) = ((long long*)__xlx_apatb_param_external_memory_ptr)[j*8+2];
__xlx_tmp_lv.range(255,192) = ((long long*)__xlx_apatb_param_external_memory_ptr)[j*8+3];
__xlx_tmp_lv.range(319,256) = ((long long*)__xlx_apatb_param_external_memory_ptr)[j*8+4];
__xlx_tmp_lv.range(383,320) = ((long long*)__xlx_apatb_param_external_memory_ptr)[j*8+5];
__xlx_tmp_lv.range(447,384) = ((long long*)__xlx_apatb_param_external_memory_ptr)[j*8+6];
__xlx_tmp_lv.range(511,448) = ((long long*)__xlx_apatb_param_external_memory_ptr)[j*8+7];

    sprintf(__xlx_sprintf_buffer.data(), "%s\n", __xlx_tmp_lv.to_string(SC_HEX).c_str());
    aesl_fh.write(AUTOTB_TVOUT_external_memory, __xlx_sprintf_buffer.data()); 
      }
  }
}
  tcl_file.set_num(1, &tcl_file.external_memory_depth);
  sprintf(__xlx_sprintf_buffer.data(), "[[/transaction]] \n");
  aesl_fh.write(AUTOTB_TVOUT_external_memory, __xlx_sprintf_buffer.data());
}
CodeState = DELETE_CHAR_BUFFERS;
AESL_transaction++;
tcl_file.set_num(AESL_transaction , &tcl_file.trans_num);
}
