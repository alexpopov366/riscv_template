// This is a generated file. Use and modify at your own risk.
////////////////////////////////////////////////////////////////////////////////

//-----------------------------------------------------------------------------
// kernel: riscv_template
//
// Purpose: This is a C-model of the RTL kernel intended to be used for cpu
//          emulation.  It is designed to only be functionally equivalent to
//          the RTL Kernel.
//-----------------------------------------------------------------------------
#define WORD_SIZE 32
#define SHORT_WORD_SIZE 16
#define CHAR_WORD_SIZE 8
// Transfer size and buffer size are in words.
#define TRANSFER_SIZE_BITS WORD_SIZE*4096*8
#define BUFFER_WORD_SIZE 8192
#include <string.h>
#include <stdbool.h>
#include "hls_half.h"
#include "ap_axi_sdata.h"
#include "hls_stream.h"


// Function declaration/Interface pragmas to match RTL Kernel
extern "C" void riscv_template (
    unsigned char gpn_reset,
    unsigned int gpn_config,
    int* global_memory_ptr,
    int* external_memory_ptr
) {

    #pragma HLS INTERFACE m_axi port=global_memory_ptr offset=slave bundle=global_memory
    #pragma HLS INTERFACE m_axi port=external_memory_ptr offset=slave bundle=external_memory
    #pragma HLS INTERFACE s_axilite port=gpn_reset bundle=control
    #pragma HLS INTERFACE s_axilite port=gpn_config bundle=control
    #pragma HLS INTERFACE s_axilite port=global_memory_ptr bundle=control
    #pragma HLS INTERFACE s_axilite port=external_memory_ptr bundle=control
    #pragma HLS INTERFACE s_axilite port=return bundle=control
    #pragma HLS INTERFACE ap_ctrl_hs port=return

// Modify contents below to match the function of the RTL Kernel
    unsigned int data;

    // Create input and output buffers for interface global_memory
    int global_memory_input_buffer[BUFFER_WORD_SIZE];
    int global_memory_output_buffer[BUFFER_WORD_SIZE];


    // length is specified in number of words.
    unsigned int global_memory_length = 4096;


    // Assign input to a buffer
    memcpy(global_memory_input_buffer, (int*) global_memory_ptr, global_memory_length*sizeof(int));

    // Add 1 to input buffer and assign to output buffer.
    for (unsigned int i = 0; i < global_memory_length; i++) {
      global_memory_output_buffer[i] = global_memory_input_buffer[i]  + 1;
    }

    // assign output buffer out to memory
    memcpy((int*) global_memory_ptr, global_memory_output_buffer, global_memory_length*sizeof(int));


    // Create input and output buffers for interface external_memory
    int external_memory_input_buffer[BUFFER_WORD_SIZE];
    int external_memory_output_buffer[BUFFER_WORD_SIZE];


    // length is specified in number of words.
    unsigned int external_memory_length = 4096;


    // Assign input to a buffer
    memcpy(external_memory_input_buffer, (int*) external_memory_ptr, external_memory_length*sizeof(int));

    // Add 1 to input buffer and assign to output buffer.
    for (unsigned int i = 0; i < external_memory_length; i++) {
      external_memory_output_buffer[i] = external_memory_input_buffer[i]  + 1;
    }

    // assign output buffer out to memory
    memcpy((int*) external_memory_ptr, external_memory_output_buffer, external_memory_length*sizeof(int));


}

