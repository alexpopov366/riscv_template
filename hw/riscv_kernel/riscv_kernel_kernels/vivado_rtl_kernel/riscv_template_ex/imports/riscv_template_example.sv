// This is a generated file. Use and modify at your own risk.
//////////////////////////////////////////////////////////////////////////////// 
// default_nettype of none prevents implicit wire declaration.
`default_nettype none
module riscv_template_example #(
  parameter integer C_GLOBAL_MEMORY_ADDR_WIDTH   = 64,
  parameter integer C_GLOBAL_MEMORY_DATA_WIDTH   = 32,
  parameter integer C_EXTERNAL_MEMORY_ADDR_WIDTH = 64,
  parameter integer C_EXTERNAL_MEMORY_DATA_WIDTH = 32
)
(
  // System Signals
  input  wire                                      ap_clk                 ,
  input  wire                                      ap_rst_n               ,
  input  wire                                      ap_clk_2               ,
  input  wire                                      ap_rst_n_2             ,
  // AXI4 master interface global_memory
  output wire                                      global_memory_awvalid  ,
  input  wire                                      global_memory_awready  ,
  output wire [C_GLOBAL_MEMORY_ADDR_WIDTH-1:0]     global_memory_awaddr   ,
  output wire [8-1:0]                              global_memory_awlen    ,
  output wire                                      global_memory_wvalid   ,
  input  wire                                      global_memory_wready   ,
  output wire [C_GLOBAL_MEMORY_DATA_WIDTH-1:0]     global_memory_wdata    ,
  output wire [C_GLOBAL_MEMORY_DATA_WIDTH/8-1:0]   global_memory_wstrb    ,
  output wire                                      global_memory_wlast    ,
  input  wire                                      global_memory_bvalid   ,
  output wire                                      global_memory_bready   ,
  output wire                                      global_memory_arvalid  ,
  input  wire                                      global_memory_arready  ,
  output wire [C_GLOBAL_MEMORY_ADDR_WIDTH-1:0]     global_memory_araddr   ,
  output wire [8-1:0]                              global_memory_arlen    ,
  input  wire                                      global_memory_rvalid   ,
  output wire                                      global_memory_rready   ,
  input  wire [C_GLOBAL_MEMORY_DATA_WIDTH-1:0]     global_memory_rdata    ,
  input  wire                                      global_memory_rlast    ,
  // AXI4 master interface external_memory
  output wire                                      external_memory_awvalid,
  input  wire                                      external_memory_awready,
  output wire [C_EXTERNAL_MEMORY_ADDR_WIDTH-1:0]   external_memory_awaddr ,
  output wire [8-1:0]                              external_memory_awlen  ,
  output wire                                      external_memory_wvalid ,
  input  wire                                      external_memory_wready ,
  output wire [C_EXTERNAL_MEMORY_DATA_WIDTH-1:0]   external_memory_wdata  ,
  output wire [C_EXTERNAL_MEMORY_DATA_WIDTH/8-1:0] external_memory_wstrb  ,
  output wire                                      external_memory_wlast  ,
  input  wire                                      external_memory_bvalid ,
  output wire                                      external_memory_bready ,
  output wire                                      external_memory_arvalid,
  input  wire                                      external_memory_arready,
  output wire [C_EXTERNAL_MEMORY_ADDR_WIDTH-1:0]   external_memory_araddr ,
  output wire [8-1:0]                              external_memory_arlen  ,
  input  wire                                      external_memory_rvalid ,
  output wire                                      external_memory_rready ,
  input  wire [C_EXTERNAL_MEMORY_DATA_WIDTH-1:0]   external_memory_rdata  ,
  input  wire                                      external_memory_rlast  ,
  // Control Signals
  input  wire                                      ap_start               ,
  output wire                                      ap_idle                ,
  output wire                                      ap_done                ,
  output wire                                      ap_ready               ,
  input  wire [8-1:0]                              gpn_reset              ,
  input  wire [32-1:0]                             gpn_config             ,
  input  wire [64-1:0]                             global_memory_ptr      ,
  input  wire [64-1:0]                             external_memory_ptr    
);


timeunit 1ps;
timeprecision 1ps;

///////////////////////////////////////////////////////////////////////////////
// Local Parameters
///////////////////////////////////////////////////////////////////////////////
// Large enough for interesting traffic.
localparam integer  LP_DEFAULT_LENGTH_IN_BYTES = 16384;
localparam integer  LP_NUM_EXAMPLES    = 2;

///////////////////////////////////////////////////////////////////////////////
// Wires and Variables
///////////////////////////////////////////////////////////////////////////////
(* KEEP = "yes" *)
logic                                areset                         = 1'b0;
logic                                kernel_rst                     = 1'b0;
logic                                ap_start_r                     = 1'b0;
logic                                ap_idle_r                      = 1'b1;
logic                                ap_start_pulse                ;
logic [LP_NUM_EXAMPLES-1:0]          ap_done_i                     ;
logic [LP_NUM_EXAMPLES-1:0]          ap_done_r                      = {LP_NUM_EXAMPLES{1'b0}};
logic [32-1:0]                       ctrl_xfer_size_in_bytes        = LP_DEFAULT_LENGTH_IN_BYTES;
logic [32-1:0]                       ctrl_constant                  = 32'd1;

///////////////////////////////////////////////////////////////////////////////
// Begin RTL
///////////////////////////////////////////////////////////////////////////////

// Register and invert reset signal.
always @(posedge ap_clk) begin
  areset <= ~ap_rst_n;
end

// create pulse when ap_start transitions to 1
always @(posedge ap_clk) begin
  begin
    ap_start_r <= ap_start;
  end
end

assign ap_start_pulse = ap_start & ~ap_start_r;

// ap_idle is asserted when done is asserted, it is de-asserted when ap_start_pulse
// is asserted
always @(posedge ap_clk) begin
  if (areset) begin
    ap_idle_r <= 1'b1;
  end
  else begin
    ap_idle_r <= ap_done ? 1'b1 :
      ap_start_pulse ? 1'b0 : ap_idle;
  end
end

assign ap_idle = ap_idle_r;

// Done logic
always @(posedge ap_clk) begin
  if (areset) begin
    ap_done_r <= '0;
  end
  else begin
    ap_done_r <= (ap_done) ? '0 : ap_done_r | ap_done_i;
  end
end

assign ap_done = &ap_done_r;

// Ready Logic (non-pipelined case)
assign ap_ready = ap_done;


// Register and invert kernel reset signal.
always @(posedge ap_clk_2) begin
  kernel_rst <= ~ap_rst_n_2;
end


// Vadd example
riscv_template_example_vadd #(
  .C_M_AXI_ADDR_WIDTH ( C_GLOBAL_MEMORY_ADDR_WIDTH ),
  .C_M_AXI_DATA_WIDTH ( C_GLOBAL_MEMORY_DATA_WIDTH ),
  .C_ADDER_BIT_WIDTH  ( 32                         ),
  .C_XFER_SIZE_WIDTH  ( 32                         )
)
inst_example_vadd_global_memory (
  .aclk                    ( ap_clk                  ),
  .areset                  ( areset                  ),
  .kernel_clk              ( ap_clk_2                ),
  .kernel_rst              ( kernel_rst              ),
  .ctrl_addr_offset        ( global_memory_ptr       ),
  .ctrl_xfer_size_in_bytes ( ctrl_xfer_size_in_bytes ),
  .ctrl_constant           ( ctrl_constant           ),
  .ap_start                ( ap_start_pulse          ),
  .ap_done                 ( ap_done_i[0]            ),
  .m_axi_awvalid           ( global_memory_awvalid   ),
  .m_axi_awready           ( global_memory_awready   ),
  .m_axi_awaddr            ( global_memory_awaddr    ),
  .m_axi_awlen             ( global_memory_awlen     ),
  .m_axi_wvalid            ( global_memory_wvalid    ),
  .m_axi_wready            ( global_memory_wready    ),
  .m_axi_wdata             ( global_memory_wdata     ),
  .m_axi_wstrb             ( global_memory_wstrb     ),
  .m_axi_wlast             ( global_memory_wlast     ),
  .m_axi_bvalid            ( global_memory_bvalid    ),
  .m_axi_bready            ( global_memory_bready    ),
  .m_axi_arvalid           ( global_memory_arvalid   ),
  .m_axi_arready           ( global_memory_arready   ),
  .m_axi_araddr            ( global_memory_araddr    ),
  .m_axi_arlen             ( global_memory_arlen     ),
  .m_axi_rvalid            ( global_memory_rvalid    ),
  .m_axi_rready            ( global_memory_rready    ),
  .m_axi_rdata             ( global_memory_rdata     ),
  .m_axi_rlast             ( global_memory_rlast     )
);


// Vadd example
riscv_template_example_vadd #(
  .C_M_AXI_ADDR_WIDTH ( C_EXTERNAL_MEMORY_ADDR_WIDTH ),
  .C_M_AXI_DATA_WIDTH ( C_EXTERNAL_MEMORY_DATA_WIDTH ),
  .C_ADDER_BIT_WIDTH  ( 32                           ),
  .C_XFER_SIZE_WIDTH  ( 32                           )
)
inst_example_vadd_external_memory (
  .aclk                    ( ap_clk                  ),
  .areset                  ( areset                  ),
  .kernel_clk              ( ap_clk_2                ),
  .kernel_rst              ( kernel_rst              ),
  .ctrl_addr_offset        ( external_memory_ptr     ),
  .ctrl_xfer_size_in_bytes ( ctrl_xfer_size_in_bytes ),
  .ctrl_constant           ( ctrl_constant           ),
  .ap_start                ( ap_start_pulse          ),
  .ap_done                 ( ap_done_i[1]            ),
  .m_axi_awvalid           ( external_memory_awvalid ),
  .m_axi_awready           ( external_memory_awready ),
  .m_axi_awaddr            ( external_memory_awaddr  ),
  .m_axi_awlen             ( external_memory_awlen   ),
  .m_axi_wvalid            ( external_memory_wvalid  ),
  .m_axi_wready            ( external_memory_wready  ),
  .m_axi_wdata             ( external_memory_wdata   ),
  .m_axi_wstrb             ( external_memory_wstrb   ),
  .m_axi_wlast             ( external_memory_wlast   ),
  .m_axi_bvalid            ( external_memory_bvalid  ),
  .m_axi_bready            ( external_memory_bready  ),
  .m_axi_arvalid           ( external_memory_arvalid ),
  .m_axi_arready           ( external_memory_arready ),
  .m_axi_araddr            ( external_memory_araddr  ),
  .m_axi_arlen             ( external_memory_arlen   ),
  .m_axi_rvalid            ( external_memory_rvalid  ),
  .m_axi_rready            ( external_memory_rready  ),
  .m_axi_rdata             ( external_memory_rdata   ),
  .m_axi_rlast             ( external_memory_rlast   )
);


endmodule : riscv_template_example
`default_nettype wire
