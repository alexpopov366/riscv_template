


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;



---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;




entity QUEUE is
GENERIC (
	DATA_WIDTH: integer:=8;
    ALMOST_FULL_LEVEL: integer:=1;
	FIRST_WORD_FALL_THROUGH : string := "TRUE"; 	-- Sets the FIFO FWFT to TRUE or FALSE
	EMPTY_REG: integer := 0;						-- Enable EMPTY signal registering
	FULL_REG: integer := 0;							-- Enable FULL signal registering
	FIFO_TYPE : string := "FIFO18E2";				--"FIFO36E2","FIFO18E2"	
    CLOCK_DOMAINS : string := "INDEPENDENT"     	-- COMMON, INDEPENDENT
);
PORT(
	CLK_IN:IN std_logic;
	CLK_OUT:IN std_logic;
	CLK_INIT:IN std_logic;
	RST:IN std_logic;
	--Sheduler interface
	D_IN_VLD:IN std_logic; --Impulse wdth must be 1 clock
	D_IN:IN std_logic_vector(DATA_WIDTH-1 downto 0);
	Q_FULL:OUT std_logic;
	Q_ALMOST_FULL:OUT std_logic;
	--Arbitr interface
	D_OUT_NXT:IN std_logic; --Impulse wdth must be 1 clock
	D_OUT:OUT std_logic_vector(DATA_WIDTH-1 downto 0);
	Q_EMPTY:OUT std_logic
);
end QUEUE;


architecture Behavioral_FIFO_72 of QUEUE is

-- For FIFO18E2
signal		Q_FULL_MULT36,Q_EMPTY_MULT36:std_logic_vector((DATA_WIDTH+35)/36 - 1 downto 0) := (others=>'0');
signal		Q_ALMOST_FULL_36:std_logic_vector((DATA_WIDTH+35)/36 - 1 downto 0) := (others=>'0');
signal		Q_FULL36_int,Q_EMPTY36_int:std_logic_vector((DATA_WIDTH+35)/36 downto 0) := (others=>'0');
constant 	ZERO36 : std_logic_vector(36 * ((DATA_WIDTH+35)/36) - 1 downto 0) := (others=>'0');
signal		DATA_TO_QUEUE36: std_logic_vector(36 * ((DATA_WIDTH+35)/36) - 1 downto 0) := (others=>'0');
signal		DATA_FROM_QUEUE36: std_logic_vector(36 * ((DATA_WIDTH+35)/36) - 1 downto 0) := (others=>'0');

-- For FIFO35E2
signal		Q_FULL_MULT72,Q_EMPTY_MULT72:std_logic_vector((DATA_WIDTH+71)/72 - 1 downto 0) := (others=>'0');
signal		Q_ALMOST_FULL_72:std_logic_vector((DATA_WIDTH+71)/72 - 1 downto 0) := (others=>'0');
signal		Q_FULL72_int,Q_EMPTY72_int:std_logic_vector((DATA_WIDTH+71)/72 downto 0) := (others=>'0');
constant 	ZERO72 : std_logic_vector(72 * ((DATA_WIDTH+71)/72) - 1 downto 0) := (others=>'0');
signal		DATA_TO_QUEUE72: std_logic_vector(72 * ((DATA_WIDTH+71)/72) - 1 downto 0) := (others=>'0');
signal		DATA_FROM_QUEUE72: std_logic_vector(72 * ((DATA_WIDTH+71)/72) - 1 downto 0) := (others=>'0');
signal		D_IN_VLD_Int,D_OUT_NXT_Int: std_logic;

--Ignore RST timings
attribute TIG : string;
attribute TIG of RST: signal is "TRUE";


begin


	D_IN_VLD_Int<=D_IN_VLD;
	D_OUT_NXT_Int<=D_OUT_NXT;
	

	-------------------------------------------------
	--		SYNTHESIZABLE BRAM36 CACHE
	-------------------------------------------------


	FIFO18_INST: if (FIFO_TYPE="FIFO18E2") generate

		Q_FULL36_int(0)<='0'; -- "or" logic
		Q_EMPTY36_int(0)<='0';-- "or" logic
		Q_ALMOST_FULL<=Q_ALMOST_FULL_36(0);--get LSB

		QUEUE_inst: for I in 0 to (DATA_WIDTH+35)/36 - 1 generate

		   -- FIFO18E2: 18Kb FIFO (First-In-First-Out) Block RAM Memory
		   --           Kintex UltraScale+
		   -- Xilinx HDL Language Template, version 2019.1

		   FIFO18E2_inst : FIFO18E2
		   generic map (
		      CASCADE_ORDER => "NONE",            -- FIRST, LAST, MIDDLE, NONE, PARALLEL
		      CLOCK_DOMAINS => CLOCK_DOMAINS,     -- COMMON, INDEPENDENT
		      FIRST_WORD_FALL_THROUGH => FIRST_WORD_FALL_THROUGH, -- FALSE, TRUE
		      INIT => X"000000000",               -- Initial values on output port
		      PROG_EMPTY_THRESH => 256,           -- Programmable Empty Threshold
		      PROG_FULL_THRESH => ALMOST_FULL_LEVEL, -- Programmable Full Threshold
		      -- Programmable Inversion Attributes: Specifies the use of the built-in programmable inversion
		      IS_RDCLK_INVERTED => '0',           -- Optional inversion for RDCLK
		      IS_RDEN_INVERTED => '0',            -- Optional inversion for RDEN
		      IS_RSTREG_INVERTED => '0',          -- Optional inversion for RSTREG
		      IS_RST_INVERTED => '0',             -- Optional inversion for RST
		      IS_WRCLK_INVERTED => '0',           -- Optional inversion for WRCLK
		      IS_WREN_INVERTED => '0',            -- Optional inversion for WREN
		      RDCOUNT_TYPE => "RAW_PNTR",         -- EXTENDED_DATACOUNT, RAW_PNTR, SIMPLE_DATACOUNT, SYNC_PNTR
		      READ_WIDTH => 36,                    -- 18-9
		      REGISTER_MODE => "UNREGISTERED",    -- DO_PIPELINED, REGISTERED, UNREGISTERED
		      RSTREG_PRIORITY => "RSTREG",        -- REGCE, RSTREG
		      SLEEP_ASYNC => "FALSE",             -- FALSE, TRUE
		      SRVAL => X"000000000",              -- SET/reset value of the FIFO outputs
		      WRCOUNT_TYPE => "RAW_PNTR",         -- EXTENDED_DATACOUNT, RAW_PNTR, SIMPLE_DATACOUNT, SYNC_PNTR
		      WRITE_WIDTH => 36                    -- 18-9
		   )
		   port map (
		      -- Cascade Signals outputs: Multi-FIFO cascade signals
		      CASDOUT => open,             -- 64-bit output: Data cascade output bus
		      CASDOUTP => open,           -- 4-bit output: Parity data cascade output bus
		      CASNXTEMPTY => open,     -- 1-bit output: Cascade next empty
		      CASPRVRDEN => open,       -- 1-bit output: Cascade previous read enable
		      -- Read Data outputs: Read output data
		      DOUT => DATA_FROM_QUEUE36(I*36+31 downto I*36),                   -- 64-bit output: FIFO data output bus
		      DOUTP => DATA_FROM_QUEUE36(I*36+35 downto I*36+32),                 -- 4-bit output: FIFO parity output bus.
		      -- Status outputs: Flags and other FIFO status outputs
		      EMPTY => Q_EMPTY_MULT36(I),                 -- 1-bit output: Empty
		      FULL => Q_FULL_MULT36(I),                   -- 1-bit output: Full
		      PROGEMPTY => open,         -- 1-bit output: Programmable empty
		      PROGFULL => Q_ALMOST_FULL_36(I),           -- 1-bit output: Programmable full
		      RDCOUNT => open,             -- 13-bit output: Read count
		      RDERR => open,                 -- 1-bit output: Read error
		      RDRSTBUSY => open,         -- 1-bit output: Reset busy (sync to RDCLK)
		      WRCOUNT => open,             -- 13-bit output: Write count
		      WRERR => open,                 -- 1-bit output: Write Error
		      WRRSTBUSY => open,         -- 1-bit output: Reset busy (sync to WRCLK)
		      -- Cascade Signals inputs: Multi-FIFO cascade signals
		      CASDIN => X"00000000",               -- 64-bit input: Data cascade input bus
		      CASDINP => "0000",             -- 4-bit input: Parity data cascade input bus
		      CASDOMUX => '0',           -- 1-bit input: Cascade MUX select
		      CASDOMUXEN => '0',       -- 1-bit input: Enable for cascade MUX select
		      CASNXTRDEN => '0',       -- 1-bit input: Cascade next read enable
		      CASOREGIMUX => '0',     -- 1-bit input: Cascade output MUX select
		      CASOREGIMUXEN => '0', -- 1-bit input: Cascade output MUX select enable
		      CASPRVEMPTY => '0',     -- 1-bit input: Cascade previous empty
		      -- Read Control Signals inputs: Read clock, enable and reset input signals
		      RDCLK => CLK_OUT,                 -- 1-bit input: Read clock
		      RDEN => D_OUT_NXT_Int,                   -- 1-bit input: Read enable
		      REGCE => '1',                 -- 1-bit input: Output register clock enable
		      RSTREG => RST,               -- 1-bit input: Output register reset
		      SLEEP => '0',                 -- 1-bit input: Sleep Mode
		      -- Write Control Signals inputs: Write clock and enable input signals
		      RST => RST,                     -- 1-bit input: Reset
		      WRCLK => CLK_IN,                 -- 1-bit input: Write clock
		      WREN => D_IN_VLD_Int,                   -- 1-bit input: Write enable
		      -- Write Data inputs: Write input data
		      DIN => DATA_TO_QUEUE36(I*36+31 downto I*36),                     -- 64-bit input: FIFO data input bus
		      DINP => DATA_TO_QUEUE36(I*36+35 downto I*36+32)                    -- 4-bit input: FIFO parity input bus
		   );

		   -- End of FIFO36E2_inst instantiation

			Q_FULL36_int(I+1) <= Q_FULL_MULT36(I) or Q_FULL36_int(I);	
			Q_EMPTY36_int(I+1) <= Q_EMPTY_MULT36(I) or Q_EMPTY36_int(I);	
			
			
		end generate;

		DATA_TO_QUEUE36<=ZERO36(36 * ((DATA_WIDTH+35)/36) - 1 downto DATA_WIDTH)&D_IN;

		D_OUT<=DATA_FROM_QUEUE36(DATA_WIDTH-1 downto 0);

		--IF OUTPUT REGISTER USED
		FULL_GEN_REG: if FULL_REG=1 generate
			FULL_REG: process (CLK_IN)
			begin
				if (CLK_IN'event and CLK_IN = '1') then
					if (RST='1') then
						Q_FULL<='0';
					else
						Q_FULL<=Q_FULL36_int((DATA_WIDTH+35)/36);
					end if;
				end if;
			end process;
		end generate;	


		EMPTY_GEN_REG: if EMPTY_REG=1 generate
			EMPTY_REG: process (CLK_OUT)
			begin
				if (CLK_OUT'event and CLK_OUT = '1') then
					if (RST='1') then
						Q_EMPTY<='1';
					else
						Q_EMPTY<=Q_EMPTY36_int((DATA_WIDTH+35)/36);
					end if;
				end if;
			end process;	
		end generate;
		
		--IF OUTPUT REGISTER NOT USED
		EMPTY_GEN_COMB:if EMPTY_REG=0 generate
			Q_EMPTY<=Q_EMPTY36_int((DATA_WIDTH+35)/36);
		end generate;

		--IF OUTPUT REGISTER NOT USED
		FULL_GEN_COMB:if FULL_REG=0 generate
			Q_FULL<=Q_FULL36_int((DATA_WIDTH+35)/36);
		end generate;


	end generate;


	FIFO36_INST: if (FIFO_TYPE="FIFO36E2") generate

		Q_FULL72_int(0)<='0'; -- "or" logic
		Q_EMPTY72_int(0)<='0';-- "or" logic
		Q_ALMOST_FULL<=Q_ALMOST_FULL_72(0);--get LSB

		QUEUE_inst: for I in 0 to (DATA_WIDTH+71)/72 - 1 generate


		   -- FIFO72E2: 72Kb FIFO (First-In-First-Out) Block RAM Memory
		   --           Kintex UltraScale+
		   -- Xilinx HDL Language Template, version 2019.1

		   FIFO36E2_inst : FIFO36E2
		   generic map (
		      CASCADE_ORDER => "NONE",            -- FIRST, LAST, MIDDLE, NONE, PARALLEL
		      CLOCK_DOMAINS => "INDEPENDENT",     -- COMMON, INDEPENDENT
		      EN_ECC_PIPE => "FALSE",             -- ECC pipeline register, (FALSE, TRUE)
		      EN_ECC_READ => "FALSE",             -- Enable ECC decoder, (FALSE, TRUE)
		      EN_ECC_WRITE => "FALSE",            -- Enable ECC encoder, (FALSE, TRUE)
		      FIRST_WORD_FALL_THROUGH => FIRST_WORD_FALL_THROUGH, -- FALSE, TRUE
		      INIT => X"000000000000000000",      -- Initial values on output port
		      PROG_EMPTY_THRESH => 256,           -- Programmable Empty Threshold
		      PROG_FULL_THRESH => ALMOST_FULL_LEVEL,            -- Programmable Full Threshold
		      -- Programmable Inversion Attributes: Specifies the use of the built-in programmable inversion
		      IS_RDCLK_INVERTED => '0',           -- Optional inversion for RDCLK
		      IS_RDEN_INVERTED => '0',            -- Optional inversion for RDEN
		      IS_RSTREG_INVERTED => '0',          -- Optional inversion for RSTREG
		      IS_RST_INVERTED => '0',             -- Optional inversion for RST
		      IS_WRCLK_INVERTED => '0',           -- Optional inversion for WRCLK
		      IS_WREN_INVERTED => '0',            -- Optional inversion for WREN
		      RDCOUNT_TYPE => "RAW_PNTR",         -- EXTENDED_DATACOUNT, RAW_PNTR, SIMPLE_DATACOUNT, SYNC_PNTR
		      READ_WIDTH => 72,                    -- 36-9
		      REGISTER_MODE => "UNREGISTERED",    -- DO_PIPELINED, REGISTERED, UNREGISTERED
		      RSTREG_PRIORITY => "RSTREG",        -- REGCE, RSTREG
		      SLEEP_ASYNC => "FALSE",             -- FALSE, TRUE
		      SRVAL => X"000000000000000000",     -- SET/reset value of the FIFO outputs
		      WRCOUNT_TYPE => "RAW_PNTR",         -- EXTENDED_DATACOUNT, RAW_PNTR, SIMPLE_DATACOUNT, SYNC_PNTR
		      WRITE_WIDTH => 72                    -- 36-9
		   )
		   port map (
		      -- Cascade Signals outputs: Multi-FIFO cascade signals
		      CASDOUT => open,             -- 64-bit output: Data cascade output bus
		      CASDOUTP => open,           -- 8-bit output: Parity data cascade output bus
		      CASNXTEMPTY => open,     -- 1-bit output: Cascade next empty
		      CASPRVRDEN => open,       -- 1-bit output: Cascade previous read enable
		      -- ECC Signals outputs: Error Correction Circuitry ports
		      DBITERR => open,             -- 1-bit output: Double bit error status
		      ECCPARITY => open,         -- 8-bit output: Generated error correction parity
		      SBITERR => open,             -- 1-bit output: Single bit error status
		      -- Read Data outputs: Read output data
		      DOUT => DATA_FROM_QUEUE72(I*72+63 downto I*72),                   -- 64-bit output: FIFO data output bus
		      DOUTP => DATA_FROM_QUEUE72(I*72+71 downto I*72+64),                 -- 8-bit output: FIFO parity output bus.
		      -- Status outputs: Flags and other FIFO status outputs
		      EMPTY => Q_EMPTY_MULT72(I),                 -- 1-bit output: Empty
		      FULL => Q_FULL_MULT72(I),                   -- 1-bit output: Full
		      PROGEMPTY => open,         -- 1-bit output: Programmable empty
		      PROGFULL => Q_ALMOST_FULL_72(I),           -- 1-bit output: Programmable full
		      RDCOUNT => open,             -- 14-bit output: Read count
		      RDERR => open,                 -- 1-bit output: Read error
		      RDRSTBUSY => open,         -- 1-bit output: Reset busy (sync to RDCLK)
		      WRCOUNT => open,             -- 14-bit output: Write count
		      WRERR => open,                 -- 1-bit output: Write Error
		      WRRSTBUSY => open,         -- 1-bit output: Reset busy (sync to WRCLK)
		      -- Cascade Signals inputs: Multi-FIFO cascade signals
		      CASDIN => X"0000000000000000",               -- 64-bit input: Data cascade input bus
		      CASDINP => "00000000",             -- 8-bit input: Parity data cascade input bus
		      CASDOMUX => '0',           -- 1-bit input: Cascade MUX select input
		      CASDOMUXEN => '0',       -- 1-bit input: Enable for cascade MUX select
		      CASNXTRDEN => '0',       -- 1-bit input: Cascade next read enable
		      CASOREGIMUX => '0',     -- 1-bit input: Cascade output MUX select
		      CASOREGIMUXEN => '0', -- 1-bit input: Cascade output MUX select enable
		      CASPRVEMPTY => '0',     -- 1-bit input: Cascade previous empty
		      -- ECC Signals inputs: Error Correction Circuitry ports
		      INJECTDBITERR => '0', -- 1-bit input: Inject a double bit error
		      INJECTSBITERR => '0', -- 1-bit input: Inject a single bit error
		      -- Read Control Signals inputs: Read clock, enable and reset input signals
		      RDCLK => CLK_OUT,                 -- 1-bit input: Read clock
		      RDEN => D_OUT_NXT_Int,                   -- 1-bit input: Read enable
		      REGCE => '1',                 -- 1-bit input: Output register clock enable
		      RSTREG => RST,               -- 1-bit input: Output register reset
		      SLEEP => '0',                 -- 1-bit input: Sleep Mode
		      -- Write Control Signals inputs: Write clock and enable input signals
		      RST => RST,                     -- 1-bit input: Reset
		      WRCLK => CLK_IN,                 -- 1-bit input: Write clock
		      WREN => D_IN_VLD_Int,                   -- 1-bit input: Write enable
		      -- Write Data inputs: Write input data
		      DIN => DATA_TO_QUEUE72(I*72+63 downto I*72),                     -- 64-bit input: FIFO data input bus
		      DINP => DATA_TO_QUEUE72(I*72+71 downto I*72+64)                    -- 8-bit input: FIFO parity input bus
		   );

		   -- End of FIFO36E2_inst instantiation

			Q_FULL72_int(I+1) <= Q_FULL_MULT72(I) or Q_FULL72_int(I);	
			Q_EMPTY72_int(I+1) <= Q_EMPTY_MULT72(I) or Q_EMPTY72_int(I);	
			
			
		end generate;

		DATA_TO_QUEUE72<=ZERO72(72 * ((DATA_WIDTH+71)/72) - 1 downto DATA_WIDTH)&D_IN;

		D_OUT<=DATA_FROM_QUEUE72(DATA_WIDTH-1 downto 0);

		--IF OUTPUT REGISTER USED
		FULL_GEN_REG: if FULL_REG=1 generate
			FULL_REG: process (CLK_IN)
			begin
				if (CLK_IN'event and CLK_IN = '1') then
					if (RST='1') then
						Q_FULL<='0';
					else
						Q_FULL<=Q_FULL72_int((DATA_WIDTH+71)/72);
					end if;
				end if;
			end process;
		end generate;	


		EMPTY_GEN_REG: if EMPTY_REG=1 generate
			EMPTY_REG: process (CLK_OUT)
			begin
				if (CLK_OUT'event and CLK_OUT = '1') then
					if (RST='1') then
						Q_EMPTY<='1';
					else
						Q_EMPTY<=Q_EMPTY72_int((DATA_WIDTH+71)/72);
					end if;
				end if;
			end process;	
		end generate;
		
		--IF OUTPUT REGISTER NOT USED
		EMPTY_GEN_COMB:if EMPTY_REG=0 generate
			Q_EMPTY<=Q_EMPTY72_int((DATA_WIDTH+71)/72);
		end generate;

		--IF OUTPUT REGISTER NOT USED
		FULL_GEN_COMB:if FULL_REG=0 generate
			Q_FULL<=Q_FULL72_int((DATA_WIDTH+71)/72);
		end generate;


	end generate;


end Behavioral_FIFO_72;


--architecture Behavioral_FIFO_72 of QUEUE is
--
--signal		Q_FULL_MULT72,Q_EMPTY_MULT72:std_logic_vector((DATA_WIDTH+71)/72 - 1 downto 0);
--constant 	ZERO72 : std_logic_vector(72 * ((DATA_WIDTH+71)/72) - 1 downto 0) := (others=>'0');
--signal		DATA_TO_QUEUE72: std_logic_vector(72 * ((DATA_WIDTH+71)/72) - 1 downto 0);
--signal		DATA_FROM_QUEUE72: std_logic_vector(72 * ((DATA_WIDTH+71)/72) - 1 downto 0);
--
--
--begin
--
--
--
--	QUEUE_inst: for I in 0 to (DATA_WIDTH+71)/72 - 1 generate
--
--	FIFO72E1_inst : FIFO72E1
--	generic map (
--		ALMOST_EMPTY_OFFSET => X"0080",   -- Sets the almost empty threshold
--		ALMOST_FULL_OFFSET => ALMOST_FULL_LEVEL,    -- Sets almost full threshold
--		DATA_WIDTH => 72,                  -- Sets data width to 4, 9, 36, 72, or 72
--		DO_REG => DO_REG,                      -- Enable output register (0 or 1) Must be 1 if EN_SYN = FALSE
--		EN_ECC_READ => FALSE,             -- Enable ECC decoder, TRUE or FALSE
--		EN_ECC_WRITE => FALSE,            -- Enable ECC encoder, TRUE or FALSE
--		EN_SYN => EN_SYN,                  -- Specifies FIFO as Asynchronous (FALSE) or Synchronous (TRUE)
--		FIFO_MODE => "FIFO72_72",            -- Sets mode to FIFO72 or FIFO72_72
--		FIRST_WORD_FALL_THROUGH => FIRST_WORD_FALL_THROUGH, -- Sets the FIFO FWFT to TRUE or FALSE
--		INIT => X"000000000000000000",    -- Initial values on output port
--		SRVAL => X"000000000000000000"    -- Set/Reset value for output port
--	)
--	port map (
--		-- ECC Signals: 1-bit (each) output: Error Correction Circuitry ports
--		DBITERR => open,             -- 1-bit output: double bit error status output
--		ECCPARITY => open,         -- 8-bit output: generated error correction parity
--		SBITERR => open,             -- 1-bit output: single bit error status output
--		-- Read Data: 64-bit (each) output: Read output data
--		DO => DATA_FROM_QUEUE72(I*72+63 downto I*72),                   -- 64-bit output: data output
--		DOP => DATA_FROM_QUEUE72(I*72+71 downto I*72+64),               -- 8-bit output: parity data output
--		-- Status: 1-bit (each) output: Flags and other FIFO status outputs
--		ALMOSTEMPTY => open, -- 1-bit output: almost empty output flag
--		ALMOSTFULL => open,   -- 1-bit output: almost full output flag
--		EMPTY => Q_EMPTY_MULT72(I),             -- 1-bit output: empty output flag
--		FULL => Q_FULL_MULT72(I),               -- 1-bit output: full output flag
--		RDCOUNT => open,         -- 13-bit output: read count output
--		RDERR => open,             -- 1-bit output: read error output
--		WRCOUNT => open,         -- 13-bit output: write count output
--		WRERR => open,             -- 1-bit output: write error
--		-- ECC Signals: 1-bit (each) input: Error Correction Circuitry ports
--		INJECTDBITERR => '0', -- 1-bit input: Inject a double bit error
--		INJECTSBITERR => '0',
--		-- Read Control Signals: 1-bit (each) input: Read clock, enable and reset input signals
--		RDCLK => CLK_OUT,             -- 1-bit input: read clock input
--		RDEN => D_OUT_NXT,               -- 1-bit input: read enable input
--		REGCE => '1',             -- 1-bit input: clock enable input
--		RST => RST,                 -- 1-bit input: reset input
--		RSTREG => RST,           -- 1-bit input: output register set/reset
--		-- Write Control Signals: 1-bit (each) input: Write clock and enable input signals
--		WRCLK => CLK_IN,             -- 1-bit input: write clock input
--		WREN => D_IN_VLD,               -- 1-bit input: write enable input
--		-- Write Data: 64-bit (each) input: Write input data
--		DI => DATA_TO_QUEUE72(I*72+63 downto I*72),                   -- 64-bit input: data input
--		DIP => DATA_TO_QUEUE72(I*72+71 downto I*72+64)                  -- 8-bit input: parity input
--	);
--
--	-- End of FIFO72E1_inst instantiation
--	end generate;
--
--	DATA_TO_QUEUE72<=ZERO72(72 * ((DATA_WIDTH+71)/72) - 1 downto DATA_WIDTH)&D_IN;
--	D_OUT<=DATA_FROM_QUEUE72(DATA_WIDTH-1 downto 0);
--
--	Q_FULL<=Q_FULL_MULT72(0);
--	Q_EMPTY<=Q_EMPTY_MULT72(0);
--
--
--end Behavioral_FIFO_72;


--architecture Behavioral_BRAM of QUEUE is
--
--	TYPE FIFO_MEM is array(2**QUEUE_WIDTH-1 downto 0) of std_logic_vector(DATA_WIDTH-1 downto 0);
--
--		 impure function INIT_FIFO (init_val : in std_logic) return FIFO_MEM is                                                   
--			 variable FIFO : FIFO_MEM;                                      
--		 begin                                                        
--			 for I in FIFO_MEM'range loop                                  
--				  FIFO(I):=(others=>init_val);                                  
--			 end loop;                                                    
--			 return FIFO;                                                  
--		 end function;                                                
--
--	signal FIFO:FIFO_MEM:=INIT_FIFO('0');
--
--	signal LAST,NEXT_LAST,GRAY_LAST,FIRST,NEXT_FIRST,GRAY_FIRST: std_logic_vector(QUEUE_WIDTH-1 downto 0);
--	signal Q_FULL_S,Q_FULL_R,Q_EMPTY_S,Q_EMPTY_R,Q_FULL72_int,Q_EMPTY72_int:std_logic;
--	signal FF_Q_FULL72_int,FF_Q_EMPTY72_int:std_logic;
--
--
--begin
--
--
--		EMPTY_MUX : process (Q_EMPTY_S,Q_EMPTY_R,RST)
--		begin
--			if (Q_EMPTY_R='1' and Q_EMPTY_S='0') then
--				Q_EMPTY72_int<='0';
--			elsif (Q_EMPTY_R='0' and Q_EMPTY_S='1') then
--				Q_EMPTY72_int<='1';
--			else 
--				Q_EMPTY72_int<=FF_Q_EMPTY72_int;
--			end if;
--		end process;
--
--
--		EMPTY_FF : process (CLK_IN)
--		begin
--			if (CLK_IN'event and CLK_IN='1') then
--				if (RST='1') then
--					FF_Q_EMPTY72_int<='1';
--				elsif (Q_EMPTY_R='1' and Q_EMPTY_S='0') then
--					FF_Q_EMPTY72_int<='0';
--				elsif (Q_EMPTY_R='0' and Q_EMPTY_S='1') then
--					FF_Q_EMPTY72_int<='1';
--				end if;
--			end if;
--		end process;
--
--		
--		FULL_MUX : process (Q_FULL_S,Q_FULL_R,RST)
--		begin
--			if (Q_FULL_R='1' and Q_FULL_S='0') then
--				Q_FULL72_int<='0';
--			elsif (Q_FULL_R='0' and Q_FULL_S='1') then
--				Q_FULL72_int<='1';
--			else 
--				Q_FULL72_int<=FF_Q_FULL72_int;
--			end if;
--		end process;
--
--
--		FULL_FF : process (CLK_IN)
--		begin
--			if (CLK_IN'event and CLK_IN='1') then
--				if (RST='1') then
--					FF_Q_FULL72_int<='0';
--				elsif (Q_FULL_R='1' and Q_FULL_S='0') then
--					FF_Q_FULL72_int<='0';
--				elsif (Q_FULL_R='0' and Q_FULL_S='1') then
--					FF_Q_FULL72_int<='1';
--				end if;
--			end if;
--		end process;
--
--
--	Q_EMPTY<=Q_EMPTY72_int;
--	Q_FULL<=Q_FULL72_int;
--
--	--DUAL PORT DISTRIBUTED FIFO MEMORY
--
--	process (CLK_IN)
--	begin
--		if (CLK_IN'event and CLK_IN = '1') then
--			if (D_IN_VLD = '1' and Q_FULL72_int='0') then
--				FIFO(conv_integer(LAST)) <= D_IN;
--			end if;
--		end if;
--	end process;
--
--	D_OUT <= FIFO(conv_integer(FIRST));
--
--
--	--FIRST GRAY Counter
--
--	NEXT_FIRST <= FIRST + 1; 
--	  
--	process(CLK_IN,RST)
--	begin
--		if ( RST = '1') then
--			FIRST <= (others => '0'); 
--			GRAY_FIRST <= (others =>'0');
--			Q_EMPTY_S<='0';
--			Q_FULL_R<='0';
--		elsif ( CLK_IN'event and CLK_IN ='1') then 
--			Q_EMPTY_S<='0';
--			Q_FULL_R<='0';
--			if D_OUT_NXT='1' and Q_EMPTY72_int='0' then 
--				Q_FULL_R<='1';
--				FIRST <= NEXT_FIRST;
--				GRAY_FIRST <= (('0' & NEXT_FIRST(QUEUE_WIDTH-1 downto 1)) XOR NEXT_FIRST); 
--					if NEXT_FIRST=LAST then 
--						 Q_EMPTY_S<='1';
--					 end if;   
--			end if;
--		end if;
--	end process;
--
--	--LAST GRAY Counter
--
--	NEXT_LAST <= LAST + 1; 
--	  
--	process(CLK_IN,RST)
--	begin
--		if ( RST = '1') then
--			LAST <= (others => '0'); 
--			GRAY_LAST <= (others =>'0'); 
--			Q_EMPTY_R<='0';
--			Q_FULL_S<='0';
--		elsif ( CLK_IN'event and CLK_IN ='1') then 
--			Q_EMPTY_R<='0';
--			Q_FULL_S<='0';
--			if D_IN_VLD='1' and Q_FULL72_int='0' then 
--				Q_EMPTY_R<='1';
--				LAST <= NEXT_LAST;
--				GRAY_LAST <= (('0' & NEXT_LAST(QUEUE_WIDTH-1 downto 1)) XOR NEXT_LAST); 
--					if NEXT_LAST=FIRST then 
--						 Q_FULL_S<='1';
--					 end if;
--			end if;
--		end if;
--	end process;
--
--
--end Behavioral_BRAM;
--



