-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
-- Date        : Sun Aug 28 18:14:55 2022
-- Host        : dl580 running 64-bit Ubuntu 18.04.6 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ gpn_bundle_block_local_rom_0_0_sim_netlist.vhdl
-- Design      : gpn_bundle_block_local_rom_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xcu200-fsgd2104-2-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xilinx_byte_enable_rom is
  port (
    porta_data_out : out STD_LOGIC_VECTOR ( 24 downto 0 );
    porta_addr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    porta_en : in STD_LOGIC;
    clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xilinx_byte_enable_rom;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xilinx_byte_enable_rom is
  signal \data_out_a[10]_i_1_n_0\ : STD_LOGIC;
  signal \data_out_a[11]_i_1_n_0\ : STD_LOGIC;
  signal \data_out_a[12]_i_1_n_0\ : STD_LOGIC;
  signal \data_out_a[13]_i_1_n_0\ : STD_LOGIC;
  signal \data_out_a[14]_i_1_n_0\ : STD_LOGIC;
  signal \data_out_a[15]_i_1_n_0\ : STD_LOGIC;
  signal \data_out_a[16]_i_1_n_0\ : STD_LOGIC;
  signal \data_out_a[17]_i_1_n_0\ : STD_LOGIC;
  signal \data_out_a[18]_i_1_n_0\ : STD_LOGIC;
  signal \data_out_a[1]_i_1_n_0\ : STD_LOGIC;
  signal \data_out_a[20]_i_1_n_0\ : STD_LOGIC;
  signal \data_out_a[21]_i_1_n_0\ : STD_LOGIC;
  signal \data_out_a[22]_i_1_n_0\ : STD_LOGIC;
  signal \data_out_a[23]_i_1_n_0\ : STD_LOGIC;
  signal \data_out_a[25]_i_1_n_0\ : STD_LOGIC;
  signal \data_out_a[29]_i_1_n_0\ : STD_LOGIC;
  signal \data_out_a[2]_i_1_n_0\ : STD_LOGIC;
  signal \data_out_a[30]_i_1_n_0\ : STD_LOGIC;
  signal \data_out_a[31]_i_1_n_0\ : STD_LOGIC;
  signal \data_out_a[4]_i_1_n_0\ : STD_LOGIC;
  signal \data_out_a[5]_i_1_n_0\ : STD_LOGIC;
  signal \data_out_a[6]_i_1_n_0\ : STD_LOGIC;
  signal \data_out_a[7]_i_1_n_0\ : STD_LOGIC;
  signal \data_out_a[8]_i_1_n_0\ : STD_LOGIC;
  signal \data_out_a[9]_i_1_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \data_out_a[10]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \data_out_a[11]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \data_out_a[12]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \data_out_a[13]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \data_out_a[14]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \data_out_a[15]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \data_out_a[16]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \data_out_a[17]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \data_out_a[18]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \data_out_a[1]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \data_out_a[20]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \data_out_a[21]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \data_out_a[22]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \data_out_a[23]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \data_out_a[25]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \data_out_a[29]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \data_out_a[2]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \data_out_a[31]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \data_out_a[4]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \data_out_a[5]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \data_out_a[6]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \data_out_a[7]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \data_out_a[8]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \data_out_a[9]_i_1\ : label is "soft_lutpair3";
begin
\data_out_a[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"37137767"
    )
        port map (
      I0 => porta_addr(3),
      I1 => porta_addr(4),
      I2 => porta_addr(2),
      I3 => porta_addr(0),
      I4 => porta_addr(1),
      O => \data_out_a[10]_i_1_n_0\
    );
\data_out_a[11]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00400100"
    )
        port map (
      I0 => porta_addr(3),
      I1 => porta_addr(4),
      I2 => porta_addr(2),
      I3 => porta_addr(1),
      I4 => porta_addr(0),
      O => \data_out_a[11]_i_1_n_0\
    );
\data_out_a[12]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00002000"
    )
        port map (
      I0 => porta_addr(0),
      I1 => porta_addr(1),
      I2 => porta_addr(2),
      I3 => porta_addr(4),
      I4 => porta_addr(3),
      O => \data_out_a[12]_i_1_n_0\
    );
\data_out_a[13]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"22112450"
    )
        port map (
      I0 => porta_addr(3),
      I1 => porta_addr(4),
      I2 => porta_addr(2),
      I3 => porta_addr(1),
      I4 => porta_addr(0),
      O => \data_out_a[13]_i_1_n_0\
    );
\data_out_a[14]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00008000"
    )
        port map (
      I0 => porta_addr(3),
      I1 => porta_addr(1),
      I2 => porta_addr(0),
      I3 => porta_addr(2),
      I4 => porta_addr(4),
      O => \data_out_a[14]_i_1_n_0\
    );
\data_out_a[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60600738"
    )
        port map (
      I0 => porta_addr(3),
      I1 => porta_addr(4),
      I2 => porta_addr(2),
      I3 => porta_addr(1),
      I4 => porta_addr(0),
      O => \data_out_a[15]_i_1_n_0\
    );
\data_out_a[16]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0C3D3F03"
    )
        port map (
      I0 => porta_addr(0),
      I1 => porta_addr(3),
      I2 => porta_addr(4),
      I3 => porta_addr(2),
      I4 => porta_addr(1),
      O => \data_out_a[16]_i_1_n_0\
    );
\data_out_a[17]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"27377571"
    )
        port map (
      I0 => porta_addr(3),
      I1 => porta_addr(4),
      I2 => porta_addr(2),
      I3 => porta_addr(0),
      I4 => porta_addr(1),
      O => \data_out_a[17]_i_1_n_0\
    );
\data_out_a[18]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"22713770"
    )
        port map (
      I0 => porta_addr(3),
      I1 => porta_addr(4),
      I2 => porta_addr(2),
      I3 => porta_addr(1),
      I4 => porta_addr(0),
      O => \data_out_a[18]_i_1_n_0\
    );
\data_out_a[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"01FFFFFF"
    )
        port map (
      I0 => porta_addr(0),
      I1 => porta_addr(1),
      I2 => porta_addr(2),
      I3 => porta_addr(4),
      I4 => porta_addr(3),
      O => \data_out_a[1]_i_1_n_0\
    );
\data_out_a[20]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02020004"
    )
        port map (
      I0 => porta_addr(3),
      I1 => porta_addr(4),
      I2 => porta_addr(0),
      I3 => porta_addr(1),
      I4 => porta_addr(2),
      O => \data_out_a[20]_i_1_n_0\
    );
\data_out_a[21]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00020600"
    )
        port map (
      I0 => porta_addr(3),
      I1 => porta_addr(4),
      I2 => porta_addr(0),
      I3 => porta_addr(1),
      I4 => porta_addr(2),
      O => \data_out_a[21]_i_1_n_0\
    );
\data_out_a[22]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"22200400"
    )
        port map (
      I0 => porta_addr(3),
      I1 => porta_addr(4),
      I2 => porta_addr(0),
      I3 => porta_addr(1),
      I4 => porta_addr(2),
      O => \data_out_a[22]_i_1_n_0\
    );
\data_out_a[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"22120600"
    )
        port map (
      I0 => porta_addr(3),
      I1 => porta_addr(4),
      I2 => porta_addr(0),
      I3 => porta_addr(1),
      I4 => porta_addr(2),
      O => \data_out_a[23]_i_1_n_0\
    );
\data_out_a[25]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20401100"
    )
        port map (
      I0 => porta_addr(3),
      I1 => porta_addr(4),
      I2 => porta_addr(2),
      I3 => porta_addr(1),
      I4 => porta_addr(0),
      O => \data_out_a[25]_i_1_n_0\
    );
\data_out_a[29]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"24540013"
    )
        port map (
      I0 => porta_addr(3),
      I1 => porta_addr(4),
      I2 => porta_addr(1),
      I3 => porta_addr(2),
      I4 => porta_addr(0),
      O => \data_out_a[29]_i_1_n_0\
    );
\data_out_a[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"4056400B"
    )
        port map (
      I0 => porta_addr(3),
      I1 => porta_addr(4),
      I2 => porta_addr(1),
      I3 => porta_addr(2),
      I4 => porta_addr(0),
      O => \data_out_a[2]_i_1_n_0\
    );
\data_out_a[30]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20400100"
    )
        port map (
      I0 => porta_addr(3),
      I1 => porta_addr(4),
      I2 => porta_addr(2),
      I3 => porta_addr(1),
      I4 => porta_addr(0),
      O => \data_out_a[30]_i_1_n_0\
    );
\data_out_a[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"24564013"
    )
        port map (
      I0 => porta_addr(3),
      I1 => porta_addr(4),
      I2 => porta_addr(1),
      I3 => porta_addr(2),
      I4 => porta_addr(0),
      O => \data_out_a[31]_i_1_n_0\
    );
\data_out_a[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"15264227"
    )
        port map (
      I0 => porta_addr(3),
      I1 => porta_addr(4),
      I2 => porta_addr(2),
      I3 => porta_addr(1),
      I4 => porta_addr(0),
      O => \data_out_a[4]_i_1_n_0\
    );
\data_out_a[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"393F344F"
    )
        port map (
      I0 => porta_addr(2),
      I1 => porta_addr(3),
      I2 => porta_addr(4),
      I3 => porta_addr(0),
      I4 => porta_addr(1),
      O => \data_out_a[5]_i_1_n_0\
    );
\data_out_a[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"38000242"
    )
        port map (
      I0 => porta_addr(1),
      I1 => porta_addr(3),
      I2 => porta_addr(4),
      I3 => porta_addr(2),
      I4 => porta_addr(0),
      O => \data_out_a[6]_i_1_n_0\
    );
\data_out_a[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"73674140"
    )
        port map (
      I0 => porta_addr(3),
      I1 => porta_addr(4),
      I2 => porta_addr(2),
      I3 => porta_addr(1),
      I4 => porta_addr(0),
      O => \data_out_a[7]_i_1_n_0\
    );
\data_out_a[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"15022767"
    )
        port map (
      I0 => porta_addr(3),
      I1 => porta_addr(4),
      I2 => porta_addr(2),
      I3 => porta_addr(0),
      I4 => porta_addr(1),
      O => \data_out_a[8]_i_1_n_0\
    );
\data_out_a[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"37037565"
    )
        port map (
      I0 => porta_addr(3),
      I1 => porta_addr(4),
      I2 => porta_addr(2),
      I3 => porta_addr(0),
      I4 => porta_addr(1),
      O => \data_out_a[9]_i_1_n_0\
    );
\data_out_a_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => porta_en,
      D => \data_out_a[10]_i_1_n_0\,
      Q => porta_data_out(8),
      R => '0'
    );
\data_out_a_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => porta_en,
      D => \data_out_a[11]_i_1_n_0\,
      Q => porta_data_out(9),
      R => '0'
    );
\data_out_a_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => porta_en,
      D => \data_out_a[12]_i_1_n_0\,
      Q => porta_data_out(10),
      R => '0'
    );
\data_out_a_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => porta_en,
      D => \data_out_a[13]_i_1_n_0\,
      Q => porta_data_out(11),
      R => '0'
    );
\data_out_a_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => porta_en,
      D => \data_out_a[14]_i_1_n_0\,
      Q => porta_data_out(12),
      R => '0'
    );
\data_out_a_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => porta_en,
      D => \data_out_a[15]_i_1_n_0\,
      Q => porta_data_out(13),
      R => '0'
    );
\data_out_a_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => porta_en,
      D => \data_out_a[16]_i_1_n_0\,
      Q => porta_data_out(14),
      R => '0'
    );
\data_out_a_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => porta_en,
      D => \data_out_a[17]_i_1_n_0\,
      Q => porta_data_out(15),
      R => '0'
    );
\data_out_a_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => porta_en,
      D => \data_out_a[18]_i_1_n_0\,
      Q => porta_data_out(16),
      R => '0'
    );
\data_out_a_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => porta_en,
      D => \data_out_a[1]_i_1_n_0\,
      Q => porta_data_out(0),
      R => '0'
    );
\data_out_a_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => porta_en,
      D => \data_out_a[20]_i_1_n_0\,
      Q => porta_data_out(17),
      R => '0'
    );
\data_out_a_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => porta_en,
      D => \data_out_a[21]_i_1_n_0\,
      Q => porta_data_out(18),
      R => '0'
    );
\data_out_a_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => porta_en,
      D => \data_out_a[22]_i_1_n_0\,
      Q => porta_data_out(19),
      R => '0'
    );
\data_out_a_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => porta_en,
      D => \data_out_a[23]_i_1_n_0\,
      Q => porta_data_out(20),
      R => '0'
    );
\data_out_a_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => porta_en,
      D => \data_out_a[25]_i_1_n_0\,
      Q => porta_data_out(21),
      R => '0'
    );
\data_out_a_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => porta_en,
      D => \data_out_a[29]_i_1_n_0\,
      Q => porta_data_out(22),
      R => '0'
    );
\data_out_a_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => porta_en,
      D => \data_out_a[2]_i_1_n_0\,
      Q => porta_data_out(1),
      R => '0'
    );
\data_out_a_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => porta_en,
      D => \data_out_a[30]_i_1_n_0\,
      Q => porta_data_out(23),
      R => '0'
    );
\data_out_a_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => porta_en,
      D => \data_out_a[31]_i_1_n_0\,
      Q => porta_data_out(24),
      R => '0'
    );
\data_out_a_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => porta_en,
      D => \data_out_a[4]_i_1_n_0\,
      Q => porta_data_out(2),
      R => '0'
    );
\data_out_a_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => porta_en,
      D => \data_out_a[5]_i_1_n_0\,
      Q => porta_data_out(3),
      R => '0'
    );
\data_out_a_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => porta_en,
      D => \data_out_a[6]_i_1_n_0\,
      Q => porta_data_out(4),
      R => '0'
    );
\data_out_a_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => porta_en,
      D => \data_out_a[7]_i_1_n_0\,
      Q => porta_data_out(5),
      R => '0'
    );
\data_out_a_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => porta_en,
      D => \data_out_a[8]_i_1_n_0\,
      Q => porta_data_out(6),
      R => '0'
    );
\data_out_a_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => porta_en,
      D => \data_out_a[9]_i_1_n_0\,
      Q => porta_data_out(7),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_byte_en_ROM is
  port (
    porta_data_out : out STD_LOGIC_VECTOR ( 24 downto 0 );
    porta_addr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    porta_en : in STD_LOGIC;
    clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_byte_en_ROM;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_byte_en_ROM is
begin
ram_block: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xilinx_byte_enable_rom
     port map (
      clk => clk,
      porta_addr(4 downto 0) => porta_addr(4 downto 0),
      porta_data_out(24 downto 0) => porta_data_out(24 downto 0),
      porta_en => porta_en
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_local_rom is
  port (
    porta_data_out : out STD_LOGIC_VECTOR ( 24 downto 0 );
    porta_addr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    porta_en : in STD_LOGIC;
    clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_local_rom;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_local_rom is
begin
inst_data_rom: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_byte_en_ROM
     port map (
      clk => clk,
      porta_addr(4 downto 0) => porta_addr(4 downto 0),
      porta_data_out(24 downto 0) => porta_data_out(24 downto 0),
      porta_en => porta_en
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    clk : in STD_LOGIC;
    rstn : in STD_LOGIC;
    porta_addr : in STD_LOGIC_VECTOR ( 29 downto 0 );
    porta_en : in STD_LOGIC;
    porta_be : in STD_LOGIC_VECTOR ( 3 downto 0 );
    porta_data_in : in STD_LOGIC_VECTOR ( 31 downto 0 );
    porta_data_out : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "gpn_bundle_block_local_rom_0_0,local_rom,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "package_project";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "local_rom,Vivado 2020.2";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  signal \^porta_data_out\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of clk : signal is "xilinx.com:signal:clock:1.0 clk CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of clk : signal is "XIL_INTERFACENAME clk, ASSOCIATED_RESET rstn, ASSOCIATED_BUSIF portA, FREQ_HZ 200000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN gpn_bundle_block_kernel_clk, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of porta_en : signal is "user.org:interface:local_memory_interface:1.0 portA en";
  attribute X_INTERFACE_INFO of rstn : signal is "xilinx.com:signal:reset:1.0 rstn RST";
  attribute X_INTERFACE_PARAMETER of rstn : signal is "XIL_INTERFACENAME rstn, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of porta_addr : signal is "user.org:interface:local_memory_interface:1.0 portA addr";
  attribute X_INTERFACE_INFO of porta_be : signal is "user.org:interface:local_memory_interface:1.0 portA be";
  attribute X_INTERFACE_INFO of porta_data_in : signal is "user.org:interface:local_memory_interface:1.0 portA data_in";
  attribute X_INTERFACE_INFO of porta_data_out : signal is "user.org:interface:local_memory_interface:1.0 portA data_out";
  attribute X_INTERFACE_PARAMETER of porta_data_out : signal is "XIL_INTERFACENAME portA, SV_INTERFACE true";
begin
  porta_data_out(31) <= \^porta_data_out\(31);
  porta_data_out(30) <= \^porta_data_out\(28);
  porta_data_out(29 downto 28) <= \^porta_data_out\(29 downto 28);
  porta_data_out(27) <= \^porta_data_out\(28);
  porta_data_out(26) <= \^porta_data_out\(28);
  porta_data_out(25) <= \^porta_data_out\(25);
  porta_data_out(24) <= \<const0>\;
  porta_data_out(23 downto 20) <= \^porta_data_out\(23 downto 20);
  porta_data_out(19) <= \<const0>\;
  porta_data_out(18 downto 4) <= \^porta_data_out\(18 downto 4);
  porta_data_out(3) <= \<const0>\;
  porta_data_out(2) <= \^porta_data_out\(2);
  porta_data_out(1) <= \^porta_data_out\(0);
  porta_data_out(0) <= \^porta_data_out\(0);
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_local_rom
     port map (
      clk => clk,
      porta_addr(4 downto 0) => porta_addr(4 downto 0),
      porta_data_out(24) => \^porta_data_out\(31),
      porta_data_out(23) => \^porta_data_out\(28),
      porta_data_out(22) => \^porta_data_out\(29),
      porta_data_out(21) => \^porta_data_out\(25),
      porta_data_out(20 downto 17) => \^porta_data_out\(23 downto 20),
      porta_data_out(16 downto 2) => \^porta_data_out\(18 downto 4),
      porta_data_out(1) => \^porta_data_out\(2),
      porta_data_out(0) => \^porta_data_out\(0),
      porta_en => porta_en
    );
end STRUCTURE;
