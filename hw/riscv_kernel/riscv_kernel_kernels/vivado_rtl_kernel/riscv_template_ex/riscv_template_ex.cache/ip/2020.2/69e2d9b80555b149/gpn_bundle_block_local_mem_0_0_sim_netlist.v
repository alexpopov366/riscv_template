// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Mon Aug 29 21:30:23 2022
// Host        : dl580 running 64-bit Ubuntu 18.04.6 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ gpn_bundle_block_local_mem_0_0_sim_netlist.v
// Design      : gpn_bundle_block_local_mem_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcu200-fsgd2104-2-e
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_byte_en_BRAM
   (portA_data_out,
    portB_data_out,
    clk,
    portA_addr,
    portB_addr,
    portA_data_in,
    portB_data_in,
    portA_en,
    portB_en,
    portA_be,
    portB_be);
  output [31:0]portA_data_out;
  output [31:0]portB_data_out;
  input clk;
  input [13:0]portA_addr;
  input [13:0]portB_addr;
  input [31:0]portA_data_in;
  input [31:0]portB_data_in;
  input portA_en;
  input portB_en;
  input [3:0]portA_be;
  input [3:0]portB_be;

  wire clk;
  wire [13:0]portA_addr;
  wire [3:0]portA_be;
  wire [31:0]portA_data_in;
  wire [31:0]portA_data_out;
  wire portA_en;
  wire [13:0]portB_addr;
  wire [3:0]portB_be;
  wire [31:0]portB_data_in;
  wire [31:0]portB_data_out;
  wire portB_en;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xilinx_byte_enable_ram ram_block
       (.clk(clk),
        .portA_addr(portA_addr),
        .portA_be(portA_be),
        .portA_data_in(portA_data_in),
        .portA_data_out(portA_data_out),
        .portA_en(portA_en),
        .portB_addr(portB_addr),
        .portB_be(portB_be),
        .portB_data_in(portB_data_in),
        .portB_data_out(portB_data_out),
        .portB_en(portB_en));
endmodule

(* CHECK_LICENSE_TYPE = "gpn_bundle_block_local_mem_0_0,local_mem,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* IP_DEFINITION_SOURCE = "package_project" *) 
(* X_CORE_INFO = "local_mem,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clk,
    rstn,
    portA_en,
    portA_be,
    portA_addr,
    portA_data_in,
    portA_data_out,
    portB_en,
    portB_be,
    portB_addr,
    portB_data_in,
    portB_data_out);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME clk, ASSOCIATED_RESET rstn, ASSOCIATED_BUSIF portA:portB, FREQ_HZ 200000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN gpn_bundle_block_kernel_clk, INSERT_VIP 0" *) input clk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 rstn RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME rstn, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input rstn;
  (* X_INTERFACE_INFO = "user.org:interface:local_memory_interface:1.0 portA en" *) input portA_en;
  (* X_INTERFACE_INFO = "user.org:interface:local_memory_interface:1.0 portA be" *) input [3:0]portA_be;
  (* X_INTERFACE_INFO = "user.org:interface:local_memory_interface:1.0 portA addr" *) input [29:0]portA_addr;
  (* X_INTERFACE_INFO = "user.org:interface:local_memory_interface:1.0 portA data_in" *) input [31:0]portA_data_in;
  (* X_INTERFACE_INFO = "user.org:interface:local_memory_interface:1.0 portA data_out" *) output [31:0]portA_data_out;
  (* X_INTERFACE_INFO = "user.org:interface:local_memory_interface:1.0 portB en" *) input portB_en;
  (* X_INTERFACE_INFO = "user.org:interface:local_memory_interface:1.0 portB be" *) input [3:0]portB_be;
  (* X_INTERFACE_INFO = "user.org:interface:local_memory_interface:1.0 portB addr" *) input [29:0]portB_addr;
  (* X_INTERFACE_INFO = "user.org:interface:local_memory_interface:1.0 portB data_in" *) input [31:0]portB_data_in;
  (* X_INTERFACE_INFO = "user.org:interface:local_memory_interface:1.0 portB data_out" *) output [31:0]portB_data_out;

  wire clk;
  wire [29:0]portA_addr;
  wire [3:0]portA_be;
  wire [31:0]portA_data_in;
  wire [31:0]portA_data_out;
  wire portA_en;
  wire [29:0]portB_addr;
  wire [3:0]portB_be;
  wire [31:0]portB_data_in;
  wire [31:0]portB_data_out;
  wire portB_en;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_local_mem inst
       (.clk(clk),
        .portA_addr(portA_addr[13:0]),
        .portA_be(portA_be),
        .portA_data_in(portA_data_in),
        .portA_data_out(portA_data_out),
        .portA_en(portA_en),
        .portB_addr(portB_addr[13:0]),
        .portB_be(portB_be),
        .portB_data_in(portB_data_in),
        .portB_data_out(portB_data_out),
        .portB_en(portB_en));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_local_mem
   (portA_data_out,
    portB_data_out,
    clk,
    portA_addr,
    portB_addr,
    portA_data_in,
    portB_data_in,
    portA_en,
    portB_en,
    portA_be,
    portB_be);
  output [31:0]portA_data_out;
  output [31:0]portB_data_out;
  input clk;
  input [13:0]portA_addr;
  input [13:0]portB_addr;
  input [31:0]portA_data_in;
  input [31:0]portB_data_in;
  input portA_en;
  input portB_en;
  input [3:0]portA_be;
  input [3:0]portB_be;

  wire clk;
  wire [13:0]portA_addr;
  wire [3:0]portA_be;
  wire [31:0]portA_data_in;
  wire [31:0]portA_data_out;
  wire portA_en;
  wire [13:0]portB_addr;
  wire [3:0]portB_be;
  wire [31:0]portB_data_in;
  wire [31:0]portB_data_out;
  wire portB_en;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_byte_en_BRAM inst_data_ram
       (.clk(clk),
        .portA_addr(portA_addr),
        .portA_be(portA_be),
        .portA_data_in(portA_data_in),
        .portA_data_out(portA_data_out),
        .portA_en(portA_en),
        .portB_addr(portB_addr),
        .portB_be(portB_be),
        .portB_data_in(portB_data_in),
        .portB_data_out(portB_data_out),
        .portB_en(portB_en));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xilinx_byte_enable_ram
   (portA_data_out,
    portB_data_out,
    clk,
    portA_addr,
    portB_addr,
    portA_data_in,
    portB_data_in,
    portA_en,
    portB_en,
    portA_be,
    portB_be);
  output [31:0]portA_data_out;
  output [31:0]portB_data_out;
  input clk;
  input [13:0]portA_addr;
  input [13:0]portB_addr;
  input [31:0]portA_data_in;
  input [31:0]portB_data_in;
  input portA_en;
  input portB_en;
  input [3:0]portA_be;
  input [3:0]portB_be;

  wire clk;
  wire \genblk2[1].ram_reg_0_bram_0_i_1_n_0 ;
  wire \genblk2[1].ram_reg_0_bram_0_i_2_n_0 ;
  wire \genblk2[1].ram_reg_0_bram_0_i_3_n_0 ;
  wire \genblk2[1].ram_reg_0_bram_0_i_4_n_0 ;
  wire \genblk2[1].ram_reg_0_bram_0_n_0 ;
  wire \genblk2[1].ram_reg_0_bram_0_n_1 ;
  wire \genblk2[1].ram_reg_0_bram_0_n_132 ;
  wire \genblk2[1].ram_reg_0_bram_0_n_133 ;
  wire \genblk2[1].ram_reg_0_bram_0_n_134 ;
  wire \genblk2[1].ram_reg_0_bram_0_n_135 ;
  wire \genblk2[1].ram_reg_0_bram_0_n_136 ;
  wire \genblk2[1].ram_reg_0_bram_0_n_137 ;
  wire \genblk2[1].ram_reg_0_bram_0_n_138 ;
  wire \genblk2[1].ram_reg_0_bram_0_n_139 ;
  wire \genblk2[1].ram_reg_0_bram_0_n_28 ;
  wire \genblk2[1].ram_reg_0_bram_0_n_29 ;
  wire \genblk2[1].ram_reg_0_bram_0_n_30 ;
  wire \genblk2[1].ram_reg_0_bram_0_n_31 ;
  wire \genblk2[1].ram_reg_0_bram_0_n_32 ;
  wire \genblk2[1].ram_reg_0_bram_0_n_33 ;
  wire \genblk2[1].ram_reg_0_bram_0_n_34 ;
  wire \genblk2[1].ram_reg_0_bram_0_n_35 ;
  wire \genblk2[1].ram_reg_0_bram_0_n_60 ;
  wire \genblk2[1].ram_reg_0_bram_0_n_61 ;
  wire \genblk2[1].ram_reg_0_bram_0_n_62 ;
  wire \genblk2[1].ram_reg_0_bram_0_n_63 ;
  wire \genblk2[1].ram_reg_0_bram_0_n_64 ;
  wire \genblk2[1].ram_reg_0_bram_0_n_65 ;
  wire \genblk2[1].ram_reg_0_bram_0_n_66 ;
  wire \genblk2[1].ram_reg_0_bram_0_n_67 ;
  wire \genblk2[1].ram_reg_0_bram_1_i_1_n_0 ;
  wire \genblk2[1].ram_reg_0_bram_1_i_2_n_0 ;
  wire \genblk2[1].ram_reg_0_bram_1_i_3_n_0 ;
  wire \genblk2[1].ram_reg_0_bram_1_i_4_n_0 ;
  wire \genblk2[1].ram_reg_0_bram_1_i_5_n_0 ;
  wire \genblk2[1].ram_reg_0_bram_1_i_6_n_0 ;
  wire \genblk2[1].ram_reg_0_bram_1_n_0 ;
  wire \genblk2[1].ram_reg_0_bram_1_n_1 ;
  wire \genblk2[1].ram_reg_0_bram_1_n_132 ;
  wire \genblk2[1].ram_reg_0_bram_1_n_133 ;
  wire \genblk2[1].ram_reg_0_bram_1_n_134 ;
  wire \genblk2[1].ram_reg_0_bram_1_n_135 ;
  wire \genblk2[1].ram_reg_0_bram_1_n_136 ;
  wire \genblk2[1].ram_reg_0_bram_1_n_137 ;
  wire \genblk2[1].ram_reg_0_bram_1_n_138 ;
  wire \genblk2[1].ram_reg_0_bram_1_n_139 ;
  wire \genblk2[1].ram_reg_0_bram_1_n_28 ;
  wire \genblk2[1].ram_reg_0_bram_1_n_29 ;
  wire \genblk2[1].ram_reg_0_bram_1_n_30 ;
  wire \genblk2[1].ram_reg_0_bram_1_n_31 ;
  wire \genblk2[1].ram_reg_0_bram_1_n_32 ;
  wire \genblk2[1].ram_reg_0_bram_1_n_33 ;
  wire \genblk2[1].ram_reg_0_bram_1_n_34 ;
  wire \genblk2[1].ram_reg_0_bram_1_n_35 ;
  wire \genblk2[1].ram_reg_0_bram_1_n_60 ;
  wire \genblk2[1].ram_reg_0_bram_1_n_61 ;
  wire \genblk2[1].ram_reg_0_bram_1_n_62 ;
  wire \genblk2[1].ram_reg_0_bram_1_n_63 ;
  wire \genblk2[1].ram_reg_0_bram_1_n_64 ;
  wire \genblk2[1].ram_reg_0_bram_1_n_65 ;
  wire \genblk2[1].ram_reg_0_bram_1_n_66 ;
  wire \genblk2[1].ram_reg_0_bram_1_n_67 ;
  wire \genblk2[1].ram_reg_0_bram_2_i_1_n_0 ;
  wire \genblk2[1].ram_reg_0_bram_2_i_2_n_0 ;
  wire \genblk2[1].ram_reg_0_bram_2_i_3_n_0 ;
  wire \genblk2[1].ram_reg_0_bram_2_i_4_n_0 ;
  wire \genblk2[1].ram_reg_0_bram_2_i_5_n_0 ;
  wire \genblk2[1].ram_reg_0_bram_2_i_6_n_0 ;
  wire \genblk2[1].ram_reg_0_bram_2_n_0 ;
  wire \genblk2[1].ram_reg_0_bram_2_n_1 ;
  wire \genblk2[1].ram_reg_0_bram_2_n_132 ;
  wire \genblk2[1].ram_reg_0_bram_2_n_133 ;
  wire \genblk2[1].ram_reg_0_bram_2_n_134 ;
  wire \genblk2[1].ram_reg_0_bram_2_n_135 ;
  wire \genblk2[1].ram_reg_0_bram_2_n_136 ;
  wire \genblk2[1].ram_reg_0_bram_2_n_137 ;
  wire \genblk2[1].ram_reg_0_bram_2_n_138 ;
  wire \genblk2[1].ram_reg_0_bram_2_n_139 ;
  wire \genblk2[1].ram_reg_0_bram_2_n_28 ;
  wire \genblk2[1].ram_reg_0_bram_2_n_29 ;
  wire \genblk2[1].ram_reg_0_bram_2_n_30 ;
  wire \genblk2[1].ram_reg_0_bram_2_n_31 ;
  wire \genblk2[1].ram_reg_0_bram_2_n_32 ;
  wire \genblk2[1].ram_reg_0_bram_2_n_33 ;
  wire \genblk2[1].ram_reg_0_bram_2_n_34 ;
  wire \genblk2[1].ram_reg_0_bram_2_n_35 ;
  wire \genblk2[1].ram_reg_0_bram_2_n_60 ;
  wire \genblk2[1].ram_reg_0_bram_2_n_61 ;
  wire \genblk2[1].ram_reg_0_bram_2_n_62 ;
  wire \genblk2[1].ram_reg_0_bram_2_n_63 ;
  wire \genblk2[1].ram_reg_0_bram_2_n_64 ;
  wire \genblk2[1].ram_reg_0_bram_2_n_65 ;
  wire \genblk2[1].ram_reg_0_bram_2_n_66 ;
  wire \genblk2[1].ram_reg_0_bram_2_n_67 ;
  wire \genblk2[1].ram_reg_0_bram_3_i_1_n_0 ;
  wire \genblk2[1].ram_reg_0_bram_3_i_2_n_0 ;
  wire \genblk2[1].ram_reg_1_bram_0_i_1_n_0 ;
  wire \genblk2[1].ram_reg_1_bram_0_i_2_n_0 ;
  wire \genblk2[1].ram_reg_1_bram_0_n_0 ;
  wire \genblk2[1].ram_reg_1_bram_0_n_1 ;
  wire \genblk2[1].ram_reg_1_bram_0_n_132 ;
  wire \genblk2[1].ram_reg_1_bram_0_n_133 ;
  wire \genblk2[1].ram_reg_1_bram_0_n_134 ;
  wire \genblk2[1].ram_reg_1_bram_0_n_135 ;
  wire \genblk2[1].ram_reg_1_bram_0_n_136 ;
  wire \genblk2[1].ram_reg_1_bram_0_n_137 ;
  wire \genblk2[1].ram_reg_1_bram_0_n_138 ;
  wire \genblk2[1].ram_reg_1_bram_0_n_139 ;
  wire \genblk2[1].ram_reg_1_bram_0_n_28 ;
  wire \genblk2[1].ram_reg_1_bram_0_n_29 ;
  wire \genblk2[1].ram_reg_1_bram_0_n_30 ;
  wire \genblk2[1].ram_reg_1_bram_0_n_31 ;
  wire \genblk2[1].ram_reg_1_bram_0_n_32 ;
  wire \genblk2[1].ram_reg_1_bram_0_n_33 ;
  wire \genblk2[1].ram_reg_1_bram_0_n_34 ;
  wire \genblk2[1].ram_reg_1_bram_0_n_35 ;
  wire \genblk2[1].ram_reg_1_bram_0_n_60 ;
  wire \genblk2[1].ram_reg_1_bram_0_n_61 ;
  wire \genblk2[1].ram_reg_1_bram_0_n_62 ;
  wire \genblk2[1].ram_reg_1_bram_0_n_63 ;
  wire \genblk2[1].ram_reg_1_bram_0_n_64 ;
  wire \genblk2[1].ram_reg_1_bram_0_n_65 ;
  wire \genblk2[1].ram_reg_1_bram_0_n_66 ;
  wire \genblk2[1].ram_reg_1_bram_0_n_67 ;
  wire \genblk2[1].ram_reg_1_bram_1_i_1_n_0 ;
  wire \genblk2[1].ram_reg_1_bram_1_i_2_n_0 ;
  wire \genblk2[1].ram_reg_1_bram_1_n_0 ;
  wire \genblk2[1].ram_reg_1_bram_1_n_1 ;
  wire \genblk2[1].ram_reg_1_bram_1_n_132 ;
  wire \genblk2[1].ram_reg_1_bram_1_n_133 ;
  wire \genblk2[1].ram_reg_1_bram_1_n_134 ;
  wire \genblk2[1].ram_reg_1_bram_1_n_135 ;
  wire \genblk2[1].ram_reg_1_bram_1_n_136 ;
  wire \genblk2[1].ram_reg_1_bram_1_n_137 ;
  wire \genblk2[1].ram_reg_1_bram_1_n_138 ;
  wire \genblk2[1].ram_reg_1_bram_1_n_139 ;
  wire \genblk2[1].ram_reg_1_bram_1_n_28 ;
  wire \genblk2[1].ram_reg_1_bram_1_n_29 ;
  wire \genblk2[1].ram_reg_1_bram_1_n_30 ;
  wire \genblk2[1].ram_reg_1_bram_1_n_31 ;
  wire \genblk2[1].ram_reg_1_bram_1_n_32 ;
  wire \genblk2[1].ram_reg_1_bram_1_n_33 ;
  wire \genblk2[1].ram_reg_1_bram_1_n_34 ;
  wire \genblk2[1].ram_reg_1_bram_1_n_35 ;
  wire \genblk2[1].ram_reg_1_bram_1_n_60 ;
  wire \genblk2[1].ram_reg_1_bram_1_n_61 ;
  wire \genblk2[1].ram_reg_1_bram_1_n_62 ;
  wire \genblk2[1].ram_reg_1_bram_1_n_63 ;
  wire \genblk2[1].ram_reg_1_bram_1_n_64 ;
  wire \genblk2[1].ram_reg_1_bram_1_n_65 ;
  wire \genblk2[1].ram_reg_1_bram_1_n_66 ;
  wire \genblk2[1].ram_reg_1_bram_1_n_67 ;
  wire \genblk2[1].ram_reg_1_bram_2_i_1_n_0 ;
  wire \genblk2[1].ram_reg_1_bram_2_i_2_n_0 ;
  wire \genblk2[1].ram_reg_1_bram_2_n_0 ;
  wire \genblk2[1].ram_reg_1_bram_2_n_1 ;
  wire \genblk2[1].ram_reg_1_bram_2_n_132 ;
  wire \genblk2[1].ram_reg_1_bram_2_n_133 ;
  wire \genblk2[1].ram_reg_1_bram_2_n_134 ;
  wire \genblk2[1].ram_reg_1_bram_2_n_135 ;
  wire \genblk2[1].ram_reg_1_bram_2_n_136 ;
  wire \genblk2[1].ram_reg_1_bram_2_n_137 ;
  wire \genblk2[1].ram_reg_1_bram_2_n_138 ;
  wire \genblk2[1].ram_reg_1_bram_2_n_139 ;
  wire \genblk2[1].ram_reg_1_bram_2_n_28 ;
  wire \genblk2[1].ram_reg_1_bram_2_n_29 ;
  wire \genblk2[1].ram_reg_1_bram_2_n_30 ;
  wire \genblk2[1].ram_reg_1_bram_2_n_31 ;
  wire \genblk2[1].ram_reg_1_bram_2_n_32 ;
  wire \genblk2[1].ram_reg_1_bram_2_n_33 ;
  wire \genblk2[1].ram_reg_1_bram_2_n_34 ;
  wire \genblk2[1].ram_reg_1_bram_2_n_35 ;
  wire \genblk2[1].ram_reg_1_bram_2_n_60 ;
  wire \genblk2[1].ram_reg_1_bram_2_n_61 ;
  wire \genblk2[1].ram_reg_1_bram_2_n_62 ;
  wire \genblk2[1].ram_reg_1_bram_2_n_63 ;
  wire \genblk2[1].ram_reg_1_bram_2_n_64 ;
  wire \genblk2[1].ram_reg_1_bram_2_n_65 ;
  wire \genblk2[1].ram_reg_1_bram_2_n_66 ;
  wire \genblk2[1].ram_reg_1_bram_2_n_67 ;
  wire \genblk2[1].ram_reg_1_bram_3_i_1_n_0 ;
  wire \genblk2[1].ram_reg_1_bram_3_i_2_n_0 ;
  wire \genblk2[1].ram_reg_2_bram_0_i_1_n_0 ;
  wire \genblk2[1].ram_reg_2_bram_0_i_2_n_0 ;
  wire \genblk2[1].ram_reg_2_bram_0_n_0 ;
  wire \genblk2[1].ram_reg_2_bram_0_n_1 ;
  wire \genblk2[1].ram_reg_2_bram_0_n_132 ;
  wire \genblk2[1].ram_reg_2_bram_0_n_133 ;
  wire \genblk2[1].ram_reg_2_bram_0_n_134 ;
  wire \genblk2[1].ram_reg_2_bram_0_n_135 ;
  wire \genblk2[1].ram_reg_2_bram_0_n_136 ;
  wire \genblk2[1].ram_reg_2_bram_0_n_137 ;
  wire \genblk2[1].ram_reg_2_bram_0_n_138 ;
  wire \genblk2[1].ram_reg_2_bram_0_n_139 ;
  wire \genblk2[1].ram_reg_2_bram_0_n_28 ;
  wire \genblk2[1].ram_reg_2_bram_0_n_29 ;
  wire \genblk2[1].ram_reg_2_bram_0_n_30 ;
  wire \genblk2[1].ram_reg_2_bram_0_n_31 ;
  wire \genblk2[1].ram_reg_2_bram_0_n_32 ;
  wire \genblk2[1].ram_reg_2_bram_0_n_33 ;
  wire \genblk2[1].ram_reg_2_bram_0_n_34 ;
  wire \genblk2[1].ram_reg_2_bram_0_n_35 ;
  wire \genblk2[1].ram_reg_2_bram_0_n_60 ;
  wire \genblk2[1].ram_reg_2_bram_0_n_61 ;
  wire \genblk2[1].ram_reg_2_bram_0_n_62 ;
  wire \genblk2[1].ram_reg_2_bram_0_n_63 ;
  wire \genblk2[1].ram_reg_2_bram_0_n_64 ;
  wire \genblk2[1].ram_reg_2_bram_0_n_65 ;
  wire \genblk2[1].ram_reg_2_bram_0_n_66 ;
  wire \genblk2[1].ram_reg_2_bram_0_n_67 ;
  wire \genblk2[1].ram_reg_2_bram_1_i_1_n_0 ;
  wire \genblk2[1].ram_reg_2_bram_1_i_2_n_0 ;
  wire \genblk2[1].ram_reg_2_bram_1_n_0 ;
  wire \genblk2[1].ram_reg_2_bram_1_n_1 ;
  wire \genblk2[1].ram_reg_2_bram_1_n_132 ;
  wire \genblk2[1].ram_reg_2_bram_1_n_133 ;
  wire \genblk2[1].ram_reg_2_bram_1_n_134 ;
  wire \genblk2[1].ram_reg_2_bram_1_n_135 ;
  wire \genblk2[1].ram_reg_2_bram_1_n_136 ;
  wire \genblk2[1].ram_reg_2_bram_1_n_137 ;
  wire \genblk2[1].ram_reg_2_bram_1_n_138 ;
  wire \genblk2[1].ram_reg_2_bram_1_n_139 ;
  wire \genblk2[1].ram_reg_2_bram_1_n_28 ;
  wire \genblk2[1].ram_reg_2_bram_1_n_29 ;
  wire \genblk2[1].ram_reg_2_bram_1_n_30 ;
  wire \genblk2[1].ram_reg_2_bram_1_n_31 ;
  wire \genblk2[1].ram_reg_2_bram_1_n_32 ;
  wire \genblk2[1].ram_reg_2_bram_1_n_33 ;
  wire \genblk2[1].ram_reg_2_bram_1_n_34 ;
  wire \genblk2[1].ram_reg_2_bram_1_n_35 ;
  wire \genblk2[1].ram_reg_2_bram_1_n_60 ;
  wire \genblk2[1].ram_reg_2_bram_1_n_61 ;
  wire \genblk2[1].ram_reg_2_bram_1_n_62 ;
  wire \genblk2[1].ram_reg_2_bram_1_n_63 ;
  wire \genblk2[1].ram_reg_2_bram_1_n_64 ;
  wire \genblk2[1].ram_reg_2_bram_1_n_65 ;
  wire \genblk2[1].ram_reg_2_bram_1_n_66 ;
  wire \genblk2[1].ram_reg_2_bram_1_n_67 ;
  wire \genblk2[1].ram_reg_2_bram_2_i_1_n_0 ;
  wire \genblk2[1].ram_reg_2_bram_2_i_2_n_0 ;
  wire \genblk2[1].ram_reg_2_bram_2_n_0 ;
  wire \genblk2[1].ram_reg_2_bram_2_n_1 ;
  wire \genblk2[1].ram_reg_2_bram_2_n_132 ;
  wire \genblk2[1].ram_reg_2_bram_2_n_133 ;
  wire \genblk2[1].ram_reg_2_bram_2_n_134 ;
  wire \genblk2[1].ram_reg_2_bram_2_n_135 ;
  wire \genblk2[1].ram_reg_2_bram_2_n_136 ;
  wire \genblk2[1].ram_reg_2_bram_2_n_137 ;
  wire \genblk2[1].ram_reg_2_bram_2_n_138 ;
  wire \genblk2[1].ram_reg_2_bram_2_n_139 ;
  wire \genblk2[1].ram_reg_2_bram_2_n_28 ;
  wire \genblk2[1].ram_reg_2_bram_2_n_29 ;
  wire \genblk2[1].ram_reg_2_bram_2_n_30 ;
  wire \genblk2[1].ram_reg_2_bram_2_n_31 ;
  wire \genblk2[1].ram_reg_2_bram_2_n_32 ;
  wire \genblk2[1].ram_reg_2_bram_2_n_33 ;
  wire \genblk2[1].ram_reg_2_bram_2_n_34 ;
  wire \genblk2[1].ram_reg_2_bram_2_n_35 ;
  wire \genblk2[1].ram_reg_2_bram_2_n_60 ;
  wire \genblk2[1].ram_reg_2_bram_2_n_61 ;
  wire \genblk2[1].ram_reg_2_bram_2_n_62 ;
  wire \genblk2[1].ram_reg_2_bram_2_n_63 ;
  wire \genblk2[1].ram_reg_2_bram_2_n_64 ;
  wire \genblk2[1].ram_reg_2_bram_2_n_65 ;
  wire \genblk2[1].ram_reg_2_bram_2_n_66 ;
  wire \genblk2[1].ram_reg_2_bram_2_n_67 ;
  wire \genblk2[1].ram_reg_2_bram_3_i_17_n_0 ;
  wire \genblk2[1].ram_reg_2_bram_3_i_18_n_0 ;
  wire \genblk2[1].ram_reg_3_bram_0_i_1_n_0 ;
  wire \genblk2[1].ram_reg_3_bram_0_i_2_n_0 ;
  wire \genblk2[1].ram_reg_3_bram_0_n_0 ;
  wire \genblk2[1].ram_reg_3_bram_0_n_1 ;
  wire \genblk2[1].ram_reg_3_bram_0_n_132 ;
  wire \genblk2[1].ram_reg_3_bram_0_n_133 ;
  wire \genblk2[1].ram_reg_3_bram_0_n_134 ;
  wire \genblk2[1].ram_reg_3_bram_0_n_135 ;
  wire \genblk2[1].ram_reg_3_bram_0_n_136 ;
  wire \genblk2[1].ram_reg_3_bram_0_n_137 ;
  wire \genblk2[1].ram_reg_3_bram_0_n_138 ;
  wire \genblk2[1].ram_reg_3_bram_0_n_139 ;
  wire \genblk2[1].ram_reg_3_bram_0_n_28 ;
  wire \genblk2[1].ram_reg_3_bram_0_n_29 ;
  wire \genblk2[1].ram_reg_3_bram_0_n_30 ;
  wire \genblk2[1].ram_reg_3_bram_0_n_31 ;
  wire \genblk2[1].ram_reg_3_bram_0_n_32 ;
  wire \genblk2[1].ram_reg_3_bram_0_n_33 ;
  wire \genblk2[1].ram_reg_3_bram_0_n_34 ;
  wire \genblk2[1].ram_reg_3_bram_0_n_35 ;
  wire \genblk2[1].ram_reg_3_bram_0_n_60 ;
  wire \genblk2[1].ram_reg_3_bram_0_n_61 ;
  wire \genblk2[1].ram_reg_3_bram_0_n_62 ;
  wire \genblk2[1].ram_reg_3_bram_0_n_63 ;
  wire \genblk2[1].ram_reg_3_bram_0_n_64 ;
  wire \genblk2[1].ram_reg_3_bram_0_n_65 ;
  wire \genblk2[1].ram_reg_3_bram_0_n_66 ;
  wire \genblk2[1].ram_reg_3_bram_0_n_67 ;
  wire \genblk2[1].ram_reg_3_bram_1_i_1_n_0 ;
  wire \genblk2[1].ram_reg_3_bram_1_i_2_n_0 ;
  wire \genblk2[1].ram_reg_3_bram_1_n_0 ;
  wire \genblk2[1].ram_reg_3_bram_1_n_1 ;
  wire \genblk2[1].ram_reg_3_bram_1_n_132 ;
  wire \genblk2[1].ram_reg_3_bram_1_n_133 ;
  wire \genblk2[1].ram_reg_3_bram_1_n_134 ;
  wire \genblk2[1].ram_reg_3_bram_1_n_135 ;
  wire \genblk2[1].ram_reg_3_bram_1_n_136 ;
  wire \genblk2[1].ram_reg_3_bram_1_n_137 ;
  wire \genblk2[1].ram_reg_3_bram_1_n_138 ;
  wire \genblk2[1].ram_reg_3_bram_1_n_139 ;
  wire \genblk2[1].ram_reg_3_bram_1_n_28 ;
  wire \genblk2[1].ram_reg_3_bram_1_n_29 ;
  wire \genblk2[1].ram_reg_3_bram_1_n_30 ;
  wire \genblk2[1].ram_reg_3_bram_1_n_31 ;
  wire \genblk2[1].ram_reg_3_bram_1_n_32 ;
  wire \genblk2[1].ram_reg_3_bram_1_n_33 ;
  wire \genblk2[1].ram_reg_3_bram_1_n_34 ;
  wire \genblk2[1].ram_reg_3_bram_1_n_35 ;
  wire \genblk2[1].ram_reg_3_bram_1_n_60 ;
  wire \genblk2[1].ram_reg_3_bram_1_n_61 ;
  wire \genblk2[1].ram_reg_3_bram_1_n_62 ;
  wire \genblk2[1].ram_reg_3_bram_1_n_63 ;
  wire \genblk2[1].ram_reg_3_bram_1_n_64 ;
  wire \genblk2[1].ram_reg_3_bram_1_n_65 ;
  wire \genblk2[1].ram_reg_3_bram_1_n_66 ;
  wire \genblk2[1].ram_reg_3_bram_1_n_67 ;
  wire \genblk2[1].ram_reg_3_bram_2_i_1_n_0 ;
  wire \genblk2[1].ram_reg_3_bram_2_i_2_n_0 ;
  wire \genblk2[1].ram_reg_3_bram_2_n_0 ;
  wire \genblk2[1].ram_reg_3_bram_2_n_1 ;
  wire \genblk2[1].ram_reg_3_bram_2_n_132 ;
  wire \genblk2[1].ram_reg_3_bram_2_n_133 ;
  wire \genblk2[1].ram_reg_3_bram_2_n_134 ;
  wire \genblk2[1].ram_reg_3_bram_2_n_135 ;
  wire \genblk2[1].ram_reg_3_bram_2_n_136 ;
  wire \genblk2[1].ram_reg_3_bram_2_n_137 ;
  wire \genblk2[1].ram_reg_3_bram_2_n_138 ;
  wire \genblk2[1].ram_reg_3_bram_2_n_139 ;
  wire \genblk2[1].ram_reg_3_bram_2_n_28 ;
  wire \genblk2[1].ram_reg_3_bram_2_n_29 ;
  wire \genblk2[1].ram_reg_3_bram_2_n_30 ;
  wire \genblk2[1].ram_reg_3_bram_2_n_31 ;
  wire \genblk2[1].ram_reg_3_bram_2_n_32 ;
  wire \genblk2[1].ram_reg_3_bram_2_n_33 ;
  wire \genblk2[1].ram_reg_3_bram_2_n_34 ;
  wire \genblk2[1].ram_reg_3_bram_2_n_35 ;
  wire \genblk2[1].ram_reg_3_bram_2_n_60 ;
  wire \genblk2[1].ram_reg_3_bram_2_n_61 ;
  wire \genblk2[1].ram_reg_3_bram_2_n_62 ;
  wire \genblk2[1].ram_reg_3_bram_2_n_63 ;
  wire \genblk2[1].ram_reg_3_bram_2_n_64 ;
  wire \genblk2[1].ram_reg_3_bram_2_n_65 ;
  wire \genblk2[1].ram_reg_3_bram_2_n_66 ;
  wire \genblk2[1].ram_reg_3_bram_2_n_67 ;
  wire \genblk2[1].ram_reg_3_bram_3_i_1_n_0 ;
  wire \genblk2[1].ram_reg_3_bram_3_i_21_n_0 ;
  wire \genblk2[1].ram_reg_3_bram_3_i_22_n_0 ;
  wire \genblk2[1].ram_reg_3_bram_3_i_2_n_0 ;
  wire \genblk2[1].ram_reg_3_bram_3_i_3_n_0 ;
  wire \genblk2[1].ram_reg_3_bram_3_i_4_n_0 ;
  wire [31:16]p_1_in;
  wire [31:16]p_2_in;
  wire [13:0]portA_addr;
  wire [3:0]portA_be;
  wire [31:0]portA_data_in;
  wire [31:0]portA_data_out;
  wire portA_en;
  wire [13:0]portB_addr;
  wire [3:0]portB_be;
  wire [31:0]portB_data_in;
  wire [31:0]portB_data_out;
  wire portB_en;
  wire \NLW_genblk2[1].ram_reg_0_bram_0_DBITERR_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_0_bram_0_SBITERR_UNCONNECTED ;
  wire [31:8]\NLW_genblk2[1].ram_reg_0_bram_0_CASDOUTA_UNCONNECTED ;
  wire [31:8]\NLW_genblk2[1].ram_reg_0_bram_0_CASDOUTB_UNCONNECTED ;
  wire [31:0]\NLW_genblk2[1].ram_reg_0_bram_0_DOUTADOUT_UNCONNECTED ;
  wire [31:0]\NLW_genblk2[1].ram_reg_0_bram_0_DOUTBDOUT_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_0_bram_0_DOUTPADOUTP_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_0_bram_0_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_genblk2[1].ram_reg_0_bram_0_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_genblk2[1].ram_reg_0_bram_0_RDADDRECC_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_0_bram_1_DBITERR_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_0_bram_1_SBITERR_UNCONNECTED ;
  wire [31:8]\NLW_genblk2[1].ram_reg_0_bram_1_CASDOUTA_UNCONNECTED ;
  wire [31:8]\NLW_genblk2[1].ram_reg_0_bram_1_CASDOUTB_UNCONNECTED ;
  wire [31:0]\NLW_genblk2[1].ram_reg_0_bram_1_DOUTADOUT_UNCONNECTED ;
  wire [31:0]\NLW_genblk2[1].ram_reg_0_bram_1_DOUTBDOUT_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_0_bram_1_DOUTPADOUTP_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_0_bram_1_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_genblk2[1].ram_reg_0_bram_1_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_genblk2[1].ram_reg_0_bram_1_RDADDRECC_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_0_bram_2_DBITERR_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_0_bram_2_SBITERR_UNCONNECTED ;
  wire [31:8]\NLW_genblk2[1].ram_reg_0_bram_2_CASDOUTA_UNCONNECTED ;
  wire [31:8]\NLW_genblk2[1].ram_reg_0_bram_2_CASDOUTB_UNCONNECTED ;
  wire [31:0]\NLW_genblk2[1].ram_reg_0_bram_2_DOUTADOUT_UNCONNECTED ;
  wire [31:0]\NLW_genblk2[1].ram_reg_0_bram_2_DOUTBDOUT_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_0_bram_2_DOUTPADOUTP_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_0_bram_2_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_genblk2[1].ram_reg_0_bram_2_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_genblk2[1].ram_reg_0_bram_2_RDADDRECC_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_0_bram_3_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_0_bram_3_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_0_bram_3_DBITERR_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_0_bram_3_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_genblk2[1].ram_reg_0_bram_3_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_genblk2[1].ram_reg_0_bram_3_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_0_bram_3_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_0_bram_3_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_genblk2[1].ram_reg_0_bram_3_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_genblk2[1].ram_reg_0_bram_3_DOUTBDOUT_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_0_bram_3_DOUTPADOUTP_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_0_bram_3_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_genblk2[1].ram_reg_0_bram_3_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_genblk2[1].ram_reg_0_bram_3_RDADDRECC_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_1_bram_0_DBITERR_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_1_bram_0_SBITERR_UNCONNECTED ;
  wire [31:8]\NLW_genblk2[1].ram_reg_1_bram_0_CASDOUTA_UNCONNECTED ;
  wire [31:8]\NLW_genblk2[1].ram_reg_1_bram_0_CASDOUTB_UNCONNECTED ;
  wire [31:0]\NLW_genblk2[1].ram_reg_1_bram_0_DOUTADOUT_UNCONNECTED ;
  wire [31:0]\NLW_genblk2[1].ram_reg_1_bram_0_DOUTBDOUT_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_1_bram_0_DOUTPADOUTP_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_1_bram_0_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_genblk2[1].ram_reg_1_bram_0_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_genblk2[1].ram_reg_1_bram_0_RDADDRECC_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_1_bram_1_DBITERR_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_1_bram_1_SBITERR_UNCONNECTED ;
  wire [31:8]\NLW_genblk2[1].ram_reg_1_bram_1_CASDOUTA_UNCONNECTED ;
  wire [31:8]\NLW_genblk2[1].ram_reg_1_bram_1_CASDOUTB_UNCONNECTED ;
  wire [31:0]\NLW_genblk2[1].ram_reg_1_bram_1_DOUTADOUT_UNCONNECTED ;
  wire [31:0]\NLW_genblk2[1].ram_reg_1_bram_1_DOUTBDOUT_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_1_bram_1_DOUTPADOUTP_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_1_bram_1_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_genblk2[1].ram_reg_1_bram_1_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_genblk2[1].ram_reg_1_bram_1_RDADDRECC_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_1_bram_2_DBITERR_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_1_bram_2_SBITERR_UNCONNECTED ;
  wire [31:8]\NLW_genblk2[1].ram_reg_1_bram_2_CASDOUTA_UNCONNECTED ;
  wire [31:8]\NLW_genblk2[1].ram_reg_1_bram_2_CASDOUTB_UNCONNECTED ;
  wire [31:0]\NLW_genblk2[1].ram_reg_1_bram_2_DOUTADOUT_UNCONNECTED ;
  wire [31:0]\NLW_genblk2[1].ram_reg_1_bram_2_DOUTBDOUT_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_1_bram_2_DOUTPADOUTP_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_1_bram_2_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_genblk2[1].ram_reg_1_bram_2_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_genblk2[1].ram_reg_1_bram_2_RDADDRECC_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_1_bram_3_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_1_bram_3_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_1_bram_3_DBITERR_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_1_bram_3_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_genblk2[1].ram_reg_1_bram_3_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_genblk2[1].ram_reg_1_bram_3_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_1_bram_3_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_1_bram_3_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_genblk2[1].ram_reg_1_bram_3_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_genblk2[1].ram_reg_1_bram_3_DOUTBDOUT_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_1_bram_3_DOUTPADOUTP_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_1_bram_3_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_genblk2[1].ram_reg_1_bram_3_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_genblk2[1].ram_reg_1_bram_3_RDADDRECC_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_2_bram_0_DBITERR_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_2_bram_0_SBITERR_UNCONNECTED ;
  wire [31:8]\NLW_genblk2[1].ram_reg_2_bram_0_CASDOUTA_UNCONNECTED ;
  wire [31:8]\NLW_genblk2[1].ram_reg_2_bram_0_CASDOUTB_UNCONNECTED ;
  wire [31:0]\NLW_genblk2[1].ram_reg_2_bram_0_DOUTADOUT_UNCONNECTED ;
  wire [31:0]\NLW_genblk2[1].ram_reg_2_bram_0_DOUTBDOUT_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_2_bram_0_DOUTPADOUTP_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_2_bram_0_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_genblk2[1].ram_reg_2_bram_0_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_genblk2[1].ram_reg_2_bram_0_RDADDRECC_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_2_bram_1_DBITERR_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_2_bram_1_SBITERR_UNCONNECTED ;
  wire [31:8]\NLW_genblk2[1].ram_reg_2_bram_1_CASDOUTA_UNCONNECTED ;
  wire [31:8]\NLW_genblk2[1].ram_reg_2_bram_1_CASDOUTB_UNCONNECTED ;
  wire [31:0]\NLW_genblk2[1].ram_reg_2_bram_1_DOUTADOUT_UNCONNECTED ;
  wire [31:0]\NLW_genblk2[1].ram_reg_2_bram_1_DOUTBDOUT_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_2_bram_1_DOUTPADOUTP_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_2_bram_1_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_genblk2[1].ram_reg_2_bram_1_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_genblk2[1].ram_reg_2_bram_1_RDADDRECC_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_2_bram_2_DBITERR_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_2_bram_2_SBITERR_UNCONNECTED ;
  wire [31:8]\NLW_genblk2[1].ram_reg_2_bram_2_CASDOUTA_UNCONNECTED ;
  wire [31:8]\NLW_genblk2[1].ram_reg_2_bram_2_CASDOUTB_UNCONNECTED ;
  wire [31:0]\NLW_genblk2[1].ram_reg_2_bram_2_DOUTADOUT_UNCONNECTED ;
  wire [31:0]\NLW_genblk2[1].ram_reg_2_bram_2_DOUTBDOUT_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_2_bram_2_DOUTPADOUTP_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_2_bram_2_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_genblk2[1].ram_reg_2_bram_2_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_genblk2[1].ram_reg_2_bram_2_RDADDRECC_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_2_bram_3_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_2_bram_3_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_2_bram_3_DBITERR_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_2_bram_3_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_genblk2[1].ram_reg_2_bram_3_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_genblk2[1].ram_reg_2_bram_3_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_2_bram_3_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_2_bram_3_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_genblk2[1].ram_reg_2_bram_3_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_genblk2[1].ram_reg_2_bram_3_DOUTBDOUT_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_2_bram_3_DOUTPADOUTP_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_2_bram_3_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_genblk2[1].ram_reg_2_bram_3_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_genblk2[1].ram_reg_2_bram_3_RDADDRECC_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_3_bram_0_DBITERR_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_3_bram_0_SBITERR_UNCONNECTED ;
  wire [31:8]\NLW_genblk2[1].ram_reg_3_bram_0_CASDOUTA_UNCONNECTED ;
  wire [31:8]\NLW_genblk2[1].ram_reg_3_bram_0_CASDOUTB_UNCONNECTED ;
  wire [31:0]\NLW_genblk2[1].ram_reg_3_bram_0_DOUTADOUT_UNCONNECTED ;
  wire [31:0]\NLW_genblk2[1].ram_reg_3_bram_0_DOUTBDOUT_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_3_bram_0_DOUTPADOUTP_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_3_bram_0_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_genblk2[1].ram_reg_3_bram_0_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_genblk2[1].ram_reg_3_bram_0_RDADDRECC_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_3_bram_1_DBITERR_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_3_bram_1_SBITERR_UNCONNECTED ;
  wire [31:8]\NLW_genblk2[1].ram_reg_3_bram_1_CASDOUTA_UNCONNECTED ;
  wire [31:8]\NLW_genblk2[1].ram_reg_3_bram_1_CASDOUTB_UNCONNECTED ;
  wire [31:0]\NLW_genblk2[1].ram_reg_3_bram_1_DOUTADOUT_UNCONNECTED ;
  wire [31:0]\NLW_genblk2[1].ram_reg_3_bram_1_DOUTBDOUT_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_3_bram_1_DOUTPADOUTP_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_3_bram_1_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_genblk2[1].ram_reg_3_bram_1_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_genblk2[1].ram_reg_3_bram_1_RDADDRECC_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_3_bram_2_DBITERR_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_3_bram_2_SBITERR_UNCONNECTED ;
  wire [31:8]\NLW_genblk2[1].ram_reg_3_bram_2_CASDOUTA_UNCONNECTED ;
  wire [31:8]\NLW_genblk2[1].ram_reg_3_bram_2_CASDOUTB_UNCONNECTED ;
  wire [31:0]\NLW_genblk2[1].ram_reg_3_bram_2_DOUTADOUT_UNCONNECTED ;
  wire [31:0]\NLW_genblk2[1].ram_reg_3_bram_2_DOUTBDOUT_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_3_bram_2_DOUTPADOUTP_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_3_bram_2_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_genblk2[1].ram_reg_3_bram_2_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_genblk2[1].ram_reg_3_bram_2_RDADDRECC_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_3_bram_3_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_3_bram_3_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_3_bram_3_DBITERR_UNCONNECTED ;
  wire \NLW_genblk2[1].ram_reg_3_bram_3_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_genblk2[1].ram_reg_3_bram_3_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_genblk2[1].ram_reg_3_bram_3_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_3_bram_3_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_3_bram_3_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_genblk2[1].ram_reg_3_bram_3_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_genblk2[1].ram_reg_3_bram_3_DOUTBDOUT_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_3_bram_3_DOUTPADOUTP_UNCONNECTED ;
  wire [3:0]\NLW_genblk2[1].ram_reg_3_bram_3_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_genblk2[1].ram_reg_3_bram_3_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_genblk2[1].ram_reg_3_bram_3_RDADDRECC_UNCONNECTED ;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-15 {cell *THIS*} {string {address width (14) is more than optimal threshold of 12. Implementing using BWWE will require more logic and timing would be suboptimal. Please use attribute ram_decomp = power if BWWE is desired.}}} {SYNTH-7 {cell *THIS*}}" *) 
  (* RDADDR_COLLISION_HWCONFIG = "DELAYED_WRITE" *) 
  (* RTL_RAM_BITS = "524288" *) 
  (* RTL_RAM_NAME = "genblk2[1].ram" *) 
  (* RTL_RAM_TYPE = "RAM_TDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "4095" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "7" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("FIRST"),
    .CASCADE_ORDER_B("FIRST"),
    .CLOCK_DOMAINS("COMMON"),
    .DOA_REG(0),
    .DOB_REG(0),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \genblk2[1].ram_reg_0_bram_0 
       (.ADDRARDADDR({portA_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({portB_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b1),
        .CASDOMUXEN_B(1'b1),
        .CASDOUTA({\NLW_genblk2[1].ram_reg_0_bram_0_CASDOUTA_UNCONNECTED [31:8],\genblk2[1].ram_reg_0_bram_0_n_28 ,\genblk2[1].ram_reg_0_bram_0_n_29 ,\genblk2[1].ram_reg_0_bram_0_n_30 ,\genblk2[1].ram_reg_0_bram_0_n_31 ,\genblk2[1].ram_reg_0_bram_0_n_32 ,\genblk2[1].ram_reg_0_bram_0_n_33 ,\genblk2[1].ram_reg_0_bram_0_n_34 ,\genblk2[1].ram_reg_0_bram_0_n_35 }),
        .CASDOUTB({\NLW_genblk2[1].ram_reg_0_bram_0_CASDOUTB_UNCONNECTED [31:8],\genblk2[1].ram_reg_0_bram_0_n_60 ,\genblk2[1].ram_reg_0_bram_0_n_61 ,\genblk2[1].ram_reg_0_bram_0_n_62 ,\genblk2[1].ram_reg_0_bram_0_n_63 ,\genblk2[1].ram_reg_0_bram_0_n_64 ,\genblk2[1].ram_reg_0_bram_0_n_65 ,\genblk2[1].ram_reg_0_bram_0_n_66 ,\genblk2[1].ram_reg_0_bram_0_n_67 }),
        .CASDOUTPA({\genblk2[1].ram_reg_0_bram_0_n_132 ,\genblk2[1].ram_reg_0_bram_0_n_133 ,\genblk2[1].ram_reg_0_bram_0_n_134 ,\genblk2[1].ram_reg_0_bram_0_n_135 }),
        .CASDOUTPB({\genblk2[1].ram_reg_0_bram_0_n_136 ,\genblk2[1].ram_reg_0_bram_0_n_137 ,\genblk2[1].ram_reg_0_bram_0_n_138 ,\genblk2[1].ram_reg_0_bram_0_n_139 }),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b1),
        .CASOREGIMUXEN_B(1'b1),
        .CASOUTDBITERR(\genblk2[1].ram_reg_0_bram_0_n_0 ),
        .CASOUTSBITERR(\genblk2[1].ram_reg_0_bram_0_n_1 ),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DBITERR(\NLW_genblk2[1].ram_reg_0_bram_0_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,portA_data_in[7:0]}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,portB_data_in[7:0]}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT(\NLW_genblk2[1].ram_reg_0_bram_0_DOUTADOUT_UNCONNECTED [31:0]),
        .DOUTBDOUT(\NLW_genblk2[1].ram_reg_0_bram_0_DOUTBDOUT_UNCONNECTED [31:0]),
        .DOUTPADOUTP(\NLW_genblk2[1].ram_reg_0_bram_0_DOUTPADOUTP_UNCONNECTED [3:0]),
        .DOUTPBDOUTP(\NLW_genblk2[1].ram_reg_0_bram_0_DOUTPBDOUTP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_genblk2[1].ram_reg_0_bram_0_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b1),
        .ENARDEN(\genblk2[1].ram_reg_0_bram_0_i_1_n_0 ),
        .ENBWREN(\genblk2[1].ram_reg_0_bram_0_i_2_n_0 ),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_genblk2[1].ram_reg_0_bram_0_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_genblk2[1].ram_reg_0_bram_0_SBITERR_UNCONNECTED ),
        .SLEEP(1'b0),
        .WEA({\genblk2[1].ram_reg_0_bram_0_i_3_n_0 ,\genblk2[1].ram_reg_0_bram_0_i_3_n_0 ,\genblk2[1].ram_reg_0_bram_0_i_3_n_0 ,\genblk2[1].ram_reg_0_bram_0_i_3_n_0 }),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_0_bram_0_i_4_n_0 ,\genblk2[1].ram_reg_0_bram_0_i_4_n_0 ,\genblk2[1].ram_reg_0_bram_0_i_4_n_0 ,\genblk2[1].ram_reg_0_bram_0_i_4_n_0 }));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \genblk2[1].ram_reg_0_bram_0_i_1 
       (.I0(portA_en),
        .I1(portA_addr[13]),
        .I2(portA_addr[12]),
        .O(\genblk2[1].ram_reg_0_bram_0_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \genblk2[1].ram_reg_0_bram_0_i_2 
       (.I0(portB_en),
        .I1(portB_addr[13]),
        .I2(portB_addr[12]),
        .O(\genblk2[1].ram_reg_0_bram_0_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \genblk2[1].ram_reg_0_bram_0_i_3 
       (.I0(portA_be[0]),
        .I1(portA_addr[13]),
        .I2(portA_addr[12]),
        .O(\genblk2[1].ram_reg_0_bram_0_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \genblk2[1].ram_reg_0_bram_0_i_4 
       (.I0(portB_be[0]),
        .I1(portB_addr[13]),
        .I2(portB_addr[12]),
        .O(\genblk2[1].ram_reg_0_bram_0_i_4_n_0 ));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-15 {cell *THIS*} {string {address width (14) is more than optimal threshold of 12. Implementing using BWWE will require more logic and timing would be suboptimal. Please use attribute ram_decomp = power if BWWE is desired.}}} {SYNTH-7 {cell *THIS*}}" *) 
  (* RDADDR_COLLISION_HWCONFIG = "DELAYED_WRITE" *) 
  (* RTL_RAM_BITS = "524288" *) 
  (* RTL_RAM_NAME = "genblk2[1].ram" *) 
  (* RTL_RAM_TYPE = "RAM_TDP" *) 
  (* ram_addr_begin = "4096" *) 
  (* ram_addr_end = "8191" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "7" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("MIDDLE"),
    .CASCADE_ORDER_B("MIDDLE"),
    .CLOCK_DOMAINS("COMMON"),
    .DOA_REG(0),
    .DOB_REG(0),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \genblk2[1].ram_reg_0_bram_1 
       (.ADDRARDADDR({portA_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({portB_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_0_bram_0_n_28 ,\genblk2[1].ram_reg_0_bram_0_n_29 ,\genblk2[1].ram_reg_0_bram_0_n_30 ,\genblk2[1].ram_reg_0_bram_0_n_31 ,\genblk2[1].ram_reg_0_bram_0_n_32 ,\genblk2[1].ram_reg_0_bram_0_n_33 ,\genblk2[1].ram_reg_0_bram_0_n_34 ,\genblk2[1].ram_reg_0_bram_0_n_35 }),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_0_bram_0_n_60 ,\genblk2[1].ram_reg_0_bram_0_n_61 ,\genblk2[1].ram_reg_0_bram_0_n_62 ,\genblk2[1].ram_reg_0_bram_0_n_63 ,\genblk2[1].ram_reg_0_bram_0_n_64 ,\genblk2[1].ram_reg_0_bram_0_n_65 ,\genblk2[1].ram_reg_0_bram_0_n_66 ,\genblk2[1].ram_reg_0_bram_0_n_67 }),
        .CASDINPA({\genblk2[1].ram_reg_0_bram_0_n_132 ,\genblk2[1].ram_reg_0_bram_0_n_133 ,\genblk2[1].ram_reg_0_bram_0_n_134 ,\genblk2[1].ram_reg_0_bram_0_n_135 }),
        .CASDINPB({\genblk2[1].ram_reg_0_bram_0_n_136 ,\genblk2[1].ram_reg_0_bram_0_n_137 ,\genblk2[1].ram_reg_0_bram_0_n_138 ,\genblk2[1].ram_reg_0_bram_0_n_139 }),
        .CASDOMUXA(\genblk2[1].ram_reg_0_bram_1_i_1_n_0 ),
        .CASDOMUXB(\genblk2[1].ram_reg_0_bram_1_i_2_n_0 ),
        .CASDOMUXEN_A(portA_en),
        .CASDOMUXEN_B(portB_en),
        .CASDOUTA({\NLW_genblk2[1].ram_reg_0_bram_1_CASDOUTA_UNCONNECTED [31:8],\genblk2[1].ram_reg_0_bram_1_n_28 ,\genblk2[1].ram_reg_0_bram_1_n_29 ,\genblk2[1].ram_reg_0_bram_1_n_30 ,\genblk2[1].ram_reg_0_bram_1_n_31 ,\genblk2[1].ram_reg_0_bram_1_n_32 ,\genblk2[1].ram_reg_0_bram_1_n_33 ,\genblk2[1].ram_reg_0_bram_1_n_34 ,\genblk2[1].ram_reg_0_bram_1_n_35 }),
        .CASDOUTB({\NLW_genblk2[1].ram_reg_0_bram_1_CASDOUTB_UNCONNECTED [31:8],\genblk2[1].ram_reg_0_bram_1_n_60 ,\genblk2[1].ram_reg_0_bram_1_n_61 ,\genblk2[1].ram_reg_0_bram_1_n_62 ,\genblk2[1].ram_reg_0_bram_1_n_63 ,\genblk2[1].ram_reg_0_bram_1_n_64 ,\genblk2[1].ram_reg_0_bram_1_n_65 ,\genblk2[1].ram_reg_0_bram_1_n_66 ,\genblk2[1].ram_reg_0_bram_1_n_67 }),
        .CASDOUTPA({\genblk2[1].ram_reg_0_bram_1_n_132 ,\genblk2[1].ram_reg_0_bram_1_n_133 ,\genblk2[1].ram_reg_0_bram_1_n_134 ,\genblk2[1].ram_reg_0_bram_1_n_135 }),
        .CASDOUTPB({\genblk2[1].ram_reg_0_bram_1_n_136 ,\genblk2[1].ram_reg_0_bram_1_n_137 ,\genblk2[1].ram_reg_0_bram_1_n_138 ,\genblk2[1].ram_reg_0_bram_1_n_139 }),
        .CASINDBITERR(\genblk2[1].ram_reg_0_bram_0_n_0 ),
        .CASINSBITERR(\genblk2[1].ram_reg_0_bram_0_n_1 ),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b1),
        .CASOREGIMUXEN_B(1'b1),
        .CASOUTDBITERR(\genblk2[1].ram_reg_0_bram_1_n_0 ),
        .CASOUTSBITERR(\genblk2[1].ram_reg_0_bram_1_n_1 ),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DBITERR(\NLW_genblk2[1].ram_reg_0_bram_1_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,portA_data_in[7:0]}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,portB_data_in[7:0]}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT(\NLW_genblk2[1].ram_reg_0_bram_1_DOUTADOUT_UNCONNECTED [31:0]),
        .DOUTBDOUT(\NLW_genblk2[1].ram_reg_0_bram_1_DOUTBDOUT_UNCONNECTED [31:0]),
        .DOUTPADOUTP(\NLW_genblk2[1].ram_reg_0_bram_1_DOUTPADOUTP_UNCONNECTED [3:0]),
        .DOUTPBDOUTP(\NLW_genblk2[1].ram_reg_0_bram_1_DOUTPBDOUTP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_genblk2[1].ram_reg_0_bram_1_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b1),
        .ENARDEN(\genblk2[1].ram_reg_0_bram_1_i_3_n_0 ),
        .ENBWREN(\genblk2[1].ram_reg_0_bram_1_i_4_n_0 ),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_genblk2[1].ram_reg_0_bram_1_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_genblk2[1].ram_reg_0_bram_1_SBITERR_UNCONNECTED ),
        .SLEEP(1'b0),
        .WEA({\genblk2[1].ram_reg_0_bram_1_i_5_n_0 ,\genblk2[1].ram_reg_0_bram_1_i_5_n_0 ,\genblk2[1].ram_reg_0_bram_1_i_5_n_0 ,\genblk2[1].ram_reg_0_bram_1_i_5_n_0 }),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_0_bram_1_i_6_n_0 ,\genblk2[1].ram_reg_0_bram_1_i_6_n_0 ,\genblk2[1].ram_reg_0_bram_1_i_6_n_0 ,\genblk2[1].ram_reg_0_bram_1_i_6_n_0 }));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \genblk2[1].ram_reg_0_bram_1_i_1 
       (.I0(portA_addr[13]),
        .I1(portA_addr[12]),
        .O(\genblk2[1].ram_reg_0_bram_1_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \genblk2[1].ram_reg_0_bram_1_i_2 
       (.I0(portB_addr[13]),
        .I1(portB_addr[12]),
        .O(\genblk2[1].ram_reg_0_bram_1_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'h20)) 
    \genblk2[1].ram_reg_0_bram_1_i_3 
       (.I0(portA_en),
        .I1(portA_addr[13]),
        .I2(portA_addr[12]),
        .O(\genblk2[1].ram_reg_0_bram_1_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'h20)) 
    \genblk2[1].ram_reg_0_bram_1_i_4 
       (.I0(portB_en),
        .I1(portB_addr[13]),
        .I2(portB_addr[12]),
        .O(\genblk2[1].ram_reg_0_bram_1_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'h20)) 
    \genblk2[1].ram_reg_0_bram_1_i_5 
       (.I0(portA_be[0]),
        .I1(portA_addr[13]),
        .I2(portA_addr[12]),
        .O(\genblk2[1].ram_reg_0_bram_1_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h20)) 
    \genblk2[1].ram_reg_0_bram_1_i_6 
       (.I0(portB_be[0]),
        .I1(portB_addr[13]),
        .I2(portB_addr[12]),
        .O(\genblk2[1].ram_reg_0_bram_1_i_6_n_0 ));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-15 {cell *THIS*} {string {address width (14) is more than optimal threshold of 12. Implementing using BWWE will require more logic and timing would be suboptimal. Please use attribute ram_decomp = power if BWWE is desired.}}} {SYNTH-7 {cell *THIS*}}" *) 
  (* RDADDR_COLLISION_HWCONFIG = "DELAYED_WRITE" *) 
  (* RTL_RAM_BITS = "524288" *) 
  (* RTL_RAM_NAME = "genblk2[1].ram" *) 
  (* RTL_RAM_TYPE = "RAM_TDP" *) 
  (* ram_addr_begin = "8192" *) 
  (* ram_addr_end = "12287" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "7" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("MIDDLE"),
    .CASCADE_ORDER_B("MIDDLE"),
    .CLOCK_DOMAINS("COMMON"),
    .DOA_REG(0),
    .DOB_REG(0),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \genblk2[1].ram_reg_0_bram_2 
       (.ADDRARDADDR({portA_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({portB_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_0_bram_1_n_28 ,\genblk2[1].ram_reg_0_bram_1_n_29 ,\genblk2[1].ram_reg_0_bram_1_n_30 ,\genblk2[1].ram_reg_0_bram_1_n_31 ,\genblk2[1].ram_reg_0_bram_1_n_32 ,\genblk2[1].ram_reg_0_bram_1_n_33 ,\genblk2[1].ram_reg_0_bram_1_n_34 ,\genblk2[1].ram_reg_0_bram_1_n_35 }),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_0_bram_1_n_60 ,\genblk2[1].ram_reg_0_bram_1_n_61 ,\genblk2[1].ram_reg_0_bram_1_n_62 ,\genblk2[1].ram_reg_0_bram_1_n_63 ,\genblk2[1].ram_reg_0_bram_1_n_64 ,\genblk2[1].ram_reg_0_bram_1_n_65 ,\genblk2[1].ram_reg_0_bram_1_n_66 ,\genblk2[1].ram_reg_0_bram_1_n_67 }),
        .CASDINPA({\genblk2[1].ram_reg_0_bram_1_n_132 ,\genblk2[1].ram_reg_0_bram_1_n_133 ,\genblk2[1].ram_reg_0_bram_1_n_134 ,\genblk2[1].ram_reg_0_bram_1_n_135 }),
        .CASDINPB({\genblk2[1].ram_reg_0_bram_1_n_136 ,\genblk2[1].ram_reg_0_bram_1_n_137 ,\genblk2[1].ram_reg_0_bram_1_n_138 ,\genblk2[1].ram_reg_0_bram_1_n_139 }),
        .CASDOMUXA(\genblk2[1].ram_reg_0_bram_2_i_1_n_0 ),
        .CASDOMUXB(\genblk2[1].ram_reg_0_bram_2_i_2_n_0 ),
        .CASDOMUXEN_A(portA_en),
        .CASDOMUXEN_B(portB_en),
        .CASDOUTA({\NLW_genblk2[1].ram_reg_0_bram_2_CASDOUTA_UNCONNECTED [31:8],\genblk2[1].ram_reg_0_bram_2_n_28 ,\genblk2[1].ram_reg_0_bram_2_n_29 ,\genblk2[1].ram_reg_0_bram_2_n_30 ,\genblk2[1].ram_reg_0_bram_2_n_31 ,\genblk2[1].ram_reg_0_bram_2_n_32 ,\genblk2[1].ram_reg_0_bram_2_n_33 ,\genblk2[1].ram_reg_0_bram_2_n_34 ,\genblk2[1].ram_reg_0_bram_2_n_35 }),
        .CASDOUTB({\NLW_genblk2[1].ram_reg_0_bram_2_CASDOUTB_UNCONNECTED [31:8],\genblk2[1].ram_reg_0_bram_2_n_60 ,\genblk2[1].ram_reg_0_bram_2_n_61 ,\genblk2[1].ram_reg_0_bram_2_n_62 ,\genblk2[1].ram_reg_0_bram_2_n_63 ,\genblk2[1].ram_reg_0_bram_2_n_64 ,\genblk2[1].ram_reg_0_bram_2_n_65 ,\genblk2[1].ram_reg_0_bram_2_n_66 ,\genblk2[1].ram_reg_0_bram_2_n_67 }),
        .CASDOUTPA({\genblk2[1].ram_reg_0_bram_2_n_132 ,\genblk2[1].ram_reg_0_bram_2_n_133 ,\genblk2[1].ram_reg_0_bram_2_n_134 ,\genblk2[1].ram_reg_0_bram_2_n_135 }),
        .CASDOUTPB({\genblk2[1].ram_reg_0_bram_2_n_136 ,\genblk2[1].ram_reg_0_bram_2_n_137 ,\genblk2[1].ram_reg_0_bram_2_n_138 ,\genblk2[1].ram_reg_0_bram_2_n_139 }),
        .CASINDBITERR(\genblk2[1].ram_reg_0_bram_1_n_0 ),
        .CASINSBITERR(\genblk2[1].ram_reg_0_bram_1_n_1 ),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b1),
        .CASOREGIMUXEN_B(1'b1),
        .CASOUTDBITERR(\genblk2[1].ram_reg_0_bram_2_n_0 ),
        .CASOUTSBITERR(\genblk2[1].ram_reg_0_bram_2_n_1 ),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DBITERR(\NLW_genblk2[1].ram_reg_0_bram_2_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,portA_data_in[7:0]}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,portB_data_in[7:0]}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT(\NLW_genblk2[1].ram_reg_0_bram_2_DOUTADOUT_UNCONNECTED [31:0]),
        .DOUTBDOUT(\NLW_genblk2[1].ram_reg_0_bram_2_DOUTBDOUT_UNCONNECTED [31:0]),
        .DOUTPADOUTP(\NLW_genblk2[1].ram_reg_0_bram_2_DOUTPADOUTP_UNCONNECTED [3:0]),
        .DOUTPBDOUTP(\NLW_genblk2[1].ram_reg_0_bram_2_DOUTPBDOUTP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_genblk2[1].ram_reg_0_bram_2_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b1),
        .ENARDEN(\genblk2[1].ram_reg_0_bram_2_i_3_n_0 ),
        .ENBWREN(\genblk2[1].ram_reg_0_bram_2_i_4_n_0 ),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_genblk2[1].ram_reg_0_bram_2_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_genblk2[1].ram_reg_0_bram_2_SBITERR_UNCONNECTED ),
        .SLEEP(1'b0),
        .WEA({\genblk2[1].ram_reg_0_bram_2_i_5_n_0 ,\genblk2[1].ram_reg_0_bram_2_i_5_n_0 ,\genblk2[1].ram_reg_0_bram_2_i_5_n_0 ,\genblk2[1].ram_reg_0_bram_2_i_5_n_0 }),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_0_bram_2_i_6_n_0 ,\genblk2[1].ram_reg_0_bram_2_i_6_n_0 ,\genblk2[1].ram_reg_0_bram_2_i_6_n_0 ,\genblk2[1].ram_reg_0_bram_2_i_6_n_0 }));
  LUT2 #(
    .INIT(4'hB)) 
    \genblk2[1].ram_reg_0_bram_2_i_1 
       (.I0(portA_addr[12]),
        .I1(portA_addr[13]),
        .O(\genblk2[1].ram_reg_0_bram_2_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hB)) 
    \genblk2[1].ram_reg_0_bram_2_i_2 
       (.I0(portB_addr[12]),
        .I1(portB_addr[13]),
        .O(\genblk2[1].ram_reg_0_bram_2_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'h20)) 
    \genblk2[1].ram_reg_0_bram_2_i_3 
       (.I0(portA_en),
        .I1(portA_addr[12]),
        .I2(portA_addr[13]),
        .O(\genblk2[1].ram_reg_0_bram_2_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'h20)) 
    \genblk2[1].ram_reg_0_bram_2_i_4 
       (.I0(portB_en),
        .I1(portB_addr[12]),
        .I2(portB_addr[13]),
        .O(\genblk2[1].ram_reg_0_bram_2_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h20)) 
    \genblk2[1].ram_reg_0_bram_2_i_5 
       (.I0(portA_be[0]),
        .I1(portA_addr[12]),
        .I2(portA_addr[13]),
        .O(\genblk2[1].ram_reg_0_bram_2_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h20)) 
    \genblk2[1].ram_reg_0_bram_2_i_6 
       (.I0(portB_be[0]),
        .I1(portB_addr[12]),
        .I2(portB_addr[13]),
        .O(\genblk2[1].ram_reg_0_bram_2_i_6_n_0 ));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-15 {cell *THIS*} {string {address width (14) is more than optimal threshold of 12. Implementing using BWWE will require more logic and timing would be suboptimal. Please use attribute ram_decomp = power if BWWE is desired.}}} {SYNTH-6 {cell *THIS*}} {SYNTH-7 {cell *THIS*}}" *) 
  (* RDADDR_COLLISION_HWCONFIG = "DELAYED_WRITE" *) 
  (* RTL_RAM_BITS = "524288" *) 
  (* RTL_RAM_NAME = "genblk2[1].ram" *) 
  (* RTL_RAM_TYPE = "RAM_TDP" *) 
  (* ram_addr_begin = "12288" *) 
  (* ram_addr_end = "16383" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "7" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("LAST"),
    .CASCADE_ORDER_B("LAST"),
    .CLOCK_DOMAINS("COMMON"),
    .DOA_REG(0),
    .DOB_REG(0),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \genblk2[1].ram_reg_0_bram_3 
       (.ADDRARDADDR({portA_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({portB_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_0_bram_2_n_28 ,\genblk2[1].ram_reg_0_bram_2_n_29 ,\genblk2[1].ram_reg_0_bram_2_n_30 ,\genblk2[1].ram_reg_0_bram_2_n_31 ,\genblk2[1].ram_reg_0_bram_2_n_32 ,\genblk2[1].ram_reg_0_bram_2_n_33 ,\genblk2[1].ram_reg_0_bram_2_n_34 ,\genblk2[1].ram_reg_0_bram_2_n_35 }),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_0_bram_2_n_60 ,\genblk2[1].ram_reg_0_bram_2_n_61 ,\genblk2[1].ram_reg_0_bram_2_n_62 ,\genblk2[1].ram_reg_0_bram_2_n_63 ,\genblk2[1].ram_reg_0_bram_2_n_64 ,\genblk2[1].ram_reg_0_bram_2_n_65 ,\genblk2[1].ram_reg_0_bram_2_n_66 ,\genblk2[1].ram_reg_0_bram_2_n_67 }),
        .CASDINPA({\genblk2[1].ram_reg_0_bram_2_n_132 ,\genblk2[1].ram_reg_0_bram_2_n_133 ,\genblk2[1].ram_reg_0_bram_2_n_134 ,\genblk2[1].ram_reg_0_bram_2_n_135 }),
        .CASDINPB({\genblk2[1].ram_reg_0_bram_2_n_136 ,\genblk2[1].ram_reg_0_bram_2_n_137 ,\genblk2[1].ram_reg_0_bram_2_n_138 ,\genblk2[1].ram_reg_0_bram_2_n_139 }),
        .CASDOMUXA(\genblk2[1].ram_reg_3_bram_3_i_1_n_0 ),
        .CASDOMUXB(\genblk2[1].ram_reg_3_bram_3_i_2_n_0 ),
        .CASDOMUXEN_A(portA_en),
        .CASDOMUXEN_B(portB_en),
        .CASDOUTA(\NLW_genblk2[1].ram_reg_0_bram_3_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_genblk2[1].ram_reg_0_bram_3_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_genblk2[1].ram_reg_0_bram_3_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_genblk2[1].ram_reg_0_bram_3_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(\genblk2[1].ram_reg_0_bram_2_n_0 ),
        .CASINSBITERR(\genblk2[1].ram_reg_0_bram_2_n_1 ),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b1),
        .CASOREGIMUXEN_B(1'b1),
        .CASOUTDBITERR(\NLW_genblk2[1].ram_reg_0_bram_3_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_genblk2[1].ram_reg_0_bram_3_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DBITERR(\NLW_genblk2[1].ram_reg_0_bram_3_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,portA_data_in[7:0]}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,portB_data_in[7:0]}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_genblk2[1].ram_reg_0_bram_3_DOUTADOUT_UNCONNECTED [31:8],portA_data_out[7:0]}),
        .DOUTBDOUT({\NLW_genblk2[1].ram_reg_0_bram_3_DOUTBDOUT_UNCONNECTED [31:8],portB_data_out[7:0]}),
        .DOUTPADOUTP(\NLW_genblk2[1].ram_reg_0_bram_3_DOUTPADOUTP_UNCONNECTED [3:0]),
        .DOUTPBDOUTP(\NLW_genblk2[1].ram_reg_0_bram_3_DOUTPBDOUTP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_genblk2[1].ram_reg_0_bram_3_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b1),
        .ENARDEN(\genblk2[1].ram_reg_3_bram_3_i_3_n_0 ),
        .ENBWREN(\genblk2[1].ram_reg_3_bram_3_i_4_n_0 ),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_genblk2[1].ram_reg_0_bram_3_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_genblk2[1].ram_reg_0_bram_3_SBITERR_UNCONNECTED ),
        .SLEEP(1'b0),
        .WEA({\genblk2[1].ram_reg_0_bram_3_i_1_n_0 ,\genblk2[1].ram_reg_0_bram_3_i_1_n_0 ,\genblk2[1].ram_reg_0_bram_3_i_1_n_0 ,\genblk2[1].ram_reg_0_bram_3_i_1_n_0 }),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_0_bram_3_i_2_n_0 ,\genblk2[1].ram_reg_0_bram_3_i_2_n_0 ,\genblk2[1].ram_reg_0_bram_3_i_2_n_0 ,\genblk2[1].ram_reg_0_bram_3_i_2_n_0 }));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \genblk2[1].ram_reg_0_bram_3_i_1 
       (.I0(portA_be[0]),
        .I1(portA_addr[13]),
        .I2(portA_addr[12]),
        .O(\genblk2[1].ram_reg_0_bram_3_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \genblk2[1].ram_reg_0_bram_3_i_2 
       (.I0(portB_be[0]),
        .I1(portB_addr[13]),
        .I2(portB_addr[12]),
        .O(\genblk2[1].ram_reg_0_bram_3_i_2_n_0 ));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-15 {cell *THIS*} {string {address width (14) is more than optimal threshold of 12. Implementing using BWWE will require more logic and timing would be suboptimal. Please use attribute ram_decomp = power if BWWE is desired.}}} {SYNTH-7 {cell *THIS*}}" *) 
  (* RDADDR_COLLISION_HWCONFIG = "DELAYED_WRITE" *) 
  (* RTL_RAM_BITS = "524288" *) 
  (* RTL_RAM_NAME = "genblk2[1].ram" *) 
  (* RTL_RAM_TYPE = "RAM_TDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "4095" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "8" *) 
  (* ram_slice_end = "15" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("FIRST"),
    .CASCADE_ORDER_B("FIRST"),
    .CLOCK_DOMAINS("COMMON"),
    .DOA_REG(0),
    .DOB_REG(0),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \genblk2[1].ram_reg_1_bram_0 
       (.ADDRARDADDR({portA_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({portB_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b1),
        .CASDOMUXEN_B(1'b1),
        .CASDOUTA({\NLW_genblk2[1].ram_reg_1_bram_0_CASDOUTA_UNCONNECTED [31:8],\genblk2[1].ram_reg_1_bram_0_n_28 ,\genblk2[1].ram_reg_1_bram_0_n_29 ,\genblk2[1].ram_reg_1_bram_0_n_30 ,\genblk2[1].ram_reg_1_bram_0_n_31 ,\genblk2[1].ram_reg_1_bram_0_n_32 ,\genblk2[1].ram_reg_1_bram_0_n_33 ,\genblk2[1].ram_reg_1_bram_0_n_34 ,\genblk2[1].ram_reg_1_bram_0_n_35 }),
        .CASDOUTB({\NLW_genblk2[1].ram_reg_1_bram_0_CASDOUTB_UNCONNECTED [31:8],\genblk2[1].ram_reg_1_bram_0_n_60 ,\genblk2[1].ram_reg_1_bram_0_n_61 ,\genblk2[1].ram_reg_1_bram_0_n_62 ,\genblk2[1].ram_reg_1_bram_0_n_63 ,\genblk2[1].ram_reg_1_bram_0_n_64 ,\genblk2[1].ram_reg_1_bram_0_n_65 ,\genblk2[1].ram_reg_1_bram_0_n_66 ,\genblk2[1].ram_reg_1_bram_0_n_67 }),
        .CASDOUTPA({\genblk2[1].ram_reg_1_bram_0_n_132 ,\genblk2[1].ram_reg_1_bram_0_n_133 ,\genblk2[1].ram_reg_1_bram_0_n_134 ,\genblk2[1].ram_reg_1_bram_0_n_135 }),
        .CASDOUTPB({\genblk2[1].ram_reg_1_bram_0_n_136 ,\genblk2[1].ram_reg_1_bram_0_n_137 ,\genblk2[1].ram_reg_1_bram_0_n_138 ,\genblk2[1].ram_reg_1_bram_0_n_139 }),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b1),
        .CASOREGIMUXEN_B(1'b1),
        .CASOUTDBITERR(\genblk2[1].ram_reg_1_bram_0_n_0 ),
        .CASOUTSBITERR(\genblk2[1].ram_reg_1_bram_0_n_1 ),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DBITERR(\NLW_genblk2[1].ram_reg_1_bram_0_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,portA_data_in[15:8]}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,portB_data_in[15:8]}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT(\NLW_genblk2[1].ram_reg_1_bram_0_DOUTADOUT_UNCONNECTED [31:0]),
        .DOUTBDOUT(\NLW_genblk2[1].ram_reg_1_bram_0_DOUTBDOUT_UNCONNECTED [31:0]),
        .DOUTPADOUTP(\NLW_genblk2[1].ram_reg_1_bram_0_DOUTPADOUTP_UNCONNECTED [3:0]),
        .DOUTPBDOUTP(\NLW_genblk2[1].ram_reg_1_bram_0_DOUTPBDOUTP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_genblk2[1].ram_reg_1_bram_0_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b1),
        .ENARDEN(\genblk2[1].ram_reg_0_bram_0_i_1_n_0 ),
        .ENBWREN(\genblk2[1].ram_reg_0_bram_0_i_2_n_0 ),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_genblk2[1].ram_reg_1_bram_0_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_genblk2[1].ram_reg_1_bram_0_SBITERR_UNCONNECTED ),
        .SLEEP(1'b0),
        .WEA({\genblk2[1].ram_reg_1_bram_0_i_1_n_0 ,\genblk2[1].ram_reg_1_bram_0_i_1_n_0 ,\genblk2[1].ram_reg_1_bram_0_i_1_n_0 ,\genblk2[1].ram_reg_1_bram_0_i_1_n_0 }),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_1_bram_0_i_2_n_0 ,\genblk2[1].ram_reg_1_bram_0_i_2_n_0 ,\genblk2[1].ram_reg_1_bram_0_i_2_n_0 ,\genblk2[1].ram_reg_1_bram_0_i_2_n_0 }));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \genblk2[1].ram_reg_1_bram_0_i_1 
       (.I0(portA_be[1]),
        .I1(portA_addr[13]),
        .I2(portA_addr[12]),
        .O(\genblk2[1].ram_reg_1_bram_0_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \genblk2[1].ram_reg_1_bram_0_i_2 
       (.I0(portB_be[1]),
        .I1(portB_addr[13]),
        .I2(portB_addr[12]),
        .O(\genblk2[1].ram_reg_1_bram_0_i_2_n_0 ));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-15 {cell *THIS*} {string {address width (14) is more than optimal threshold of 12. Implementing using BWWE will require more logic and timing would be suboptimal. Please use attribute ram_decomp = power if BWWE is desired.}}} {SYNTH-7 {cell *THIS*}}" *) 
  (* RDADDR_COLLISION_HWCONFIG = "DELAYED_WRITE" *) 
  (* RTL_RAM_BITS = "524288" *) 
  (* RTL_RAM_NAME = "genblk2[1].ram" *) 
  (* RTL_RAM_TYPE = "RAM_TDP" *) 
  (* ram_addr_begin = "4096" *) 
  (* ram_addr_end = "8191" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "8" *) 
  (* ram_slice_end = "15" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("MIDDLE"),
    .CASCADE_ORDER_B("MIDDLE"),
    .CLOCK_DOMAINS("COMMON"),
    .DOA_REG(0),
    .DOB_REG(0),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \genblk2[1].ram_reg_1_bram_1 
       (.ADDRARDADDR({portA_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({portB_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_1_bram_0_n_28 ,\genblk2[1].ram_reg_1_bram_0_n_29 ,\genblk2[1].ram_reg_1_bram_0_n_30 ,\genblk2[1].ram_reg_1_bram_0_n_31 ,\genblk2[1].ram_reg_1_bram_0_n_32 ,\genblk2[1].ram_reg_1_bram_0_n_33 ,\genblk2[1].ram_reg_1_bram_0_n_34 ,\genblk2[1].ram_reg_1_bram_0_n_35 }),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_1_bram_0_n_60 ,\genblk2[1].ram_reg_1_bram_0_n_61 ,\genblk2[1].ram_reg_1_bram_0_n_62 ,\genblk2[1].ram_reg_1_bram_0_n_63 ,\genblk2[1].ram_reg_1_bram_0_n_64 ,\genblk2[1].ram_reg_1_bram_0_n_65 ,\genblk2[1].ram_reg_1_bram_0_n_66 ,\genblk2[1].ram_reg_1_bram_0_n_67 }),
        .CASDINPA({\genblk2[1].ram_reg_1_bram_0_n_132 ,\genblk2[1].ram_reg_1_bram_0_n_133 ,\genblk2[1].ram_reg_1_bram_0_n_134 ,\genblk2[1].ram_reg_1_bram_0_n_135 }),
        .CASDINPB({\genblk2[1].ram_reg_1_bram_0_n_136 ,\genblk2[1].ram_reg_1_bram_0_n_137 ,\genblk2[1].ram_reg_1_bram_0_n_138 ,\genblk2[1].ram_reg_1_bram_0_n_139 }),
        .CASDOMUXA(\genblk2[1].ram_reg_0_bram_1_i_1_n_0 ),
        .CASDOMUXB(\genblk2[1].ram_reg_0_bram_1_i_2_n_0 ),
        .CASDOMUXEN_A(portA_en),
        .CASDOMUXEN_B(portB_en),
        .CASDOUTA({\NLW_genblk2[1].ram_reg_1_bram_1_CASDOUTA_UNCONNECTED [31:8],\genblk2[1].ram_reg_1_bram_1_n_28 ,\genblk2[1].ram_reg_1_bram_1_n_29 ,\genblk2[1].ram_reg_1_bram_1_n_30 ,\genblk2[1].ram_reg_1_bram_1_n_31 ,\genblk2[1].ram_reg_1_bram_1_n_32 ,\genblk2[1].ram_reg_1_bram_1_n_33 ,\genblk2[1].ram_reg_1_bram_1_n_34 ,\genblk2[1].ram_reg_1_bram_1_n_35 }),
        .CASDOUTB({\NLW_genblk2[1].ram_reg_1_bram_1_CASDOUTB_UNCONNECTED [31:8],\genblk2[1].ram_reg_1_bram_1_n_60 ,\genblk2[1].ram_reg_1_bram_1_n_61 ,\genblk2[1].ram_reg_1_bram_1_n_62 ,\genblk2[1].ram_reg_1_bram_1_n_63 ,\genblk2[1].ram_reg_1_bram_1_n_64 ,\genblk2[1].ram_reg_1_bram_1_n_65 ,\genblk2[1].ram_reg_1_bram_1_n_66 ,\genblk2[1].ram_reg_1_bram_1_n_67 }),
        .CASDOUTPA({\genblk2[1].ram_reg_1_bram_1_n_132 ,\genblk2[1].ram_reg_1_bram_1_n_133 ,\genblk2[1].ram_reg_1_bram_1_n_134 ,\genblk2[1].ram_reg_1_bram_1_n_135 }),
        .CASDOUTPB({\genblk2[1].ram_reg_1_bram_1_n_136 ,\genblk2[1].ram_reg_1_bram_1_n_137 ,\genblk2[1].ram_reg_1_bram_1_n_138 ,\genblk2[1].ram_reg_1_bram_1_n_139 }),
        .CASINDBITERR(\genblk2[1].ram_reg_1_bram_0_n_0 ),
        .CASINSBITERR(\genblk2[1].ram_reg_1_bram_0_n_1 ),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b1),
        .CASOREGIMUXEN_B(1'b1),
        .CASOUTDBITERR(\genblk2[1].ram_reg_1_bram_1_n_0 ),
        .CASOUTSBITERR(\genblk2[1].ram_reg_1_bram_1_n_1 ),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DBITERR(\NLW_genblk2[1].ram_reg_1_bram_1_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,portA_data_in[15:8]}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,portB_data_in[15:8]}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT(\NLW_genblk2[1].ram_reg_1_bram_1_DOUTADOUT_UNCONNECTED [31:0]),
        .DOUTBDOUT(\NLW_genblk2[1].ram_reg_1_bram_1_DOUTBDOUT_UNCONNECTED [31:0]),
        .DOUTPADOUTP(\NLW_genblk2[1].ram_reg_1_bram_1_DOUTPADOUTP_UNCONNECTED [3:0]),
        .DOUTPBDOUTP(\NLW_genblk2[1].ram_reg_1_bram_1_DOUTPBDOUTP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_genblk2[1].ram_reg_1_bram_1_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b1),
        .ENARDEN(\genblk2[1].ram_reg_0_bram_1_i_3_n_0 ),
        .ENBWREN(\genblk2[1].ram_reg_0_bram_1_i_4_n_0 ),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_genblk2[1].ram_reg_1_bram_1_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_genblk2[1].ram_reg_1_bram_1_SBITERR_UNCONNECTED ),
        .SLEEP(1'b0),
        .WEA({\genblk2[1].ram_reg_1_bram_1_i_1_n_0 ,\genblk2[1].ram_reg_1_bram_1_i_1_n_0 ,\genblk2[1].ram_reg_1_bram_1_i_1_n_0 ,\genblk2[1].ram_reg_1_bram_1_i_1_n_0 }),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_1_bram_1_i_2_n_0 ,\genblk2[1].ram_reg_1_bram_1_i_2_n_0 ,\genblk2[1].ram_reg_1_bram_1_i_2_n_0 ,\genblk2[1].ram_reg_1_bram_1_i_2_n_0 }));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h20)) 
    \genblk2[1].ram_reg_1_bram_1_i_1 
       (.I0(portA_be[1]),
        .I1(portA_addr[13]),
        .I2(portA_addr[12]),
        .O(\genblk2[1].ram_reg_1_bram_1_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'h20)) 
    \genblk2[1].ram_reg_1_bram_1_i_2 
       (.I0(portB_be[1]),
        .I1(portB_addr[13]),
        .I2(portB_addr[12]),
        .O(\genblk2[1].ram_reg_1_bram_1_i_2_n_0 ));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-15 {cell *THIS*} {string {address width (14) is more than optimal threshold of 12. Implementing using BWWE will require more logic and timing would be suboptimal. Please use attribute ram_decomp = power if BWWE is desired.}}} {SYNTH-7 {cell *THIS*}}" *) 
  (* RDADDR_COLLISION_HWCONFIG = "DELAYED_WRITE" *) 
  (* RTL_RAM_BITS = "524288" *) 
  (* RTL_RAM_NAME = "genblk2[1].ram" *) 
  (* RTL_RAM_TYPE = "RAM_TDP" *) 
  (* ram_addr_begin = "8192" *) 
  (* ram_addr_end = "12287" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "8" *) 
  (* ram_slice_end = "15" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("MIDDLE"),
    .CASCADE_ORDER_B("MIDDLE"),
    .CLOCK_DOMAINS("COMMON"),
    .DOA_REG(0),
    .DOB_REG(0),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \genblk2[1].ram_reg_1_bram_2 
       (.ADDRARDADDR({portA_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({portB_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_1_bram_1_n_28 ,\genblk2[1].ram_reg_1_bram_1_n_29 ,\genblk2[1].ram_reg_1_bram_1_n_30 ,\genblk2[1].ram_reg_1_bram_1_n_31 ,\genblk2[1].ram_reg_1_bram_1_n_32 ,\genblk2[1].ram_reg_1_bram_1_n_33 ,\genblk2[1].ram_reg_1_bram_1_n_34 ,\genblk2[1].ram_reg_1_bram_1_n_35 }),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_1_bram_1_n_60 ,\genblk2[1].ram_reg_1_bram_1_n_61 ,\genblk2[1].ram_reg_1_bram_1_n_62 ,\genblk2[1].ram_reg_1_bram_1_n_63 ,\genblk2[1].ram_reg_1_bram_1_n_64 ,\genblk2[1].ram_reg_1_bram_1_n_65 ,\genblk2[1].ram_reg_1_bram_1_n_66 ,\genblk2[1].ram_reg_1_bram_1_n_67 }),
        .CASDINPA({\genblk2[1].ram_reg_1_bram_1_n_132 ,\genblk2[1].ram_reg_1_bram_1_n_133 ,\genblk2[1].ram_reg_1_bram_1_n_134 ,\genblk2[1].ram_reg_1_bram_1_n_135 }),
        .CASDINPB({\genblk2[1].ram_reg_1_bram_1_n_136 ,\genblk2[1].ram_reg_1_bram_1_n_137 ,\genblk2[1].ram_reg_1_bram_1_n_138 ,\genblk2[1].ram_reg_1_bram_1_n_139 }),
        .CASDOMUXA(\genblk2[1].ram_reg_0_bram_2_i_1_n_0 ),
        .CASDOMUXB(\genblk2[1].ram_reg_0_bram_2_i_2_n_0 ),
        .CASDOMUXEN_A(portA_en),
        .CASDOMUXEN_B(portB_en),
        .CASDOUTA({\NLW_genblk2[1].ram_reg_1_bram_2_CASDOUTA_UNCONNECTED [31:8],\genblk2[1].ram_reg_1_bram_2_n_28 ,\genblk2[1].ram_reg_1_bram_2_n_29 ,\genblk2[1].ram_reg_1_bram_2_n_30 ,\genblk2[1].ram_reg_1_bram_2_n_31 ,\genblk2[1].ram_reg_1_bram_2_n_32 ,\genblk2[1].ram_reg_1_bram_2_n_33 ,\genblk2[1].ram_reg_1_bram_2_n_34 ,\genblk2[1].ram_reg_1_bram_2_n_35 }),
        .CASDOUTB({\NLW_genblk2[1].ram_reg_1_bram_2_CASDOUTB_UNCONNECTED [31:8],\genblk2[1].ram_reg_1_bram_2_n_60 ,\genblk2[1].ram_reg_1_bram_2_n_61 ,\genblk2[1].ram_reg_1_bram_2_n_62 ,\genblk2[1].ram_reg_1_bram_2_n_63 ,\genblk2[1].ram_reg_1_bram_2_n_64 ,\genblk2[1].ram_reg_1_bram_2_n_65 ,\genblk2[1].ram_reg_1_bram_2_n_66 ,\genblk2[1].ram_reg_1_bram_2_n_67 }),
        .CASDOUTPA({\genblk2[1].ram_reg_1_bram_2_n_132 ,\genblk2[1].ram_reg_1_bram_2_n_133 ,\genblk2[1].ram_reg_1_bram_2_n_134 ,\genblk2[1].ram_reg_1_bram_2_n_135 }),
        .CASDOUTPB({\genblk2[1].ram_reg_1_bram_2_n_136 ,\genblk2[1].ram_reg_1_bram_2_n_137 ,\genblk2[1].ram_reg_1_bram_2_n_138 ,\genblk2[1].ram_reg_1_bram_2_n_139 }),
        .CASINDBITERR(\genblk2[1].ram_reg_1_bram_1_n_0 ),
        .CASINSBITERR(\genblk2[1].ram_reg_1_bram_1_n_1 ),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b1),
        .CASOREGIMUXEN_B(1'b1),
        .CASOUTDBITERR(\genblk2[1].ram_reg_1_bram_2_n_0 ),
        .CASOUTSBITERR(\genblk2[1].ram_reg_1_bram_2_n_1 ),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DBITERR(\NLW_genblk2[1].ram_reg_1_bram_2_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,portA_data_in[15:8]}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,portB_data_in[15:8]}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT(\NLW_genblk2[1].ram_reg_1_bram_2_DOUTADOUT_UNCONNECTED [31:0]),
        .DOUTBDOUT(\NLW_genblk2[1].ram_reg_1_bram_2_DOUTBDOUT_UNCONNECTED [31:0]),
        .DOUTPADOUTP(\NLW_genblk2[1].ram_reg_1_bram_2_DOUTPADOUTP_UNCONNECTED [3:0]),
        .DOUTPBDOUTP(\NLW_genblk2[1].ram_reg_1_bram_2_DOUTPBDOUTP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_genblk2[1].ram_reg_1_bram_2_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b1),
        .ENARDEN(\genblk2[1].ram_reg_0_bram_2_i_3_n_0 ),
        .ENBWREN(\genblk2[1].ram_reg_0_bram_2_i_4_n_0 ),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_genblk2[1].ram_reg_1_bram_2_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_genblk2[1].ram_reg_1_bram_2_SBITERR_UNCONNECTED ),
        .SLEEP(1'b0),
        .WEA({\genblk2[1].ram_reg_1_bram_2_i_1_n_0 ,\genblk2[1].ram_reg_1_bram_2_i_1_n_0 ,\genblk2[1].ram_reg_1_bram_2_i_1_n_0 ,\genblk2[1].ram_reg_1_bram_2_i_1_n_0 }),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_1_bram_2_i_2_n_0 ,\genblk2[1].ram_reg_1_bram_2_i_2_n_0 ,\genblk2[1].ram_reg_1_bram_2_i_2_n_0 ,\genblk2[1].ram_reg_1_bram_2_i_2_n_0 }));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h20)) 
    \genblk2[1].ram_reg_1_bram_2_i_1 
       (.I0(portA_be[1]),
        .I1(portA_addr[12]),
        .I2(portA_addr[13]),
        .O(\genblk2[1].ram_reg_1_bram_2_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'h20)) 
    \genblk2[1].ram_reg_1_bram_2_i_2 
       (.I0(portB_be[1]),
        .I1(portB_addr[12]),
        .I2(portB_addr[13]),
        .O(\genblk2[1].ram_reg_1_bram_2_i_2_n_0 ));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-15 {cell *THIS*} {string {address width (14) is more than optimal threshold of 12. Implementing using BWWE will require more logic and timing would be suboptimal. Please use attribute ram_decomp = power if BWWE is desired.}}} {SYNTH-6 {cell *THIS*}} {SYNTH-7 {cell *THIS*}}" *) 
  (* RDADDR_COLLISION_HWCONFIG = "DELAYED_WRITE" *) 
  (* RTL_RAM_BITS = "524288" *) 
  (* RTL_RAM_NAME = "genblk2[1].ram" *) 
  (* RTL_RAM_TYPE = "RAM_TDP" *) 
  (* ram_addr_begin = "12288" *) 
  (* ram_addr_end = "16383" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "8" *) 
  (* ram_slice_end = "15" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("LAST"),
    .CASCADE_ORDER_B("LAST"),
    .CLOCK_DOMAINS("COMMON"),
    .DOA_REG(0),
    .DOB_REG(0),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \genblk2[1].ram_reg_1_bram_3 
       (.ADDRARDADDR({portA_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({portB_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_1_bram_2_n_28 ,\genblk2[1].ram_reg_1_bram_2_n_29 ,\genblk2[1].ram_reg_1_bram_2_n_30 ,\genblk2[1].ram_reg_1_bram_2_n_31 ,\genblk2[1].ram_reg_1_bram_2_n_32 ,\genblk2[1].ram_reg_1_bram_2_n_33 ,\genblk2[1].ram_reg_1_bram_2_n_34 ,\genblk2[1].ram_reg_1_bram_2_n_35 }),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_1_bram_2_n_60 ,\genblk2[1].ram_reg_1_bram_2_n_61 ,\genblk2[1].ram_reg_1_bram_2_n_62 ,\genblk2[1].ram_reg_1_bram_2_n_63 ,\genblk2[1].ram_reg_1_bram_2_n_64 ,\genblk2[1].ram_reg_1_bram_2_n_65 ,\genblk2[1].ram_reg_1_bram_2_n_66 ,\genblk2[1].ram_reg_1_bram_2_n_67 }),
        .CASDINPA({\genblk2[1].ram_reg_1_bram_2_n_132 ,\genblk2[1].ram_reg_1_bram_2_n_133 ,\genblk2[1].ram_reg_1_bram_2_n_134 ,\genblk2[1].ram_reg_1_bram_2_n_135 }),
        .CASDINPB({\genblk2[1].ram_reg_1_bram_2_n_136 ,\genblk2[1].ram_reg_1_bram_2_n_137 ,\genblk2[1].ram_reg_1_bram_2_n_138 ,\genblk2[1].ram_reg_1_bram_2_n_139 }),
        .CASDOMUXA(\genblk2[1].ram_reg_3_bram_3_i_1_n_0 ),
        .CASDOMUXB(\genblk2[1].ram_reg_3_bram_3_i_2_n_0 ),
        .CASDOMUXEN_A(portA_en),
        .CASDOMUXEN_B(portB_en),
        .CASDOUTA(\NLW_genblk2[1].ram_reg_1_bram_3_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_genblk2[1].ram_reg_1_bram_3_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_genblk2[1].ram_reg_1_bram_3_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_genblk2[1].ram_reg_1_bram_3_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(\genblk2[1].ram_reg_1_bram_2_n_0 ),
        .CASINSBITERR(\genblk2[1].ram_reg_1_bram_2_n_1 ),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b1),
        .CASOREGIMUXEN_B(1'b1),
        .CASOUTDBITERR(\NLW_genblk2[1].ram_reg_1_bram_3_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_genblk2[1].ram_reg_1_bram_3_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DBITERR(\NLW_genblk2[1].ram_reg_1_bram_3_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,portA_data_in[15:8]}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,portB_data_in[15:8]}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_genblk2[1].ram_reg_1_bram_3_DOUTADOUT_UNCONNECTED [31:8],portA_data_out[15:8]}),
        .DOUTBDOUT({\NLW_genblk2[1].ram_reg_1_bram_3_DOUTBDOUT_UNCONNECTED [31:8],portB_data_out[15:8]}),
        .DOUTPADOUTP(\NLW_genblk2[1].ram_reg_1_bram_3_DOUTPADOUTP_UNCONNECTED [3:0]),
        .DOUTPBDOUTP(\NLW_genblk2[1].ram_reg_1_bram_3_DOUTPBDOUTP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_genblk2[1].ram_reg_1_bram_3_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b1),
        .ENARDEN(\genblk2[1].ram_reg_3_bram_3_i_3_n_0 ),
        .ENBWREN(\genblk2[1].ram_reg_3_bram_3_i_4_n_0 ),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_genblk2[1].ram_reg_1_bram_3_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_genblk2[1].ram_reg_1_bram_3_SBITERR_UNCONNECTED ),
        .SLEEP(1'b0),
        .WEA({\genblk2[1].ram_reg_1_bram_3_i_1_n_0 ,\genblk2[1].ram_reg_1_bram_3_i_1_n_0 ,\genblk2[1].ram_reg_1_bram_3_i_1_n_0 ,\genblk2[1].ram_reg_1_bram_3_i_1_n_0 }),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_1_bram_3_i_2_n_0 ,\genblk2[1].ram_reg_1_bram_3_i_2_n_0 ,\genblk2[1].ram_reg_1_bram_3_i_2_n_0 ,\genblk2[1].ram_reg_1_bram_3_i_2_n_0 }));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \genblk2[1].ram_reg_1_bram_3_i_1 
       (.I0(portA_be[1]),
        .I1(portA_addr[13]),
        .I2(portA_addr[12]),
        .O(\genblk2[1].ram_reg_1_bram_3_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \genblk2[1].ram_reg_1_bram_3_i_2 
       (.I0(portB_be[1]),
        .I1(portB_addr[13]),
        .I2(portB_addr[12]),
        .O(\genblk2[1].ram_reg_1_bram_3_i_2_n_0 ));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-15 {cell *THIS*} {string {address width (14) is more than optimal threshold of 12. Implementing using BWWE will require more logic and timing would be suboptimal. Please use attribute ram_decomp = power if BWWE is desired.}}} {SYNTH-7 {cell *THIS*}}" *) 
  (* RDADDR_COLLISION_HWCONFIG = "DELAYED_WRITE" *) 
  (* RTL_RAM_BITS = "524288" *) 
  (* RTL_RAM_NAME = "genblk2[1].ram" *) 
  (* RTL_RAM_TYPE = "RAM_TDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "4095" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "16" *) 
  (* ram_slice_end = "23" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("FIRST"),
    .CASCADE_ORDER_B("FIRST"),
    .CLOCK_DOMAINS("COMMON"),
    .DOA_REG(0),
    .DOB_REG(0),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \genblk2[1].ram_reg_2_bram_0 
       (.ADDRARDADDR({portA_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({portB_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b1),
        .CASDOMUXEN_B(1'b1),
        .CASDOUTA({\NLW_genblk2[1].ram_reg_2_bram_0_CASDOUTA_UNCONNECTED [31:8],\genblk2[1].ram_reg_2_bram_0_n_28 ,\genblk2[1].ram_reg_2_bram_0_n_29 ,\genblk2[1].ram_reg_2_bram_0_n_30 ,\genblk2[1].ram_reg_2_bram_0_n_31 ,\genblk2[1].ram_reg_2_bram_0_n_32 ,\genblk2[1].ram_reg_2_bram_0_n_33 ,\genblk2[1].ram_reg_2_bram_0_n_34 ,\genblk2[1].ram_reg_2_bram_0_n_35 }),
        .CASDOUTB({\NLW_genblk2[1].ram_reg_2_bram_0_CASDOUTB_UNCONNECTED [31:8],\genblk2[1].ram_reg_2_bram_0_n_60 ,\genblk2[1].ram_reg_2_bram_0_n_61 ,\genblk2[1].ram_reg_2_bram_0_n_62 ,\genblk2[1].ram_reg_2_bram_0_n_63 ,\genblk2[1].ram_reg_2_bram_0_n_64 ,\genblk2[1].ram_reg_2_bram_0_n_65 ,\genblk2[1].ram_reg_2_bram_0_n_66 ,\genblk2[1].ram_reg_2_bram_0_n_67 }),
        .CASDOUTPA({\genblk2[1].ram_reg_2_bram_0_n_132 ,\genblk2[1].ram_reg_2_bram_0_n_133 ,\genblk2[1].ram_reg_2_bram_0_n_134 ,\genblk2[1].ram_reg_2_bram_0_n_135 }),
        .CASDOUTPB({\genblk2[1].ram_reg_2_bram_0_n_136 ,\genblk2[1].ram_reg_2_bram_0_n_137 ,\genblk2[1].ram_reg_2_bram_0_n_138 ,\genblk2[1].ram_reg_2_bram_0_n_139 }),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b1),
        .CASOREGIMUXEN_B(1'b1),
        .CASOUTDBITERR(\genblk2[1].ram_reg_2_bram_0_n_0 ),
        .CASOUTSBITERR(\genblk2[1].ram_reg_2_bram_0_n_1 ),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DBITERR(\NLW_genblk2[1].ram_reg_2_bram_0_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,p_1_in[23:16]}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,p_2_in[23:16]}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT(\NLW_genblk2[1].ram_reg_2_bram_0_DOUTADOUT_UNCONNECTED [31:0]),
        .DOUTBDOUT(\NLW_genblk2[1].ram_reg_2_bram_0_DOUTBDOUT_UNCONNECTED [31:0]),
        .DOUTPADOUTP(\NLW_genblk2[1].ram_reg_2_bram_0_DOUTPADOUTP_UNCONNECTED [3:0]),
        .DOUTPBDOUTP(\NLW_genblk2[1].ram_reg_2_bram_0_DOUTPBDOUTP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_genblk2[1].ram_reg_2_bram_0_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b1),
        .ENARDEN(\genblk2[1].ram_reg_0_bram_0_i_1_n_0 ),
        .ENBWREN(\genblk2[1].ram_reg_0_bram_0_i_2_n_0 ),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_genblk2[1].ram_reg_2_bram_0_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_genblk2[1].ram_reg_2_bram_0_SBITERR_UNCONNECTED ),
        .SLEEP(1'b0),
        .WEA({\genblk2[1].ram_reg_2_bram_0_i_1_n_0 ,\genblk2[1].ram_reg_2_bram_0_i_1_n_0 ,\genblk2[1].ram_reg_2_bram_0_i_1_n_0 ,\genblk2[1].ram_reg_2_bram_0_i_1_n_0 }),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_2_bram_0_i_2_n_0 ,\genblk2[1].ram_reg_2_bram_0_i_2_n_0 ,\genblk2[1].ram_reg_2_bram_0_i_2_n_0 ,\genblk2[1].ram_reg_2_bram_0_i_2_n_0 }));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \genblk2[1].ram_reg_2_bram_0_i_1 
       (.I0(portA_be[2]),
        .I1(portA_addr[13]),
        .I2(portA_addr[12]),
        .O(\genblk2[1].ram_reg_2_bram_0_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \genblk2[1].ram_reg_2_bram_0_i_2 
       (.I0(portB_be[2]),
        .I1(portB_addr[13]),
        .I2(portB_addr[12]),
        .O(\genblk2[1].ram_reg_2_bram_0_i_2_n_0 ));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-15 {cell *THIS*} {string {address width (14) is more than optimal threshold of 12. Implementing using BWWE will require more logic and timing would be suboptimal. Please use attribute ram_decomp = power if BWWE is desired.}}} {SYNTH-7 {cell *THIS*}}" *) 
  (* RDADDR_COLLISION_HWCONFIG = "DELAYED_WRITE" *) 
  (* RTL_RAM_BITS = "524288" *) 
  (* RTL_RAM_NAME = "genblk2[1].ram" *) 
  (* RTL_RAM_TYPE = "RAM_TDP" *) 
  (* ram_addr_begin = "4096" *) 
  (* ram_addr_end = "8191" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "16" *) 
  (* ram_slice_end = "23" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("MIDDLE"),
    .CASCADE_ORDER_B("MIDDLE"),
    .CLOCK_DOMAINS("COMMON"),
    .DOA_REG(0),
    .DOB_REG(0),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \genblk2[1].ram_reg_2_bram_1 
       (.ADDRARDADDR({portA_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({portB_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_2_bram_0_n_28 ,\genblk2[1].ram_reg_2_bram_0_n_29 ,\genblk2[1].ram_reg_2_bram_0_n_30 ,\genblk2[1].ram_reg_2_bram_0_n_31 ,\genblk2[1].ram_reg_2_bram_0_n_32 ,\genblk2[1].ram_reg_2_bram_0_n_33 ,\genblk2[1].ram_reg_2_bram_0_n_34 ,\genblk2[1].ram_reg_2_bram_0_n_35 }),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_2_bram_0_n_60 ,\genblk2[1].ram_reg_2_bram_0_n_61 ,\genblk2[1].ram_reg_2_bram_0_n_62 ,\genblk2[1].ram_reg_2_bram_0_n_63 ,\genblk2[1].ram_reg_2_bram_0_n_64 ,\genblk2[1].ram_reg_2_bram_0_n_65 ,\genblk2[1].ram_reg_2_bram_0_n_66 ,\genblk2[1].ram_reg_2_bram_0_n_67 }),
        .CASDINPA({\genblk2[1].ram_reg_2_bram_0_n_132 ,\genblk2[1].ram_reg_2_bram_0_n_133 ,\genblk2[1].ram_reg_2_bram_0_n_134 ,\genblk2[1].ram_reg_2_bram_0_n_135 }),
        .CASDINPB({\genblk2[1].ram_reg_2_bram_0_n_136 ,\genblk2[1].ram_reg_2_bram_0_n_137 ,\genblk2[1].ram_reg_2_bram_0_n_138 ,\genblk2[1].ram_reg_2_bram_0_n_139 }),
        .CASDOMUXA(\genblk2[1].ram_reg_0_bram_1_i_1_n_0 ),
        .CASDOMUXB(\genblk2[1].ram_reg_0_bram_1_i_2_n_0 ),
        .CASDOMUXEN_A(portA_en),
        .CASDOMUXEN_B(portB_en),
        .CASDOUTA({\NLW_genblk2[1].ram_reg_2_bram_1_CASDOUTA_UNCONNECTED [31:8],\genblk2[1].ram_reg_2_bram_1_n_28 ,\genblk2[1].ram_reg_2_bram_1_n_29 ,\genblk2[1].ram_reg_2_bram_1_n_30 ,\genblk2[1].ram_reg_2_bram_1_n_31 ,\genblk2[1].ram_reg_2_bram_1_n_32 ,\genblk2[1].ram_reg_2_bram_1_n_33 ,\genblk2[1].ram_reg_2_bram_1_n_34 ,\genblk2[1].ram_reg_2_bram_1_n_35 }),
        .CASDOUTB({\NLW_genblk2[1].ram_reg_2_bram_1_CASDOUTB_UNCONNECTED [31:8],\genblk2[1].ram_reg_2_bram_1_n_60 ,\genblk2[1].ram_reg_2_bram_1_n_61 ,\genblk2[1].ram_reg_2_bram_1_n_62 ,\genblk2[1].ram_reg_2_bram_1_n_63 ,\genblk2[1].ram_reg_2_bram_1_n_64 ,\genblk2[1].ram_reg_2_bram_1_n_65 ,\genblk2[1].ram_reg_2_bram_1_n_66 ,\genblk2[1].ram_reg_2_bram_1_n_67 }),
        .CASDOUTPA({\genblk2[1].ram_reg_2_bram_1_n_132 ,\genblk2[1].ram_reg_2_bram_1_n_133 ,\genblk2[1].ram_reg_2_bram_1_n_134 ,\genblk2[1].ram_reg_2_bram_1_n_135 }),
        .CASDOUTPB({\genblk2[1].ram_reg_2_bram_1_n_136 ,\genblk2[1].ram_reg_2_bram_1_n_137 ,\genblk2[1].ram_reg_2_bram_1_n_138 ,\genblk2[1].ram_reg_2_bram_1_n_139 }),
        .CASINDBITERR(\genblk2[1].ram_reg_2_bram_0_n_0 ),
        .CASINSBITERR(\genblk2[1].ram_reg_2_bram_0_n_1 ),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b1),
        .CASOREGIMUXEN_B(1'b1),
        .CASOUTDBITERR(\genblk2[1].ram_reg_2_bram_1_n_0 ),
        .CASOUTSBITERR(\genblk2[1].ram_reg_2_bram_1_n_1 ),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DBITERR(\NLW_genblk2[1].ram_reg_2_bram_1_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,p_1_in[23:16]}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,p_2_in[23:16]}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT(\NLW_genblk2[1].ram_reg_2_bram_1_DOUTADOUT_UNCONNECTED [31:0]),
        .DOUTBDOUT(\NLW_genblk2[1].ram_reg_2_bram_1_DOUTBDOUT_UNCONNECTED [31:0]),
        .DOUTPADOUTP(\NLW_genblk2[1].ram_reg_2_bram_1_DOUTPADOUTP_UNCONNECTED [3:0]),
        .DOUTPBDOUTP(\NLW_genblk2[1].ram_reg_2_bram_1_DOUTPBDOUTP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_genblk2[1].ram_reg_2_bram_1_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b1),
        .ENARDEN(\genblk2[1].ram_reg_0_bram_1_i_3_n_0 ),
        .ENBWREN(\genblk2[1].ram_reg_0_bram_1_i_4_n_0 ),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_genblk2[1].ram_reg_2_bram_1_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_genblk2[1].ram_reg_2_bram_1_SBITERR_UNCONNECTED ),
        .SLEEP(1'b0),
        .WEA({\genblk2[1].ram_reg_2_bram_1_i_1_n_0 ,\genblk2[1].ram_reg_2_bram_1_i_1_n_0 ,\genblk2[1].ram_reg_2_bram_1_i_1_n_0 ,\genblk2[1].ram_reg_2_bram_1_i_1_n_0 }),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_2_bram_1_i_2_n_0 ,\genblk2[1].ram_reg_2_bram_1_i_2_n_0 ,\genblk2[1].ram_reg_2_bram_1_i_2_n_0 ,\genblk2[1].ram_reg_2_bram_1_i_2_n_0 }));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'h20)) 
    \genblk2[1].ram_reg_2_bram_1_i_1 
       (.I0(portA_be[2]),
        .I1(portA_addr[13]),
        .I2(portA_addr[12]),
        .O(\genblk2[1].ram_reg_2_bram_1_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h20)) 
    \genblk2[1].ram_reg_2_bram_1_i_2 
       (.I0(portB_be[2]),
        .I1(portB_addr[13]),
        .I2(portB_addr[12]),
        .O(\genblk2[1].ram_reg_2_bram_1_i_2_n_0 ));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-15 {cell *THIS*} {string {address width (14) is more than optimal threshold of 12. Implementing using BWWE will require more logic and timing would be suboptimal. Please use attribute ram_decomp = power if BWWE is desired.}}} {SYNTH-7 {cell *THIS*}}" *) 
  (* RDADDR_COLLISION_HWCONFIG = "DELAYED_WRITE" *) 
  (* RTL_RAM_BITS = "524288" *) 
  (* RTL_RAM_NAME = "genblk2[1].ram" *) 
  (* RTL_RAM_TYPE = "RAM_TDP" *) 
  (* ram_addr_begin = "8192" *) 
  (* ram_addr_end = "12287" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "16" *) 
  (* ram_slice_end = "23" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("MIDDLE"),
    .CASCADE_ORDER_B("MIDDLE"),
    .CLOCK_DOMAINS("COMMON"),
    .DOA_REG(0),
    .DOB_REG(0),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \genblk2[1].ram_reg_2_bram_2 
       (.ADDRARDADDR({portA_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({portB_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_2_bram_1_n_28 ,\genblk2[1].ram_reg_2_bram_1_n_29 ,\genblk2[1].ram_reg_2_bram_1_n_30 ,\genblk2[1].ram_reg_2_bram_1_n_31 ,\genblk2[1].ram_reg_2_bram_1_n_32 ,\genblk2[1].ram_reg_2_bram_1_n_33 ,\genblk2[1].ram_reg_2_bram_1_n_34 ,\genblk2[1].ram_reg_2_bram_1_n_35 }),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_2_bram_1_n_60 ,\genblk2[1].ram_reg_2_bram_1_n_61 ,\genblk2[1].ram_reg_2_bram_1_n_62 ,\genblk2[1].ram_reg_2_bram_1_n_63 ,\genblk2[1].ram_reg_2_bram_1_n_64 ,\genblk2[1].ram_reg_2_bram_1_n_65 ,\genblk2[1].ram_reg_2_bram_1_n_66 ,\genblk2[1].ram_reg_2_bram_1_n_67 }),
        .CASDINPA({\genblk2[1].ram_reg_2_bram_1_n_132 ,\genblk2[1].ram_reg_2_bram_1_n_133 ,\genblk2[1].ram_reg_2_bram_1_n_134 ,\genblk2[1].ram_reg_2_bram_1_n_135 }),
        .CASDINPB({\genblk2[1].ram_reg_2_bram_1_n_136 ,\genblk2[1].ram_reg_2_bram_1_n_137 ,\genblk2[1].ram_reg_2_bram_1_n_138 ,\genblk2[1].ram_reg_2_bram_1_n_139 }),
        .CASDOMUXA(\genblk2[1].ram_reg_0_bram_2_i_1_n_0 ),
        .CASDOMUXB(\genblk2[1].ram_reg_0_bram_2_i_2_n_0 ),
        .CASDOMUXEN_A(portA_en),
        .CASDOMUXEN_B(portB_en),
        .CASDOUTA({\NLW_genblk2[1].ram_reg_2_bram_2_CASDOUTA_UNCONNECTED [31:8],\genblk2[1].ram_reg_2_bram_2_n_28 ,\genblk2[1].ram_reg_2_bram_2_n_29 ,\genblk2[1].ram_reg_2_bram_2_n_30 ,\genblk2[1].ram_reg_2_bram_2_n_31 ,\genblk2[1].ram_reg_2_bram_2_n_32 ,\genblk2[1].ram_reg_2_bram_2_n_33 ,\genblk2[1].ram_reg_2_bram_2_n_34 ,\genblk2[1].ram_reg_2_bram_2_n_35 }),
        .CASDOUTB({\NLW_genblk2[1].ram_reg_2_bram_2_CASDOUTB_UNCONNECTED [31:8],\genblk2[1].ram_reg_2_bram_2_n_60 ,\genblk2[1].ram_reg_2_bram_2_n_61 ,\genblk2[1].ram_reg_2_bram_2_n_62 ,\genblk2[1].ram_reg_2_bram_2_n_63 ,\genblk2[1].ram_reg_2_bram_2_n_64 ,\genblk2[1].ram_reg_2_bram_2_n_65 ,\genblk2[1].ram_reg_2_bram_2_n_66 ,\genblk2[1].ram_reg_2_bram_2_n_67 }),
        .CASDOUTPA({\genblk2[1].ram_reg_2_bram_2_n_132 ,\genblk2[1].ram_reg_2_bram_2_n_133 ,\genblk2[1].ram_reg_2_bram_2_n_134 ,\genblk2[1].ram_reg_2_bram_2_n_135 }),
        .CASDOUTPB({\genblk2[1].ram_reg_2_bram_2_n_136 ,\genblk2[1].ram_reg_2_bram_2_n_137 ,\genblk2[1].ram_reg_2_bram_2_n_138 ,\genblk2[1].ram_reg_2_bram_2_n_139 }),
        .CASINDBITERR(\genblk2[1].ram_reg_2_bram_1_n_0 ),
        .CASINSBITERR(\genblk2[1].ram_reg_2_bram_1_n_1 ),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b1),
        .CASOREGIMUXEN_B(1'b1),
        .CASOUTDBITERR(\genblk2[1].ram_reg_2_bram_2_n_0 ),
        .CASOUTSBITERR(\genblk2[1].ram_reg_2_bram_2_n_1 ),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DBITERR(\NLW_genblk2[1].ram_reg_2_bram_2_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,p_1_in[23:16]}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,p_2_in[23:16]}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT(\NLW_genblk2[1].ram_reg_2_bram_2_DOUTADOUT_UNCONNECTED [31:0]),
        .DOUTBDOUT(\NLW_genblk2[1].ram_reg_2_bram_2_DOUTBDOUT_UNCONNECTED [31:0]),
        .DOUTPADOUTP(\NLW_genblk2[1].ram_reg_2_bram_2_DOUTPADOUTP_UNCONNECTED [3:0]),
        .DOUTPBDOUTP(\NLW_genblk2[1].ram_reg_2_bram_2_DOUTPBDOUTP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_genblk2[1].ram_reg_2_bram_2_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b1),
        .ENARDEN(\genblk2[1].ram_reg_0_bram_2_i_3_n_0 ),
        .ENBWREN(\genblk2[1].ram_reg_0_bram_2_i_4_n_0 ),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_genblk2[1].ram_reg_2_bram_2_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_genblk2[1].ram_reg_2_bram_2_SBITERR_UNCONNECTED ),
        .SLEEP(1'b0),
        .WEA({\genblk2[1].ram_reg_2_bram_2_i_1_n_0 ,\genblk2[1].ram_reg_2_bram_2_i_1_n_0 ,\genblk2[1].ram_reg_2_bram_2_i_1_n_0 ,\genblk2[1].ram_reg_2_bram_2_i_1_n_0 }),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_2_bram_2_i_2_n_0 ,\genblk2[1].ram_reg_2_bram_2_i_2_n_0 ,\genblk2[1].ram_reg_2_bram_2_i_2_n_0 ,\genblk2[1].ram_reg_2_bram_2_i_2_n_0 }));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'h20)) 
    \genblk2[1].ram_reg_2_bram_2_i_1 
       (.I0(portA_be[2]),
        .I1(portA_addr[12]),
        .I2(portA_addr[13]),
        .O(\genblk2[1].ram_reg_2_bram_2_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'h20)) 
    \genblk2[1].ram_reg_2_bram_2_i_2 
       (.I0(portB_be[2]),
        .I1(portB_addr[12]),
        .I2(portB_addr[13]),
        .O(\genblk2[1].ram_reg_2_bram_2_i_2_n_0 ));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-15 {cell *THIS*} {string {address width (14) is more than optimal threshold of 12. Implementing using BWWE will require more logic and timing would be suboptimal. Please use attribute ram_decomp = power if BWWE is desired.}}} {SYNTH-6 {cell *THIS*}} {SYNTH-7 {cell *THIS*}}" *) 
  (* RDADDR_COLLISION_HWCONFIG = "DELAYED_WRITE" *) 
  (* RTL_RAM_BITS = "524288" *) 
  (* RTL_RAM_NAME = "genblk2[1].ram" *) 
  (* RTL_RAM_TYPE = "RAM_TDP" *) 
  (* ram_addr_begin = "12288" *) 
  (* ram_addr_end = "16383" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "16" *) 
  (* ram_slice_end = "23" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("LAST"),
    .CASCADE_ORDER_B("LAST"),
    .CLOCK_DOMAINS("COMMON"),
    .DOA_REG(0),
    .DOB_REG(0),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \genblk2[1].ram_reg_2_bram_3 
       (.ADDRARDADDR({portA_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({portB_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_2_bram_2_n_28 ,\genblk2[1].ram_reg_2_bram_2_n_29 ,\genblk2[1].ram_reg_2_bram_2_n_30 ,\genblk2[1].ram_reg_2_bram_2_n_31 ,\genblk2[1].ram_reg_2_bram_2_n_32 ,\genblk2[1].ram_reg_2_bram_2_n_33 ,\genblk2[1].ram_reg_2_bram_2_n_34 ,\genblk2[1].ram_reg_2_bram_2_n_35 }),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_2_bram_2_n_60 ,\genblk2[1].ram_reg_2_bram_2_n_61 ,\genblk2[1].ram_reg_2_bram_2_n_62 ,\genblk2[1].ram_reg_2_bram_2_n_63 ,\genblk2[1].ram_reg_2_bram_2_n_64 ,\genblk2[1].ram_reg_2_bram_2_n_65 ,\genblk2[1].ram_reg_2_bram_2_n_66 ,\genblk2[1].ram_reg_2_bram_2_n_67 }),
        .CASDINPA({\genblk2[1].ram_reg_2_bram_2_n_132 ,\genblk2[1].ram_reg_2_bram_2_n_133 ,\genblk2[1].ram_reg_2_bram_2_n_134 ,\genblk2[1].ram_reg_2_bram_2_n_135 }),
        .CASDINPB({\genblk2[1].ram_reg_2_bram_2_n_136 ,\genblk2[1].ram_reg_2_bram_2_n_137 ,\genblk2[1].ram_reg_2_bram_2_n_138 ,\genblk2[1].ram_reg_2_bram_2_n_139 }),
        .CASDOMUXA(\genblk2[1].ram_reg_3_bram_3_i_1_n_0 ),
        .CASDOMUXB(\genblk2[1].ram_reg_3_bram_3_i_2_n_0 ),
        .CASDOMUXEN_A(portA_en),
        .CASDOMUXEN_B(portB_en),
        .CASDOUTA(\NLW_genblk2[1].ram_reg_2_bram_3_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_genblk2[1].ram_reg_2_bram_3_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_genblk2[1].ram_reg_2_bram_3_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_genblk2[1].ram_reg_2_bram_3_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(\genblk2[1].ram_reg_2_bram_2_n_0 ),
        .CASINSBITERR(\genblk2[1].ram_reg_2_bram_2_n_1 ),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b1),
        .CASOREGIMUXEN_B(1'b1),
        .CASOUTDBITERR(\NLW_genblk2[1].ram_reg_2_bram_3_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_genblk2[1].ram_reg_2_bram_3_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DBITERR(\NLW_genblk2[1].ram_reg_2_bram_3_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,p_1_in[23:16]}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,p_2_in[23:16]}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_genblk2[1].ram_reg_2_bram_3_DOUTADOUT_UNCONNECTED [31:8],portA_data_out[23:16]}),
        .DOUTBDOUT({\NLW_genblk2[1].ram_reg_2_bram_3_DOUTBDOUT_UNCONNECTED [31:8],portB_data_out[23:16]}),
        .DOUTPADOUTP(\NLW_genblk2[1].ram_reg_2_bram_3_DOUTPADOUTP_UNCONNECTED [3:0]),
        .DOUTPBDOUTP(\NLW_genblk2[1].ram_reg_2_bram_3_DOUTPBDOUTP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_genblk2[1].ram_reg_2_bram_3_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b1),
        .ENARDEN(\genblk2[1].ram_reg_3_bram_3_i_3_n_0 ),
        .ENBWREN(\genblk2[1].ram_reg_3_bram_3_i_4_n_0 ),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_genblk2[1].ram_reg_2_bram_3_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_genblk2[1].ram_reg_2_bram_3_SBITERR_UNCONNECTED ),
        .SLEEP(1'b0),
        .WEA({\genblk2[1].ram_reg_2_bram_3_i_17_n_0 ,\genblk2[1].ram_reg_2_bram_3_i_17_n_0 ,\genblk2[1].ram_reg_2_bram_3_i_17_n_0 ,\genblk2[1].ram_reg_2_bram_3_i_17_n_0 }),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_2_bram_3_i_18_n_0 ,\genblk2[1].ram_reg_2_bram_3_i_18_n_0 ,\genblk2[1].ram_reg_2_bram_3_i_18_n_0 ,\genblk2[1].ram_reg_2_bram_3_i_18_n_0 }));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \genblk2[1].ram_reg_2_bram_3_i_1 
       (.I0(portA_en),
        .I1(portA_data_in[23]),
        .O(p_1_in[23]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \genblk2[1].ram_reg_2_bram_3_i_10 
       (.I0(portB_en),
        .I1(portB_data_in[22]),
        .O(p_2_in[22]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \genblk2[1].ram_reg_2_bram_3_i_11 
       (.I0(portB_en),
        .I1(portB_data_in[21]),
        .O(p_2_in[21]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \genblk2[1].ram_reg_2_bram_3_i_12 
       (.I0(portB_en),
        .I1(portB_data_in[20]),
        .O(p_2_in[20]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \genblk2[1].ram_reg_2_bram_3_i_13 
       (.I0(portB_en),
        .I1(portB_data_in[19]),
        .O(p_2_in[19]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \genblk2[1].ram_reg_2_bram_3_i_14 
       (.I0(portB_en),
        .I1(portB_data_in[18]),
        .O(p_2_in[18]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \genblk2[1].ram_reg_2_bram_3_i_15 
       (.I0(portB_en),
        .I1(portB_data_in[17]),
        .O(p_2_in[17]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \genblk2[1].ram_reg_2_bram_3_i_16 
       (.I0(portB_en),
        .I1(portB_data_in[16]),
        .O(p_2_in[16]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \genblk2[1].ram_reg_2_bram_3_i_17 
       (.I0(portA_be[2]),
        .I1(portA_addr[13]),
        .I2(portA_addr[12]),
        .O(\genblk2[1].ram_reg_2_bram_3_i_17_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \genblk2[1].ram_reg_2_bram_3_i_18 
       (.I0(portB_be[2]),
        .I1(portB_addr[13]),
        .I2(portB_addr[12]),
        .O(\genblk2[1].ram_reg_2_bram_3_i_18_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \genblk2[1].ram_reg_2_bram_3_i_2 
       (.I0(portA_en),
        .I1(portA_data_in[22]),
        .O(p_1_in[22]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \genblk2[1].ram_reg_2_bram_3_i_3 
       (.I0(portA_en),
        .I1(portA_data_in[21]),
        .O(p_1_in[21]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \genblk2[1].ram_reg_2_bram_3_i_4 
       (.I0(portA_en),
        .I1(portA_data_in[20]),
        .O(p_1_in[20]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \genblk2[1].ram_reg_2_bram_3_i_5 
       (.I0(portA_en),
        .I1(portA_data_in[19]),
        .O(p_1_in[19]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \genblk2[1].ram_reg_2_bram_3_i_6 
       (.I0(portA_en),
        .I1(portA_data_in[18]),
        .O(p_1_in[18]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \genblk2[1].ram_reg_2_bram_3_i_7 
       (.I0(portA_en),
        .I1(portA_data_in[17]),
        .O(p_1_in[17]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \genblk2[1].ram_reg_2_bram_3_i_8 
       (.I0(portA_en),
        .I1(portA_data_in[16]),
        .O(p_1_in[16]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \genblk2[1].ram_reg_2_bram_3_i_9 
       (.I0(portB_en),
        .I1(portB_data_in[23]),
        .O(p_2_in[23]));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-15 {cell *THIS*} {string {address width (14) is more than optimal threshold of 12. Implementing using BWWE will require more logic and timing would be suboptimal. Please use attribute ram_decomp = power if BWWE is desired.}}} {SYNTH-7 {cell *THIS*}}" *) 
  (* RDADDR_COLLISION_HWCONFIG = "DELAYED_WRITE" *) 
  (* RTL_RAM_BITS = "524288" *) 
  (* RTL_RAM_NAME = "genblk2[1].ram" *) 
  (* RTL_RAM_TYPE = "RAM_TDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "4095" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "24" *) 
  (* ram_slice_end = "31" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("FIRST"),
    .CASCADE_ORDER_B("FIRST"),
    .CLOCK_DOMAINS("COMMON"),
    .DOA_REG(0),
    .DOB_REG(0),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \genblk2[1].ram_reg_3_bram_0 
       (.ADDRARDADDR({portA_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({portB_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b1),
        .CASDOMUXEN_B(1'b1),
        .CASDOUTA({\NLW_genblk2[1].ram_reg_3_bram_0_CASDOUTA_UNCONNECTED [31:8],\genblk2[1].ram_reg_3_bram_0_n_28 ,\genblk2[1].ram_reg_3_bram_0_n_29 ,\genblk2[1].ram_reg_3_bram_0_n_30 ,\genblk2[1].ram_reg_3_bram_0_n_31 ,\genblk2[1].ram_reg_3_bram_0_n_32 ,\genblk2[1].ram_reg_3_bram_0_n_33 ,\genblk2[1].ram_reg_3_bram_0_n_34 ,\genblk2[1].ram_reg_3_bram_0_n_35 }),
        .CASDOUTB({\NLW_genblk2[1].ram_reg_3_bram_0_CASDOUTB_UNCONNECTED [31:8],\genblk2[1].ram_reg_3_bram_0_n_60 ,\genblk2[1].ram_reg_3_bram_0_n_61 ,\genblk2[1].ram_reg_3_bram_0_n_62 ,\genblk2[1].ram_reg_3_bram_0_n_63 ,\genblk2[1].ram_reg_3_bram_0_n_64 ,\genblk2[1].ram_reg_3_bram_0_n_65 ,\genblk2[1].ram_reg_3_bram_0_n_66 ,\genblk2[1].ram_reg_3_bram_0_n_67 }),
        .CASDOUTPA({\genblk2[1].ram_reg_3_bram_0_n_132 ,\genblk2[1].ram_reg_3_bram_0_n_133 ,\genblk2[1].ram_reg_3_bram_0_n_134 ,\genblk2[1].ram_reg_3_bram_0_n_135 }),
        .CASDOUTPB({\genblk2[1].ram_reg_3_bram_0_n_136 ,\genblk2[1].ram_reg_3_bram_0_n_137 ,\genblk2[1].ram_reg_3_bram_0_n_138 ,\genblk2[1].ram_reg_3_bram_0_n_139 }),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b1),
        .CASOREGIMUXEN_B(1'b1),
        .CASOUTDBITERR(\genblk2[1].ram_reg_3_bram_0_n_0 ),
        .CASOUTSBITERR(\genblk2[1].ram_reg_3_bram_0_n_1 ),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DBITERR(\NLW_genblk2[1].ram_reg_3_bram_0_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,p_1_in[31:24]}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,p_2_in[31:24]}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT(\NLW_genblk2[1].ram_reg_3_bram_0_DOUTADOUT_UNCONNECTED [31:0]),
        .DOUTBDOUT(\NLW_genblk2[1].ram_reg_3_bram_0_DOUTBDOUT_UNCONNECTED [31:0]),
        .DOUTPADOUTP(\NLW_genblk2[1].ram_reg_3_bram_0_DOUTPADOUTP_UNCONNECTED [3:0]),
        .DOUTPBDOUTP(\NLW_genblk2[1].ram_reg_3_bram_0_DOUTPBDOUTP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_genblk2[1].ram_reg_3_bram_0_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b1),
        .ENARDEN(\genblk2[1].ram_reg_0_bram_0_i_1_n_0 ),
        .ENBWREN(\genblk2[1].ram_reg_0_bram_0_i_2_n_0 ),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_genblk2[1].ram_reg_3_bram_0_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_genblk2[1].ram_reg_3_bram_0_SBITERR_UNCONNECTED ),
        .SLEEP(1'b0),
        .WEA({\genblk2[1].ram_reg_3_bram_0_i_1_n_0 ,\genblk2[1].ram_reg_3_bram_0_i_1_n_0 ,\genblk2[1].ram_reg_3_bram_0_i_1_n_0 ,\genblk2[1].ram_reg_3_bram_0_i_1_n_0 }),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_3_bram_0_i_2_n_0 ,\genblk2[1].ram_reg_3_bram_0_i_2_n_0 ,\genblk2[1].ram_reg_3_bram_0_i_2_n_0 ,\genblk2[1].ram_reg_3_bram_0_i_2_n_0 }));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \genblk2[1].ram_reg_3_bram_0_i_1 
       (.I0(portA_be[3]),
        .I1(portA_addr[13]),
        .I2(portA_addr[12]),
        .O(\genblk2[1].ram_reg_3_bram_0_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \genblk2[1].ram_reg_3_bram_0_i_2 
       (.I0(portB_be[3]),
        .I1(portB_addr[13]),
        .I2(portB_addr[12]),
        .O(\genblk2[1].ram_reg_3_bram_0_i_2_n_0 ));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-15 {cell *THIS*} {string {address width (14) is more than optimal threshold of 12. Implementing using BWWE will require more logic and timing would be suboptimal. Please use attribute ram_decomp = power if BWWE is desired.}}} {SYNTH-7 {cell *THIS*}}" *) 
  (* RDADDR_COLLISION_HWCONFIG = "DELAYED_WRITE" *) 
  (* RTL_RAM_BITS = "524288" *) 
  (* RTL_RAM_NAME = "genblk2[1].ram" *) 
  (* RTL_RAM_TYPE = "RAM_TDP" *) 
  (* ram_addr_begin = "4096" *) 
  (* ram_addr_end = "8191" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "24" *) 
  (* ram_slice_end = "31" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("MIDDLE"),
    .CASCADE_ORDER_B("MIDDLE"),
    .CLOCK_DOMAINS("COMMON"),
    .DOA_REG(0),
    .DOB_REG(0),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \genblk2[1].ram_reg_3_bram_1 
       (.ADDRARDADDR({portA_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({portB_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_3_bram_0_n_28 ,\genblk2[1].ram_reg_3_bram_0_n_29 ,\genblk2[1].ram_reg_3_bram_0_n_30 ,\genblk2[1].ram_reg_3_bram_0_n_31 ,\genblk2[1].ram_reg_3_bram_0_n_32 ,\genblk2[1].ram_reg_3_bram_0_n_33 ,\genblk2[1].ram_reg_3_bram_0_n_34 ,\genblk2[1].ram_reg_3_bram_0_n_35 }),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_3_bram_0_n_60 ,\genblk2[1].ram_reg_3_bram_0_n_61 ,\genblk2[1].ram_reg_3_bram_0_n_62 ,\genblk2[1].ram_reg_3_bram_0_n_63 ,\genblk2[1].ram_reg_3_bram_0_n_64 ,\genblk2[1].ram_reg_3_bram_0_n_65 ,\genblk2[1].ram_reg_3_bram_0_n_66 ,\genblk2[1].ram_reg_3_bram_0_n_67 }),
        .CASDINPA({\genblk2[1].ram_reg_3_bram_0_n_132 ,\genblk2[1].ram_reg_3_bram_0_n_133 ,\genblk2[1].ram_reg_3_bram_0_n_134 ,\genblk2[1].ram_reg_3_bram_0_n_135 }),
        .CASDINPB({\genblk2[1].ram_reg_3_bram_0_n_136 ,\genblk2[1].ram_reg_3_bram_0_n_137 ,\genblk2[1].ram_reg_3_bram_0_n_138 ,\genblk2[1].ram_reg_3_bram_0_n_139 }),
        .CASDOMUXA(\genblk2[1].ram_reg_0_bram_1_i_1_n_0 ),
        .CASDOMUXB(\genblk2[1].ram_reg_0_bram_1_i_2_n_0 ),
        .CASDOMUXEN_A(portA_en),
        .CASDOMUXEN_B(portB_en),
        .CASDOUTA({\NLW_genblk2[1].ram_reg_3_bram_1_CASDOUTA_UNCONNECTED [31:8],\genblk2[1].ram_reg_3_bram_1_n_28 ,\genblk2[1].ram_reg_3_bram_1_n_29 ,\genblk2[1].ram_reg_3_bram_1_n_30 ,\genblk2[1].ram_reg_3_bram_1_n_31 ,\genblk2[1].ram_reg_3_bram_1_n_32 ,\genblk2[1].ram_reg_3_bram_1_n_33 ,\genblk2[1].ram_reg_3_bram_1_n_34 ,\genblk2[1].ram_reg_3_bram_1_n_35 }),
        .CASDOUTB({\NLW_genblk2[1].ram_reg_3_bram_1_CASDOUTB_UNCONNECTED [31:8],\genblk2[1].ram_reg_3_bram_1_n_60 ,\genblk2[1].ram_reg_3_bram_1_n_61 ,\genblk2[1].ram_reg_3_bram_1_n_62 ,\genblk2[1].ram_reg_3_bram_1_n_63 ,\genblk2[1].ram_reg_3_bram_1_n_64 ,\genblk2[1].ram_reg_3_bram_1_n_65 ,\genblk2[1].ram_reg_3_bram_1_n_66 ,\genblk2[1].ram_reg_3_bram_1_n_67 }),
        .CASDOUTPA({\genblk2[1].ram_reg_3_bram_1_n_132 ,\genblk2[1].ram_reg_3_bram_1_n_133 ,\genblk2[1].ram_reg_3_bram_1_n_134 ,\genblk2[1].ram_reg_3_bram_1_n_135 }),
        .CASDOUTPB({\genblk2[1].ram_reg_3_bram_1_n_136 ,\genblk2[1].ram_reg_3_bram_1_n_137 ,\genblk2[1].ram_reg_3_bram_1_n_138 ,\genblk2[1].ram_reg_3_bram_1_n_139 }),
        .CASINDBITERR(\genblk2[1].ram_reg_3_bram_0_n_0 ),
        .CASINSBITERR(\genblk2[1].ram_reg_3_bram_0_n_1 ),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b1),
        .CASOREGIMUXEN_B(1'b1),
        .CASOUTDBITERR(\genblk2[1].ram_reg_3_bram_1_n_0 ),
        .CASOUTSBITERR(\genblk2[1].ram_reg_3_bram_1_n_1 ),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DBITERR(\NLW_genblk2[1].ram_reg_3_bram_1_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,p_1_in[31:24]}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,p_2_in[31:24]}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT(\NLW_genblk2[1].ram_reg_3_bram_1_DOUTADOUT_UNCONNECTED [31:0]),
        .DOUTBDOUT(\NLW_genblk2[1].ram_reg_3_bram_1_DOUTBDOUT_UNCONNECTED [31:0]),
        .DOUTPADOUTP(\NLW_genblk2[1].ram_reg_3_bram_1_DOUTPADOUTP_UNCONNECTED [3:0]),
        .DOUTPBDOUTP(\NLW_genblk2[1].ram_reg_3_bram_1_DOUTPBDOUTP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_genblk2[1].ram_reg_3_bram_1_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b1),
        .ENARDEN(\genblk2[1].ram_reg_0_bram_1_i_3_n_0 ),
        .ENBWREN(\genblk2[1].ram_reg_0_bram_1_i_4_n_0 ),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_genblk2[1].ram_reg_3_bram_1_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_genblk2[1].ram_reg_3_bram_1_SBITERR_UNCONNECTED ),
        .SLEEP(1'b0),
        .WEA({\genblk2[1].ram_reg_3_bram_1_i_1_n_0 ,\genblk2[1].ram_reg_3_bram_1_i_1_n_0 ,\genblk2[1].ram_reg_3_bram_1_i_1_n_0 ,\genblk2[1].ram_reg_3_bram_1_i_1_n_0 }),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_3_bram_1_i_2_n_0 ,\genblk2[1].ram_reg_3_bram_1_i_2_n_0 ,\genblk2[1].ram_reg_3_bram_1_i_2_n_0 ,\genblk2[1].ram_reg_3_bram_1_i_2_n_0 }));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'h20)) 
    \genblk2[1].ram_reg_3_bram_1_i_1 
       (.I0(portA_be[3]),
        .I1(portA_addr[13]),
        .I2(portA_addr[12]),
        .O(\genblk2[1].ram_reg_3_bram_1_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'h20)) 
    \genblk2[1].ram_reg_3_bram_1_i_2 
       (.I0(portB_be[3]),
        .I1(portB_addr[13]),
        .I2(portB_addr[12]),
        .O(\genblk2[1].ram_reg_3_bram_1_i_2_n_0 ));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-15 {cell *THIS*} {string {address width (14) is more than optimal threshold of 12. Implementing using BWWE will require more logic and timing would be suboptimal. Please use attribute ram_decomp = power if BWWE is desired.}}} {SYNTH-7 {cell *THIS*}}" *) 
  (* RDADDR_COLLISION_HWCONFIG = "DELAYED_WRITE" *) 
  (* RTL_RAM_BITS = "524288" *) 
  (* RTL_RAM_NAME = "genblk2[1].ram" *) 
  (* RTL_RAM_TYPE = "RAM_TDP" *) 
  (* ram_addr_begin = "8192" *) 
  (* ram_addr_end = "12287" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "24" *) 
  (* ram_slice_end = "31" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("MIDDLE"),
    .CASCADE_ORDER_B("MIDDLE"),
    .CLOCK_DOMAINS("COMMON"),
    .DOA_REG(0),
    .DOB_REG(0),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \genblk2[1].ram_reg_3_bram_2 
       (.ADDRARDADDR({portA_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({portB_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_3_bram_1_n_28 ,\genblk2[1].ram_reg_3_bram_1_n_29 ,\genblk2[1].ram_reg_3_bram_1_n_30 ,\genblk2[1].ram_reg_3_bram_1_n_31 ,\genblk2[1].ram_reg_3_bram_1_n_32 ,\genblk2[1].ram_reg_3_bram_1_n_33 ,\genblk2[1].ram_reg_3_bram_1_n_34 ,\genblk2[1].ram_reg_3_bram_1_n_35 }),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_3_bram_1_n_60 ,\genblk2[1].ram_reg_3_bram_1_n_61 ,\genblk2[1].ram_reg_3_bram_1_n_62 ,\genblk2[1].ram_reg_3_bram_1_n_63 ,\genblk2[1].ram_reg_3_bram_1_n_64 ,\genblk2[1].ram_reg_3_bram_1_n_65 ,\genblk2[1].ram_reg_3_bram_1_n_66 ,\genblk2[1].ram_reg_3_bram_1_n_67 }),
        .CASDINPA({\genblk2[1].ram_reg_3_bram_1_n_132 ,\genblk2[1].ram_reg_3_bram_1_n_133 ,\genblk2[1].ram_reg_3_bram_1_n_134 ,\genblk2[1].ram_reg_3_bram_1_n_135 }),
        .CASDINPB({\genblk2[1].ram_reg_3_bram_1_n_136 ,\genblk2[1].ram_reg_3_bram_1_n_137 ,\genblk2[1].ram_reg_3_bram_1_n_138 ,\genblk2[1].ram_reg_3_bram_1_n_139 }),
        .CASDOMUXA(\genblk2[1].ram_reg_0_bram_2_i_1_n_0 ),
        .CASDOMUXB(\genblk2[1].ram_reg_0_bram_2_i_2_n_0 ),
        .CASDOMUXEN_A(portA_en),
        .CASDOMUXEN_B(portB_en),
        .CASDOUTA({\NLW_genblk2[1].ram_reg_3_bram_2_CASDOUTA_UNCONNECTED [31:8],\genblk2[1].ram_reg_3_bram_2_n_28 ,\genblk2[1].ram_reg_3_bram_2_n_29 ,\genblk2[1].ram_reg_3_bram_2_n_30 ,\genblk2[1].ram_reg_3_bram_2_n_31 ,\genblk2[1].ram_reg_3_bram_2_n_32 ,\genblk2[1].ram_reg_3_bram_2_n_33 ,\genblk2[1].ram_reg_3_bram_2_n_34 ,\genblk2[1].ram_reg_3_bram_2_n_35 }),
        .CASDOUTB({\NLW_genblk2[1].ram_reg_3_bram_2_CASDOUTB_UNCONNECTED [31:8],\genblk2[1].ram_reg_3_bram_2_n_60 ,\genblk2[1].ram_reg_3_bram_2_n_61 ,\genblk2[1].ram_reg_3_bram_2_n_62 ,\genblk2[1].ram_reg_3_bram_2_n_63 ,\genblk2[1].ram_reg_3_bram_2_n_64 ,\genblk2[1].ram_reg_3_bram_2_n_65 ,\genblk2[1].ram_reg_3_bram_2_n_66 ,\genblk2[1].ram_reg_3_bram_2_n_67 }),
        .CASDOUTPA({\genblk2[1].ram_reg_3_bram_2_n_132 ,\genblk2[1].ram_reg_3_bram_2_n_133 ,\genblk2[1].ram_reg_3_bram_2_n_134 ,\genblk2[1].ram_reg_3_bram_2_n_135 }),
        .CASDOUTPB({\genblk2[1].ram_reg_3_bram_2_n_136 ,\genblk2[1].ram_reg_3_bram_2_n_137 ,\genblk2[1].ram_reg_3_bram_2_n_138 ,\genblk2[1].ram_reg_3_bram_2_n_139 }),
        .CASINDBITERR(\genblk2[1].ram_reg_3_bram_1_n_0 ),
        .CASINSBITERR(\genblk2[1].ram_reg_3_bram_1_n_1 ),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b1),
        .CASOREGIMUXEN_B(1'b1),
        .CASOUTDBITERR(\genblk2[1].ram_reg_3_bram_2_n_0 ),
        .CASOUTSBITERR(\genblk2[1].ram_reg_3_bram_2_n_1 ),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DBITERR(\NLW_genblk2[1].ram_reg_3_bram_2_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,p_1_in[31:24]}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,p_2_in[31:24]}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT(\NLW_genblk2[1].ram_reg_3_bram_2_DOUTADOUT_UNCONNECTED [31:0]),
        .DOUTBDOUT(\NLW_genblk2[1].ram_reg_3_bram_2_DOUTBDOUT_UNCONNECTED [31:0]),
        .DOUTPADOUTP(\NLW_genblk2[1].ram_reg_3_bram_2_DOUTPADOUTP_UNCONNECTED [3:0]),
        .DOUTPBDOUTP(\NLW_genblk2[1].ram_reg_3_bram_2_DOUTPBDOUTP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_genblk2[1].ram_reg_3_bram_2_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b1),
        .ENARDEN(\genblk2[1].ram_reg_0_bram_2_i_3_n_0 ),
        .ENBWREN(\genblk2[1].ram_reg_0_bram_2_i_4_n_0 ),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_genblk2[1].ram_reg_3_bram_2_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_genblk2[1].ram_reg_3_bram_2_SBITERR_UNCONNECTED ),
        .SLEEP(1'b0),
        .WEA({\genblk2[1].ram_reg_3_bram_2_i_1_n_0 ,\genblk2[1].ram_reg_3_bram_2_i_1_n_0 ,\genblk2[1].ram_reg_3_bram_2_i_1_n_0 ,\genblk2[1].ram_reg_3_bram_2_i_1_n_0 }),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_3_bram_2_i_2_n_0 ,\genblk2[1].ram_reg_3_bram_2_i_2_n_0 ,\genblk2[1].ram_reg_3_bram_2_i_2_n_0 ,\genblk2[1].ram_reg_3_bram_2_i_2_n_0 }));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'h20)) 
    \genblk2[1].ram_reg_3_bram_2_i_1 
       (.I0(portA_be[3]),
        .I1(portA_addr[12]),
        .I2(portA_addr[13]),
        .O(\genblk2[1].ram_reg_3_bram_2_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'h20)) 
    \genblk2[1].ram_reg_3_bram_2_i_2 
       (.I0(portB_be[3]),
        .I1(portB_addr[12]),
        .I2(portB_addr[13]),
        .O(\genblk2[1].ram_reg_3_bram_2_i_2_n_0 ));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-15 {cell *THIS*} {string {address width (14) is more than optimal threshold of 12. Implementing using BWWE will require more logic and timing would be suboptimal. Please use attribute ram_decomp = power if BWWE is desired.}}} {SYNTH-6 {cell *THIS*}} {SYNTH-7 {cell *THIS*}}" *) 
  (* RDADDR_COLLISION_HWCONFIG = "DELAYED_WRITE" *) 
  (* RTL_RAM_BITS = "524288" *) 
  (* RTL_RAM_NAME = "genblk2[1].ram" *) 
  (* RTL_RAM_TYPE = "RAM_TDP" *) 
  (* ram_addr_begin = "12288" *) 
  (* ram_addr_end = "16383" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "24" *) 
  (* ram_slice_end = "31" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("LAST"),
    .CASCADE_ORDER_B("LAST"),
    .CLOCK_DOMAINS("COMMON"),
    .DOA_REG(0),
    .DOB_REG(0),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \genblk2[1].ram_reg_3_bram_3 
       (.ADDRARDADDR({portA_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({portB_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_3_bram_2_n_28 ,\genblk2[1].ram_reg_3_bram_2_n_29 ,\genblk2[1].ram_reg_3_bram_2_n_30 ,\genblk2[1].ram_reg_3_bram_2_n_31 ,\genblk2[1].ram_reg_3_bram_2_n_32 ,\genblk2[1].ram_reg_3_bram_2_n_33 ,\genblk2[1].ram_reg_3_bram_2_n_34 ,\genblk2[1].ram_reg_3_bram_2_n_35 }),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_3_bram_2_n_60 ,\genblk2[1].ram_reg_3_bram_2_n_61 ,\genblk2[1].ram_reg_3_bram_2_n_62 ,\genblk2[1].ram_reg_3_bram_2_n_63 ,\genblk2[1].ram_reg_3_bram_2_n_64 ,\genblk2[1].ram_reg_3_bram_2_n_65 ,\genblk2[1].ram_reg_3_bram_2_n_66 ,\genblk2[1].ram_reg_3_bram_2_n_67 }),
        .CASDINPA({\genblk2[1].ram_reg_3_bram_2_n_132 ,\genblk2[1].ram_reg_3_bram_2_n_133 ,\genblk2[1].ram_reg_3_bram_2_n_134 ,\genblk2[1].ram_reg_3_bram_2_n_135 }),
        .CASDINPB({\genblk2[1].ram_reg_3_bram_2_n_136 ,\genblk2[1].ram_reg_3_bram_2_n_137 ,\genblk2[1].ram_reg_3_bram_2_n_138 ,\genblk2[1].ram_reg_3_bram_2_n_139 }),
        .CASDOMUXA(\genblk2[1].ram_reg_3_bram_3_i_1_n_0 ),
        .CASDOMUXB(\genblk2[1].ram_reg_3_bram_3_i_2_n_0 ),
        .CASDOMUXEN_A(portA_en),
        .CASDOMUXEN_B(portB_en),
        .CASDOUTA(\NLW_genblk2[1].ram_reg_3_bram_3_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_genblk2[1].ram_reg_3_bram_3_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_genblk2[1].ram_reg_3_bram_3_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_genblk2[1].ram_reg_3_bram_3_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(\genblk2[1].ram_reg_3_bram_2_n_0 ),
        .CASINSBITERR(\genblk2[1].ram_reg_3_bram_2_n_1 ),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b1),
        .CASOREGIMUXEN_B(1'b1),
        .CASOUTDBITERR(\NLW_genblk2[1].ram_reg_3_bram_3_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_genblk2[1].ram_reg_3_bram_3_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DBITERR(\NLW_genblk2[1].ram_reg_3_bram_3_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,p_1_in[31:24]}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,p_2_in[31:24]}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_genblk2[1].ram_reg_3_bram_3_DOUTADOUT_UNCONNECTED [31:8],portA_data_out[31:24]}),
        .DOUTBDOUT({\NLW_genblk2[1].ram_reg_3_bram_3_DOUTBDOUT_UNCONNECTED [31:8],portB_data_out[31:24]}),
        .DOUTPADOUTP(\NLW_genblk2[1].ram_reg_3_bram_3_DOUTPADOUTP_UNCONNECTED [3:0]),
        .DOUTPBDOUTP(\NLW_genblk2[1].ram_reg_3_bram_3_DOUTPBDOUTP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_genblk2[1].ram_reg_3_bram_3_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b1),
        .ENARDEN(\genblk2[1].ram_reg_3_bram_3_i_3_n_0 ),
        .ENBWREN(\genblk2[1].ram_reg_3_bram_3_i_4_n_0 ),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_genblk2[1].ram_reg_3_bram_3_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_genblk2[1].ram_reg_3_bram_3_SBITERR_UNCONNECTED ),
        .SLEEP(1'b0),
        .WEA({\genblk2[1].ram_reg_3_bram_3_i_21_n_0 ,\genblk2[1].ram_reg_3_bram_3_i_21_n_0 ,\genblk2[1].ram_reg_3_bram_3_i_21_n_0 ,\genblk2[1].ram_reg_3_bram_3_i_21_n_0 }),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,\genblk2[1].ram_reg_3_bram_3_i_22_n_0 ,\genblk2[1].ram_reg_3_bram_3_i_22_n_0 ,\genblk2[1].ram_reg_3_bram_3_i_22_n_0 ,\genblk2[1].ram_reg_3_bram_3_i_22_n_0 }));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \genblk2[1].ram_reg_3_bram_3_i_1 
       (.I0(portA_addr[13]),
        .I1(portA_addr[12]),
        .O(\genblk2[1].ram_reg_3_bram_3_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \genblk2[1].ram_reg_3_bram_3_i_10 
       (.I0(portA_en),
        .I1(portA_data_in[26]),
        .O(p_1_in[26]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \genblk2[1].ram_reg_3_bram_3_i_11 
       (.I0(portA_en),
        .I1(portA_data_in[25]),
        .O(p_1_in[25]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \genblk2[1].ram_reg_3_bram_3_i_12 
       (.I0(portA_en),
        .I1(portA_data_in[24]),
        .O(p_1_in[24]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \genblk2[1].ram_reg_3_bram_3_i_13 
       (.I0(portB_en),
        .I1(portB_data_in[31]),
        .O(p_2_in[31]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \genblk2[1].ram_reg_3_bram_3_i_14 
       (.I0(portB_en),
        .I1(portB_data_in[30]),
        .O(p_2_in[30]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \genblk2[1].ram_reg_3_bram_3_i_15 
       (.I0(portB_en),
        .I1(portB_data_in[29]),
        .O(p_2_in[29]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \genblk2[1].ram_reg_3_bram_3_i_16 
       (.I0(portB_en),
        .I1(portB_data_in[28]),
        .O(p_2_in[28]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \genblk2[1].ram_reg_3_bram_3_i_17 
       (.I0(portB_en),
        .I1(portB_data_in[27]),
        .O(p_2_in[27]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \genblk2[1].ram_reg_3_bram_3_i_18 
       (.I0(portB_en),
        .I1(portB_data_in[26]),
        .O(p_2_in[26]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \genblk2[1].ram_reg_3_bram_3_i_19 
       (.I0(portB_en),
        .I1(portB_data_in[25]),
        .O(p_2_in[25]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \genblk2[1].ram_reg_3_bram_3_i_2 
       (.I0(portB_addr[13]),
        .I1(portB_addr[12]),
        .O(\genblk2[1].ram_reg_3_bram_3_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \genblk2[1].ram_reg_3_bram_3_i_20 
       (.I0(portB_en),
        .I1(portB_data_in[24]),
        .O(p_2_in[24]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \genblk2[1].ram_reg_3_bram_3_i_21 
       (.I0(portA_be[3]),
        .I1(portA_addr[13]),
        .I2(portA_addr[12]),
        .O(\genblk2[1].ram_reg_3_bram_3_i_21_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \genblk2[1].ram_reg_3_bram_3_i_22 
       (.I0(portB_be[3]),
        .I1(portB_addr[13]),
        .I2(portB_addr[12]),
        .O(\genblk2[1].ram_reg_3_bram_3_i_22_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \genblk2[1].ram_reg_3_bram_3_i_3 
       (.I0(portA_en),
        .I1(portA_addr[13]),
        .I2(portA_addr[12]),
        .O(\genblk2[1].ram_reg_3_bram_3_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \genblk2[1].ram_reg_3_bram_3_i_4 
       (.I0(portB_en),
        .I1(portB_addr[13]),
        .I2(portB_addr[12]),
        .O(\genblk2[1].ram_reg_3_bram_3_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \genblk2[1].ram_reg_3_bram_3_i_5 
       (.I0(portA_en),
        .I1(portA_data_in[31]),
        .O(p_1_in[31]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \genblk2[1].ram_reg_3_bram_3_i_6 
       (.I0(portA_en),
        .I1(portA_data_in[30]),
        .O(p_1_in[30]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \genblk2[1].ram_reg_3_bram_3_i_7 
       (.I0(portA_en),
        .I1(portA_data_in[29]),
        .O(p_1_in[29]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \genblk2[1].ram_reg_3_bram_3_i_8 
       (.I0(portA_en),
        .I1(portA_data_in[28]),
        .O(p_1_in[28]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \genblk2[1].ram_reg_3_bram_3_i_9 
       (.I0(portA_en),
        .I1(portA_data_in[27]),
        .O(p_1_in[27]));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
