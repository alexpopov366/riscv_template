// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Sun Aug 28 18:14:55 2022
// Host        : dl580 running 64-bit Ubuntu 18.04.6 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ gpn_bundle_block_local_rom_0_0_sim_netlist.v
// Design      : gpn_bundle_block_local_rom_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcu200-fsgd2104-2-e
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_byte_en_ROM
   (porta_data_out,
    porta_addr,
    porta_en,
    clk);
  output [24:0]porta_data_out;
  input [4:0]porta_addr;
  input porta_en;
  input clk;

  wire clk;
  wire [4:0]porta_addr;
  wire [24:0]porta_data_out;
  wire porta_en;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xilinx_byte_enable_rom ram_block
       (.clk(clk),
        .porta_addr(porta_addr),
        .porta_data_out(porta_data_out),
        .porta_en(porta_en));
endmodule

(* CHECK_LICENSE_TYPE = "gpn_bundle_block_local_rom_0_0,local_rom,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* IP_DEFINITION_SOURCE = "package_project" *) 
(* X_CORE_INFO = "local_rom,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clk,
    rstn,
    porta_addr,
    porta_en,
    porta_be,
    porta_data_in,
    porta_data_out);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME clk, ASSOCIATED_RESET rstn, ASSOCIATED_BUSIF portA, FREQ_HZ 200000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN gpn_bundle_block_kernel_clk, INSERT_VIP 0" *) input clk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 rstn RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME rstn, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input rstn;
  (* X_INTERFACE_INFO = "user.org:interface:local_memory_interface:1.0 portA addr" *) input [29:0]porta_addr;
  (* X_INTERFACE_INFO = "user.org:interface:local_memory_interface:1.0 portA en" *) input porta_en;
  (* X_INTERFACE_INFO = "user.org:interface:local_memory_interface:1.0 portA be" *) input [3:0]porta_be;
  (* X_INTERFACE_INFO = "user.org:interface:local_memory_interface:1.0 portA data_in" *) input [31:0]porta_data_in;
  (* X_INTERFACE_INFO = "user.org:interface:local_memory_interface:1.0 portA data_out" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME portA, SV_INTERFACE true" *) output [31:0]porta_data_out;

  wire \<const0> ;
  wire clk;
  wire [29:0]porta_addr;
  wire [31:0]\^porta_data_out ;
  wire porta_en;

  assign porta_data_out[31] = \^porta_data_out [31];
  assign porta_data_out[30] = \^porta_data_out [28];
  assign porta_data_out[29:28] = \^porta_data_out [29:28];
  assign porta_data_out[27] = \^porta_data_out [28];
  assign porta_data_out[26] = \^porta_data_out [28];
  assign porta_data_out[25] = \^porta_data_out [25];
  assign porta_data_out[24] = \<const0> ;
  assign porta_data_out[23:20] = \^porta_data_out [23:20];
  assign porta_data_out[19] = \<const0> ;
  assign porta_data_out[18:4] = \^porta_data_out [18:4];
  assign porta_data_out[3] = \<const0> ;
  assign porta_data_out[2] = \^porta_data_out [2];
  assign porta_data_out[1] = \^porta_data_out [0];
  assign porta_data_out[0] = \^porta_data_out [0];
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_local_rom inst
       (.clk(clk),
        .porta_addr(porta_addr[4:0]),
        .porta_data_out({\^porta_data_out [31],\^porta_data_out [28],\^porta_data_out [29],\^porta_data_out [25],\^porta_data_out [23:20],\^porta_data_out [18:4],\^porta_data_out [2],\^porta_data_out [0]}),
        .porta_en(porta_en));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_local_rom
   (porta_data_out,
    porta_addr,
    porta_en,
    clk);
  output [24:0]porta_data_out;
  input [4:0]porta_addr;
  input porta_en;
  input clk;

  wire clk;
  wire [4:0]porta_addr;
  wire [24:0]porta_data_out;
  wire porta_en;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_byte_en_ROM inst_data_rom
       (.clk(clk),
        .porta_addr(porta_addr),
        .porta_data_out(porta_data_out),
        .porta_en(porta_en));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xilinx_byte_enable_rom
   (porta_data_out,
    porta_addr,
    porta_en,
    clk);
  output [24:0]porta_data_out;
  input [4:0]porta_addr;
  input porta_en;
  input clk;

  wire clk;
  wire \data_out_a[10]_i_1_n_0 ;
  wire \data_out_a[11]_i_1_n_0 ;
  wire \data_out_a[12]_i_1_n_0 ;
  wire \data_out_a[13]_i_1_n_0 ;
  wire \data_out_a[14]_i_1_n_0 ;
  wire \data_out_a[15]_i_1_n_0 ;
  wire \data_out_a[16]_i_1_n_0 ;
  wire \data_out_a[17]_i_1_n_0 ;
  wire \data_out_a[18]_i_1_n_0 ;
  wire \data_out_a[1]_i_1_n_0 ;
  wire \data_out_a[20]_i_1_n_0 ;
  wire \data_out_a[21]_i_1_n_0 ;
  wire \data_out_a[22]_i_1_n_0 ;
  wire \data_out_a[23]_i_1_n_0 ;
  wire \data_out_a[25]_i_1_n_0 ;
  wire \data_out_a[29]_i_1_n_0 ;
  wire \data_out_a[2]_i_1_n_0 ;
  wire \data_out_a[30]_i_1_n_0 ;
  wire \data_out_a[31]_i_1_n_0 ;
  wire \data_out_a[4]_i_1_n_0 ;
  wire \data_out_a[5]_i_1_n_0 ;
  wire \data_out_a[6]_i_1_n_0 ;
  wire \data_out_a[7]_i_1_n_0 ;
  wire \data_out_a[8]_i_1_n_0 ;
  wire \data_out_a[9]_i_1_n_0 ;
  wire [4:0]porta_addr;
  wire [24:0]porta_data_out;
  wire porta_en;

  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h37137767)) 
    \data_out_a[10]_i_1 
       (.I0(porta_addr[3]),
        .I1(porta_addr[4]),
        .I2(porta_addr[2]),
        .I3(porta_addr[0]),
        .I4(porta_addr[1]),
        .O(\data_out_a[10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT5 #(
    .INIT(32'h00400100)) 
    \data_out_a[11]_i_1 
       (.I0(porta_addr[3]),
        .I1(porta_addr[4]),
        .I2(porta_addr[2]),
        .I3(porta_addr[1]),
        .I4(porta_addr[0]),
        .O(\data_out_a[11]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h00002000)) 
    \data_out_a[12]_i_1 
       (.I0(porta_addr[0]),
        .I1(porta_addr[1]),
        .I2(porta_addr[2]),
        .I3(porta_addr[4]),
        .I4(porta_addr[3]),
        .O(\data_out_a[12]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'h22112450)) 
    \data_out_a[13]_i_1 
       (.I0(porta_addr[3]),
        .I1(porta_addr[4]),
        .I2(porta_addr[2]),
        .I3(porta_addr[1]),
        .I4(porta_addr[0]),
        .O(\data_out_a[13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT5 #(
    .INIT(32'h00008000)) 
    \data_out_a[14]_i_1 
       (.I0(porta_addr[3]),
        .I1(porta_addr[1]),
        .I2(porta_addr[0]),
        .I3(porta_addr[2]),
        .I4(porta_addr[4]),
        .O(\data_out_a[14]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h60600738)) 
    \data_out_a[15]_i_1 
       (.I0(porta_addr[3]),
        .I1(porta_addr[4]),
        .I2(porta_addr[2]),
        .I3(porta_addr[1]),
        .I4(porta_addr[0]),
        .O(\data_out_a[15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'h0C3D3F03)) 
    \data_out_a[16]_i_1 
       (.I0(porta_addr[0]),
        .I1(porta_addr[3]),
        .I2(porta_addr[4]),
        .I3(porta_addr[2]),
        .I4(porta_addr[1]),
        .O(\data_out_a[16]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'h27377571)) 
    \data_out_a[17]_i_1 
       (.I0(porta_addr[3]),
        .I1(porta_addr[4]),
        .I2(porta_addr[2]),
        .I3(porta_addr[0]),
        .I4(porta_addr[1]),
        .O(\data_out_a[17]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'h22713770)) 
    \data_out_a[18]_i_1 
       (.I0(porta_addr[3]),
        .I1(porta_addr[4]),
        .I2(porta_addr[2]),
        .I3(porta_addr[1]),
        .I4(porta_addr[0]),
        .O(\data_out_a[18]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h01FFFFFF)) 
    \data_out_a[1]_i_1 
       (.I0(porta_addr[0]),
        .I1(porta_addr[1]),
        .I2(porta_addr[2]),
        .I3(porta_addr[4]),
        .I4(porta_addr[3]),
        .O(\data_out_a[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'h02020004)) 
    \data_out_a[20]_i_1 
       (.I0(porta_addr[3]),
        .I1(porta_addr[4]),
        .I2(porta_addr[0]),
        .I3(porta_addr[1]),
        .I4(porta_addr[2]),
        .O(\data_out_a[20]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT5 #(
    .INIT(32'h00020600)) 
    \data_out_a[21]_i_1 
       (.I0(porta_addr[3]),
        .I1(porta_addr[4]),
        .I2(porta_addr[0]),
        .I3(porta_addr[1]),
        .I4(porta_addr[2]),
        .O(\data_out_a[21]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT5 #(
    .INIT(32'h22200400)) 
    \data_out_a[22]_i_1 
       (.I0(porta_addr[3]),
        .I1(porta_addr[4]),
        .I2(porta_addr[0]),
        .I3(porta_addr[1]),
        .I4(porta_addr[2]),
        .O(\data_out_a[22]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT5 #(
    .INIT(32'h22120600)) 
    \data_out_a[23]_i_1 
       (.I0(porta_addr[3]),
        .I1(porta_addr[4]),
        .I2(porta_addr[0]),
        .I3(porta_addr[1]),
        .I4(porta_addr[2]),
        .O(\data_out_a[23]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'h20401100)) 
    \data_out_a[25]_i_1 
       (.I0(porta_addr[3]),
        .I1(porta_addr[4]),
        .I2(porta_addr[2]),
        .I3(porta_addr[1]),
        .I4(porta_addr[0]),
        .O(\data_out_a[25]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'h24540013)) 
    \data_out_a[29]_i_1 
       (.I0(porta_addr[3]),
        .I1(porta_addr[4]),
        .I2(porta_addr[1]),
        .I3(porta_addr[2]),
        .I4(porta_addr[0]),
        .O(\data_out_a[29]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h4056400B)) 
    \data_out_a[2]_i_1 
       (.I0(porta_addr[3]),
        .I1(porta_addr[4]),
        .I2(porta_addr[1]),
        .I3(porta_addr[2]),
        .I4(porta_addr[0]),
        .O(\data_out_a[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h20400100)) 
    \data_out_a[30]_i_1 
       (.I0(porta_addr[3]),
        .I1(porta_addr[4]),
        .I2(porta_addr[2]),
        .I3(porta_addr[1]),
        .I4(porta_addr[0]),
        .O(\data_out_a[30]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT5 #(
    .INIT(32'h24564013)) 
    \data_out_a[31]_i_1 
       (.I0(porta_addr[3]),
        .I1(porta_addr[4]),
        .I2(porta_addr[1]),
        .I3(porta_addr[2]),
        .I4(porta_addr[0]),
        .O(\data_out_a[31]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'h15264227)) 
    \data_out_a[4]_i_1 
       (.I0(porta_addr[3]),
        .I1(porta_addr[4]),
        .I2(porta_addr[2]),
        .I3(porta_addr[1]),
        .I4(porta_addr[0]),
        .O(\data_out_a[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h393F344F)) 
    \data_out_a[5]_i_1 
       (.I0(porta_addr[2]),
        .I1(porta_addr[3]),
        .I2(porta_addr[4]),
        .I3(porta_addr[0]),
        .I4(porta_addr[1]),
        .O(\data_out_a[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h38000242)) 
    \data_out_a[6]_i_1 
       (.I0(porta_addr[1]),
        .I1(porta_addr[3]),
        .I2(porta_addr[4]),
        .I3(porta_addr[2]),
        .I4(porta_addr[0]),
        .O(\data_out_a[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'h73674140)) 
    \data_out_a[7]_i_1 
       (.I0(porta_addr[3]),
        .I1(porta_addr[4]),
        .I2(porta_addr[2]),
        .I3(porta_addr[1]),
        .I4(porta_addr[0]),
        .O(\data_out_a[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'h15022767)) 
    \data_out_a[8]_i_1 
       (.I0(porta_addr[3]),
        .I1(porta_addr[4]),
        .I2(porta_addr[2]),
        .I3(porta_addr[0]),
        .I4(porta_addr[1]),
        .O(\data_out_a[8]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h37037565)) 
    \data_out_a[9]_i_1 
       (.I0(porta_addr[3]),
        .I1(porta_addr[4]),
        .I2(porta_addr[2]),
        .I3(porta_addr[0]),
        .I4(porta_addr[1]),
        .O(\data_out_a[9]_i_1_n_0 ));
  FDRE \data_out_a_reg[10] 
       (.C(clk),
        .CE(porta_en),
        .D(\data_out_a[10]_i_1_n_0 ),
        .Q(porta_data_out[8]),
        .R(1'b0));
  FDRE \data_out_a_reg[11] 
       (.C(clk),
        .CE(porta_en),
        .D(\data_out_a[11]_i_1_n_0 ),
        .Q(porta_data_out[9]),
        .R(1'b0));
  FDRE \data_out_a_reg[12] 
       (.C(clk),
        .CE(porta_en),
        .D(\data_out_a[12]_i_1_n_0 ),
        .Q(porta_data_out[10]),
        .R(1'b0));
  FDRE \data_out_a_reg[13] 
       (.C(clk),
        .CE(porta_en),
        .D(\data_out_a[13]_i_1_n_0 ),
        .Q(porta_data_out[11]),
        .R(1'b0));
  FDRE \data_out_a_reg[14] 
       (.C(clk),
        .CE(porta_en),
        .D(\data_out_a[14]_i_1_n_0 ),
        .Q(porta_data_out[12]),
        .R(1'b0));
  FDRE \data_out_a_reg[15] 
       (.C(clk),
        .CE(porta_en),
        .D(\data_out_a[15]_i_1_n_0 ),
        .Q(porta_data_out[13]),
        .R(1'b0));
  FDRE \data_out_a_reg[16] 
       (.C(clk),
        .CE(porta_en),
        .D(\data_out_a[16]_i_1_n_0 ),
        .Q(porta_data_out[14]),
        .R(1'b0));
  FDRE \data_out_a_reg[17] 
       (.C(clk),
        .CE(porta_en),
        .D(\data_out_a[17]_i_1_n_0 ),
        .Q(porta_data_out[15]),
        .R(1'b0));
  FDRE \data_out_a_reg[18] 
       (.C(clk),
        .CE(porta_en),
        .D(\data_out_a[18]_i_1_n_0 ),
        .Q(porta_data_out[16]),
        .R(1'b0));
  FDRE \data_out_a_reg[1] 
       (.C(clk),
        .CE(porta_en),
        .D(\data_out_a[1]_i_1_n_0 ),
        .Q(porta_data_out[0]),
        .R(1'b0));
  FDRE \data_out_a_reg[20] 
       (.C(clk),
        .CE(porta_en),
        .D(\data_out_a[20]_i_1_n_0 ),
        .Q(porta_data_out[17]),
        .R(1'b0));
  FDRE \data_out_a_reg[21] 
       (.C(clk),
        .CE(porta_en),
        .D(\data_out_a[21]_i_1_n_0 ),
        .Q(porta_data_out[18]),
        .R(1'b0));
  FDRE \data_out_a_reg[22] 
       (.C(clk),
        .CE(porta_en),
        .D(\data_out_a[22]_i_1_n_0 ),
        .Q(porta_data_out[19]),
        .R(1'b0));
  FDRE \data_out_a_reg[23] 
       (.C(clk),
        .CE(porta_en),
        .D(\data_out_a[23]_i_1_n_0 ),
        .Q(porta_data_out[20]),
        .R(1'b0));
  FDRE \data_out_a_reg[25] 
       (.C(clk),
        .CE(porta_en),
        .D(\data_out_a[25]_i_1_n_0 ),
        .Q(porta_data_out[21]),
        .R(1'b0));
  FDRE \data_out_a_reg[29] 
       (.C(clk),
        .CE(porta_en),
        .D(\data_out_a[29]_i_1_n_0 ),
        .Q(porta_data_out[22]),
        .R(1'b0));
  FDRE \data_out_a_reg[2] 
       (.C(clk),
        .CE(porta_en),
        .D(\data_out_a[2]_i_1_n_0 ),
        .Q(porta_data_out[1]),
        .R(1'b0));
  FDRE \data_out_a_reg[30] 
       (.C(clk),
        .CE(porta_en),
        .D(\data_out_a[30]_i_1_n_0 ),
        .Q(porta_data_out[23]),
        .R(1'b0));
  FDRE \data_out_a_reg[31] 
       (.C(clk),
        .CE(porta_en),
        .D(\data_out_a[31]_i_1_n_0 ),
        .Q(porta_data_out[24]),
        .R(1'b0));
  FDRE \data_out_a_reg[4] 
       (.C(clk),
        .CE(porta_en),
        .D(\data_out_a[4]_i_1_n_0 ),
        .Q(porta_data_out[2]),
        .R(1'b0));
  FDRE \data_out_a_reg[5] 
       (.C(clk),
        .CE(porta_en),
        .D(\data_out_a[5]_i_1_n_0 ),
        .Q(porta_data_out[3]),
        .R(1'b0));
  FDRE \data_out_a_reg[6] 
       (.C(clk),
        .CE(porta_en),
        .D(\data_out_a[6]_i_1_n_0 ),
        .Q(porta_data_out[4]),
        .R(1'b0));
  FDRE \data_out_a_reg[7] 
       (.C(clk),
        .CE(porta_en),
        .D(\data_out_a[7]_i_1_n_0 ),
        .Q(porta_data_out[5]),
        .R(1'b0));
  FDRE \data_out_a_reg[8] 
       (.C(clk),
        .CE(porta_en),
        .D(\data_out_a[8]_i_1_n_0 ),
        .Q(porta_data_out[6]),
        .R(1'b0));
  FDRE \data_out_a_reg[9] 
       (.C(clk),
        .CE(porta_en),
        .D(\data_out_a[9]_i_1_n_0 ),
        .Q(porta_data_out[7]),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
