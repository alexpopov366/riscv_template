/*
 * Copyright © 2017 Eric Matthews,  Lesley Shannon
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Initial code developed under the supervision of Dr. Lesley Shannon,
 * Reconfigurable Computing Lab, Simon Fraser University.
 *
 * Author(s):
 *             Eric Matthews <ematthew@sfu.ca>
 */

import taiga_config::*;
import riscv_types::*;
import taiga_types::*;

module axi_master
        (
        input logic clk,
        input logic rst,

        axi_interface.master m_axi,
        input logic [2:0] size,
        output logic[31:0] data_out,

        input data_access_shared_inputs_t ls_inputs,
        ls_sub_unit_interface.sub_unit ls

        );
        
        
        
    /*   
  	//New AXI version


  
    logic ready;
    logic wnext;
    logic wbusy;
    logic do_single_burst_write;
    logic start_single_burst_write;
    logic stop_single_burst_write;
    logic wait_write_resp;
    logic rnext;
    logic rbusy;
    logic do_single_burst_read;
    logic start_single_burst_read;
    logic stop_single_burst_read;

    
    
    
    //assign constants
  

    //read constants
    assign m_axi.arlen = 0; // 1 request
    assign m_axi.arburst = 0;// burst type does not matter
    assign m_axi.arid = 0;
	assign m_axi.arlock = 0;
	assign m_axi.arprot = 0;
	assign m_axi.arqos = 0;
	assign m_axi.aruser = 0;
    assign m_axi.arlen = 0;
    //assign m_axi.arsize = 0;
    assign m_axi.arburst = 0;
    assign m_axi.arcache = 4'b0010;
  

    //write constants
    assign m_axi.awlen = 0;
    assign m_axi.awburst = 0;
    assign m_axi.awid = 0;
	assign m_axi.awlock = 0;
	assign m_axi.awprot = 0;
	assign m_axi.awqos = 0;
	assign m_axi.awuser = 0; 
	assign m_axi.awcache = 4'b0010; 
	//assign m_axi.awsize = 0;
	assign m_axi.wuser = 0;
	assign m_axi.wid = 0;

	//--------------------
	//Write Data Channel
	//--------------------

	assign wnext = m_axi.wready & m_axi.wvalid;
	assign wbusy = do_single_burst_write | m_axi.awvalid ; 
	assign start_single_burst_write = ls.new_request & ls_inputs.store;
	assign stop_single_burst_write = do_single_burst_write && wnext && m_axi.wlast;

    always @ (posedge clk) begin
        if ( rst ) begin
            m_axi.araddr <= 0;
            m_axi.arsize <= 0;
            m_axi.awsize <= 0;
            m_axi.awaddr <= 0;
            m_axi.wdata <= 0;
            m_axi.wstrb  <= 0;
        end
        else if (ls.new_request) begin
            m_axi.araddr <= ls_inputs.addr;
            m_axi.arsize <= size;
            m_axi.awsize <= size;
            m_axi.awaddr <= ls_inputs.addr;
            m_axi.wdata <= ls_inputs.data_in;
            m_axi.wstrb  <= ls_inputs.be;
        end
    end

    always @(posedge clk)                                   
	  begin                                                                
	                                                                       
	    if ( rst )                                           
	        m_axi.awvalid <= 1'b0;                                           
	    // If previously not valid , start next transaction                
	    else if ( ~m_axi.awvalid && start_single_burst_write )                 
	        m_axi.awvalid <= 1'b1;                                           
	    else if ( m_axi.awready && m_axi.awvalid )                             
	        m_axi.awvalid <= 1'b0;                                           
	    else                                                               
	      m_axi.awvalid <= m_axi.awvalid;                                      
	  end                 
	     	  
	//Start write burst transaction
	//Generate a pulse to initiate AXI transaction.
	always @(posedge clk)										      
	  begin                                                                        
	    // Initiates AXI transaction delay    
	    if ( rst || stop_single_burst_write )                                                   
	       do_single_burst_write <= 1'b0;                                                                  
	    else if (start_single_burst_write && ~do_single_burst_write) 
	           do_single_burst_write <= 1'b1;
	  end     
	                                                                                    
	// WVALID logic, similar to the axi_awvalid always block above                      
	  always @(posedge clk)                                                      
	  begin                                                                             
	    if ( rst || start_single_burst_write || wait_write_resp)                                                        
	        m_axi.wvalid <= 1'b0;                                                         
	    else if (~m_axi.wvalid && do_single_burst_write && ~wait_write_resp)                               
	        m_axi.wvalid <= 1'b1;                                                         
	    else if (wnext && m_axi.wlast)                                                    
	      m_axi.wvalid <= 1'b0;                                                           
	    else                                                                            
	      m_axi.wvalid <= m_axi.wvalid;                                                     
	  end                                                                               
	                                                                                    
	                                                                                    
      assign m_axi.wlast =  m_axi.wvalid; //Due to the burst length = 0
	  
	  always @(posedge clk)                                     
	  begin                                                                 
	    if ( rst )                                            
	        m_axi.bready <= 1'b0;                                             
	    // accept/acknowledge bresp with axi_bready by the master           
	    // when M_AXI_BVALID is asserted by slave                           
	    else if ( stop_single_burst_write )                               
	        m_axi.bready <= 1'b1;                                             
	    // deassert after one clock cycle                                   
	    else if (m_axi.bready && m_axi.bvalid)                                                
	        m_axi.bready <= 1'b0;                                             
	    // retain the previous value                                        
	    else                                                                
	      m_axi.bready <= m_axi.bready;                                         
	  end  

	  assign wait_write_resp = m_axi.bready;                                                                  

  

	//--------------------------------
	//Read Data (and Response) Channel
	//--------------------------------

	// Forward movement occurs when the channel is valid and ready   
	assign rnext = m_axi.rvalid & m_axi.rready;  
	assign rbusy = do_single_burst_read | m_axi.arvalid; 
	assign start_single_burst_read = ls.new_request & ls_inputs.load;
	assign stop_single_burst_read = do_single_burst_read && rnext && m_axi.rlast;

	//The Read Address Channel (AW) provides a similar function to the
	//Write Address channel- to provide the tranfer qualifiers for the burst.

    always @(posedge clk)                                   
	  begin                                                                
	                                                                       
	    if ( rst )                                           
	        m_axi.arvalid <= 1'b0;                                           
	    // If previously not valid , start next transaction                
	    else if ( ~m_axi.arvalid && start_single_burst_read )                 
	        m_axi.arvalid <= 1'b1;                                           
	    else if ( m_axi.arready && m_axi.arvalid )                         
	        m_axi.arvalid <= 1'b0;                                           
	    else                                                               
	        m_axi.arvalid <= m_axi.arvalid;                                      
	  end                 
	    	  
	//Start read burst transaction
	//Generate a pulse to initiate AXI transaction.
	always @(posedge clk)										      
	  begin                                                                        
	    // Initiates AXI transaction delay    
	    if ( rst || stop_single_burst_read )                                                   
	        do_single_burst_read <= 1'b0;                                                                  
	    else if ( start_single_burst_read && ~do_single_burst_read ) 
	        do_single_burst_read <= 1'b1;
	  end     
	                                                                                    

	//The Read Data channel returns the results of the read request          
	assign 	m_axi.rready = 1'b1;                 

	//Store data ready flag.
	always @(posedge clk)										      
	  begin                                                                        
	    if ( rst )                                                   
    	      ls.data_valid <= 0;                                                                
	    else                                                                      
    	      ls.data_valid <= stop_single_burst_read;
	  end 

	//Store data out.
	always @(posedge clk)										      
	  begin                                                                        
	    if ( rst )                                                   
    	      data_out <= 0;                                                                
	    else if (stop_single_burst_read)                                                                     
    	      data_out <= m_axi.rdata;
	  end 

      assign ls.ready = ~rbusy & ~wbusy;
 	*/
	
	
	
	
	
	//Native AXI version
	
    logic ready;


    //assign constants
  

    //read constants
    assign m_axi.arlen = 0; // 1 request
    assign m_axi.arburst = 0;// burst type does not matter
    assign m_axi.rready = 1; //always ready to receive data
    assign m_axi.arid = 0;
	assign m_axi.arlock = 0;
	assign m_axi.arprot = 0;
	assign m_axi.arqos = 0;
	assign m_axi.aruser = 0;
    assign m_axi.arlen = 0;
    //assign m_axi.arsize = 0;
    assign m_axi.arburst = 0;
    assign m_axi.arcache = 4'b0010;
  
    

    always_ff @ (posedge clk) begin
        if (ls.new_request) begin
            m_axi.araddr <= ls_inputs.addr;
            m_axi.arsize <= size;
            m_axi.awsize <= size;
            m_axi.awaddr <= ls_inputs.addr;
            m_axi.wdata <= ls_inputs.data_in;
            m_axi.wstrb  <= ls_inputs.be;
        end
    end

    //write constants
    assign m_axi.awlen = 0;
    assign m_axi.awburst = 0;
    assign m_axi.bready = 1;
    assign m_axi.awid = 0;
	assign m_axi.awlock = 0;
	assign m_axi.awprot = 0;
	assign m_axi.awqos = 0;
	assign m_axi.awuser = 0; 
	assign m_axi.awcache = 4'b0010; 
	//assign m_axi.awsize = 0;
	assign m_axi.wuser = 0;
	assign m_axi.wid = 0;

    set_clr_reg_with_rst #(.SET_OVER_CLR(0), .WIDTH(1), .RST_VALUE(1)) ready_m (
      .clk, .rst,
      .set(m_axi.rvalid | m_axi.bvalid),
      .clr(ls.new_request),
      .result(ready)
    );
    assign ls.ready = ready;

    always_ff @ (posedge clk) begin
        if (rst)
            ls.data_valid <= 0;
        else
            ls.data_valid <= m_axi.rvalid;
    end

    //read channel
    set_clr_reg_with_rst #(.SET_OVER_CLR(1), .WIDTH(1), .RST_VALUE(0)) arvalid_m (
      .clk, .rst,
      .set(ls.new_request & ls_inputs.load),
      .clr(m_axi.arready),
      .result(m_axi.arvalid)
    );

    always_ff @ (posedge clk) begin
        if (m_axi.rvalid)
            data_out <= m_axi.rdata;
    end

    //write channel
    set_clr_reg_with_rst #(.SET_OVER_CLR(1), .WIDTH(1), .RST_VALUE(0)) awvalid_m (
      .clk, .rst,
      .set(ls.new_request & ls_inputs.store),
      .clr(m_axi.awready),
      .result(m_axi.awvalid)
    );

    set_clr_reg_with_rst #(.SET_OVER_CLR(1), .WIDTH(1), .RST_VALUE(0)) wvalid_m (
      .clk, .rst,
      .set(ls.new_request & ls_inputs.store),
      .clr(m_axi.wready),
      .result(m_axi.wvalid)
    );
    assign  m_axi.wlast = m_axi.wvalid;
    
endmodule
