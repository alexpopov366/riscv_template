onbreak {quit -f}
onerror {quit -f}

vsim -lib xil_defaultlib gpn_bundle_block_opt

do {wave.do}

view wave
view structure
view signals

do {gpn_bundle_block.udo}

run -all

quit -force
