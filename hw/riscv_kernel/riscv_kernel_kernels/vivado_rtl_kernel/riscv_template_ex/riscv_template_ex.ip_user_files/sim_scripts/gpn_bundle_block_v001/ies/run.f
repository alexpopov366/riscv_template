-makelib ies_lib/xilinx_vip -sv \
  "/data/Xilinx/Vivado/2020.2/data/xilinx_vip/hdl/axi4stream_vip_axi4streampc.sv" \
  "/data/Xilinx/Vivado/2020.2/data/xilinx_vip/hdl/axi_vip_axi4pc.sv" \
  "/data/Xilinx/Vivado/2020.2/data/xilinx_vip/hdl/xil_common_vip_pkg.sv" \
  "/data/Xilinx/Vivado/2020.2/data/xilinx_vip/hdl/axi4stream_vip_pkg.sv" \
  "/data/Xilinx/Vivado/2020.2/data/xilinx_vip/hdl/axi_vip_pkg.sv" \
  "/data/Xilinx/Vivado/2020.2/data/xilinx_vip/hdl/axi4stream_vip_if.sv" \
  "/data/Xilinx/Vivado/2020.2/data/xilinx_vip/hdl/axi_vip_if.sv" \
  "/data/Xilinx/Vivado/2020.2/data/xilinx_vip/hdl/clk_vip_if.sv" \
  "/data/Xilinx/Vivado/2020.2/data/xilinx_vip/hdl/rst_vip_if.sv" \
-endlib
-makelib ies_lib/xpm -sv \
  "/data/Xilinx/Vivado/2020.2/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
  "/data/Xilinx/Vivado/2020.2/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \
  "/data/Xilinx/Vivado/2020.2/data/ip/xpm/xpm_fifo/hdl/xpm_fifo.sv" \
-endlib
-makelib ies_lib/xpm \
  "/data/Xilinx/Vivado/2020.2/data/ip/xpm/xpm_VCOMP.vhd" \
-endlib
-makelib ies_lib/axi_infrastructure_v1_1_0 \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block_v001/ipshared/ec67/hdl/axi_infrastructure_v1_1_vl_rfs.v" \
-endlib
-makelib ies_lib/fifo_generator_v13_2_5 \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block_v001/ipshared/276e/simulation/fifo_generator_vlog_beh.v" \
-endlib
-makelib ies_lib/fifo_generator_v13_2_5 \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block_v001/ipshared/276e/hdl/fifo_generator_v13_2_rfs.vhd" \
-endlib
-makelib ies_lib/fifo_generator_v13_2_5 \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block_v001/ipshared/276e/hdl/fifo_generator_v13_2_rfs.v" \
-endlib
-makelib ies_lib/axi_clock_converter_v2_1_21 \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block_v001/ipshared/1304/hdl/axi_clock_converter_v2_1_vl_rfs.v" \
-endlib
-makelib ies_lib/xil_defaultlib \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block_v001/ip/gpn_bundle_block_auto_cc_0/sim/gpn_bundle_block_auto_cc_0.v" \
-endlib
-makelib ies_lib/axi_lite_ipif_v3_0_4 \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/66ea/hdl/axi_lite_ipif_v3_0_vh_rfs.vhd" \
-endlib
-makelib ies_lib/lib_cdc_v1_0_2 \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/ef1e/hdl/lib_cdc_v1_0_rfs.vhd" \
-endlib
-makelib ies_lib/interrupt_control_v3_1_4 \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/a040/hdl/interrupt_control_v3_1_vh_rfs.vhd" \
-endlib
-makelib ies_lib/axi_gpio_v2_0_24 \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/4318/hdl/axi_gpio_v2_0_vh_rfs.vhd" \
-endlib
-makelib ies_lib/xil_defaultlib \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block_v001/ip/gpn_bundle_block_axi_gpio_0_0/sim/gpn_bundle_block_axi_gpio_0_0.vhd" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block_v001/ip/gpn_bundle_block_axi_gpio_1_0/sim/gpn_bundle_block_axi_gpio_1_0.vhd" \
-endlib
-makelib ies_lib/generic_baseblocks_v2_1_0 \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/b752/hdl/generic_baseblocks_v2_1_vl_rfs.v" \
-endlib
-makelib ies_lib/axi_register_slice_v2_1_22 \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/af2c/hdl/axi_register_slice_v2_1_vl_rfs.v" \
-endlib
-makelib ies_lib/axi_data_fifo_v2_1_21 \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/54c0/hdl/axi_data_fifo_v2_1_vl_rfs.v" \
-endlib
-makelib ies_lib/axi_crossbar_v2_1_23 \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/bc0a/hdl/axi_crossbar_v2_1_vl_rfs.v" \
-endlib
-makelib ies_lib/xil_defaultlib \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block_v001/ip/gpn_bundle_block_xbar_0/sim/gpn_bundle_block_xbar_0.v" \
-endlib
-makelib ies_lib/axi_protocol_converter_v2_1_22 \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block_v001/ipshared/5cee/hdl/axi_protocol_converter_v2_1_vl_rfs.v" \
-endlib
-makelib ies_lib/xil_defaultlib \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block_v001/ip/gpn_bundle_block_auto_pc_0/sim/gpn_bundle_block_auto_pc_0.v" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block_v001/ip/gpn_bundle_block_auto_pc_1/sim/gpn_bundle_block_auto_pc_1.v" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block_v001/ip/gpn_bundle_block_auto_pc_2/sim/gpn_bundle_block_auto_pc_2.v" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block_v001/ip/gpn_bundle_block_auto_pc_3/sim/gpn_bundle_block_auto_pc_3.v" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block_v001/ip/gpn_bundle_block_auto_pc_4/sim/gpn_bundle_block_auto_pc_4.v" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/300a/src/xilinx_byte_enable_ultraram.v" \
-endlib
-makelib ies_lib/xil_defaultlib -sv \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/300a/local_memory_IP.srcs/sources_1/imports/core/taiga_config.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/300a/local_memory_IP.srcs/sources_1/imports/core/riscv_types.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/300a/local_memory_IP.srcs/sources_1/imports/core/taiga_types.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/300a/local_memory_IP.srcs/sources_1/imports/core/byte_en_BRAM.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/300a/src/xilinx_byte_enable_ram.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/300a/local_memory_IP.srcs/sources_1/imports/local_memory/local_mem.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block_v001/ip/gpn_bundle_block_local_mem_0_0/sim/gpn_bundle_block_local_mem_0_0.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/34a1/local_rom_IP.srcs/sources_1/imports/core/taiga_config.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/34a1/local_rom_IP.srcs/sources_1/imports/core/byte_en_BRAM.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/34a1/src/local_memory_interface.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/34a1/src/xilinx_byte_enable_ram.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/34a1/local_rom_IP.srcs/sources_1/imports/src/local_rom.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block_v001/ip/gpn_bundle_block_local_rom_0_0/sim/gpn_bundle_block_local_rom_0_0.sv" \
-endlib
-makelib ies_lib/proc_sys_reset_v5_0_13 \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/8842/hdl/proc_sys_reset_v5_0_vh_rfs.vhd" \
-endlib
-makelib ies_lib/xil_defaultlib \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block_v001/ip/gpn_bundle_block_proc_sys_reset_0_0/sim/gpn_bundle_block_proc_sys_reset_0_0.vhd" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block_v001/ip/gpn_bundle_block_axi_gpio_2_0/sim/gpn_bundle_block_axi_gpio_2_0.vhd" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block_v001/ip/gpn_bundle_block_axi_gpio_2_1/sim/gpn_bundle_block_axi_gpio_2_1.vhd" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block_v001/ip/gpn_bundle_block_axi_gpio_2_2/sim/gpn_bundle_block_axi_gpio_2_2.vhd" \
-endlib
-makelib ies_lib/xil_defaultlib \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block_v001/ip/gpn_bundle_block_auto_cc_1/sim/gpn_bundle_block_auto_cc_1.v" \
-endlib
-makelib ies_lib/xil_defaultlib -sv \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/taiga_config.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/alu_unit.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/amo_alu.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/avalon_master.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/axi_master.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/barrel_shifter.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/branch_comparator.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/branch_predictor.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/branch_predictor_ram.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/branch_unit.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/byte_en_BRAM.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/clz.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/csr_types.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/csr_regs.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/cycler.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/dbram.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/dcache.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/ddata_bank.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/decode_and_issue.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/div_algorithms/div_algorithm.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/div_algorithms/div_quick_clz.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/div_algorithms/div_radix2.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/div_unit.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/dtag_banks.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/l2_arbiter/l2_config_and_types.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/external_interfaces.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/fetch.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/gc_unit.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/ibram.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/icache.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/illegal_instruction_checker.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/instruction_metadata_and_id_management.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/intel/intel_byte_enable_ram.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/interfaces.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/src/irom.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/itag_banks.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/l1_arbiter.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/src/l2_external_interfaces.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/load_store_queue.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/load_store_unit.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/lut_ram.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/mmu.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/mul_unit.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/one_hot_to_integer.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/ras.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/regfile_bank_sel.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/register_file.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/register_file_and_writeback.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/set_clr_reg_with_rst.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/shift_counter.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/tag_bank.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/taiga.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/taiga_fifo.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/tlb_lut_ram.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/toggle_memory.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/core/wishbone_master.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block/ipshared/cc20/taiga_wrapper_IP.srcs/sources_1/imports/xilinx/taiga_wrapper_xilinx.sv" \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block_v001/ip/gpn_bundle_block_taiga_wrapper_xilinx_0_0/sim/gpn_bundle_block_taiga_wrapper_xilinx_0_0.sv" \
-endlib
-makelib ies_lib/xil_defaultlib \
  "../../../../riscv_template_ex.gen/sources_1/bd/gpn_bundle_block_v001/sim/gpn_bundle_block.v" \
-endlib
-makelib ies_lib/xil_defaultlib \
  glbl.v
-endlib

