onbreak {quit -f}
onerror {quit -f}

vsim -lib xil_defaultlib slv_global_memory_vip_opt

do {wave.do}

view wave
view structure
view signals

do {slv_global_memory_vip.udo}

run -all

quit -force
