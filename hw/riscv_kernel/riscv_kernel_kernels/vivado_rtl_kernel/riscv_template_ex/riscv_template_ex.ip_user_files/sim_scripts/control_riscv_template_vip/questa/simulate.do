onbreak {quit -f}
onerror {quit -f}

vsim -lib xil_defaultlib control_riscv_template_vip_opt

do {wave.do}

view wave
view structure
view signals

do {control_riscv_template_vip.udo}

run -all

quit -force
