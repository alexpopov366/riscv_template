# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "RAM_SIZE" -parent ${Page_0}
  ipgui::add_param $IPINST -name "USE_PRELOAD_FILE" -parent ${Page_0}
  ipgui::add_param $IPINST -name "preload_file" -parent ${Page_0}


}

proc update_PARAM_VALUE.RAM_SIZE { PARAM_VALUE.RAM_SIZE } {
	# Procedure called to update RAM_SIZE when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.RAM_SIZE { PARAM_VALUE.RAM_SIZE } {
	# Procedure called to validate RAM_SIZE
	return true
}

proc update_PARAM_VALUE.USE_PRELOAD_FILE { PARAM_VALUE.USE_PRELOAD_FILE } {
	# Procedure called to update USE_PRELOAD_FILE when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.USE_PRELOAD_FILE { PARAM_VALUE.USE_PRELOAD_FILE } {
	# Procedure called to validate USE_PRELOAD_FILE
	return true
}

proc update_PARAM_VALUE.preload_file { PARAM_VALUE.preload_file } {
	# Procedure called to update preload_file when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.preload_file { PARAM_VALUE.preload_file } {
	# Procedure called to validate preload_file
	return true
}


proc update_MODELPARAM_VALUE.RAM_SIZE { MODELPARAM_VALUE.RAM_SIZE PARAM_VALUE.RAM_SIZE } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.RAM_SIZE}] ${MODELPARAM_VALUE.RAM_SIZE}
}

proc update_MODELPARAM_VALUE.preload_file { MODELPARAM_VALUE.preload_file PARAM_VALUE.preload_file } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.preload_file}] ${MODELPARAM_VALUE.preload_file}
}

proc update_MODELPARAM_VALUE.USE_PRELOAD_FILE { MODELPARAM_VALUE.USE_PRELOAD_FILE PARAM_VALUE.USE_PRELOAD_FILE } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.USE_PRELOAD_FILE}] ${MODELPARAM_VALUE.USE_PRELOAD_FILE}
}

