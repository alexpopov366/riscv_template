/*
 * Copyright © 2017 Eric Matthews,  Lesley Shannon
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Initial code developed under the supervision of Dr. Lesley Shannon,
 * Reconfigurable Computing Lab, Simon Fraser University.
 *
 * Author(s):
 *             Eric Matthews <ematthew@sfu.ca>
 */



module taiga_wrapper_xilinx
import taiga_config::*;
import taiga_types::*;
import l2_config_and_types::*;
/*# (
 		// Parameters of Axi00 Slave Bus Interface S00_AXI
		parameter  C_M00_AXI_TARGET_SLAVE_BASE_ADDR	= 32'h60000000,
		parameter integer C_M_AXI_BURST_LEN	= 1,
		parameter integer C_M_AXI_ID_WIDTH	= 8,
		parameter integer C_M_AXI_ADDR_WIDTH	= 32,
		parameter integer C_M_AXI_DATA_WIDTH	= 32,
		parameter integer C_M_AXI_AWUSER_WIDTH	= 4,
		parameter integer C_M_AXI_ARUSER_WIDTH	= 4,
		parameter integer C_M_AXI_WUSER_WIDTH	= 4,
		parameter integer C_M_AXI_RUSER_WIDTH	= 4,
		parameter integer C_M_AXI_BUSER_WIDTH	= 4


 )*/

 (
        input logic clk,
        input logic rst,

        //local_memory_interface.master instruction_bram,
        
	    output logic           instruction_bram_en,
	    output logic [3:0]     instruction_bram_be,
	    output logic [29:0]    instruction_bram_addr,
	    output logic [31:0]    instruction_bram_data_in,
	    input  logic [31:0]    instruction_bram_data_out,
	            
        //local_memory_interface.master instruction_bram,
        
	    output logic           instruction_rom_en,
	    output logic [3:0]     instruction_rom_be,
	    output logic [29:0]    instruction_rom_addr,
	    output logic [31:0]    instruction_rom_data_in,
	    input  logic [31:0]    instruction_rom_data_out,

        
        //local_memory_interface.master data_bram,
	    output logic           data_bram_en,
	    output logic [3:0]     data_bram_be,
	    output logic [29:0]    data_bram_addr,
	    output logic [31:0]    data_bram_data_in,
	    input  logic [31:0]    data_bram_data_out,

         //local_memory_interface.master data_bram,
	    output logic           axl_en,
	    output logic [3:0]     axl_be,
	    output logic [29:0]    axl_addr,
	    output logic [31:0]    axl_data_in,
	    input  logic [31:0]    axl_data_out,
	    
	    
	    //l2_requester_interface.master l2,

        // AXI SIGNALS - need these to unwrap the interface for packaging //
        
        
        
	    (* dont_touch = "yes" *) input logic m_axi_arready,
	    (* dont_touch = "yes" *) output logic m_axi_arvalid,
	    (* dont_touch = "yes" *) output logic [C_M_AXI_ADDR_WIDTH-1:0] m_axi_araddr,
	    (* dont_touch = "yes" *) output logic [7:0] m_axi_arlen,
	    (* dont_touch = "yes" *) output logic [2:0] m_axi_arsize,
	    (* dont_touch = "yes" *) output logic [1:0] m_axi_arburst,
	    (* dont_touch = "yes" *) output logic [3:0] m_axi_arcache,
	    (* dont_touch = "yes" *) output logic [5:0] m_axi_arid,
		(* dont_touch = "yes" *) output logic  m_axi_arlock,
		(* dont_touch = "yes" *) output logic [2:0] m_axi_arprot,
		(* dont_touch = "yes" *) output logic [3:0] m_axi_arqos,
		(* dont_touch = "yes" *) output logic [3:0] m_axi_aruser,
	   
	   
	    //read data
	    (* dont_touch = "yes" *) output logic m_axi_rready,
	    (* dont_touch = "yes" *) input logic m_axi_rvalid,
	    (* dont_touch = "yes" *) input logic [C_M_AXI_DATA_WIDTH-1:0] m_axi_rdata,
	    (* dont_touch = "yes" *) input logic [1:0] m_axi_rresp,
	    (* dont_touch = "yes" *) input logic m_axi_rlast,
	    (* dont_touch = "yes" *) input logic [5:0] m_axi_rid,
	    (* dont_touch = "yes" *) input logic [3:0] m_axi_ruser,

	    //Write channel
	    //write address
	    
	    
	    
	    
	    (* dont_touch = "yes" *) input logic m_axi_awready,
	    (* dont_touch = "yes" *) output logic m_axi_awvalid,
	    (* dont_touch = "yes" *) output logic [C_M_AXI_ADDR_WIDTH-1:0] m_axi_awaddr,
	    (* dont_touch = "yes" *) output logic [7:0] m_axi_awlen,
	    (* dont_touch = "yes" *) output logic [2:0] m_axi_awsize,
	    (* dont_touch = "yes" *) output logic [1:0] m_axi_awburst,
	    (* dont_touch = "yes" *) output logic [3:0] m_axi_awcache,
	    (* dont_touch = "yes" *) output logic [5:0] m_axi_awid,
		(* dont_touch = "yes" *) output logic  m_axi_awlock,
		(* dont_touch = "yes" *) output logic [2:0] m_axi_awprot,
		(* dont_touch = "yes" *) output logic [3:0] m_axi_awqos,
		(* dont_touch = "yes" *) output logic [3:0] m_axi_awuser,

	    //write data
	    (* dont_touch = "yes" *) input logic m_axi_wready,
	    (* dont_touch = "yes" *) output logic m_axi_wvalid,
	    (* dont_touch = "yes" *) output logic [C_M_AXI_DATA_WIDTH-1:0] m_axi_wdata,
	    (* dont_touch = "yes" *) output logic [(C_M_AXI_DATA_WIDTH/8)-1:0] m_axi_wstrb,
	    (* dont_touch = "yes" *) output logic m_axi_wlast,
	    (* dont_touch = "yes" *) output logic [5:0] m_axi_wid,
	    (* dont_touch = "yes" *) output logic [3:0] m_axi_wuser,

	    //write response
	    (* dont_touch = "yes" *) output logic m_axi_bready,
	    (* dont_touch = "yes" *) input logic m_axi_bvalid,
	    (* dont_touch = "yes" *) input logic [1:0] m_axi_bresp,
	    (* dont_touch = "yes" *) input logic [5:0] m_axi_bid,
		(* dont_touch = "yes" *) input logic [3:0] m_axi_buser
		
		
		/*
	    input logic m_axi_arready,
	    output logic m_axi_arvalid,
	    output logic [C_M_AXI_ADDR_WIDTH-1:0] m_axi_araddr,
	    output logic [7:0] m_axi_arlen,
	    output logic [2:0] m_axi_arsize,
	    output logic [1:0] m_axi_arburst,
	    output logic [3:0] m_axi_arcache,
	    output logic [5:0] m_axi_arid,
		output logic  m_axi_arlock,
		output logic [2:0] m_axi_arprot,
		output logic [3:0] m_axi_arqos,
		output logic [3:0] m_axi_aruser,
	   
	   
	    //read data
	    output logic m_axi_rready,
	    input logic m_axi_rvalid,
	    input logic [C_M_AXI_DATA_WIDTH-1:0] m_axi_rdata,
	    input logic [1:0] m_axi_rresp,
	    input logic m_axi_rlast,
	    input logic [5:0] m_axi_rid,
	    input logic [3:0] m_axi_ruser,

	    //Write channel
	    //write address
	    
	    
	    
	    
	    input logic m_axi_awready,
	    output logic m_axi_awvalid,
	    output logic [C_M_AXI_ADDR_WIDTH-1:0] m_axi_awaddr,
	    output logic [7:0] m_axi_awlen,
	    output logic [2:0] m_axi_awsize,
	    output logic [1:0] m_axi_awburst,
	    output logic [3:0] m_axi_awcache,
	    output logic [5:0] m_axi_awid,
		output logic  m_axi_awlock,
		output logic [2:0] m_axi_awprot,
		output logic [3:0] m_axi_awqos,
		output logic [3:0] m_axi_awuser,

	    //write data
	    input logic m_axi_wready,
	    output logic m_axi_wvalid,
	    output logic [C_M_AXI_DATA_WIDTH-1:0] m_axi_wdata,
	    output logic [(C_M_AXI_DATA_WIDTH/8)-1:0] m_axi_wstrb,
	    output logic m_axi_wlast,
	    output logic [5:0] m_axi_wid,
	    output logic [3:0] m_axi_wuser,

	    //write response
	    output logic m_axi_bready,
	    input logic m_axi_bvalid,
	    input logic [1:0] m_axi_bresp,
	    input logic [5:0] m_axi_bid,
		input logic [3:0] m_axi_buser
		*/
	    
    );
    
    //Positive reset signal
    //logic rst;
    //assign rst = ~rstn;
    
    //Unused outputs
    avalon_interface m_avalon ();
    wishbone_interface m_wishbone ();
    trace_outputs_t tr;
    logic timer_interrupt;
    logic interrupt;
    
    
    //Instruction BRAM
    local_memory_interface instruction_bram();
    assign instruction_bram_en = instruction_bram.en;
	assign instruction_bram_be = instruction_bram.be;
    assign instruction_bram_addr = instruction_bram.addr;
    assign instruction_bram_data_in = instruction_bram.data_in;
    assign instruction_bram.data_out = instruction_bram_data_out;

    //Instruction ROM
    local_memory_interface instruction_rom();
    assign instruction_rom_en = instruction_rom.en;
	assign instruction_rom_be = instruction_rom.be;
    assign instruction_rom_addr = instruction_rom.addr;
    assign instruction_rom_data_in = instruction_rom.data_in;
    assign instruction_rom.data_out = instruction_rom_data_out;


    //Data BRAM
    local_memory_interface data_bram();
    assign data_bram_en = data_bram.en;
	assign data_bram_be = data_bram.be;
    assign data_bram_addr = data_bram.addr;
    assign data_bram_data_in = data_bram.data_in;
    assign data_bram.data_out = data_bram_data_out;
    
    //Acceleration Link
    local_memory_interface axl();
    assign axl_en = axl.en;
	assign axl_be = axl.be;
    assign axl_addr = axl.addr;
    assign axl_data_in = axl.data_in;
    assign axl.data_out = axl_data_out;

    //AXI interface
    axi_interface m_axi();

    assign m_axi.arready = m_axi_arready;
    assign m_axi_arvalid = m_axi.arvalid;
    assign m_axi_araddr = m_axi.araddr;
    assign m_axi_arlen = m_axi.arlen;
    assign m_axi_arsize = m_axi.arsize;
    assign m_axi_arburst = m_axi.arburst;
    assign m_axi_arcache = m_axi.arcache;
    assign m_axi_arid = m_axi.arid;
	assign m_axi_arlock = m_axi.arlock;
	assign m_axi_arprot = m_axi.arprot;
	assign m_axi_arqos = m_axi.arqos;
	assign m_axi_aruser = m_axi.aruser;


    assign m_axi_rready = m_axi.rready;
    assign m_axi.rvalid = m_axi_rvalid;
    assign m_axi.rdata = m_axi_rdata;
	assign m_axi.rresp = m_axi_rresp;
	assign m_axi.rlast = m_axi_rlast;
	assign m_axi.rid = m_axi_rid;
	assign m_axi.ruser = m_axi_ruser;


	assign m_axi.awready = m_axi_awready;
	assign m_axi_awvalid = m_axi.awvalid;
	assign m_axi_awaddr = m_axi.awaddr;
	assign m_axi_awlen = m_axi.awlen;
	assign m_axi_awsize = m_axi.awsize;
	assign m_axi_awburst = m_axi.awburst;
	assign m_axi_awcache = m_axi.awcache;
	assign m_axi_awid = m_axi.awid;
	assign m_axi_awlock = m_axi.awlock;
	assign m_axi_awprot = m_axi.awprot;
	assign m_axi_awqos = m_axi.awqos;
	assign m_axi_awuser = m_axi.awuser;    

	  //write data
	assign m_axi.wready = m_axi_wready;
	assign m_axi_wvalid = m_axi.wvalid;
	assign m_axi_wdata = m_axi.wdata;
	assign m_axi_wstrb = m_axi.wstrb;
	assign m_axi_wlast = m_axi.wlast;
	assign m_axi_wid = m_axi.wid;
	assign m_axi_wuser = m_axi.wuser;



	    //write response
	assign m_axi_bready = m_axi.bready;
	assign m_axi.bvalid = m_axi_bvalid;
	assign m_axi.bresp = m_axi_bresp;
	assign m_axi.bid = m_axi_bid;
    assign m_axi.buser = m_axi_buser;

    l2_requester_interface l2();

    taiga cpu(.*);

endmodule

