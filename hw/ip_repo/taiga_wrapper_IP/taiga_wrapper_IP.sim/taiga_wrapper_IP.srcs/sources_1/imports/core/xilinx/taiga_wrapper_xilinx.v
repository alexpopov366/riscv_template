/*
 * Copyright © 2017 Eric Matthews,  Lesley Shannon
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Initial code developed under the supervision of Dr. Lesley Shannon,
 * Reconfigurable Computing Lab, Simon Fraser University.
 *
 * Author(s):
 *             Eric Matthews <ematthew@sfu.ca>
 */


 module taiga_wrapper_xilinx_verilog(
   // Input Ports - Single Bit
   input  clk,
   input  l2_con_result,
   input  l2_con_valid,
   input  l2_data_full,
   input  l2_inv_valid,
   input  l2_rd_data_valid,
   input  l2_request_full,
   input  m_axi_arready,
   input  m_axi_awready,
   input  m_axi_bvalid,
   input  m_axi_rlast,
   input  m_axi_rvalid,
   input  m_axi_wready,
   input  rst,
   // Input Ports - Busses
   input  [31:0] data_bram_data_out,
   input  [31:0] instruction_bram_data_out,
   input  [31:2] l2_inv_addr,
   input  [31:0] l2_rd_data,
   input  [1:0] l2_rd_sub_id,
   input  [5:0] m_axi_bid,
   input  [1:0] m_axi_bresp,
   input  [31:0] m_axi_rdata,
   input  [5:0] m_axi_rid,
   input  [1:0] m_axi_rresp,
   // Output Ports - Single Bit
   output data_bram_en,
   output instruction_bram_en,
   output l2_inv_ack,
   output l2_is_amo,
   output l2_rd_data_ack,
   output l2_request_push,
   output l2_rnw,
   output l2_wr_data_push,
   output m_axi_arvalid,
   output m_axi_awvalid,
   output m_axi_bready,
   output m_axi_rready,
   output m_axi_wlast,
   output m_axi_wvalid,
   // Output Ports - Busses
   output [29:0] data_bram_addr,
   output [3:0] data_bram_be,
   output [31:0] data_bram_data_in,
   output [29:0] instruction_bram_addr,
   output [3:0] instruction_bram_be,
   output [31:0] instruction_bram_data_in,
   output [29:0] l2_addr,
   output [4:0] l2_amo_type_or_burst_size,
   output [3:0] l2_be,
   output [1:0] l2_sub_id,
   output [31:0] l2_wr_data,
   output [31:0] m_axi_araddr,
   output [1:0] m_axi_arburst,
   output [3:0] m_axi_arcache,
   output [5:0] m_axi_arid,
   output [7:0] m_axi_arlen,
   output [2:0] m_axi_arsize,
   output [31:0] m_axi_awaddr,
   output [1:0] m_axi_awburst,
   output [3:0] m_axi_awcache,
   output [5:0] m_axi_awid,
   output [7:0] m_axi_awlen,
   output [2:0] m_axi_awsize,
   output [31:0] m_axi_wdata,
   output [3:0] m_axi_wstrb
   // InOut Ports - Single Bit
   // InOut Ports - Busses
);

taiga_wrapper_xilinx taiga_wrapper_xilinx_inst (
   // Input Ports - Single Bit
   .clk                             (clk),                          
   .l2_con_result                  (l2.con_result),               
   .l2_con_valid                   (l2.con_valid),                
   .l2_data_full                   (l2.data_full),                
   .l2_inv_valid                   (l2.inv_valid),                
   .l2_rd_data_valid               (l2.rd_data_valid),            
   .l2_request_full                (l2.request_full),             
   .m_axi_arready            (m_axi_arready),         
   .m_axi_awready            (m_axi_awready),         
   .m_axi_bvalid             (m_axi_bvalid),          
   .m_axi_rlast              (m_axi_rlast),           
   .m_axi_rvalid             (m_axi_rvalid),          
   .m_axi_wready             (m_axi_wready),          
   .rst                             (rst),                          
   // Input Ports - Busses
   .data_bram_data_out        (data_bram.data_out),     
   .instruction_bram_data_out (instruction_bram.data_out),
   .l2_inv_addr               (l2.inv_addr),            
   .l2_rd_data                (l2.rd_data),             
   .l2_rd_sub_id               (l2.rd_sub_id),            
   .m_axi_bid           (m_axi_bid),        
   .m_axi_bresp         (m_axi_bresp),      
   .m_axi_rdata        (m_axi_rdata),     
   .m_axi_rid           (m_axi_rid),        
   .m_axi_rresp         (m_axi_rresp),      
   // Output Ports - Single Bit
   .data_bram_en                   (data_bram.en),                
   .instruction_bram_en            (instruction_bram.en),         
   .l2_inv_ack                     (l2.inv_ack),                  
   .l2_is_amo                      (l2.is_amo),                   
   .l2_rd_data_ack                 (l2.rd_data_ack),              
   .l2_request_push                (l2.request_push),             
   .l2_rnw                         (l2.rnw),                      
   .l2_wr_data_push                (l2.wr_data_push),             
   .m_axi_arvalid            (m_axi_arvalid),         
   .m_axi_awvalid            (m_axi_awvalid),         
   .m_axi_bready             (m_axi_bready),          
   .m_axi_rready             (m_axi_rready),          
   .m_axi_wlast              (m_axi_wlast),           
   .m_axi_wvalid             (m_axi_wvalid),          
   // Output Ports - Busses
   .data_bram_addr            (data_bram.addr),         
   .data_bram_be               (data_bram.be),            
   .data_bram_data_in         (data_bram.data_in),      
   .instruction_bram_addr     (instruction_bram.addr),  
   .instruction_bram_be        (instruction_bram.be),     
   .instruction_bram_data_in  (instruction_bram.data_in),
   .l2_addr                   (l2.addr),                
   .l2_amo_type_or_burst_size  (l2.amo_type_or_burst_size),
   .l2_be                      (l2.be),                   
   .l2_sub_id                  (l2.sub_id),               
   .l2_wr_data                (l2.wr_data),             
   .m_axi_araddr       (m_axi_araddr),    
   .m_axi_arburst       (m_axi_arburst),    
   .m_axi_arcache       (m_axi_arcache),    
   .m_axi_arid          (m_axi_arid),       
   .m_axi_arlen         (m_axi_arlen),      
   .m_axi_arsize        (m_axi_arsize),     
   .m_axi_awaddr       (m_axi_awaddr),    
   .m_axi_awburst       (m_axi_awburst),    
   .m_axi_awcache       (m_axi_awcache),    
   .m_axi_awid          (m_axi_awid),       
   .m_axi_awlen         (m_axi_awlen),      
   .m_axi_awsize        (m_axi_awsize),     
   .m_axi_wdata        (m_axi_wdata),     
   .m_axi_wstrb         (m_axi_wstrb)      
   // InOut Ports - Single Bit
   // InOut Ports - Busses
);
    
    
endmodule

