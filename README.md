# Проект RISCV32. Тестовый пример проекта RiscV 

## Общее описание

Тестовый проект для взаимодействия HOST систем с RISCV ядром (XRT Runtime версия), используются аппаратные FIFO буферы по 512 слов и память External Memory.

## Установка

Для установки требуется клонировать репозиторий:

```bash
git clone https://gitlab.com/alexpopov366/riscv_template.git
```

## Зависимости

Зависимости для сборки проекта:

* набор средст сборки [riscv toolchain](https://gitlab.com/quantr/toolchain/riscv-gnu-toolchain) и экспорт исполняемых файлов в `PATH`

* набор библиотек [picolib](https://github.com/picolibc/picolibc) и экспорт в `C_INCLUDE_PATH`

* исходный текст проекта [taiga](https://github.gitop.top/taiga-project/taiga) и экспорт в переменную окружения `TAIGA_DIR`

* библиотека [xrt](https://gitlab.com/xilinx4jet/XRT) и установка по пути `/opt/xilinx/xrt`

Для стандартного пользователя ВМ студенческой команды хакатона все необходимые переменные окружения установлены по-умолчанию.

## Сборка проекта

Аппаратный проект собирается следующим образом:

```bash
cd hw
./riscv_build.sh
``` 

В итоге будет получен файл riscv_kernel.xclbin.

Далее следует собрать программную часть проекта для хоста и ядра riscv. Для этого необходимо выполнить команду:

```bash
cd ../sw/riscv_template/
make
```

Результатом выполнения команды станет файлы host_template, riscv_template.rawbinary.


## Запуск проекта

```
./host_template ../../hw/riscv_kernel.xclbin riscv_template.rawbinary
```

## Очистка проекта

Следует выполнить команду:

```bash
make clean
```
