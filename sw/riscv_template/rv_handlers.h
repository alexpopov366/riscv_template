/*
 * gpc_handlers.h
 *
 * host and sw_kernel library
 *
 * Macro instantiation for handlers
 * !!! This file should be identical for sw_kernel and host libraries !!!
 *
 * Created on: August 31, 2022
 * Author: A.Popov
 */
#ifndef DEF_HANDLERS_H_
#define DEF_HANDLERS_H_
#define DECLARE_EVENT_HANDLER(handler) \
            const unsigned int event_ ## handler =__LINE__; \
            void handler ();
#define __event__(handler) event_ ## handler
//  Event handlers declarations by declaration line number!!! 
DECLARE_EVENT_HANDLER(get_version);
DECLARE_EVENT_HANDLER(mq_rate_test);
DECLARE_EVENT_HANDLER(buf_rate_test);
DECLARE_EVENT_HANDLER(external_memory_test);


#endif
