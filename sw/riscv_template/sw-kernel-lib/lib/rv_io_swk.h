/*
 * rv_io_swk.h
 *
 * sw_kernel library
 *
 *  Created on: August 31, 2022
 *      Author: A.Popov
 */

#ifndef RV_IO_H_
#define RV_IO_H_

#include "rv_swk.h"

//====================================
// Адресация ресурсов на шине AXI
//====================================
#define MSG_BLOCK_START 0x00010000                              // area for IO

// Check parameters to avoid global memory conflict
// 1. MSG_BUFFER_SIZE == 2 * (MSG_QUEUE_FSIZE + MSG_BUFFER_SIZE)
// 2. MSG_BLOCK_SIZE * (RV cores in GROUP, 6 is default) < 64K
#define MSG_BLOCK_SIZE  0x2800                                  // 10K Block size for every RV
#define MSG_QUEUE_FSIZE 0x400                                   // 1024 bytes for queues
#define MSG_BUFFER_SIZE 0x1000                                  // 4096 bytes buffers for RV IO

// Automatically calculated parameters
#define MSG_QUEUE_SIZE  (MSG_QUEUE_FSIZE - 2 * sizeof(int))     // 1016 bytes for data, 8 bytes for pointers
#define HOST2RV_QUEUE  0                                       // Offset for Host2RV queue
#define RV2HOST_QUEUE  MSG_QUEUE_FSIZE                         // Offset for RV2Host queue
#define HOST2RV_BUFFER (2*MSG_QUEUE_FSIZE)                     // Offset for Host2RV queue
#define RV2HOST_BUFFER (HOST2RV_BUFFER + MSG_BUFFER_SIZE)     // Offset for RV2Host queue
#define	MQ_CREDITS		255


//===================================
// Типа данных и структуры для работы
//===================================

// Define Leonhard x64 descriptor
typedef struct
    global_memory_io
{
        // Queue credit
        u32 rv2host_credit;
        // Buffers offsets
        u32 rv2host_buffer;
        u32 host2rv_buffer;
        
} global_memory_io;

#ifdef DEFINE_GM_IO
#define EXTERN /* nothing */
#else
#define EXTERN extern
#endif /* DEFINE_GM_IO */
EXTERN global_memory_io gmio;

//========================================
// Функции для обмена данными с HOST
//========================================

void gmio_init(int core);
void mq_send(unsigned int message);
unsigned int mq_receive();
void buf_read(unsigned int size, unsigned int *local_buf);
void buf_write(unsigned int size, unsigned int *local_buf);
void ext_buf_read(unsigned int size, unsigned int *external_buf, unsigned int *local_buf);
void ext_buf_write(unsigned int size, unsigned int *external_buf, unsigned int *local_buf);

void sync_with_host();

#endif
