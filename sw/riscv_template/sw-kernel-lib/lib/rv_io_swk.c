/*
 * rv_io_swk.c
 *
 * sw_kernel library
 *
 *  Created on: April 23, 2021
 *      Author: A.Popov
 */

#include "rv_io_swk.h"

using namespace std;

//====================================================
// Глобальная структура очереди
//====================================================

global_memory_io gmio;

//====================================================
// Инициализация структуры
//====================================================

void gmio_init(int core)
{
    // Init queue and message offsets
    gmio.rv2host_credit=0;
    // Init buffers offsets
    gmio.rv2host_buffer = AXI4BRAM_BASE + MSG_BLOCK_START + core * MSG_BLOCK_SIZE + RV2HOST_BUFFER;
    gmio.host2rv_buffer = AXI4BRAM_BASE + MSG_BLOCK_START + core * MSG_BLOCK_SIZE + HOST2RV_BUFFER;
}

//====================================================
// Передача сообщения в очередь
//====================================================

void mq_send(unsigned int message)
{
	if (++gmio.rv2host_credit > MQ_CREDITS) {
		while (((*((volatile unsigned int *)(AXI4GPIO_MQ_ST))) & RV2HOST_MQ_AFULL) == RV2HOST_MQ_AFULL);
		gmio.rv2host_credit = 0;
	}
	*(volatile unsigned int *)(AXI4GPIO_RV2HOST_MQ) = message;    
	*(volatile unsigned int *)(AXI4GPIO_MQ_CTRL) = RV2HOST_MQ_WRITE;    
	*(volatile unsigned int *)(AXI4GPIO_MQ_CTRL) = 0;    
}

void mq_send(unsigned int size, unsigned int *buffer)
{
    for (unsigned int i=0; i<size; i++) 
        mq_send(buffer[i]);
}


//====================================================
// Чтение сообщения из очереди
//====================================================

unsigned int mq_receive()
{
    while (((*((volatile unsigned int *)(AXI4GPIO_MQ_ST))) & HOST2RV_MQ_EMPTY) == HOST2RV_MQ_EMPTY) {
        wait;
    };    
    unsigned int message = *(volatile unsigned int *)(AXI4GPIO_HOST2RV_MQ);
	*(volatile unsigned int *)(AXI4GPIO_MQ_CTRL) = HOST2RV_MQ_NEXT;    
	*(volatile unsigned int *)(AXI4GPIO_MQ_CTRL) = 0;     
    return message;
}


//====================================================
// Чтение буфера из global memory в RAM
//====================================================

void buf_read(unsigned int size, unsigned int *local_buf)
{
    for (unsigned int i=0; i<size; i+=4) 
        *((volatile unsigned int *)(local_buf + i)) = *((volatile unsigned int*)(gmio.host2rv_buffer + i));
}


//====================================================
// Чтение буфера из global memory в RAM
//====================================================

void buf_write(unsigned int size, unsigned int *local_buf)
{
    for (unsigned int i=0; i<size; i+=4) 
        *((volatile unsigned int*)(gmio.rv2host_buffer + i)) = *((volatile unsigned int *)(local_buf + i));
}

//====================================================
// Чтение буфера из external memory в RAM
//====================================================

void ext_buf_read(unsigned int size, unsigned int *external_buf, unsigned int *local_buf)
{
    for (unsigned int i=0; i<size; i+=4) 
        *((unsigned int *)(local_buf + i)) = *((volatile unsigned int*)(AXI4EXTMEM_BASE + external_buf + i));
}


//====================================================
// Чтение буфера из external memory в RAM
//====================================================

void ext_buf_write(unsigned int size, unsigned int *external_buf, unsigned int *local_buf)
{
    for (unsigned int i=0; i<size; i+=4) 
        *((volatile unsigned int*)(AXI4EXTMEM_BASE + external_buf + i)) = *((unsigned int *)(local_buf + i));
}


//====================================================
// Синхронизация ядра и host через передачу сообщений
//====================================================

void sync_with_host()
{
    mq_send(mq_receive());
}
