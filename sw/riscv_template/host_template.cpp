#include <iostream>
#include <stdio.h>
#include <stdexcept>
#include <iomanip>
#include <unistd.h>
#include <sys/time.h>

#include "experimental/xrt_device.h"
#include "experimental/xrt_kernel.h"
#include "experimental/xrt_bo.h"
#include "experimental/xrt_ini.h"

#include "rv_defs.h"
#include "riscv32_xrt.h"
#include "rv_handlers.h"

static void usage()
{
	std::cout << "usage: <xclbin> <sw_kernel>\n\n";
}

static void print_table(std::string test, float value, std::string units)
{
	std::cout << std::left << std::setfill(' ') << std::setw(50) << test << std::right << std::setw(20) << std::fixed << std::setprecision(0) << value << std::setw(15) << units << std::endl;
	std::cout << std::setfill('-') << std::setw(85) << "-" << std::endl;
}


int main(int argc, char** argv)
{

	unsigned int err = 0;
	unsigned int cores_count = 0;

	__foreach_core(core) cores_count++;

	//Assign xclbin
	if (argc < 3) {
		usage();
		throw std::runtime_error("FAILED_TEST\nNo xclbin specified");
	}

	//Open device #0
	riscv32 rv32_inst = riscv32(0,argv[1]);
	__foreach_core(core)
	{
		rv32_inst.load_sw_kernel(argv[2], core);
	}

	/*
	 *
	 * SW Kernel Version and Status
	 *
	 */
	__foreach_core(core)
	{
		printf("Core #%d\n", core);
		rv32_inst.rv[core]->start_sync(__event__(get_version));
		printf("\tSoftware Kernel Version:\t0x%08x\n", rv32_inst.rv[core]->mq_receive());
	}


	//-------------------------------------------------------------
	// Измерение производительности Leonhard
	//-------------------------------------------------------------

	char buf[100];
	err = 0;

	time_t now = time(0);
	strftime(buf, 100, "Start at local date: %d.%m.%Y.; local time: %H.%M.%S", localtime(&now));

	printf("\nDISC system speed test v3.0\n%s\n\n", buf);
	std::cout << std::left << std::setw(50) << "Test" << std::right << std::setw(20) << "value" << std::setw(15) << "units" << std::endl;
	std::cout << std::setfill('-') << std::setw(85) << "-" << std::endl;
	print_table("Graph Processing Cores count (RVC)", cores_count, "instances");



	/*
	 *
	 * Kernel RUN rate
	 *
	 */

	clock_t start,stop;

	start=clock();
	for (int i=0; i<1000; i++)
		__foreach_core(core)
			rv32_inst.rv[core]->start_async(0);
	stop=clock();
	print_table("Average sw_kernel Run Rate (AKRR)", float(1000 * CLOCKS_PER_SEC * cores_count) / float(stop-start), "calls/sec.");

	/*
	 *
	 * Write kernel register rate
	 *
	 */

	start=clock();
	for (int i=0; i<100000; i++)
		__foreach_core(core)
			rv32_inst.rv[core]->kernel->write_register(RV_CONFIG_OFFSET, i);
	stop=clock();
	print_table("Average RV IO register write rate (IORW)",float(100000 * CLOCKS_PER_SEC * cores_count) / float(stop-start) , "calls/sec.");

	/*
	 *
	 * Read kernel register rate
	 *
	 */

	start=clock();
	for (int i=0; i<100000; i++)
		__foreach_core(core)
			rv32_inst.rv[core]->kernel->read_register(RV_CONFIG_OFFSET);
	stop=clock();
	print_table("Average RV IO register read rate (IORR)", float(100000 * CLOCKS_PER_SEC * cores_count) / float(stop-start), "calls/sec.");


	/*
	 *
	 * Queue single messaging send and receive rates
	 *
	 */

	__foreach_core(core)
	{
		rv32_inst.rv[core]->start_async(__event__(mq_rate_test));
	}
	start=clock();
	for (int i=0;i<256;i++) {
		__foreach_core(core)
		{
			rv32_inst.rv[core]->mq_send(i);
		}
	}
	stop=clock();
	print_table("Message Queue Single Send  Rate (AQSSR)", float(256 * CLOCKS_PER_SEC * cores_count) / float(stop-start), "messages/sec.");
	start=clock();
	for (int i=0;i<256;i++) {
		__foreach_core(core)
			{
			rv32_inst.rv[core]->mq_receive();
		}
	}
	stop=clock();
	print_table("Message Queue Single Receive Rate (AQSRR)", float(256 * CLOCKS_PER_SEC * cores_count) / float(stop-start), "messages/sec.");

	/*
	 *
	 * Queue block messaging send-receive rate
	 *
	 */

	unsigned int *host2rv_buffer[LNH_MAX_CORES_IN_GROUP];
	__foreach_core(core)
	{
		host2rv_buffer[core] = (unsigned int*) malloc(256*sizeof(int));
		for (int i=0;i<256;i++) host2rv_buffer[core][i] = i;
	}
	unsigned int *rv2host_buffer[LNH_MAX_CORES_IN_GROUP];
	__foreach_core(core)
	{
		rv2host_buffer[core] = (unsigned int*) malloc(256*sizeof(int));
	}
	__foreach_core(core)
	{
		rv32_inst.rv[core]->start_async(__event__(mq_rate_test));
	}
	start=clock();
	__foreach_core(core)
	{
		rv32_inst.rv[core]->mq_send(256*sizeof(int),host2rv_buffer[core]);
	}
	__foreach_core(core)
		{
		rv32_inst.rv[core]->mq_receive(256*sizeof(int),rv2host_buffer[core]);
	}
	__foreach_core(core)
	{
		rv32_inst.rv[core]->mq_send_join();
	}
	__foreach_core(core)
	{
		rv32_inst.rv[core]->mq_receive_join();
	}
	stop=clock();
	__foreach_core(core) {
		free(host2rv_buffer[core]);
		free(rv2host_buffer[core]);
	}
	print_table("Message Queue Block Send-Receive Rate (AQBSRR)", float(256 * CLOCKS_PER_SEC * cores_count) / float(stop-start), "messages/sec.");

	/*
	 *
	 * Queue single messaging send-receive rate
	 *
	 */

	__foreach_core(core)
	{
		rv32_inst.rv[core]->start_async(__event__(mq_rate_test));
	}
	start=clock();
	for (int i=0;i<256;i++) {
		__foreach_core(core)
		{
			rv32_inst.rv[core]->mq_send(i);
		}
		__foreach_core(core)
			{
			rv32_inst.rv[core]->mq_receive();
		}
	}
	stop=clock();
	print_table("Message Queue Single Send-Receive Rate (AQSSRR)", float(256 * CLOCKS_PER_SEC * cores_count) / float(stop-start), "messages/sec.");



	/*
	 *
	 * Global Memory Buffer block data transfer rate
	 *
	 */

	__foreach_core(core)
	{
		host2rv_buffer[core] = (unsigned int*) malloc(1024*sizeof(int));
		for (int i=0;i<1024;i++) host2rv_buffer[core][i] = i;
	}
	__foreach_core(core)
	{
		rv2host_buffer[core] = (unsigned int*) malloc(1024*sizeof(int));
	}
	__foreach_core(core) {
		rv32_inst.rv[core]->start_async(__event__(buf_rate_test));
	}
	start=clock();
	__foreach_core(core) {
		rv32_inst.rv[core]->buf_write(1024*sizeof(int),host2rv_buffer[core]);
	}
	__foreach_core(core) {
		rv32_inst.rv[core]->buf_write_join();
	}
	__foreach_core(core) {
		rv32_inst.rv[core]->mq_send(1024*sizeof(int));
	}
	__foreach_core(core) {
		rv32_inst.rv[core]->mq_receive();
	}
	__foreach_core(core) {
		rv32_inst.rv[core]->buf_read(1024*sizeof(int),rv2host_buffer[core]);
	}
	__foreach_core(core) {
		rv32_inst.rv[core]->buf_read_join();
	}
	stop=clock();
	__foreach_core(core) {
		free(host2rv_buffer[core]);
		free(rv2host_buffer[core]);
	}
	print_table("Global Memory Send-Receive Rate (GMSRR)", float(4096*CLOCKS_PER_SEC*cores_count) / float(stop-start), "bytes/sec.");

	/*
	 *
	 * External Memory block data transfer rate
	 *
	 */

	char* host2rv_ext_buffer[LNH_MAX_CORES_IN_GROUP];

	__foreach_core(core)
	{
		host2rv_ext_buffer[core] = rv32_inst.rv[core]->external_memory_create_buffer(32*1024*sizeof(int));
		for (int i=0;i<1024;i++) ((unsigned int*)host2rv_ext_buffer[core])[i] = i;
		for (int i=1024;i<2048;i++) ((unsigned int*)host2rv_ext_buffer[core])[i] = 0; //required by XRT to avoid read-before_write exceprion
		rv32_inst.rv[core]->external_memory_sync_to_device(0,2*1024*sizeof(int));
	}
	__foreach_core(core) {
		rv32_inst.rv[core]->start_async(__event__(external_memory_test));
	}
	start=clock();
	__foreach_core(core) {
		long long tmp = rv32_inst.rv[core]->external_memory_address();
		rv32_inst.rv[core]->mq_send((unsigned int)tmp);
	}
	__foreach_core(core) {
		rv32_inst.rv[core]->mq_send(1024*sizeof(int));
	}
	__foreach_core(core) {
		rv32_inst.rv[core]->mq_receive();
	}
	__foreach_core(core) {
		rv32_inst.rv[core]->external_memory_sync_from_device(1024*sizeof(int),1024*sizeof(int));
	}
	stop=clock();
	__foreach_core(core) {
		free(host2rv_ext_buffer[core]);
	}
	print_table("External Memory Send-Receive Rate (EMSRR)", float(4096*CLOCKS_PER_SEC*cores_count) / float(stop-start), "bytes/sec.");


	//--------------------------------------------------------------------------
	// Shutdown and cleanup
	//--------------------------------------------------------------------------

	if (err)
	{
		printf("ERROR: Test failed\n");
		return EXIT_FAILURE;
	}
	else
	{
		printf("INFO: Test completed successfully.\n");
		return EXIT_SUCCESS;
	}





	return 0;
}
