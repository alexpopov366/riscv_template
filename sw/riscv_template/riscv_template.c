/*
 * riscv_template.c
 *
 * sw_kernel library
 *
 *  Created on: August 31, 2022
 *      Author: A.Popov
 */

#include <stdlib.h>
#include "rv_io_swk.h"
#include "rv_handlers.h"

#define VERSION 01
#define DEFINE_MQ_R2L
#define DEFINE_MQ_L2R
#define ROM_LOW_ADDR 0x00000000

extern global_memory_io gmio;
volatile unsigned int event_source;

int main(void) {
    /////////////////////////////////////////////////////////
    //                  Main Event Loop
    /////////////////////////////////////////////////////////
    //Initialise host2gpc and gpc2host queues
    gmio_init(0);
    //Initialise host2rv and rv2host queues
    for (;;) {
        //Wait for event
        while (!rv_start());
        //Enable RW operations
        set_rv_state(BUSY);
        //Wait for event
        event_source = rv_config();
        switch(event_source) {
            /////////////////////////////////////////////
            //  Measure GPN operation frequency
            /////////////////////////////////////////////
            case __event__(get_version): get_version(); break;
            case __event__(mq_rate_test): mq_rate_test(); break;
            case __event__(buf_rate_test): buf_rate_test(); break;
            case __event__(external_memory_test): external_memory_test(); break;
        }
        //Disable RW operations
        set_rv_state(IDLE);
        while (rv_start());

    }
}
    
//-------------------------------------------------------------
//      Глобальные переменные (для сокращения объема кода)
//-------------------------------------------------------------
// Объявление переменных

//-------------------------------------------------------------
//      Получить версию микрокода 
//-------------------------------------------------------------
 
void get_version() {
    
        mq_send(VERSION);

}
   
//-------------------------------------------------------------
//      Измерение скорости обмена сообщениями через очереди MQ
//-------------------------------------------------------------

void mq_rate_test() {

    for (int i=0;i<256;i++) 
        mq_send(mq_receive()); //echo

    
}


//-------------------------------------------------------------
//      Измерение скорости обмена данными через буферы 4К
//-------------------------------------------------------------

void buf_rate_test() {
    unsigned int size = mq_receive();
    unsigned int *buffer = (unsigned int*)malloc(size);
    buf_read(size, buffer);
    buf_write(size, buffer);   
    mq_send(size);
    free(buffer);
}



//-------------------------------------------------------------
//      Измерение скорости обмена данными с external memory
//-------------------------------------------------------------

void external_memory_test() {
    unsigned int buffer_pointer = mq_receive();
    unsigned int size = mq_receive();
    unsigned int *buffer = (unsigned int*)malloc(size);
    ext_buf_read(size, (unsigned int *)buffer_pointer, buffer);
    ext_buf_write(size, (unsigned int *)buffer_pointer, buffer);
    mq_send(size);
    free(buffer);
}



