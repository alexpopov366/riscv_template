/*
 * rv_host.cpp
 *
 * host library (XRT runtime version)
 *
 *  Created on: August 31, 2022
 *      Author: A.Popov
 */

#include "rv_xrt_host.h"


// Constructor
rv::rv(xrt::device *xrt_device,xrt::uuid *xrt_uuid,unsigned int core_number)
{
	core=core_number;
	device=xrt_device;
	uuid=xrt_uuid;
	kernel = new xrt::kernel(*device, *uuid, LNH_CORE_DEFS.KERNEL_NAME[core], xrt::kernel::cu_access_mode::exclusive);
};

rv::~rv() {
	free(kernel);
}

unsigned int rv::load_file_to_memory(const char *filename, char **result)
{
	unsigned int size = 0;
	FILE *f = fopen(filename, "rb");
	if (f == NULL)
	{
		*result = NULL;
		return -1; // -1 means file opening fail
	}
	fseek(f, 0, SEEK_END);
	size = ftell(f);
	fseek(f, 0, SEEK_SET);
	if (size != fread(*result, sizeof(char), size, f))
	{
		free(*result);
		return -2; // -2 means file reading fail
	}
	fclose(f);
	(*result)[size] = 0;
	return size;
}


// Load RV kernel binary to the GPN
void rv::load_sw_kernel(char *rvbin)
{
	//Set buffers
	global_memory_map = global_memory_pointer->map<char*>();
	external_memory_map = external_memory_pointer->map<char*>();
	//Write sw_kernel to global memory
	unsigned int n_rv0 = load_file_to_memory(rvbin, (char **)&global_memory_map);
	global_memory_pointer->sync(XCL_BO_SYNC_BO_TO_DEVICE, n_rv0, 0);
	//Create RUN context, Reset CPE and start bootloader
	run = kernel->operator()(RV_RESET_HIGH,n_rv0,*global_memory_pointer,*external_memory_pointer);
	run.wait();
	// Free memory
	// Enable handlers execution
	run.set_arg(0,RV_RESET_LOW);
}


void rv::start_sync(const unsigned int event_handler)
{
	//Set args for RUN context
	run.set_arg(1,event_handler);
	run.start();
	run.wait();
}

void rv::start_async(const unsigned int event_handler)
{
	//Check if previous run is finished
	run.wait();
	//Set args for RUN context
	run.set_arg(1,event_handler);
	run.start();
}

void rv::finish()
{
	run.wait();
}



