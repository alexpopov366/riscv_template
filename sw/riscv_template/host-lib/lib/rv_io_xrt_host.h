/*
 * rv_io_host.h
 *
 * host library (XRT runtime version)
 *
 *  Created on: August 31, 2022
 *      Author: A.Popov
 */

#ifndef MQ_H_
#define MQ_H_

#include <chrono>
#include <thread>
#include <mutex>
#include <unistd.h>
#include "rv_xrt_host.h"


//====================================
// Адресация ресурсов в Global Memory
//====================================
#define MSG_BLOCK_START 0x00010000                  			// area for IO

// Check parameters to avoid global memory conflict
// 1. MSG_BUFFER_SIZE == 2 * (MSG_QUEUE_FSIZE + MSG_BUFFER_SIZE)
// 2. MSG_BLOCK_SIZE * (RV cores in GROUP, 6 is default) < 64K
#define MSG_BLOCK_SIZE	0x2800									// 10K Block size for every RV
#define MSG_QUEUE_FSIZE 0x400                      				// 1024 bytes for queues
#define MSG_BUFFER_SIZE	0x1000									// 4096 bytes buffers for RV IO

// Automatically calculated parameters
#define MSG_QUEUE_SIZE  (MSG_QUEUE_FSIZE - 2 * sizeof(int))  	// 1016 bytes for data, 8 bytes for pointers
#define HOST2RV_QUEUE  0                           			// Offset for Host2RV queue
#define RV2HOST_QUEUE  MSG_QUEUE_FSIZE             			// Offset for RV2Host queue
#define HOST2RV_BUFFER (2*MSG_QUEUE_FSIZE)                     // Offset for Host2RV queue
#define RV2HOST_BUFFER (HOST2RV_BUFFER + MSG_BUFFER_SIZE)		// Offset for RV2Host queue


//=============================================
// Класс для взаимодействия с RV через очереди
//=============================================

class rv_io : public rv
{
private:
  // host2gpn queue credits
  unsigned int host2rv_credit;
  // Buffers offsets
  unsigned int rv2host_buffer;
  unsigned int host2rv_buffer;
  // Threads and alive flags
  std::thread *mq_send_th;
  std::mutex mq_send_th_alive;
  std::thread *mq_receive_th;
  std::mutex mq_receive_th_alive;
  std::thread *buf_write_th;
  std::mutex buf_write_th_alive;
  std::thread *buf_read_th;
  std::mutex buf_read_th_alive;
  // Send message to RV queues
  void mq_send_thread(unsigned int message);
  void mq_send_buf_thread(unsigned int size, unsigned int *buffer);
  // Wait and read single message from RV queue
  unsigned int mq_receive_thread();
  void mq_receive_buf_thread(unsigned int size, unsigned int *buffer);
  // Write data to HOST2RV buffer
  void buf_write_thread(unsigned int size, unsigned int *buffer);
  // Read data from RV2HOST buffer
  void buf_read_thread(unsigned int size, unsigned int *buffer);
public:
  // Constructor
  rv_io(xrt::device *xrt_device,xrt::uuid *xrt_uuid,unsigned int core_number);
  // Destructor
  ~rv_io();
  void load_sw_kernel(char *gpnbin,xrt::bo *global_memory_ptr);
  char* external_memory_create_buffer(unsigned int size);
  void external_memory_sync_to_device(unsigned int offset, unsigned int size);
  void external_memory_sync_from_device(unsigned int offset, unsigned int size);
  long long external_memory_address();
  // Send message to RV queues
  void mq_send(unsigned int message);
  void mq_send(unsigned int size, unsigned int *buffer);
  void mq_send_join();
  // Wait and read single message from RV queue
  unsigned int mq_receive();
  void mq_receive(unsigned int size, unsigned int *buffer);
  void mq_receive_join();
  // Write data to HOST2RV buffer
  void buf_write(unsigned int size, unsigned int *buffer);
  void buf_write_join();
  // Read data from RV2HOST buffer
  void buf_read(unsigned int size, unsigned int *buffer);
  void buf_read_join();
    // Sync point with RV
  void sync_with_rv();

protected:

};

#endif
