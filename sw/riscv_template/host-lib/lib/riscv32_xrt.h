/*
 * riscv32.h
 *
 * host library (XRT runtime version)
 *
 *  Created on: April 23, 2021
 *      Author: A.Popov
 */

#ifndef RISCV32_H_
#define RISCV32_H_

#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include "rv_io_xrt_host.h"
#include "rv_defs.h"

//====================================================
// Класс для управления картой ускорителя Leonhard x64
//====================================================

class riscv32
{
private:
	xrt::device *xrt_device;
	xrt::uuid *xrt_uuid;
	xrt::bo *global_memory;                          	  // device memory used for a rv IO
    void device_reset();
public:
  //RV instances
  rv_io *rv[LNH_MAX_CORES_IN_GROUP]; 		 // global memory queue
  // Constructor
  riscv32(
			  unsigned int 		dev_index,
			  char 			*xclbin
			  );
  // Destructor
  ~riscv32();
  // Reset device
  void load_sw_kernel(char *gpnbin, unsigned int core);

protected:
};

#endif
