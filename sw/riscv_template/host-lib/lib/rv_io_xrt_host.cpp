/*
 * rv_io_host.cpp
 *
 * host library (XRT runtime version)
 *
 *  Created on: August 31, 2022
 *      Author: A.Popov
 */
#include "rv_io_xrt_host.h"

// Read queue status from global memory

// Constructor
rv_io	::	rv_io(xrt::device *xrt_device,xrt::uuid *xrt_uuid,unsigned int core_number)
		: 	rv (xrt_device,xrt_uuid,core_number)
{
	// Init queues credits
	host2rv_credit = 0;
	// Init buffers offsets
	host2rv_buffer = MSG_BLOCK_START + core * MSG_BLOCK_SIZE + HOST2RV_BUFFER;
	rv2host_buffer = MSG_BLOCK_START + core * MSG_BLOCK_SIZE + RV2HOST_BUFFER;
	// Map IO user space buffers to global memory
	xrt::bo::flags flags = xrt::bo::flags::normal;
	external_memory_pointer = new xrt::bo(*device, 4096, flags, kernel->group_id(3));   // external memory used for a rv external IO
	external_memory_map = external_memory_pointer->map<char*>();
}

// Constructor
rv_io::~rv_io() {
	free(mq_send_th);
	free(buf_write_th);
	free(mq_receive_th);
	free(buf_read_th);
	free(external_memory_pointer);
}
//Thread version

// Load RV kernel binary to the GPN

void rv_io::load_sw_kernel(char *gpnbin,xrt::bo *global_memory_ptr){
	global_memory_pointer = global_memory_ptr;
	rv::load_sw_kernel(gpnbin);
}

char* rv_io::external_memory_create_buffer(unsigned int size){
	// Map IO user space buffers to external memory
	free(external_memory_pointer);
	xrt::bo::flags flags = xrt::bo::flags::normal;
	external_memory_pointer = new xrt::bo(*device, size, flags, kernel->group_id(3));   // external memory used for a rv external IO
	external_memory_map = external_memory_pointer->map<char*>();
	run.set_arg(3,*external_memory_pointer);
	return external_memory_map;
}

// Send message to rv
void rv_io::mq_send_thread(unsigned int message)
{
	using namespace std::chrono_literals;
	if (++host2rv_credit > MQ_CREDITS) {
		while ((kernel->read_register(MQ_SR) & HOST2RV_MQ_AFULL) == HOST2RV_MQ_AFULL) {
			std::this_thread::sleep_for(10us);
		}
		host2rv_credit = 0;
	}
	kernel->write_register(HOST2RV_MQ, message);
}


// Send multiple messages to rv
void rv_io::mq_send_buf_thread(unsigned int size, unsigned int *buffer)
{
	for (unsigned int i=0; i<size; i+=4) {
		mq_send_thread(*(unsigned int*)((char*)buffer + i));
	}
}

// Wait and read single message from rv
unsigned int rv_io::mq_receive_thread()
{
	using namespace std::chrono_literals;
	volatile unsigned int mq_st = kernel->read_register(MQ_SR);
	while ((mq_st & RV2HOST_MQ_EMPTY) == RV2HOST_MQ_EMPTY) {
			std::this_thread::sleep_for(10us);
			mq_st = kernel->read_register(MQ_SR);
	}
	return kernel->read_register(RV2HOST_MQ);
}

// Wait and read multiple messages from rv
void rv_io::mq_receive_buf_thread(unsigned int size, unsigned int *buffer)
{
	for (unsigned int i=0; i<size; i+=4) {
		*(unsigned int*)((char*)buffer + i) = mq_receive_thread();
	}
}

// Write data to HOST2RV buffer
void rv_io::buf_write_thread(unsigned int size, unsigned int *buffer) {
	for (unsigned int i = 0; i < size; i+=4)
		*(volatile unsigned int*)(global_memory_map + host2rv_buffer + i) = *(unsigned int*)((char*)buffer + i);
	global_memory_pointer->sync(XCL_BO_SYNC_BO_TO_DEVICE, size, host2rv_buffer);


}

// Read data from RV2HOST buffer
void rv_io::buf_read_thread(unsigned int size, unsigned int *buffer) {
	global_memory_pointer->sync(XCL_BO_SYNC_BO_FROM_DEVICE , size, rv2host_buffer);
	for (unsigned int i = 0; i < size; i+=4)
		*(unsigned int*)((char*)buffer + i) = *(volatile unsigned int*)(global_memory_map + host2rv_buffer + i);
}

// Send single message to rv
void rv_io::mq_send(unsigned int message)
{
	mq_send_th_alive.lock();
	mq_send_thread(message);
	mq_send_th_alive.unlock();
}

// Send multiple messages to rv asynchronously
void rv_io::mq_send(unsigned int size, unsigned int *buffer)
{
	mq_send_th_alive.lock();
    mq_send_th = new std::thread(&rv_io::mq_send_buf_thread, this,  size, buffer);
	mq_send_th_alive.unlock();
}

// Wait for the
void rv_io::mq_send_join()
{
	mq_send_th->join();
	free(mq_send_th);

}

// Wait and read single message from rv
unsigned int rv_io::mq_receive()
{
	mq_receive_th_alive.lock();
	unsigned int message  = mq_receive_thread();
	mq_receive_th_alive.unlock();
	return message;
}

void rv_io::mq_receive(unsigned int size, unsigned int *buffer)
{
	mq_receive_th_alive.lock();
    mq_receive_th = new std::thread(&rv_io::mq_receive_buf_thread, this,  size, buffer);
	mq_receive_th_alive.unlock();
}

void rv_io::mq_receive_join()
{
	mq_receive_th->join();
	free(mq_receive_th);
}

// Write data to HOST2RV buffer
void rv_io::buf_write(unsigned int size, unsigned int *buffer)
{
	buf_write_th_alive.lock();
    buf_write_th = new std::thread(&rv_io::buf_write_thread, this,  size, buffer);
	buf_write_th_alive.unlock();
}

void rv_io::buf_write_join()
{
	buf_write_th->join();
	free(buf_write_th);
}

// Read data from RV2HOST buffer
void rv_io::buf_read(unsigned int size, unsigned int *buffer)
{
	buf_read_th_alive.lock();
    buf_read_th = new std::thread(&rv_io::buf_read_thread, this,  size, buffer);
	buf_read_th_alive.unlock();
}

void rv_io::buf_read_join()
{
	buf_read_th->join();
	free(buf_read_th);

}

void rv_io::external_memory_sync_to_device(unsigned int offset, unsigned int size)
{
	external_memory_pointer->sync(XCL_BO_SYNC_BO_TO_DEVICE, size, offset);
}

void rv_io::external_memory_sync_from_device(unsigned int offset, unsigned int size)
{
	external_memory_pointer->sync(XCL_BO_SYNC_BO_FROM_DEVICE, size, offset);
}

long long rv_io::external_memory_address()
{
	return external_memory_pointer->address();
}


// Syncronization point with rv
void rv_io::sync_with_rv()
{
	mq_send(0);
	mq_receive();
}
