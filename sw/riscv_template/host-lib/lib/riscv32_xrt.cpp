/*
 * riscv32.cpp
 *
 * host library (XRT runtime version)
 *
 *  Created on: August 31, 2022
 *      Author: A.Popov
 */
#include "riscv32_xrt.h"

// Constructor
riscv32::riscv32(
		unsigned int 		dev_index,
		char 			*xclbin
) {
	// Open device
	xrt_device = new xrt::device(dev_index);
	// Program or connect to the hw kernel
	xrt_uuid = new xrt::uuid(xrt_device->load_xclbin(xclbin));
	// Create RV objects
	xrt::bo::flags flags = xrt::bo::flags::normal;
	__foreach_core(core)
	{
		rv[core] = new rv_io(xrt_device,xrt_uuid,core);

	}
	//Create buffers
	global_memory = new xrt::bo(*xrt_device, GLOBAL_MEMORY_MAX_LENGTH*sizeof(int), flags, rv[LNH_CORES_LOW]->kernel->group_id(2));
}

void riscv32::load_sw_kernel(char *gpnbin, unsigned int core) {
	// RV queue initialization
	rv[core]->load_sw_kernel(gpnbin,global_memory);
}

// Destructor
riscv32::~riscv32() {
	device_reset();
	__foreach_core(core) {
		free(rv[core]);
	}
	free(global_memory);
	free(xrt_uuid);
	free(xrt_device);
}

//Reset device
void riscv32::device_reset() {

}
