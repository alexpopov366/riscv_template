/*
 * rv_defs.h
 *
 * host library
 *
 *  Created on: August 31, 2022
 *      Author: A.Popov
 */

#ifndef RVCORE_DEFS_H_
#define RVCORE_DEFS_H_

//Macros for multicore
#define 			__foreach_core(core) \
						for (int core=LNH_CORES_LOW; core<=LNH_CORES_HIGH; core++)

//Cores soft specification
#define			LNH_MAX_CORES_IN_GROUP		12
const struct {
	char *KERNEL_NAME[LNH_MAX_CORES_IN_GROUP] = {
				"riscv_template:{riscv_kernel0}",
				"riscv_template:{riscv_kernel1}",
				"riscv_template:{riscv_kernel2}",
				"riscv_template:{riscv_kernel3}",
				"riscv_template:{riscv_kernel4}",
				"riscv_template:{riscv_kernel5}",
				"riscv_template:{riscv_kernel6}",
				"riscv_template:{riscv_kernel7}",
				"riscv_template:{riscv_kernel8}",
				"riscv_template:{riscv_kernel9}",
				"riscv_template:{riscv_kernel10}",
				"riscv_template:{riscv_kernel11}"		
	};
} LNH_CORE_DEFS;



const int 		LNH_CORES_LOW 		= 	0;
const int 		LNH_CORES_HIGH 		= 	0;
const unsigned int	RV_RESET_HIGH 		= 	1;
const unsigned int	RV_RESET_LOW 		= 	0;
#define 		GLOBAL_MEMORY_MAX_LENGTH  	32768
#define			MQ_CREDITS			255

//Define register offsets
// 0x00 : Control signals
//        bit 0  - ap_start (Read/Write/COH)
//        bit 1  - ap_done (Read/COR)
//        bit 2  - ap_idle (Read)
//        bit 3  - ap_ready (Read)
//        bit 7  - auto_restart (Read/Write)
//        others - reserved
// 0x04 : Global Interrupt Enable Register
//        bit 0  - Global Interrupt Enable (Read/Write)
//        others - reserved
// 0x08 : IP Interrupt Enable Register (Read/Write)
//        bit 0  - enable ap_done interrupt (Read/Write)
//        bit 1  - enable ap_ready interrupt (Read/Write)
//        others - reserved
// 0x0c : IP Interrupt Status Register (Read/TOW)
//        bit 0  - ap_done (COR/TOW)
//        bit 1  - ap_ready (COR/TOW)
//        others - reserved
// (SC = Self Clear, COR = Clear on Read, TOW = Toggle on Write, COH = Clear on Handshake)
// Standard IO registers
#define			   CONTROL_OFFSET				0x000
#define 				AP_START 				(1<<0)
#define					AP_DONE					(1<<1)
#define					AP_IDLE					(1<<2)
#define					AP_READY				(1<<3)
#define					AUTO_RESTART				(1<<7)
#define			   G_IE_OFFSET					0x004
#define 				G_IE_ENABLE				(1<<0)
#define			   IP_IE_OFFSET					0x008
#define 				AP_DONE_IE_ENABLE			(1<<0)
#define 				AP_READY_IE_ENABLE			(1<<1)
#define			   IP_IS_OFFSET					0x00c
#define 				AP_DONE_IS_STATUS			(1<<0)
#define 				AP_READY_IS_STATUS			(1<<1)
// Custom IP registers
#define			   RV_RESET_OFFSET				0x010
#define			   RV_CONFIG_OFFSET				0x018
#define			   GLOBAL_MEMORY_PTR_OFFSET			0x020
#define			   EXTERNAL_MEMORY_PTR_OFFSET			0x02C
#define			   MQ_SR					0x04C
#define 				HOST2RV_MQ_FULL				(1<<0)
#define 				HOST2RV_MQ_AFULL			(1<<1)
#define 				HOST2RV_MQ_EMPTY			(1<<2)
#define 				RV2HOST_MQ_FULL				(1<<3)
#define 				RV2HOST_MQ_AFULL			(1<<4)
#define 				RV2HOST_MQ_EMPTY			(1<<5)
#define			   MQ_RST					0x04C
#define			   HOST2RV_MQ					0x050
#define			   RV2HOST_MQ					0x054

#endif
