//====================================
// Адресация ресурсов на шине AXI
//====================================
#define AXI4BRAM_BASE           0xA0000000              //Global Memory Base Address
#define AXI4BRAM_SIZE           0x00020000              //Size: 128 KB
#define AXI4GPIO_START_BASE     0xA0030000              //GPIO port 0 IN  /START/
#define AXI4GPIO_CONFIG_BASE    0xA0030008              //GPIO port 1 IN  /CONFIG/
#define AXI4GPIO_STATE_BASE     0xA0020000              //GPIO port 2 OUT /STATE/
#define RAM_LOW_ADDR            0x80000000              //RAM starting address
#define RAM_SIZE                0x00010000              //RAM size in bytes
    

void _start(void) {
    
    //Version 1.1
    //Valid for picolib library
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    while ((*((volatile unsigned int*)(AXI4GPIO_START_BASE))) == 0);  //Wait for the start signal
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    *((volatile unsigned int*)(AXI4GPIO_STATE_BASE)) = 0;             //IDLE go down
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    unsigned int sw_kernel_size = (*((volatile unsigned int*)(AXI4GPIO_CONFIG_BASE)));
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    for (unsigned int i=0; i<sw_kernel_size; i+=4) {                  //Copy RAM contents
        *((volatile unsigned int *)(RAM_LOW_ADDR + i)) = *((volatile unsigned int*)(AXI4BRAM_BASE + i));
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    *((volatile unsigned int*)(AXI4GPIO_STATE_BASE)) = 1;             //IDLE go high
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    while ((*((volatile unsigned int*)(AXI4GPIO_START_BASE))) != 0);  //Wait for the initial state
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    asm ("lui ra, %0;jalr ra,ra,0" \
            : /* no outputs */ \
            :"g" (RAM_LOW_ADDR>>12)
    );
    ///////////////////////////////////////////////////////////////////////////////////////////////////
   
}
